{
    "id": "35482b56-769d-48b5-a571-436e42441b3a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene_thin",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f2c1349f-43cb-42d0-86e5-8541f85d3877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a8ba30fb-25f6-4973-959c-e45bdfe90d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "85e834c1-fa9e-4a23-8bd5-51b9b30f0835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6e14551b-96dd-4e44-8c7a-fb5edb48ca30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 69,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "29bc5fdf-3bae-4478-9560-f078d49fe27e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "996dae4d-862e-4354-8aff-f0375aedd825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4b9a7c24-1a6d-46c7-a89e-e1d5a1c94da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fc977857-ef6d-476b-a650-6416ccbc3cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 487,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9e4cd9d0-61f1-4f3d-96c3-519e88b5bf23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 478,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cf92eeb7-129e-4ef9-92e3-fafdad8b7008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 469,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ab71171f-8f51-4736-af96-228ad09ae624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "431b822d-3497-49f0-aef9-6a36d48e6f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "63863c47-8001-4503-ac03-3903335c038f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 430,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1eea01de-ab73-45ee-8c0c-3b9a0a75170c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6c06914b-a966-4a9f-bf0c-2e7c8e4bafd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 414,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1ef39c0c-fb5f-4c3f-bf94-30ede9a99ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 400,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f178dcce-ed92-4679-b083-a810f3b0674f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 382,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "279ea356-7b2a-4f73-8100-0845cc5ee7f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 8,
                "x": 372,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bf666d72-9406-4f13-aadf-8288d1e498b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 354,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a7e4eb63-d7a9-4907-8b7b-7f4526f8addf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 336,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2b0e0765-5520-4b44-9ef8-9337de27e4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 318,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7ea371be-4e6d-4e2a-84ba-ba7fe3e468c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c493b176-14e7-4e8f-bf57-78b07ce82a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 136,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "de3abea4-5d75-4ae9-9afa-f37d8ef83e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 319,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a3bdc2cd-f457-4f4c-9a0b-7c73db57b258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 154,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d734dfb1-e142-40b8-ad18-c4755bba5314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 481,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5e5cefa1-ff83-4296-ad5d-6f711d44cb02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 475,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ba031ad1-2b39-4c85-b6c7-4000a577b0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 469,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4bc05de7-51fb-4933-a908-bfba4f06f368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "74e89244-a51d-4357-be45-19e1ac5ed88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 439,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "87810eb4-699c-4a18-86d6-29514ebc7a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e9492736-dc2f-498a-8667-e4ce3938c398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 410,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6c47db1a-ee79-4a22-9045-cda93419cc21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 381,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8182b7f1-ccff-4616-84aa-f76f2e54c92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 358,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "cc54175e-b12e-4e78-ae18-1911ccef6dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6c911dd0-20a8-40a0-b64a-961226ab5461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 337,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "57cb7d57-a6d0-43e5-b031-3d125bef12de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 298,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a1126bda-7414-478a-b5aa-132e8ec9a95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "170765c5-bf13-4f56-a69d-e292969f8d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 265,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fefba040-b44c-4561-8989-efc7e1ddf136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "762ae4bf-adc2-43d5-8d41-a2bac5aaade4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 224,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8eb512ed-7694-4364-af18-32075fbb3802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 219,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "50752ad8-4dfe-40aa-80a5-07bf643c33e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "36816ba1-db43-417d-8888-c5085e1d21ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5b70e67b-8ef7-4437-a423-1b71a5530f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c6e1f0a7-d0b0-4a1c-a2cd-61f6114045be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 295,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "41061f02-a2d5-41f7-9c9e-2b8171cd35e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 116,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f92b8d14-1618-487f-8b95-cb5e9c73a062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fb2815cb-e58e-4a72-876b-c334f255ce9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 393,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ef9ba043-5d67-40b1-8eeb-a62019bbfa34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "dc667d8f-3618-40de-9321-ef6fafb3c02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bbe11d56-c932-4d66-b9b7-2a383f59f8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bad545b8-7f4f-4e3e-81dc-a0cfb2caf161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "87ff51aa-3f9b-428c-9fef-d3a74aace77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "88175426-8880-44b4-8b16-2638065063cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "71b7fbb7-266e-4b67-b3e6-98971f6d6192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fdd0cfc0-1228-4db6-8043-54e947d1a0a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ea784a04-9f47-4bd3-bc04-9b1633be8237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a59161ad-e468-44ff-8610-baf62a633b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1498df8b-87f7-49b7-bc91-d9f043ca6727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "32ec8332-e5b4-4c8c-a6c2-415bf428f7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ea2c12a2-778d-4615-b74f-3bd6ec6a84a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fa6d4cc6-25da-4937-9e09-e2890e2168a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9f0779ee-51d3-465e-be75-913c347359eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "26c71164-f131-4f17-be10-ed7397c9b400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0ad348f5-0eb1-4bcd-9148-4d58cedd2f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "366e9338-aa83-4222-8ef2-c90408825dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7945a63e-1ce9-4715-aa32-031c873b5d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6cc92342-e818-4eca-b85e-35570462f8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e88a56e4-55ca-4ace-888a-7a8b5f960e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "03bc6be3-54f5-4111-bef3-0d83c220ab67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "23e9d2fc-76a2-4472-a132-07339b3789a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bfed2565-c911-408c-8e34-c3813524449d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a413500a-50b5-45cf-ad0c-5850daa7ab1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 244,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "effffca2-0c31-4336-9811-5a21d1ea6df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 8,
                "w": 9,
                "x": 233,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "76f8d526-0859-4e5d-aec2-a4ae49e5aa80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "80e120f8-7e55-4178-aa3d-224e18ae3cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b2982ab5-9cb8-4a99-9475-6294de6db5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1215f579-a483-42fe-b187-eb96b725c106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 164,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9b0f1edd-6761-4a2b-a7db-d78a3f31d2e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "310a3450-d087-4b70-8a27-79ef7de1fa59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 127,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c683855b-c564-46a5-8406-379bf24e5079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "37127cb4-4235-42d0-9d9e-3a738109a350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 250,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "98c9b809-a2eb-49bb-9268-73a261295dce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "44b3856a-ee09-49a4-b87c-e5cf90225235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "98cfe7ad-5568-4867-b9e1-ce17baf4b5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 47,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a7585af0-ee5d-44fa-ae8c-81496a68e9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0b9f905e-7ae1-4f0c-b348-c2f2723df4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "82d5f203-df00-409d-861f-be5089734c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "07f5b9a5-c7d6-48c0-babf-93920c4b4b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4f89eb3e-5fe4-4076-8984-ff5c33ce2eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b42cd505-7d75-460e-9349-6cea02ff416e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "157567f4-3394-436e-97c8-159f4ada3c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "eb172b0e-de38-4e1c-85ed-299b153abadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 261,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c0765fe2-1274-49a7-918e-2da17ad5ef49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 21,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}