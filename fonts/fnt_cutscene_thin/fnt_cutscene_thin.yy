{
    "id": "35482b56-769d-48b5-a571-436e42441b3a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene_thin",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7b1c06aa-127d-4160-9d16-b5b6b5da6871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "964656fb-1eb8-4702-ab98-ca9288b748cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3b8eae73-640d-427d-9e57-8e03c0b36c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1ec86854-a9d2-45d1-bc18-418b128e4f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 69,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6717aa58-8d53-483e-ad40-38a01f57ab6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "586b45c3-a862-4ed7-8f3c-834365523461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f0669b6f-3b27-44ff-a925-1ef15adf4fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0e8aa160-afcb-4363-a51d-65b6d029a25d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 487,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b67a41b5-5110-4f2b-9fe6-e77b60904412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 478,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "15d7852d-2569-4b07-93d0-ea9617ac8e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 469,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d41cf838-7e74-4d92-8960-9cee976719ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "22d8f0cf-129c-43a1-984e-1f360d813fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cfd672a0-f69f-4ce1-b56c-2b8ce6b5ce0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 430,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b94eb6bb-0265-4e85-9054-93bf30991f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b94c6e19-7fb9-4452-a043-fe49e59d8130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 414,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "798d5e96-fd2f-4a01-8a27-8598620047ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 400,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d7fa2930-f491-44db-889a-4b797206625a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 382,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "98fafe0d-7da8-420f-8f54-7d27944843f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 8,
                "x": 372,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2478445d-d143-4ce1-b5c4-038327e0fdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 354,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "42b6bd1d-4edd-4dfa-b6f5-c356f6060929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 336,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7ec989db-534b-4126-9bc5-71f572bce6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 318,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "26e597db-92db-4c1c-98c3-6171dec4a691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0dd26762-d654-4535-8f48-86c0e7edf88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 136,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7c4dc49d-7b45-4c57-a543-24cfc5f01045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 319,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6b138935-042e-41f2-bc6b-22d61a9b2bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 154,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0fdad052-1dca-4de7-a9cc-833f9c3bf79a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 481,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "93018b2d-1d2e-485c-9a8d-22391b0cd37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 475,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7719c578-fb86-47d5-a241-b4dd8ecb2c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 469,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "271bc775-1a37-40e8-a9f9-4bbc88ad05a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bfd0c079-7e97-42a4-9266-ea9ef017c467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 439,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "747b5212-01d4-4125-9ec2-feae5b7db839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e052f716-3b44-4cc4-8637-cb202f6ba7f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 410,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "359b91ed-5a93-4ea2-af1e-f65342f43a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 381,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "beacd25f-e708-417f-b075-d842368e1842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 358,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "42fce157-c3a5-481f-8392-01f4aa874e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8f475272-43d9-43f9-85dc-c01fdd913f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 337,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "edc914f3-acb8-4325-a077-08493aae3342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 298,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "33a53a3b-f4f5-4a1e-94b3-c7de82dad6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c4469a4a-5eaa-4b09-aee5-684de0e7aede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 265,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "37d853c5-5264-4f4e-aedc-ac15e08c4059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eb84928a-0c96-4f30-86c6-254db1112f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 224,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "40984848-d36a-4bec-af32-121c465b4505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 219,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d10fea69-866a-4321-a32b-c4a6a1c9323c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1f6e0c54-8f8d-4a65-b255-dab0068018ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "12efc58b-1df1-45a8-92be-427d2d77ce9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2d067262-fb85-4b67-b52b-8ed12f4c451b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 295,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "06cbff41-147a-40fc-9a77-55466e49ad5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 116,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "440194c4-320f-4038-931b-6be262696058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f1e061ae-a7d1-4dbc-ac9e-d41faaf3fbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 393,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "89805045-b303-4f9e-ab5b-aee27dabce20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1adcbfcb-d7ca-4bb1-9e39-78277cc104d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "608b7d5b-7096-4942-a33f-49633688c4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7da64574-e07c-4212-be5a-66448d3a4da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1b7e5b36-3c96-4467-87b7-799dbff968da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "498a122f-e0cd-475d-929f-2ea924fdbabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1d4da0b5-6c46-4c3b-881b-654d50a8f102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "82084087-b08b-4978-a9e4-abb0aed3ab65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4efcf99c-613d-41dd-90a5-9941fd23f783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8ebd87ae-0a6c-4d70-a04d-f899167ed4f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9ad47cb0-b710-4906-acec-f792bd914022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "00a2f73d-65d2-4630-9f50-dced518d3a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6ba78636-f4b1-4eab-a9d2-d5e9d0c0d0e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "de362999-694a-4834-bf4a-d505032064a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7c9eac82-b800-4329-93be-0d2aabf3e03b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "461ed004-4b49-429e-be02-c2a2c1eb8889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c50a53b5-6313-4dcb-a079-c799432905a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "06b891a0-dc0d-41b7-8c52-fe7185c4b507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5a4a9c29-a8eb-40fe-a60e-924c4abab894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "75c5875d-e623-46c2-838d-f99021e813ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7f8b11d5-9e54-44a5-877b-111753f6f439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e7a74a72-0588-4125-8995-12a7488ee0e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ddc47363-d431-4dac-bca3-2a963aecd34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "31424810-f848-48de-b649-d7badce87849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7e3ae0e1-a535-46c3-84f6-a99d23dc0373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 244,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d6aa4722-40af-4c05-8fb6-1d1f6d74e73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 8,
                "w": 9,
                "x": 233,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4d32e0f5-101f-4cf7-a3ae-926566a3478f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "78fd724e-3f41-4cc4-bb0c-59c5782d969d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f7f4aedf-124c-45a0-aa1b-6b62bfb887ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3d8d4c45-558b-4217-83de-5d4783e87b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 164,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "32d94db1-1cdb-4f0a-9113-6de692c844ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "df2f6451-6041-47e4-b11d-c1574b042744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 127,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "689ce651-c848-49a7-8034-1670eb9c6b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "248f5958-f6a7-49a0-96ac-8d4d567f7a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 250,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7e2c3eab-92fe-4c7f-b10c-6e13be934605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "28b7d4e2-06c8-477c-b53a-1984e27a7d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "36c1f8ef-d3e9-4ed7-841f-3dd2671d256f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 47,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "77c83b8a-89d4-481b-b0b2-d619a789fd46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a04d0ab7-690b-414a-9e4e-7d66b962d86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0b91abee-5d04-45f0-8045-7e51b167e1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7adaeb71-2929-4088-9940-dde18ec8706c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b5f270dc-f866-4f96-ae1a-3f4c10a2d7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a22a7f73-fc0b-4279-b7e0-e65594cd506a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d6e786a8-9bd2-4436-8685-9337d682e323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "33a7b25e-9724-415c-bd52-7de50c073d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 261,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "08ef860f-3ff8-466f-9d71-38ae0e85ecec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 21,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}