{
    "id": "55250f7d-5bea-4e54-89c6-ccd459a6b82c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9e0d7672-6677-4950-b924-b7dabd93d9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cd0eb5f8-9ae9-417c-ba9e-7ae7f4ff5189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 167,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "da4b3231-1a1f-4366-b60f-b89f30e9a826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e7d85281-d90b-43c2-b64b-09a89569a6c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 133,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9889d0b0-10b7-4abd-96d8-fe83c9a4ff94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 113,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "44b9f4ac-85e8-441b-9eb9-d8ea96408dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 84,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "27f9df93-7b66-4232-9869-9d1eeccf7701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 63,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9d642765-2d93-4e16-ac97-77e1f4847a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dc203841-67a8-4c17-b205-d0fa89952153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b002fecd-6675-45a1-a8dd-cd0578e5cb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4089ad92-bdca-4e7f-a000-02a5779df30c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 174,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4aef294b-0cea-49f1-b67d-ef395b1d1ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 21,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "842d9632-d1ae-4e88-9a98-1bd10f1d85af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 500,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "958ae69b-577a-4762-b0a0-2db83a15a1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 489,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "67f854b5-8ece-48f9-a7f2-8e19a2946e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 482,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6727e5be-55a8-4724-a3ea-7d6598cf85ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 10,
                "w": 13,
                "x": 467,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "00894251-8b39-415e-9b67-7a8445e83839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 449,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7498301f-f980-4e3a-882c-ea6099828c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 438,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6984c336-0d8b-4ab5-b562-407d9d63e98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "37e1d613-22f8-4745-90bc-56497bc68908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 401,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c3856432-8ce0-42ae-864a-9f92c1781386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 381,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9f96ab36-d12a-496c-b7f1-89b4c2347a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bc7b767b-530f-41ff-a23e-0bdcc08d5f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 207,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "576028f9-c3e8-4e47-a48a-5aa7a66b9b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 405,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "045f6beb-8cd1-4890-8dc2-4ea6122b69a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 226,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2fb95f1c-6bd5-4c6d-b792-cee2dd423dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 78,
                "y": 116
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e599b938-dac6-480f-ad4e-3d78a3409fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 116
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ad1e9596-8f6c-4968-a622-98619716b7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 116
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d7961553-5df7-4a00-a5f7-40af3a7ee4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 116
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "07d43cb3-d72b-4655-a004-bcec591ae8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 116
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4d623feb-b433-4a7a-99ec-645e32bae4e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 116
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3d610baa-2895-4931-9e19-37e8c293d9b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7662fb35-b0a6-4bef-9a31-1acfcafa1c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 471,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9bb35614-eef1-4f8b-b8dc-2e19d132de50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 446,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "15f0a06c-4067-4d0d-b033-56e948185ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 96,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "808af7c3-67df-45be-aa15-ac667c02cacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cfa4550a-ec53-48c7-bd40-0cbeb5918644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 383,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27715721-07dd-40b6-91df-a746273c906d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 365,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "65db2e2c-4559-4391-979a-88b604ce69a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 348,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1e9a7b6a-fc8a-47b9-b452-7ca592ddb7c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 325,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "95068b11-aba0-4c11-80ad-dbb67f9dc508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 304,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "abb3fe1b-4869-487d-a6bc-bf2c78b345cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 297,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "499a2f3f-6624-46a7-becf-6be7c2872e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "71046b9b-9a5d-433c-bd73-2ec0d171dbc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 260,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "31f42c88-808b-41bf-96b2-6990edd49963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0246593a-9af7-4866-b6f0-903e19125b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 356,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "812aebd7-9961-47cf-8b0a-0b9bbed4a7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e07b782f-9a5a-4092-9d1c-81dac5184e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 331,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fbc1dae9-af7e-4a0f-9eeb-7f2d74a468fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dc16ebe9-22a5-4236-a1d8-f3b88b396374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1188ea5b-fab9-4ac1-be3a-6f34a15b96b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d076807c-5f36-4fdf-a655-e591c46a7e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a4c65a2a-a0b4-40cf-9f6c-2d64471ca9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b06f7769-bdc8-4119-8fd2-0358db65797a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7e057b06-9567-455a-bce3-5815684791c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6a1137d6-7e7f-4104-b60d-4326ccc42434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "89d13bb1-86c4-46bc-9172-27b36fa70d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a9a51066-dce3-4314-a152-8d4585ade99c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "66cffcc0-163c-45eb-92ff-712a4ec36170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f5a315bc-9c00-4137-bab0-6342a1bdf016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "382c7ebd-a128-4780-bd32-cb6443a2315e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9d024408-d0a2-4e93-a9e1-e68da77939cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "83d45687-9eb1-4b52-b9cf-3d813bece8d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8feea3e0-744b-4d06-8dc0-c750f494edba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2e2b6139-69ff-4302-a8af-f4ceabafd7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b5b7bbb0-2ea1-4d36-b5d8-fe60e1c899db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a4f1a3a6-4626-4fc6-8ccf-e266d486df8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6644c368-75ab-4d1c-b605-16b8768c9364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2c37a035-ad5e-40c6-a771-dfe8e8160ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c43df5c5-ec62-467c-b88b-84e946fb629a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "797caec6-db5b-4e71-b877-ce5b23de1f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 12,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "be75fe02-27b9-4e3c-8c7c-7a1c9d2ba921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 122,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c477c892-e36a-469a-b58a-7dc2afe1fe1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "146cb2de-dbde-496f-b5b3-73521e07c3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 301,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "684d7215-3e68-46c7-aec8-aa9b2d3856dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 9,
                "w": 10,
                "x": 289,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1efa288f-4e5f-4491-acc8-9d02ceeeb38c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "94d3f5bd-ee78-4163-a40a-66ad7ec4c7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 264,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f8f71116-245f-4e7c-9b86-eb4bdd2007f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 235,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c5c91987-a290-42c8-a77c-42778fb25744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "566ce36e-c6a2-4804-b11d-08b7ad26d3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 197,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4cfffb22-2368-403c-b8da-81eaf9de2dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 177,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f171e53d-2aa0-4f92-a37e-a91b68ce7202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a5f8719d-3e94-469f-af04-329f49b57cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 308,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8ae6accc-0328-41a6-a102-9f8dccc9f42d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 141,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0622dd09-c626-4a83-b7ba-70c70a6617b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 108,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e7f9c6f9-8ff1-4177-8804-e29d0d924cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 90,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f770fbab-f99e-42e4-a13d-85089ad5dd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 71,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "48b941fa-599a-4034-bffb-03c93a20d9d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7bd4bba2-0e31-4b75-9842-73c6434f5268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 22,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "980e3552-1748-4468-97a5-4d205f97546a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cbb32ce5-8da0-486f-827c-0dec53b70c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 489,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8fd2c0f8-9efb-4ae8-9c2f-05c932a40866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c00c135c-3807-49d6-9b48-23f33320a636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "39ed1c06-b194-4a5e-a517-1c1a1e15215a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 320,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "18572604-dd4a-4d31-9a44-5178af3a3629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 116,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}