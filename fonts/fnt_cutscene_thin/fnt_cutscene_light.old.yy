{
    "id": "35482b56-769d-48b5-a571-436e42441b3a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene_light",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7d430294-db4c-495c-8990-129a6a300014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9154bae9-5912-4fa4-965c-ce8f5d678f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6c546fa1-d6ed-4d42-b797-f961c791afd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7a80106b-bf5a-4d8b-abf0-193811278ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 69,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "920fe341-9e19-430a-8392-7e6ae2fbd5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "30535db8-2c36-4d78-a3bc-974a9f1bd006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2d2ad26a-6943-4718-86f6-83e1e61f711c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5a1511a7-5e7d-4c1c-9389-28c9271cc39f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 487,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "52fb6f6a-ffde-439a-9116-ffd334b00e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 478,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "83c26646-3ae0-45ba-ac5d-f9dafddea820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 469,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "26e01a20-75a2-482c-a177-a1360158df7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d17e72de-df64-4c88-8f99-d03dba707eba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c643fd3b-0406-4dc1-b10a-1fae015396f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 430,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0255bac5-dd23-466b-9354-a4ff4f7a59f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1dc2e3c9-fec7-4c09-a64d-eeba19ace73d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 414,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "374e4c8a-2acb-41fb-935d-e892e57357e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 400,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "710f002a-e459-4271-a6b8-0000098846d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 382,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0002ef0f-e67f-43d8-88ad-5d1a92230ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 8,
                "x": 372,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1b735895-2a52-4e2b-a2ff-d9c26c522a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 354,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "65453eb3-f773-4a24-aa28-e8e9f116cfd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 336,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9fcbf339-551c-4137-b9a7-75c30e9460e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 318,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "52db4a8f-4ffc-49c8-939f-0954adf653bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "166bb478-505f-4ed4-bd94-4e1259ab5bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 136,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fc86af33-9a72-4294-810f-7d57b36d3bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 319,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e4fd2604-b37e-489c-bd99-1bc0260413b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 154,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a73ed8d0-7456-44f7-ab32-5f821b877968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 481,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a6377c25-59da-4953-8bd2-38ece106a9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 475,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d3975b0c-540f-4a10-a4db-c990e4c387f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 469,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "56e89e31-98c1-4612-bebf-97e0dcd70b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "66bebe59-06e0-41ac-a5d5-aab95aafeffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 439,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7dd48eb2-0c8a-40e9-a68a-a12173c36969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "04c8afb2-cc8d-4ff9-9aac-315652f30c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 410,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4ae98f86-6d03-4a8c-9d08-5bc0adce2206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 381,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "569d5083-2099-4e5d-90fc-21a4c0b46ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 358,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6903d2bf-793d-4aac-8508-3999e1a27519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9eb2390d-455c-4aaf-b08c-a65726b0efe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 337,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fe50d5a3-3506-44dc-a7a8-cd9107f08c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 298,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1d691cb4-b2c8-4cc6-afb6-6233e0c2c49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ab29f3ed-5295-4e76-914e-d3aa72bf93dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 265,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0b0357f9-44da-47d5-a541-77d148bba367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3d8931b6-c9ca-49d6-bada-3fe2df1d043f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 224,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bce23c38-096f-4a36-9659-1d69f80c4efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 219,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9c04d9fc-fb1a-4c2d-a232-e5f64b33daf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "593aa19c-7e7b-4275-ab6f-5caa0892b04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "edd137fe-d503-4fd0-bda7-450c181092eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "82a36594-f3fa-4a9e-b751-39efc838988e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 295,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0dc75ba9-df00-49f4-a7c9-44faf1d3971d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 116,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f89f0dac-c01b-4bf8-9a92-509513de66a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "067dcb59-4721-420e-9e1a-bb35c1e39c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 393,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9e5836d8-cec3-4567-83e0-986936230c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2c2939a3-1cbc-4f24-86e4-2d1e66a98d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "71706aed-bb1f-47f3-b5a7-04906609c257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f5c6617b-6260-4250-ae60-887ab5d1122c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "561f46eb-f10f-4c29-a511-cf8586492a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e944a99e-97b5-4f5e-a8ef-8ae61da4ef1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c63d5c81-1098-4768-b90c-c6fd28a6d99c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1f5ac6c8-1fea-4944-bccc-304760b683c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "02c89b49-aa51-424d-965b-0342c5b4f096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cc1764e9-cd34-4096-82cb-99c6629ecf9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e29f71aa-697d-4ee3-82a6-38c13efb4fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "db56742c-c988-4854-934b-753e68c2eb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "80368694-c5f4-4d5c-9c8e-96e7284f7148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0646fe01-cb17-4e27-9805-884fcfa31935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fcfea5f5-e68f-4c5c-ae4e-a3e7faff51e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "922f6668-8346-4b81-85bc-f813e1fdaa32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ee84ea0a-d5a1-4756-a7d8-4898c517ff6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c3756c62-9250-4a1e-9718-f98eb0937536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "71e8f5ac-90b3-4cab-a94d-9ecb4e19170f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c1379843-5aa0-4e01-9de0-6c774718784c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f99bbe55-57e4-46e0-ad4d-96bc9e7d70f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5c4accab-a526-49a0-80b8-7a7454c8563f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b3f92303-6c94-495b-b16e-093b8ac0cb78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "04ac7a3a-6ba2-441d-b0e7-0ae01881de08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "91dd168d-11a4-4d61-a62e-f63d3bce3a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 244,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b3bf2d59-cd34-4423-85ab-6b4145b44f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 8,
                "w": 9,
                "x": 233,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "18d67ab4-80fa-4242-aa8b-1bd2c5ac5bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f7e7dc3d-a9b6-4d6a-9cac-d443ebb7af14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5921c94e-84ee-4c96-8cf1-6dc55463e2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bf794e4c-6d54-4f3f-b7b6-b76548592e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 164,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f50f31b2-190b-4184-9a5d-6c032497e430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8de582f2-1905-432e-9136-4a8c2f55db35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 127,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8d7ec86c-9de6-4537-83a5-e70bc8760e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c17c3081-244c-4ced-9fc2-0ac49c7d5e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 250,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9328f836-27ab-4c48-acf2-6c6ce2d1bea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f64ea218-656d-4dc6-a7ef-b9ff73b45d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "08a5e35c-d83f-474d-b02e-5fb4be2feec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 47,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "67bf32e7-97bc-4485-a2b8-a9f86b6da2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8d30e4e4-c5da-4767-9039-813465631b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "12b6ef9b-7d92-4ec1-bc4c-0e6c75d35f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ac6df6b0-39bf-4a8c-845b-0d8dce661825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0e1a3768-5f04-4cd0-b418-a82c6ee8cf21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9410ca90-073c-47ac-9c3e-8554b6b3bad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "732bda6b-34f6-4f8f-9786-d97e1fd8a73c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "169257ee-af9d-44b9-a88a-845b6bae4475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 261,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7f5ce866-c343-4687-a641-cca283edfdbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 21,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}