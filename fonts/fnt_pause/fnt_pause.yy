{
    "id": "ef16ef0a-ba99-42a2-bec1-52a56fdfe41f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pause",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f087c60f-a7a2-4dfe-af36-1632bdc71ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "588515b7-3ccc-48c3-8818-deb00be19952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 55,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 70,
                "y": 173
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "de1b8996-7f76-4255-8feb-43a697ba2e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 60,
                "y": 173
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6d5440ae-d768-4b29-abb8-705dd2e80feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 55,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 28,
                "y": 173
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a37ab03a-de88-4767-aec1-8a644c299833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 173
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a8fa47d6-2f83-4b00-ae40-4e4f5c0f2159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 55,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 449,
                "y": 116
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1a08ffbd-a497-4c8e-aaa9-dda5901935df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 55,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 420,
                "y": 116
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "89d30c91-8b31-401e-ab84-c106de80cbd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 55,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 415,
                "y": 116
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "409eb8b6-5e54-41f4-8b0c-471fb06eb3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 8,
                "x": 405,
                "y": 116
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ba7da137-e31c-49b6-98d1-ad17f213cfec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 395,
                "y": 116
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "78b0336b-a055-4110-bbe1-ba67ba2fcfdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 55,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 76,
                "y": 173
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a0763871-3f6b-4eef-8c5a-e14629605a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 373,
                "y": 116
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "82195ad3-f642-4211-af63-706c9bcf3401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 342,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6dc948a8-1814-4576-aa0f-a82380e50fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 329,
                "y": 116
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ccfdab85-14c9-4c79-8fba-f1bd402a1de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 323,
                "y": 116
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "933b1b2b-6c12-4375-bc49-c2cfc54b7219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 55,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 304,
                "y": 116
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9fd5d213-bf34-4b2d-9a59-dccb03b3e946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 278,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9e507b81-5a3e-424c-bc09-f8ffb59836b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 55,
                "offset": 5,
                "shift": 28,
                "w": 11,
                "x": 265,
                "y": 116
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c55dc185-9a97-44d5-8a9e-51c1da9a52b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 240,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dce29040-e9b4-475b-b0c2-5f3e40964c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 23,
                "x": 215,
                "y": 116
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3e2c30ab-8da3-4d4d-9fd8-8a184ee893b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 189,
                "y": 116
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "adc62649-6008-4d34-b5c6-5517aab7dda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 348,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c0cef73b-7aae-4110-8a80-51812b3043db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 118,
                "y": 173
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fcd162d1-8ef4-4626-b212-fe4640ba4814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 376,
                "y": 173
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a5bc5e97-9077-4fa8-a494-e64d64cd207e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 144,
                "y": 173
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "af32564d-f876-4498-a8bc-2821bbacce2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 99,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0a033fe1-abf0-4b7e-8084-27b00b18f16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 93,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7089fa4b-f75a-4a27-8354-4d5d31221fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 87,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "46e04a7b-e0f3-4206-931c-475d75a270a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 65,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "814e9bbe-cd2a-4b2b-9b52-bf48c068774f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 43,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4e932098-a91c-43bc-8fa8-ddf8c3c9e621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 21,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8242f568-ed25-4961-b556-056008cea389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 55,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b5670cb5-9bd5-42a6-b8cc-966f0f8bfea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 2,
                "shift": 45,
                "w": 42,
                "x": 466,
                "y": 173
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1561a9df-0867-4d48-ae0b-940ff5343f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 432,
                "y": 173
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fc3421b1-6952-4f8d-96e7-68af661e1a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 5,
                "shift": 33,
                "w": 25,
                "x": 125,
                "y": 230
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4349d25b-013f-4d0b-906d-48510c619ad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 402,
                "y": 173
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a774e95c-df53-47ab-9511-520ad5dc20fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 30,
                "x": 344,
                "y": 173
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "15f2e023-77cc-4219-9f7f-8a5878fbe072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 5,
                "shift": 29,
                "w": 22,
                "x": 320,
                "y": 173
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0cbe8660-9112-41fc-a88d-48c3ee0940f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 5,
                "shift": 25,
                "w": 20,
                "x": 298,
                "y": 173
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9644e6ed-1689-4a9c-8f43-f7740dae0cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 2,
                "shift": 34,
                "w": 28,
                "x": 268,
                "y": 173
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4db63eda-7084-4e66-93f7-8b625c86ced8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 240,
                "y": 173
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e8e01751-61a4-4f7e-99e0-b57cf7dd7285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 235,
                "y": 173
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "40c266ea-ecda-4d28-a7cc-febae60de216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": 0,
                "shift": 21,
                "w": 17,
                "x": 216,
                "y": 173
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "86301922-de8c-4cf5-98fd-cef511edb16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 5,
                "shift": 31,
                "w": 25,
                "x": 189,
                "y": 173
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "04ebf549-bc9f-42d4-bb51-98ed6f1b4bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 5,
                "shift": 23,
                "w": 17,
                "x": 170,
                "y": 173
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8b0f446e-9911-4483-b472-1013771c96de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 5,
                "shift": 42,
                "w": 32,
                "x": 155,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "33138da2-b873-452d-9ba1-1e4ab70aa1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 90,
                "y": 173
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e0f6d95e-80f0-4e80-b856-bf38d51550b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 120,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "027faef7-3997-49f6-b6ee-acb2b80460ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 5,
                "shift": 30,
                "w": 24,
                "x": 67,
                "y": 59
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "48125d48-d1a5-43c8-8675-66beb078da80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a4b9a710-f5d8-41de-b65c-3ea74cdd8fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 5,
                "shift": 33,
                "w": 25,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fa5407be-b76b-4c7c-ba9c-0ff23adaaa6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "67232422-d28d-4685-850a-4124d5292907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3189688a-b44f-46da-b965-9e9af7994029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 4,
                "shift": 35,
                "w": 27,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "000a4704-b320-4638-912d-cb3877cf3a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 346,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1f44a63a-6c34-42c7-b0e0-0be73101d9f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cc84bd0f-56a6-4d86-a4e2-c5ce4e941376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2b5d2f00-5af1-48b5-b712-598310e71ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "be4fe56a-80d5-4072-a711-939a41dff9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 39,
                "y": 59
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "91370353-6256-4a09-b843-48688add2002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 55,
                "offset": 5,
                "shift": 15,
                "w": 8,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ab89500f-bbba-4aee-a7c0-84a606187149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5fc9f6cd-f48a-443c-bef0-8defb90aea81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b8cf87a9-c739-4c18-93ed-43e844e7aad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "91bc45f9-1cee-4855-bd97-c93377d4a489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 55,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6553e1a0-68ea-46bf-8e04-ddf14184040e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a016ebd6-16cf-41ac-90d7-d2f46ac0ddb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 2,
                "shift": 26,
                "w": 20,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "86b37634-b9f7-4062-b79c-e822e77bda95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 25,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0f6757f5-c00f-4cc7-bb03-db358805b1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1ef83058-c331-4e11-af28-79831062f247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 2,
                "shift": 31,
                "w": 24,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "be42ff9d-c598-4c29-a194-17c666414e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bbc82b62-19b6-4cf4-bc9d-b3da48612688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 2,
                "shift": 14,
                "w": 15,
                "x": 93,
                "y": 59
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b3f8d934-778c-44c5-80e1-59d24ee04683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 2,
                "shift": 29,
                "w": 23,
                "x": 326,
                "y": 59
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b5a29f62-b52a-4b8a-ae6e-0da2402593d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 22,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6ea549e3-0445-4dbc-9a0e-7053cfbe4c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 87,
                "y": 116
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f5eaa531-700f-43c5-9fdf-93dcc5e02043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -5,
                "shift": 11,
                "w": 13,
                "x": 72,
                "y": 116
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "266dfbdc-2ab5-48dc-9364-c97271dfa9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 4,
                "shift": 25,
                "w": 21,
                "x": 49,
                "y": 116
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c952a5dd-21e8-4fa6-962c-4e97f04d9a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 44,
                "y": 116
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7f16870f-fee0-4481-bf54-20da64f0874a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 4,
                "shift": 47,
                "w": 40,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0e07dec9-05e5-4254-800b-c7715f2fcf0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 4,
                "shift": 29,
                "w": 22,
                "x": 450,
                "y": 59
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f246a942-fccb-4194-9955-017aaff52d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 424,
                "y": 59
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2e6c9526-a07c-4255-ae8e-c9e03207ddf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 25,
                "x": 397,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0ace2625-cd41-4a7f-8cfa-e8f4e701b59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 2,
                "shift": 31,
                "w": 24,
                "x": 371,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2382cc74-f908-4f29-a674-5de957c42000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 93,
                "y": 116
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "72008d71-6a8d-4594-b0dc-01628fe5e2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 351,
                "y": 59
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c287c215-410d-4aed-ba40-072314967501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 310,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3b43eff6-545d-415c-99a9-e26a1c3c25c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 3,
                "shift": 29,
                "w": 22,
                "x": 286,
                "y": 59
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dd4f97ee-52a7-4df3-a10a-809090d3b005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 262,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1fcfb95e-3f2c-41a5-9093-9302fdcc02d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 222,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3666c9b9-da12-421d-9311-327f6770145a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 199,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "961c6d32-ddb7-4937-a926-0fb375b7a4a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 174,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "557f51d0-13ad-48c5-89db-93cd69f79f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 152,
                "y": 59
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "25581359-3bcc-437d-93b9-b46fc2af1215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 139,
                "y": 59
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "89baca17-53f0-4cbb-9dd7-81205a5e202e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 55,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c51c5a9f-6203-480a-9195-1f0798cb0278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 55,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 107,
                "y": 116
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "953512ff-8fa4-416c-8067-a7f6986995bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 55,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 152,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 34,
    "styleName": "ExtraLight",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}