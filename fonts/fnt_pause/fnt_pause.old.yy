{
    "id": "ef16ef0a-ba99-42a2-bec1-52a56fdfe41f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pause",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "edd4d9df-190e-4add-bd36-49b840f851dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3c34b894-5b39-45d4-b622-0a16c11c4654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 55,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 70,
                "y": 173
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e67a3278-448c-4464-b5c5-47647e7c4714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 55,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 60,
                "y": 173
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "28ea30cd-c96c-4a08-9145-0a43ca4904e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 55,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 28,
                "y": 173
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "92666e7b-91b9-456a-9426-2dc838f31343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 173
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "94d3d367-da59-4971-b785-7fd64e328a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 55,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 449,
                "y": 116
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9fc2be4d-650b-4b08-80b9-a53f12eb97e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 55,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 420,
                "y": 116
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "82b59a9f-8f78-450e-9d8d-bdfc2db93e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 55,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 415,
                "y": 116
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "932e8f17-aa12-4301-87d8-2921fa841d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 8,
                "x": 405,
                "y": 116
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d3b0ceb1-8043-43b5-8168-0ba70d1ceaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 395,
                "y": 116
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3222dea3-57eb-472c-8ad6-5105943e5f82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 55,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 76,
                "y": 173
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9b5a5394-d97a-4580-aee8-5893574101ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 373,
                "y": 116
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fec2098c-dd2c-4445-bcf7-c7da0e5ce434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 342,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d36647aa-f739-4b16-89fc-5631fa231e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 329,
                "y": 116
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bf7be3df-bc8b-43d7-b8d2-5d4837f9a378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 323,
                "y": 116
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "364dcc53-d290-478f-b107-77ecebb3fbe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 55,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 304,
                "y": 116
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "209790d6-6547-4292-b606-a5bd61f81e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 278,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d880c07d-fe8d-4c7d-b9a7-3c642d68963e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 55,
                "offset": 5,
                "shift": 28,
                "w": 11,
                "x": 265,
                "y": 116
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bd166115-2cdc-4158-ae94-563ad9282400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 240,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "df77c39d-bfbf-4153-98b7-21f657ea5ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 23,
                "x": 215,
                "y": 116
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "34977d3f-2f9d-4966-b041-9dbd3bc3b83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 189,
                "y": 116
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fde0d159-15bd-48d7-881c-f64efe9d0456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 348,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "64dd16e6-92f9-4c50-b95d-6783af86ea5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 118,
                "y": 173
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0e6096c9-0b22-472a-b13c-d357995ea485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 376,
                "y": 173
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8a12578c-1ce7-48fd-a7ba-343f2872df9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 144,
                "y": 173
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5204e466-1496-4623-903e-9d0ef6918971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 99,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "446644dc-c921-4ce2-b090-bb95f3dfd313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 93,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "16414e74-e4e2-4524-94b8-1c682bf3abac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 55,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 87,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "310a22fa-6900-4477-9e67-76c86a7d09cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 65,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b3ff43a9-ee24-4381-8efa-91e86344a407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 43,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "05e084e4-87df-45d3-9718-46b8b082d1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 21,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "34bd5cbe-bdaf-4149-9af0-83f39acc0fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 55,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d21a7850-7b17-4516-8eda-17734123c7f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 2,
                "shift": 45,
                "w": 42,
                "x": 466,
                "y": 173
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9591b9e9-0439-4551-aed6-c9058245ea71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 432,
                "y": 173
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "de39c46d-4731-45f4-a7cf-771d7561c2ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 5,
                "shift": 33,
                "w": 25,
                "x": 125,
                "y": 230
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e5e54992-5f46-496d-a340-ce1c084b2354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 402,
                "y": 173
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e01ad6b1-833a-4ff9-b64f-01a473d739ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 30,
                "x": 344,
                "y": 173
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "81b6a023-15b9-4948-b51d-f6b0e59dcbef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 5,
                "shift": 29,
                "w": 22,
                "x": 320,
                "y": 173
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5e671f2f-eb69-4fe6-8fe3-7b42e6492fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 5,
                "shift": 25,
                "w": 20,
                "x": 298,
                "y": 173
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cdd68e9d-2a96-4ddc-93d8-c60ee0e8f642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 2,
                "shift": 34,
                "w": 28,
                "x": 268,
                "y": 173
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3525d62d-9cd6-40ea-8607-6f5fa74f8d77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 240,
                "y": 173
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ffd08c07-2d55-41b0-b7ad-77593b574ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 235,
                "y": 173
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ef4b775f-9848-419b-89f6-ca374cbfc4b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": 0,
                "shift": 21,
                "w": 17,
                "x": 216,
                "y": 173
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "162ee7b9-52e6-48d6-96fd-389b39d6cf3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 5,
                "shift": 31,
                "w": 25,
                "x": 189,
                "y": 173
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a5ac15aa-c512-48b9-9dbc-c17544b667c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 5,
                "shift": 23,
                "w": 17,
                "x": 170,
                "y": 173
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "db28cfee-4cc0-428b-b15c-b52547704042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 5,
                "shift": 42,
                "w": 32,
                "x": 155,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c141dfbe-1af5-4f01-b1da-b207f3d579d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 90,
                "y": 173
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "95ba0a03-e480-447a-a4fe-1d7e4f98377d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 120,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2ba8513f-a4fd-4d12-86e1-612040b8ced6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 5,
                "shift": 30,
                "w": 24,
                "x": 67,
                "y": 59
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d77bedfc-a656-47d0-8992-294df50e03aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a6c7f11-4792-4089-a0de-7aced63f4b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 5,
                "shift": 33,
                "w": 25,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "79a78e1f-6c32-424c-b6db-75730ebce834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dce444d1-b02f-4b95-ab89-a9540e87d5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d76651d5-3e75-4647-8258-31c46bbae4c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 4,
                "shift": 35,
                "w": 27,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a656a719-003e-4d4a-929c-b3746bfbcfed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 346,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4112ea72-a1d9-4f85-a576-b9e411a5d929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b712c365-d4a5-42ff-baed-770042402e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "dd9cf2eb-f3ca-4443-9034-b5d04205cbec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "425897cc-e717-48bd-b208-d1276451ae6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 39,
                "y": 59
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3145790f-de75-4088-93ef-091ac900b601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 55,
                "offset": 5,
                "shift": 15,
                "w": 8,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3ebee260-d6f1-4922-a5aa-2c5f1f51e7f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 55,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "918740a7-07a4-4738-ad11-c3893282c0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "961055d7-95f1-43ad-88c4-960156bd90d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 55,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "82ffe8ed-eed3-4b22-8bcf-7ad1e1a29690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 55,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6e4c3b75-610a-40a1-89dd-245809218796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d0d21316-448d-4278-bccd-f04eb89365bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 2,
                "shift": 26,
                "w": 20,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "24f56e8a-d523-42e8-8ae1-0a4f667f42ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 25,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2f6682e6-61d4-4399-8e93-6292548e9ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "89916f0f-731f-4d9a-96c4-a6e3cfbce9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 2,
                "shift": 31,
                "w": 24,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "65f93149-cfd5-4004-bd40-956915dee973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8c42456a-8ab1-472a-afcd-f472aacf250c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 2,
                "shift": 14,
                "w": 15,
                "x": 93,
                "y": 59
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "08712436-7c35-4292-9362-28b31360c0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 2,
                "shift": 29,
                "w": 23,
                "x": 326,
                "y": 59
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7d7dc4f0-016f-48ca-ad86-5f1c9900514d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 22,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "edf8d1c7-0805-434a-a715-b21faced6017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 87,
                "y": 116
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e8c5ce39-f7ac-4c69-9a0d-43b2e949ed4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -5,
                "shift": 11,
                "w": 13,
                "x": 72,
                "y": 116
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a0fcaf9b-c89b-43b0-b58d-aa9396bbb391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 4,
                "shift": 25,
                "w": 21,
                "x": 49,
                "y": 116
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4a2a7fc8-bb3e-4c24-bb37-b0cd7c6f2a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 44,
                "y": 116
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f71c8fdf-5520-47c1-8af2-5127d1763825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 4,
                "shift": 47,
                "w": 40,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a8b9b679-0015-493b-964d-b807cbcf2d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 4,
                "shift": 29,
                "w": 22,
                "x": 450,
                "y": 59
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9464071f-98f1-4db5-8000-1c4a1d4986e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 424,
                "y": 59
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f07f1ae5-6e90-43ac-babe-1c7db8c7bd51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 4,
                "shift": 30,
                "w": 25,
                "x": 397,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "725621fb-a3a7-4144-8ead-699596ff794b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 2,
                "shift": 31,
                "w": 24,
                "x": 371,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dd00aaa2-720e-4302-941f-16a1a1227653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 93,
                "y": 116
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bf36db88-6db8-482f-8ce7-895442fea0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 351,
                "y": 59
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "53e10226-cbb6-49cd-9d2e-913ff2246bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 310,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "465b25d5-4036-4e73-8c06-2a8c799061f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 3,
                "shift": 29,
                "w": 22,
                "x": 286,
                "y": 59
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b7f702d4-4796-40e7-aade-4095a896ed07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 262,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "47c5c94c-fcc4-4c59-b059-9afaa3dc427a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 222,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9852c863-1d95-43bb-a050-ea99443e1b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 199,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "112a1f45-fc96-4746-891f-16b8e459db6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 174,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "41f461d8-248d-4a36-bd98-4d3739c212b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 152,
                "y": 59
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "53688d1b-9024-4d6d-859c-892b4789d1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 55,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 139,
                "y": 59
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1c719605-9fbe-41de-8ae2-54ee69709022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 55,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c1bc399d-43b8-47e5-9601-9a5dfae06a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 55,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 107,
                "y": 116
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "86ac9ba4-22fe-4024-83dd-d6c14ff62459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 55,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 152,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 34,
    "styleName": "ExtraLight",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}