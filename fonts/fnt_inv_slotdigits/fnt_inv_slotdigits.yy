{
    "id": "a41765c7-b88a-4775-96d4-1e8a53d1b196",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_slotdigits",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Calibri",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6b776af2-bfa1-43c3-ad2d-00a96eeb1ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6fa19a20-63c2-4f91-b5ef-9a597d8d04ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "91bcbed7-7e7a-49a2-8694-2e14d6cc84eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7f45d257-63f0-4bd6-9a77-95bc0de3b251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "95fb46e7-3bf8-48ee-b9a9-266ef1413f31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "611c5843-968a-418c-8609-c6a6442b507d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b7e6bb30-c9f9-4f49-b398-31af13870809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b5be9526-7b8d-495a-8cdc-8f2044addc52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 27
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9c93b75e-e33c-403e-814b-7f591b5241ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 27
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "50ca46b4-6a54-466a-af5c-4dc0c78f843f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 27
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "83d686fb-13fb-4c6f-b3ee-9fca1db5de29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 27
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 48,
            "y": 57
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}