{
    "id": "8f0485b3-a83d-4d30-8c68-3236700d6764",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pauseselected",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2bb26300-b173-44aa-8a96-2cdb6d624340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b615f38d-e5ed-487b-bc36-7002ed049240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": -1,
                "shift": 34,
                "w": 35,
                "x": 220,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8a185d14-8a90-40fe-a83f-5085d661cfd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 257,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d2884b1a-1bb4-4c1a-9d9c-ef3b41064819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 287,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "86b282e3-f18a-4e9f-a364-ccab8b3df037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 319,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7b1e4b90-c7c1-4612-9b09-6a53a23d6a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 352,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "efc4c2ef-6c7e-4bf4-8673-6f16e1d4b486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 379,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b368da5a-8b97-430d-92a9-1f16cbcf8fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 1,
                "shift": 34,
                "w": 30,
                "x": 404,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "af78ad68-268d-4741-bdaf-9060aa34f93e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 3,
                "shift": 36,
                "w": 29,
                "x": 436,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b03813d4-8753-4b13-81df-201bd9cd9670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 467,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5d6c78d2-728a-46a1-a118-6e8979266588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": -1,
                "shift": 23,
                "w": 21,
                "x": 476,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f249aa31-5a5c-40bc-9ccc-2d3273c27e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 3,
                "shift": 33,
                "w": 30,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "923d4a43-64aa-4fdb-b448-998e2253e86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 34,
                "y": 116
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fb5add37-22d4-4eb7-9e6f-40dfd88e3775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 3,
                "shift": 42,
                "w": 35,
                "x": 57,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b4922d9a-2187-4e5b-afd6-1847d70a41b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 3,
                "shift": 36,
                "w": 29,
                "x": 94,
                "y": 116
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "44dafdb6-2645-4fec-af18-2c0b36432084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 125,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "aab1d505-7f69-4951-b757-b58e15a6b1d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 162,
                "y": 116
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "03f6d15c-f292-46d9-b9fa-e3872ea78a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 1,
                "shift": 37,
                "w": 37,
                "x": 191,
                "y": 116
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5bfd70dc-2b1d-4453-87fa-1f913b1b8b96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 230,
                "y": 116
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e3c7b138-19f5-4c2f-a530-196a8909772a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 260,
                "y": 116
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a315d70f-df66-44d0-bc27-caef13565e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 288,
                "y": 116
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c53721d1-d1da-47b5-80df-0ad54d080722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 317,
                "y": 116
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67378bb8-2c1c-4e44-9e62-12a312744494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 348,
                "y": 116
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ebb0fbc1-40e4-4407-b7db-fa62a9334aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 382,
                "y": 116
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "abec68b5-5763-4763-8a98-5d71392ae9c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 186,
                "y": 59
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b123151e-85d5-442f-8665-1fdb991433d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": -1,
                "shift": 29,
                "w": 31,
                "x": 432,
                "y": 116
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cd305f42-6641-4846-a993-b9921e832962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 156,
                "y": 59
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b436a422-5a01-40d3-8ecc-59d5f96295a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 103,
                "y": 59
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a636bffd-1f91-411b-98cd-17b538841a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0e1bf9d1-109b-4e29-bbab-42f71ba64e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a9dee96c-5eb4-40d1-b2a1-a7ae48a256de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e185bed2-b0b3-4da3-b59f-9280f64f8fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c57d105b-3c38-4601-87eb-825b4b4ecaf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 19,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6236b41f-7058-4711-9f6b-b04c91b0ca1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f687b673-261b-4b14-9daf-61edd879b39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "479b574c-d5fa-4679-b5b9-68ef844716a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "47fbe2fb-2c8a-4e72-b000-b88d433ae7f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -4,
                "shift": 13,
                "w": 15,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "99135848-06e8-47eb-8388-d0e10ef3e817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 25,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "aeb1c602-1711-48d9-8639-8e2d6eaef069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "256ef30f-e97c-4c44-88e9-b8205d64aabf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 266,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "06bb6de8-97ca-4520-87cc-8241d3baf7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "05dd0f3b-08cd-4d9f-9ca7-87f719e16562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1182b309-6958-4c64-8664-f90587719ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "369aea60-bfbd-4dbb-a748-a8d80b2e44ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "37084fca-514d-48cb-97e9-e98b567caeeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bc4ff50c-c32a-4bab-9ac7-e36b03538640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 441,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "44688ce7-afd8-4692-ab0e-22d0749a9e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b5447c1f-350e-4126-ae5d-7e71b4130ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a97e4453-a8b4-4eb8-8402-95cae73a946e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c309c426-99fa-45b0-9d00-2511bc6afa6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": -1,
                "shift": 41,
                "w": 43,
                "x": 30,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6b248cc1-63cc-4c06-a0ac-4512e6cf9ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 75,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "33851010-6bf2-4140-ac92-a7ea7402b53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": -1,
                "shift": 25,
                "w": 26,
                "x": 128,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "01bd6cc2-6936-43ef-b1a7-902354885d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 465,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 65,
            "y": 90
        },
        {
            "x": 97,
            "y": 122
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 34,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}