{
    "id": "8f0485b3-a83d-4d30-8c68-3236700d6764",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pauseselected",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2dcfa91f-6ffc-40eb-a7b1-ecd7595e3524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "fe7c8f12-f4a9-4ab1-9695-7e75d1d961f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 55,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 189,
                "y": 173
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "43521ff8-ec7c-42d5-bfc1-109d1641186d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 55,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 174,
                "y": 173
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1c06522b-b31a-4325-bcca-f23e9cf92b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 55,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 141,
                "y": 173
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f28a91e2-17ca-4710-99df-6f758c40e29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 113,
                "y": 173
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c509cc58-c992-4b52-9cd1-35511a8a64a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 55,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 68,
                "y": 173
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f3ea6e3d-fd78-4976-8f87-6e785044b725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 28,
                "x": 38,
                "y": 173
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7ba24f20-9966-4620-b238-f0117d4ddfd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 55,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 30,
                "y": 173
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3f81a95e-6a9a-4414-8871-6e2e9d333828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 55,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 16,
                "y": 173
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "94eaed6c-9dd8-414c-9141-ea746f13e2f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 55,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 173
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "88e3b117-c0bc-44e7-9f88-4a7d6b2ee583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 198,
                "y": 173
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "990eff49-00ba-4d04-bc2f-0d87273b425d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 483,
                "y": 116
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c7576dfc-f32d-4e43-bd71-51f06658802f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 55,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 446,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9262d99e-91d8-470c-860b-01e1e368c61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 431,
                "y": 116
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "482a7ccc-d759-4058-973e-ed1af5702654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 55,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 421,
                "y": 116
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9de82cec-a7cf-40d4-a3aa-ee419916b286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 55,
                "offset": -2,
                "shift": 15,
                "w": 21,
                "x": 398,
                "y": 116
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6eccdcd3-7df7-468f-908e-6a7f1928b4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 370,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "db1ab829-15ef-4df9-98d5-c309abd0732e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 55,
                "offset": 4,
                "shift": 28,
                "w": 14,
                "x": 354,
                "y": 116
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4aed61fe-0f33-4705-a176-697bac7e668e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 327,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "72a2b9cd-037d-45a6-83cf-3be48a38d01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 55,
                "offset": 0,
                "shift": 28,
                "w": 26,
                "x": 299,
                "y": 116
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7e43b705-ab3a-483a-86a7-f6df5762ccfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 271,
                "y": 116
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8da6045b-f88a-4478-9d5b-ac1bd02657eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 456,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fd400295-2f7b-47f7-9a1b-194167fd8e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 246,
                "y": 173
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "116d62bd-a24e-4246-891a-1d41a8a19b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 35,
                "y": 230
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "eb96abb6-d3d7-47b2-b32a-e2ef6e48ff1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 273,
                "y": 173
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "de300e43-0629-4f20-a04b-0c6d2c043483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 286,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3b5e9ed5-e013-497f-be8f-ade9edaacf55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 55,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 276,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cd37ff1e-2091-4af4-938a-2fda4cefa964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 55,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 266,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "945a5820-a78c-484e-9cb1-76b01c098195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 243,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f45051ad-41e0-41c0-999a-3f64aa77905c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 220,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d63e98ca-86ac-436a-ada5-3465778df3ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 197,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3f961424-5350-4ed1-bf8a-3df094433a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 55,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 175,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9b3cc377-40ff-4f8e-83f3-8bacc4cc39e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 1,
                "shift": 43,
                "w": 42,
                "x": 131,
                "y": 230
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "49ca21af-b5c8-4d3a-bc0e-3cfc66be82f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": -1,
                "shift": 34,
                "w": 35,
                "x": 94,
                "y": 230
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "868e8849-2fb3-488f-83bf-eab1a44f4819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 313,
                "y": 230
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2b193671-51d4-405b-9006-cf785772e59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 62,
                "y": 230
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cccaa7ce-41c8-4861-9f17-615ac3a8e831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8a105d48-dc2a-4e60-9e45-f072f58c5cc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 476,
                "y": 173
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "22ee2466-ac58-4ce4-897d-38a037e55b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 451,
                "y": 173
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9811d152-9852-4e74-801d-d7690de3c6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 1,
                "shift": 34,
                "w": 30,
                "x": 419,
                "y": 173
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a5d29298-09a9-41f5-9eaf-07e1924ad3a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 3,
                "shift": 36,
                "w": 29,
                "x": 388,
                "y": 173
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7755866f-2fd3-48fe-9a61-b1eca960efcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 379,
                "y": 173
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e8405244-f6a9-4f9e-84ce-9ef3bbfda0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": -1,
                "shift": 23,
                "w": 21,
                "x": 356,
                "y": 173
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "deb6092e-3b4d-4cc4-aa04-a955f0efaaaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 3,
                "shift": 33,
                "w": 30,
                "x": 324,
                "y": 173
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "82105484-ab6f-41b2-8887-8683c7ed8d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 301,
                "y": 173
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0d4b25ef-5c4f-44b5-b0ea-5bcde8694128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 3,
                "shift": 42,
                "w": 35,
                "x": 234,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51491d86-aecd-496e-b69e-9ef08cb425c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 3,
                "shift": 36,
                "w": 29,
                "x": 215,
                "y": 173
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c83e80ac-a3ef-4fa0-9ebf-1fab00805d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 197,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7273712f-2ca4-4984-8e4a-e2ddb3653371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 101,
                "y": 59
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "31200aa5-07d1-40b1-a4c9-6d5fb95b37f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 1,
                "shift": 37,
                "w": 37,
                "x": 32,
                "y": 59
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4b80c756-85b3-4d1a-83b7-c76982953005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3eb140f5-cf18-4f66-b9d7-d01c7c10fd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "70ce96eb-8589-42be-b717-c0b171ac3754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b5d0b72b-9598-43d6-976c-d52b57404a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 421,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "45187c1d-4948-4bd4-b78e-959b835142f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "295ff03f-63c5-4198-a724-24ad0b169fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": 0,
                "shift": 48,
                "w": 48,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a53f07d8-2f05-44b9-ae3f-635b1ba65ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6da46035-1a2c-404b-8738-eb7f2cefa5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": -1,
                "shift": 29,
                "w": 31,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3572fe72-40e2-4eee-8398-3f7d3aaa6861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 71,
                "y": 59
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7ac2b570-53a7-4a18-9dce-073df26778e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1845bb7c-8ab1-4b23-9312-f24c47faedbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 55,
                "offset": 4,
                "shift": 24,
                "w": 20,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "92fb3222-5b5d-4c0b-bfc2-5181fbd89f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 55,
                "offset": 6,
                "shift": 21,
                "w": 12,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0712f50d-61b5-4b1d-9369-ace7afef6bf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5fe01740-8795-4ed2-93a9-5ee448015d3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 55,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dad4af3e-718f-4ac7-bfb8-1ab3ec30cc53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3ef7a263-ec1e-4f69-ba8c-19c98c767ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "deddd5ef-91c8-42aa-a947-ef2c2deceb88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "696a0254-acba-48d2-bb20-11bb95cb82a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e9dd2b9e-6287-427e-af0a-3f686d08f11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "521941b5-2a15-4815-8713-59d5dd014c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bf571b96-0f2f-4d26-bca3-ef2215dbb07c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 19,
                "x": 130,
                "y": 59
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4e8ae9f8-5011-493a-855a-d457771d6dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 400,
                "y": 59
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "026b20c1-2d25-414c-8898-cd9d8ca6d5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 151,
                "y": 59
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "731dd451-1429-46ad-ba70-da12941a1ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 155,
                "y": 116
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "55f8553d-e927-4899-a960-862e67062b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -4,
                "shift": 13,
                "w": 15,
                "x": 138,
                "y": 116
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d6fd71bf-4881-4a28-aaa5-ed8a684226ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 25,
                "x": 111,
                "y": 116
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fa456f91-49e4-4e62-82e3-d703b2af1277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 102,
                "y": 116
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d73288d3-632d-48b2-ab01-cfb10d8ebbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 58,
                "y": 116
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "78a6fc5a-e301-42e0-93f2-9a666c6520b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 3,
                "shift": 30,
                "w": 25,
                "x": 31,
                "y": 116
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "96a72e28-91ab-4ae1-9c54-162377fa8f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2844065d-36ec-4b16-ba80-974a8ae6f124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 3,
                "shift": 31,
                "w": 27,
                "x": 479,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5f737fff-ba82-4856-946a-426040d5cbb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 450,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b172f961-82b2-4f8d-9a80-32ca3e7716cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 164,
                "y": 116
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1158cf65-4c6d-4a9a-b5fa-21106066b619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 427,
                "y": 59
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e4dcf51f-807b-4816-a651-5046c413a8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 381,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "11d41ad8-cf83-4cda-909d-eb101bd0fe9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 354,
                "y": 59
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "efa54666-dde4-4cf8-bc4a-15da82934442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 326,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "850a7150-c58e-4d6d-8b6a-3d00ffac43ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": -1,
                "shift": 41,
                "w": 43,
                "x": 281,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9e2df534-ec11-490c-b31b-cd96fb894477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 253,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "df1498d8-4fe9-4ef3-a781-f5ac2019a1df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": -1,
                "shift": 25,
                "w": 26,
                "x": 225,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "56868286-4aab-40c8-85b1-7da1170186ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 201,
                "y": 59
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8b09cbf1-83e3-4523-bb8b-605e7311fdd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 55,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 185,
                "y": 59
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a822b303-101f-42c2-9166-0ec5c21de9c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 178,
                "y": 59
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cac44fad-6db9-44a5-90a1-3b7a28088c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 55,
                "offset": 5,
                "shift": 18,
                "w": 14,
                "x": 181,
                "y": 116
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "98b97eb4-1d03-4f8a-af69-4aa79ef1db91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 55,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 343,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 34,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}