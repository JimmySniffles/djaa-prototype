{
    "id": "54c1d382-2446-449c-933b-f8c35d8353d4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d9b42b41-f750-4e4c-8078-6e2b1c50b7e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7969d35b-efb8-41f6-83da-3ccd6ca166e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 330,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3722e4a9-3e2f-4d8e-91a3-4e3bfddea5ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 320,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "635f49eb-9dba-4457-aa05-f4d49ca8af5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 293,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d875dffb-c63d-495f-a0e8-8ec5be15da57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 271,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "67625d71-5dba-451d-b278-1f3266a13a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 236,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8c47cdd5-f08c-462c-939a-abf5b60fde69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 212,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1d282616-0973-4041-9e94-88f0a34efff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 207,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2a52c774-8b8d-4367-8a00-71b9b10d282c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 197,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c3a41764-b855-4407-a348-d68350ea2fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 187,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "55512733-fe31-4831-a21c-4328b28c8381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 336,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "16955d50-fd40-4ce7-8539-49df3a7082b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 169,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "07530b6c-ddd5-44ce-8d27-24cf952a1b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 140,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1b8ef284-1bd2-4fc1-91a9-f76c8036209b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4515d529-6a39-43bb-b57f-dd657a20ca97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 121,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e7651bdf-fcf5-4e32-bec4-09dbd3633c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": -1,
                "shift": 12,
                "w": 15,
                "x": 104,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "58e1c5a7-3374-4bcd-9d67-42cde43e52db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 81,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "430a7a39-5bb0-41a7-a280-09bed1dc5f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 4,
                "shift": 23,
                "w": 10,
                "x": 69,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a2834278-ea19-45f8-85d9-8b27579944ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 47,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2766d207-04e2-49c3-819f-79639473b857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3c6bba84-bf1a-4c49-be4f-1d541ba8a75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0b0a98ac-ae26-4e8b-a82a-7e0896a088c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 147,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aaff0734-5ce2-45a3-9eee-fbcc9a01a0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 374,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cb1f6163-1a73-46a6-a1ba-9692c4bc9c33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 97,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7a8a03e4-a855-484e-9738-2675a78897dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 396,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "22bc2436-09dc-4017-8d5d-6d3694d86a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 296,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8500e9eb-44b8-41d7-bc83-ca652f543dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 289,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c01e88f2-6abe-4875-83e7-dcf5aeef58d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 282,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b7bdee63-fd34-440a-90ce-43a21092888b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 264,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f2f8b344-adb8-4d23-8d5e-9521fe6da7fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 246,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1c97f624-7df4-4e07-a15d-8600c06f080c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 228,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7325640e-e06f-4fad-a791-6ce6922f78c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 211,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e7a365ae-b9c2-40db-a0ca-828e0f1aa294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 174,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9258e92c-91d0-4741-99d7-706f040170ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 145,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "027352a9-85b8-44a6-aad9-bde9222e1fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "651eacc9-7e10-47a5-90b9-c85325dbcbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 119,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1598fe11-adab-47e6-8418-65fb85ff18b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "51763ffb-6adb-4518-ac73-5c4656d96ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 48,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6c6e441e-b225-4b84-bb90-c66f11f27765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 28,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e30ddfcf-60f5-481e-a534-799590cfbf16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4260dfba-9767-4ba4-b5f4-b454c73c4796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 485,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d09b57ab-6149-4485-bca7-13e06a7c38e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 479,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6388c3ef-cef8-431c-9bc9-a3b2f0f2396e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 0,
                "shift": 18,
                "w": 15,
                "x": 462,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2e493dcc-4bd8-45ef-8451-750fc9ff7456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 437,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dac49f58-f44e-4bab-89f4-053699dfd4d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 419,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3cadcef2-e16d-4254-8225-261a8ea45ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 477,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "53c71568-c6bd-41f9-af17-0246a90c9696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 349,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b4be33c9-a41b-4c6b-b9a2-254aee9a9ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 447,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "147975b0-0d82-42fa-b2c3-3e925dc6044e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ad4d6a3d-5970-4f32-b9f2-08677fc8ffd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bab7d2c8-4c58-4365-a995-98aec56c9175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c8a3bddf-67ea-45fe-9a6e-cc77d0e92fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f1032fa2-f31a-4fb3-b9d1-8d4bbc868bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "452bebb7-008d-4b92-95ad-992a1ca4f649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 335,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "904c8d19-6747-4462-821a-54580fbe5026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d640b1b6-43a1-41b5-a937-88c486c7cc11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0233d7db-6b0c-433e-926a-029e44d0e995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1277af31-df96-4806-8f08-4c15ad182448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "25f844ed-3b0d-442f-bb04-8f631c14b617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "82619055-f2bc-41fd-a8e9-ffa6e2c8cdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f6eecd07-7a5b-48d9-98b1-45c9d10c4283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "45ee4063-2ce3-4d8c-aa41-2364d3f184e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ff95ad97-c069-4b44-bad5-e32f273637bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "85a7b05b-bcb8-448d-8045-e47b2e7f9db6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c9140884-ee73-409c-b4f7-43fde934197a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f7f99f20-6319-45b7-83de-05c924278a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c333a6a3-55ab-4531-b3d3-7d1f6403a911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "af379de4-63b7-45fb-bbec-b1dab5069f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1f6399e3-26cc-4c2e-ab49-027ed7d8b4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9939141a-67db-439a-ac59-0cb8c521e22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5bc5a9c9-bef6-4d56-b8b7-8b7bcd72cce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 14,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "28a2d961-b1b8-49a8-92c7-b00ce683ab7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c27dda76-f4ae-4b11-aee3-a48addf0e3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 18,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0c9552df-01d5-49a0-b58b-368add662438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 416,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cd30e01b-6022-41c1-a7e0-ba9941aa28ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -4,
                "shift": 10,
                "w": 11,
                "x": 403,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7ab0f18e-de97-4c85-b54f-ea2327cca3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 383,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8dc74644-d920-4577-a6cb-c820da9b00ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 377,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "622bade3-1cfa-4f8b-91bf-bb7496033241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 3,
                "shift": 39,
                "w": 34,
                "x": 341,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c6caff2f-e8f7-4a84-80c3-2fb7887f8c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 320,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ef06198e-1769-47c6-9ca1-07ec5c56aa3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 297,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7023481c-8d91-47b3-906d-40a2a3565710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 274,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4ded7cc9-963f-49b9-90ff-26d0c69aa11c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 251,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3d284aee-44ba-499a-8ac8-8253fb8a65c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 422,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "35b9509a-7841-4641-bdb4-f7aaa853584e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 233,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f46251d9-3abe-4a2a-943b-6f277849eebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 196,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "fb9aa345-21a5-47b3-a4a4-f09a9bc6f463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 175,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e5d2b753-6305-45f0-b651-881991012a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5f1649cb-6023-41aa-872f-cc22f28a94b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 32,
                "w": 33,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0780b07a-9e76-4e5d-90a5-691bf5b285ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "11a76fd5-1cb4-4601-a0bc-59449ce40395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": -1,
                "shift": 19,
                "w": 20,
                "x": 76,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6d245d96-334c-4b14-a802-d104b764768c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4ca691f4-13ab-4587-9d1a-59cd96908cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7e2a6318-c89c-4b2c-9e55-0ba28b95af53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 39,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "62476711-3b05-40e6-a490-05d933ea1d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 435,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ede2bec2-2845-4793-b294-ea02c1544ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 342,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}