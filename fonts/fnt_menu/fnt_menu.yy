{
    "id": "54c1d382-2446-449c-933b-f8c35d8353d4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "21433222-7393-46d8-884d-4c202f6ea156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "936cd77d-8c9e-4a8c-9e4d-5c3eb18b5d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 330,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "45985585-3805-4537-93f5-034f88a0c6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 320,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b3c9f30c-5b2a-42f5-a769-4f07619e28f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 293,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e19c12fb-a9c4-420b-afe9-9335ee922bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 271,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "82bd63d1-3980-482a-a08d-d2f6d719b775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 236,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c694587e-5528-47fa-bfa4-c4726da57b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 212,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "10c1e5af-f19c-4576-bf83-e17679b4346c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 207,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "97738d18-b7a2-4e15-a5a8-5f3b06884c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 197,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ab2ffa5c-cf87-4e74-824f-5c769cbd8365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 187,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "dfebc590-6df9-4c16-923c-4ddf1d5bd4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 336,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a36c3635-8425-43a4-92dd-4f78590b181b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 169,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "83d1ed13-26d1-455e-ba70-8dec0ecd3951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 140,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "12007360-0442-429c-afc6-7f472405f71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "131c46e7-5b8b-4096-ac18-cec99882c9f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 121,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d146bf69-55ad-4d47-81a5-e6f3f65ae724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": -1,
                "shift": 12,
                "w": 15,
                "x": 104,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "52855c87-81f5-46e4-b43a-518e9f5a13c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 81,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a6db4d0f-f712-4697-ac36-e0d4a1588ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 4,
                "shift": 23,
                "w": 10,
                "x": 69,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f77ba6d3-3240-4a41-b412-129fa0f6d83a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 47,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a5cf2ae6-3a4f-4d2c-8728-97fa5d797316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 25,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4e5b8e67-660a-4dff-805f-e631b0fdac5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f473b370-3d1d-4351-aac2-317fb588f139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 147,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8d5de91c-bd59-4428-8fca-1c3fb118db56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 374,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bef21369-d871-494f-a21d-b828894a94fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 97,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ca4bb551-76c9-4cc2-a4fc-afe39d5a0205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 396,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3e83e944-7b64-415a-9d7d-003e87be5280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 296,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "08b6fcde-5b5c-4e7f-941f-30c16ef8a84e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 289,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c8358585-a472-41cb-9a9d-559507db1450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 282,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "26983a0e-2670-466c-a8b1-d32ea26b316f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 264,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d820fb63-844f-4f3c-b376-121a59b38093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 246,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "37996419-3400-45e2-897b-1fce3cf3fce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 228,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "43e1eed7-edf1-447d-8e17-1a00609cdcf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 211,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b2c34a75-0b57-4ae0-a46e-876d6e8f349f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 174,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "54bfa666-a284-4df1-9094-db0485ac85a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 145,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8c601b7e-86fe-4366-a133-25ff3003f35c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "68307610-d967-429a-9c6c-a0f8ca7b9201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 119,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ddb1dbcd-a16e-41f2-9a7a-2903a39de1ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6902b509-4e50-4c8f-9a95-30b3c117d156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 48,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1ff9519d-1a6f-478d-b069-023e2440eecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 28,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "22b92102-2b24-4011-b446-03e0d3138f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9e511ce0-4a66-48fa-a382-17f92027b247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 485,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1b9d709f-086e-44e1-9dcf-87cec0f80285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 479,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "29ea0be3-4222-4922-8067-70be585a74db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 0,
                "shift": 18,
                "w": 15,
                "x": 462,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b2f47a0d-0b9c-47d8-9a53-5a4479a24be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 437,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1cfd2ba7-60b5-4767-b59b-d2a6e1cb6a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 419,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5d7b7a9d-0d75-4d84-9efb-e46263082ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 477,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f56c4432-a595-4e86-9948-6b8ab71e2929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 349,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "115cc369-36e8-447f-95b1-dd1a375b3e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 447,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d1dad921-8acd-402d-ab09-2cb128f9e02b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9978ef30-c452-48a7-9252-e15a1a9f2115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd17cd9a-9046-4066-a78e-9024d15c9932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "14895d1b-2fa0-4ce1-b94e-c97b444a142b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "006a6f99-0aa0-4733-a8de-ad519b5d68c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f102c2af-d8e6-4abd-a065-c73820aba54a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 335,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dbdc867f-54d9-43be-b9d8-63865263aea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "30b9e381-44a1-468f-8564-e9695d9787fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 267,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c47b895a-32c9-4fc2-90a0-6e6f8b9d313b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "baaeae1d-6f09-4fdb-8ed9-4901f14bda21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0c2da35d-f3ff-4639-a033-2e6f26fbab87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "08e1e0e4-c089-4cdd-88d0-f9942b5f810e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cb082d0e-6ff7-4442-af64-bbd63853e73c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9f52a5d6-68bd-4568-93a1-3599dc206159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 4,
                "shift": 15,
                "w": 8,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b4f83a50-e683-40f0-a85a-1f97f1d851f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4cdb9521-71e4-4de4-ba79-cf594dc212ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1f1e0bae-d522-42a6-abe8-e2fb9422c8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a723aee1-b724-49fd-8b10-f1110d909fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e120a7ce-a837-469d-876f-f711af4dd750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f0f87fcd-7235-47ab-8bc1-df3ee8778b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3f03be50-8d20-4025-a4d2-438712be71f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9db9f48b-245c-42c3-bd3c-a31191ace4eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8062b2fe-e565-423b-81ed-e25ddd68e0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 14,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7ac2a6de-8eea-498d-9ce6-d6373747797f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "87f86131-b3cb-407d-b640-1cd0ec073348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 18,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d4836f15-8349-4d3e-9503-1cff04335ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 416,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "24b3c9b8-d01b-4246-b043-5305e1be2d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -4,
                "shift": 10,
                "w": 11,
                "x": 403,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d0496279-bde0-4b0b-8775-a0987da4f545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 383,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8751888a-b44b-4dca-bfa1-be589297a2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 377,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d1fa0020-7ace-4073-b394-08980a13a7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 3,
                "shift": 39,
                "w": 34,
                "x": 341,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a4ff4136-4871-41b3-90d9-4e79b7492ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 320,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d75d647f-d45d-4b96-b2d1-07b5c571d8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 297,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8cc78f52-aaf3-4209-90cd-6c310a2fc818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 274,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ef54e9d4-d070-40fb-af26-279b8ef74be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 251,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "82928ed1-5e47-47e7-b9f8-c91833fae95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 422,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ad5f91b3-4272-4d2e-86d0-29c5e049908b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 233,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f78aa793-a7d2-4a31-ab8d-2e5a798d0124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 196,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "888dcec8-35e5-4a93-8cfe-2c622be17740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 175,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "571a7b3f-0aa3-40d8-b236-09a38576fe0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fc948af1-1fce-4b39-bc9c-f002cbeb5fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 32,
                "w": 33,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2b8ffb9c-7d5b-467a-bf0f-0187e3a055d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "52a01095-7df3-455d-bc0a-4366ab37b043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": -1,
                "shift": 19,
                "w": 20,
                "x": 76,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8c328234-5a51-43dd-9439-49d4cd740bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cbc61c27-f4e7-4c77-ac0d-9803bb9ec786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e3665aca-3205-44d9-9991-88d7a2d50762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 39,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1d1e572a-8838-40c0-9f2f-c1654e282d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 435,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "69e150ac-17a8-4f88-a9fe-103ad48f2a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 342,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}