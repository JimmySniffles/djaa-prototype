{
    "id": "47c90029-41e5-4dca-98df-6ff81ce60a77",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "17bc22c5-c577-4faf-8791-17961a0c2578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3735bdc5-114a-4eee-8413-12e0cfc5f052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 5,
                "shift": 13,
                "w": 5,
                "x": 229,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d0bba986-eca6-4134-8947-84cb2082f629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 215,
                "y": 104
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "328c9434-e558-44fc-a17e-0add35e6ba71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 193,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bf70ea4f-3827-4263-9333-18c17faf543d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 173,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "001e777b-371a-46fd-8166-e63a045f5026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 144,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "635bdbd9-7af5-4109-9abc-1c8e2ef1df90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 116,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b80dda08-c0ab-43ae-9d2f-6f3dcb9648c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 109,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a169d57b-1972-4dad-b85e-fe82ec7b7fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 99,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "75153028-78e5-48f8-86d8-fdd841f87300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 89,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "534774b9-0b60-4124-a578-0b8f409f236d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 236,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0edb78cc-63e9-4ecb-b409-c8aab075bbd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 69,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "92695a5a-5825-471f-b3ed-0a71e3344922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9f4406d8-d191-461d-9ff6-cf8347348c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 28,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ee3091b0-5fe7-45b0-8f13-293de64350f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 20,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e24fb875-a1ba-4268-ba4d-4a3261ac0ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "060eff01-e1cf-45e9-9efb-c4b1c180b338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 486,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "44832657-e83d-4888-9ec3-4e98cff3c7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "705565a4-9e9a-4b76-8965-3b4fde1f92f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 449,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5b4e76e3-143e-4b0a-8060-0fe91864e546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 431,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "86f476f7-7487-419a-8aae-0fcdd652237c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 409,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "41dbf70b-cc4a-4588-b111-1f5fd3cfb992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 50,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d1954f47-2efb-4037-81d1-e586c53c55d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 274,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "48812c4a-adc6-4b15-8f25-41956b449087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 466,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "32294564-bd13-4776-850f-2fdaaf8694cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 293,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "72de9d9f-e73e-4453-8cd3-3d367aabc519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 154,
                "y": 155
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2eab39fe-940f-42f5-ade2-05884dd59bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 147,
                "y": 155
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "567c0a4f-a3a4-44b7-9d41-c774e719d8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 138,
                "y": 155
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "806ffd70-8619-480d-b4ed-3916ba6d710b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 119,
                "y": 155
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "36150c76-f497-4762-a272-b878314fc144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 99,
                "y": 155
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "92336054-1192-4826-9a8b-ec56562d97f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 79,
                "y": 155
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d39cd492-1aa7-4b74-8a00-e8f8db6d2b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 62,
                "y": 155
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f59bfae9-7830-4a39-8910-caab59d5edb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 0,
                "shift": 36,
                "w": 33,
                "x": 27,
                "y": 155
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6f5bdf2c-b725-42cb-a6da-ca3ea126301e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f25a64a6-32f3-4636-890f-99dd6fb87dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 174,
                "y": 155
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c1c543d1-0c8c-4ff2-a191-e8eee697b243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 486,
                "y": 104
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "04276d8c-a261-46fc-a741-0566b13afac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 444,
                "y": 104
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "543341e9-06c1-4c0a-bf91-a02a3e68f78f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 427,
                "y": 104
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ca5c0864-eaca-4a22-99a6-7ae617d4ec59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 411,
                "y": 104
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b9cd8ea9-d827-4d7f-9992-039b59cb8331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 387,
                "y": 104
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a0983916-8451-4128-90c2-b37491479261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 3,
                "shift": 25,
                "w": 19,
                "x": 366,
                "y": 104
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1b5e9ce4-33c4-4308-8e19-67a4b4063b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 360,
                "y": 104
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "db63f9dd-9921-4d2b-8db8-3b50526ef5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 348,
                "y": 104
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a07d1991-8dfa-4bf2-af97-9adb77f16535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 329,
                "y": 104
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cc3d0a86-7f85-4824-9087-11dda444e758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 313,
                "y": 104
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4bcafcc7-4068-42d7-8e49-1e35cbbffa19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 379,
                "y": 53
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c5e79bc1-aece-432a-b88e-eb80c70bee35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 252,
                "y": 104
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8b502d72-3866-4130-950a-44f0d404df86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 353,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e11f16bb-aa74-43c8-a1d3-a51261e0f008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "aa1543dd-1e87-41f6-939d-583fd25dee35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 27,
                "w": 27,
                "x": 384,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d678f713-cab0-46d0-872e-bd17a45853fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "518a18c4-bff0-471a-bf56-a360553cb0b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 346,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "63368319-f2cf-4a6d-bb67-70a4597c7b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9969791f-d2ca-4c95-b4a8-864dac4e507b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "322d13bc-8936-4424-b99f-f9e9fc480135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 277,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1ecdafdb-23ad-4d1c-9c5c-b9954ff2bf5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d827a022-5405-4018-bbb1-6767d58a7c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "25887a19-afd2-4296-833b-8898a1166ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f4561614-baed-49ab-b953-1020518c0f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5ca9be63-eb67-4632-89ae-e0ac7ba6c545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "07d3dd0b-f80e-433d-9842-3723b319070c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5a9b6f9a-cb50-454e-9699-5132a44bf784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "efaf9cf3-185d-4315-b2ec-e27a053dac2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e2ff8c93-5475-4d67-b58f-7d20db8111b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fdbd14b8-db0a-4bcd-af88-aa691fb42eba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7d58b271-3bc0-4795-aebe-3a60b4645e4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "537ade1f-ebb8-4c2c-82b9-70ed40f49b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5fdadef2-3948-4a7b-a55b-fb01cc2f2576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "69f88de2-f0b3-4b6b-acb4-65bfe2f3319a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cbdc224c-8bbd-4035-aac3-c8412fe10ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "167f0f56-14eb-43b1-b239-5d6075b57e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 450,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "96ee05ce-a7a1-4c87-b91f-8db035ca5371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 140,
                "y": 53
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3977bbb4-664b-4eb7-a52b-b6de9f850fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "65c92d8b-9308-4700-9699-ceb24765c361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 320,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "aa0c0455-4964-461f-a153-ff05664baf5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -1,
                "shift": 10,
                "w": 8,
                "x": 310,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e1b0a0d2-3a2b-448e-8351-2d7d08cacfda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 292,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d3873221-13a0-427f-8fc8-de79a190b240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 285,
                "y": 53
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d365be09-9d71-45d7-9f11-029a4c6108ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 255,
                "y": 53
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8e89167e-4883-4f08-8fa6-17a576ed8390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 236,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "97b6c017-8616-4d6e-b78b-44d8eaace3f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 215,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "94a8248a-5dba-4076-9c35-527d36b644bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 195,
                "y": 53
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a67d0757-d267-4523-8ede-eb2561f3f730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 175,
                "y": 53
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d9cc472d-ebd2-45f4-9012-1fe90fde83d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 327,
                "y": 53
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "662e3e50-c790-472b-85fc-54a6a7ff32fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 159,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d90ac028-89d9-48a9-805d-511ad22661cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 125,
                "y": 53
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4667e283-5fae-443f-9086-75331e46bf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 106,
                "y": 53
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ef79028a-6359-488f-bfad-477d8ed3ed07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 86,
                "y": 53
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "61d75f0d-9f30-453f-a88b-a27291bf05a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 57,
                "y": 53
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5ad3aba6-7d79-460c-9af7-cedb895b7300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 38,
                "y": 53
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b22c1988-f7c0-4ccb-bdd4-57bea9badd5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 18,
                "y": 53
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c52eb67e-7332-40b5-bcd4-e8e655bdb82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "97e00a98-83db-46c4-8ee4-c5305b3364a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6c5b1ffd-358b-4bea-91f1-642c22bfbe4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 7,
                "shift": 18,
                "w": 4,
                "x": 484,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f7d0626c-9c14-4fdd-8886-677ad66dd451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 341,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8bb99f9d-d0f7-4bb3-b7d8-f747aca2e479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 194,
                "y": 155
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b941f811-91b0-40f5-b9dc-e833f58b510a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "9366518c-fd3f-42c1-b29f-a04c3b023768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "7055811d-e808-4464-85e2-069da1316be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "89d084a7-c5d1-499d-9850-706ed8a016c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "8c8a08c7-6455-40f0-93ba-1df792fd0730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "d6158f03-7e24-4961-a478-a3012af1f1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "632db37d-adec-462d-b05e-785a5e217355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "a193b78f-0c70-4bd3-815e-33ea3eb05eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "73fb7c83-4d80-4386-9754-1b585a45eda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "2cc1fe95-a3ae-4320-9321-86d7d4ac6fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 217
        },
        {
            "id": "348d7c80-29a4-495f-8c57-986025b20fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 218
        },
        {
            "id": "b1cf0136-6821-44d5-840a-31f5b1a6307b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 219
        },
        {
            "id": "444a1aad-e12c-4c54-b718-64509a459f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 220
        },
        {
            "id": "5991d058-8283-48d7-bbdf-f2ca87567df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "e2eddea2-bbbc-4cef-b436-ce7ef98e2cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "b880c2ea-a257-4112-94a7-0ab979c37b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "326a61f8-dffe-49f4-adca-28100965c822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 356
        },
        {
            "id": "a76d2e67-dfde-49ae-90b7-10ec623c526c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 357
        },
        {
            "id": "ea8135c9-d197-45ad-8557-fdc588c84a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 360
        },
        {
            "id": "7fe707b3-e850-467b-97ce-b8994e73eecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 362
        },
        {
            "id": "3c13cb28-94c7-4ad7-b2c2-c4135cee624a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 364
        },
        {
            "id": "a0fae169-f14a-4b5b-a2cf-1109c04881f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 366
        },
        {
            "id": "18db42d6-7229-4d49-9ad1-e16ae3abb245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 368
        },
        {
            "id": "bdb8d3a6-1f9e-45cb-89c5-31ad5369a698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 370
        },
        {
            "id": "d9f2c456-c9b2-49ff-ac83-a85c1877bb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "53714116-43cc-48b6-911f-a40e463e0242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "8553e0b5-f439-42ae-b459-49f6ed75ee25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "9f95d9bc-c2de-4139-858a-e7d41ea07da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "2e49c3d3-25cf-4348-b30d-0d26cd756c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 431
        },
        {
            "id": "fbb19385-cafd-48d8-9df9-7ef5c477001a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 538
        },
        {
            "id": "93eccb22-161d-4ad6-a663-205d8d0b597e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 539
        },
        {
            "id": "8c044010-3c59-4b31-9b56-459dce04dde7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "d59e35a8-155e-4ec1-a951-1382197e4a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "21f7598c-c23e-48ea-a254-cb5d22b6ef48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "b0acb171-791e-4e6f-9876-479530bacbdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7908
        },
        {
            "id": "3371cf9d-519b-48d6-922c-2b3fecdab6de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7910
        },
        {
            "id": "3dbe8661-5260-42e0-9660-fca44efaf23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7912
        },
        {
            "id": "5bd022d2-d22f-47fd-8d81-46e54a4a98ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7914
        },
        {
            "id": "331c2758-75f4-4af0-a520-d64ca28213b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7916
        },
        {
            "id": "057922d3-fbc1-45cc-96e3-f45d30e34837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7918
        },
        {
            "id": "cd6f945b-ef00-4331-84ab-4c401286c1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7920
        },
        {
            "id": "cf0c5bed-a6ae-465a-9e1d-aac40a0c6bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7922
        },
        {
            "id": "d4cc008f-b860-4ae2-859c-628a5fd0c9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "13a106b6-912d-4935-bbb8-6b71b82ee871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7924
        },
        {
            "id": "310ab3e8-01ec-4edb-9e58-139bd53ae774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "b224d489-db42-473d-9389-5ede6974eaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7926
        },
        {
            "id": "bb15e8e6-4494-4298-9a1d-b0e1cb25617e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "3e23ba50-a064-4c4f-b72b-5850e4c39653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7928
        },
        {
            "id": "25f06013-0203-4e6e-ac0d-09ae9fe98915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "0697fad3-b773-406e-b24e-cfe104acf603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8216
        },
        {
            "id": "460d7e3c-0968-413a-916f-ebc0ee746a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8045232a-9722-4f48-a9e6-45faddb2cf17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8220
        },
        {
            "id": "118c84a4-bfe3-4750-ad4f-209a966ecac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "26b8167c-5967-4270-95ea-83ddd103d586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "0e880601-eac6-46f4-bdae-585b7812f5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "5690a8a3-9329-4883-84c6-5ba01c2d31b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "ce54c394-9035-4a94-859b-6b2e66981bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "12bb5a3e-b9ca-4769-a6da-2d43b345738f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "565629f7-33b6-45a2-81d3-72c1c7969d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "be881a65-1172-40db-a702-bf72bffe73f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "ca4e4211-d0c8-4513-8c6f-45c487d486a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "2a22a265-be16-4751-95a5-7c53a5a5070b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "1a9219dc-b3b7-496b-929c-52367c453a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "fa830e22-c106-4a1e-b173-5cda91396402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "07083cc2-3f4e-4070-a38f-061857eeb78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "8f047f55-46d9-4139-874d-0c053ec29e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "a719eec3-5be5-47f3-9d7c-09beb0744275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8216
        },
        {
            "id": "e28d64e6-e992-4be4-9e59-b057e5ddd3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8220
        },
        {
            "id": "c8614b21-e551-4989-9785-04acd210297b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 67,
            "second": 8217
        },
        {
            "id": "38131bd2-b818-42a9-99b4-f1c01e7b7694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 67,
            "second": 8221
        },
        {
            "id": "2f6862d5-c851-408a-a06c-9a157f204cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "99cef2f6-0733-4f7e-94a7-49a22c0f9ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "03bd4a55-efac-4af9-b52b-d4b5fe5d358d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "55db48b2-f17c-45cb-87a2-dc6278b71729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "ba4da5f8-53e9-43d2-b41a-5e69777de385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "40eafbca-8ba1-4c73-9932-73f4e8c0086d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "9a149a57-1870-4f08-9eb7-61e8745c7068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "26e3d2c1-36d0-4ad3-8872-788ec48302e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "4bef6805-53f5-4782-a5c6-a18d31a0a8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "e9412014-cc72-4242-b115-da16fb4e0d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "272be434-ed3d-4b36-8880-83a7de7bf9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "a7e4ac9b-2490-439b-98b0-6e2ea9d93101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "3268089d-50ce-4ba7-91f4-a4ae8f6211de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "3f920798-aa98-49b4-b424-f004572b206a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "fb6909d8-8dd5-422a-8f58-ce59aeb37ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "1e195afb-72cc-4a5a-88f4-44e25dfc3829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "97e94abf-78f1-4a5b-899f-f7a0bc0a212b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "9c6b99ad-629c-48e3-b8b4-89e57738f93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "2fed939a-ebe2-4899-9631-1528aafbc348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "c2f82a4b-ef56-4ef2-97c6-f4c5b4fe8579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "d8754f9f-f765-46b7-bc52-cc548ff71299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "95ac4499-16c1-4eb8-b6d4-4252e8639975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "1174c650-a3db-48e9-accc-27c89fdc178f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "eeaac422-c4c5-4ef8-b10e-9da433a3e3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "2f0c6dbc-6865-40fa-9efb-bf5e3c7a32c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "3e86790e-69fc-494f-9dd0-4f43a08bfbad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "ec5d5718-fdef-455a-b356-8679e0015c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "9d8c6d33-6d63-4202-8192-d8a33789ec73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "0f458660-6c03-4798-b0fe-e9142b6bb562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "2b5aa446-551d-4597-9900-225a9f739c41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "dd55f655-dae5-48e4-8d50-0bdf7219f71b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "30e042ce-cc3e-4526-a9bb-f109f5d39575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "4d0c5be5-d7b3-47d7-94b6-b232f97566e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "c67df090-ca09-4076-b6d7-9b1beee8e0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "e4bf0ba5-419d-4acc-a5d7-2856a27d053a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "02a13a26-15ac-4c50-9984-56b8920dd3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "4f00f03c-b001-4888-9627-da13c2bc9cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 79
        },
        {
            "id": "52fd2277-f31e-4203-af81-e75c5ebf494d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 81
        },
        {
            "id": "7516abcc-f8b6-4aff-8695-494538733c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 97
        },
        {
            "id": "228a952a-dd04-4df3-9d18-ab70139eb37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 99
        },
        {
            "id": "c36eb114-a39b-46d0-862e-42a70f2a4316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "70e909e8-325e-43f3-a313-3a2913e74d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 101
        },
        {
            "id": "935f4bac-55f3-4bf8-8d7d-0f347c4fd96d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 102
        },
        {
            "id": "7026a708-327a-49f4-a4d1-055850eddb4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "d17beed7-8b11-47f5-970a-ad5e5a34bfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "1c1adc2a-61cd-47e5-b20f-5098a863b4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "147fca28-3aa7-479f-9cec-1b1e30356fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "c69c90d6-27b6-4af7-aec4-94f91751d2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "d0d02263-f51e-44de-8a0a-4204d8e9ca20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "5ece0243-1740-4e24-8b94-f0b37085c4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 210
        },
        {
            "id": "ce8460f9-7067-4c77-83c5-fdaaa713c21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 211
        },
        {
            "id": "a0cefc8a-82b3-454b-89ad-09e0e9af766e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 212
        },
        {
            "id": "2c2cbafe-ef0d-4795-aaaa-07a4bc9cd6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 213
        },
        {
            "id": "667d89cf-ad7d-438c-a162-8dfc4445a44f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 214
        },
        {
            "id": "6afce1dc-452d-4856-94d6-04f1287381a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 224
        },
        {
            "id": "2ffd915c-f15f-4b36-afbc-66dce8a3c3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 225
        },
        {
            "id": "0d29e604-83be-4f6a-b5c7-8b4a6d1bc22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 226
        },
        {
            "id": "07c7c2fa-5fa7-45ed-add5-6015e590ff8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 227
        },
        {
            "id": "d422f5bc-8349-48f7-9e43-97b9098548b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 228
        },
        {
            "id": "0febc46d-20a5-4de5-a747-8b10b353d35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 229
        },
        {
            "id": "4496a31a-4e67-4466-8e3a-9f1f635ce6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 230
        },
        {
            "id": "f616c7f0-de56-4f04-bd98-5230816b0080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 231
        },
        {
            "id": "aead86d9-caf3-4ad2-935f-7f8f29317216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 232
        },
        {
            "id": "8a049650-b426-4a40-b9c6-a95c0046acd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 233
        },
        {
            "id": "6b6ea514-dccd-42ec-a6f0-90aadd8e2a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 234
        },
        {
            "id": "61ceb81d-94a6-447f-b83d-cf6b5a35dfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 235
        },
        {
            "id": "1c83fe73-c9cc-46bb-aaec-1d440c40cb12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 242
        },
        {
            "id": "b9578d6c-b8e0-4586-8c04-9700a45b9044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 243
        },
        {
            "id": "bcca640d-abb6-41fa-a51f-807d96c03692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 244
        },
        {
            "id": "d7a97593-55c9-421d-888c-ab806d804633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 245
        },
        {
            "id": "bb3b7fb8-4b71-407e-a398-5478b2742055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 246
        },
        {
            "id": "40f2f200-29de-4286-8a6e-daf63a30824d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 248
        },
        {
            "id": "e2241894-13ee-48c6-9808-9e4b3fae7868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 253
        },
        {
            "id": "cb0f994b-080b-496a-91cb-d066b92a7ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 255
        },
        {
            "id": "b36183c9-2604-4339-a9c0-7eb2be202245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 257
        },
        {
            "id": "85c62dd7-fd7b-4827-b4c2-5ad6c038ad6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 259
        },
        {
            "id": "e019618f-6fda-4ddd-9b29-9ff46671c022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 263
        },
        {
            "id": "b56c5385-dfe4-4dae-893e-3fb7789908d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 265
        },
        {
            "id": "543ac5a8-667b-4f55-b441-cc79ed2f01cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 267
        },
        {
            "id": "2200a41f-605e-43bc-875e-7a46876ab701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 269
        },
        {
            "id": "c58c93c5-5b98-40f3-bd84-e3e89d13bcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 275
        },
        {
            "id": "4bc44bd2-bcc6-4d2e-856a-fbf1330bbbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 277
        },
        {
            "id": "81f4e195-ccc7-4deb-b4bd-93fe9a5e87cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 279
        },
        {
            "id": "9b9c5908-612f-45b5-8acf-7da56aebf830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 281
        },
        {
            "id": "036c30f7-dce4-475a-81c9-f315930fafba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 283
        },
        {
            "id": "ebd0c610-204b-44f0-a4ff-a61785ed8504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 332
        },
        {
            "id": "5b1b5de3-4012-4d0a-8bd1-1ec0feb8fa3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 333
        },
        {
            "id": "a55c913c-b0e8-4342-9a60-1aa99d775882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 334
        },
        {
            "id": "8bee07a1-6440-413b-9285-e7af342b119f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 335
        },
        {
            "id": "86919cfc-0710-448d-a63d-445bc3c23b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 336
        },
        {
            "id": "e65afc78-3a9e-44aa-845a-62654e400a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 337
        },
        {
            "id": "8b1bae95-a8b9-4209-b876-7158e925067c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 338
        },
        {
            "id": "4c5e3cac-00e7-4e58-b3d2-5027c05f145a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 339
        },
        {
            "id": "822b5d8c-a153-4302-8347-c7d70e246790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 373
        },
        {
            "id": "424400b6-14b3-4c37-83b7-c2b6bb5594be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 375
        },
        {
            "id": "98920466-b1df-4563-8978-f9cb376ac98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 416
        },
        {
            "id": "e69cb642-1ad7-4999-9b8a-ddc7adce5d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 417
        },
        {
            "id": "8d5a24ee-19bb-45cf-893e-2c55f5871f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 507
        },
        {
            "id": "8047b4af-4d2e-4bc8-ab47-1b63a3798582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 511
        },
        {
            "id": "618cfc5e-3ba3-4028-8ce1-f7741f22d025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7809
        },
        {
            "id": "f0b22566-e485-4c2c-9e82-77eb83ed55a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7811
        },
        {
            "id": "1d363793-d39d-4341-b950-180ac9d2458c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7813
        },
        {
            "id": "bb457b61-df0e-4778-8bf6-539f0e9a67a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7841
        },
        {
            "id": "1454b853-0b0d-42e5-8f73-e82c022095ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7843
        },
        {
            "id": "1144c2dc-e0b3-4fd0-bbfd-7b9d90311faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7845
        },
        {
            "id": "4df13d70-75e7-4d46-a1f9-5bff988ce7ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7847
        },
        {
            "id": "e7495c7d-5907-4f0c-8399-bf88f66e7821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7849
        },
        {
            "id": "6ffca1b0-b81d-4a77-bc29-c2b54d60c366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7851
        },
        {
            "id": "4682389f-3267-488b-8579-324dcebd2a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7853
        },
        {
            "id": "4003e22c-a741-40d0-bdfb-74910fee49ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7855
        },
        {
            "id": "3a352187-2a4a-410f-9bd7-f08fefa84a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7857
        },
        {
            "id": "95a425f4-612c-4e75-afa2-d762c64aeab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7859
        },
        {
            "id": "9dec7206-8602-414a-9d7d-e2bbdfe8afaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7861
        },
        {
            "id": "088fdab1-b910-4aa3-b719-6ec60672d48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7863
        },
        {
            "id": "38d58e7f-b0af-46cc-9024-38abc05fd2ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7865
        },
        {
            "id": "06c2d16b-4512-48f6-961a-3cd2606428aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7867
        },
        {
            "id": "fd187576-293c-4083-9b46-dfeec70b8c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7869
        },
        {
            "id": "a70df589-0af4-4e06-8212-0a8cb8d096ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7871
        },
        {
            "id": "ce1c5444-c301-4097-870f-3f9c4e50952f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7873
        },
        {
            "id": "32ec51ad-fa1a-4a18-b53c-d327d41f5551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7875
        },
        {
            "id": "2fe10fa4-7bdf-4b5a-b3c7-ddd086b3a604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7877
        },
        {
            "id": "85aab167-0cf4-4864-adb9-966aa847f487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7879
        },
        {
            "id": "e0bdc3c9-2298-4c73-b529-a6a3253e9219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7884
        },
        {
            "id": "6f66c31f-51b3-45ac-93f6-1b21eda2cdca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7885
        },
        {
            "id": "87f97237-74a2-4660-80fc-fdf7a58d61da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7886
        },
        {
            "id": "a6ec3c10-56c8-484c-8cdc-b6df3b4a2e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7887
        },
        {
            "id": "44d86c43-d1af-450d-a547-618997c2e1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7888
        },
        {
            "id": "77a442f1-b38a-487f-8cbc-f8267e011d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7889
        },
        {
            "id": "a7268544-573a-4478-baa9-fa48a359feae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7890
        },
        {
            "id": "c4e6bad6-e81a-4363-8193-286fff5f0ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7891
        },
        {
            "id": "c2c570e5-f547-4f0e-8e70-fa5f69ff7c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7892
        },
        {
            "id": "d3ef5407-447d-4e73-8122-a6be4004a988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7893
        },
        {
            "id": "63a7d6f2-8342-4ff0-9c38-453cc1cccc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7894
        },
        {
            "id": "a00efc80-e9ca-4f1f-804f-5d44a4ad27ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7895
        },
        {
            "id": "52613d8e-f987-4e09-9d70-1722e9a093d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7896
        },
        {
            "id": "f9ad9c23-d407-4e94-8232-6bd4f5fe3697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7897
        },
        {
            "id": "9b3d5329-39b7-4144-86fd-28fdfce4cf5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7898
        },
        {
            "id": "730f2bda-b50c-4fea-8881-040d4b9fa237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7899
        },
        {
            "id": "a71bb863-47fd-4bca-89c2-5272587b1f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7900
        },
        {
            "id": "4b17e546-2db6-499c-a593-659959c6e399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7901
        },
        {
            "id": "eb1d0b87-908a-40ba-9b47-fe58612c4e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7902
        },
        {
            "id": "5df85228-560c-4033-b8c6-399bd9a44dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7903
        },
        {
            "id": "374c4ec2-f119-4805-990e-4f1939840c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7904
        },
        {
            "id": "edd0defe-8bf3-413f-9a48-3f24016f7ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7905
        },
        {
            "id": "389fb31e-def1-4668-9723-19596ed6102b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7906
        },
        {
            "id": "12d4302f-482f-4e19-b978-d73bb50c89e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7907
        },
        {
            "id": "71baaa21-e528-49f5-9c72-78e7e363d407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7923
        },
        {
            "id": "d78bc725-895e-4abf-bbc3-9da9bada379c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7925
        },
        {
            "id": "35e97ad4-a7f0-4cf1-95ff-a4884c1efc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7927
        },
        {
            "id": "7d2972a4-272a-4ddd-af0a-e2956fd760de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7929
        },
        {
            "id": "08991291-1e37-48c9-95fe-c1a7a4d720e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "7a1f44dd-b9a6-4188-aad6-e340fc5bc810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64256
        },
        {
            "id": "99e87b1d-f35a-4c03-b8f2-dba066e096c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64257
        },
        {
            "id": "1ac4ab81-2c75-4875-ba3b-aa8547200997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64258
        },
        {
            "id": "f7358e8e-056e-4442-9b3b-18b79ff7640e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64259
        },
        {
            "id": "b6662e60-4445-4c96-bf4e-76564dc52d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64260
        },
        {
            "id": "bb26662e-5e5e-4e45-9fd1-e0ce9837a07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "e81197ca-d927-4038-a15f-691ad6e26c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "8630e49f-7e3f-499b-9870-abec9bc60691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "126a0407-1de3-4e6d-b756-a7ebe21c554e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "efd03e53-d21e-4fc0-97c3-b073ade37c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 74
        },
        {
            "id": "17d4d9d4-02b6-47c3-96cb-f24ca3c7279a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "79c6c48c-fc32-4787-a633-cd3de90c0a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "7b02b6c2-b4a8-414e-821f-ee1971984382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "a1c03e0a-57ba-479c-ab48-98893da440a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "4167baa5-8101-4b55-9f35-f1b4eb24798a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "25b94400-28b6-47d2-a078-e54fb7bd1ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "58dede37-bc80-4c60-97fe-ccd26a2e4ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "9453e350-9a68-468a-9f1c-5243bc0d571f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "be898816-7d3a-4038-a846-0a0f2555798c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "243c1d2d-cb04-4f74-9db0-b0e707150247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "c37624da-0e43-4601-b68e-139a901c38a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "7e40aed5-bcb7-4b81-baec-ad7088b23359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "8ec15ffb-24f1-4704-a6a3-43a318fd5a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 198
        },
        {
            "id": "5825cf7f-61e2-4ab5-838c-e2d7b3ba7c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "b533a6c2-c11d-4a1f-b18b-18aa9dcef368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "94351560-1320-4c2f-a4da-91c1a63433d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "b8395c88-9922-42d1-a300-a2190642e9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "6ebb91dc-073a-4266-a39b-8ab22604735c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "46894def-af2a-4792-8f32-b690d98d9d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "4149b079-c667-4dcd-bd79-108f86b9078d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "71ab4bcd-695e-455f-9e2e-c6da58011766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 231
        },
        {
            "id": "b7551f70-7776-4d7d-b0ef-71e0cdd1ec12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 232
        },
        {
            "id": "13041fa0-81c5-41a5-b86e-ab771dba389d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 233
        },
        {
            "id": "05d54086-c49b-4abf-adf9-ab74299f6ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 234
        },
        {
            "id": "647fa20f-b2f3-4745-a39f-7d4563a3b708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 235
        },
        {
            "id": "c5c0f689-87c7-4fc0-b815-0062578284c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 242
        },
        {
            "id": "8a9d1e6f-d4d9-4d7e-a6da-906f3aca219b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 243
        },
        {
            "id": "0c220a4c-e957-4967-ac9e-c5dfc9ac037b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 244
        },
        {
            "id": "0e7a5909-cf53-4d73-a838-ae824e550578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 245
        },
        {
            "id": "6bd7d2c7-3d32-44fc-844c-84ea99356792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 246
        },
        {
            "id": "461ee757-1591-4d6e-adb9-b5ff9f764c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 248
        },
        {
            "id": "b0012a3f-3a33-431b-91b9-b6af6c41aba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "b6b4f65e-aeea-4e47-91fe-cddf7bc8c91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "a2d36835-b293-47d5-9d31-efddd9352328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "b1ddf238-15e3-4135-9d21-c64f8f4ac495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "f688ed15-24da-4ae7-b3b9-1c87a33a504a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "01e0204b-18ab-4a14-8388-364355e75c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 263
        },
        {
            "id": "24968f61-7043-4d12-acd7-bc04170ba659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 265
        },
        {
            "id": "166d4c08-77f3-494e-9f09-ab036b1f8f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 267
        },
        {
            "id": "1b1bf591-c60b-493f-bca6-043e66d8046f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 269
        },
        {
            "id": "43708409-8b4e-4540-a5a0-f518aedfca97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 275
        },
        {
            "id": "ad57ae49-f4bb-45e5-bbd4-f6cc273869f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 277
        },
        {
            "id": "10e42a62-9e5a-41b9-9437-3c5120ff64b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 279
        },
        {
            "id": "3cc63ff0-3226-48c0-90b5-e3a4057af362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 281
        },
        {
            "id": "6d916253-6c80-45de-9991-c7771b74f9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 283
        },
        {
            "id": "2f479cc7-4eec-451e-8f44-4ec8e179246d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 297
        },
        {
            "id": "843b57ee-045d-4754-a2d0-406241f2935e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "6e74d898-ea67-4238-86c7-048bf2ea0e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 308
        },
        {
            "id": "71b90d85-4d5c-4a3a-a892-dd486ef01083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 333
        },
        {
            "id": "41276665-e3e3-40f4-a5c9-5830e54fffdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 335
        },
        {
            "id": "72eae200-a014-40c8-b101-a206178f5a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 337
        },
        {
            "id": "1e5dc255-01ac-4c18-a0d5-df547616696a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 339
        },
        {
            "id": "b5bc62fe-a845-47e8-905c-168b50d05f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 346
        },
        {
            "id": "c9e3ddb2-b1e3-4cae-badb-707f79cc9bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 347
        },
        {
            "id": "15b758ff-97d3-4d3c-a66d-c3582dfa22b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 348
        },
        {
            "id": "2b6f727f-9f99-4a68-b6b9-a24b56487261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 349
        },
        {
            "id": "9276b3dd-7cf1-4690-bfb8-b3787a447c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 350
        },
        {
            "id": "4fcb9529-f657-45c5-a832-75e0568b5ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 351
        },
        {
            "id": "46a8bdc3-7cff-402f-bcf2-df9d3501226a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 352
        },
        {
            "id": "a2c147ac-4974-4dda-b58e-49b56f3bb476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 353
        },
        {
            "id": "e8419566-50de-462d-8d3a-1a3aea239114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 417
        },
        {
            "id": "2354bf45-4d76-47e9-8f7a-02455c6acbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 506
        },
        {
            "id": "58cca210-abb6-4886-8f85-561cf56a0a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 507
        },
        {
            "id": "b9ca76fc-201f-4a28-888e-308ef109c7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 511
        },
        {
            "id": "4fa0451a-bc8f-43b4-8fea-11eb7e5569e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 536
        },
        {
            "id": "063bd7e0-a53d-4a45-9710-2bb15537a5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 537
        },
        {
            "id": "c84ef89a-a577-4493-bc0c-e0928bc5bbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7840
        },
        {
            "id": "4aa6153a-ba91-4e34-8b34-b278f622aa41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7841
        },
        {
            "id": "581fddfe-3e42-452b-9319-4e6792403cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7842
        },
        {
            "id": "19bb8ca9-0d61-40d8-b704-aea698a82f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7843
        },
        {
            "id": "2bd0d8b1-f329-4c71-a665-a357cda44eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7844
        },
        {
            "id": "bc7b2e7a-d3ec-489a-9a91-798e25f8edc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7845
        },
        {
            "id": "034a98c8-c1cc-45b6-b7e8-de3160a015bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7846
        },
        {
            "id": "1dc04834-b7fa-451c-ab19-c9b4d21be298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7847
        },
        {
            "id": "2871763b-a359-4b90-befe-0fa370a95bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7848
        },
        {
            "id": "9a0d16c1-758c-428e-95b9-8333861a5a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7849
        },
        {
            "id": "8f667678-8138-4e74-9cbd-2765cd348315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7850
        },
        {
            "id": "9f0c3648-1ec7-497f-b6f2-eccd0d1b57dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7851
        },
        {
            "id": "460cdd11-806e-4fef-b29d-cf044b44bed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7852
        },
        {
            "id": "43861061-e76c-4dbb-b2c6-3b5131b6fe8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7853
        },
        {
            "id": "95da0f76-bf19-4aba-a0df-c5c47e38c917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7854
        },
        {
            "id": "4703adba-9826-41f9-ac17-be7b426628ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7855
        },
        {
            "id": "7d1eb5a0-3e92-4271-a701-31d49f4c7c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7856
        },
        {
            "id": "aac8be90-74be-4b5f-be63-78bd076667e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7857
        },
        {
            "id": "4ff41d0b-0d34-439a-b6b2-d73a67d9e02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7858
        },
        {
            "id": "69643a5d-4218-4de6-b8d6-472f82115bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7859
        },
        {
            "id": "ea61deda-485d-4ba2-b1f7-b125d488869d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7860
        },
        {
            "id": "b24e6fa9-601c-4f3f-a547-0f4529a550c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7861
        },
        {
            "id": "8a6c408d-ae5a-426b-9df7-132a8e8076d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7862
        },
        {
            "id": "bc2df74e-00fb-4f0a-9190-157e68e1fff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7863
        },
        {
            "id": "76c93004-a064-407b-8212-75f376df9d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7865
        },
        {
            "id": "da0f35f3-0ede-43ef-b42f-816696324902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7867
        },
        {
            "id": "e0985484-9233-4d27-a7bd-85b03732fdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7869
        },
        {
            "id": "60e56225-a601-4527-95d8-c45bc89ab8f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7871
        },
        {
            "id": "224ddf40-6eb2-45fc-9aff-326a9ad2e1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7873
        },
        {
            "id": "d9ac73f3-d6d4-40ee-984c-fb8adecc0e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7875
        },
        {
            "id": "371a78da-fe3d-4081-9af1-9ff0d3e01068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7877
        },
        {
            "id": "86f929f1-150d-45d4-b70f-9427040ecd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7879
        },
        {
            "id": "3683e99b-4cdb-42e2-92f3-45991b5ba0d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7885
        },
        {
            "id": "06d6507d-dc24-4748-a14f-396837dc6e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7887
        },
        {
            "id": "1082525f-8f5f-445d-a32e-31c6f6d996d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7889
        },
        {
            "id": "50568dfb-4d6c-4629-a27f-5633a9fe78bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7891
        },
        {
            "id": "bb2af1ae-e254-4cc7-ad78-e5188588b223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7893
        },
        {
            "id": "5df79cc1-d5da-4da8-a9af-38883a71174e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7895
        },
        {
            "id": "61deda51-131b-43c3-b8df-3df20ebaaf89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7897
        },
        {
            "id": "86fdeacf-6724-43f7-9ff2-532bf8565925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7899
        },
        {
            "id": "927e07b8-c523-4fc8-bfe6-fdaa132d399e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7901
        },
        {
            "id": "05cd7cc2-b275-4365-8579-ab59a31d16f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7903
        },
        {
            "id": "dbb5847a-f160-406f-9d8e-ebd8991ff9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7905
        },
        {
            "id": "4203272a-ba75-446c-a55d-8cac6b1003c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7907
        },
        {
            "id": "ae0b2fd6-014c-4ea7-8c35-c537e7c2718f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "534c69a6-acfc-4eb7-a50c-a40bb5140633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "3fd24575-885f-499a-937d-01e2e69a0cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "5fa5b64d-9e74-49b5-8f54-e87d971cc4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 221
        },
        {
            "id": "29f3b852-9a65-4b7f-9e6b-bf1652de72e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 253
        },
        {
            "id": "1943e56f-2eec-4cab-8036-8d946c989d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 255
        },
        {
            "id": "50fc16e2-c7bd-411a-9d83-db709a2453d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 374
        },
        {
            "id": "c6edb15e-3c5d-4e3e-9a22-feda2cd3deb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 375
        },
        {
            "id": "09900f4d-bedb-4814-be22-e1eae1a32533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 376
        },
        {
            "id": "4fc67a25-3c1e-4799-9fe2-d43589eabd22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7922
        },
        {
            "id": "0c9a7734-83f1-4707-a530-121c2a49ee82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7923
        },
        {
            "id": "7cc079eb-3cb2-4e30-ba3b-61fe482fe233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7924
        },
        {
            "id": "b6398d49-720e-4ad5-87a7-cc093ecaebb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7925
        },
        {
            "id": "1ce221b4-2f06-4ef5-8c15-88a09c43a89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7926
        },
        {
            "id": "b15a417a-2f72-43b9-8fba-18f0a2f74cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7927
        },
        {
            "id": "6d5a0ab7-7687-4c12-a4a7-b9298aa108a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7928
        },
        {
            "id": "aa8f50c5-e298-4e98-859f-cfc97106dff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7929
        },
        {
            "id": "41bb20a0-ee38-4898-a541-8318c9b432ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "b41647bd-c517-4acc-910c-0e3e79eaaa5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "b75ac37b-2bc8-4d7d-9137-4e46abe2cd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "7e47be14-e18a-4657-a72a-c8a452d2d1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "34270e2a-b490-4176-a02f-fee8a9f2d2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "bada55d5-997e-416a-b2a6-3f3ea34eaf2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "469de136-189e-4f78-a345-76a0e7b127ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "e577b1ff-da49-42f5-bb32-b02881da82f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "28c6d22c-77ef-495d-b8e6-a9612a339ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 256
        },
        {
            "id": "4b7ce365-88ea-4c38-ab13-93808bdbb5ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 258
        },
        {
            "id": "9ecd283e-0840-4948-9a88-9ed0ed053110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 260
        },
        {
            "id": "9ab6968e-bbc6-4755-a28e-bc38850bc1b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 506
        },
        {
            "id": "864ac14d-7d13-49ce-ad5d-8f5ba394b1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7840
        },
        {
            "id": "551482e5-2611-4dfc-9b14-9505eb3b35c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7842
        },
        {
            "id": "e6670385-4772-43f4-aad0-70319382cfdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7844
        },
        {
            "id": "031f0fe7-63d4-4af7-8f02-3fab0bc6e1f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7846
        },
        {
            "id": "523f6103-9e21-4e9d-84b7-11fef4004524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7848
        },
        {
            "id": "434f00d3-ba31-4076-826a-5be9092af862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7850
        },
        {
            "id": "a632a239-ba31-4e1e-a6ba-13bfed77b9f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7852
        },
        {
            "id": "d9b52bac-a612-40f0-8d2b-c9ed314e6edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7854
        },
        {
            "id": "a169bc50-fc1c-400f-9ad3-841f882767a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7856
        },
        {
            "id": "6c59a985-7f2a-4679-9784-8e307dfec181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7858
        },
        {
            "id": "798caa73-e880-4eb0-afbf-6d62af2b7bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7860
        },
        {
            "id": "42063642-9c30-4b0f-8923-37df5f436738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7862
        },
        {
            "id": "a6d712a1-9cad-43c8-9ca6-d24f54cadde9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "987bafbf-b19f-435f-bd90-686eb4258cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "a0b047ca-a7dd-45e3-8c69-e6afebd9db20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "4d13529a-f227-4967-8d90-a9a566934041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "373f8e3e-8933-445b-8989-49e55aa52d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "68d3a458-6002-4ebf-aae4-f99d04b7bc09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "3f61383e-6b9f-4e34-b774-0ecb81638038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "c15b736e-a453-49dd-b26f-7a2f259e16bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 97
        },
        {
            "id": "ca989872-5276-48ca-85e9-3a43e4d49423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "7c4bc1b0-37b0-4877-8cab-6e407d20861c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "c69e6166-d3bc-4e8b-ae71-9c7b79737d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "ba0f8699-3f82-4189-ae23-5f468caca7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 109
        },
        {
            "id": "d1936fe5-95cb-4f43-8e95-13b71bc41589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "ee644b36-b257-4c93-92e0-af7a1f954145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "d2b6b71e-7724-4f48-b76a-1c25dd2a922e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 112
        },
        {
            "id": "4fdba970-e047-4704-b466-28f95aad937e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "3b2daf85-2b47-4e00-908f-8b46695f0e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 114
        },
        {
            "id": "2d5c0711-94f8-468c-b7ed-2d4813311384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "2a6982ed-191b-4872-98e7-7f2a9e94a118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "2a06008a-535a-4d0d-b6b8-a84c6ea742c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 118
        },
        {
            "id": "d83f980a-280f-40e5-8843-cdef524700ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 119
        },
        {
            "id": "0c03542c-5ba8-41ce-8ba3-d21505ad8045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 121
        },
        {
            "id": "7e50102b-4587-4ca9-9f0d-6a8650f00ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "d5ca2279-280f-495d-b892-b0726690d2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 199
        },
        {
            "id": "7652f26d-aae0-4910-9995-07562eb98d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 210
        },
        {
            "id": "23cb2319-c056-4d8c-8a21-70d732ce6cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 211
        },
        {
            "id": "94233ac5-3056-4409-92d5-65fac9e7292f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 212
        },
        {
            "id": "b1d8ab0d-bb23-4216-99eb-43bf999ef75e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 213
        },
        {
            "id": "f86822a4-3aef-47ce-bd7e-be981891e85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 214
        },
        {
            "id": "5309bd38-78cc-40ff-813a-c0a76a0c0cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 217
        },
        {
            "id": "a378305c-04cc-4c45-8aea-94b0e7d4ea87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 218
        },
        {
            "id": "947958d1-443f-4999-be22-9ed33793e379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 219
        },
        {
            "id": "e71067ec-cee3-4146-b08a-f11031f06ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 220
        },
        {
            "id": "23ff7220-3821-4d60-90a8-3665a8c09b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 224
        },
        {
            "id": "472130a9-47ee-4075-bf0d-1b447cd699b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 225
        },
        {
            "id": "a94eb717-191e-420d-af34-405e7d411e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 226
        },
        {
            "id": "3a1b2a03-d679-4d51-9312-b1e91b5b9f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 227
        },
        {
            "id": "5d785c25-c9f2-4bc2-afe4-64d0aa65c95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 228
        },
        {
            "id": "f7f9b027-cbf7-4890-a7e4-a07554b9165d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 229
        },
        {
            "id": "db0a105c-c4bb-490a-9d2b-540267673052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 230
        },
        {
            "id": "5922f4ee-93d7-4af1-9977-98824b4c994b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 231
        },
        {
            "id": "a577f3d0-c3c9-461f-9902-8ed01aef0b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 232
        },
        {
            "id": "6c336435-9d4b-4c44-9e03-fe35b75cfc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 233
        },
        {
            "id": "3080ec26-afe5-46c1-9975-620a4320759c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 234
        },
        {
            "id": "b3ec9661-2e6e-4d14-b592-e3cf7b361f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 235
        },
        {
            "id": "602b8bac-e42c-4694-9dca-4192e8130499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 236
        },
        {
            "id": "fc8ac139-52ca-4c03-8548-9c47135eb69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 238
        },
        {
            "id": "e01c582b-349f-48d5-8f9d-32ba14a4dc16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "7b79322b-bbc6-467e-937d-7af1b11f8d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 241
        },
        {
            "id": "877b144a-29fa-4d2b-b8ce-6742ef97f761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "fbdfe272-e4d0-48fe-a86a-5d36516d8a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "4702da1a-05ec-4cea-a6f5-c25fa60c1c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "10f6c25a-d670-442f-bf69-02e4fe298eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "953cf7d7-e7a2-49fe-ab1a-d70da7f016ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "e4439dd9-849b-4710-b29d-590d90d73ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "34cb3e6f-c522-4014-8e46-ee1e1470dc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 249
        },
        {
            "id": "bb2d6495-a033-41e5-926f-04b3d4479549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 250
        },
        {
            "id": "f88232df-c8d4-4d0b-b5fe-dd74e617a31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 251
        },
        {
            "id": "d423302f-3f9d-4b1d-be11-8cb4e4abb34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 252
        },
        {
            "id": "2f75ef00-7151-4b4f-81d1-cb348acc1c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 253
        },
        {
            "id": "6bc00c65-1559-4cfe-bc78-1528e6f7bacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 255
        },
        {
            "id": "56dc73cd-0fcc-441a-b81d-8cd9c0fbab92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 257
        },
        {
            "id": "8b2ecb8a-8d90-473c-860f-bc786e97ba1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 259
        },
        {
            "id": "cf91f6f1-50de-4f5c-bcdd-c301463f21dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 262
        },
        {
            "id": "a33a74a2-9ebb-4152-9635-d46985d8c3e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 263
        },
        {
            "id": "937c0848-dc1d-4a17-903a-31cdc997ccc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 264
        },
        {
            "id": "e6778cde-7f6a-457f-a634-7d8b561a1461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 265
        },
        {
            "id": "9554f00a-faf1-4c2a-bae1-46bd0600c111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 266
        },
        {
            "id": "5664a32e-b31c-4775-818a-4102fde2698e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 267
        },
        {
            "id": "4d603a11-2d2f-4c93-a669-601e2d112d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 268
        },
        {
            "id": "afeac11a-760d-4941-99ee-46c1d529fc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 269
        },
        {
            "id": "fe524194-fd51-407e-898c-eb5b6c4d5a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 275
        },
        {
            "id": "67e355e2-7c76-4d75-89bb-d570c81159a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 277
        },
        {
            "id": "0610534b-6fef-453e-aaae-2e60d0c3abe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 279
        },
        {
            "id": "5d8f871a-5c60-42ba-86ee-51a10275c42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 281
        },
        {
            "id": "a9e50946-7801-4162-902e-435dde61029d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 283
        },
        {
            "id": "27055c74-f120-432b-8c6a-e01681367e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 284
        },
        {
            "id": "af2fac38-8295-4ad0-bbb9-e07c67b16fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 286
        },
        {
            "id": "4ea98d0a-82fc-4812-b37f-11b205a67c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 288
        },
        {
            "id": "f5d1f7a2-51fd-4b85-aa38-cabb3545131e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 290
        },
        {
            "id": "81d0cafc-ebc0-4cf3-ada5-81a90bfa43ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 75,
            "second": 297
        },
        {
            "id": "3ab19269-7f9c-44a8-a0ff-e3dcca0d4079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 299
        },
        {
            "id": "2334ef75-bbef-4bd9-a276-ad94d94a7d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 301
        },
        {
            "id": "21d54d52-fc0f-4623-9d32-44a741b6ead8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 305
        },
        {
            "id": "fef5c54c-30a8-48a1-8543-f3186478295d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 309
        },
        {
            "id": "82f65f2f-28a7-4416-b6f7-d7cbaae2ad5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 324
        },
        {
            "id": "461aa17f-f87f-4c53-970d-21073a6052e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 326
        },
        {
            "id": "34c52bc1-47eb-4610-9866-373396516981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 328
        },
        {
            "id": "e8e9f3dd-6778-4d47-a118-adef04ae77c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 331
        },
        {
            "id": "1aed0a00-6362-4c5d-91e0-e64a965a96bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 332
        },
        {
            "id": "7a7675fd-9492-49eb-ac10-57755c50e632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "6b2a4b1d-3a19-4305-bd23-53b6bb7589c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 334
        },
        {
            "id": "5f6299bb-2a62-479a-a8ae-88f3b5b7420a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "7ffecfcb-a8cc-4da4-9b3e-1a1e375af30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 336
        },
        {
            "id": "3eb95171-497d-4a3d-a1b1-39b142b4f553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "7778d077-3c3e-44c5-981f-72cf5f69cf28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 338
        },
        {
            "id": "1d78ab5a-f2ac-4ed4-92c2-b908efa2f351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "b25455a3-c0c9-4bff-ae5e-4fae43419347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 341
        },
        {
            "id": "5cb977c4-0a77-4b52-94e6-1b594d9af7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 343
        },
        {
            "id": "a9026e0b-893a-4e70-ac1d-1d408e566fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 345
        },
        {
            "id": "864dd3d5-980a-42ec-97cf-a225122167a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "3940eda9-9f7f-42d2-8c4f-0938f15641ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 360
        },
        {
            "id": "15f51762-fb30-44f2-87f1-637cccfb66b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 361
        },
        {
            "id": "a590d688-71cc-492f-9833-f8b59f325c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 362
        },
        {
            "id": "9175756c-8555-40e4-ae4c-f6e158a400d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 363
        },
        {
            "id": "d62633f7-663a-42dd-be14-0226b3ef55f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 364
        },
        {
            "id": "824766f9-7e56-4879-ba02-d35ba3701b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 365
        },
        {
            "id": "b831f65b-89ed-4db2-87a9-af95801c848e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 366
        },
        {
            "id": "e3db9adc-b61d-409c-8aeb-a39e8da48251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 367
        },
        {
            "id": "35bbc273-4f99-4a27-84fb-d7e0d3e15f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 368
        },
        {
            "id": "66946609-b73a-4412-9014-a15fdade4f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 369
        },
        {
            "id": "e769369a-f712-471d-8fcc-32647b6f3eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 370
        },
        {
            "id": "b6ef55f8-0531-45c3-99e9-6637686fc6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 371
        },
        {
            "id": "d49b1090-0b38-4c7b-ab66-f587424880c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 372
        },
        {
            "id": "e9f06dff-6b5c-4f18-9bd9-7ccc3accdc34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 373
        },
        {
            "id": "76909623-ac93-4dba-ad85-88141fcfc1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 375
        },
        {
            "id": "ff7d79e7-b46b-4a42-9c5c-3da749224147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 416
        },
        {
            "id": "cde5e818-532c-45bc-91d6-746c9c35df15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 417
        },
        {
            "id": "0cd3b1ce-202b-49b9-aef4-5ad6f0d6c676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 431
        },
        {
            "id": "e09d5ec5-f993-4c4f-a871-1c0490afd15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 432
        },
        {
            "id": "bf653324-adbb-4e65-b3fd-1db9e8256d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 507
        },
        {
            "id": "4271a993-6162-4f61-b11d-fc4cf195f337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 511
        },
        {
            "id": "1a4d737e-a6ce-4309-8a6b-6863a4b2b1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 539
        },
        {
            "id": "8e4a416a-4b34-4f9d-9cd1-c64eeffb06c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7808
        },
        {
            "id": "5130a045-8610-4f9d-b545-d70e32f622eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7809
        },
        {
            "id": "888e35a7-e784-4df1-8b03-a3f936852d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7810
        },
        {
            "id": "3ef6a09f-d401-4b6b-a2a1-e506e9587657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7811
        },
        {
            "id": "83996989-276a-4992-b0c6-3c5144f16fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7812
        },
        {
            "id": "a6fd836e-465f-429d-ae60-ed0cd498c320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7813
        },
        {
            "id": "e34606e5-ff14-4ded-9458-b20ee03a6ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7841
        },
        {
            "id": "8fee40fa-2fee-444e-8067-aefe5fa84c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7843
        },
        {
            "id": "2739ee70-39e4-48ea-9686-992e606111f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7845
        },
        {
            "id": "7f6dd264-a7f9-4709-bdb3-92f1b263aee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7847
        },
        {
            "id": "3b41c61b-34a3-407a-b444-6c7fc65cce15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7849
        },
        {
            "id": "054f81e3-3b1e-400c-90fa-cab7c6493c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7851
        },
        {
            "id": "31a6220d-7106-409b-aa9e-68141cf75bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7853
        },
        {
            "id": "341708a5-3c83-40e3-9b2b-6ace53eaa5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7855
        },
        {
            "id": "254b4c4e-4b9a-4356-b533-3e4daa0b1b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7857
        },
        {
            "id": "2cb0360a-8257-47f7-b162-fa23297d1a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7859
        },
        {
            "id": "59e996d2-5ed2-477c-8541-9af196f00303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7861
        },
        {
            "id": "d01e7bc0-197d-493f-9d90-097251ba5d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7863
        },
        {
            "id": "434ff0f5-250e-4da5-b869-d61e91a8dbcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7865
        },
        {
            "id": "54eecee3-7bd4-40d6-ae10-59c771e4d7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7867
        },
        {
            "id": "eb03e563-093f-4c39-88f6-edf81fc4cf88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7869
        },
        {
            "id": "815150f1-0050-4b31-8e43-2a8d6eb05124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7871
        },
        {
            "id": "96da0c22-aee3-4a10-a6d7-689a15b61760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7873
        },
        {
            "id": "64a1dbcf-da98-48f3-984f-146490636086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7875
        },
        {
            "id": "e35cdf24-5896-4214-9920-f1690f19a93b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7877
        },
        {
            "id": "976f7375-b9b2-436e-bb4b-d7942735b5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7879
        },
        {
            "id": "c63f42f0-0548-46cd-b5b5-248a724582c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7884
        },
        {
            "id": "71616fee-fd3a-4156-81f0-6c7ff271e17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7885
        },
        {
            "id": "17e3c2ab-0121-467f-9368-044774b8d1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7886
        },
        {
            "id": "a72dfc2e-0a50-4aaa-9a1c-a51c50f6240b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7887
        },
        {
            "id": "26478657-8774-4ce1-b6bf-91aeaa943c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7888
        },
        {
            "id": "a1a4d6a8-e7e7-4661-b40e-e8c8b3b6a184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7889
        },
        {
            "id": "4dedab5a-0046-4edf-b213-f2d8443771c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7890
        },
        {
            "id": "c78bdcb6-9746-4e2f-ad02-fc9e7513eb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7891
        },
        {
            "id": "855b6bab-77c6-41b1-a19b-a1aba0c00ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7892
        },
        {
            "id": "0eaf0c31-b65f-4e92-b741-b9c3d54111e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7893
        },
        {
            "id": "9112c08a-bb27-4120-a295-5d099e203aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7894
        },
        {
            "id": "a50e5f2c-8044-404b-908e-3b26d95184d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7895
        },
        {
            "id": "ba0a1a7f-1259-49ff-9ee8-7848f4fbd0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7896
        },
        {
            "id": "0e50a375-ad7e-4331-81cb-d2b8588920b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7897
        },
        {
            "id": "f6eb212e-96ba-47d1-b053-4ed655c55367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7898
        },
        {
            "id": "de300f33-10ac-4fdc-91db-526fc7f4e4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7899
        },
        {
            "id": "231804c7-9304-4e49-81cc-1075525c0350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7900
        },
        {
            "id": "5e03d1f4-b8db-463a-b85f-977aeed5f161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7901
        },
        {
            "id": "7490c2e0-46f5-43ea-881e-3ee3985bb695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7902
        },
        {
            "id": "d50470b9-3ffd-4447-8d71-86d2dd05ebdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7903
        },
        {
            "id": "c831a4f8-96f5-4854-b981-b941926d03b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7904
        },
        {
            "id": "1a4f35d2-1ee3-4164-92c6-66e1ceb401fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7905
        },
        {
            "id": "8304b185-2ad8-4dab-ba1a-2a218d3e21a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7906
        },
        {
            "id": "b10c55c9-cbf1-4c94-9b95-9cdb5e5b6deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7907
        },
        {
            "id": "f06d25bc-80de-4587-8680-670fb52a3de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7908
        },
        {
            "id": "d2c5fc12-54fb-4b83-9f04-120e8f6096b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7909
        },
        {
            "id": "b455a98d-df20-4fca-8b98-c2326cfc9144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7910
        },
        {
            "id": "e939c470-a389-45f4-a839-602e7a3339c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7911
        },
        {
            "id": "d49af693-51d2-4e1c-ae3b-866723ec07fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7912
        },
        {
            "id": "09a49631-9231-4d65-a34a-ad95c679f8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7913
        },
        {
            "id": "af9423fc-7603-4f9a-a6bf-15ceac925d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7914
        },
        {
            "id": "b607ceaa-0c75-4245-9c65-c7679ea3efe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7915
        },
        {
            "id": "7343aba0-38ac-47f6-b835-bc9185b41bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7916
        },
        {
            "id": "08b9f654-2fe0-4292-9177-70ccb054e5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7917
        },
        {
            "id": "da3fcf81-bcf2-464a-8894-e8ab6ccb91fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7918
        },
        {
            "id": "2f5b56eb-0260-4bc2-b3ad-c557475307dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7919
        },
        {
            "id": "739cecec-d369-4385-9685-da7e6918bcb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7920
        },
        {
            "id": "4cfd0420-06e4-49ef-8ab0-de8d5ee62aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7921
        },
        {
            "id": "0668c386-6832-43e0-a39c-d139e537699f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7923
        },
        {
            "id": "29a22a7b-c101-4bd3-ac49-3b15e9817f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7925
        },
        {
            "id": "8e7176a2-28fc-43e2-974f-3b9e0dd14583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7927
        },
        {
            "id": "64df325f-3fa0-4ac9-b704-d0cec258eeef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7929
        },
        {
            "id": "ddb4673c-a72b-4b31-b04b-13217fa2ae77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "1eb8fc0a-72d7-404e-a06f-5b25082f50e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "52dbd25c-6dc2-4310-b539-592575c9ea92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "e54812ea-f65e-44e4-99e6-e59050c2237e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "1016eb8f-6d0d-495f-b5e6-435193104b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "bb6bbdb9-9310-46d4-8523-152d66c6942f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "e3fe8610-92cf-4378-b03f-e72dfbf5a110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "cfc559b5-e079-480d-97e8-c9b906fffcbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "3b9333cc-6a4f-4f14-ada7-7f7662aacd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "325307d2-5c47-41bf-9e18-53aaaec01474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "da1531bb-63f5-4bdf-aacd-b630c3c7a9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 118
        },
        {
            "id": "75f5fb41-fa44-4086-86c6-150f62ed0f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "7b6a2201-4f32-448a-ac83-f0b5f77acfa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "3e43cb66-f9b3-482f-a6c6-07dc7433bef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 183
        },
        {
            "id": "01ed5285-ed21-448f-a4ad-bfbe8f3a70b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "d3abe629-0e98-46c3-b927-4a034b4f5598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "b6e5d88e-3097-4ded-85cb-a23314eb4e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "e2747faf-a3e7-4e9f-885f-55d426ddfe68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "411b9cdd-2309-4ebc-b362-234567475bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "9c56f528-8848-4495-af18-898fbe516b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "e1531f52-88ff-439a-a7f3-1409f8f9d34d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "c1b06464-7d0c-47da-be0a-2edfacb65c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "86f569be-c51b-4b0e-98a8-643d0e933eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "06c902bd-67ac-4f83-a437-87f38ea2706a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "8ee163c1-5533-4ced-a2c3-b0be134fae96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 221
        },
        {
            "id": "a23449d5-b320-4847-9e1a-fc8ee00c632f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 253
        },
        {
            "id": "b1299e34-5c91-4448-895a-b9325557763f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 255
        },
        {
            "id": "fbd8225c-8893-46bf-8f8b-b42972c20bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "29c695a1-c1a3-4666-9c41-9634bd8b067a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "c8c87295-4a65-41c2-884d-78884e757bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "1186f015-289f-461f-9267-9f97d9dd2341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "a18774fd-78fc-4ce3-a169-963c4a59b522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "45094aa9-8e4e-4678-afdd-804696f0dd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "a01963ae-bb4f-48ed-b878-57148efc66a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "8b55ef88-09e3-482f-b45a-54b04e48acfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "3d71d2b7-6c78-486a-9847-0edc0307ded9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 356
        },
        {
            "id": "47d4cdcd-76bf-4fc5-b14b-7e43da1ca0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 357
        },
        {
            "id": "5f37c19a-faf8-479a-8049-e755904f2bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "64819c8a-747e-461e-b0ed-0ecc3c96fd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "14bdcc2a-5e40-4bbb-9c7c-c16b04d73509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "605c35b8-f682-4fa4-8304-06d9e107782c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "e970632b-9a8b-431e-b913-8b2d763cd004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "f29b49da-45ea-4dc8-ac4a-d671e8c8a4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "d4fa0e5c-65fa-4a31-b8e1-628dd034334a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 372
        },
        {
            "id": "d8ae025f-3df2-49c8-bba6-74513218d11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "ccf0e226-cc36-414f-bf34-45151a95cb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 374
        },
        {
            "id": "95422dd0-4e51-4b20-9eb9-53f2b4e2bb9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 375
        },
        {
            "id": "63e77db6-2ac5-48d7-bcb3-0fd948f1c6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "3752ca71-324d-4b64-9cf7-2fb83231cbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "45e1845c-c6ea-4fc5-aa76-0ac280f88bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "e9cc99a0-cd25-4156-983d-7b6c6b90ebaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 538
        },
        {
            "id": "6e8e9432-ade4-40cb-b172-bf5a903e0381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 539
        },
        {
            "id": "8300dd07-4596-461e-a2c8-91a2a0290dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7808
        },
        {
            "id": "48b4c455-3fbc-46dc-aba2-509688a823c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "0e4b38c5-7d32-42de-9434-e2d48c865572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7810
        },
        {
            "id": "0d8489f1-f191-42eb-a8ed-4c710e7ff79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "f0eeb2c1-9b47-4acc-97f7-e81be7d0a624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7812
        },
        {
            "id": "4cb4a33e-a583-48f0-b781-c38c5106c7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "93b75243-9576-47f5-ba0f-097fbcb6a925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "3c60199f-8fa1-433d-9623-f28cf911463c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "711e23f8-4373-49cb-b4fc-94851f184df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "98a6826a-4fcd-496b-a2ee-cc840468a263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "602c76d2-3bfe-441e-8918-c1b7c12a45d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "9ea23633-3e00-4378-b510-c9a59f1d8dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "be7efa43-4b78-4adb-8116-b001c9a50abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "e39b3717-8793-4db2-b8d4-30d80b5ba093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "3a0ac6d1-ec1c-446a-8417-2d98c472e41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "61929886-7960-44b7-b9d0-2a4ff6176630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "af657c85-9dee-4fcc-8462-c1db0b5fea98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "110c571d-926b-441b-be8b-d4deb617bfc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "23880390-d35d-4dcf-9ad0-28ff1074bf8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "6cabd604-e5ad-4a0f-a62e-0f9a935f8d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "e223ead8-b476-4d59-8652-a8b80f73b4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "152d9ac4-f567-4055-9540-8fba198b3fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "7686352a-0290-45af-a2a2-d3fd9f0a6b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "806cd77f-ecfe-4b1d-92ed-23918ca5f901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "a06d806e-624d-4c03-b9b5-fbaf8ef5a971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "062e01ab-46e5-4eac-b3d6-9c9c485ea011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7922
        },
        {
            "id": "53f1e8f8-0e46-48b2-aaec-e8c155c6e56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7923
        },
        {
            "id": "eb4b442e-286b-45b8-ae89-42340630020a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7924
        },
        {
            "id": "cc75ea5e-5978-4e67-8937-5aeb9d599c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7925
        },
        {
            "id": "2286ef3d-d1d7-40fe-bb04-a0d47ca90a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7926
        },
        {
            "id": "6417aa7e-bb1d-4224-bc0f-0b1c1dcbb7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7927
        },
        {
            "id": "7036e89d-e50e-4166-a781-208b55c01ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7928
        },
        {
            "id": "4c0781c0-6306-4ad3-9dc3-505b02a45d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7929
        },
        {
            "id": "32ce10ca-5734-495f-9aef-ef170d4f33d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8216
        },
        {
            "id": "5fe71b70-23ce-4035-b099-6706848a685f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "e2b7ef1a-3f3d-42da-8dbc-42d0b6ce2677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8220
        },
        {
            "id": "c2c44c68-d69a-4394-b56e-cb4294d55d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "0f71242b-65bd-4776-a9d1-284d55b38af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8729
        },
        {
            "id": "2b6831fd-c66b-46ff-aecd-498f2d32c49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "e8092ae6-7e0f-4d62-892c-d4dc3f4dfac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "c747a9bc-f1ca-43c2-b9c1-242360336661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "aae25f2b-65cf-435d-94e7-00b576ec8bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "2ffe71f6-e27f-4b36-9f44-ed6fdae2dd89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "54f299d7-de49-42e0-a6f7-1a8d021ff49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "1786d0bd-6566-4f00-8e0d-aa0af70d6533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "580b486d-fae8-4e50-ba38-f6cfe5fe56c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "75ec43b6-dbbd-4bc9-9d5c-ab40c334496f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "f8bf13c0-50bf-4499-a6e0-628677c16edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 308
        },
        {
            "id": "096426e6-a83b-4bc2-aac6-5eb8ffcf7e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "3fb7ce35-f472-4435-bd0e-613c35bbb651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "92ca9245-1317-44c3-ba63-93de69f72885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "8ba28f62-4625-4455-be58-2a749e1dd80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "ade15a4c-1182-4cd5-bed9-849beddc76f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "1dd0be78-1ddb-4e6f-8ef2-1ab6399c0c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "df5fa41e-ced6-45bd-ae86-347c785cd2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "17abf90a-df4c-4b37-a11b-58c11fd4d43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "54bcc335-f68d-4733-a7b3-6f281a167c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "c209e9e5-731a-4837-ba66-972b66a2db83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "1d595127-d007-4090-909f-e596303dd9fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}