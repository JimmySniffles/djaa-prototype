{
    "id": "47c90029-41e5-4dca-98df-6ff81ce60a77",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3d95bedb-ee2a-4e99-a055-056ed18324ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1d40d21d-0470-4b48-8925-62693b1de70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 142,
                "y": 84
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f0cfe437-3d77-4e1c-94a3-21b5ce012d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 131,
                "y": 84
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "03c48d56-ca6d-4651-8735-9558bcac30bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 111,
                "y": 84
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9d182727-3121-4052-aaf9-62b685fbe8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 93,
                "y": 84
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "152fa2e9-812a-4bc5-bb5b-43346726689d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 74,
                "y": 84
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ca8418d7-99b0-4566-ad4b-0ac097c7c4ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 55,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1f8cd206-5abe-4f42-b392-fbeaac54f73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 7,
                "shift": 19,
                "w": 4,
                "x": 49,
                "y": 84
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2f105654-d7e3-4f4f-9ed0-7a21669f7192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 35,
                "y": 84
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "08292799-10c7-4289-a8d0-9dd7e0c12e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 21,
                "y": 84
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ad6520a4-be9b-484a-b724-5563dc6346d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 150,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b71f86db-fb29-4699-a257-32b898e2daba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "559efa0a-2878-4ea0-975e-e4f07cd3db79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 476,
                "y": 43
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "056356a1-51f5-4735-9ad4-76fdcd505556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 459,
                "y": 43
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a0b6aca5-5ead-41ea-9a33-93023b33f662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 451,
                "y": 43
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b07223b9-4e8b-4522-aa7d-8b4e3df9fda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 434,
                "y": 43
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "77367f1e-7c2e-43f0-9ec8-31708714a3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 417,
                "y": 43
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "76d8edcd-6bf7-4348-a326-86f76073766f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 3,
                "shift": 19,
                "w": 9,
                "x": 406,
                "y": 43
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bcbd5f9d-76e1-4547-8637-d9fca555376c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 389,
                "y": 43
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "088e0bc6-01aa-4f99-8186-1a043d215a14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 373,
                "y": 43
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "602f74f7-6aea-400b-bbd4-a904bd165736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 354,
                "y": 43
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "14d1b81b-203d-415c-ad4c-2b8031bcb464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 484,
                "y": 43
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "312c628a-97b5-49ac-8a83-2c5d0f3f73d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 188,
                "y": 84
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9a52f16c-dfb0-4496-b968-281d6e432376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 381,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1ac67955-ec4a-483e-b3c3-2ab22528dc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 205,
                "y": 84
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "be69d1ea-d50e-4161-8af6-91e2b0bbc7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 56,
                "y": 125
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "48d23816-1c04-4fbd-a701-2cbdd57b5521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 48,
                "y": 125
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1d662ea4-f41a-481c-93c0-bccc2472f7a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 40,
                "y": 125
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7435246f-4445-4668-953e-dc9c9843a1e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 125
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8654f3fc-2cf0-4a53-a44a-816c17914e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "acb20f15-f45c-41a3-8055-1bfc8620fe97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 474,
                "y": 84
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a668c424-eeba-46c3-b3fc-26bda771a6c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 457,
                "y": 84
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e27d6c11-a20f-407f-9834-5ed82a0b3fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 84
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2e79f9d8-886d-47ad-a9aa-67da9aa3c10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 417,
                "y": 84
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "45018b58-c816-4efb-9911-aebd5fbe7a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 73,
                "y": 125
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c706c7f-f3ad-49ee-b044-4675dba21962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 398,
                "y": 84
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "935ccd1f-f7c3-4f6d-a15b-3ca9e6ab5196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 363,
                "y": 84
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "77dcdac3-bc6e-469a-af70-b2ba05b7aa9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 346,
                "y": 84
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b6da0758-f391-46cd-bc89-b42077338b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 329,
                "y": 84
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f8f14d40-4c04-4795-8559-718391db6fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 310,
                "y": 84
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "842c0533-14cb-4948-8deb-d4740e94450c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 292,
                "y": 84
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c4010331-9a63-49b8-b4c7-9ad9c12d9bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 277,
                "y": 84
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "68811570-c0d7-4546-9d17-2e2fded97623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 258,
                "y": 84
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "59f6dc1b-3b00-4af2-ae2b-3b90ae6a7fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 239,
                "y": 84
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9f2a0fd3-7cf3-4b3c-b324-e6ff99a498be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 222,
                "y": 84
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "80e6588c-8bdb-4fb0-8035-05c03d681649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 335,
                "y": 43
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e3f760d7-5d39-4132-a17f-622ae48d25b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 169,
                "y": 84
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3228c755-0e5d-469c-a387-dd4fee464b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 316,
                "y": 43
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "969e9ac9-61b2-44ee-9872-f1ebb7d6e635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "267ccf95-f833-4e08-b685-9ace5887bfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 352,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7a6d4a1e-2fa6-42f6-ba95-1e7e50d7e9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "457681c2-952a-4d2f-8ac0-bc34944dd837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 316,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a8461271-73b9-4142-8c65-6c84e7d0dbc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0fe34f9c-5e85-46ab-bf1c-06ca0260653f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a17dc869-b64c-4fb3-995a-1963f75b51b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "08dd00a9-dedf-454b-8060-64c13a1eac06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9ca5465a-4b46-43d0-a518-d3067e087b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "de5c4da8-93e9-4887-9a2f-57b1e92f5bdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9ddc338a-7aaf-4420-bc12-047c55e32b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "23c9d85f-0aa0-4043-94a1-536956592c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "611fa337-6011-4a88-b7f0-2fb6df5992b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "094f6e9c-e2ce-4dfe-bec2-4b654b96aecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9ebbf686-a50d-4757-b091-fd1843831a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3346062d-0fcb-489b-982c-b3996a766d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "230f5a90-7fab-4306-8215-dace02e3c230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 5,
                "shift": 19,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f06b08f9-8cde-483d-a8a6-fd1a4298cbfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f3775924-f94a-410d-86ef-9125daeb610c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bf4a75f0-0597-4bb8-960e-e45b38fbb4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dbf913d5-d389-4d5b-9a2a-89bd85ea742d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "946f938f-9991-4109-8331-0fc50d60854d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a364cc20-1839-4b00-a571-cd7fe0b1bf4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5fdfd7f2-b373-4134-b989-02fdf175098d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 93,
                "y": 43
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e7e5b4cf-568c-42e0-b8a9-03d5900dcadf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1ce884c4-f95f-422e-b7c7-8fa9f45cf744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 3,
                "shift": 19,
                "w": 12,
                "x": 270,
                "y": 43
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fc731387-43ff-4324-99ea-396747a81219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 14,
                "x": 254,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "be3a823d-5553-4bca-b974-0788221a7efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 236,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "33206732-2f10-4eee-8a44-3ca07c3bad2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 220,
                "y": 43
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "58671d94-8395-463e-b252-a2f698ee4e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 201,
                "y": 43
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4de65895-713b-4224-84fd-3e913c264342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 184,
                "y": 43
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "67eaf0b7-8bd1-4f71-841e-15b966dd5528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 165,
                "y": 43
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7396057f-ba86-4f69-ae51-7bd89d434a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 147,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a83a0c77-e9b6-4936-8e9e-4d641a52ed43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 129,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eaeb0bc0-56c0-42d7-b0d1-55d4a99f0187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 4,
                "shift": 19,
                "w": 14,
                "x": 284,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "50ceab06-10ff-41df-8da2-4139ee909334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 112,
                "y": 43
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c14cae3e-67e7-4d66-bf11-12a5fe833a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 76,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "79166733-31ef-4ffc-90c6-d99577298323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 59,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9588401b-2679-4695-80e2-191479b21e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 41,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "cbf34a72-2ca8-4ab7-8421-db65ed9a0a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 21,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6d732938-6d5d-4641-a8ee-9352f6b5b56c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "eca8d73d-a1f2-4c07-800a-0d63935005c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d6288713-6125-4ba0-9448-c44c2124bd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f311173c-4e55-4517-a88e-2203f95d9aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ea03889f-194d-4af7-a17c-e76fe9c7c53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 443,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0cb5477a-2052-4eab-b43d-f9516cc1520f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 300,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "159a90f1-a04e-49b5-84c3-8d90c99be556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 125
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}