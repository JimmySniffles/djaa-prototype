{
    "id": "11afd6a9-daf1-4cff-8705-3b4f183362b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_tut_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "228387cb-f999-4f93-a5ca-63d86f3adcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "54720327-f98a-4139-978d-f7e111f3da77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 64,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "46be30dc-2840-4b38-92c5-1589a2cb4873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "656446ee-ba85-497e-b1f0-f258d60ef7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 42,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "deefaa1a-792b-4daf-85a2-afa7e6a4870c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 30,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "013f89c6-718e-4e39-a930-5fb273a9f6ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "caa9b3ee-42ba-4434-a1a3-9513daad4d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "534a246e-4a59-4a2c-b47d-bc0acff7ebac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 6,
                "shift": 13,
                "w": 2,
                "x": 248,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6d6ba867-4a58-4273-a7cc-3dfbb0cef703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e5788579-fbe1-40c4-ac06-e1584847c942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 228,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "21e7fc0a-4bfa-4800-82b4-5ad365182c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 69,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "03a40e5c-eea7-45e6-b492-8c47c2ba05e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 215,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3193ac63-86af-48de-b298-8c7f58e46ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aee6a945-584f-4018-a1f3-142da4c5e8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 185,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d0cf69c9-273c-48df-b6d2-4d0b2a60dcac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 180,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "95875385-1f2a-44ac-994c-b272ee58d02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 168,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "526d0723-8fe3-4cf6-9935-a6c8ee659d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "448fc38e-2f94-4e5e-b9cd-57642f5fbf33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 6,
                "x": 148,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8933fd71-efb8-44c4-9cf1-1c725aed73bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 136,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "07dc0f4f-0724-4924-95d3-343a5b33648e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 125,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9480606f-518b-46d6-a3cc-3be730e6a77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 111,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7aaab21f-6b76-4222-b96a-9b55c13e5b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 203,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f347e051-e543-4785-baa8-9bcaea2b7ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 95,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7aca6d00-04f7-423a-88a6-fe670a485322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 232,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1dcab673-c1bc-4777-9821-0c77d739fbb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 107,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff6dbb17-c028-4ecb-9822-824ef3f36386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 106,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "832a07b6-c171-4db0-9f9c-798b08f730d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 101,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ff2e3e7b-cf84-42df-aa05-23a550106b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 95,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3fb38ff2-d709-443f-9eb4-f4e0a07212c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 82,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "610f3478-cb5b-4be2-b7eb-1d78f31602e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 69,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fbee8695-9cc6-46cd-8b77-960aefd6ffcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 56,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "10360519-5d93-4c41-bdfc-92a98b681311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 44,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bc60869f-aec0-4174-b84e-6d180083722c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0c39056c-52c8-48ef-b63b-8b50d70df24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7e7820e5-d926-4bc5-abf7-906668ce878f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a6037e2f-5d02-45b4-bba8-f484e56f5378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fac19fa3-3370-4bff-929f-849f911dc6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2ddb2d61-b5ef-4c2f-87fd-41e9d7ce1939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 207,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "45324aea-67ce-4e7e-b541-4c50aec2e668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 195,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3b3d2745-b74e-453f-a2bf-ce055ab5ee13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 182,
                "y": 92
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "19be585b-f35e-419b-bf16-cbdb7cd42c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 169,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2c6bc018-e623-43d4-91bd-3fd7b5718d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 158,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a46212c5-2523-4990-8a30-85073b379ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b93aae3d-66fd-4abc-be57-769e8fca62d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 131,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4523f621-a7cd-4562-8f17-b9f820bd835b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 119,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a5712567-f3ee-498c-9f99-a4ca6d6c50b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "be24e0e7-c3f0-49dd-a8aa-f24618d9e713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 82,
                "y": 92
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d7b87c77-2f92-454c-8259-bd94185ddf20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ab009c9d-f850-4d4b-977b-6f023c2b067c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 29,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c3637526-e74c-4bdf-a199-58d0ad67b6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "dda1080c-342d-4c0c-bdae-12227c941a23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "247504fc-c7fc-47e7-870d-f5587aceded7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "22cd8ece-19c4-43f6-9d5e-05b6b84dccd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "75b25973-3f2e-4f1f-967b-4255b056b089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b9fb1651-cd75-43b1-80cd-826ee33a4964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "191e861d-d0a6-4ed0-a30c-619c47099df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2bf15f93-025d-4f3d-9c02-1b02f13a8a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1ebf0f11-c3dc-48dc-af81-8139a3b227fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ae1f062b-6235-4e5a-b6f4-5b31995e20aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4ee44c03-ec72-43cf-b644-5cbd8e1f353d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5b7ba4f0-4a69-4116-b675-965045b0ec3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "54fe84ca-58f3-4566-819b-202bc664d542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "38040b1a-972a-4ef3-991b-862d3bec12d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bcea60d8-ddbe-4282-bf12-c73dbda6d71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1e34e2ec-96ad-4045-a8ad-2e5073b762e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d646f7ec-397f-4fe1-a39d-793ee42bfdd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fca7244d-0a58-44bc-b7c4-0c08f93d170c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "46220ca5-d9b8-4420-b9bd-fcb6a8e3a710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0d8241c5-2802-482c-975d-59838fa5aaf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "63163f96-bd99-430c-a624-3ec3d829e4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9837e329-c254-4f06-9b8d-e66d3d991fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 41,
                "y": 32
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fa30cad2-d23e-4594-908a-0fe916ab8b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 173,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b89a981e-80f3-4702-b8d9-3fbdecfa1216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 54,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2a12f525-d98b-40ba-90b6-c791fe0c8867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f81d36c8-1e3f-4647-a858-ec2a60e9b893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 40,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "147e16aa-a8a3-4ac4-9f51-c0607e955b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b0983697-9a1f-47b0-9045-62134ce0b207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9716ebac-3e38-4e51-84d0-1a3ea4755420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a2c3ac25-834e-49d2-a7ef-cc07f6c4cac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 237,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "902e8c7e-e8cc-4bcf-8df9-a1ef23aea409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 224,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "79d6293f-7541-40ad-8e13-d40a67e64bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 212,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "276ec4b7-8fe1-4876-8ea0-d240a24816f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 199,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2fbae428-3953-4c63-b6ee-3d8d66d1b9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bb6538b8-9a62-4653-8427-a92ad1caa897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 187,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0c9dfccb-df6b-4276-aecc-06bf99176284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 161,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "98158955-6075-452d-b7bf-88e1993eea1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 149,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bd883c00-e842-469d-98de-691b25730a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6224ef1d-c3dc-4c0e-a00d-22b1938d9e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 121,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c63c4f38-f5d8-4891-8391-622cb7febc77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 108,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "eadb9b36-5b7e-4b78-936e-7f589ccf5189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 95,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0696d2c9-994c-4746-91e3-a94da7eea4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fffaa6d3-87f5-4a18-880a-bc98847b6b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 70,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "341f40ad-b590-405e-aabe-49ebd25bd143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 6,
                "shift": 13,
                "w": 2,
                "x": 66,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "602e2816-76a9-495f-908b-3c6500af5a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f388561b-5d73-4751-9af7-91f72d773d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 131,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}