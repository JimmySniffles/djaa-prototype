{
    "id": "11afd6a9-daf1-4cff-8705-3b4f183362b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_tut_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "58b1a23e-2385-4be5-b1b4-26f0d0187964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "df741512-3da3-45bf-8c5c-1ae98594465c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 123,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9ee30f57-60bb-46e9-a1b5-112e129b03ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 114,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "24d34e59-46e6-4d72-ad24-c4eacc011fa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 99,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d1debd80-5a24-4565-b87b-c8ede768319a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 85,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "10825c73-b06e-435d-8bff-36d2b366a0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 70,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0ec9687e-4c79-43fa-89ec-da37ff7da5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 55,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1eb03a59-12d9-48c6-819d-e1c491f242e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 50,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7e720ddc-798f-45f3-9d40-54c29504f1de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 39,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "64900771-4998-40d3-ba8b-524838c28c4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 29,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ff50ef09-1a35-4905-bf58-3a33f640ff40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "67ae0823-fe3d-4e10-8fb6-d709fb5bd51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bafee16d-126c-4237-8d9c-1384f6199f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e4b33c26-752b-48e8-a223-edcb21ae1eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 225,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "79d6ec42-8f1a-40bf-a59c-edf0e7fe97b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 219,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "de95c33e-d9a8-4920-b15c-10c531dbcd52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 206,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "96691040-3825-4bd5-a70e-ce46de86e1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 193,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "06af0942-255f-404c-9494-3299cc20a12e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 6,
                "x": 185,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "20b882f0-9f75-4d2a-8f63-85238e2e6133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 172,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "83822625-9e83-4a07-928b-2839e9d50842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 159,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3f9acba8-316b-4490-98a9-4e860e92e031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ba99e1f6-2ef5-4fff-b30c-48bb5d693c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2af796ab-f30a-4357-ba46-8090b090b86b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 157,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f94617f0-1c90-43f5-91f9-c25a6ff94287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 56,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3b59950d-6ce6-4700-962a-f1492c19962a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 170,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "03af1c71-1eba-4080-bdbf-e16d9c8d602f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 181,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ba620bce-8125-4015-9b16-605508ef44a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 175,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0698aff2-d4a7-436b-bc7c-79cc10e6a9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 168,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b0a895f3-3be4-4569-87eb-99bb1130387e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 153,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3ac5226f-f45c-4960-83c5-c24c5e3fa7f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 139,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7dccb31c-181e-452b-a25b-0c7f5d3b2af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "10d5f122-b64a-4a4e-a825-2afcf1d63a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5a050c69-9b73-4578-92dd-f76fc99fb8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 97,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "35017b84-ffc6-4fb0-9579-92283a7819ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 82,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6de6e523-10a7-42ae-8e55-33588570f617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 194,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eaabe567-c011-4586-b7c9-3da010ca0836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 68,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e5186b06-b790-4a6d-b669-99726391dfff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 42,
                "y": 122
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2026cc75-4f33-4321-98e6-9423259e054b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 29,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3346f4e9-b353-4b8d-8a6f-df8e8ad98bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 17,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "923d0737-f6ef-424c-ac5e-2b4275f7a5bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2308116f-600f-4066-ae26-6b12b1fc9e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 235,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "721ba77b-6125-48e4-a32a-eedbbb4ef535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 224,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "be75b410-e698-4ca2-a056-86e5e95c42bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "952cd90b-8557-43c0-b0b9-df323307ad7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "149237f4-a697-4c9d-943d-8b125883ff67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 183,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "81c488fe-2533-4830-9ef0-44cdaa7064a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 131,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "91e286fa-7c5e-4fd7-8cbf-d6bf447e297f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 143,
                "y": 92
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "baf0e6d3-dba9-43bd-a37b-95dd9b9376cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b437886a-398e-40fb-b13a-8cc01259cf12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2546654d-1af9-4f73-a4ea-bc6e1990e086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eb28d79e-e5cc-4cbc-ae88-a3c9ae84c1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e6eb994a-958d-4a37-9bf7-c68b0cf5dc3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b694073d-e56a-401c-975b-1448ee67bd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "21d5bcbe-9f03-4984-b452-e6bae3618a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e62e5131-88c6-48ee-af9c-b9be34655662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "36efa79a-bb23-4d4b-90ab-3c2d030b7623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8d996f2e-27d1-4111-81f4-9948bd5e308d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f5cc9792-6b8a-4d0a-bb3e-a524998766f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e73ede9a-2ab1-42ad-9710-cba1fdc839ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f9652f90-78ad-4f68-82b5-458df3115f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2f2e1132-84a3-47c7-90df-71ffc95ad778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "08c04630-53a8-44e9-b382-ecd4da9384c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f8d1564f-4170-4690-9549-0863b7590589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0c7058dd-e735-4c27-b0a1-d7fe0806ecd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3c60aa90-2f70-489b-97eb-0f6601655dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3f3ff399-12bc-4a65-ad67-578202da5cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3cd83ad7-e11a-4a8e-8111-250fdd82f09d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5d1ab666-7386-46f3-a98b-7e09159df2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e112dadf-95ee-4b6e-9190-82892b9a6c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2ad16767-b2cb-4b63-b338-9d814a5339c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9c3144ce-c295-477f-927f-9929a31ca996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 59,
                "y": 32
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "95b214a5-8b42-4d98-94d8-fd17820bf985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 199,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f8504231-5229-4381-9fab-bdc8ba9208db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 73,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e6fbdbab-e82e-42d1-8685-1d240114f2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 81,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9e31f2d9-bf76-4984-9015-c4b3a91926b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "80f7fd57-6007-497f-adfe-4cdf98c1e99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 55,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "10567ac4-1def-42e9-a910-3989182ac99d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 43,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "edb428b4-faf5-4f1e-a8a9-9a42a6a9a74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0b37a273-d3a3-4e03-89dd-8b6ae27e31b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "24d1204c-6979-48b5-9222-8b652b39b005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "04ec6b6c-0e44-4065-8d33-2e433568a063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 239,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8dadef22-d801-40a0-8ae8-209645092b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 226,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "139b8353-eb67-4c6b-96ab-82b7fdd0fc51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f6aac52a-5677-4b9e-91b8-a80d75b73e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 213,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "36700a3e-81c7-4c67-ae25-32cecfa8480e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 186,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d56c1b4a-1ddd-479c-9528-f9bf28b3525d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 173,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5ba2a24f-098b-4ac4-848c-bdd0d046d6f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 159,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "875df475-6348-4ec7-9c6b-70749f40c3f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 144,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bbe12285-6d5c-4916-a7d5-92fd0f96b734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 130,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0f5b47b7-6440-4698-a52e-1d6de6b25720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 116,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "67e7d778-2fbd-46b3-8f85-c6c4fc78a36f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 103,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "79faeb38-95db-4bd8-9589-65945e7929bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 91,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a52725ef-6e4c-433c-9d59-8c1a389be59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1057d774-d5c0-4bbb-8819-9e85b96ee59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 104,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0288dec4-152f-4ca0-bf87-8c32d3ab0a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 208,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}