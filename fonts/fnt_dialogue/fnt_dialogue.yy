{
    "id": "d973c76c-3db0-49f2-b849-9d27b3f5ed85",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "56141e3d-733d-44f9-bf67-d225c4c024a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "850ae447-79e3-4a21-8fd0-b4db1f3cecb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 30,
                "y": 146
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d40d060b-0ae1-4bc5-a74d-484ba676cb3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 20,
                "y": 146
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "874bcdc8-3491-4d8a-876b-32cc42b9a2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e48df28f-139e-4a59-9822-b065a36c1be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 230,
                "y": 110
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c9182b81-36db-4db4-9b0d-aca5a3dc8f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 212,
                "y": 110
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c72c0ebc-97be-4231-8ba3-e0d81ae5245f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 195,
                "y": 110
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b9738a4c-8a09-4fd0-8113-a28a75a4acc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 189,
                "y": 110
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "464fedd1-9e70-4aee-9a0c-f3e948c3bb3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 176,
                "y": 110
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5e8e8515-b6ff-40d1-a322-f2a9a5d914eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 164,
                "y": 110
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "89cf7585-a2be-42bd-85cb-b01d6e281deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 37,
                "y": 146
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c8f7c56a-05ec-4074-a239-76d36e2018c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 110
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3b9d7bee-dd8a-4ab4-919e-ad09ad87c5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 126,
                "y": 110
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6e731b78-8fb8-4a81-9e0b-46bed52e49f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 112,
                "y": 110
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e6e58955-502f-4ce7-a67e-2d9abfc0a37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 105,
                "y": 110
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f21d7b7e-e2e3-47ac-b2da-9a988a33987f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 91,
                "y": 110
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "765a9ef9-fda8-45a1-914c-768479921599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 75,
                "y": 110
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a8280e15-c778-4dd5-922d-6756ba64f427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 65,
                "y": 110
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "15e69edc-ca49-405a-8244-f17ace055dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 49,
                "y": 110
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9c0ab042-17e3-49ed-8792-a7c6ac119bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 35,
                "y": 110
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0ea85d5b-6b31-4fea-9ade-b6fab8708471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ed827102-162c-499e-9050-3bb7d895fce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 133,
                "y": 110
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b49c26f8-82a6-4069-aa39-64ab20bedb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 69,
                "y": 146
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a879a906-ee51-4185-a7bd-f157b2a6b696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 239,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5d7e5c2e-601e-40f8-93f2-cd1155fa51e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8dd18325-51d6-48d1-8915-b9ab5fdb077b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 130,
                "y": 182
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "98abfbdb-ff5c-4e28-bd3b-e7c2289def10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 123,
                "y": 182
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6dc4cdbc-7fd3-4223-8f61-bff85669f43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 116,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ffd5cd1f-65f6-4742-96ef-7b0a2e8ff957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 182
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "05d95833-2caa-46e5-917a-714017d834d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 182
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "43ddab08-cc0a-4a0d-aa99-e29f5165a193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 182
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d7f1833f-3e45-4037-a395-717bb94d2f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 182
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5eaa1721-c935-4de3-8035-2218b639d786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 37,
                "y": 182
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0a4d96bf-723f-4b38-8eaf-fff894ea3f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 19,
                "y": 182
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "df5466f4-df18-4f3c-a385-8d04145c3b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 145,
                "y": 182
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8ca49ae8-653e-4fd5-8d42-1b1d9da398ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3a68f5f1-485e-4893-ad22-2279f802bc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 223,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8cb04266-0dcb-4b65-b09c-22bdd6caceda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 207,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cab29b8d-dbf8-40d3-b706-61a30ee2235e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 193,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e783d650-c9c0-48a7-956d-b8597998a16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 177,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "774247a6-acd0-4c62-b212-1ee4e18bb466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 161,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b3a5f289-8fa8-458b-932a-f89403294faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 148,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d0618130-1bce-489d-9972-7a71b80fb448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1d086156-f0dc-42c8-9629-c88ab5c5e156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 115,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7884ba87-0445-4c2e-9268-48009050f23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 100,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cb230b6a-426f-45bc-8044-b1422f014915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d88b279e-fc61-45ac-a05c-fdc50b6da5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 53,
                "y": 146
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cad482a3-a98b-4a9e-89b7-b207219057e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "67be72b3-f04f-4aae-bcce-2b63c3280b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 38
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f357ce0b-44ba-4481-8f43-99ce4e4aa88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 67,
                "y": 38
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e889752c-f448-471b-862d-41c8dec9d38f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 51,
                "y": 38
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2488e23c-7205-47a7-97b8-2425228d7002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 35,
                "y": 38
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ab570295-e18b-4bb5-8db0-1dd3b084e0f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b9b21daa-9ecf-496e-8b51-1cef2c6052b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8864fb66-44c4-4830-9b94-d12e429fae4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7a68a56c-2b0a-403d-9392-df14680d280d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bd36ab54-1663-47f5-863e-debabbe6216c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "df49fe49-f30d-483c-b18e-d62582e4ac28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e3499af3-0ef3-4c4a-a875-555f12612861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 85,
                "y": 38
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "04f3aa5e-bd8d-4c15-a500-308aba226a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f42f487c-a404-4163-9c8c-f2ca717df545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "537d2304-ad7b-48a5-a11b-9c400aa72470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a4d94999-346e-4c08-9fa9-7bdc5dc87a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e3a7ebfd-11c6-4f39-bacd-8d14465748fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "94f4e379-20f9-4fa3-a1f5-eb823408d240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "632da4a5-1c88-4b4a-874c-18cb6f79e0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1242e1e2-db16-47b5-8772-88d587aae50b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "07e3c10f-1912-453d-ad20-dae8fbd2433e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0d1b3068-1db0-4fa4-9cab-ff4e6b412540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9971b82b-c857-46c4-9cd8-d1440a4c1e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "24a0a3b4-e15f-4111-aaef-dd4e01424274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 117,
                "y": 38
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6370edbc-9278-4f1f-9ebe-06963fd11fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4288deb2-0f3e-4144-8e7f-c1e2c72286a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 133,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f9201e98-45a3-4e93-9578-770f3b04f36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 188,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bf7495f1-6fea-443b-bffe-68343c5d6d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 12,
                "x": 174,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "25760994-1f3a-459f-9639-a043cc8d6748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9e9fbccc-eea0-4d5c-a332-8e68052bc5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 144,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e28c5348-d1f1-495d-8972-7cafe4a2fdee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 127,
                "y": 74
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "87c036fd-1b73-4d8e-96e5-0b4f8ebbc241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 113,
                "y": 74
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fbfd13a1-311e-4091-ae3c-8932a1806175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 97,
                "y": 74
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "99a43429-948c-411b-90c1-eb0b06dc806c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 81,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3a557f79-2088-4c76-bdc3-9fce971483f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 65,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "281ba36c-a453-4632-8aaf-38cc0e4c6cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d0fad485-b6d4-48b0-b0a6-ec7ba7304998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 49,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "921c02b3-fb2a-4fbb-8e1c-9928c01c9b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5ea7ee44-ae3a-4678-8653-f7c05dd648cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ed368861-0fd3-4bed-91f1-c13229c74737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 235,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "da7a59b5-8bef-4616-826d-82f5d1c7894d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 217,
                "y": 38
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9a96113e-85c6-449a-80de-9170c0f7ddf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 201,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a427bebd-6516-436b-ad7e-418c553500eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 184,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "60e27dcc-9582-4c4c-98b7-c46dbde9154c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 168,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4c9e8046-a4f6-43b6-a52f-ec8536a132cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 153,
                "y": 38
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "db3e30d8-5613-45cd-ab00-c74392b97e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 147,
                "y": 38
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "86c96594-72e0-4606-b1bc-230cde88f04c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 214,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "220e7bdb-3f54-4cee-8dc2-0d5ba71391fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 161,
                "y": 182
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}