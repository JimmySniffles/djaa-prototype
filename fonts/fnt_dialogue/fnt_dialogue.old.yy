{
    "id": "d973c76c-3db0-49f2-b849-9d27b3f5ed85",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Inconsolata",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bab50879-2303-4cad-a1e3-3bb684e4ad80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b85e4d2f-cb57-48a3-bfcc-de8c7a3ccca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 6,
                "shift": 17,
                "w": 4,
                "x": 47,
                "y": 150
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bbbef0c5-d711-46f5-8be0-96473d45d295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 36,
                "y": 150
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "abb5c3ef-5de9-4159-bd67-afbd402ecebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 18,
                "y": 150
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f8f25d81-ebf8-4757-aa7f-02bd5322df07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1d153166-e6b3-436a-bcc5-da833684a009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 235,
                "y": 113
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2860b661-a9e2-4194-9ef4-f7ab08306ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 217,
                "y": 113
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6f33f95c-3eca-401b-8c9d-a9e2f3cd2a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 7,
                "shift": 17,
                "w": 3,
                "x": 212,
                "y": 113
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e0294552-1c6e-4032-a94f-68757a708e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 200,
                "y": 113
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8633c370-5c13-464d-91d1-31766350ad21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 187,
                "y": 113
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ada18fc0-a61e-4f3a-8f8a-76277d9682e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 150
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8ec89bf4-d215-4508-9172-ef6e5145f177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 170,
                "y": 113
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e32e4f0d-2385-4212-b1c3-7076daf0b610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 6,
                "x": 147,
                "y": 113
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "332bb078-e788-429a-8a35-81f46e32d4e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 132,
                "y": 113
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fcaf6bee-569b-4661-9de1-d565cf4d935d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 6,
                "shift": 17,
                "w": 4,
                "x": 126,
                "y": 113
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "74f32490-d2b0-4c1c-bf50-d45221bffeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 111,
                "y": 113
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6b0b4a61-73a5-4c79-938a-065a371db396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 95,
                "y": 113
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8bf1c51c-c76d-475e-b205-89264b203d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 8,
                "x": 85,
                "y": 113
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "86088f19-49f0-4945-a015-f28d52904e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 69,
                "y": 113
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a6626e1a-0b80-4a19-b487-fbe6a77e04b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 54,
                "y": 113
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8feb2c4b-df8a-428a-9176-3c0ce6f6fde2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 37,
                "y": 113
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "70987c4b-2267-4a13-88d3-8f2c56314466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 155,
                "y": 113
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "895bc6d9-a526-4595-98d2-1841d31e440e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 87,
                "y": 150
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d0d721c6-ab55-4a19-88b2-8739afb84640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 19,
                "y": 187
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4ff8ad36-dc28-4d9f-8c15-31520e56c027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 102,
                "y": 150
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1d41fe95-b168-4b90-9852-50e792fe3278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 168,
                "y": 187
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ec713542-26c8-419d-9e50-0bff2b6aef85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 5,
                "x": 161,
                "y": 187
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "633638b3-b0bd-41f7-a8ef-deeecde46f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 6,
                "x": 153,
                "y": 187
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "32c0405d-aaa1-4d8b-af2b-e5512191dcb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 136,
                "y": 187
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "51842941-1749-4b87-bc9e-a3444cd16ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 119,
                "y": 187
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8517005d-d3e1-41a1-87c3-489703019d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 102,
                "y": 187
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f6f6ebaa-7810-4122-a419-f092fe30ef66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 87,
                "y": 187
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e164138d-2ed6-419c-bc17-ca14dbf8b5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 187
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eb18677e-1c58-4de7-ba12-315f646e6f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 51,
                "y": 187
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e678dc47-8531-4de3-90c9-65f2a0a86400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 183,
                "y": 187
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "da9434d4-50ec-47fd-bd9c-c28bc444ba3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 34,
                "y": 187
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "93871ef0-80d1-4a43-a43f-c174d5b7c59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 187
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "546b4094-c27c-459c-b3a2-6950c1a7c5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 228,
                "y": 150
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f4fc863c-7a09-4a46-b862-fe73b20ac6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 213,
                "y": 150
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a1791d87-fad8-4006-8ba6-2360469c245a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 196,
                "y": 150
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4455c8be-dfaa-4c6e-b9bc-c201008f66bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 180,
                "y": 150
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0542c006-9bc6-4039-9074-d9d48dcf35ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 166,
                "y": 150
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "30a80e23-9495-4d62-a4a7-fe895d0cc222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 149,
                "y": 150
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6cc78f58-e769-4489-b948-561526093e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 132,
                "y": 150
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "32e6dd62-4569-47a4-b97c-3897af95bf40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 117,
                "y": 150
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9d3f06eb-e458-42b4-9298-61bb9452cf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 20,
                "y": 113
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bc297b91-ae3c-4d15-b9b4-f8b0cb2b9f86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 150
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8220d7d5-f4a5-4328-aff5-7a321e021bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1da080cf-d6fe-4bac-8ae9-d6b6b768e9bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 105,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "773dec94-6e4b-4cc6-9cf5-4450b2095053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 70,
                "y": 39
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bb3c9ac4-fa7f-4d9b-a7bb-d3a8438f0870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "68328826-cf27-4525-b9ab-9cd17826ea5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 39
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "651b5186-3407-4b9b-9511-7e2a55b2cb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 39
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3b7eaf0d-7db6-4990-a42b-d96dd53497c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9db02c8f-d52d-40c7-bdad-ecfb9fe0ca75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "67e809b6-b801-4b9c-918e-e4f6f5459e37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0f3308fa-4b2a-41a6-9598-ec73dcaf3fc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "df57c11d-dc3b-4d3c-8ba4-248059624fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4208c1b7-5d1a-4afc-9a97-308370811336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 88,
                "y": 39
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "18d8e02a-9405-4a98-b546-25986d506c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f6c3d902-4d31-44ab-9a8c-4faef7e1f87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0dce3be6-30cb-435c-a10e-616dc5256aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "af00d4a8-1ece-43d4-84d9-5517086cf16f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0fe32635-caf4-4ce3-81a6-5e822183aa12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "54c8c683-6677-48d7-bac4-ee534611dd02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 5,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3b8ca636-b5ac-4aed-9099-a749f3df5a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8816f8d2-bd58-4a66-8c41-758a91c8334b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0d37b4cb-0ec0-453f-8e2d-f7c52b18fca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bfbf7c42-b9e6-480b-a6d3-d8ae4a66db32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0f2895d2-e527-4b75-82c6-8753db78b089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bb013611-fbcc-4420-8d28-0384a9564c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 122,
                "y": 39
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b2708916-4141-46c8-be39-f52f4f96c71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 49,
                "y": 76
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "545e561f-1aab-42c8-8f1c-3305330e807c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 138,
                "y": 39
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d9ac3870-8d95-4cdd-ad4d-de01cbca4a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 208,
                "y": 76
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "27e0243c-6b50-4fd7-81f4-87fe1889375b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 194,
                "y": 76
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "91f818fc-2d1d-43e8-aa86-7ad2b5fdaa50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 178,
                "y": 76
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c10c61f6-f436-4acf-8594-7bc185c015f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 163,
                "y": 76
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cf39c2d9-0a28-4ed2-a970-075a5d3110cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 146,
                "y": 76
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b252e85d-5318-4baa-8f97-64ce4e70e4da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 131,
                "y": 76
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2af5cb17-d103-4444-9f89-17f1dd4794ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 114,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e19bfc9-6532-4ea2-93a2-a197f730901d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 98,
                "y": 76
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b617e7c2-34c5-43af-a274-2e74a0cbfe1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 82,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6299b1bf-9dfc-4a09-87dd-46d4bff8da86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 221,
                "y": 76
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "99bdd5bd-2cf7-4ac8-9874-0cadb7724675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 66,
                "y": 76
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "63f973f1-f54f-47d3-b974-42f8c34dcbd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 34,
                "y": 76
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4096f254-2032-4191-a17a-af30a7838a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 19,
                "y": 76
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "20b4f850-e912-496a-a298-f8035b80cfe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ddd39f82-1e7f-42bc-8180-4ef6c87a19ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 223,
                "y": 39
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "42400d7a-dfd6-4827-a878-764edb0b9629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4513183c-81cd-40ec-a21b-85bddc73f2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d9e93ebf-f150-444f-9b87-931d1258ab87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 173,
                "y": 39
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4da16678-1ed4-48cb-8b2a-93e0962c0c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 13,
                "x": 158,
                "y": 39
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1c3b643c-c272-46ee-91f4-e407e977f9a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 7,
                "shift": 17,
                "w": 3,
                "x": 153,
                "y": 39
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f257f1c4-aec5-45d4-bd18-02d03a6034f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 13,
                "x": 236,
                "y": 76
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "144dd63e-fd5d-47b6-8650-15af0918c0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 200,
                "y": 187
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}