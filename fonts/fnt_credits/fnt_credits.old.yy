{
    "id": "93337d6b-ddcf-435a-9853-f0161fe92e7e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_credits",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7fdf5119-aa45-4d1b-b0b9-d35819e4ea0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "df6bc160-27f8-42a7-aa65-ad102c1740c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 186,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8f8749a2-eaa1-44ad-ac12-15e64e26f29c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 178,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d99325ca-ec5f-477f-bc83-2adf3ae0f23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 159,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9f67b63e-7625-4c5c-aab9-ad6a67651896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 143,
                "y": 95
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3d045b91-13d3-4ded-8bce-11e827a83d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 120,
                "y": 95
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a37dc2ec-e3ba-43d5-b0ff-fa4cd89767e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 103,
                "y": 95
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d74d2edd-6160-45d7-a6d0-3865a5c2448c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 99,
                "y": 95
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "50769e16-e014-4781-b109-11f007fea5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 92,
                "y": 95
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c2be784d-87de-44a3-94f5-76cec3888821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 85,
                "y": 95
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "92b7adf8-911a-4e47-91c1-d0cbd65e33a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9d3e6144-3e44-4ca7-b069-1bd7dfb404ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 72,
                "y": 95
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "83cd3ec9-8bb5-4c3c-8517-15801f444c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 52,
                "y": 95
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "dfccaa4b-0cf7-4663-ba56-c1f62e0fee3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 43,
                "y": 95
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "07a8207e-f1d4-4305-8c2e-0755548cb942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 38,
                "y": 95
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7b0e4583-cc5f-4950-be88-12030256bd6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 26,
                "y": 95
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f41ebc79-758d-4a62-ad28-f79d56e04fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 11,
                "y": 95
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "89ce9aa8-90ec-4743-b409-f3f4e4fda333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 7,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dcfccc01-bddb-4af3-ae47-9e7bca2ae8af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 239,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ab28e30d-17ae-4d0f-bcda-9236a7b59c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 224,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2e97ccec-0e4c-4583-bb61-ff1169b119b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 208,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2e880c3d-4bd4-4912-b04f-c5a3704d33ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 57,
                "y": 95
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9aaa3e30-d1dd-4cf1-b9b5-3c4049b2967b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 218,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "42438877-11fe-488f-b720-c1323e1fb933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 130,
                "y": 126
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "847d6199-6382-4c93-9825-43144d636375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 233,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "49a742d8-f75b-4890-8314-4f6c49338d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 25,
                "y": 157
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4531531e-c09c-4a22-ba51-0fc059016984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 20,
                "y": 157
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "11fb70b9-b89b-4b38-b575-877fc562fb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 15,
                "y": 157
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "088a158b-2854-4e93-b2a3-191b86382b62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 157
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bf6ce2ff-c605-42c0-8ee1-a0cf4af60c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 233,
                "y": 126
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c5720d95-d484-45dc-940d-217c10218c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 220,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8ead4ad9-7ab0-4f6b-8e6f-c6cc07fdbb4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 208,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c1518a24-6a2f-425c-993e-b8f04ce4dadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 183,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cffd5363-9104-46a8-aa8b-4d02bdf40cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 163,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1a3f0b75-2ebe-47ce-bc0a-1133069098b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 40,
                "y": 157
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f1554a72-e5cc-467c-ad95-3187f85bc3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 145,
                "y": 126
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "44bf9f49-c5c2-47b4-b3e8-07146398f4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 112,
                "y": 126
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d763a163-b33c-4d39-96cd-a471dc8fc94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 97,
                "y": 126
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "02d316e8-cb32-48f5-9220-a57297c72f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 83,
                "y": 126
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c91dae47-64bf-4402-986a-38061531702c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 65,
                "y": 126
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "80f8c60c-bf92-4746-a60f-ccffa7bdf950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 48,
                "y": 126
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c39cf9bf-263d-4e3f-8cbc-a578d6013535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 43,
                "y": 126
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8b600a14-e80a-43c4-ae36-2cce4932d506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 31,
                "y": 126
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f1db1c38-b11e-412c-8815-da1c3b21a5f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 14,
                "y": 126
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a30d9a42-86b0-4425-96f6-b4b4cf8ecb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b3e3e77b-75b5-4d1f-a429-9ddccbadd1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 188,
                "y": 64
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fd3c524e-f1a2-42a5-ab36-fd64a22bdae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 201,
                "y": 95
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "66c79a68-b801-48b1-9444-6b56471805e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 167,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "aa90206a-c38f-4791-bf80-c4b568638359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 88,
                "y": 33
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "668505eb-512d-4c2d-b977-5ebde89f621c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 50,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "acd0f9ed-be4d-42fb-98a5-2e13f31e9e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 34,
                "y": 33
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ca683938-d3c7-4d17-96c1-2e3c5d509536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 33
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d5c9da12-5499-493b-b1a7-e39f29883d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "990bcafc-b52d-4901-ba83-43a7b5f270b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "47bfd025-d5a0-497d-99a0-9838da19e26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7ea7fa34-953d-479c-b5ba-d19000b4564b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "58781144-1ce6-454b-bb02-c7d7c4156cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "777f6437-4a8a-41d5-b4e0-84ee0a275fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2c68edd8-a4e6-415a-9c9b-e21bd55b7a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 72,
                "y": 33
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "421bc4ba-9bef-4f6f-9b4b-9e9400367a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7696ad91-4b8b-4dec-b375-925f96b34530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "878a2a39-99c3-446e-9006-20b12111220c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7a567fde-d353-4247-ad4d-1192b9facdc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9e070821-ebfe-4911-8fd4-920952eb5d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c7f0ba6b-ea14-4a92-8c33-b031893c45f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "484c978c-62b1-47ab-af35-bd5e41dc82d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2db0c1ab-e17a-4296-8666-fb9bfa426196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3c947c15-9186-4b42-bf54-2daf2518b4f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "10271309-a244-400d-87b7-ce3bb1de0761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "acadf4d9-fbb9-4a5a-bb0e-761e8566b42c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a440c0b6-544e-4cee-8ba4-94db0a8c315c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 104,
                "y": 33
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "26ba237d-fbb0-4d30-b48b-ed9d645a0d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8ffed18d-3513-4249-8421-493ad5c3c6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 115,
                "y": 33
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "42e52783-9c7d-4ff2-89a4-460248fed66a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 143,
                "y": 64
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "91c8dea4-32fc-48f3-92ce-e6a9c392e98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -3,
                "shift": 6,
                "w": 8,
                "x": 133,
                "y": 64
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8a4f70af-bffe-4351-99ba-df65c8117ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 119,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f30fe3ef-e40e-45f4-99b7-45281b145eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 114,
                "y": 64
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "059d26fc-85b1-41c4-bacb-86790be51537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 90,
                "y": 64
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6754fef7-7201-4456-ad41-a6bbc44c1212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 76,
                "y": 64
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "04c6aff2-592a-4620-a073-dfad54e080f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 61,
                "y": 64
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "45c0864f-b3d4-421e-a2b5-1072491352a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 45,
                "y": 64
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c9a09224-f4a5-430c-b9bc-2b08445c3b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 30,
                "y": 64
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "87b6e6fb-73dc-4cf9-ac5c-353731a55bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 149,
                "y": 64
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "36184efa-9111-412d-b9da-08c584365624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 17,
                "y": 64
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d685b12f-e63a-4af3-be79-f06c5a98c39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 238,
                "y": 33
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aee77d4f-cca4-4bd2-84b8-67e4094cf43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 223,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "814241c9-1715-4268-ba56-03e4dde01bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 208,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "237e3ea5-73f7-4469-9035-930a96add12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 185,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d1655b0e-1dfd-420c-a15b-7829e754d8f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 170,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7580f38c-68a2-4b7c-b4c4-73928fff2c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 154,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "de399391-a26f-4b55-adb9-b39435e629ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 141,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8c8e1e98-d997-4a4a-bdff-4b50dcc4f845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 133,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6b217e08-a9ff-4948-bdcd-dfd345f1f357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 129,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e77cd91e-b415-4334-9e75-722008cb976f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 158,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "27578b43-aeb7-4f69-9871-e8ff9edb2c0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 56,
                "y": 157
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}