{
    "id": "93337d6b-ddcf-435a-9853-f0161fe92e7e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_credits",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "17777d53-a473-4310-82a0-2e0426ba17d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b022a210-5779-46f9-818d-8fd3397a73f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ed1da484-2e5b-4e33-883c-727df18d1ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 23,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d9061553-7029-42fe-985b-b70ddf3d4503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f762c1fa-730a-4c84-b4be-3e3a31254962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 228,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f6a2d8ee-31e8-45aa-9095-41d08049c4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 203,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4f3ae895-844a-4359-9693-e82c036ef89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 184,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f3bd1802-86c4-49a4-af4d-7d3083c9d766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 179,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9c97f9fe-e854-49c9-b8a0-fbafb3393ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 171,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9d1c7b09-51c3-4438-8d0d-63f6dec23193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 163,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8eac9abd-1315-4fb1-8d08-be5bfadfc359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 37,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "845dadf0-0419-47bc-95d7-21eb5a1b0ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 148,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c386e673-6e59-46e2-9f0e-47fa3377d3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2df4406c-3666-4b9d-b922-62c785963e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 118,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2e0db392-a8f1-4e4a-8680-a1c875258821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 113,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "acc6ff73-2b32-455f-9e56-8e8fc0f0a158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": -1,
                "shift": 8,
                "w": 11,
                "x": 100,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b83d3788-33e0-4fd6-9a14-f39589945041",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9040619b-379e-4469-b33b-88a8c36e35d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 3,
                "shift": 16,
                "w": 7,
                "x": 74,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "25addb34-95c5-4b05-86ff-9d464a556220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 58,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "76873fed-12f0-4281-989a-e88d17640864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 41,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3bc14f5d-a544-4753-81ba-6df9915e4e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 24,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cbd8379c-6034-40f3-bb04-e91cae4d959c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9b5b5834-170d-47a6-a334-4f7f0423ba3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 67,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b510ea69-bacd-48cd-8049-786f2a853070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b7594ef1-3734-4a0e-b897-ab8dffb2a0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 84,
                "y": 142
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3796b413-1d34-4791-9ae8-9f40912b96f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 155,
                "y": 177
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "40747f30-3ee0-4bfa-ac39-a32e5dafc273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 150,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9fd88b95-ebe0-456b-972d-59de56ab2b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 145,
                "y": 177
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2d0d6e09-3be8-42b2-aa4c-681f6bf05d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 130,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d3d955c6-0755-4269-b750-08ab45a26676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 115,
                "y": 177
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3e2959b0-e916-43dd-9f66-426e6aaacb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 100,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "69a0853e-0cbf-46a2-a4ac-a2ac3f4ebe59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0cec722e-df45-47b6-b60e-7499babd664b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 60,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c44484e9-0a12-4dd2-969b-2e0726e42a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 38,
                "y": 177
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d2292a8f-ce3f-4cde-8db9-a7432f4351d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 172,
                "y": 177
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "65bd6495-58dc-401e-a72e-f5273ca1a3fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 19,
                "y": 177
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6f1be7df-778e-49ed-9973-a33bb8605c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 221,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e87157c8-fa4c-4853-b370-76a77ff71a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 205,
                "y": 142
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b6a9b7e0-8332-4f81-bac7-08a6e3ffad1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 190,
                "y": 142
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a6926a4a-e47b-4622-94f4-a8ccc0a235b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 171,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3b2892e0-b9da-4cb5-9603-a1e9ab46f3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 152,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6c36418b-ddeb-4195-ba94-6f156d399c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 147,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c6fa5898-39d9-4146-b7d3-3908f1028606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 134,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fa0e3752-6aaf-43f0-9b2a-47337551f842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 115,
                "y": 142
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "71888800-d38f-4219-8660-24406cb05271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 142
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "58550848-1f4f-47a3-985e-8d28d192cd61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9915ee17-6f65-4b55-9ee5-c57c26029e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 48,
                "y": 142
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6c6ea8ab-9f82-4069-a762-59398b1adc24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 221,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a621abc4-9977-48bd-80c1-8db6a0919e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 114,
                "y": 37
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "071215d4-20a6-464f-9a05-a7a4280f7e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 73,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "23303d69-6ca1-49aa-b449-f25fadf8cb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 55,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3cfa8d8e-b0a9-4f5f-ac66-506e16f1bafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 38,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b6f751d7-c3a5-4fcc-8618-b5f85b876192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 21,
                "y": 37
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2eff7a1c-8772-4f69-a5aa-5a6dda09855d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "96e6c18a-d9fb-452c-a2cc-811dab1b7afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6af07640-990f-42f6-86c2-7b4c996247f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1e4df698-4cd8-45e1-b080-08029e224f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0e6c89c3-1b51-46f5-bb6f-6c6d13bf45a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "40e394b0-2a13-470d-b432-b8250b81d4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 96,
                "y": 37
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c7518e95-9c9f-4e35-ab4d-3a762ff9c4f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "70cff317-49c1-4592-b945-cf32804be7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6fde8ddc-ab9c-4669-9454-3cf834ae6cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3abadb36-1de8-4b98-a992-8262eedde5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fae8b1c9-0d26-4930-928e-62f190776736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "78963e43-3b6b-4b70-9a87-0ee07c91f1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "45e51318-da8c-4010-b70f-e260fb81d830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b0e42b40-d97a-4e72-b6b3-8fd503ec2118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "81273e8b-1401-4220-a2d2-b9697347b408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "52937462-c2cc-43c8-be70-49da47caf20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a71dd808-4efa-4143-8ffd-f1e11af773c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "effd9266-fe94-4cfd-8be1-3bb6afb1de83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 10,
                "x": 132,
                "y": 37
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "37b4dee5-8a47-4b5a-ae84-7f46e3b95b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 44,
                "y": 72
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c033ff9d-1c6d-4b6c-8b29-04ed61ac9fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 144,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "24626e1a-1b74-43a8-98e1-967c94b6fb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 197,
                "y": 72
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d77838ed-4ce1-4885-8fc8-df1899bfd40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": -3,
                "shift": 7,
                "w": 8,
                "x": 187,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f1b839c0-b1d8-4b23-81d6-342bf448ad47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 172,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "dbfa7b8b-3acc-4025-9101-5d12edd0a6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 167,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "055c5aa2-9dfb-4bc6-afa4-d5e0e5b86ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 141,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d851fac3-7bcf-438d-8974-cdff5139f689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 125,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3c062ccd-e1a5-4c13-ad54-5ee52e9b3be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 108,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c4e761a7-26cb-403a-9e89-5d1cb096955d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 91,
                "y": 72
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6b9ec96a-4b3c-456a-9954-61c7d5b48e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 74,
                "y": 72
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4b89924e-cecd-41ca-961c-b5f9632e261f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2cc1e3bb-ac15-43dc-9812-e9022a911cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 60,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0ca1005c-2b97-4b98-a2f6-e703f1275735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aee20995-ae59-47c4-8b33-918defe4ef33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 18,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c477dc76-e9c4-494d-a311-388752ba0238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2724271b-c16e-474a-ba1a-012fd0691087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 221,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7ce24745-19c9-40d1-9fb9-f2ed7bda8ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 205,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3afa4056-7231-42c8-ac81-71d9c6eb83ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 188,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8537e062-19d9-4a33-a5d2-d64c219dd735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 174,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5f98ede1-1dc3-49b9-a661-69485c0942ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 165,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "75dbd70b-efd4-4683-bedd-1cf3c8088712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 160,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "46de0e44-019a-4804-a939-ab2073c1a15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 212,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5d510f2f-4de4-489c-8047-7d91f78fc6ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 190,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}