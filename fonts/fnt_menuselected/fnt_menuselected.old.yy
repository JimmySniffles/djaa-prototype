{
    "id": "7d906c97-091c-485d-b37c-c58dc235a50a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menuselected",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "680368ce-2823-4627-9c01-3ed4af80cda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1f8bd640-d471-4d88-825e-f3c80294c8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fa2b68f8-ffc8-48af-8d07-d73d65a5cadf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 404,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "44484561-4457-4837-805b-9ff2b2e4a157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 375,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bf32f966-543a-41d9-aa5c-8b03fd8baf9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 351,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "62c49ab8-ab6c-446f-bd38-580c79eee4d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 314,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5d42cac6-b655-43dc-8f3a-319dc21be8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 289,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "159a80fb-d565-4e05-ae3b-cc01c2b21066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 283,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3881effe-7d17-467f-9715-38fd3f7c2df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 271,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "207131f4-164e-4a64-b103-55954c085bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 259,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cb990f97-b599-4813-a592-c11844a41f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 425,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1b036074-0fc4-42dc-ac4a-7aa90684b119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 240,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "89f34e27-dd57-4c38-a73f-7465a0576adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 209,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "31df0e3d-9fb8-4ead-9c1b-965d5a9e7512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 197,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f78ca9ec-cdaa-492b-a9d2-8f5edd8fc0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 189,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cfbe0859-3c3b-47ec-98ab-1f1f61b8665e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": -1,
                "shift": 13,
                "w": 17,
                "x": 170,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c2005796-5e1c-4845-8163-d212ded84247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 147,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e4e25301-1ee4-4567-918f-37769487c520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 3,
                "shift": 23,
                "w": 12,
                "x": 133,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f0089074-49c9-4a90-98bd-318b570f4686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "053194ee-d58f-4048-a3bf-27ff0b5b887e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 87,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5078141d-8599-452a-ba49-eb03aaea3a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 63,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ae6c8cdd-8ff6-4235-92d9-f2d49735785a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 217,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b9afa53d-38a2-4272-8e2f-e2a93cd355bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 466,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "134ac7e2-bbc6-4826-ba26-ddde9ca3beef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 221,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "73db26d3-61de-4b3f-b6ea-7452ef062fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1e4ddd85-1317-4768-a42f-f6c437acbd30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 429,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8a397434-2d26-402b-aa27-d1a27d99452f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 421,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2739485f-fd73-422a-b5a0-ad5f45a61dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 413,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6362ef32-1d87-437d-85b1-49c73f37722f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 394,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8e0bd02b-7803-4212-87fa-1213cd10f915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 375,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "559449b7-ae6a-40fe-b642-aa948b7d348c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 356,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "83999bfd-28c3-4fc0-a6e7-eb22b1e5bd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 338,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8b12727e-9021-4e8c-859d-1d025b213e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 302,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "51c929c5-5770-428a-a0b3-540ff66a0cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": -1,
                "shift": 28,
                "w": 29,
                "x": 271,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "48cb2726-8ec3-4052-8cc6-456e570346e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 452,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "47e55536-c5df-4bcb-a094-ce62dc4f1a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 244,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "80b09b71-5fa4-46f2-a5f3-153c76aaa047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 194,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "06c4dd3f-e951-4178-8da3-e07f9f7afb46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4e360340-a099-424d-8e3d-51fc4a42ad77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 151,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "98ea7836-fa5f-418a-94b6-c257784a18ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 124,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bfaa96b7-b618-4821-8093-fd53d2c511ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 98,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "75ead8b3-147f-4c73-88b1-dc0d852686e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 90,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bda8a08a-e418-4ce6-94a8-162c7bf0babb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "522bca4c-b3be-43ec-83b2-ff6f495c0bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 44,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "187600b7-cc30-4552-9ddd-e8cf89115fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 25,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f47eb0ce-d8c2-4f7b-a5b7-ff1f19020d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 32,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "92171fed-db6f-44dc-a3a6-055d4e0d4b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 440,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "341f91a5-8a8c-4f4a-908a-fd68142c65a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9d1b9cf7-af0b-44cc-b8ec-8813ead1b22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "878e1ec5-e355-4997-9207-6f6093c77f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf825319-a73e-431b-9d4e-93fa719bebe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "62de2570-5337-45d5-b3ed-fb359be98f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "012893b5-f606-46a4-941f-efbba2628123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8c8b43c3-058a-4f33-b864-f72d5130012a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0c9188b1-ac08-4406-b14b-9c19ae80b699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "62fe0769-1302-49e5-b5d7-9ac1ebdb8747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 39,
                "w": 40,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9af1cb7f-35f2-49fb-b08b-91806ddb4c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6b50187b-37a8-4818-ae71-98d0870f569d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6585e243-afbe-401c-a325-551c27179849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b6a38815-0475-481d-a24f-6e01b58c5d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e4df5467-89c6-4dcb-b20b-4aff5cd13b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "047c03db-c618-424a-bed8-9ee18a821257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 5,
                "shift": 17,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "201c3a41-d710-4a6e-a72d-5d03521b78da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "467fb5a7-d832-40c2-ae9d-e5364ee4b6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3669d78e-5c76-4fd8-a57e-b5dc194ace43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "48eb197a-9bf6-4069-a574-bf569e28b4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6588248a-ae00-4498-b639-9a5e4f59d913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8c054371-2b29-47a2-bd57-b45d2dbf4c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0161cff6-cde9-4c19-97fd-53d68ea0bc0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 22,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0a6fe3dc-50bf-4cf3-ae94-0a8048264345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a52a7385-8ffc-4ce6-b872-5fe8cf43965c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 15,
                "x": 26,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1b8fa28d-5ed9-4c31-abef-d6f9f0cd1519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 251,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cc4c13cc-0f0f-48c0-92a2-91e89df60f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bc23bb9f-bfd5-45d0-8734-97d1195399fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 468,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "93c53182-c700-4c28-a6c5-d4a92fb19c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -4,
                "shift": 11,
                "w": 13,
                "x": 453,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8b1ec2c6-1b75-4d4d-85c0-4bcc07bb3856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 431,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9f67b57f-9659-4cc0-9291-8c12d1da5036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 423,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d6c8cf97-a7c4-4e2a-a4ac-74c8f42b7fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 3,
                "shift": 39,
                "w": 34,
                "x": 387,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9a9548ef-483e-4dc5-a459-3a5259a014f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 365,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "319472f0-6266-424d-8782-ef6f669fe171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 341,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7cbbbcc6-fd47-4c54-96bf-0c0601ee0528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 317,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6455ee96-b089-4888-872f-d5b6581f4e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 22,
                "x": 293,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0eca9226-e86e-445f-965e-86026a4c0cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 477,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "de97f2cc-6bbe-4696-a299-51ea9f2b9ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 274,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "97387f82-c333-4f52-9bbc-5a608aad76c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bb2bc6ea-717a-4436-aac2-a90f30edb29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6eeafd2b-f2fc-4734-be6f-486879cb0508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bd67dc0f-f52e-41e1-bf3d-8de8c2ea1794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": -1,
                "shift": 34,
                "w": 35,
                "x": 152,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0fab1eea-b519-4904-8d41-ef472b072fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "06573b1a-5de0-4cf9-b0a0-3de452baf1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "87a9faf3-adb6-4898-b645-12899f1782d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bc02f139-4229-4ce8-b256-0cc342e21013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3498d57f-f757-429e-bf82-a6b9cbaa2507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 65,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "68063eda-fa2b-4a70-86b5-df5595cd6353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 4,
                "shift": 15,
                "w": 11,
                "x": 491,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "44d9ab67-a88b-48f8-bd27-2048d967115f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 476,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}