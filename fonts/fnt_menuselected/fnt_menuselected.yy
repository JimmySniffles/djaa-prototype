{
    "id": "7d906c97-091c-485d-b37c-c58dc235a50a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menuselected",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7ed173ec-3e42-4074-8016-ee5aaef589ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8affb6a4-a57d-4b96-92e0-7c836e6ffb0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "91855ab1-67e2-4820-9723-58dfc9361152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 404,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0f3b4477-aaa1-461a-9623-ceca1c97a188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 375,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "09d5d6cc-5899-43c9-ab4f-8626b6c7d337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 351,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "af839752-c6ca-4ca7-be91-849425caef97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 314,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ffbefab0-c2f2-4829-9931-41f67e6d4844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 289,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "516b04e2-c98d-47d1-a79e-38b86b877e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 283,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0d7c75ed-20cf-421f-9893-816d92a2299a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 271,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fe473866-cc86-485c-a3cf-5bcf3e3f935b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 259,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cd184f93-d486-4aee-a5fb-904deca99a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 425,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f8372654-bf22-438d-8ec1-da72723dbba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 240,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cf50ad14-c012-4ba6-84da-471df88a81ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 209,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "29c5dff8-10e8-4dda-907a-8f76bb5560a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 197,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0dc351dd-8e57-454e-a1d9-4766b2389609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 189,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "599b501b-d235-483d-8088-bf65098dc218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": -1,
                "shift": 13,
                "w": 17,
                "x": 170,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ce510760-9187-438d-89bd-1d88bf709bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 147,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3be9e4a2-fa09-43de-9379-79505d5b0d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 3,
                "shift": 23,
                "w": 12,
                "x": 133,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c9581b6e-1623-4420-8840-9ffcc2da4739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eaa2ddef-ed9e-4087-8a51-ba7aff55c560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 87,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ba79f08f-ed63-4e73-bd23-6a7e4444e004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 63,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9171e0d5-8738-42a1-85c6-79da0c5587d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 217,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a7c62bd1-b6cb-4bf9-9366-a9164126e585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 466,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2baf613a-8607-4295-bc81-9f7dea41294d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 221,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "316c06e5-9462-411f-9045-4a9450bc7022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "af1ff059-a72b-4a21-89a4-085892a09241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 429,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e9dd7507-5fe4-48a9-b5a1-02f1bb832068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 421,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "804ae9ee-da92-4541-a62c-24aaa51689af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 413,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c4ccb19e-5e62-4814-809e-2f38c6b2f591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 394,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d003ad00-6a53-440d-a30f-1518c3cac867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 375,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "62f21db0-48e1-4aa5-94dc-c6373eefd66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 356,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8969e8fa-2933-4f22-bebe-97455d277f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 338,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "06a05422-290e-40d2-b0b0-f38c14474da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 302,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a7ba8e77-9113-47f8-9093-a2e8741a943d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": -1,
                "shift": 28,
                "w": 29,
                "x": 271,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2880847d-2a85-41e9-9b7b-4feb98b3bd67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 452,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "eb978df0-2c3f-4bc7-b10f-b8497c45842e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 244,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "102949cf-a813-42f6-9bd0-c78e8642ceac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 194,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "52eecf1d-5729-4143-b3de-c655a942dc4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b45d0bbd-c4d8-413c-91e3-abbc4dd3723e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 151,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "91cabacb-7dc0-474c-a82a-1017f0128c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 124,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eca8c3eb-1338-46f4-916f-906b0047e45a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 98,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5d2a4501-6ca5-4623-b955-4f6253e86ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 90,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6da08833-6be2-4022-8a10-095aac9acf31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5f20a3dc-3e50-4e50-872d-ff285efe4fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 44,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e2aa929a-641d-4980-a87e-c68a5320957b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 25,
                "y": 146
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "58eb7089-e883-483e-9434-d25cda19ea71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 3,
                "shift": 34,
                "w": 29,
                "x": 32,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "08e0051e-89d4-4cf2-aa8b-9b731c2ae826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 440,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "40f9a607-9daa-4457-86e6-c7f7304b536e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "158f3c85-f50e-4754-b0c0-1f2bdda5d7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e3e31fa7-e6ae-45fb-934d-fc88bbc18f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "928f21bc-0141-44a1-8f0e-cad5899a4616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "187b08f7-3b7e-41cd-b0b7-49854ed5457c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ca091b82-35ea-484b-a2ed-200b94dcff7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e0f35f69-0026-4e41-83e5-1fe3dbefd3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "479d4fca-bea6-448c-8ea2-414872d33bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4b281a89-56eb-4aae-ab14-8e2c13dff91f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 39,
                "w": 40,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5a00b45d-d953-477c-9d6b-f3530d71b4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "80a2e7d8-a5e2-4132-9076-c0d97256bcf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6f4a54db-e4ae-43a2-a463-6c8f3199f4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d330c04d-736f-4391-bdf5-7997d34401bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "76d62b16-661d-42d5-8856-943dcb831ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6c20a9a9-b49a-4d3a-92a8-a7263f672590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 5,
                "shift": 17,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ca2d5d4c-f07f-4c4a-8652-20f47192d012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "24a5f6bf-e929-4d3d-975f-50acb750f9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0d4d2ef6-2bc6-4046-9432-0106c89eaaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b077987a-7043-49f6-b1e3-ba2ea82119d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a26e12df-2c59-4d71-b117-eae58629dd68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8488a006-060a-441e-8df2-e983a2d9c36f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4643543d-eb57-49df-9e59-549d365fd0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 22,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "36c9c4d2-fb48-4fad-9983-16f9f5786e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8648859c-a379-4131-a37a-b239beddfb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 15,
                "x": 26,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ee3024b7-e9a0-4549-afd9-1e102faf43bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 251,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c3450717-22a0-408f-960f-2827d2752276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "00183144-ad3f-4a06-a37b-8730e5f5c5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 468,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6a269dbb-7f72-4d4f-b828-50bcb31ebfa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -4,
                "shift": 11,
                "w": 13,
                "x": 453,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4266df03-5645-45d4-97c2-2139256a7306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 431,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "df716390-46bf-45f6-83f7-bfaab3feff8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 423,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f11b1017-9262-4a4c-93a3-5f04bcf22c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 3,
                "shift": 39,
                "w": 34,
                "x": 387,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8eb4d8cc-4ffa-4e35-b8a9-082ecff3d60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 365,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5facf9eb-5786-4e93-9ebf-587c986e08c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 341,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "01fed902-f235-4dd5-9443-cb4471138d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 3,
                "shift": 25,
                "w": 22,
                "x": 317,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2ddbf724-a310-4515-b155-867bd858a23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 26,
                "w": 22,
                "x": 293,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "13de5ff8-6202-46f4-97b8-a5a3704014d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 477,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8b01b4e3-ec4c-4efd-b176-7437dc3b793a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 274,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a81ec7ee-5d4a-474c-a1c5-8531718c60f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "490f3ddb-5feb-4a83-bd5c-7d4026e4b7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "edb899c8-ec65-476c-b55c-9fc9e9c0417f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "efa98fd4-bd2e-4d98-8d6e-f01bddbccf39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": -1,
                "shift": 34,
                "w": 35,
                "x": 152,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "216c07a1-37cc-4a53-820e-6f4705b7361c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e62d73e5-083b-4cb0-b158-03df4f34f7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "87f93787-cc3e-430e-8a76-4a992beb0f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "61cd9897-05ca-4394-ac59-f4b4f37425dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e5284caa-7864-4127-b943-7d27fa3df0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 65,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cf7e1833-6381-415e-92fc-7153ec595a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 4,
                "shift": 15,
                "w": 11,
                "x": 491,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "126fe413-a2ee-499c-8555-266279db2055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 476,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 28,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}