{
    "id": "7ab6f062-c398-40ca-ba33-16c316498468",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b9d6a4f5-f84c-4d9f-b55d-f80504620e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "00c1dd50-5811-47cb-bf81-e742175cf433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 167,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b34273c1-b204-411a-adbc-53fead42804a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "db2e0e9c-0b7c-49e0-a2c7-7afe1058b5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 133,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a444b40b-b7d9-414c-a767-bd711f9110ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 113,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ac8efb08-b1ba-4e0c-939c-92aa1042e39f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 84,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "593a5495-907f-44da-9c5c-7364b7f8470d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 63,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c6ecc23b-4fae-46b6-9b4f-dae11a0d0527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1c2f4b0a-fcd5-4673-bc1b-0388d165dc4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c32122c8-d933-4239-b299-6559519db791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "629d4a26-e09a-49d4-99d7-5a5184f2b716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 174,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e5d0dd20-e672-475c-8e21-ceb34b511df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 21,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "66a8a1f1-b450-4e7d-b21e-c346d1dcb477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 500,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d51dd9f9-b61d-4277-a1e2-23c481ce32fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 489,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "657dc0dc-d021-435d-b456-7aeb6b1905ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 482,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "be8b912b-f885-4938-86fb-35ee6e2ad0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 10,
                "w": 13,
                "x": 467,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f963311a-1c32-4741-ab6a-53999ea2b58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 449,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b82acd0d-62a5-4b8c-b241-44e0a827a15a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 438,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "45c942da-2613-4f27-a6de-e90b5347d35d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "77860de5-ddff-4833-8dc8-69adf5b8479e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 401,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "43a510fa-504b-4f50-b7c5-20e94152a6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 381,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "df86a816-1c2a-4cc4-947e-df627a2a6731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1f83389a-3603-4600-88d3-c0c22b7670c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 207,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "50819583-2480-4ca2-ac9d-291c31f09a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 405,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "623326d4-c1f4-4d39-bc35-fee27653bc92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 226,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "24a2da8f-d102-4282-98f3-629451776581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 78,
                "y": 116
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "52c9610d-e54e-416c-b5e3-a8e678fab46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 116
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "926d43e2-697b-4bbb-99c5-161993e13e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 116
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e2fda3b2-b0a3-483f-984b-11ef6e7c5e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 116
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2b841f82-54a4-43fe-a0cc-6c6c4c57c8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 116
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d83536e8-de59-4371-ab78-35cd85cbccfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 116
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7c4c7523-bf10-40f2-8b26-f64b2ed3c954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "91426f9f-2fa9-4cb9-ae34-f2aca280d1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 471,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2a5d2d9f-00ad-4916-985c-ec2f4acb591a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 446,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e2403baf-d277-466f-bf99-33455203ae89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 96,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "befaa5b9-9817-41e9-b47b-a49eaed0ddfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ae253a0c-dca0-4cc8-90a9-a1ef5c3cbfed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 383,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e46e2f4d-9c39-4f73-ab63-f36bee2181b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 365,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1fdac8bb-537b-480d-83d1-e3f802e196a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 348,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b069cfa7-aec6-4264-b3d4-f0c227ccfa38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 325,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f67dc712-29e5-4424-b6a7-357b9ce471d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 304,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "357dc083-1c86-419e-867f-db125166e151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 297,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e5313957-7d56-42d8-8ea4-6edb1cbea052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ccc91c55-5ce0-4e50-80c2-2c94054c143d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 260,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7a03c188-b83f-425a-99ef-f9230ee06c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "23201253-6dde-4276-8a9e-959d44e94235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 356,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a4c71d10-e298-45d3-aad8-809b9166d453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e0f6518a-577b-4e27-a449-b2fc994eeae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 331,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6df2d435-fa19-4101-befe-d60d3ae6b634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "29eacf43-61d9-4446-a6a3-deb1a99f3213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2c7b1538-dd37-4677-8dc2-4a3b5e8e3783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "68dd06b1-7698-4de8-b043-6909ab99a8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d05df909-4437-4c9a-a1a4-028a2539812b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "71ad4015-3ce8-4070-bd45-cd102f093e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bef9b445-a118-444b-8bf0-b290942ac183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b82811b6-0859-434a-89f8-8c46461d23a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e4dfa749-f69d-4cfa-acbb-a40ff03ca6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1897b1c7-5ecc-4b32-8b3b-e06d229962db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "46721aad-29e0-45ba-94d5-8de83d284ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "baa796fa-6dd0-41ac-ae2e-383726f3927f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b9565f1f-cd3f-4dbe-ba64-c8fa27249a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3e47b794-cabe-418e-b022-e6b1f868bee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "689f01d1-8b38-4153-83c4-bd0892207ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f3cf2771-9375-4077-9d4b-a9d693df9f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f520d42d-818e-4d50-acc0-fa9c76e573fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dec91675-073d-4243-83a1-0ec1c23238ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7b4a6e2f-8f76-4b7e-b25f-60aee060904d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "296ffbb0-3d32-47fd-a489-0cdcce1cdc5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4f7ceb8b-46cc-4324-92ce-776be1197c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2343483f-bdc6-4d61-890f-7efb3138d985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7c176f37-c052-4dbb-9dad-0f95df51f017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 12,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d2529108-2f44-4824-b0d5-1134a420dc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 122,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "25564e87-f835-40fd-a2f8-1b20490749f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0ba83920-7a7f-45b7-afad-d6c8b7fe80d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 301,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d98ef3d6-85de-4d2f-b5db-d7204124d243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 9,
                "w": 10,
                "x": 289,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "90080f20-2ffc-4959-819e-aaca8eb526f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e8aa925b-45cd-48aa-a176-666068e9b6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 264,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "41d4ebbb-1eff-4ddf-8aac-127247ce4280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 235,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0172b85e-c22a-47a0-b4de-7637c1191812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "79f7e48a-86e3-4873-86a7-955e45c3eca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 197,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6aa77612-ebd2-48b6-ae03-983d3691b55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 177,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d892f04f-e964-4fd5-b5fb-87cf520f9b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f3d6278b-ee46-4ad5-84a8-67c9d998d5f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 308,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "85db174f-65ad-47c7-ba67-69b2053f7a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 141,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "36b6c1e5-69d7-4ecf-8b39-7ec0363973c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 108,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "cad100f4-e700-441c-8e05-13f220a81269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 90,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "72d4681c-37d8-4460-adda-1c0e3acb0277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 71,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f7e01e4d-817e-443a-b845-dcbba8974acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6b1f6124-00ec-4326-8c44-97cce07f92d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 22,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0308e755-9f18-4482-bf73-6907c3cce86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9cc7bf50-4111-45db-835d-c0dc4eb4d662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 489,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5ff06fb3-ec9d-4411-b216-f1a76d39e6bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6f0bdccf-f6e2-43b6-b399-b1d44d4af740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e5bd5919-2fda-4909-a53b-26b28e48ebce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 320,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "47a250c5-7916-4e86-b2bf-536432adbc87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 116,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}