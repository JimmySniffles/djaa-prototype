{
    "id": "7ab6f062-c398-40ca-ba33-16c316498468",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene_thin1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a8f9086d-30db-4207-98f1-508645ac5a54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dad931e6-9cba-4039-9f73-19cfd9e7ee86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "658f4bdc-7702-46af-a7e2-8e343ac3c7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3a397d86-5426-47d2-8554-27bf8ce01605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 69,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "36c66e16-8a55-412c-a79c-a3397a78e18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fc8612fb-9970-461e-94c2-aa3f3ee3cffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5e9a271a-47b1-4058-b91d-b5882cccc57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d786467a-4127-4ab9-9de0-2a2ccbd4d757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 487,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4435876f-63b8-4ce6-8162-d84e7d6d2219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 478,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6c80f135-15fe-43f1-bb35-5f014241cebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 469,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9d6a4f77-dfca-47ea-8937-14d4feb750e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a9e0a093-c60c-41df-9120-83cbb231c476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b0c45d2e-0ea8-4fd9-860e-de3a33200776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 430,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "31984ceb-1271-4e8e-a151-8a17d1f3349c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "766a3074-3bb3-45c1-87e5-c25afa0f88b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 414,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6d36274a-ac1f-4ec0-99a1-fc4d9ab05ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 400,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "912e69dc-9a18-4a6e-97fc-0b297aa9319c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 382,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6dd7063e-a177-4450-80fc-e9c519f5d179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 8,
                "x": 372,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e8f053dd-4a24-48b4-8cae-0d571287273a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 354,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2b70c0f4-4b39-4fc2-a629-ab5a0970e180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 336,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5852ff6d-d6f9-442b-9c54-ad26094548b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 318,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "903e5e25-42bc-45b0-aaf4-15aa11542e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 436,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "048de0eb-d9fc-462d-a834-23395a5a28ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 136,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "174bee66-e834-4e08-8f13-4042663c6de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 319,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "77f4e4fb-efc0-48bc-bdf8-c24f67954a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 154,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "412eaad3-6878-4f57-9c9c-5007b7f67806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 481,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "92549e6c-7bf4-46cf-a57f-948629df6c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 475,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9a2fea23-4ce5-4de6-bab8-4aa184cf9bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 469,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aa1c8cc6-0604-4973-9390-4ffcd8ea7357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 454,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "73c2311d-eb85-40db-95ab-b331f135cff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 439,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e300c0ae-026c-45a9-a111-3a05b9d654c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "89576b12-2088-47a3-a700-085dab8b64ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 410,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ec313375-7d1d-48ca-873a-319209fe5a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 381,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2496a842-5049-4307-8c2c-7780d83aad92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 358,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9a04a0fd-ce87-4c33-9286-bd06d00daf92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1680cbd4-885a-44df-8ee8-f47c4c756156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 337,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c70f4272-c0b7-40dc-8864-521e275df754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 298,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4b0afa54-da5c-4c48-b3b2-cce503fd29c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "be84802d-c0b0-4a2a-a6c7-e825d0d6b0b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 265,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4b3dfd5e-9020-4a95-a2ae-02ece156cd7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9691d6f9-e594-471c-97e7-aa58156e58c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 224,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "91759395-80f2-4921-bbb9-58345f8a9e6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 219,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f5d2a26e-0bd6-4f9b-98b9-9cb3029ef2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 205,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "209b7a54-3c09-4caf-9a2e-3340cbf843b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9ae853de-71b5-4931-bca2-de4d62c0f32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "eaa51a21-6d83-4b7c-82db-43ad15ae6d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 295,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1a4b1804-715c-4e01-90d5-5a5b8d17fd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 116,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "33489e8d-1878-4858-a714-6393a6138e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5af2be6b-0c7b-4c10-b5e0-e529aabf97d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 393,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9d05c458-4f7d-44eb-9ae1-2d9a4a6f2f5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ee1b361f-573c-4930-a2e3-0a9eff18e0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 3,
                "shift": 21,
                "w": 17,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ae6c7bd2-14b9-47d0-bc7e-aaf603f672c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 310,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d93c2798-9783-492c-834f-d872ca32edf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 291,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4338ad1d-6dee-4eae-90af-5b20496ea4c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 271,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0674416d-95f5-4d73-896b-9922c01947c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "611213f7-816f-430c-bc07-7e38fb4e9bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "56809a8f-7038-49b6-b7bb-2b75c51ca8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "274601f1-508c-4dc9-81d1-18ce978aaa22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "63b8d3b9-ea5b-42f4-99e6-1ba70a520618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6c29fbce-1cf9-4b5b-a06b-5a88acdb1a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6e3f4e5c-2ad0-47e6-8133-854435415b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2d8aa2ed-9c06-4e52-9279-30a228813b45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "119e6b37-b140-4cf9-9bf3-8abfba95b0f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6e312b8d-70bc-4147-8ae5-563123e573f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d7780b46-6343-4325-95fc-d8b690b806bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7fe6cba2-42d9-4a40-85d5-85be6f56f098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8801dea5-714c-4ba3-9c20-b312a6ca4fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4cc13894-8387-4c58-81ae-935439369b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8f67ca92-9910-4f80-aa72-27ca0cb1a4af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "45d737a0-0051-4864-b8da-e415db9c8d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a370608b-b46f-4048-b938-30c070074700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "119a0cb6-cb9d-4232-8eb3-3e511ccd910c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0b1e30d0-1e48-492b-8ce4-7c5ced1d2bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a02d1dbe-62fa-4124-8bf3-28fd6ac60b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 244,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cbf08135-9a5c-4944-8186-f1e23e5e8350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 8,
                "w": 9,
                "x": 233,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0b0a4537-e6df-417c-814a-68c18d315cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "478fe6bd-7d51-497b-a669-04cba40079b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fa1e8312-118f-45bf-a329-d0c76e91385f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a7955aa3-2f19-4429-8d3a-38dc124c25bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 164,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "576a5cc4-07be-4d2b-b140-90332dcd54d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "64f884ce-1f35-48d5-b8b6-0f09073b0f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 127,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2d13c368-fd9d-45bc-9b72-b9df4c2d7f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f4f86e8c-5c4b-47fc-b4c2-5d434a91fea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 250,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "aa391064-599f-42b3-bcbf-24415efeb351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1f6e2ab3-7e27-447b-9cd0-956239c0d156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ea0fb199-e060-4af2-bb24-b149d1e6a1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 47,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "57f9ae70-ee33-43aa-ba41-eb1b9989b676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "36fa7e92-6024-4600-854a-dd189ae102fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8755a4ef-df58-44f0-8489-77b738641af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "21e7d08f-ba5d-4215-91b8-7db0914ea3e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2192094b-9ae1-4f29-927b-871bcd9a0a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1a9c41d6-66d5-41fb-bbfb-2babaf1b08b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "08f61ebc-5f41-477b-a409-d281ff26fd53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "394f7076-3363-4a9f-b204-bad5c171de82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 261,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "91901f76-bf85-42b6-8d31-8b59d0c7a809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 21,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Light",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}