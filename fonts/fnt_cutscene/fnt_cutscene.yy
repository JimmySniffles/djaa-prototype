{
    "id": "7ab6f062-c398-40ca-ba33-16c316498468",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_cutscene",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e66bd511-7e7d-4f81-ae58-1f20755a91dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d1f76934-6267-485e-b08e-45c2719a7414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 167,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8d9c6f73-5416-438d-8621-6eb7adbe3a24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2eabc10b-22eb-4536-87b4-248fd6645a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 133,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ad71386f-3946-4079-a1d7-a64672b445ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 113,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "06969824-860a-403a-8fe3-5e23a7f5d941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 84,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "cb466923-0641-4711-9147-c137661b8c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 63,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4502112c-9eec-46b8-9808-3fe9297b8425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6a71b1dd-6a86-41a4-9ccb-a512deefa1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8ee39c8a-6fb2-437c-af10-6944ae5d4c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "672411de-8e43-4ba9-a5db-c9cc87660bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 174,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "732f28b3-5c2c-4958-9c46-69e529b32522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 21,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "48b10bb5-a45a-4c94-b3dc-8b8494f1c4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 500,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "dd612784-703b-443e-95dd-45f47b9e23c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 489,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bd267d53-ea51-4365-9521-79b66a3e6e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 482,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6853092c-093e-4164-be33-a103e5108aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": -1,
                "shift": 10,
                "w": 13,
                "x": 467,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f3b13971-10f0-41ff-8674-74330dd10665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 449,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "75f012fe-a408-4674-b216-8f8a53dc929e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 438,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e1682f55-3762-450d-b5f2-6ef6ad1df03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 420,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b7ae4f6f-4f49-493d-94c8-e5953ce800e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 401,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "52a01cc3-dbe4-431e-8353-88fd43b35ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 381,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5d07b9f4-a63c-4c9d-986c-10ada577ef3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ae453a00-5a92-4b56-be5e-1be6c52be05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 207,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2e7f955f-56b1-4ed6-bb4c-79c9937c4cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 405,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7e2340cd-6d45-4580-afc0-d12d873c13c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 226,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c0e50814-59d7-4266-b6c5-207ce32cdfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 78,
                "y": 116
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "93f65016-4963-41bc-9b58-cc4f6b785d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 116
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dee62898-4686-4068-af63-8dc4a9eb83ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 116
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "df338d35-0e7c-47df-bb02-dcdcb198db04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 48,
                "y": 116
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bf4737b9-6613-4318-8cb5-a59a2265eb5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 116
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ae330dfb-a1b3-46a0-bcab-aaad93f05418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 116
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fda56a15-bda1-4db7-8b16-5b924ea4006e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ce2e9378-5cc7-458a-a68f-8a4f7fe9128e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 471,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9da1c6c3-5846-4b8c-b81c-5490a63aa8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 446,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f01f678e-864a-422e-b281-92ececd8c2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 96,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8a23ee64-0109-4bf4-a91e-773e60fb8988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 424,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f6807669-3b37-4b6e-92b7-373aef8b9b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 383,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7c0a5a70-755d-470b-8ad1-cc039e0b3654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 365,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e6361ecc-7d07-4050-b213-30258a82ebda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 348,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5f7daac9-847b-4ac5-9a7a-6d7723051763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 325,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1475653b-6a2b-4428-9d0b-093aebc204b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 304,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d33d23fc-8fc2-4548-b0ed-e24bc52cd2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 297,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "641dd771-d5fc-4765-82c8-81b1802a4c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 281,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "05d801a7-5185-43b2-9562-0cbe83682b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 260,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "99bfb810-01b1-49ce-9cbe-5f9542efdfaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0e911959-75ed-4672-a2ce-7ced6c95bb9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 356,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5bf02f43-7ec6-44bf-aa77-e6cf639058a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 186,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "110c8aca-2b01-4186-af9f-5b093c3b0ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 331,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ab2c9682-9180-41c4-a91a-2ccef22aaa1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "246902d0-8f9c-4e64-a33c-fc4b4d2d4087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b929c67c-20dd-4f68-9e03-2fa17185f7f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "40c845c0-7218-47e2-96ef-eaa7f73e557f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c7580a91-f250-41b9-b272-43edd2646cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "02978603-a702-4c16-a4ef-97b65b14a6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d7df3aed-9727-49e5-8aad-644b4e298ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "21b2e040-367f-4043-aba9-fab8a2ae195e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7ebec2f1-7f60-4d8a-a290-0b7e198e4f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "340cc520-769d-4c84-92b3-bdffe9d3e8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c876cf35-196e-4bc3-ab43-90d111a07f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e1644461-4421-4082-a549-715611319474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3a11c864-291f-49b2-9c6c-0e55fc339791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3943fa75-5ac1-4fd5-9298-645c54f27376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2bfd2d84-8599-442a-85ac-2b24b54a0611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5e32d62a-46e1-44ae-ad14-9b82db4f2a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fa8bb199-38fa-4ab9-ab1d-03fd0fb661fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8a2246b0-507e-4288-8ba6-fbe2ddc157f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e58c7e85-f4a4-4075-a4df-05c3dbea1818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8557aa1b-8de7-408a-9ad3-deac41bd86a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5edde397-52bc-48f3-b97b-21add24e0206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1634e328-d5b2-4313-8cdb-18cc21e39cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eee2a9b3-a784-447b-8849-a4b55c83e10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 12,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0183a267-0854-4ab3-8e56-16e79b3a15d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 122,
                "y": 40
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "721cfd69-3d54-4ec9-a9d2-62372f736ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b9816e91-96a5-4952-b373-ff3268b3bdea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 301,
                "y": 40
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7d2365a5-71bb-4d19-b500-449cdad418bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 9,
                "w": 10,
                "x": 289,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "be536530-8ebd-4cb1-8dc0-5d2d5e9ab025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 271,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6a15582a-a808-4ed5-a29b-5bc0d0b615e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 264,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fcb5195f-6004-4b84-8c09-60a845a89d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 235,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1655526d-e642-4ad4-937e-6281c0f207fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f32ede00-261b-4a19-9552-e1b50ebcf5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 197,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7a9e7869-8bda-4509-922c-f3828854d7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 177,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "51234d48-281d-4281-84fd-4eed4908d3cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ae23c51e-1242-43e1-9982-128370f38ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 308,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "14cb5eaf-04c7-47e6-9370-ed13c75b74c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 141,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "702fcf93-3153-472d-9acc-76b618424b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 108,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "17054675-48a5-4f4e-9a46-f43bc154e897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 90,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "718d26e6-9663-49a2-a283-569cb28d47d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 71,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "249d91db-92ca-4149-8c65-77fb7a12c7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1d4df67f-793d-4e53-8fec-57454ae5d0e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 22,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e912de97-2cf5-4d37-8d2e-e2deaa064583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "045d7c69-71ef-485a-8ffe-3baf8b3ef0ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 489,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "810f275d-660b-4f7f-bbfa-604b844e7f50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4b81fd4d-c68c-415b-94c4-67ec2118046c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 471,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "91dafa42-6053-4404-9b7e-262934bc8f62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 320,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b01ff997-ef0d-49db-93a2-f172a33e4d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 116,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 22,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}