{
    "id": "11e542f5-70b9-410e-b3b4-8766c2252b8f",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_berrybushes_2",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 6,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "6235ffa9-0c9c-47d7-94c8-218b10354fd9",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 32,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 64,
    "tilexoff": 0,
    "tileyoff": 0
}