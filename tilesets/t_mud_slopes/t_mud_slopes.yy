{
    "id": "a70a615a-6b29-4a8f-927b-0dfad56fc0c2",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_mud_slopes",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 3,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "f751c876-ac13-4443-bd6a-ebb1457967d1",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 12,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 64,
    "tilexoff": 0,
    "tileyoff": 0
}