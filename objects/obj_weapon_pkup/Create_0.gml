/// @description Setting Collectable Variable And Flash Variables

#region Intialising Variables

	#region Pickup Variables
	collected = false;
	scr_shader_flash_int();
	flashtimer = 0;
	ui_y_offset = 25;
	image_index = 0;
	image_speed = 0;
	if (instance_exists(obj_boy)) { player = obj_boy; }					//Checking if boy player object exists and if so it sets player to obj_boy
	if (instance_exists(obj_girl)) { player = obj_girl; }				//Checking if girl player object exists and if so it sets player to obj_girl
	#endregion

	#region Weapon Type Intialisation
	if (weapon_type == "club") { weapontype = 1; image_index = 0; }
	if (weapon_type == "stunboomerang") { weapontype = 2; image_index = 1; }
	if (weapon_type == "spear") { weapontype = 3; image_index = 2; }
	if (weapon_type == "killboomerang") { weapontype = 4; image_index = 3; }
	#endregion

#endregion