/// @description Collection of Weapon

#region Flash Timer For Shader
if (flashtimer < room_speed * 1.5) { flashtimer ++; }
if (flashtimer >= room_speed * 1.5) { flashtimer = 0 whiteflash = 1; }
#endregion

#region Collection Of Weapon
if (collected) || (room == rm_level_1a) {
	audio_play_sound(sn_weaponpickup,5,false);
	with (obj_b_wloadout) 
	{
		if (ammo[other.weapontype] != -1)
		{
		scr_changeweapon(other.weapontype);
		ammo[other.weapontype] = -1;
		obj_boy.weaponpickedup = true;
		}
		else
		ammo[other.weapontype] -= 0;
	}
	instance_destroy();
}
#endregion