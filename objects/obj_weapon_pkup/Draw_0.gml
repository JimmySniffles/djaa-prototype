/// @description Self, Shader & Collectable UI

#region Drawing Self, Flash Effect & Collectable UI
draw_self();
if (whiteflash > 0) { scr_shader_flash(0.05,1.0,1.0,1.0); }
scr_interactable(x,y-ui_y_offset);
#endregion