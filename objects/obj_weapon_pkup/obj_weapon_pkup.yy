{
    "id": "f768c819-da4a-47bc-a791-4afb67e37c9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_weapon_pkup",
    "eventList": [
        {
            "id": "4ac57ad1-d33b-40ba-a1c8-f380af89f1ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f768c819-da4a-47bc-a791-4afb67e37c9b"
        },
        {
            "id": "bda29e8d-4063-43f3-89fc-2562be3702b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f768c819-da4a-47bc-a791-4afb67e37c9b"
        },
        {
            "id": "24684fa1-cb15-4e7b-ba5d-9a574cc6cdeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f768c819-da4a-47bc-a791-4afb67e37c9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "37fc8d74-e646-44ae-9008-55e6b7ad5d12",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"club\"",
                "\"stunboomerang\"",
                "\"spear\"",
                "\"killboomerang\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"club\"",
            "varName": "weapon_type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
    "visible": true
}