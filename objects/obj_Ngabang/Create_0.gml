event_inherited(); //so it will inherit from par_speaker

#region Setting State
if (state = "standing") { sprite_index = spr_ngabang_idle; }
if (state = "sitting") { sprite_index = spr_ngabang_sitting; }
#endregion

#region Tutorial Level Dialogue Check Variables
if (tut_text = 0) { tut_text_display = 0; }
if (tut_text = 1) { tut_text_display = 1; }
if (tut_text = 2) { tut_text_display = 2; }
#endregion

#region Dialogue Variables
myPortrait			= spr_portrait_ngabang;
myVoice				= -1;
myFont				= fnt_dialogue;
myName				= "Ngabang";

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;
#endregion

//-------OTHER

choice_variable		= -1;	//the variable we change depending on the player's choice in dialogue