//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region Room 1 Dialogue
if (room = rm_level_1) 
{
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
		  //Line 0
		  var i = 0;
		  myText[i]  = "Oh, honey. You are finally awake. Your father is waiting. Best to head off.";
		  mySpeaker[i] = id;
		  myEmotion[i]	= 1;
		  
		  //line 1
		  i++;
		  myText[i]  = "Will do mother. Are you doing ok?";
		  mySpeaker[i] = obj_boy;
		  myEmotion[i]	= 1;
		  
		  //line 2
		  i++;
		  myText[i]  = "Yes, about to head off now with your sister. Go on then. Be sure to keep safe.";
		  mySpeaker[i] = id;
		  myEmotion[i]	= 1;
		  
		  //line 3
		  i++;
		  myText[i]  = "That's good. Okay talk to you soon. Oh, also Gira wanted some blueberries.";
		  mySpeaker[i] = obj_boy;
		  myEmotion[i]	= 1;
		  
		  //line 4
		  i++;
		  myText[i]  = "Alright I'll get her some. Have fun.";
		  mySpeaker[i] = id;
		  myEmotion[i]	= 1;
		  myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		 break;
		 #endregion
		
		 #region Consequences For The Dialogue
		 case "dialogueplayed":
			var i = 0;
				//Line 0
				myText[i]		= "Take care honey.";
				mySpeaker[i]	= id;
				myEmotion[i]	= 1;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		 #endregion
		 break;
			}
	#endregion
}
#endregion

#region Room 2a Dialogue 1
if (room = rm_level_2a) && (tut_text_display = 0) {
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
		 //Line 0
          var i = 0;
          myText[i] = "These are the ranges around our home, Hope.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
          
          //line 1
          i++;
          myText[i] = "I love it out here. Where will we be going?";
          mySpeaker[i] = obj_girl;
          myEmotion[i] = 1;
          
          //line 2
          i++;
          myText[i] = "A very special place, but it can be difficult to get there.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
          
          //line 3
          i++;
          myText[i] = "Are we going somewhere dangerous? Are we going to the spirit realm?";
          mySpeaker[i] = obj_girl;
          myEmotion[i] = 0;
		  
		  //line 4
          i++;
          myText[i] = "Let's leave it as a surprise.";
          mySpeaker[i] = id;
          myEmotion[i] = 0;
		  
		  //line 5
          i++;
          myText[i] = "You'll need to do a lot of jumping and climbing though however.";
          mySpeaker[i] = id;
          myEmotion[i] = 0;
		  
		  //line 6
          i++;
          myText[i] = "Continue further, I'll catch up.";
          mySpeaker[i] = id;
          myEmotion[i] = 0;
          
          //line 7
          i++;
          myText[i]  = "Alright then.";
          mySpeaker[i] = obj_girl;
          myEmotion[i] = 0;
          myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
          break;
          #endregion
        
          #region Consequences For The Dialogue
          case "dialogueplayed":
            var i = 0;
                //Line 0
                myText[i] = "Let’s keep going.";
                mySpeaker[i] = id;
                myEmotion[i] = 0;

                //Line 1
				i++;
                myText[i] = "It’s a fair hike from here.";
                mySpeaker[i] = id;
                myEmotion[i] = 1;
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
			break;	
		#endregion
		}
	#endregion
}
#endregion

#region Room 2a Dialogue 2
if (room = rm_level_2a) && (tut_text_display = 1) {
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
		  //Line 0
          var i = 0;
          myText[i] = ["How do we get across?", "Those cliffs look a bit high.", "There are some ledges I can’t reach."];
          myTypes[i] = 1;
          myNextLine[i] = [1, 3, 7];
          myScripts[i] = [[change_variable, id, "choice_variable", "get_across"], [change_variable, id, "choice_variable", "a_bit_high"], [change_variable, id, "choice_variable", "can’t_reach"]];
          mySpeaker[i] = obj_girl;
          myEmotion[i] = 0;

          //line 1
          i++;
          myText[i] = "Get a running start before you jump and,";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 2;
		  
		  //line 2
          i++;
          myText[i] = "and you should make it to the other side.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 8;

          //line 3
          i++;
          myText[i] = "If you jump from the cliff face and turn back to keep going up,";
          myNextLine[i] = 4;
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  
		  //line 4
          i++;
          myText[i] = "you can climb that way.";
          myNextLine[i] = 5;
          mySpeaker[i] = id;
          myEmotion[i] = 1;

          //line 5
          i++;
          myText[i] = "You can also jump from one cliff to another,";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 6;
		  
		  //line 6
          i++;
          myText[i] = "and then back again if they are close enough together.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 8;

          //line 7
          i++;
          myText[i] = "Sometimes there may be places where you simply cannot reach, I'm sorry.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = -1;
		  
		  //line 8
          i++;
          myText[i] = "Just keep at it.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = -1;
          break;
          #endregion

          #region Consequences For The Dialogue
          case "get_across":
		  		var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				break;
				#endregion

          #region Consequences For The Dialogue
		  case "a_bit_high":
				var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				 break;
				#endregion

          #region Consequences For The Dialogue
          case "can’t_reach":
				var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				break;
				#endregion
		}
	#endregion
}
#endregion

#region Room 2a Dialogue 3
if (room = rm_level_2a) && (tut_text_display = 2) {
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
			 //Line 0
	         var i = 0;
	         myText[i] = "Is this the place?";
	         mySpeaker[i] = obj_girl;
	         myEmotion[i] = 0;

	        //line 1
	        i++;
	        myText[i] = "This is the place. This is a scar tree.";
	        mySpeaker[i] = id;
	        myEmotion[i] = 1;
			
			//line 2
	        i++;
	        myText[i] = "Scar trees mark places of importance.";
	        mySpeaker[i] = id;
	        myEmotion[i] = 1;

	        //line 3
	        i++;
	        myText[i] = "They remind us of our past,";
	        mySpeaker[i] = id;
	        myEmotion[i] = 1;
			
			//line 4
	        i++;
	        myText[i] = "and take us to other places so that we can see.";
	        mySpeaker[i] = id;
	        myEmotion[i] = 1;

	        //line 5
	        i++;
	        myText[i] = "We go into the scar tree?";
	        mySpeaker[i] = obj_girl;
	        myEmotion[i] = 0;

	        //line 6
	        i++;
	        myText[i] = "Yes. Go through and I will see you on the other side.";
	        mySpeaker[i] = id;
	        myEmotion[i] = 1;
			myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
	        break;
	        #endregion
        
          #region Consequences For The Dialogue
          case "dialogueplayed":
            var i = 0;
                //Line 0
                myText[i] = "Go through the scar tree. I'll catch up with you.";
                mySpeaker[i] = id;
                myEmotion[i] = 0;

			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
			break;	
		#endregion
		}
	#endregion
}
#endregion

#region Room Spirit Realm Dialogue 1
if (room = rm_sr_level_2) && (tut_text_display = 0) {
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
		//Line 0
         var i = 0;
         myText[i] = "Where are we?";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;

         //line 1
         i++;
         myText[i] = "This is where our stories live.";
         mySpeaker[i] = id;
         myEmotion[i] = 0;
		 
		 //line 2
         i++;
         myText[i] = "For us, yesterday, today, and tomorrow, they come and go.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 3
         i++;
         myText[i] = "In this place, our history and stories are always today.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 3
         i++;
         myText[i] = "I love it here. I’m going to go explore!";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;

         //line 4
         i++;
         myText[i] = "Alright honey. Take care.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;
		 myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
         break;
         #endregion
        
          #region Consequences For The Dialogue
          case "dialogueplayed":
            var i = 0;
                //Line 0
                myText[i] = "Take care of yourself.";
                mySpeaker[i] = id;
                myEmotion[i] = 0;

                //Line 1
				i++;
                myText[i] = "This place has unique quirks to it.";
                mySpeaker[i] = id;
                myEmotion[i] = 1;
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
			break;	
		#endregion
		}
	#endregion
}
#endregion

#region Room Spirit Realm Dialogue 2
if (room = rm_sr_level_2) && (tut_text_display = 1) {
	#region The Dialogue
	switch(choice_variable) {
		case -1:
		 #region First Dialogue
		 //Line 0
          var i = 0;
          myText[i] = ["How do we get across?", "Those cliffs look a bit high.", "There are some ledges I can’t reach."];
          myTypes[i] = 1;
          myNextLine[i] = [1, 3, 7];
          myScripts[i] = [[change_variable, id, "choice_variable", "get_across"], [change_variable, id, "choice_variable", "a_bit_high"], [change_variable, id, "choice_variable", "can’t_reach"]];
          mySpeaker[i] = obj_girl;
          myEmotion[i] = 0;

          //line 1
          i++;
          myText[i] = "The same as before, get a running start before you jump and,";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 2;
		  
		  //line 2
          i++;
          myText[i] = "and you should make it to the other side.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 9;

          //line 3
          i++;
          myText[i] = "As before, if you jump from the cliff face and turn back to keep going up,";
          myNextLine[i] = 4;
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  
		  //line 4
          i++;
          myText[i] = "you can climb that way.";
          myNextLine[i] = 5;
          mySpeaker[i] = id;
          myEmotion[i] = 1;

          //line 5
          i++;
          myText[i] = "In this place, you will be able to jump on air.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 6;
		  
		  //line 6
          i++;
          myText[i] = "When you're in the air, try jumping again.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 9;

          //line 7
          i++;
          myText[i] = "In this place, you will be able to jump on air.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = 8;
		  
		  //line 8
          i++;
          myText[i] = "When you're in the air, try jumping again.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = -1;
		  
		  //line 9
          i++;
          myText[i] = "Just keep at it.";
          mySpeaker[i] = id;
          myEmotion[i] = 1;
		  myNextLine[i] = -1;
          break;
          #endregion

          #region Consequences For The Dialogue
          case "get_across":
		  		var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				break;
				#endregion

          #region Consequences For The Dialogue
		  case "a_bit_high":
				var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				break;
				#endregion

          #region Consequences For The Dialogue
          case "can’t_reach":
				var i = 0;
				//Line 0
				myText[i]		= "Could you not get across?";
				mySpeaker[i]	= id;
				myEmotion[i]	= 0;
				choice_variable	= -1;
				break;
				#endregion
		}
	#endregion
}
#endregion

#region Room Spirit Realm Dialogue 3
if (room = rm_sr_level_2) && (tut_text_display = 2) {
	#region The Dialogue
	switch(choice_variable){
		case -1:
		 #region First Dialogue
		  //Line 0
         var i = 0;
         myText[i] = "This is a beautiful place, but how do we get home?";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 0;

         //line 1
         i++;
         myText[i] = "I don't want to miss the Bunya festival.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 2;

         //line 2
         i++;
         myText[i] = "Don't worry about taking too long in here,";
         mySpeaker[i] = id;
         myEmotion[i] = 1;
		 
		 //line 3
         i++;
         myText[i] = "you can be as long as you like, you won't be late.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 4
         i++;
         myText[i] = "Finding another scar tree will take you back. There’s one up on that ridge.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 5
         i++;
         myText[i] = "Whenever you're ready, head up there.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;
		 
		 //line 6
         i++;
         myText[i] = "I need to get home to help get things ready.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 7
         i++;
         myText[i] = "Don't you need me to come with you? I can help.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 2;

         //line 8
         i++;
         myText[i] = "You have already helped.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;
		 
		 //line 9
         i++;
         myText[i] = "I'll take these berries back while you relax, explore, and have fun.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 10
         i++;
         myText[i] = "I'll see you at the Bunya festival!";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 11
         i++;
         myText[i] = "Okay, I'll see you later, mum!";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;
		 myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
         break;
         #endregion
        
          #region Consequences For The Dialogue
          case "dialogueplayed":
            var i = 0;
                //Line 0
                myText[i] = "Have fun, we will meet again soon!";
                mySpeaker[i] = id;
                myEmotion[i] = 1;
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		break;	
		#endregion
		}
	#endregion
}
#endregion