{
    "id": "4044fd95-64b9-401a-a866-59db41722803",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_movingplatform",
    "eventList": [
        {
            "id": "c522fc45-6848-41b2-961f-1f0d5632b2e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4044fd95-64b9-401a-a866-59db41722803"
        },
        {
            "id": "79fc9f40-d39c-45ee-92e1-69f5fcee8ad1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4044fd95-64b9-401a-a866-59db41722803"
        },
        {
            "id": "b6ad1c8f-4014-484e-b4a6-8fa51b580c79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "4044fd95-64b9-401a-a866-59db41722803"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f8838d28-c3e6-4d8a-aae3-ededfc22a90e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3d4ee24f-160a-4199-b5c6-e8b2eed67460",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"sw\"",
            "varName": "type",
            "varType": 6
        },
        {
            "id": "7986a3eb-b895-47b9-87cb-92bcff309174",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "500",
            "varName": "max_range",
            "varType": 0
        },
        {
            "id": "b345dfce-3bed-4c37-81c3-1f32e098d1bd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"horizontal\"",
                "\"vertical\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"horizontal\"",
            "varName": "platform_type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "a398c061-459c-415c-a9f0-6e035d004bb8",
    "visible": true
}