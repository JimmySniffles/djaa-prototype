/// @description Collisions and Movement

#region Setting Back & Forth Movement
if (platform_type == "horizontal") {
	hsp = dir * movespeed;
	if (dir == -1 && x <= destination) || (dir == 1 && x >= destination) {
		dir *= -1;
		destination = startx + max_range * dir;
	}
}

if (platform_type == "vertical") {
	vsp = dir * movespeed;
	if (dir == -1 && y <= destination) || (dir == 1 && y >= destination) {
		dir *= -1;
		destination = starty + max_range * dir;
	}
}
#endregion

#region Collision With Tiles & Damageable Objects & Setting X or Y Position To hsp or vsp
if (platform_type == "horizontal") {
	//Tile Collision
	var side; if (dir == 1) { side = bbox_right+hsp; } else if (dir == -1) { side = bbox_left-hsp; }
	if (scr_infloor(global.tilemap, side, y) >= 0) { hsp = 0; dir *= -1; destination = startx + max_range * dir; } 

	//Damageable Objects Collision
	if place_meeting(x+hsp, y, par_dmgobjs) {
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel, y, par_dmgobjs)) { x += onepixel; }
		hsp = 0;
		dir *= -1;
		destination = startx + max_range * dir;
	}
	x += hsp;
}

if (platform_type == "vertical") {
	//Tile Collision
	var side; if (dir == -1) { side = bbox_top-vsp; } else if (dir == 1) { side = bbox_bottom+vsp; }
	if (scr_infloor(global.tilemap, x, side) >= 0) { vsp = 0; dir *= -1; destination = starty + max_range * dir; } 

	//Damageable Objects Collision
	if place_meeting(x,y+vsp, par_dmgobjs) {
		var onepixel = sign(vsp);
		while (!place_meeting(x, y+onepixel, par_dmgobjs)) { y += onepixel; }
		vsp = 0;
		dir *= -1;
		destination = starty + max_range * dir;
	}
	y += vsp;
}
#endregion

#region Allow Objects To Stay On Moving Platform
if (platform_type == "horizontal") {
	if (instance_exists(player))
	{
		//if position_meeting(x,y-1,player) { player.hsp_carry = hsp; }
		with (instance_place(x,y-1,player)) { x += other.hsp; }
	}
}
#endregion