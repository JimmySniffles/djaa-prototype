/// @description Intializing Variables & Setting Sprite Image Index

#region Intializing Variables
dir				= choose(-1,1);							//-1 is moving upwards if vertical moving platform chosen.
movespeed		= irandom_range(1,3);					//Platform move speed.
hsp				= 0;									//Setting horizontal speed.
vsp				= 0;									//Setting horizontal speed.
startx			= xstart;								//Getting start x position upon intialisation.
starty			= ystart;								//Getting start y position upon intialisation.
image_speed		= 0;									//Setting image animation speed to zero so it does not move between frames.

//Getting direction.
if (platform_type == "horizontal") { destination = startx + max_range * dir; }
if (platform_type == "vertical") { destination = starty + max_range * dir; }

//Getting player whether they exist or not.
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }

//Setting frame of moving platform based upon world.
if (type == "nw") { image_index = 0; }
if (type == "sw") { image_index = 1; }
#endregion