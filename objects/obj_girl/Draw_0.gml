/// @description Drawing Self, Stats & Weapon Icons
draw_self();
if (state != states.death) { scr_stats_display(x-hp_disp_x, y-hp_disp_y, x-energy_disp_x, y-energy_disp_y, x-breath_disp_x, y-breath_disp_y); }

#region Flash Effects
//Invincibility flash pulse effect
if (invincible) { invincible_flash_timer ++; }
if (invincible_flash_timer >= room_speed * 2) { whiteflash = 0.8; invincible_flash_timer = 0; }
if (whiteflash > 0) && (takingdamage == true) { scr_shader_flash(0.05,1.0,1.0,1.0); }
if (whiteflash > 0) && (takingdamage == false) && (state != states.death) { scr_shader_flash(0.02,1.0,1.0,1.0); }

//Death flash & fade effect
if (whiteflash > 0) && (state == states.death) { scr_shader_flash(0.01,1.0,1.0,1.0); image_alpha -= 0.02; }

if (blueflash > 0) { scr_shader_flash(0.05,0,1.0,1.0); }
if (redflash > 0) { scr_shader_flash(0.05,1.0,0,0); }
if (greenflash > 0) { scr_shader_flash(0.05,0,0,1.0); }
#endregion