///@description Setting Variables
scr_player_var();
level = 1;
experience = 0;
max_experience = 10;
climb_delay = 0;

#region Setting Player Type, Dialogue & Name Classification Variables
reset_dialogue_defaults();
myPortrait			= spr_portrait_hope;
myVoice				= -1;
myFont				= fnt_dialogue;
myName				= "Hope";

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;

player_type			= obj_girl;
#endregion

#region Movement Variables

	#region Gravity Variables
	grv = 0.3;					//Standard Gravity
	grv_wall = 0.1;				//Gravity on a wall
	#endregion

	#region Horizontal Movement Variables
	hsp_wmsp = 0;				//Horizontal walking movement speed
	hsp_wmaxsp = 1.5;				//Maximum horizontal walking movement speed
	hsp_msp = 0;				//Horizontal movement speed
	hsp_maxsp = 4;				//Maximum horizontal movement speed
	hsp_cmsp = 0;				//Horizontal climbing movement speed
	hsp_cmaxsp = 1;				//Maximum horizontal climbing movement speed
	
	hsp_acc = 1;				//Horizontal acceleration
	hsp_wjump = 6;				//Horizontal wall jump speed
	hsp_fric_ground = 0.50;		//Horizontal friction on the ground
	hsp_fric_air = 0.15;		//Horizontal friction in the air
	
	roll_spd = 4.5;							//The roll dodge speed
	roll_ani_spd = 0.6;						//The roll animation speed
	
	gslide_spd = 4.5;						//The ground slide dodge speed
	gslide_fric = 0.10;						//The ground slide friction to slow down by
	gslide_ani_spd = 1;						//The ground slide animation speed
	#endregion

	#region Vertical Jumping & Speed Downwards Variables
	vsp_cacc = 0.3;				//Vertical climbing acceleration speed
	vsp_cmsp = 0;				//Vertical climbing movement speed
	vsp_cmaxsp = 2;				//Maximum vertical climbing movement speed
	vsp_jump = -12;				//Vertical jump height
	vsp_max = 12;				//Vertical maximum speed downwards (known as terminal velocity)
	vsp_wjump = -9;				//Vertical wall jump height
	vsp_max_wall = 5;			//Maximum vertical speed when on a wall		//Make sure of: Less than half of the vsp_max speed when falling (Make sure to set this to be less than half)
	jumps = 0;					//Jumps available at a given time
	jumpsmax = 1;				//Jump Amount (more than one implemented when in spirit world)
	//if (room = rm_sr_level_2) { jumpsmax += 1; }
	walljumpdelay = 0;			//Wall jump delay before being able to jump again so the player can not just climb up a wall
	walljumpdelay_max = 45;		//Max wall jump delay
	#endregion

#endregion

#region Player Object Status
	
	#region Health, Energy, Breath Variables
	max_hp = 75;										//Max amount of health points
	hp = max_hp;										//Setting hp to equal max hp
	old_hp = hp;										//Setting old hp amount for a delay catch up to hp value
	max_hp_bar_dec_delay = room_speed * 1;				//max old hp decrease timer delay
	hp_bar_dec_delay = max_hp_bar_dec_delay;			//hp bar decrease timer delay
	max_hp_recovery_timer = room_speed * 4;				//Max hp recovery timer
	hp_recovery_timer = max_hp_recovery_timer;			//Recovery hp amount timer
	hp_disp_x = 30;										//X position of the hp display
	hp_disp_y = 30;										//X position of the hp display
	
	max_energy = 100;									//Max amount of energy points
	energy = max_energy;								//Setting energy to equal max energy
	old_energy = energy;								//Setting old energy amount for a delay catch up to energy value
	max_energy_bar_dec_delay = room_speed * 4;			//max old energy decrease timer delay
	energy_bar_dec_delay = max_energy_bar_dec_delay;	//energy bar decrease timer delay
	max_energy_recovery_timer = room_speed * 2;			//Max energy recovery timer
	energy_recovery_timer = max_energy_recovery_timer;	//Recovery energy amount timer
	engy_exert_breath = room_speed * 20;				//Amount of time before needing to breath creating the breathing sound effect
	energy_disp_x = 28;									//X position of the energy display
	energy_disp_y = 35;									//X position of the energy display
	e_jumpuse = 3;										//Amount of energy used for jumping
	e_runuse = 2;										//Amount of energy used for running
	e_rolluse = 3;										//Amount of energy used for rolling
	e_gslideuse = 2;									//Amount of energy used for ground sliding
	
	max_breath = 100;									//Max amount of breath points
	breath = max_breath;								//Setting breath to equal max breath
	old_breath = breath;								//Setting old breath amount for a delay catch up to breath value
	max_breath_bar_dec_delay = room_speed * 4;			//max old breath decrease timer delay
	breath_bar_dec_delay = max_breath_bar_dec_delay;	//breath bar decrease timer delay
	max_breath_timer = room_speed * 1;					//Maximum breath timer to count down from
	breath_timer = max_breath_timer;					//Breath timer that counts to an amount for the breath to degrade by
	max_breath_recovery_timer = room_speed * 1;			//Max breath timer for recovery if just got out of the water
	breath_recovery_timer = max_breath_recovery_timer;	//Breath timer for recovery
	breath_disp_x = 0;									//X position of the breath display
	breath_disp_y = 35;									//Y position of the breath display	
	#endregion
	
#endregion

#region Animation, Visual Particle Effects & Sounds

	#region Visual Particle Effects & Screenshake
	dirt_jumping =	3;					//Sets amount of dirt particles when jumping
	dirt_walljumping = 5;				//Sets amount of dirt particles when wall jumping
	dirt_landing = 5;					//Set amount of dirt pacticles when landing after jump or fall
	dirt_moving_wlk = 1;				//Sets amount of dirt particles when walking
	dirt_moving_run = 2;				//Sets amount of dirt particles when running
	dirt_sliding = 1;					//Sets amount of dirt particles when sliding
	dirt_gsliding =	1;					//Sets amount of dirt particles when ground sliding
	dirt_dodge = 5;						//Sets amount of dirt particles when landing the dodge
	
	scrshk_landing_mag = 1;				//Set screenshake landing magnitude when landing after jump or fall
	scrshk_landing_frms = 15;			//Set screenshake landing frames when landing after jump or fall
	
	scrshk_dodge_mag = 1;				//Set screenshake magnitude when dodging
	scrshk_dodge_frms = 20;				//Set screenshake frames when dodging
	#endregion
	
	#region Sound Effects
	snd_idle_v1			= sn_g_slide_v1;			//Sets idle sound version 1
	snd_idle_v2			= sn_g_slide_v2;			//Sets idle sound version 2
	snd_idle_v3			= sn_g_slide_v3;			//Sets idle sound version 3
	snd_idle_v4			= sn_g_slide_v2;			//Sets idle sound version 4
	
	snd_slidecall_v1	= sn_g_slide_v1;			//Sets slide call sound version 1
	snd_slidecall_v2	= sn_g_slide_v2;			//Sets slide call sound version 2
	snd_slidecall_v3	= sn_g_slide_v3;			//Sets slide call sound version 3
	
	snd_jump_v1			= sn_g_jump_v1;				//Sets jump sound version 1
	snd_jump_v2			= sn_g_jump_v2;				//Sets jump sound version 2
	snd_jump_v3			= sn_g_jump_v3;				//Sets jump sound version 3
	
	snd_landing_v1		= sn_g_landing_v1;			//Sets landing sound version 1
	snd_landing_v2		= sn_g_landing_v2;			//Sets landing sound version 2
	snd_landing_v3		= sn_g_landing_v3;			//Sets landing sound version 3
	
	snd_running_v1		= sn_g_running_v1;			//Sets running sound version 1
	snd_running_v2		= sn_g_running_v2;			//Sets running sound version 2
	
	snd_swallow_v1		= sn_g_swallow_v1;			//Sets swallow sound version 1
	snd_swallow_v2		= sn_g_swallow_v2;			//Sets swallow sound version 2
	
	snd_death_v1		= sn_g_death_v1;			//Sets death sound version 1
	snd_death_v2		= sn_g_death_v2;			//Sets death sound version 2
	snd_death_v3		= sn_g_death_v1;			//Sets death sound version 3
	snd_death_v4		= sn_g_death_v2;			//Sets death sound version 4
	
	snd_takinghit_v1	= sn_g_hit_v1;				//Sets taking hit sound version 1
	snd_takinghit_v2	= sn_g_hit_v2;				//Sets taking hit sound version 2
	snd_takinghit_v3	= sn_g_hit_v3;				//Sets taking hit sound version 3
	#endregion
	
	#region Animation
	ani_idle = spr_girl_idle;			//Gets objects idle animation
	ani_walk = spr_girl_walk;			//Gets objects walk animation
	ani_run = spr_girl_run;				//Gets objects run animation
	ani_jump = spr_girl_jump;			//Gets objects jump animation
	ani_fall = spr_girl_fall;			//Gets objects fall animation
	ani_slide = spr_girl_slide;			//Gets objects slide animation
	ani_gslide = spr_girl_gslide;		//Gets objects ground slide animation
	ani_dodge = spr_girl_dodge;			//Gets objects dodge animation
	ani_interact = spr_girl_interact;	//Gets objects grab animation
	ani_climb = spr_girl_climb;			//Gets objects climb animation
	ani_push = spr_girl_push;			//Gets objects push animation
	ani_dead = spr_girl_dead;			//Gets objects death animation
	ani_sitting = spr_girl_sitting;		//Gets objects sitting animation
	#endregion
	
#endregion