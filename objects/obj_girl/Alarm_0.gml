/// @description Cooldown Set & Berry Boost Reversion
cooldown = 0;

#region Berry Boost Reversion
if (berryeaten = string("yellow")) || (berryeaten = string("black")) {
	hsp_msp = 4;				
	hsp_maxsp = 4;				
	vsp_jump = -10;			    
}

//if (berryeaten = string("orange")) { if (room != rm_sr_level_2) { jumpsmax = 1; } else { jumpsmax = 2; } }

//if (berryeaten = string("white")) { if (room != rm_sr_level_2) { jumpsmax = 1; } else { jumpsmax = 2; } }

if (berryeaten = string("lemon_myrtle")) { invincible = false; }
#endregion