///@description Switching States & General Player Mechanics Outside Of States

#region The Amount Of States Hope Can Switch Too
switch (state)
{
	case states.normal:		scr_girl_normal();			break;		//Default state for Hope
	case states.gslide:		scr_player_gslide();		break;		//Ground slide state of the player
	case states.dodge:		scr_player_dodge();			break;		//Dodge state of the player
	case states.interact:	scr_player_interact();		break;		//Interact state for Hope	
	case states.push:		scr_player_push();			break;		//Pushing state for Hope
	case states.climb:		scr_player_climb();			break;		//Climbing state for Hope
	case states.death:		scr_player_death();			break;		//Death state for Hope
}
#endregion

#region Direction Of Player Set
if (hsp != 0) && (state != states.push) { image_xscale = sign(hsp); }
#endregion

#region Battle Music If Within Distance Of Enemy
if (instance_exists(obj_FoN_grunt)) {
	if (distance_to_object(obj_FoN_grunt) <= combatmusicrange) {
		if (combatmusic = false) {
		combat_music = audio_play_sound(sn_bg_combat,3,true);
		audio_sound_gain(combat_music,1,300);
		combatmusic = true;
		}
	}				
}

if (distance_to_object(obj_FoN_grunt) > combatmusicrange) || (!instance_exists(obj_FoN_grunt)) {
	if (combatmusic = true) {
		audio_sound_gain(combat_music,0,300);
		combatmusic = false;
	}
	endingmusictimer++
}

if (endingmusictimer >= 600) && (combatmusic = false) { if (audio_is_playing(combat_music)) { endingmusictimer = 0 audio_stop_sound(combat_music); } }
#endregion

#region Stating What Happens When Health, Energy Or Breath Is Equal Too Or Less Than Zero
if (hp <= 0) { state = states.death; }
if (energy <= 0) { hascontrol = false; }
if (breath <= 0) { hp = 0; }
#endregion

#region Disappear UI Of Health, Energy & Breath When True
if (show_ui_hp) && (hp = max_hp) { show_ui_hp = false; }
if (show_ui_energy) && (energy = max_energy) { show_ui_energy = false; }
if (show_ui_breath) && (breath = max_breath) { show_ui_breath = false; }
#endregion

#region Health Recovery
if (!takingdamage) { if (hp < max_hp) { hp_recovery_timer -= 1; } }

if (hp_recovery_timer <= 0) && (!takingdamage) {
	//Add 10% of hp if hp is less than or equal to 90%
	if (hp <= max_hp - (max_hp * 0.1)) { hp += max_hp * 0.1; show_ui_hp = true; } 
	//Sets hp to maximum hp if above 90% and less than 100% of maximum hp
	else { if (hp > max_hp - (max_hp * 0.1)) && (hp < max_hp) { hp = max_hp; show_ui_hp = true; } }
	hp_recovery_timer = max_hp_recovery_timer;
}
#endregion

#region Energy Running Degradation, Checking Low Energy & Recovery
if (show_ui_energy) || (sprite_index == ani_run) || (energy < max_energy) { energy_recovery_timer -= 1; }

if (energy_recovery_timer <= 0) {
	if (sprite_index == ani_run) && (hsp != 0) { energy -= e_runuse; show_ui_energy = true; }

	if (sprite_index == ani_walk) || (sprite_index == ani_idle) || (hsp == 0) { 
		//Add 15% of energy if energy is less than or equal to 85%
		if (energy <= max_energy - (max_energy * 0.15)) { energy += max_energy * 0.15; show_ui_energy = true; } 
		//Sets energy to maximum energy if above 85% and less than 100% of maximum energy
		else { if (energy > max_energy - (max_energy * 0.15)) && (energy < max_energy) { energy = max_energy; show_ui_energy = true; } }
	}
	energy_recovery_timer = max_energy_recovery_timer;
}

	#region Low Energy Check 
	if (energy < (max_energy * 0.1))
	{
		if (!audio_is_playing(snd_running_v1)) && (!audio_is_playing(snd_running_v2)) {
				audio_play_sound(choose(snd_running_v1,snd_running_v2),5,false);
		}
	}
	#endregion
	
#endregion

#region Breath Recovery
if (is_water_surface_collision) && (inandoutofwater = 1) { inandoutofwater = 2; }
if (inandoutofwater = 2) { breath_recovery_timer -= 1; }
if (breath = max_breath) { inandoutofwater = 0; }

if (breath_recovery_timer <= 0) {
	//Add 10% of breath if breath is less than or equal to 90% and set breath degrade timer to maximum breath timer
	if (breath <= max_breath - (max_breath * 0.1)) { breath += max_breath * 0.1; breath_timer = max_breath_timer; show_ui_breath = true; } 
	//Sets breath to maximum breath if above 90% and less than 100% of maximum breath
	else { if (breath > max_breath - (max_breath * 0.1)) && (breath < max_breath) { breath = max_breath; breath_timer = max_breath_timer; show_ui_breath = true; } } 
	breath_recovery_timer = max_breath_recovery_timer;
} 
#endregion

#region Taking Damage
if (takingdamage)
{
	if (!invincible) {
		if (scr_chance(0.4)) {
			if (!audio_is_playing(snd_takinghit_v1)) && (!audio_is_playing(snd_takinghit_v2)) && (!audio_is_playing(snd_takinghit_v3)) {
					audio_play_sound(choose(snd_takinghit_v1, snd_takinghit_v2, snd_takinghit_v3),8,false);
			}
		}
		redflash = 1;
		show_ui_hp = true;
	} else {
		whiteflash = 1;
	}
	takingdamage = false;
}
#endregion