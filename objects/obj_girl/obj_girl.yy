{
    "id": "b1bb8fb2-9d51-4e02-9496-017711b8488b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_girl",
    "eventList": [
        {
            "id": "f052d28c-75b1-4648-b652-d29466340dd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "15a1a720-a36d-40f0-919c-aec802b09a5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "16fd7644-14a4-4582-8977-929336555cfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "cc65c22e-8041-4ab4-8d3a-a32ef0413413",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "a9ad9f09-3a94-485c-89f2-ce9d5ef0ea45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "31707756-7077-41e6-95dc-1d86f372c05c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        },
        {
            "id": "e4e313a9-d7f0-4691-af26-f7f6bdbda7fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c27cd1d-35fb-432a-bd5a-734a907ec783",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b1bb8fb2-9d51-4e02-9496-017711b8488b"
        }
    ],
    "maskSpriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
    "visible": true
}