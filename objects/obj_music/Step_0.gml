/// @description Setting Up Background Music, Sound Effects And Voices Throughout The Levels Of The Game

if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }

#region Setting Background Music Through Each Level
if (background_music_loaded)
{
	#region Menu And Credits Music
	if (room == rm_continue) && (!audio_is_playing(sn_menubg)) { audio_play_sound(sn_menubg,20,true) }
	else if (room != rm_continue) && (room != rm_menu) && (room != rm_mcredits) { if (audio_is_playing(sn_menubg)) { audio_stop_sound(sn_menubg); } }
	
	if (room == rm_menu) && (!audio_is_playing(sn_menubg)) { audio_play_sound(sn_menubg,20,true) }
	else if (room != rm_continue) && (room != rm_menu) && (room != rm_mcredits) { if (audio_is_playing(sn_menubg)) { audio_stop_sound(sn_menubg); } }
	
	if (room == rm_mcredits) && (!audio_is_playing(sn_menubg)) { audio_play_sound(sn_menubg,20,true); }
	else if (room != rm_continue) && (room != rm_menu) && (room != rm_mcredits) { if (audio_is_playing(sn_menubg)) { audio_stop_sound(sn_menubg); } }
	#endregion
	
	#region Cutscene Narration Music
	//if (room = rm_cutscene_narration) && (!audio_is_playing(sn_narration_cutscene)) && (musicplayed = 0) { audio_play_sound(sn_narration_cutscene,20,false) musicplayed = 1; }
	//else if (room = rm_cutscene_campfire) { if (audio_is_playing(sn_narration_cutscene)) { audio_stop_sound(sn_narration_cutscene) } musicplayed = 0; }
	#endregion
	
	#region Ambiant Music For All In-Game Playable Rooms Unless Raining Or Any Other Reasons
	if (room == rm_level_1a) || (room == rm_level_1b) {
		if (!audio_is_playing(sn_ambiant_v1)) && (!audio_is_playing(sn_ambiant_v2)) { audio_play_sound(choose(sn_ambiant_v1,sn_ambiant_v2),1,true); }
	}
	
	if (room != rm_level_1a) && (room != rm_level_1b)  {
		if (audio_is_playing(sn_ambiant_v1)) { audio_stop_sound(sn_ambiant_v1); }
		if (audio_is_playing(sn_ambiant_v2)) { audio_stop_sound(sn_ambiant_v2); }
	}
	#endregion
	
	#region Level 1a Music	
	if (room == rm_level_1a) && (!audio_is_playing(sn_didgeridoo_short)) && (!audio_is_playing(sn_bg_music_inlevels)) {
		if (instance_exists(player)) { if (player.combatmusic == false) && (musicplaying = false) { musictimer ++; } }
		
		if (musictimer >= room_speed * 25) && (musicplaying = false) {
			if (instance_exists(player)) {
				if (player.combatmusic = false) {
					if (!audio_is_playing(sn_didgeridoo_short)) && (!audio_is_playing(sn_bg_music_inlevels)) {
						audio_play_sound(choose(sn_didgeridoo_short,sn_bg_music_inlevels),2,false);
					}
				musicplaying = true;
				musictimer = 0;
				}
			}
		}
		
		if (musicplaying = true) { musicplayingtimer ++; }
		if (musicplayingtimer >= room_speed * 25) { musicplaying = false musicplayingtimer = 0; }
		
		if (instance_exists(player)) {
			if (player.combatmusic = true) {
				if (audio_is_playing(sn_didgeridoo_short)) { audio_stop_sound(sn_didgeridoo_short) musictimer = 0; }
				if (audio_is_playing(sn_bg_music_inlevels)) { audio_stop_sound(sn_bg_music_inlevels) musictimer = 0; }						
				}
			}
		}
	#endregion
	
	#region Level 1b Music
	if (room == rm_level_1b) && (!audio_is_playing(sn_didgeridoo_short)) && (!audio_is_playing(sn_bg_music_inlevels)) {
		if (instance_exists(player)) { if (player.combatmusic == false) && (musicplaying = false) { musictimer ++; } }
		
		if (musictimer >= room_speed * 25) && (musicplaying = false) {
			if (instance_exists(player)) {
				if (player.combatmusic = false) {
					if (!audio_is_playing(sn_didgeridoo_short)) && (!audio_is_playing(sn_bg_music_inlevels)) {
						audio_play_sound(choose(sn_didgeridoo_short,sn_bg_music_inlevels),2,false);
					}
				musicplaying = true;
				musictimer = 0;
				}
			}
		}
		
		if (musicplaying = true) { musicplayingtimer ++; }
		if (musicplayingtimer >= room_speed * 25) { musicplaying = false musicplayingtimer = 0; }
		
		if (instance_exists(player)) {
			if (player.combatmusic = true) {
				if (audio_is_playing(sn_didgeridoo_short)) { audio_stop_sound(sn_didgeridoo_short) musictimer = 0; }
				if (audio_is_playing(sn_bg_music_inlevels)) { audio_stop_sound(sn_bg_music_inlevels) musictimer = 0; }						
				}
			}
		}
	#endregion
	
	#region Rain Ambiant Music For In-Game Levels
	/*if (room = rm_level_1b) { 
		if (!audio_is_playing(sn_raining)) { audio_play_sound(sn_raining,1,true); }
	}
	
	if (room != rm_level_1b) {
		if (audio_is_playing(sn_raining)) { audio_stop_sound(sn_raining); }
	}*/
	#endregion
	
}
#endregion

#region Setting Sound Effects Through Each Level
if (sound_effects_loaded)
{	
	#region Fire Place Sound Effect
	if (room = rm_menu) && (!audio_is_playing(sn_fireplace)) { audio_play_sound(sn_fireplace,15,true); }
	else if (room != rm_menu) && (room != rm_mcredits) { if (audio_is_playing(sn_fireplace)) { audio_stop_sound(sn_fireplace); } }
	
	
	if (room = rm_mcredits) && (!audio_is_playing(sn_fireplace)) { audio_play_sound(sn_fireplace,15,true); }
	else if (room != rm_menu) && (room != rm_mcredits) { if (audio_is_playing(sn_fireplace)) { audio_stop_sound(sn_fireplace); } }
	#endregion
}
#endregion

#region Setting Voice Over Through Each Level
if (voiceover_loaded)
{
	#region Cutscene Narration Voice Over
	//if (room = rm_cutscene_narration) && (!audio_is_playing(sn_voicecutscene)) && (voiceplayed = 0) { audio_play_sound(sn_voicecutscene,25,false) voiceplayed = 1; }
	//else if (room = rm_cutscene_campfire) { if (audio_is_playing(sn_voicecutscene)) { audio_stop_sound(sn_voicecutscene) } voiceplayed = 0; }
	#endregion
}
#endregion