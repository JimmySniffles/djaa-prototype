/// @description Load Music + Setup
audio_group_load(AG_BG_Music);
audio_group_load(AG_Sound_Effects);
audio_group_load(AG_VoiceOver);

background_music = [sn_menubg,sn_ambiant_v1,sn_ambiant_v2,sn_raining,sn_narration_cutscene,sn_ambiant_nighttime,sn_bg_relaxdidgeridoo,sn_bg_combat,sn_ninderry_theme,sn_didgeridoo_short,sn_bg_music_inlevels];

sound_effects = [sn_con_new,sn_back,sn_settings,sn_nav,sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5,sn_slide_v1,sn_slide_v2,sn_slide_v3,sn_slide_v4,
				sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3,sn_meleehitroot_v1,sn_meleehitroot_v2,sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6,
				sn_glanding_v1,sn_glanding_v2,sn_glanding_v3,sn_rockbreak_v1,sn_rockbreak_v2,sn_rockbreak_v3,sn_rockbreak_final,sn_croclogo,sn_fireplace,
				sn_checkpoint_v1,sn_checkpoint_v2,sn_checkpoint_v3,sn_boomerang_air,sn_woodhitgrass_v1,sn_woodhitgrass_v2,sn_woodhitgrass_v3,sn_woodhitrock_v1,
				sn_woodhitrock_v2,sn_woodhitrock_v3,sn_woodhitroot_v1,sn_woodhitroot_v2,sn_woodhitroot_v3,sn_weaponcatch_v1,sn_weaponcatch_v2,sn_weaponcatch_v3,
				sn_trans_nw_v1,sn_trans_nw_v2,sn_trans_nw_v3,sn_trans_sw_v1,sn_trans_sw_v2,sn_trans_sw_v3,sn_pushobject_v1,sn_pushobject_v2,sn_pushobject_v3,
				sn_wingflap_v1,sn_wingflap_v2,sn_wingflap_v3,sn_wingflap_v4,sn_invincibility,sn_levelup,sn_experience,sn_hp_increase,sn_finger_lime];

voiceover = [sn_voicecutscene,sn_deadly_v1,sn_deadly_v2,sn_deadly_v3,sn_laugh_v1,sn_pickup_v1,sn_pickup_v2,sn_attack_v1,sn_attack_v2,sn_throw,sn_jump,sn_running_v1,sn_running_v2,
			 sn_swallow_v1,sn_swallow_v2,sn_death_v1,sn_death_v2,sn_death_v3,sn_death_v4,sn_oww,sn_g_jump_v1,sn_g_jump_v2,sn_g_jump_v3,sn_g_death_v1,sn_g_death_v2,sn_g_hit_v1,sn_g_hit_v2,
			 sn_g_hit_v3,sn_g_landing_v1,sn_g_landing_v2,sn_g_landing_v3,sn_g_running_v1,sn_g_running_v2,sn_g_slide_v1,sn_g_slide_v2,sn_g_slide_v3,sn_g_swallow_v1,sn_g_swallow_v2,
			 sn_fon_chase_v1,sn_fon_chase_v2,sn_fon_chase_v3,sn_fon_chase_b_v1,sn_fon_chase_b_v2,sn_fon_chase_g_v1,sn_fon_chase_g_v2,sn_fon_attack_v1,sn_fon_attack_v2,sn_fon_attack_v3,
			 sn_fon_hit_v1,sn_fon_hit_v2,sn_fon_hit_v3,sn_fon_death_v1,sn_fon_death_v2,sn_fon_death_v3,sn_kang_cluck_v1,sn_kang_cluck_v2,sn_kang_cluck_v3,sn_kang_cluck_v4,
			 sn_kang_growl_v1,sn_kang_hit_v1,sn_kang_hit_v2,sn_kang_hit_v3,sn_kang_hit_v4,sn_bat_v1,sn_bat_v2,sn_bat_v3,sn_bat_v4,sn_snake_attack_v1,sn_snake_attack_v2,sn_snake_hiss_v1,
			 sn_snake_hiss_v2,sn_snake_hiss_v3,sn_snake_hiss_v4,sn_snake_hiss_v5];

global.background_music_vol = 0.75;
global.sound_effects_vol = 0.75;
global.voice_over_vol = 0.75;

control = false;
background_music_loaded = false;
sound_effects_loaded = false;
voiceover_loaded = false;

musicplayed = 0;
voiceplayed = 0;

musictimer = 0;
musicplaying = false;
musicplayingtimer = 0;