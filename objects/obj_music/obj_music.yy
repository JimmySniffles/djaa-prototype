{
    "id": "fdac27c1-01d2-4e9f-9031-d069b5b7a755",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_music",
    "eventList": [
        {
            "id": "fc94ab1f-a742-45d3-9db7-5b33d98fce70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fdac27c1-01d2-4e9f-9031-d069b5b7a755"
        },
        {
            "id": "3450c2fa-58fa-45d3-b5b5-4a6a572220ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "fdac27c1-01d2-4e9f-9031-d069b5b7a755"
        },
        {
            "id": "c8b0b02e-e93c-4fe7-8b64-14225712bd2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fdac27c1-01d2-4e9f-9031-d069b5b7a755"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}