/// @description Sound loaded

if (audio_group_is_loaded(AG_BG_Music))
{
	background_music_loaded = true;
	control = true;
}

if (audio_group_is_loaded(AG_Sound_Effects))
{
	sound_effects_loaded = true;
	control = true;
}

if (audio_group_is_loaded(AG_VoiceOver))
{
	voiceover_loaded = true;
	control = true;
}