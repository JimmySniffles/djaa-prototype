{
    "id": "2be432d5-7ffa-4c1e-8de9-495f5cdfd500",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_snake",
    "eventList": [
        {
            "id": "4d0eae2d-264f-46a4-ba44-9bef58687957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2be432d5-7ffa-4c1e-8de9-495f5cdfd500"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4d27caab-c5a4-4ce3-89f9-5c535c0fa3cf",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "4f3ec3b7-f2b5-437c-acd1-7bf01c149cfe",
            "value": "\"snake\""
        },
        {
            "id": "ffc2c278-f4f6-47f9-8dba-5aa8905d59a9",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "b8f2e771-dfcf-48c2-addd-f78994c326f4",
            "value": "True"
        },
        {
            "id": "7c4365c4-d4ca-44d8-a115-f8fd42d671d8",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "cdfe4382-ec1c-41a5-b463-852e818a3469",
            "value": "True"
        }
    ],
    "parentObjectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "efa517bb-2a6d-463b-ab71-bea7215ff8ac",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"non_selected\"",
                "\"nw_taipan\"",
                "\"nw_redbelly\"",
                "\"nw_brown\"",
                "\"nw_boss_redbelly\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"non_selected\"",
            "varName": "type_select",
            "varType": 6
        },
        {
            "id": "9adeb7fa-2fad-4376-b60a-c1680a30051d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type_random",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "3377d68a-10dc-4ac9-a61c-9cfedb027896",
    "visible": true
}