{
    "id": "399fac47-1840-4934-bf59-d3308cd48397",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Solace",
    "eventList": [
        {
            "id": "81747ffb-2f01-4d08-96e3-8653b22b9875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "399fac47-1840-4934-bf59-d3308cd48397"
        },
        {
            "id": "a9bb0b22-bc06-4c6d-8f0d-eb5cde9241fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "399fac47-1840-4934-bf59-d3308cd48397"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
    "visible": true
}