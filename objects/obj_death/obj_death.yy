{
    "id": "28f256c5-a35f-4cd8-b825-8ea5031256df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death",
    "eventList": [
        {
            "id": "ce118a93-aeec-4365-8f96-664a4266043f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "28f256c5-a35f-4cd8-b825-8ea5031256df"
        },
        {
            "id": "05276a26-5339-4b78-92bb-ff91fee33a8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "28f256c5-a35f-4cd8-b825-8ea5031256df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
    "visible": true
}