/// @description Death Particle Range and Speed
hsp = random_range(-0.4,0.4);
vsp = random_range(-0.4,-0.6);
image_speed = random_range(0.2,0.4);
image_index = random_range(1,3);