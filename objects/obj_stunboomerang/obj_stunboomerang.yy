{
    "id": "1f78b7a8-237f-4955-bf76-f5f97a796d72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stunboomerang",
    "eventList": [
        {
            "id": "3f18608b-a8ca-4a9d-b315-ca59bb3faa69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f78b7a8-237f-4955-bf76-f5f97a796d72"
        },
        {
            "id": "cee28e81-b1e3-4c58-83ee-113f067f46a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f78b7a8-237f-4955-bf76-f5f97a796d72"
        },
        {
            "id": "cfee93cf-d439-40ef-a014-850533a14896",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1f78b7a8-237f-4955-bf76-f5f97a796d72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
    "visible": true
}