/// @description Setting Up Type, Mode & Variables

#region Setting Enums For Transition Modes & Types 
enum TRANS_MODE {
	OFF,
	NEXT,
	GOTO,
	ROOMRESTART,
	GAMERESTART,
	DEATH,
	INTRO
}

enum TRANS_TYPE {
	CENTRE,
	SIDE
}
//Setting type variable to centre by default.
type = TRANS_TYPE.CENTRE;							
#endregion

#region Setting Variables
gui_width		= display_get_gui_width();					//The width of the display (returns number, example being 1920).
gui_height		= display_get_gui_height();					//The height of the display (returns number, example being 1080).

//Variables For Centre Transition
width			= 50 + gui_width;							//Width of black rectangle for transition
height			= 50 + gui_height;							//Height of black rectangle for transition
height_half		= height * 0.5;								//The halfway point for the black rectangle
mode			= TRANS_MODE.INTRO;							//Setting mode for transition. Default is intro which begins to close the bars together
percent			= 1;										//Set between 0 and 1 depending upon how complete the black bars are in transition. 1 is full screen is covered in black and 0 is not covered in black at all.			
percenttarget	= 1.3;										//Target percent for the black bars to go towards
target			= room;										//Target for the transition

//Variables For Side Transition
left			= 0;										//Controls how far left its drawn.
right			= 0;										//Controls how far right its drawn.
stagetwo		= 0;										//Stage 1 is covering the room in black from left to right while stage 2 is bringing the transition from left to right to the next room.
stg1_shape_spd	= 60;										//Speed of the rectange at stage one of transition
stg2_shape_spd	= 60;										//Speed of the rectange at stage one of transition
play_snd_once	= 0;										//Sets to play sound effect once each stage. 0 is first stage, 1 is the second stage, 2 is its finished
#endregion