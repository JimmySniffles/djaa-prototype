/// @description Drawing Black Bars

#region Centre Transition Drawing Black Bars
if (type == TRANS_TYPE.CENTRE) {
	if (mode != TRANS_MODE.OFF) {
		draw_set_color(c_black);
		draw_rectangle(0,0,width,percent*height_half,false);
		draw_rectangle(0,height,width,height-(percent*height_half),false);	
	}
}
#endregion

#region Side Transition Drawing Black Bars
if (type == TRANS_TYPE.SIDE) {
	
	#region Stage One Transition
	draw_set_color(c_black);
	var x1,x2,y1,y2;
	x1		= (gui_width - gui_width) + left;			//Left of the screen coordinate
	x2		= (gui_width - gui_width) + right;			//Left of the screen coordinate
	y1		= gui_height - gui_height;					//Top of the screen coordinate
	y2		= gui_height;								//Bottom of the screen coordinate

	right	+= stg1_shape_spd;							//Speed of the rectange that increases to the right (higher the number faster the transition)
	left	+= stg1_shape_spd * stagetwo;				//Speed of the rectangle that increases to the left (higher the number faster the transition)

	draw_rectangle(x1,y1,x2,y2,0);

	if (play_snd_once == 0) {
		if (!audio_is_playing(sn_trans_nw_v1)) && (!audio_is_playing(sn_trans_nw_v2)) && (!audio_is_playing(sn_trans_nw_v3)) {
			audio_play_sound(choose(sn_trans_nw_v1,sn_trans_nw_v2,sn_trans_nw_v3),8,false);
		}
		play_snd_once = 1;
	}
	#endregion

	#region Stage Two Transition
	if (right > gui_width + stg2_shape_spd) && (!stagetwo)
	{
		stagetwo = 1;
		room_goto(target);
	
		if (play_snd_once == 1) && (stagetwo == 1) {
			if (!audio_is_playing(sn_trans_nw_v1)) && (!audio_is_playing(sn_trans_nw_v2)) && (!audio_is_playing(sn_trans_nw_v3)) {
				audio_play_sound(choose(sn_trans_nw_v1,sn_trans_nw_v2,sn_trans_nw_v3),8,false);
			}
			play_snd_once = 2;
		}
	}
	//Transition Finalized
	if (left > gui_width + stg2_shape_spd) { play_snd_once = 0; }
	#endregion
}	
#endregion