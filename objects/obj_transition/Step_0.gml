/// @description Progress The Transition

#region Moving The Black Rectangle For Transition & Setting The Target Transition & Type
if (type == TRANS_TYPE.CENTRE) {
	if (mode != TRANS_MODE.OFF) {
		if (mode == TRANS_MODE.INTRO) {
			//Fade 1 to 0. Percent being progress of black bar transition. Max returns the biggest value.
			percent = max(0,percent - max((percent/15),0.005));	
		}
		else {
			//Fade 0 to 1. Percent being progress of black bar transition. Min returns the smallest value.
			percent = min(percenttarget,percent + max(((percenttarget - percent)/20),0.005));
		}
	
		if (percent == percenttarget) || (percent == 0) {
			switch (mode) {
				case TRANS_MODE.INTRO: { mode = TRANS_MODE.OFF; break; }
				case TRANS_MODE.NEXT: { mode = TRANS_MODE.INTRO; room_goto_next(); break; }
				case TRANS_MODE.GOTO: { mode = TRANS_MODE.INTRO; room_goto(target); break; }
				case TRANS_MODE.ROOMRESTART: { mode = TRANS_MODE.INTRO; room_restart(); break; }
				case TRANS_MODE.GAMERESTART: { mode = TRANS_MODE.INTRO; game_restart(); break; }	
				case TRANS_MODE.DEATH: { mode = TRANS_MODE.INTRO; scr_checkpoint(); break; }
			}
		}
	}
}
#endregion