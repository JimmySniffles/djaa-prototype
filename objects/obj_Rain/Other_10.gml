//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Hello Budburra. Would you like to hear a tale?";
		mySpeaker[i]	= id;
		
		//Line 1
		i++;
		myText[i]		= ["Sure", "Nope"];
		myTypes[i]		= 1;
		myNextLine[i]	= [2,11];
		myScripts[i]	= [[change_variable, id, "choice_variable", "sure"], [change_variable, id, "choice_variable", "nope"]];
		mySpeaker[i]	= obj_boy;
		
		//Line 2
		i++;
		myText[i]		= "Ok. Back thousands of years ago, There was a man named Quen.";
		myNextLine[i]	= 3;
		mySpeaker[i]	= id;
		
		//Line 3
		i++;
		myText[i]		= "He was seeking a ancient creature, the rainbow serpant.";
		myNextLine[i]	= 4;
		mySpeaker[i]	= id;
		
		//Line 4
		i++;
		myText[i]		= "After a few hours of searching.";
		myNextLine[i]	= 5;
		mySpeaker[i]	= id;
		
		//Line 5
		i++;
		myText[i]		= "He found it, the first person to see it so quickly.";
		myNextLine[i]	= 6;
		mySpeaker[i]	= id;
		
		//Line 6
		i++;
		myText[i]		= "Simply laying its head on the beach side.";
		myNextLine[i]	= 7;
		mySpeaker[i]	= id;
		
		//Line 7
		i++;
		myText[i]		= "At a burst of ectasy he ran at the creature.";
		myNextLine[i]	= 8;
		mySpeaker[i]	= id;
		
		//Line 8
		i++;
		myText[i]		= "However because of his excitment and lust he forgot his spear.";
		myNextLine[i]	= 9;
		mySpeaker[i]	= id;
		
		//Line 9
		i++;
		myText[i]		= "Let's just say it did not go well for Quen.";
		myNextLine[i]	= 10;
		mySpeaker[i]	= id;
		
		//Line 10
		i++;
		myText[i]		= "Moral of the story, don't go chasing things unprepared, not a good idea.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;

		//Line 11
		i++;
		myText[i]		= "Alrighty, have fun.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;
		break;
	#endregion
	
	#region Consequences For The Dialogue
		case "sure":
		#region If you chose sure
			var i = 0;
			//Line 0
			myText[i]		= "Have fun and remember be prepared.";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
	
		case "nope":
		#region If you chose nope
			var i = 0;
			//Line 0
			myText[i]		= "Have fun!";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
		}
		#endregion
#endregion