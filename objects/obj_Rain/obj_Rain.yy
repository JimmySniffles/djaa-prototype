{
    "id": "c8168870-e246-48d1-bdfb-f98eba1be6c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Rain",
    "eventList": [
        {
            "id": "6034ca41-57f2-4c25-8111-d481245838c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8168870-e246-48d1-bdfb-f98eba1be6c9"
        },
        {
            "id": "00bb7ef3-8cc6-4f69-a71e-ac34be6d6f63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c8168870-e246-48d1-bdfb-f98eba1be6c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "9811d33a-5932-4f9b-bff6-088f66ee837d",
    "visible": true
}