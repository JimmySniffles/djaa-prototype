/// @description Random Movement
#region Initialize Variables
var pdirection = point_direction(x,y,startx,starty);
var directionmovement_h = lengthdir_x(1,pdirection);
var directionmovement_v = lengthdir_x(1,pdirection);
#endregion

#region Movement Calculations
hsp_msp = scr_approach(hsp_msp,directionmovement_h * hsp_maxsp,hsp_acc);
hsp = hsp_msp;

vsp_msp = scr_approach(vsp_msp,directionmovement_v * vsp_maxsp,vsp_acc);
vsp = vsp_msp;

if (abs(starty - y) < abs(vsp)) vsp = starty - y;
if (abs(startx - x) < abs(hsp)) hsp = startx - x;

scr_fractions();

x += hsp;
y += vsp;
#endregion

#region Image_scale And Random Movement
if (hsp != 0) image_xscale = sign(hsp);

if (startx == x) || (starty == y)
{
	bored++;	
	if (random(1)) < (-0.5 + (bored/100))
	{
		bored = 0;
		startx = x + irandom_range(-400,400);
		starty = y + irandom_range(-400,400);
	}
}
#endregion

scr_collision();