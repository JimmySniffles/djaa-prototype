/// @description Play Wing Flap Sound When Animation Ends

#region If Boy Exists
if (instance_exists(obj_boy)) {
	if (distance_to_object(obj_boy) <= hearingrange) {
		if (!audio_is_playing(sn_wingflap_v1)) && (!audio_is_playing(sn_wingflap_v2)) && (!audio_is_playing(sn_wingflap_v3)) && (!audio_is_playing(sn_wingflap_v4)) {
			audio_play_sound(choose(sn_wingflap_v1,sn_wingflap_v2,sn_wingflap_v3,sn_wingflap_v4),2,false);
		}
	}
}
#endregion

#region If Girl Exists
if (instance_exists(obj_girl)) {
	if (distance_to_object(obj_girl) <= hearingrange) {
		if (!audio_is_playing(sn_wingflap_v1)) && (!audio_is_playing(sn_wingflap_v2)) && (!audio_is_playing(sn_wingflap_v3)) && (!audio_is_playing(sn_wingflap_v4)) {
			audio_play_sound(choose(sn_wingflap_v1,sn_wingflap_v2,sn_wingflap_v3,sn_wingflap_v4),2,false);
		}
	}
}
#endregion