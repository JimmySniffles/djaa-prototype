{
    "id": "3e9ed20d-c786-4221-a0e6-205f54a06981",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bird",
    "eventList": [
        {
            "id": "768f96f2-2e30-4ac5-91a4-504d1852ee5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e9ed20d-c786-4221-a0e6-205f54a06981"
        },
        {
            "id": "ba5d8a33-0e05-4b6d-9bc6-2cba56ed3e13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e9ed20d-c786-4221-a0e6-205f54a06981"
        },
        {
            "id": "eec3fb7e-dde4-42a4-a5fd-2220304cbef3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "3e9ed20d-c786-4221-a0e6-205f54a06981"
        },
        {
            "id": "94e3585e-4d3f-4c7f-9a31-6c7f3e74beca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3e9ed20d-c786-4221-a0e6-205f54a06981"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
    "visible": true
}