{
    "id": "58e390e2-d59b-4c5e-8c14-e615ec956c38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dirtslide",
    "eventList": [
        {
            "id": "4b0a9ae3-0e0f-4968-a13e-f05c74cfa724",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "58e390e2-d59b-4c5e-8c14-e615ec956c38"
        },
        {
            "id": "fc756fbb-16cd-4319-b6cd-1ad769d10915",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "58e390e2-d59b-4c5e-8c14-e615ec956c38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
    "visible": true
}