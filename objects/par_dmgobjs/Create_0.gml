/// @description Intialize Variables

#region Setting Global Destructable Environment Variables
play_sound	= false;				//Play sound if set to true. This is to stop repeating sounds.
image_index = 0;					//Setting image index to 0. First frame.
image_speed = 0;					//Setting image speed to 0 so it does not animate through the frames.
last_img	= image_index;			//Sets the last image to be the current image index.
#endregion