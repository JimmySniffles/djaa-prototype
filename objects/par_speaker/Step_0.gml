var dr = detection_radius;
if instance_exists(player)
{
	if (point_in_rectangle(player.x, player.y, x-dr, y-dr, x+dr, y+dr)) {
		if (myTextbox != noone) { 
			if (!instance_exists(myTextbox)) { myTextbox = noone; exit; }
		}
		//if I haven't already created my textbox, make one:
		else if (global.key_left == false) && (global.key_right == false) && (global.key_jump == false) {
			if (global.computer_use) { if (global.key_interact) {
				if (instance_exists(obj_textbox)) { exit; }	//exit if a textbox already exists
				event_user(0);								//if you need variables to update for text
			
				//Hand over variables
				create_dialogue(myText, mySpeaker, myEffects, myTextSpeed, myTypes, myNextLine, myScripts, myTextCol, myEmotion);
			}
			}
			if (global.mobile_use) { if (global.key_runattack) {
				if (instance_exists(obj_textbox)) { exit; }	//exit if a textbox already exists
				event_user(0);								//if you need variables to update for text
			
				//Hand over variables
				create_dialogue(myText, mySpeaker, myEffects, myTextSpeed, myTypes, myNextLine, myScripts, myTextCol, myEmotion);
			}
			}
		}
	} else {	//if player moves outside of detection radius
		if (myTextbox != noone) {
			with (myTextbox) { instance_destroy(); }
			myTextbox = noone;
		}
	}
}