
//-----------Customise (FOR USER)
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl;  }

detection_radius	= 100;

myVoice				= -1;
myPortrait			= -1;
myFont				= fnt_dialogue;
myName				= "None";

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;
myPortraitIdle_x	= -1;
myPortraitIdle_y	= -1;

//-----------Defaults Setup (LEAVE THIS STUFF)
reset_dialogue_defaults();