/// @description Game Setup
//Collision tiles. Add all tiles to the screen
#macro TILE_SIZE 64
#macro TILE_MAX 16

tiles = sprite_get_width(spr_tilecollision) / TILE_SIZE;
heightstoget = tiles * TILE_SIZE;

//Setting surface variable to be drawn
surface = -1;

//Target Room
targetroom = rm_acknowledgment;

//Make Tile layer
var layerid = layer_create(0,"Tiles");
tilemapid = layer_tilemap_create(layerid,0,0,t_collision,tiles,1);

//Create Tiles
for (var i = 0; i <= tiles; i++) {
	tilemap_set(tilemapid, i, i, 0);
}