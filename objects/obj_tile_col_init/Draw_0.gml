/// @description Build height table then start game
if (!surface_exists(surface)) { surface = surface_create(TILE_SIZE * TILE_MAX, TILE_SIZE); }

surface_set_target(surface);
draw_clear_alpha(c_black,1);

draw_tilemap(tilemapid,0,0);
for (var i = heightstoget-1; i >= 0; i--) {
	var check = 0;
	while (check <= TILE_SIZE) {
		global.heights[i] = check;
		if (check == TILE_SIZE) break;
		if (surface_getpixel(surface,i,check) != c_black) break;
		check++;
	}
}

surface_reset_target();
surface_free(surface);

room_goto(targetroom);