{
    "id": "76ce6948-05eb-4531-bd11-38818bf93003",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_egrunt_boomerang",
    "eventList": [
        {
            "id": "5233eaa5-69fe-4dae-88fb-027c776726bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76ce6948-05eb-4531-bd11-38818bf93003"
        },
        {
            "id": "68dd3b71-ed2b-4fb3-baab-7cef4b9e6f6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76ce6948-05eb-4531-bd11-38818bf93003"
        },
        {
            "id": "38c3cff3-c287-4934-9542-67d5a4a78684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "76ce6948-05eb-4531-bd11-38818bf93003"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
    "visible": true
}