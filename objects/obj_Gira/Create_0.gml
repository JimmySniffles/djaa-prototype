event_inherited(); //so it will inherit from par_speaker

#region Setting State
if (state = "standing") { sprite_index = spr_gira_idle; }
if (state = "sitting") { sprite_index = spr_gira_sitting; }
#endregion

#region Dialogue Variables
myPortrait			= spr_portrait_gira;
myVoice				= -1;
myFont				= fnt_dialogue;
myName				= "Gira";

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;
#endregion

//-------OTHER

choice_variable		= -1;	//the variable we change depending on the player's choice in dialogue