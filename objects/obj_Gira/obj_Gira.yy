{
    "id": "20f358b6-9c73-4b9e-98fd-71ff7e10e4bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Gira",
    "eventList": [
        {
            "id": "f29d9517-bc6e-4be0-9fbd-bddd792d2e6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20f358b6-9c73-4b9e-98fd-71ff7e10e4bf"
        },
        {
            "id": "285588f9-8c19-4493-b280-2eae0f4ae04b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "20f358b6-9c73-4b9e-98fd-71ff7e10e4bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
    "visible": true
}