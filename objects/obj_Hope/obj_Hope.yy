{
    "id": "ef2e5af6-0735-4cde-9f73-289513fab2f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Hope",
    "eventList": [
        {
            "id": "2ffee064-2192-4188-8be2-f356b0f2ab8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef2e5af6-0735-4cde-9f73-289513fab2f1"
        },
        {
            "id": "99d75124-af19-4077-8e04-21928e0b5681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ef2e5af6-0735-4cde-9f73-289513fab2f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
    "visible": true
}