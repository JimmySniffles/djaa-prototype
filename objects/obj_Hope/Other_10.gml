//---You can update variables here!---//
reset_dialogue_defaults();
/*
if (room = rm_level_1) {
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Brother, must you always be late with everything.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 1
		i++;
		myText[i]		= "I'm not always late. Just occasionally from time to time. Are you excited?";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 0;
		
		//Line 2
		i++;
		myText[i]		= "I'm just teasing. Yeah, can not wait.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 3
		i++;
		myText[i]		= "How about you? Do you think your ready?";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 4
		i++;
		myText[i]		= "Well besides oversleeping, I know I am, I won't let dad down.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 0;
		
		//Line 5
		i++;
		myText[i]		= "I'm sure you will do well, good luck.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 6
		i++;
		myText[i]		= "You too. See you soon.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
	#endregion
	
	#region The Dialogue
	case "dialogueplayed":
		#region If you chose alright
		var i = 0;
		myText[i]		= "See ya.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 1
		i++;
		myText[i]		= "Bye.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		break;
		#endregion
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		}
	#endregion
}
#endregion