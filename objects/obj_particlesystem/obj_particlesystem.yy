{
    "id": "f363244d-cdb3-4093-85fa-0345e1b2a939",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particlesystem",
    "eventList": [
        {
            "id": "1407e625-5f7a-4daf-b72f-863612f21f77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f363244d-cdb3-4093-85fa-0345e1b2a939"
        },
        {
            "id": "dc99a23d-01f5-411f-86b8-b66e0e883b1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "f363244d-cdb3-4093-85fa-0345e1b2a939"
        },
        {
            "id": "38157772-df68-47b1-a392-4db7666eae01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "f363244d-cdb3-4093-85fa-0345e1b2a939"
        },
        {
            "id": "63e36c19-9553-481f-84c8-586e2bbd3421",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f363244d-cdb3-4093-85fa-0345e1b2a939"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}