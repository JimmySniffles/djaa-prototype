/// @description Particle Parameters

#region Fire Particle System
partFire_sys = part_system_create_layer("partsysFire", false);
partFire_typ = part_type_create();

//Fire Particle
part_type_shape(partFire_typ,pt_shape_line);
part_type_size(partFire_typ,0.20,0.60,0,0);
part_type_scale(partFire_typ,0.65,2.50);
part_type_color3(partFire_typ,16749459,33023,255);
part_type_alpha3(partFire_typ,0.04,0.06,0.07);
part_type_speed(partFire_typ,0.70,2.60,-0.02,0);
part_type_direction(partFire_typ,85,95,0,9);
part_type_gravity(partFire_typ,0,270);
part_type_orientation(partFire_typ,0,0,10,20,1);
part_type_blend(partFire_typ,1);
part_type_life(partFire_typ,50,100);

fire_emitter = part_emitter_create(partFire_sys);
part_emitter_region(partFire_sys,fire_emitter,1090,1250,780,770,ps_shape_ellipse,1);
#endregion

#region Smoke Particle System
partSmoke_sys = part_system_create_layer("partsysSmoke", false);
partSmoke_typ = part_type_create();

//Smoke Particle
part_type_shape(partSmoke_typ,pt_shape_smoke);
part_type_size(partSmoke_typ,0.80,1,0,0);
part_type_scale(partSmoke_typ,1,1);
part_type_color2(partSmoke_typ,1973790,8226430);
part_type_alpha2(partSmoke_typ,0.01,1);
part_type_speed(partSmoke_typ,2,3,0,0);
part_type_direction(partSmoke_typ,80,110,0,0);
part_type_gravity(partSmoke_typ,0,0);
part_type_orientation(partSmoke_typ,0,0,0,0,1);
part_type_blend(partSmoke_typ,0);
part_type_life(partSmoke_typ,240,260);

smoke_emitter = part_emitter_create(partSmoke_sys);
part_emitter_region(partSmoke_sys,smoke_emitter,1110,1220,500,680,ps_shape_rectangle,0);
#endregion

#region Rain Particle && Puddle System
//Watch This Tutorial For Info On This System: https://www.youtube.com/watch?v=C9fAy9aUdwo
//Depth setting if you want the rain above or below layers of artwork
//part_system_depth(partRain,-10);

partRain_sys = part_system_create_layer("partsysRain", false);

//Rain Particle
partRain_typ = part_type_create();
part_type_shape(partRain_typ,pt_shape_line);
part_type_size(partRain_typ,0.1,0.2,0,0); //Originally was 0.2 and 0.3
part_type_color2(partRain_typ,c_white, c_white);
part_type_alpha2(partRain_typ,0.5,0.1);
part_type_gravity(partRain_typ,0.1,290);
part_type_speed(partRain_typ,0.5,0.5,0,0);
part_type_direction(partRain_typ,250,330,0,1);
part_type_orientation(partRain_typ,290,290,0,0,0);
part_type_life(partRain_typ,180,180);

//Create Emitter
rain_emitter = part_emitter_create(partRain_sys);

//Advance System | Advances the rain particle system so the particles have fallen almost to the bottom before entering the room
repeat (room_speed * 3){
    part_system_update(partRain_sys);
}
#endregion