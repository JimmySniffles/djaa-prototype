/// @description Destroys particle systems and types once room ends, prevents memory leaks.
part_system_destroy(partFire_sys);
part_system_destroy(partSmoke_sys);
part_system_destroy(partRain_sys);

part_type_destroy(partFire_typ);
part_type_destroy(partSmoke_typ);
part_type_destroy(partRain_typ);