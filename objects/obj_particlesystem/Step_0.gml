/// @description Particle System Being Set depending On Rooms And So Forth

#region Getting Camera Width And Height
if (instance_exists(obj_camera)) { var camerawidth = obj_camera.view_width, var cameraheight = obj_camera.view_height; }
#endregion

#region Fire and Smoke Particle Effect In The Main Menu and Credits
if (room == rm_menu) || (room == rm_mcredits) {
part_emitter_stream(partFire_sys,fire_emitter,partFire_typ,15);
part_emitter_stream(partSmoke_sys,smoke_emitter,partSmoke_typ,3);
}
#endregion

#region Rain Particle System Depending On Levels Coded Within Region
//Updating rain emitter so the system moves with a moving view by updating its y location each step
//Update Emitter
/*
if (room = rm_level_1b) && (instance_exists(obj_camera)) {
part_emitter_region(partRain_sys,rain_emitter,camerawidth - 800,camerawidth+camerawidth + 2000,cameraheight+150,cameraheight+150,ps_shape_line,ps_distr_linear);
part_emitter_burst(partRain_sys,rain_emitter,partRain_typ,8);
}*/
#endregion