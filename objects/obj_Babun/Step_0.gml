/// @description Inherited Code From par_speaker
event_inherited(); //so it will inherit from par_speaker

//Setting facing direction towards player
if (instance_exists(obj_boy)) { image_xscale = sign(obj_boy.x - x); }