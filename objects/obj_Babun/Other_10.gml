//---You can update variables here!---//
reset_dialogue_defaults();

if (room == rm_level_1a) && (tut_text_display == 0) {
#region Level 1a Dialogue
switch(choice_variable){
	case -1:
	#region Dialogue
	
		//Line 0
		var i = 0;
		myText[i]		= "Son, take this club. You will need it for your journey.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		myScripts[i]	= [create_instance_layer, x-20, y, "Interactive", obj_weapon_pkup];
		
		//Line 1
		i++;
		myText[i]		= "Deadly!";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		
		//Line 2
		i++;
		myText[i]		= "Make sure you take care of it.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 3
		i++;
		if (global.computer_use) { myText[i] = "Press left mouse button to attack or right mouse button to do a special attack."; }
		if (global.mobile_use) { myText[i] = "Swipe right or left near the middle of the screen to use it."; }
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 4
		i++;
		myText[i]		= "Use the club to break the boulder behind me.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 5
		i++;
		myText[i]		= "You sure, that's a massive boulder?";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 0;
		
		//Line 6
		i++;
		myText[i]		= "I weakened it for you.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 7
		i++;
		myText[i]		= "Be careful there are snakes ahead. Use your club to protect yourself.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 8
		i++;
		myText[i]		= "Aahhhh, snakes?";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 2;
		myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
		#endregion

	#region Consequences For The Dialogue
		case "dialogueplayed":
			
			//Line 0
			var i = 0;
			myText[i] = "You still here son?";
			mySpeaker[i]	= id;
			myEmotion[i]	= 1;
			
			//Line 1
			i++;
			if (global.computer_use) { myText[i] = "Press left mouse button to attack or right mouse button to do a special attack."; }
			if (global.mobile_use) { myText[i] = "Swipe right or left near the middle of the screen to use the club."; }
			mySpeaker[i]	= id;
			myEmotion[i]	= 0;
			
			//Line 2
			i++;
			myText[i] = "Thanks for the reminder.";
			mySpeaker[i]	= obj_boy;
			myEmotion[i]	= 1;
					
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		break;
		#endregion
		}
#endregion
}

if (room == rm_level_1a) && (tut_text_display == 1) {
#region Level 1a Dialogue 2
switch(choice_variable){
	case -1:
	#region Dialogue
	
		//Line 0
		var i = 0;
		myText[i]		= "Some plants can help you.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 1
		i++;
		myText[i]		= "Only eat plants you know are safe.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
		#endregion
	
	#region Consequences For The Dialogue
		case "dialogueplayed":
		
			//Line 0
			var i = 0;
			myText[i] = "Take care son.";
			mySpeaker[i]	= id;
			myEmotion[i]	= 1;
								
			//Line 1
			i++;
			myText[i] = "See you soon.";
			mySpeaker[i]	= obj_boy;
			myEmotion[i]	= 1;

			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		break;
		#endregion
		}
#endregion
}

if (room == rm_level_1b) && (tut_text_display == 0) {
#region Level 1b Dialogue
switch(choice_variable){
	case -1:
	#region Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Excellent work son on reaching this far.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 1
		i++;
		myText[i]		= "Thank you father.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		
		//Line 2
		i++;
		myText[i]		= "Were now up for the main event.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 3
		i++;
		myText[i]		= "The following path is treacherous";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 4
		i++;
		myText[i]		= "and theres a surprise for you at the end.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 5
		i++;
		myText[i]		= "Pass it and you will succeed in your trial.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 6
		i++;
		myText[i]		= "I got this.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		
		//Line 9
		i++;
		myText[i]		= "Be careful son, do not let your guard down now.";
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 10
		i++;
		myText[i]		= "Will do. See you soon.";
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
		#endregion
		
	#region Consequences For The Dialogue
		case "dialogueplayed":
			var i = 0;
			//Line 0
			myText[i] = "Good luck. Remember use those berries to help you.";
			mySpeaker[i]	= id;
			myEmotion[i]	= 1;
			
			//Line 1
			i++;
			myText[i] = "Okay, see you soon.";
			mySpeaker[i]	= obj_boy;
			myEmotion[i]	= 1;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		break;
		#endregion
		}
#endregion
}