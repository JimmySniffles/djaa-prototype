/// @description Enables Views, Sets View To Visible & Gets & Stores view_camera As Global Variable
//Enables views and sets view to visible
//Gets ID of camera when its created. View_camera[] which is an array holds the unique camera ID assigned to the given view port
view_enabled			= true;
view_visible[0]			= true;
global.camera_view		= view_camera[0];