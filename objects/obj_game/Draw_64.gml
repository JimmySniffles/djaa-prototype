/// @description Mobile Display UI & Swiping Debuging

#region Drawing Mobile Display UI Buttons
if (global.mobile_use == true) {
	if (instance_exists(obj_boy)) { player = obj_boy; } if (instance_exists(obj_girl)) { player = obj_girl; }
	if (player != 0) {
		if (instance_exists(player)) {
			if (player.state != states.death) && (player.hascontrol == true) {
				//Drawing Pause Button
				draw_sprite_ext(spr_pause_button,0,pause_button_x,pause_button_y,1,1,0,c_white,0.8);
			
				//Drawing Side Rectangles
				if (player.key_right == 0) && (player.key_left == 0) {
					move_display_ui_timer ++;
					if (move_display_ui_timer >= max_display_ui_timer) {
						move_alpha = min(move_alpha + 0.01, 0.2); 
						draw_set_alpha(move_alpha);
						draw_rectangle_color(move_right_x,0,disp_w,disp_h,c_gray, c_gray, c_gray, c_gray,false);
						draw_rectangle_color(0,0,0 + move_left_x,disp_h,c_gray, c_gray, c_gray, c_gray,false);
						//Drawing arrows for sides
						draw_sprite_ext(spr_arrow_ui,0,disp_w - (move_left_x * 0.5),disp_h - ((disp_h - jumping_section) * 0.5),1,1,0,c_white,move_alpha);
						draw_sprite_ext(spr_arrow_ui,0,move_left_x - (move_left_x * 0.5),disp_h - ((disp_h - jumping_section) * 0.5),-1,1,0,c_white,move_alpha);
						draw_set_alpha(1);
					}
				} else { move_display_ui_timer = 0; if (move_alpha > 0 ) { move_alpha = min(move_alpha - 0.01, 0); } }
			
				//Drawing Bottom Jump Rectangle
				if (player.key_jump_held == 0) {
					jump_display_ui_timer ++;
					if (jump_display_ui_timer >= max_display_ui_timer) {
						jump_alpha = min(jump_alpha + 0.01, 0.2); 
						draw_set_alpha(jump_alpha);
						draw_rectangle_color(0 + move_left_x + 1,jumping_section,move_right_x - 1,disp_h,c_gray, c_gray, c_gray, c_gray,false);
						//Drawing arrows for sides
						draw_sprite_ext(spr_arrow_ui,0,(disp_w * 0.5),disp_h - ((disp_h - jumping_section) * 0.5),1,1,90,c_white,jump_alpha);
						draw_set_alpha(1);
					}
				} else { jump_display_ui_timer = 0; if (jump_alpha > 0) { jump_alpha = min(jump_alpha - 0.01, 0); } }
			}
		}
	}
}
#endregion

#region Debugging Swipe Cursor
///Draw Swipe Cursor
/*
draw_set_colour(c_orange);
draw_line(mousex_start, mousey_start, mousex, mousey);

draw_set_colour(c_black);
draw_set_alpha(0.6);
draw_rectangle(0, 0, 210, 75, 0);
draw_set_alpha(1.0);
 
draw_set_font(fnt_credits);

draw_set_halign(fa_left);
draw_set_valign(fa_center);

draw_set_colour(c_white);

draw_text(8, 12, "Swipe = " + string(swipe));
draw_text(8, 34, "Swipe Speed = " + string(swipe_spd));
draw_text(8, 58, "Swipe Direction = " + string(swipe_dir));
*/
#endregion