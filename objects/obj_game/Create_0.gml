/// @description Setting Macros & Game Intialisation
//The difference between macro and variable is that variables can change.
//Macros can be thought of as a constant that can never change to a certain point.
//How macros function is when you execute/run your game the name of the macro is changed to the set number. For example when the game is run, RES_W will be removed and changed to 1920.

#region Setting Intialization For Game
///Display & Resolution Properties
//You can either set the ideal width or height and calculate the other one depending upon aspect ratio of the display
ideal_width			= 1920 / 6;
ideal_height		= 1080 / 6;		
window_scale		= 5;
max_window_scale	= 1;

//Aspect Ratio
//aspect_ratio		= display_get_width()/ display_get_height();
 
//Calculate New Ideal Width Or Height.
//ideal_width		= round(ideal_height*aspect_ratio);
//ideal_height		= round(ideal_width/aspect_ratio);
//ideal_width		= round(ideal_width);
//ideal_height		= round(ideal_height);

//Pixel Pefect Scaling.
if (display_get_width() mod ideal_width != 0) {
	var display = round(display_get_width() / ideal_width);
	ideal_width = display_get_width()/ display;
}

if (display_get_height() mod ideal_height != 0) {
	var display = round(display_get_height() / ideal_height);
	ideal_height = display_get_height()/ display;
}
 
//Check to make sure our ideal width and height isn't an odd number, as that's usually not good.
if (ideal_width & 1) { ideal_width ++; }
if (ideal_height & 1) { ideal_height ++; }

//Setting max window scaled up.
max_window_scale	= floor(display_get_width()/ideal_width);
 
//Resize application surface, display gui size and window size to the ideal width and ideal height aspect ratio and resolution
window_set_size(ideal_width * window_scale,ideal_height * window_scale);
alarm[0] = 1;
surface_resize(application_surface,ideal_width * window_scale,ideal_height * window_scale);
display_set_gui_size(ideal_width * window_scale,ideal_height * window_scale);

//Checking Current Platform Use
global.computer_use = false;
global.mobile_use	= false;
if (os_type = os_windows) || (os_type = os_macosx) || (os_type = os_linux) || (os_type = os_uwp) { global.computer_use = true; }
if (os_type = os_android) || (os_type = os_ios) { global.mobile_use = true; }

//Randomizing Seed. Please note, that when using the random number functions in GameMaker Studio 2 the initial seed is always the same, as this makes tracing errors and debugging far easier.
randomize();

//Setting Up Macros
#macro SAVEFILE "Save.sav"

//Setting Custom Cursor
if (global.computer_use) {
	window_set_cursor(cr_none);
	cursor_sprite = spr_customcursor;
}

//Setting global window mode variable
global.windowsmode = 0;	

//Setting Global Checkpoint Variables
global.checkpoint = noone;		
global.checkpointR = 0;
global.checkpointx = 0;
global.checkpointy = 0;

//Listing Rooms For Particle Effects
partRoom[1] = rm_mcredits;
partRoom[0] = rm_menu;

//Checking that shaders have been compiled
global.shaders = false;
if (shaders_are_supported()) {
	if (shader_is_compiled(shd_flash)) {
		global.shaders = true;
	}
}
#endregion

#region Setting States For Objects

	#region Player, Enemies & Animals
		//States for Budburra and Hope
		enum states {	
			normal,
			attack_stg1,
			attack_stg2,
			attack_stg3,
			gslide,
			dodge,
			knockback,
			death,
			push,
			climb,
			interact
		}

		//States for enemies
		enum e_states {
			normal,
			alert,
			chase,
			attack,
			knockback,
			stun,
			death
		}

		//States for animals
		enum a_states {
			normal,
			alert,
			scared,
			chase,
			attack,
			knockback,
			stun,
			death
		}
	#endregion
	
	#region Tools
	//States for stun boomerang
	enum stunboomerang_states {
		thrown,
		returning
	}

	//States for spear
	enum spear_states {
		thrown,
		buried
	}

	//States for kill boomerang
	enum killboomerang_states {
		thrown,
		returning
	}
	#endregion
	
#endregion
	
#region Input Intialisation
disp_w = display_get_gui_width();						//Gets display gui width
disp_h = display_get_gui_height();						//Gets display gui height

//Move Left Section X,Y
move_left_x = disp_w * 0.15;							//Which is 153.6 with display width of 1536

//Move Right Section X,Y
move_right_x = disp_w - (disp_w * 0.15);				//Which is 998.4 with display width of 1536

//Top Jumping Section
jumping_section = disp_h - (disp_h * 0.2);				//Which is 864 - 345.6 = 518.4

//Checking if the touch is true while in the touch area for moving left, right, jumping, tapping and swiping directions
move_left				= false;
move_right				= false;
jump_pressed			= false;
jump_held				= false;
tap_check				= false;	
double_tap_check		= false;						//Checks if player double taps quickly
swipe_attack			= false;
swipe_spattack			= false;
swiped_up				= false;
swiped_down				= false;

//Mouse Swipe Control Variables
mousex					= 0;							//Gets mouse x position
mousey					= 0;							//Gets mouse y position
mousex_start			= 0;							//Gets mouse x start position
mousey_start			= 0;							//Gets mouse y start position

//If swipe is true: mousex and mousey equals current mouse x and y position. Swipe time goes up. Swipe_dir is point_direction of the mouse start x, start y and current x and y positions.
//if swipe is true and the user releases the mouse button or stops pressing the screen. Swipe is returned to false
//Swipe Speed is equal to the point of distance between the starting x and y points of the mouse and the current x and y points devided by the time it took to get to the points (the longer the time, the slower the swipe)
//Swipe time is returned to 0

swipe					= false;						//Checks if beginning to swipe				
swipe_time				= 0;							//Swipe time
swipe_spd				= 0;							//Swipe speed
swipe_dir				= 0;							//Point of Direction
input_swipe_check		= 0;							//Checks the number of which swipe input is in use

player					= 0;							//Variable for checking if player exists for drawing UI
max_display_ui_timer	= room_speed * 1.5;				//Sets maximum amount of time to display mobile ui buttons
move_display_ui_timer	= max_display_ui_timer;			//Sets time to display move mobile ui buttons
jump_display_ui_timer	= max_display_ui_timer;			//Sets time to display jump mobile ui buttons
move_alpha				= 0;							//Setting move alpha for drawing
jump_alpha				= 0;							//Setting jump alpha for drawing
pause_button_x			= 0 + 20;						//Setting pause button x position
pause_button_y			= 0 + 20;						//Setting pause button y position

/*
// WindowsOS, MacOS, Linux, Windows 10 Universal Windows Platform Input
if (global.computer_use) {
	global.key_left			=	ord("A");
	global.key_right		=	ord("D");
	global.key_jump			=	vk_space;
	global.key_jump_held	=	global.key_jump;
	global.key_down			=	ord("S");
	global.key_up			=	ord("W");
	global.key_sprint		=	vk_shift;
	global.key_attack		=	mb_left;
	global.key_runattack	=	mb_middle;
	global.key_spattack		=	mb_right;
	global.key_interact		=	ord("E");
	global.key_switch		=	ord("Q");
}

//Anroid Or iOS Mobile Input
if (global.mobile_use) {
	global.key_left			=	move_left;
	global.key_right		=	move_right;
	global.key_jump			=	jump_pressed;
	global.key_jump_held	=	jump_held;
	global.key_down			=	swiped_down;
	global.key_up			=	swiped_up;	
	global.key_sprint		=	-1;
	global.key_attack		=	swipe_attack;
	global.key_runattack	=	double_tap_check;
	global.key_spattack		=	swipe_spattack;
	global.key_interact		=	tap_check;
	global.key_switch		=	-1;
}*/
#endregion