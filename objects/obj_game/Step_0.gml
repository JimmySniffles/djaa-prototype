/// @description Global Intialisation, Object Creation & Inputs

#region Creating Objects Based Upon Intialization, Room Or Button Press
//Creating Intialization Objects
if (room == rm_initialize) {
	if (!instance_exists(obj_music)) { instance_create_layer(x,y,"Intialize",obj_music); }
	if (instance_exists(obj_music)) && (!instance_exists(obj_tile_col_init)) { instance_create_layer(x,y,"Intialize",obj_tile_col_init); }
}

//Creating Pause Menu If Button Pressed
if (!instance_exists(obj_cutscene)) && (!instance_exists(obj_menu)) && (!instance_exists(obj_credits)) {
	if (global.computer_use) {
		if (keyboard_check_pressed(vk_escape)) || (keyboard_check_pressed(ord("P"))) || (keyboard_check_pressed(vk_pause)) || (keyboard_check_pressed(vk_end)) {
			if (!instance_exists(obj_pause_menu)) { instance_create_layer(x,y,"Menu",obj_pause_menu); }
		}
	}
	
	if (global.mobile_use) {
		if (scr_sprite_button_detect(pause_button_x,pause_button_y,spr_pause_button)) || (keyboard_check_pressed(vk_pause)) || (keyboard_check_pressed(vk_end)) {
			if (!instance_exists(obj_pause_menu)) { instance_create_layer(x,y,"Menu",obj_pause_menu); }
		}
	}
}

//Deactivating Or Reactivating The Transition Object
if (room == rm_mcredits) { if (instance_exists(obj_transition)) { instance_deactivate_object(obj_transition); } }

//Creating Particle System Object Based Upon Room
if (room == partRoom[0]) || (room == partRoom[1]) {
	if (!instance_exists(obj_particlesystem)) { instance_create_layer(x,y,"Intialize",obj_particlesystem); }
}

//Creating Weapon Loadout If Does Not Exist
if (room == rm_level_1b) { if (!instance_exists(obj_b_wloadout)) { instance_create_layer(x,y,"Player",obj_b_wloadout); } }
#endregion

#region Developer Tools. Fullscreen toggle when the 'F' key is pressed. Restart Room when 'R' is pressed. Kills player if 'G' key is pressed
/*
//Fullscreen Toggle
//Returns either true or false, that gets the fullscreen and swaps the opposite of it and applies it to the fullscreen.
//Great for codes that have toggling if statement and else statement.
if keyboard_check_pressed(ord("F")) { window_set_fullscreen(!window_get_fullscreen()); }													 

//Restart Level Toggle
if keyboard_check_pressed(ord("R")) { scr_slidetransition(TRANS_MODE.ROOMRESTART); }

//Kill Player Key
if (keyboard_check_pressed(ord("G"))) { if (instance_exists(obj_boy)) obj_boy.hp = 0; }
if (keyboard_check_pressed(ord("G"))) { if (instance_exists(obj_girl)) obj_girl.hp = 0; }
*/
#endregion

#region Input Controls Intialization

	#region WindowsOS, MacOS, Linux, Windows 10 Universal Windows Platform Input
	if (global.computer_use) {
	global.key_left			=	keyboard_check(ord("A")) || keyboard_check(vk_left);
	global.key_right		=	keyboard_check(ord("D")) || keyboard_check(vk_right);
	global.key_jump			=	keyboard_check_pressed(vk_space)
	global.key_jump_held	=	keyboard_check(vk_space)
	global.key_down			=	keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down);
	global.key_up			=	keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up);
	global.key_sprint		=	keyboard_check_pressed(vk_shift);
	global.key_attack		=	mouse_check_button_pressed(mb_left);
	global.key_runattack	=	mouse_check_button_pressed(mb_middle) || keyboard_check_pressed(ord("X"));
	global.key_spattack		=	mouse_check_button_pressed(mb_right);
	global.key_interact		=	keyboard_check_pressed(ord("E"));
	global.key_switch		=	keyboard_check_pressed(ord("Q"));
	}
	#endregion
	
	#region Mobile Input Intialisation
	var mobile_x_gui_0 = device_mouse_x_to_gui(0);
	var mobile_x_gui_1 = device_mouse_x_to_gui(1);
	var mobile_y_gui_0 = device_mouse_y_to_gui(0);
	var mobile_y_gui_1 = device_mouse_y_to_gui(1);

	#region Move Left or Right
	if (device_mouse_check_button(0, mb_left)) {
		if (mobile_x_gui_0 <= move_left_x) { move_left = true; }
		if (mobile_x_gui_0 >= move_right_x) { move_right = true; }
	} else { 
		if (!device_mouse_check_button(1,mb_left)) && (move_left == true) { move_left = false; } 
		if (!device_mouse_check_button(1,mb_left)) && (move_right == true) { move_right = false; } 
	}
	
	//Checks if the original touch is on the opposite of the second touch and if so move in that direction
	if (device_mouse_check_button(0, mb_left)) {
		if (move_left == true) && (mobile_x_gui_0 >= move_right_x) { move_right = true; move_left = false; }
		if (move_right == true) && (mobile_x_gui_0 <= move_left_x) { move_left = true; move_right = false; }
	} else {
		if (mobile_x_gui_1 < move_right_x) { move_right = false; }
		if (mobile_x_gui_1 > move_left_x) { move_left = false; }
	}
	
	if (device_mouse_check_button(1, mb_left)) {
		if (mobile_x_gui_1 <= move_left_x) { move_left = true; }
		if (mobile_x_gui_1 >= move_right_x) { move_right = true; }
	} else { 
		if (!device_mouse_check_button(0,mb_left)) && (move_left == true) { move_left = false; } 
		if (!device_mouse_check_button(0,mb_left)) && (move_right == true) { move_right = false; } 	
	}
	
	//Checks if second touch is held at the opposite side and if so move in that direction
	if (device_mouse_check_button(1, mb_left)) {
		if (move_left == true) && (mobile_x_gui_1 >= move_right_x) { move_left = false; move_right = true; }
		if (move_right == true) && (mobile_x_gui_1 <= move_left_x) { move_right = false; move_left = true; } 
	} else {
		if (mobile_x_gui_0 < move_right_x) { move_right = false; }
		if (mobile_x_gui_0 > move_left_x) { move_left = false; }
	}

	if (device_mouse_check_button(0, mb_left)) { show_debug_message("device 0 is active"); }
	if (device_mouse_check_button(1, mb_left)) { show_debug_message("device 1 is active"); }
	#endregion

	#region Jump
	//Jump Pressed
	if (device_mouse_check_button_pressed(0, mb_left)) {
		//Jump if neither side is being pressed and the position of the mouse is in neither side
		if (move_left == false) && (move_right == false) && (mobile_x_gui_0 > move_left_x) && (mobile_x_gui_0 < move_right_x) && (mobile_y_gui_0 >= jumping_section) { jump_pressed = true; }
	} else { jump_pressed = false; }

	if (device_mouse_check_button_pressed(1, mb_left)) {
		//Jump if one side is being pressed and the player taps outside of the held side
		if (move_left == true) && (mobile_x_gui_1 > move_left_x) && (mobile_y_gui_1 >= jumping_section) { jump_pressed = true; }
		if (move_right == true) && (mobile_x_gui_1 < move_right_x) && (mobile_y_gui_1 >= jumping_section) { jump_pressed = true; }
	}

	if (device_mouse_check_button_pressed(0, mb_left)) {
		//Jump if one side is being pressed and the player taps outside of the held side
		if (move_left == true) && (mobile_x_gui_0 > move_left_x) && (mobile_y_gui_0 >= jumping_section) { jump_pressed = true; }
		if (move_right == true) && (mobile_x_gui_0 < move_right_x) && (mobile_y_gui_0 >= jumping_section) { jump_pressed = true; }
	}

	//Jump Held
	if (device_mouse_check_button(0, mb_left)) {
		//Jump if neither side is being held and the position of the mouse is in neither side
		if (move_left == false) && (move_right == false) && (mobile_x_gui_0 > move_left_x) && (mobile_x_gui_0 < move_right_x) && (mobile_y_gui_0 >= jumping_section) { jump_held = true; } 
	} else { jump_held = false; }
	
	if (device_mouse_check_button(0,mb_left)) {
		//Jump if one side is being held and the player taps outside of the held side
		if (move_left == true) && (mobile_x_gui_0 > move_left_x) && (mobile_y_gui_0 >= jumping_section) { jump_held = true; }
		if (move_right == true) && (mobile_x_gui_0 < move_right_x) && (mobile_y_gui_0 >= jumping_section) { jump_held = true; }
	} else { if (device_mouse_check_button(1, mb_left)) { jump_held = false; } }

	if (device_mouse_check_button(1, mb_left)) {
		//Jump if one side is being held and the player taps outside of the held side
		if (move_left == true) && (mobile_x_gui_1 > move_left_x) && (mobile_y_gui_1 >= jumping_section) { jump_held = true; }
		if (move_right == true) && (mobile_x_gui_1 < move_right_x) && (mobile_y_gui_1 >= jumping_section) { jump_held = true; }
	} else { 
		if (device_mouse_check_button(0, mb_left)) && (move_left == true) { jump_held = false; } 
		if (device_mouse_check_button(0, mb_left)) && (move_right == true) { jump_held = false; } 
	}  
	#endregion

	#region Tap & Double Tap
	//Tap
	if (device_mouse_check_button_pressed(0,mb_left)) { tap_check = true; }
	if (device_mouse_check_button_pressed(1,mb_left)) { tap_check = true; }
	if (!device_mouse_check_button_pressed(0,mb_left)) && (!device_mouse_check_button_pressed(1,mb_left)) { tap_check = false; }
	
	//Double Tap
	if (device_mouse_check_button_pressed(0,mb_right)) { double_tap_check = true; }
	if (device_mouse_check_button_pressed(1,mb_right)) { double_tap_check = true; }
	if (!device_mouse_check_button_pressed(0,mb_right)) && (!device_mouse_check_button_pressed(1,mb_right)) { double_tap_check = false; }
	#endregion

	#region Mouse Swipe
	if (device_mouse_check_button_pressed(0, mb_left))
	{
		swipe = true;
		input_swipe_check = 1;
	    mousex_start = mobile_x_gui_0;
	    mousey_start = mobile_y_gui_0;
	}
	if (device_mouse_check_button_pressed(1, mb_left))
	{
		swipe = true;
		input_swipe_check = 2;
	    mousex_start = mobile_x_gui_1;
	    mousey_start = mobile_y_gui_1;
	}
	
	if (swipe = true)
	{
	if (input_swipe_check == 1) { mousex = mobile_x_gui_0; mousey = mobile_y_gui_0; }
	if (input_swipe_check == 2) { mousex = mobile_x_gui_1; mousey = mobile_y_gui_1; }
	swipe_time++;
	swipe_dir = point_direction(mousex_start, mousey_start, mousex, mousey);

		if (device_mouse_check_button_released(0, mb_left))
		{
			swipe = false;
		    swipe_spd = point_distance(mousex_start, mousey_start, mousex, mousey) / swipe_time;
		    swipe_time = 0;
			
			if (move_right) {
				//Swiping Right
				if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; }
				//Swiping Left
				if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; }
			}
			
			if (move_left) {
				//Swiping Right
				if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; }
				//Swiping Left
				if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; }
			}
			
			if (move_left == false) && (move_right == false) {
				//Swiping For Normal Attack. Swiping right if x position of mouse is passed half way of the screen or swiping left to attack if x position is not passed half way.
				//Swiping Right If Mobile X Position Is Passed Half Way Of Screen
				if (mobile_x_gui_0 >= disp_w*0.5) { if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; } }
				//Swiping Left If Mobile X Position Is Less Than Half Way Of Screen
				if (mobile_x_gui_0 < disp_w*0.5) { if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_attack = true; } }
		
				//Swiping For Special Attack. Swiping left if x position of mouse is passed half way of the screen or swiping right to do special attack if x position is not passed half way.
				//Swiping Left If Mobile X Position Is Passed Half Way Of Screen
				if (mobile_x_gui_0 >= disp_w*0.5) { if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; } }
				//Swiping Right If Mobile X Position Is Less Than Half Way Of Screen
				if (mobile_x_gui_0 < disp_w*0.5) { if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_spattack = true; } }
			}
			//Swiping Up
			if ((swipe_dir > 70) && (swipe_dir < 110)) && (swipe_spd > 4) { swiped_up = true; } 
			//Swiping Down
			if ((swipe_dir > 250) && (swipe_dir < 290)) && (swipe_spd > 4) { swiped_down = true; }
		}
		if (device_mouse_check_button_released(1, mb_left))
		{
			swipe = false;
		    swipe_spd = point_distance(mousex_start, mousey_start, mousex, mousey) / swipe_time;
		    swipe_time = 0;
			
			if (move_right) {
				//Swiping Right
				if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; }
				//Swiping Left
				if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; }
			}
			
			if (move_left) {
				//Swiping Right
				if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; }
				//Swiping Left
				if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; }
			}
			
			if (move_left == false) && (move_right == false) {
				//Swiping For Normal Attack. Swiping right if x position of mouse is passed half way of the screen or swiping left to attack if x position is not passed half way.
				//Swiping Right If Mobile X Position Is Passed Half Way Of Screen
				if (mobile_x_gui_1 >= disp_w*0.5) { if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_attack = true; } }
				//Swiping Left If Mobile X Position Is Less Than Half Way Of Screen
				if (mobile_x_gui_1 < disp_w*0.5) { if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_attack = true; } }
		
				//Swiping For Special Attack. Swiping left if x position of mouse is passed half way of the screen or swiping right to do special attack if x position is not passed half way.
				//Swiping Left If Mobile X Position Is Passed Half Way Of Screen
				if (mobile_x_gui_1 >= disp_w*0.5) { if ((swipe_dir > 110) && (swipe_dir < 250)) && (swipe_spd > 4) { swipe_spattack = true; } }
				//Swiping Right If Mobile X Position Is Less Than Half Way Of Screen
				if (mobile_x_gui_1 < disp_w*0.5) { if ((swipe_dir > 290) || (swipe_dir < 70)) && (swipe_spd > 4) { swipe_spattack = true; } }
			}
			//Swiping Up
			if ((swipe_dir > 70) && (swipe_dir < 110)) && (swipe_spd > 4) { swiped_up = true; } 
			//Swiping Down
			if ((swipe_dir > 250) && (swipe_dir < 290)) && (swipe_spd > 4) { swiped_down = true; }
		}
	} else { input_swipe_check = 0; swipe_attack = false; swipe_spattack = false; swiped_up = false; swiped_down = false; }
	#endregion

	#endregion

	#region Anroid Or iOS Mobile Input
	if (global.mobile_use) {
	global.key_left			=	move_left;
	global.key_right		=	move_right;
	global.key_jump			=	jump_pressed;
	global.key_jump_held	=	jump_held;
	global.key_down			=	swiped_down;
	global.key_up			=	swiped_up;	
	global.key_sprint		=	-1;
	global.key_attack		=	swipe_attack;
	global.key_runattack	=	double_tap_check;
	global.key_spattack		=	swipe_spattack;
	global.key_interact		=	tap_check;
	global.key_switch		=	-1;
	}
	#endregion

#endregion