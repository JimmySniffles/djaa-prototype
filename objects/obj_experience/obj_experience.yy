{
    "id": "c61ffd1f-557d-4d79-9946-7ae9c536d1b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_experience",
    "eventList": [
        {
            "id": "1bb4874c-f8ce-4f26-9576-e2a155eea8d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c61ffd1f-557d-4d79-9946-7ae9c536d1b9"
        },
        {
            "id": "8bc7402b-2140-433c-8bc1-bfcc9f354626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c61ffd1f-557d-4d79-9946-7ae9c536d1b9"
        },
        {
            "id": "e2bd5523-646d-4357-93a1-2b687202faa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c61ffd1f-557d-4d79-9946-7ae9c536d1b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "49e7410d-62e1-405c-aa47-677901846a44",
    "visible": true
}