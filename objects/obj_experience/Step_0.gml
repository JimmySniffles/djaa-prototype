/// @description Setting Movement & Flash

#region Flash Effect Timer & Destroy Self Timer
if (flashtimer < room_speed * 1) { flashtimer ++; }
if (flashtimer >= room_speed * 1) { flashtimer = 0; blueflash = 1; }

if (destroytimer < max_destroytimer) { destroytimer --; }
if (destroytimer <= 0) { instance_destroy(); }
#endregion

#region Setting Movement
if (hsp == 0)
{
	var hsp_fric_final = hsp_fric_ground;
	if (!onground) hsp_fric_final = hsp_fric_air;
	hsp = scr_approach(hsp,0,hsp_fric_final);
}
hsp = clamp(hsp,-hsp_max,hsp_max);

vsp += grv; 
vsp = clamp(vsp,-vsp_max,vsp_max);
#endregion

#region Calculate Current Status
//Is my middle center touching the floor at the start of this frame?
onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0); 
onplatform = (place_meeting(x,y+1,par_oneway_platform));
onobject = (place_meeting(x,y+1,par_objects));
#endregion

#region Setting Collision With Itself & Player
//if (instance_exists(obj_experience)) {
//	if (place_meeting(x,y,obj_experience)) { 
//		var dir = point_direction(obj_experience.x,obj_experience.y,x,y);
//		hsp = scr_approach(hsp,dir * hsp_max,hsp_acc);
//		vsp = scr_approach(vsp,dir * vsp_max,vsp_acc);
//	}
//}	

if (instance_exists(player)) {	
	if (collision_circle(x,y,detection_radius,player,false,true)) {
		var dir = point_direction(x,y,player.x,player.y);
		hsp = scr_approach(hsp,dir * hsp_max,hsp_acc);
		vsp = scr_approach(vsp,dir * vsp_max,vsp_acc);
				
		if (place_meeting(x,y,player)) {
			//Sound effect
			var experience_snd = audio_play_sound(sn_experience,5,false);
			audio_sound_pitch(experience_snd, choose(0.9,1,1.1,1.2));
			
			//Visual effect & destroy of object
			with (player) { 
				blueflash = 0.7; 
				experience += 1; 
				if (experience >= max_experience) {
					level += 1;
					//This is for when you gain experience and you go over maximum amount but you still retain the experience you got. So this is for overflow.
					experience = experience - max_experience;
					max_experience += max_experience / 2;
					max_hp += 5;
					hp = max_hp;
				}
			}
			instance_destroy();
		}
	}
}
#endregion

scr_fractions(); 
scr_collision();