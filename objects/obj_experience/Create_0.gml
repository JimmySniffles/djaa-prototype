/// @description Intializing Variables
hsp					= random_range(-2,2);			//Horizontal speed
hsp_max				= 4;							//Horizontal max speed
hsp_acc				= random_range(0.4,0.7);		//Horizontal acceleration
hsp_frac			= 0;							//Horizontal fraction
hsp_carry			= 0;							//Horizontal carry on moving platforms
hsp_fric_ground		= 0.50;							//Horizontal friction on the ground
hsp_fric_air		= 0.15;							//Horizontal friction in the air

vsp					= random_range(-5,-7);			//Vertical speed
vsp_max				= 12;							//Vertical maximum speed downwards (known as terminal velocity)
vsp_acc				= random_range(-0.4,-0.6);		//Vertical acceleration
vsp_frac			= 0;							//Vertical fraction
vsp_carry			= 0;							//Vertical carry on moving platforms
grv					= random_range(0.1, 0.3);		//Gravity

my_platform			= noone;						//Vertical platform variable. Checks if your on a platform
flashtimer			= 0;							//Flash timer
detection_radius	= 30;							//Detection radius to start moving towards the player
max_destroytimer	= room_speed * 40;				//Destroy timer so that if the player does not collect experience over a period of time, it destroys itself
destroytimer		= max_destroytimer;				//Maximum destroy timer to count down

//Checking if either player object exists
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }

scr_shader_flash_int();