{
    "id": "a45a2814-6b53-48f2-92f3-63d9c9f3ebc4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rm_transition",
    "eventList": [
        {
            "id": "0bf62e3d-339f-4bdd-9f13-21bf43c5e241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a45a2814-6b53-48f2-92f3-63d9c9f3ebc4"
        },
        {
            "id": "aa1c5cfa-5db0-47cd-a9b4-45b61e7f5727",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a45a2814-6b53-48f2-92f3-63d9c9f3ebc4"
        },
        {
            "id": "dec55925-9f67-4efa-b7cb-4c1ec0180f2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a45a2814-6b53-48f2-92f3-63d9c9f3ebc4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "dd53d82d-6726-4078-8aa7-fe47331c30af",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"centre\"",
                "\"side\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"side\"",
            "varName": "type",
            "varType": 6
        },
        {
            "id": "a948cc1c-5e68-48d9-914a-2f8130c06765",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "portal",
            "varType": 3
        },
        {
            "id": "78f1094f-efbe-4850-a919-9f0907859953",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "target",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "2db4e53c-0189-4288-ade7-b518f9e11f9a",
    "visible": false
}