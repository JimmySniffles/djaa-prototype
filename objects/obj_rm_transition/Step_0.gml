/// @description Move To Next Room

#region Transition To Next Room Based On Which Type The Room Transition Is
if (instance_exists(player)) {
	if (place_meeting(x,y,player)) {
		with (player) {
			if (hascontrol) {
				hascontrol = false;
				if (other.type == "centre") { scr_transition(TRANS_TYPE.CENTRE,TRANS_MODE.GOTO,other.target); }
				if (other.type == "side") { scr_transition(TRANS_TYPE.SIDE,TRANS_MODE.OFF,other.target); }
			}	
		}
	}
}
#endregion