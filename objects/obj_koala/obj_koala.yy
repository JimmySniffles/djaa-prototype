{
    "id": "74327521-a3a2-4623-8e88-97ade7ffb370",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_koala",
    "eventList": [
        {
            "id": "d82ac543-7584-4f50-8d56-6de3567ec8ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74327521-a3a2-4623-8e88-97ade7ffb370"
        },
        {
            "id": "f4cd960c-e8ae-42aa-9361-be02a373ce5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74327521-a3a2-4623-8e88-97ade7ffb370"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e7b63a56-5c13-427b-8f16-df3b160ff7dc",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"non_selected\"",
                "\"nw_brown\"",
                "\"nw_grey\"",
                "\"sw_brown\"",
                "\"sw_grey\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"non_selected\"",
            "varName": "type_select",
            "varType": 6
        },
        {
            "id": "4f3df863-ba89-40c8-ac68-7e5da2a89f33",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type_random",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "740658b2-5267-4052-9039-3588435fab64",
    "visible": true
}