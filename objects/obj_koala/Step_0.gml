/// @description Checking If Player Is Close By Change Image Index

#region If Player Exists Change Image Index
if (instance_exists(player)) {
	if (distance_to_object(player) > closeby) { image_index = 0; }
	if (distance_to_object(player) <= closeby) && (distance_to_object(player) > reallycloseby) { image_index = 1; }
	if (distance_to_object(player) <= reallycloseby) && (distance_to_object(player) > verycloseby) { image_index = 2; }
	if (distance_to_object(player) <= verycloseby) { image_index = 3; }
}
#endregion