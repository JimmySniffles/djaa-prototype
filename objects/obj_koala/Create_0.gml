/// @description Setting Image Speed & Index To Zero & Setting Ranges

#region Setting Variables
if instance_exists(obj_boy) { player = obj_boy; }
if instance_exists(obj_girl) { player = obj_girl; }
image_index = 0;
image_speed = 0;

closeby = 300;
reallycloseby = 200;
verycloseby = 100;
#endregion

#region Different Koala Animal Animations

	#region Normal World Brown Koala
	if (type_select = "nw_brown") {
	sprite_index = spr_bkoala;
	}
	#endregion
	
	#region Normal World Grey Koala
	if (type_select = "nw_grey") {
	sprite_index = spr_gkoala;
	}
	#endregion
	
	#region Spirit World Brown Koala
	if (type_select = "sw_brown") {
	sprite_index = spr_sw_bkoala;
	}
	#endregion
	
	#region Spirit World Grey Koala
	if (type_select = "sw_grey") {
	sprite_index = spr_sw_gkoala;
	}
	#endregion
	
	#region Random Selection Of Koala Based On World
	if (type_select = "non_selected") {
	#region Normal World
		if (type_random = "nw") { 
			type_select = choose("nw_brown", "nw_grey"); 
			if (type_select = "nw_brown") {
			sprite_index = spr_bkoala;
			}
			if (type_select = "nw_grey") {
			sprite_index = spr_gkoala;
			}
		}
	#endregion
	
	#region Spirit World
		if (type_random = "sw") { 
			type_select = choose("sw_brown", "sw_grey"); 
			if (type_select = "sw_brown") {
			sprite_index = spr_sw_bkoala;
			}
			if (type_select = "sw_grey") {
			sprite_index = spr_sw_gkoala;
			}
		}
	#endregion
	}
	#endregion
	
#endregion