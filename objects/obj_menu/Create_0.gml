/// @description GUI/Vars/Menu Setup

#region Menu Variables				
gui_width				= display_get_gui_width();					//The width of the gui display which is 1920.
gui_height				= display_get_gui_height();					//The height of the gui display which is 1080.

menu_itemwidth			= font_get_size(fnt_menu);					//The menu font size.
menu_x_buffer			= gui_width * 0.1;							//Buffer amount for the menu text options to begin drawing horizontally from left of the screen.
menu_x_buffer_scale		= 10;										//Scale gab for each menu item that is then multiplied by menu_itemwidth.
cmenu_x_buffer_scale	= 8.5;										//Sca;e gab for each menu item for the computer main menu data structure page.
controls_x_buffer_scale	= 5.2;										//Scale gab for each menu item for the controls menu page that is then multiplied by menu_itemwidth.
menu_y_buffer			= 120;										//Buffer amount of where the menu is drawn vertically from the bottom of the screen.

start_menu_x			= gui_width - menu_x_buffer;				//The X (horizontal) position for the menu. Adding to this will animate the text from this position to the target position from right to the left.
start_menu_y			= gui_height + 150;							//The Y (Vertical) position for the menu (default is 1080). Adding to this will animate the text from this position to the target position from bottom to top.

menu_y_target			= gui_height - menu_y_buffer;				//Where the menu position will be. The target menu y position.
menu_speed				= 25;										//Movement speed of the animation. Lower is faster.
menu_commited			= -1;										//Set whether or not the menu option is selected.
menu_control			= true;										//Set whether or not the player has control of the menu.
inputting				= false;									//Checks whether or not the player is inputting/changing a value in the settings.
#endregion

#region Creating enumerators (Enumerators are a class of variables) Note: To add more entrys add it before the height variable.
//The menu pages
enum menu_page {
	main,
	levels,
	settings,
	audio,
	graphics,
	controls,
	height
}

//Menu element types or functions that the menu has that it can do
enum menu_element_type {
	script_runner,
	page_transfer,
	slider,
	toggle,
	input
}
#endregion

#region Creating Menu Pages & Menu Option Selection

#region Old Version Of Main Menu Page
/*
ds_menu_main = scr_create_menu_page(
	["Exit",				menu_element_type.script_runner,	scr_game_end],
	["Credits",				menu_element_type.script_runner,	scr_credits],
	["Settings",			menu_element_type.page_transfer,	menu_page.settings],
	["New\nJourney",		menu_element_type.script_runner,	scr_new_game],
	["Continue\nJourney",	menu_element_type.script_runner,	scr_resume_game]
);*/
#endregion

if (global.computer_use) {
	ds_menu_main = scr_create_menu_page(
		["Exit",				menu_element_type.script_runner,	scr_game_end],
		["Credits",				menu_element_type.script_runner,	scr_credits],
		["About",				menu_element_type.script_runner,	scr_about],
		["Settings",			menu_element_type.page_transfer,	menu_page.settings],
		["Future\nLevels",		menu_element_type.page_transfer,	menu_page.levels],
		["Begin",				menu_element_type.script_runner,	scr_new_game]
	);
}
if (global.mobile_use) {
	ds_menu_main = scr_create_menu_page(
		["Credits",				menu_element_type.script_runner,	scr_credits],
		["About",				menu_element_type.script_runner,	scr_about],
		["Future\nLevels",		menu_element_type.page_transfer,	menu_page.levels],
		["Begin",				menu_element_type.script_runner,	scr_new_game]
	);
}

ds_levels = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	menu_page.main],
	["Level 2",				menu_element_type.script_runner,	scr_level_2]
);

ds_settings = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	menu_page.main],
	["Controls",			menu_element_type.page_transfer,	menu_page.controls],
	["Graphics",			menu_element_type.page_transfer,	menu_page.graphics],
	["Audio",				menu_element_type.page_transfer,	menu_page.audio]
);

ds_menu_audio = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	menu_page.settings],
	["Voice\nOver",			menu_element_type.slider,			scr_change_volume,			global.voice_over_vol,			[0,1]],
	["Sound\nEffects",		menu_element_type.slider,			scr_change_volume,			global.sound_effects_vol,		[0,1]],
	["Background\nMusic",	menu_element_type.slider,			scr_change_volume,			global.background_music_vol,	[0,1]]
);

ds_menu_graphics = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	menu_page.settings],
	["Window\nMode",		menu_element_type.toggle,			scr_change_window_mode,		global.windowsmode,				["Window","Fullscreen"]]
);

ds_menu_controls = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	menu_page.settings],
	["Interact",			menu_element_type.input,			"key_interact",				ord("E")],
	["Special\nAttack",		menu_element_type.input,			"key_spattack",				mb_right],
	["Attack",				menu_element_type.input,			"key_attack",				mb_left],
	["Sprint",				menu_element_type.input,			"key_sprint",				vk_shift],
	["Up",					menu_element_type.input,			"key_up",					ord("W")],
	["Down",				menu_element_type.input,			"key_down",					ord("S")],
	["Jump",				menu_element_type.input,			"key_jump",					vk_space],
	["Right",				menu_element_type.input,			"key_right",				ord("D")],
	["Left",				menu_element_type.input,			"key_left",					ord("A")]
);

//Setting up the pages to transfer between.
page = 0;
menu_pages = [ds_menu_main, ds_levels, ds_settings, ds_menu_audio, ds_menu_graphics, ds_menu_controls]

//Setting up the current options within the pages.
var i = 0, array_len = array_length_1d(menu_pages);
repeat (array_len) {
	menu_option[i] = 0;
	i++;
}

//Setting up old menu option for playing sound effect when it no longer equals the current menu option for the mouse control.
old_menu_option = menu_option[page];

if (global.computer_use) { menu_option[page] = 5; }
if (global.mobile_use) { menu_option[page] = 3; }
//Menu cursor default selection depending whether or not the player has a saved game.
//if (file_exists(SAVEFILE)) { menu_option[page] = 4; } else { menu_option[page] = 3; }
#endregion