/// @description Control Menu

#region Intializing Variables. Getting Access To The Grid & Its Height. Adding Menu Option Ease In Movement.
//ds_grid is used to access what page were on in the menu_pages array whether its the settings page or the main page or the audio page etc.
//ds_height gets the height of the grid because that gives us the number of elements or options on a single page.
var ds_grid	= menu_pages[page], ds_height = ds_grid_height(ds_grid);

var final_menu_x_buffer_scale; //Setting the final_menu_x_buffer_scale.
if (global.computer_use) && (page == ds_menu_main) { final_menu_x_buffer_scale = cmenu_x_buffer_scale; }
if (global.computer_use) && (page != ds_menu_main) && (page != ds_menu_controls) {	final_menu_x_buffer_scale = menu_x_buffer_scale; }
if (page == ds_menu_controls) {	final_menu_x_buffer_scale = controls_x_buffer_scale; }
if (global.mobile_use) { final_menu_x_buffer_scale = menu_x_buffer_scale; }

var menu_end_x = start_menu_x - (ds_height * (menu_itemwidth * final_menu_x_buffer_scale)); //Setting the end x position of the menu. Its close to the left side since its being drawn from right to left.
//Menu option text ease in movement animation
//Things moving in from either left or right //menu_x += (menu_x_target - menu_x) / menu_speed;
//Moving something to a target //menu_x_target = gui_width+200;
start_menu_y += (menu_y_target - start_menu_y) / menu_speed;																					
#endregion

#region	Menu Selection Input Intialization
if (!inputting) {
	var input_left	= (keyboard_check_pressed(vk_right)) || (keyboard_check_pressed(vk_down)) || (keyboard_check_pressed(ord("D")))	|| (keyboard_check_pressed(ord("S"))) || mouse_wheel_down();
	var input_right = (keyboard_check_pressed(vk_left)) || (keyboard_check_pressed(vk_up)) ||(keyboard_check_pressed(ord("A"))) || (keyboard_check_pressed(ord("W"))) || mouse_wheel_up();
	var input_enter = (keyboard_check_pressed(vk_enter)) || (keyboard_check_pressed(vk_space));
	var input_mouse = (mouse_check_button_pressed(mb_left));
} else {
	if (ds_grid[# 1, menu_option[page]] == menu_element_type.toggle) {
		input_left	= (keyboard_check_pressed(vk_right)) || (keyboard_check_pressed(vk_down)) || (keyboard_check_pressed(ord("D"))) || (keyboard_check_pressed(ord("S")));
		input_right = (keyboard_check_pressed(vk_left)) || (keyboard_check_pressed(vk_up)) || (keyboard_check_pressed(ord("A"))) || (keyboard_check_pressed(ord("W")));
	} else {
		input_left	= (keyboard_check(vk_right)) || (keyboard_check(vk_down)) || (keyboard_check(ord("D"))) || (keyboard_check(ord("S")));
		input_right = (keyboard_check(vk_left)) || (keyboard_check(vk_up)) || (keyboard_check(ord("A"))) || (keyboard_check(ord("W")));
	}
	input_enter		= (keyboard_check_pressed(vk_enter));
	input_mouse		= (mouse_check_button_pressed(mb_left));
}
#endregion

#region Menu Controls & Inputting
var mouse_x_gui = device_mouse_x_to_gui(0); var mouse_y_gui = device_mouse_y_to_gui(0); 
if (menu_control) {
	if (inputting) {
		switch (ds_grid[# 1, menu_option[page]]) {
		#region Slider Menu Element Type
		case menu_element_type.slider:			
			var keyboard_input		= input_right - input_left;
			var mousewheel_input	= mouse_wheel_up() - mouse_wheel_down();
			if (keyboard_input != 0) {
				if (!audio_is_playing(sn_nav)) { audio_play_sound(sn_nav,10,false); }
				ds_grid[# 3, menu_option[page]] += keyboard_input * 0.01;								//Amount each step the keyboard hold goes by. The speed.
				ds_grid[# 3, menu_option[page]] = clamp(ds_grid[# 3, menu_option[page]], 0, 1);			//Clamping the values between 0 and 1.
				script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]);		//Execute the script to change the value.
			}
			if (mousewheel_input != 0) {
				audio_play_sound(sn_nav,10,false);
				ds_grid[# 3, menu_option[page]] += mousewheel_input * 0.1;								//Amount each mouse scroll goes by. The scroll speed.
				ds_grid[# 3, menu_option[page]] = clamp(ds_grid[# 3, menu_option[page]], 0, 1);			//Clamping the values between 0 and 1.
				script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]);		//Execute the script to change the value.
			}						
		break;
		#endregion
		
		#region Toggle Menu Element Type
		case menu_element_type.toggle:
		var keyboard_input		= input_right - input_left;
		var mousewheel_input	= mouse_wheel_down() - mouse_wheel_up();
			if (keyboard_input != 0) {
				audio_play_sound(sn_nav,10,false);
				ds_grid[# 3, menu_option[page]] += keyboard_input;
				if (ds_grid[# 3, menu_option[page]] > 1) { ds_grid[# 3, menu_option[page]] = 0; }		//If the option selection goes to the max selection of options then it goes back to start again.
				if (ds_grid[# 3, menu_option[page]] < 0) { ds_grid[# 3, menu_option[page]] = 1; }		//If the option selection goes less than the selection options then it goes to the max selection option.
			}
			if (mousewheel_input != 0) {
				audio_play_sound(sn_nav,10,false);
				ds_grid[# 3, menu_option[page]] += mousewheel_input;
				if (ds_grid[# 3, menu_option[page]] > 1) { ds_grid[# 3, menu_option[page]] = 0; }		//If the option selection goes to the max selection of options then it goes back to start again.
				if (ds_grid[# 3, menu_option[page]] < 0) { ds_grid[# 3, menu_option[page]] = 1; }		//If the option selection goes less than the selection options then it goes to the max selection option.
			}
		break;
		#endregion
		
		#region Input Menu Element Type
		case menu_element_type.input:
		var keyboard_lk = keyboard_lastkey; var mouse_lb = mouse_lastbutton;
		if (keyboard_lk != vk_enter) && (keyboard_check_pressed(vk_anykey)) {
			if (keyboard_lk != ds_grid[# 3, menu_option[page]]) { if (!audio_is_playing(sn_nav)) { audio_play_sound(sn_nav,10,false); } }
			ds_grid[# 3, menu_option[page]] = keyboard_lk;
			variable_global_set(ds_grid[# 2, menu_option[page]], keyboard_lk);
		}
		if (mouse_lb != -1) && (mouse_check_button_pressed(mb_any)) {
			if (mouse_lb != ds_grid[# 3, menu_option[page]]) { if (!audio_is_playing(sn_nav)) { audio_play_sound(sn_nav,10,false); } }
			ds_grid[# 3, menu_option[page]] = mouse_lb;
			variable_global_set(ds_grid[# 2, menu_option[page]], mouse_lb);
		}
		break;
		#endregion
		}
	} else {
		#region Menu Options Selection Via Keyboard, Mouse Or Touch
		//ochange is the option change of changing options on the menu page.
		var ochange = input_right - input_left;
		if (ochange != 0) {
			menu_option[page] += ochange;										//Changes the menu option.
			if (menu_option[page] > ds_height-1) { menu_option[page] = 0; }		//If the option selection goes to the max selection of menu options then it goes back to start again.
			if (menu_option[page] < 0) { menu_option[page] = ds_height-1; }		//If the option selection goes less than the selection menu options then it goes to the max menu selection option.
			audio_play_sound(sn_nav,10,false);
		}
		//Mouse Control
		if (mouse_x_gui >= menu_end_x) && (mouse_x_gui <= start_menu_x) && (mouse_y_gui >= start_menu_y - menu_y_buffer) {	
			menu_option[page] = (start_menu_x - mouse_x_gui) div (menu_itemwidth * final_menu_x_buffer_scale);	
			if (old_menu_option != menu_option[page]) { audio_play_sound(sn_nav,10,false); old_menu_option = menu_option[page]; } //Play sound effect when old menu option no longer equals the current menu option meaning that the player has changed menu option.
		}
		#endregion
	}
	#region If The Player Selected An Option Play Sound Effects & Do The Functions
	var final_input_selection = false;	//Separating keyboard and mouse selection so that the player must press the mouse button or touch the screen next to the menu items.
	if (input_enter) { final_input_selection = true; }
	if (input_mouse) && (mouse_x_gui >= menu_end_x) && (mouse_x_gui <= start_menu_x) && (mouse_y_gui >= start_menu_y - menu_y_buffer) { final_input_selection = true; }
	if (final_input_selection) {
		//Audio
		//Play sound effect when the option is greater than zero which is the back option.
		if (menu_option[page] > 0) {
			if (!audio_is_playing(sn_con_new)) { audio_play_sound(sn_con_new,10,false); }
		}
		//Play settings sound effect when the option is the settings option in the main menu page.
		if (menu_option[page] == 3) && (page == ds_menu_main) && (global.computer_use) {
			if (!audio_is_playing(sn_settings)) { audio_play_sound(sn_settings,10,false); }
		}
		//Play back sound effect when first menu option is pressed which is zero.
		if (menu_option[page] == 0) {
			if (!audio_is_playing(sn_back)) { audio_play_sound(sn_back,10,false); }
		}
		
		//If the menu option is not a page transfer and its on the main menu page or levels page. Also checking platform.
		if (global.computer_use) {
			if (menu_option[page] != 3) && (menu_option[page] != 4) && (page == ds_menu_main) {
				menu_y_target = gui_height + 150;
				menu_commited = menu_option[page];
				menu_control = false;
			}
		}
		if (global.mobile_use) {
			if (menu_option[page] != 2) && (page == ds_menu_main) {
				menu_y_target = gui_height + 150;
				menu_commited = menu_option[page];
				menu_control = false;
			}
		}
		if (menu_option[page] != 0) && (page == ds_levels) {
			menu_y_target = gui_height + 150;
			menu_commited = menu_option[page];
			menu_control = false;
		}
		
		//Executing the different menu element types. Using # 2 of the ds_grid accesses the second column of that grid we set up.
		switch (ds_grid[# 1, menu_option[page]]) {
			case menu_element_type.page_transfer: page = ds_grid[# 2, menu_option[page]]; break;
			case menu_element_type.toggle: if (inputting) { script_execute(ds_grid[# 2, menu_option[page]], ds_grid[# 3, menu_option[page]]); }
			case menu_element_type.slider:
			case menu_element_type.input:
				inputting = !inputting;			//Toggles whether the player is inputting for the slider and input. Because theres no break on slider and input it will execute the inputting = !inputting code for both plus the toggle since it also does not have a break.
			break;
		}
	}
}
//This has to execute outside of the input_enter & menu control if statement. Checks the menu gui position and if its right it executes script.
if ((start_menu_y > gui_height + 120) && (menu_commited != -1)) {
	if (ds_grid[# 1, menu_option[page]] == menu_element_type.script_runner) { script_execute(ds_grid[# 2, menu_option[page]]); }
}
	#endregion
#endregion