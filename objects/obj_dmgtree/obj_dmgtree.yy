{
    "id": "c3ad81d7-d0cd-40dc-82eb-96c85dc2f0ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dmgtree",
    "eventList": [
        {
            "id": "585fdefe-35c4-4327-9f3b-327c3a5df766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3ad81d7-d0cd-40dc-82eb-96c85dc2f0ac"
        },
        {
            "id": "2c8cb3da-d09f-4bd2-9785-314c4a37fd9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3ad81d7-d0cd-40dc-82eb-96c85dc2f0ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6582d453-1c23-405d-ad9e-715cdfa3c0e0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
    "visible": true
}