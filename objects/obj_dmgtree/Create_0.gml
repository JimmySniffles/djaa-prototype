/// @description Inheriting The Parent Damage Object & Adding Additional Variables
event_inherited();

//Root Variable Stats
#region Root Environment Object
hp = 0;								//Setting hp variable
max_hp = 75;						//Setting max hp variable
hp = max_hp;						//Setting hp to max hp
env_object = obj_dmgtree;			//Setting check for tree

	#region Visual Effects & Sound
	dirt_type		= dirtpart.root;
	dirt_destroy	= 25;						//Setting amount of dirt particle objects to create upon destruction of object
	snd_breaking_v1 = sn_woodhitroot_v1;		//Setting sound for when the object changes to a new frame upon breakage version 1
	snd_breaking_v2 = sn_woodhitroot_v2;		//Setting sound for when the object changes to a new frame upon breakage version 2
	snd_breaking_v3 = sn_woodhitroot_v3;		//Setting sound for when the object changes to a new frame upon breakage version 3
	
	snd_destroyed_v1 = sn_meleehitroot_v1;		//Setting sound for when the object destroys version 1
	snd_destroyed_v2 = sn_meleehitroot_v2;		//Setting sound for when the object destroys version 2
	#endregion
	
#endregion