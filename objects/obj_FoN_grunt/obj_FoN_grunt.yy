{
    "id": "9e030adf-ccda-4671-8cab-cc3cf4e43b6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_FoN_grunt",
    "eventList": [
        {
            "id": "2c819146-36f4-4f5e-9527-a9974fadd289",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e030adf-ccda-4671-8cab-cc3cf4e43b6e"
        }
    ],
    "maskSpriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
    "overriddenProperties": [
        {
            "id": "16fed9f3-48d2-4af8-92ee-b7f77c6d0c2b",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "55eed39d-fbc8-4671-9947-c7357275683a",
            "propertyId": "8b769d9d-2622-4aa4-b1aa-dbe47f29b557",
            "value": "True"
        }
    ],
    "parentObjectId": "55eed39d-fbc8-4671-9947-c7357275683a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a0e39a05-9e10-44eb-9283-1a3103449722",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"club\"",
                "\"spear\"",
                "\"boomerang\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"club\"",
            "varName": "weapon",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
    "visible": true
}