/// @description Day & Night Cycle Variables

#region Setting The Display Size For Visual Effect
gui_width		= display_get_gui_width();	//The width of the display (returns number, example being 1920).
gui_height		= display_get_gui_height(); //The height of the display (returns number, example being 1080).
#endregion

#region Time Set Variables
seconds			= 0;						//Set amount of seconds
minutes			= 0;						//Set amount of minutes
hours			= 0;						//Set amount of hours

day				= 1;						//Set amount of days
season			= 1;						//Set amount of seasons
year			= 1;						//Set amount of years

time_increment	= 250;						//Amount of seconds past per step or frame
time_pause		= false;					//Sets whether the time is paused
#endregion

#region Set Darkness Level & Whether To Draw Daylight
max_darkness	= 0.7;						//Max darkness level
darkness		= 0;			
light_colour	= c_black;
draw_daylight	= false;					//Sets whether or not to set the daylight on
#endregion

#region Phases for the different time set as enums
enum phase {
	sunrise		= 6,						//Time for sunrise
	daytime =	 8.5,						//Time for daytime
	sunset		= 18,						//Time for sunset
	nighttime	= 22,						//Time for nighttime
}
#endregion