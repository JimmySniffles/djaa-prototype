{
    "id": "2284b408-54a3-4338-8f30-5c0a72c43b25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_daycycle",
    "eventList": [
        {
            "id": "8625f734-d710-45c2-a799-5936c9ad3030",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2284b408-54a3-4338-8f30-5c0a72c43b25"
        },
        {
            "id": "33c2966a-377a-4ed5-900a-34d591202a07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2284b408-54a3-4338-8f30-5c0a72c43b25"
        },
        {
            "id": "bdea4943-95ff-40e0-a4bf-1bc010a5e4db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2284b408-54a3-4338-8f30-5c0a72c43b25"
        },
        {
            "id": "f56d1190-0629-4178-a4eb-9daa52421663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "2284b408-54a3-4338-8f30-5c0a72c43b25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}