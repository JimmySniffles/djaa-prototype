{
    "id": "139c3b3b-9259-41bd-89c1-a339348cf643",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dmgroot",
    "eventList": [
        {
            "id": "665ba2ca-edcc-4452-9120-ae391703524b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "139c3b3b-9259-41bd-89c1-a339348cf643"
        },
        {
            "id": "e9ff9651-fed6-49b1-ac47-2e015522cf90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "139c3b3b-9259-41bd-89c1-a339348cf643"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6582d453-1c23-405d-ad9e-715cdfa3c0e0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
    "visible": true
}