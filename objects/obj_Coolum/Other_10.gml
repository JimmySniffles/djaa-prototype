//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region Level 4 Dialogue
if (room = rm_level_4) {
	#region The Dialogue
	switch(choice_variable) {
		case -1:
		#region First Dialogue
			//Line 0
			var i = 0;
            myText[i] = "I made it, but I can't find Coolum. This mountain is tough . .";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 1
            i++;
            myText[i] = "I'm tougher.";
            mySpeaker[i] = id;
            myEmotion[i] = 1;

            //line 2
            i++;
            myText[i] = "Ha! That's nice, but I'll believe it when I see it.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 1;

            //line 3
            i++;
            myText[i] = "Who are you? Are you Coolum?";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 4
            i++;
            myText[i] = "I am, and this is my mountain.";
            mySpeaker[i] = id;
            myEmotion[i] = 1;

            //line 5
            i++;
            myText[i] = "Though I suppose it's not that tough a climb, so no sense bragging.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 6
            i++;
            myText[i] = "What do you need?";
            mySpeaker[i] = id;
            myEmotion[i] = 1;

            //line 7
            i++;
            myText[i] = "My home was attacked by a man named Wakan.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 2;
		  
		    //line 8
            i++;
            myText[i] = "He killed my parents and stole my sister, Gira.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 2;

            //line 9
            i++;
            myText[i] = "That's rough, I'm sorry you've been through that. Wakan, you say?";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			
			//line 10
            i++;
            myText[i] = "He sounds mean.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 11
            i++;
            myText[i] = "Do you know him?";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 12
            i++;
            myText[i] = "No, sorry.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 13
            i++;
            myText[i] = "The Elders told me to find you,";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;
			
			//line 14
            i++;
            myText[i] = "because you might be able to help me fight him.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 15
            i++;
            myText[i] = "I do not know him, but from what you say he has done,";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			
			//line 16
            i++;
            myText[i] = "I will help you.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 17
            i++;
            myText[i] = "He has followers, and they're hard to fight.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;
			
			//line 18
            i++;
            myText[i] = "Something about them is strange.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 19
            i++;
            myText[i] = "Strange?.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 20
            i++;
            myText[i] = "They're pale, and don't seem fully. There.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 21
            i++;
            myText[i] = "I might have something that can help you.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			
			//line 22
            i++;
            myText[i] = "But all this sounds like this Wakan is using old magic.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 23
            i++;
            myText[i] = "Magic? Do you mean powers?";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 2;

            //line 24
            i++;
            myText[i] = "I do not know if there's a difference.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			
			//line 25
            i++;
            myText[i] = "The Elders used magic a long time ago to trap Ninderry in his mountain.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 26
            i++;
            myText[i] = "He's a really bad man.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 27
            i++;
            myText[i] = "The Elders told my other sister,";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;
			
			//line 28
            i++;
            myText[i] = "Hope, to ask Maroochy about Gira's powers?";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 29
            i++;
            myText[i] = "Gira has powers?";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 30
            i++;
            myText[i] = "I don't know, but the Elders said that she does, so maybe.";
            mySpeaker[i] = obj_boy;
            myEmotion[i] = 0;

            //line 31
            i++;
            myText[i] = "If Gira has powers, and Wakan stole her,";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			
			//line 32
            i++;
            myText[i] = "and Hope went to Maroochy to learn.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;

            //line 33
            i++;
            myText[i] = "We must find Maroochy. Go forward and we will meet up.";
            mySpeaker[i] = id;
            myEmotion[i] = 0;
			myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
			break;
		#endregion
	
		#region Consequences For The Dialogue
			case "dialogueplayed":
			#region If you chose alright
				var i = 0;
				//Line 0
				myText[i]		= "Budburra, we will meet again soon.";
				mySpeaker[i]	= id;
				myEmotion[i]	= 1;
		
				//uncommenting this will make the first conversation begin again
				//choice_variable	= -1;
			break;
			#endregion
			}
	#endregion
	}
	#endregion
#endregion