/// @description Mechanics, Sounds, Effects and Collision Detection

switch (state) {
	case spear_states.thrown:
	{

		#region Checks If The Spear Has Travelled The Max Distance And If So Add Gravity
		if (abs(x - startx) > maxdistance) || (abs(y - starty) > maxdistance) { gravity = gravityset airspeed += gravityset; }
		#endregion
		
		#region Collision Detection With Tile Collision And Objects And If So Then Change To Returning State, Play Sound, Screen Shake, Create dirt
		if (scr_infloor(global.tilemap, x, y) >= 0) || (place_meeting(x,y,par_objects))
		{
			if (!audio_is_playing(sn_woodhitgrass_v1)) && (!audio_is_playing(sn_woodhitgrass_v2)) && (!audio_is_playing(sn_woodhitgrass_v3)) {
				audio_play_sound(choose(sn_woodhitgrass_v1,sn_woodhitgrass_v2,sn_woodhitgrass_v3),5,false);
			}
		scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
		repeat(7) { scr_dirtparticle(x, y, dirtpart.jumping); }
		state = spear_states.buried;
		wobble = 15;
		}
		#endregion
		
		#region Collision Detection With Enemies And Animals
		var enemyhit = instance_place(x,y,par_enemy);
		with (enemyhit)
		{
			if (state != e_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ene_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ene_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		var animalhit = instance_place(x,y,par_animal);
		with (animalhit)
		{
			if (state != a_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ani_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ani_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		#endregion
		
		#region Collision Detection For Damageable Objects
		var ip_rockhit = instance_place(x,y,obj_dmgrock);
		with (ip_rockhit)
		{
			audio_play_sound(choose(sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3),5,false);
			scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
			repeat(obj_b_wloadout.env_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
			repeat(obj_b_wloadout.env_particles) { scr_dirtparticle(other.x, other.y, dirtpart.jumping); }
			hp -= obj_b_wloadout.env_dmg;
			other.wobble = 20;
			other.state = spear_states.buried;
		}
		
		var ip_roothit = instance_place(x,y,obj_dmgroot);
		with (ip_roothit)
		{
			audio_play_sound(choose(sn_meleehitroot_v1,sn_meleehitroot_v2),5,false);
			scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
			repeat(obj_b_wloadout.env_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
			repeat(obj_b_wloadout.env_particles) { scr_dirtparticle(other.x, other.y, dirtpart.jumping); }
			hp -= obj_b_wloadout.env_dmg;
			other.wobble = 20;
			other.state = spear_states.buried;
		}
		#endregion
		
	}
	break;
	
	case spear_states.buried:
	{
		
		#region Wobble Effect
		speed = 0;
		gravity = 0;
		image_angle = scr_wave(direction-wobble,direction+wobble,0.5,0);
		wobble = max(wobble-1,0);	
		#endregion
		
		#region Flash Effect
		if (flashtimer < room_speed * 1.5) { flashtimer ++; }
		if (flashtimer >= room_speed * 1.5) { flashtimer = 0 whiteflash = 1; }
		#endregion
		
		#region Collision With Player
		if (place_meeting(x,y,obj_boy))
		{
			if (!audio_is_playing(sn_weaponcatch_v1)) && (!audio_is_playing(sn_weaponcatch_v2)) && (!audio_is_playing(sn_weaponcatch_v3)) {
				audio_play_sound(choose(sn_weaponcatch_v1,sn_weaponcatch_v2,sn_weaponcatch_v3),5,false);
			}
			with (obj_b_wloadout) 
			{
				if (ammo[wep_type.spear] != -1)
				{
					ammo[wep_type.spear] = -1;
					scr_changeweapon(wep_type.spear);
				}
				else
				ammo[wep_type.spear] -= 0;
			}
			instance_destroy();
		}
		#endregion	
		
	}
	break;
}