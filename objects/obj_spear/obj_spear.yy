{
    "id": "02c6a2a5-796c-4114-81b2-7e895abfbcd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spear",
    "eventList": [
        {
            "id": "63190d09-8f50-44f4-aa1a-a3584a9bd797",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02c6a2a5-796c-4114-81b2-7e895abfbcd5"
        },
        {
            "id": "bcd26fa9-b7fc-4c98-841c-c1697347dfc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02c6a2a5-796c-4114-81b2-7e895abfbcd5"
        },
        {
            "id": "95deb7dd-3ff0-4f59-a6d7-eec16239f114",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "02c6a2a5-796c-4114-81b2-7e895abfbcd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "778b6b2b-b3eb-4a89-8648-e44b134d9a29",
    "visible": true
}