/// @description Intializing Variables

#region Setting State & Flash Effect When Buried Variables
state = spear_states.thrown;
scr_shader_flash_int();
flashtimer = 0;
#endregion

#region Max Distance And Speed Drop Variable Intialization
startx = x;
starty = y;
maxdistance = obj_b_wloadout.proj_dist;
vsp = 0;
airspeed = obj_b_wloadout.proj_spd;
wobble = 0;
gravityset = 0.09;
#endregion