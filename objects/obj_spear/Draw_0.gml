/// @description Draw Self & Shader Effect When Buried
draw_self();
if (state = spear_states.thrown) { scr_draw_trail(40,3,c_ltgray,-1,true,0.1); }
if (whiteflash > 0) { scr_shader_flash(0.05,1.0,1.0,1.0); }