{
    "id": "719577f0-84f1-4e57-8d6d-ef357107a47c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_checkpoint",
    "eventList": [
        {
            "id": "8b05efdd-3f17-442d-9f68-f0e652812bff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "719577f0-84f1-4e57-8d6d-ef357107a47c"
        },
        {
            "id": "d43acceb-729c-450e-bbc0-62889531069b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "719577f0-84f1-4e57-8d6d-ef357107a47c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a8fbc16e-519a-4230-b16b-3f55498d2fdf",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
    "visible": true
}