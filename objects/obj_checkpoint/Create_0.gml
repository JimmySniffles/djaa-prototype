/// @description Checkpoint image set to normal with no animation

#region Intializing Variables
if instance_exists(obj_boy) { player = obj_boy; }
if instance_exists(obj_girl) { player = obj_girl; }

if (type == "nw") { image_index = 0; flashsprite = 1; standardsprite = 0; }
if (type == "sw") { image_index = 2; flashsprite = 3; standardsprite = 2; }

detect_radius		= 60;
chkpointsndplayed	= false;
image_speed			= 0;
#endregion