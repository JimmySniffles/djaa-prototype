/// @description Collision Check

#region Checkpoint System
if (instance_exists(player)) {
	if (collision_circle(x,y,detect_radius,player,false,true)) {
		global.checkpoint = id;
		global.checkpointx = x;
		global.checkpointy = y;
		global.checkpointR = room;
		if (chkpointsndplayed == false) {
			if (!audio_is_playing(sn_checkpoint_v1)) && (!audio_is_playing(sn_checkpoint_v2)) && (!audio_is_playing(sn_checkpoint_v3)) {
				audio_play_sound(choose(sn_checkpoint_v1,sn_checkpoint_v2,sn_checkpoint_v3),8,false);
			}
		chkpointsndplayed = true;
		}
	}
	
	#region Setting The Sprite To Flash Sprite
	if (global.checkpointR == room) {
		if (global.checkpoint == id) { image_index = flashsprite; } else { image_index = standardsprite; chkpointsndplayed = false; }
	}	
	#endregion
}
#endregion