//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Ahh Budburra! How are you boy, doing good?";
		mySpeaker[i]	= id;
		
		//Line 1
		i++;
		myText[i]		= ["Yeah I'm feeling alright.", "Nah I'm feeling terrible.","I feel goooood."];
		myTypes[i]		= 1;
		myNextLine[i]	= [2,3,4];
		myScripts[i]	= [[change_variable, id, "choice_variable", "alright"], [change_variable, id, "choice_variable", "terrible"], [change_variable, id, "choice_variable", "good"]];
		mySpeaker[i]	= obj_boy;
		
		//Line 2
		i++;
		myText[i]		= "It can get a lot worse haha, go out and enjoy yourself!";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;

		//Line 3
		i++;
		myText[i]		= "Thats a shame, hope you get better soon young man.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;
		
		//Line 4
		i++;
		myText[i]		= "Hahaha. That's my man, excellent. Get out there and have fun for me!";
		mySpeaker[i]	= id;
		break;
	#endregion
	
	#region Consequences For The Dialogue
		case "alright":
		#region If you chose alright
			var i = 0;
			//Line 0
			myText[i]		= "Go out and enjoy yourself!";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
	
		case "terribe":
		#region If you chose terrible
			var i = 0;
			//Line 0
			myText[i]		= "Hope you get better soon young man.";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
	
		case "good":
		#region If you chose good
			var i = 0;
			//Line 0
			myText[i]		= "Get out there and have fun for me!";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
		}
		#endregion
#endregion