{
    "id": "6d014609-d023-4497-b39f-93218ad20ca3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dmgrock",
    "eventList": [
        {
            "id": "4502ca3a-07bb-4fdf-965f-544e3663c26b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d014609-d023-4497-b39f-93218ad20ca3"
        },
        {
            "id": "d618297f-490f-4779-a5fe-b9be63aa114e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d014609-d023-4497-b39f-93218ad20ca3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6582d453-1c23-405d-ad9e-715cdfa3c0e0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
    "visible": true
}