/// @description Inheriting The Parent Damage Object & Adding Additional Variables
event_inherited();

//Rock Variable Stats
#region Rock Environment Object
hp = 0;								//Setting hp variable
max_hp = 100;						//Setting max hp variable
hp = max_hp;						//Setting hp to max hp
env_object = obj_dmgrock;			//Setting check for rock

	#region Visual Effects & Sound
	dirt_type		= dirtpart.rock;
	dirt_destroy	= 25;						//Setting amount of dirt particle objects to create upon destruction of object
	snd_breaking_v1 = sn_rockbreak_v1;			//Setting sound for when the object changes to a new frame upon breakage version 1
	snd_breaking_v2 = sn_rockbreak_v2;			//Setting sound for when the object changes to a new frame upon breakage version 2
	snd_breaking_v3 = sn_rockbreak_v3;			//Setting sound for when the object changes to a new frame upon breakage version 3
	
	snd_destroyed_v1 = sn_rockbreak_final;		//Setting sound for when the object destroys version 1
	snd_destroyed_v2 = sn_rockbreak_final;		//Setting sound for when the object destroys version 2
	#endregion
	
#endregion