///@description Setting Speed according to x and y values

x += hsp;
y += vsp;

hsp *= 0.9;
vsp *= 0.9;

vsp += grv; 

if (scr_animation_end()) { instance_destroy(); }