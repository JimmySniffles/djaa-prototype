{
    "id": "f54f6bd9-d640-4175-a061-0a2b206e0fef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_kangaroo",
    "eventList": [
        {
            "id": "d8df2bb9-e751-4b0e-a029-171fa56e188d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f54f6bd9-d640-4175-a061-0a2b206e0fef"
        }
    ],
    "maskSpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
    "overriddenProperties": [
        {
            "id": "d2db1781-d0f2-4bc8-8be3-d83059be3386",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "4f3ec3b7-f2b5-437c-acd1-7bf01c149cfe",
            "value": "\"kangaroo\""
        },
        {
            "id": "9a075ead-5834-4462-b144-d99d4bfbcc57",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "0611dd22-a9b6-4d67-b4d8-61690d7b919f",
            "value": "True"
        },
        {
            "id": "4fd882e2-cfd7-4105-8cc6-6e4f56e34c20",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "b8f2e771-dfcf-48c2-addd-f78994c326f4",
            "value": "True"
        },
        {
            "id": "aaad9af6-4973-472d-af7e-fba58678daa6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
            "propertyId": "920f71cc-471c-4545-9026-6809797a0178",
            "value": "350"
        }
    ],
    "parentObjectId": "7058193c-c09f-44bf-a2c4-520218bfc41c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "7926b729-33f7-49bb-a77b-670c79e0fdfb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"non_selected\"",
                "\"nw_brown\"",
                "\"nw_grey\"",
                "\"sw_brown\"",
                "\"sw_grey\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"non_selected\"",
            "varName": "type_select",
            "varType": 6
        },
        {
            "id": "1ebcaa8b-df8c-461d-8f0f-a127aef39966",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type_random",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
    "visible": true
}