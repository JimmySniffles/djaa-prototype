///@description Setting Animals Status Variables

//Inherit from parent. Setting animal attack stats.
event_inherited();
scr_animal_set_atk_stats(a_type.kangaroo);

//Setting x position variables
x_anchor_range = x;											//Gets x position and stores it as anchor for the max range for the AI to move between.
startx = x;													//Getting start x position upon intialisation.
starty = y;													//Getting start y position upon intialisation.

#region Movement Variables

	#region Horizontal & Horizontal Carry, Vertical & Vertical Carry (For Moving Platforms) Variables & Fractions
	hsp = 0;					//Horizontal speed
	hsp_frac = 0;				//Horizontal fraction
	hsp_carry = 0;				//Horizontal carry on moving platforms

	vsp = 0;					//Vertical speed
	vsp_frac = 0;				//Vertical fraction
	vsp_carry = 0;				//Vertical carry on moving platforms

	my_platform = noone;		//Vertical platform variable. Checks if your on a platform
	#endregion
	
	#region Gravity Variables
	grv = 0.3;					//Standard Gravity
	grv_wall = 0.1;				//Gravity on a wall
	#endregion

	#region Horizontal Movement Variables
	hsp_wmsp = 0;				//Horizontal walking movement speed
	hsp_wmaxsp = 2;				//Maximum horizontal walking movement speed
	hsp_msp = 0;				//Horizontal movement speed
	hsp_maxsp = 4;				//Maximum horizontal movement speed
	hsp_acc = 1;				//Horizontal acceleration
	hsp_fric_ground = 0.50;		//Horizontal friction on the ground
	hsp_fric_air = 0.15;		//Horizontal friction in the air
	#endregion
	
	#region Vertical Speed Downwards Variables
	vsp_max = 12;				//Vertical maximum speed downwards (known as terminal velocity)
	#endregion
	
#endregion

#region Animal Status

	#region General
	state = a_states.normal;	//Setting default state of the animal to the normal state
	moveable = true;			//Checks whether or not animal can move	
	catchphrase = true;			//Catch phrase said or not
	stun = false;				//Setting whether Or Not the Animal Is Stunned
	
	collided = false;			//Checking if collided with a collision wall

	onground = false;			//Returning true or false whether on the ground
	onplatform = false;			//Returning true or false whether on a platform
	onobject = false;			//Returning true or false whether on a generic object
	
	idletimer = 0;				//A idle timer
	moveidletimer = 0;			//A idle timer for moving randomly
	runningtimer = 0;			//A running timer
	max_alerttimer = 0;			//Setting max alert timer amount
	alerttimer = max_alerttimer;//A alert timer to set the amount of time the object will be alert for
	max_scaredtimer = room_speed * 10;		//Setting max scared timer amount
	scaredtimer = max_scaredtimer;			//A scared timer to set the amount of time the object will be scared for

	is_water_surface_collision = false; //Checking if colliding with surface of water
	inandoutofwater = 0;		//Checks if in and out of water. 0 default, 1 is in water and 2 is out of water which then sets to 0 (default) after breath = 100
	
	jumpbuffer = 0;				//Buffer of time when off the ground to jump
	jumpbuffer_max = 6;			//Maximum buffer time when jumping off the ground
	
	takingdamage = false;		//Variable to check if taken damage
	scr_shader_flash_int();		//Gets shader flash variables
	deathplayed = 0;			//Checking if the death effects have played whilst dead
	max_deathtimer = room_speed * 4;	//Sets the max death timer to count down when the animal has died
	deathtimer = max_deathtimer;//Sets the death timer to equal max death timer
	#endregion
	
	#region HP, Energy, Breath & Death
	max_deathtimer = room_speed * 5;					//Sets the max death timer to count down when the enemy has died
	deathtimer = max_deathtimer;						//Sets the death timer to equal max death timer
	
	max_hp = 75;										//Max amount of health points
	hp = max_hp;										//Setting hp to equal max hp
	old_hp = hp;										//Setting old hp amount for a delay catch up to hp value
	max_hp_bar_dec_delay = room_speed * 1;				//max old hp decrease timer delay
	hp_bar_dec_delay = max_hp_bar_dec_delay;			//hp bar decrease timer delay
	max_hp_recovery_timer = room_speed * 3;				//Max hp recovery timer
	hp_recovery_timer = max_hp_recovery_timer;			//Recovery hp amount timer
	hp_disp_x = 25;										//X position of the hp display
	hp_disp_y = 35;										//X position of the hp display
	
	max_energy = 100;									//Max amount of energy points
	energy = max_energy;								//Setting energy to equal max energy
	old_energy = energy;								//Setting old energy amount for a delay catch up to energy value
	max_energy_bar_dec_delay = room_speed * 4;			//max old energy decrease timer delay
	energy_bar_dec_delay = max_energy_bar_dec_delay;	//energy bar decrease timer delay
	max_energy_recovery_timer = room_speed * 2;			//Max energy recovery timer
	energy_recovery_timer = max_energy_recovery_timer;	//Recovery energy amount timer
	engy_exert_breath = room_speed * 15;				//Amount of time before needing to breath creating the breathing sound effect
	energy_disp_x = 22;									//X position of the energy display
	energy_disp_y = 40;									//X position of the energy display
	e_runuse = 3;										//Amount of energy used for running
	
	max_breath = 100;									//Max amount of breath points
	breath = max_breath;								//Setting breath to equal max breath
	old_breath = breath;								//Setting old breath amount for a delay catch up to breath value
	breath_disp_x = 5;									//X position of the breath display
	breath_disp_y = 40;									//Y position of the breath display	
	
	max_breath_bar_dec_delay = room_speed * 4;			//max old breath decrease timer delay
	breath_bar_dec_delay = max_breath_bar_dec_delay;	//breath bar decrease timer delay
	max_breath_timer = room_speed * 0.5;				//Maximum breath timer to count down from
	breath_timer = max_breath_timer;					//Breath timer that counts to an amount for the breath to degrade by
	max_breath_recovery_timer = room_speed * 2;			//Max breath timer for recovery if just got out of the water
	breath_recovery_timer = max_breath_recovery_timer;	//Breath timer for recovery
	
	show_ui_hp = false;									//If true it shows the UI of the health bar
	show_ui_energy = false;								//If true it shows the UI of the energy bar
	show_ui_breath = false;								//If true it shows the UI of the breath bar
	#endregion
	
	#region Ranges
	hearingrange = 700;			//Hearing range for sound and visual effects
	aggrorange = 120;			//Aggro range for the animal to begin chasing or running away from the player
	outofsight = 150;			//Out of sight range for the animal
	#endregion
	
#endregion

#region Different Kangaroo Animal Animations, Visual Particle Effects & Sounds
	
	#region Visual Particle Effects & Screenshake
	dirt_jumping =	5;					//Sets amount of dirt particles when jumping
	dirt_landing = 7;					//Set amount of dirt pacticles when landing after jump or fall
	dirt_moving = 1;					//Sets amount of dirt particles when walking
	
	scrshk_landing_mag = 1;				//Set screenshake landing magnitude when landing after jump or fall
	scrshk_landing_frms = 10;			//Set screenshake landing frames when landing after jump or fall
	#endregion
	
	#region Sound Effects
	snd_idle_v1			= sn_kang_cluck_v1;		//Sets idle sound version 1
	snd_idle_v2			= sn_kang_cluck_v2;		//Sets idle sound version 2
	snd_idle_v3			= sn_kang_cluck_v3;		//Sets idle sound version 3
	snd_idle_v4			= sn_kang_cluck_v4;		//Sets idle sound version 4
	
	snd_scared_v1		= sn_kang_growl_v1;		//Sets scared sound version 1
	snd_scared_v2		= sn_kang_hit_v2;		//Sets scared sound version 2
	snd_scared_v3		= sn_kang_growl_v1;		//Sets scared sound version 3
	
	snd_jump_v1			= sn_kang_hit_v2;		//Sets jump sound version 1
	snd_jump_v2			= sn_kang_hit_v2;		//Sets jump sound version 2
	snd_jump_v3			= sn_kang_hit_v4;		//Sets jump sound version 3
	
	snd_landing_v1		= sn_kang_hit_v2;		//Sets landing sound version 1
	snd_landing_v2		= sn_kang_hit_v2;		//Sets landing sound version 2
	snd_landing_v3		= sn_kang_hit_v4;		//Sets landing sound version 3
	
	snd_running_v1		= sn_kang_hit_v1;		//Sets running sound version 1
	snd_running_v2		= sn_kang_hit_v3;		//Sets running sound version 2
	
	snd_death_v1		= sn_kang_hit_v1;		//Sets death sound version 1
	snd_death_v2		= sn_kang_hit_v2;		//Sets death sound version 2
	snd_death_v3		= sn_kang_hit_v3;		//Sets death sound version 3
	
	snd_takinghit_v1	= sn_kang_growl_v1;		//Sets taking hit sound version 1
	snd_takinghit_v2	= sn_kang_hit_v2;		//Sets taking hit sound version 2
	snd_takinghit_v3	= sn_kang_hit_v4;		//Sets taking hit sound version 3
	#endregion
	
	#region Animation
	
	#region Normal World Brown Kangaroo
	if (type_select = "nw_brown") {
	ani_idle = spr_bkang_idle;			//Gets objects idle animation
	ani_walk = spr_bkang_run;			//Gets objects walk animation
	ani_run = spr_bkang_run;			//Gets objects run animation
	ani_jump = spr_bkang_jump;			//Gets objects jump animation
	ani_fall = spr_bkang_fall;			//Gets objects fall animation
	ani_dead = spr_bkang_dead;			//Gets objects death animation
	}
	#endregion
	
	#region Normal World Grey Kangaroo
	if (type_select = "nw_grey") {
	ani_idle = spr_gkang_idle;			//Gets objects idle animation
	ani_walk = spr_gkang_run;			//Gets objects walk animation
	ani_run = spr_gkang_run;			//Gets objects run animation
	ani_jump = spr_gkang_jump;			//Gets objects jump animation
	ani_fall = spr_gkang_fall;			//Gets objects fall animation
	ani_dead = spr_gkang_dead;			//Gets objects death animation
	}
	#endregion
	
	#region Spirit World Brown Kangaroo
	if (type_select = "sw_brown") {
	ani_idle = spr_sw_bkang_idle;		//Gets objects idle animation
	ani_walk = spr_sw_bkang_run;		//Gets objects walk animation
	ani_run = spr_sw_bkang_run;			//Gets objects run animation
	ani_jump = spr_sw_bkang_jump;		//Gets objects jump animation
	ani_fall = spr_sw_bkang_fall;		//Gets objects fall animation
	ani_dead = spr_sw_bkang_dead;		//Gets objects death animation
	}
	#endregion
	
	#region Spirit World Grey Kangaroo
	if (type_select = "sw_grey") {
	ani_idle = spr_sw_gkang_idle;		//Gets objects idle animation
	ani_walk = spr_sw_gkang_run;		//Gets objects walk animation
	ani_run = spr_sw_gkang_run;			//Gets objects run animation
	ani_jump = spr_sw_gkang_jump;		//Gets objects jump animation
	ani_fall = spr_sw_gkang_fall;		//Gets objects fall animation
	ani_dead = spr_sw_gkang_dead;		//Gets objects death animation
	}
	#endregion
	
	#region Random Selection Of Kangaroo Based On World
	if (type_select = "non_selected") {
	#region Normal World
		if (type_random = "nw") { 
			type_select = choose("nw_brown", "nw_grey"); 
			if (type_select = "nw_brown") {
				ani_idle = spr_bkang_idle;			//Gets objects idle animation
				ani_walk = spr_bkang_run;			//Gets objects walk animation
				ani_run = spr_bkang_run;			//Gets objects run animation
				ani_jump = spr_bkang_jump;			//Gets objects jump animation
				ani_fall = spr_bkang_fall;			//Gets objects fall animation
				ani_dead = spr_bkang_dead;			//Gets objects death animation
			}
			if (type_select = "nw_grey") {
				ani_idle = spr_gkang_idle;			//Gets objects idle animation
				ani_walk = spr_gkang_run;			//Gets objects walk animation
				ani_run = spr_gkang_run;			//Gets objects run animation
				ani_jump = spr_gkang_jump;			//Gets objects jump animation
				ani_fall = spr_gkang_fall;			//Gets objects fall animation
				ani_dead = spr_gkang_dead;			//Gets objects death animation
			}
		}
	#endregion
	
	#region Spirit World
		if (type_random = "sw") { 
			type_select = choose("sw_brown", "sw_grey"); 
			if (type_select = "sw_brown") {
				ani_idle = spr_sw_bkang_idle;		//Gets objects idle animation
				ani_walk = spr_sw_bkang_run;		//Gets objects walk animation
				ani_run = spr_sw_bkang_run;			//Gets objects run animation
				ani_jump = spr_sw_bkang_jump;		//Gets objects jump animation
				ani_fall = spr_sw_bkang_fall;		//Gets objects fall animation
				ani_dead = spr_sw_bkang_dead;		//Gets objects death animation
			}
			if (type_select = "sw_grey") {
				ani_idle = spr_sw_gkang_idle;		//Gets objects idle animation
				ani_walk = spr_sw_gkang_run;		//Gets objects walk animation
				ani_run = spr_sw_gkang_run;			//Gets objects run animation
				ani_jump = spr_sw_gkang_jump;		//Gets objects jump animation
				ani_fall = spr_sw_gkang_fall;		//Gets objects fall animation
				ani_dead = spr_sw_gkang_dead;		//Gets objects death animation
			}
		}
	#endregion
	}
	#endregion
	
	#endregion
	
#endregion