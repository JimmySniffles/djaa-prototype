/// @description Drawing Self, Stats & Weapon Icons
draw_self();
if (state != states.death) { scr_stats_display(x-hp_disp_x, y-hp_disp_y, x-energy_disp_x, y-energy_disp_y, x-breath_disp_x, y-breath_disp_y); }

#region Flash Effects
//Invincibility flash pulse effect
if (invincible) { invincible_flash_timer ++; }
if (invincible_flash_timer >= room_speed * 2) { whiteflash = 0.8; invincible_flash_timer = 0; }
if (whiteflash > 0) && (takingdamage == true) { scr_shader_flash(0.05,1.0,1.0,1.0); }
if (whiteflash > 0) && (takingdamage == false) && (state != states.death) { scr_shader_flash(0.02,1.0,1.0,1.0); }

//Death flash & fade effect
if (whiteflash > 0) && (state == states.death) { scr_shader_flash(0.01,1.0,1.0,1.0); }

if (blueflash > 0) { scr_shader_flash(0.05,0,1.0,1.0); }
if (redflash > 0) { scr_shader_flash(0.05,1.0,0,0); }
if (greenflash > 0) { scr_shader_flash(0.05,0,0,1.0); }
#endregion

#region Weapons Icon Display

	#region Melee Club Icon
	if (weaponpickedup) || (weaponswitch) {
		if (obj_b_wloadout.weapon = 1) {
			if (obj_b_wloadout.ammo[1] = -1) {
				draw_sprite_ext(spr_meleeclub_icon,1,x-13,y-30,1,1,20,c_white,0.8);
				if (obj_b_wloadout.ammo[2] = -1) { draw_sprite_ext(spr_boomerang_icon,0,x-5,y-32,1,1,10,c_white,0.8); }
				if (obj_b_wloadout.ammo[3] = -1) { draw_sprite_ext(spr_spear_icon,0,x+3,y-32,1,1,350,c_white,0.8); }
				if (obj_b_wloadout.ammo[4] = -1) { draw_sprite_ext(spr_killingboomerang_icon,0,x+10,y-30,1,1,340,c_white,0.8); }	
			}	
		}
	}
	#endregion

	#region Stunning Boomerang Icon
	if (weaponpickedup) || (weaponswitch) {
		if (obj_b_wloadout.weapon = 2) {
			if (obj_b_wloadout.ammo[2] = -1) {
				draw_sprite_ext(spr_boomerang_icon,1,x-5,y-32,1,1,10,c_white,0.8);
				if (obj_b_wloadout.ammo[1] = -1) { draw_sprite_ext(spr_meleeclub_icon,0,x-13,y-30,1,1,20,c_white,0.8); }
				if (obj_b_wloadout.ammo[3] = -1) { draw_sprite_ext(spr_spear_icon,0,x+3,y-32,1,1,350,c_white,0.8); }
				if (obj_b_wloadout.ammo[4] = -1) { draw_sprite_ext(spr_killingboomerang_icon,0,x+10,y-30,1,1,340,c_white,0.8); }	
			}	
		}
	}
	#endregion

	#region Spear Icon
	if (weaponpickedup) || (weaponswitch) {
		if (obj_b_wloadout.weapon = 3) {
			if (obj_b_wloadout.ammo[3] = -1) {
				draw_sprite_ext(spr_spear_icon,1,x+3,y-32,1,1,350,c_white,0.8);
				if (obj_b_wloadout.ammo[1] = -1) { draw_sprite_ext(spr_meleeclub_icon,0,x-13,y-30,1,1,20,c_white,0.8); }
				if (obj_b_wloadout.ammo[2] = -1) { draw_sprite_ext(spr_boomerang_icon,0,x-5,y-32,1,1,10,c_white,0.8); }
				if (obj_b_wloadout.ammo[4] = -1) { draw_sprite_ext(spr_killingboomerang_icon,0,x+10,y-30,1,1,340,c_white,0.8); }	
			}	
		}
	}
	#endregion

	#region Killing Boomerang Icon
	if (weaponpickedup) || (weaponswitch) {
		if (obj_b_wloadout.weapon = 4) {
			if (obj_b_wloadout.ammo[4] = -1) {
				draw_sprite_ext(spr_killingboomerang_icon,1,x+10,y-30,1,1,340,c_white,0.8);
				if (obj_b_wloadout.ammo[1] = -1) { draw_sprite_ext(spr_meleeclub_icon,0,x-13,y-30,1,1,20,c_white,0.8); }
				if (obj_b_wloadout.ammo[2] = -1) { draw_sprite_ext(spr_boomerang_icon,0,x-5,y-32,1,1,10,c_white,0.8); }
				if (obj_b_wloadout.ammo[3] = -1) { draw_sprite_ext(spr_spear_icon,0,x+3,y-32,1,1,350,c_white,0.8); }	
			}	
		}
	}
	#endregion
	
#endregion