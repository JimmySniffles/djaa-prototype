///@description Setting Variables
scr_player_var();
level = 1;
experience = 0;
max_experience = 10;

#region Setting Player Type, Dialogue & Name Classification Variables
reset_dialogue_defaults();
myPortrait			= spr_portrait_budburra;
myVoice				= -1;
myFont				= fnt_dialogue;
myName				= "Budburra";

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;

player_type			= obj_boy;
#endregion

#region Movement Variables

	#region Gravity Variables
	grv = 0.3;								//Standard Gravity
	grv_wall = 0.1;							//Gravity on a wall
	grv_atk = 0.01;							//Gravity while attacking if in air
	#endregion

	#region Horizontal Movement Variables
	hsp_wmsp = 0;							//Horizontal walking movement speed
	hsp_wmaxsp = 1.5;						//Maximum horizontal walking movement speed
	hsp_msp = 0;							//Horizontal movement speed
	hsp_maxsp = 3;							//Maximum horizontal movement speed
	hsp_acc = 1;							//Horizontal acceleration
	hsp_fric_ground = 0.50;					//Horizontal friction on the ground
	hsp_fric_air = 0.15;					//Horizontal friction in the air
	
	roll_spd = 3.5;							//The roll dodge speed
	roll_ani_spd = 0.6;						//The roll animation speed
	
	gslide_spd = 3.5;						//The ground slide dodge speed
	gslide_fric = 0.10;						//The ground slide friction to slow down by
	gslide_ani_spd = 1;						//The ground slide animation speed
	#endregion

	#region Vertical Jumping & Speed Downwards Variables
	vsp_jump = -10;							//Vertical jump height
	vsp_max = 12;							//Vertical maximum speed downwards (known as terminal velocity)
	vsp_max_wall = 5;						//Maximum vertical speed when on a wall		//Make sure of: Less than half of the vsp_max speed when falling (Make sure to set this to be less than half)
	vsp_max_atk = 3;						//Maximum vertical speed when attacking
	#endregion

#endregion

#region Player Object Status

	#region Health, Energy, Breath Variables
	max_hp = 100;										//Max amount of health points
	hp = max_hp;										//Setting hp to equal max hp
	old_hp = hp;										//Setting old hp amount for a delay catch up to hp value
	max_hp_bar_dec_delay = room_speed * 1;				//max old hp decrease timer delay
	hp_bar_dec_delay = max_hp_bar_dec_delay;			//hp bar decrease timer delay
	max_hp_recovery_timer = room_speed * 4;				//Max hp recovery timer
	hp_recovery_timer = max_hp_recovery_timer;			//Recovery hp amount timer
	hp_disp_x = 30;										//X position of the hp display
	hp_disp_y = 30;										//X position of the hp display

	max_energy = 100;									//Max amount of energy points
	energy = max_energy;								//Setting energy to equal max energy
	old_energy = energy;								//Setting old energy amount for a delay catch up to energy value
	max_energy_bar_dec_delay = room_speed * 4;			//max old energy decrease timer delay
	energy_bar_dec_delay = max_energy_bar_dec_delay;	//energy bar decrease timer delay
	max_energy_recovery_timer = room_speed * 2;			//Max energy recovery timer
	energy_recovery_timer = max_energy_recovery_timer;	//Recovery energy amount timer
	engy_exert_breath = room_speed * 10;				//Amount of time before needing to breath creating the breathing sound effect
	energy_disp_x = 28;									//X position of the energy display
	energy_disp_y = 35;									//X position of the energy display
	e_jumpuse = 4;										//Amount of energy used for jumping
	e_runuse = 3;										//Amount of energy used for running
	e_rolluse = 3;										//Amount of energy used for rolling
	e_gslideuse = 2;									//Amount of energy used for ground sliding
	
	max_breath = 100;									//Max amount of breath points
	breath = max_breath;								//Setting breath to equal max breath
	old_breath = breath;								//Setting old breath amount for a delay catch up to breath value
	max_breath_bar_dec_delay = room_speed * 4;			//max old breath decrease timer delay
	breath_bar_dec_delay = max_breath_bar_dec_delay;	//breath bar decrease timer delay
	max_breath_timer = room_speed * 1;					//Maximum breath timer to count down from
	breath_timer = max_breath_timer;					//Breath timer that counts to an amount for the breath to degrade by
	max_breath_recovery_timer = room_speed * 1;			//Max breath timer for recovery if just got out of the water
	breath_recovery_timer = max_breath_recovery_timer;	//Breath timer for recovery
	breath_disp_x = 0;									//X position of the breath display
	breath_disp_y = 35;									//Y position of the breath display	
	#endregion

	#region Weapon Check, Icon Display and Spear Check Variables
	hitbyattack = ds_list_create();				//This variable keeps DS List of objects that are attacked
	weaponswitch = false;						//Returns true if a weapon has switched
	weaponpickedup = false;						//Returns true if a weapon has been picked up
	max_weapondisp_timer = room_speed * 4;		//Sets the max weapon display timer to count down when the player has died
	weapondisp_timer = max_weapondisp_timer;	//Sets the weapon display timer to equal max weapon display timer
	#endregion
	
#endregion

#region Animation, Visual Particle Effects & Sounds
	
	#region Visual Particle Effects & Screenshake
	dirt_jumping =	4;					//Sets amount of dirt particles when jumping
	dirt_landing = 6;					//Set amount of dirt pacticles when landing after jump or fall
	dirt_moving_wlk = 1;				//Sets amount of dirt particles when walking
	dirt_moving_run = 2;				//Sets amount of dirt particles when running
	dirt_sliding = 1;					//Sets amount of dirt particles when sliding
	dirt_gsliding =	1;					//Sets amount of dirt particles when ground sliding
	dirt_dodge = 7;						//Sets amount of dirt particles when landing the dodge
	
	scrshk_landing_mag = 1;				//Set screenshake landing magnitude when landing after jump or fall
	scrshk_landing_frms = 20;			//Set screenshake landing frames when landing after jump or fall
	
	scrshk_dodge_mag = 1;				//Set screenshake magnitude when dodging
	scrshk_dodge_frms = 20;				//Set screenshake frames when dodging
	#endregion
	
	#region Sound Effects
	snd_idle_v1			= sn_deadly_v1;				//Sets idle sound version 1
	snd_idle_v2			= sn_deadly_v2;				//Sets idle sound version 2
	snd_idle_v3			= sn_deadly_v3;				//Sets idle sound version 3
	snd_idle_v4			= sn_laugh_v1;				//Sets idle sound version 4
	
	snd_pickup_v1		= sn_pickup_v1;				//Sets pickup sound version 1
	snd_pickup_v2		= sn_pickup_v2;				//Sets pickup sound version 2

	snd_attack_v1		= sn_attack_v1;				//Sets attack sound version 1
	snd_attack_v2		= sn_attack_v2;				//Sets attack sound version 2
	snd_attack_v3		= sn_throw;					//Sets attack sound version 3
	
	snd_slidecall_v1	= sn_deadly_v1;				//Sets slide call sound version 1
	snd_slidecall_v2	= sn_deadly_v2;				//Sets slide call sound version 2
	snd_slidecall_v3	= sn_deadly_v3;				//Sets slide call sound version 3
	
	snd_jump_v1			= sn_jump;					//Sets jump sound version 1
	snd_jump_v2			= sn_jump;					//Sets jump sound version 2
	snd_jump_v3			= sn_jump;					//Sets jump sound version 3
	
	snd_landing_v1		= sn_attack_v1;				//Sets landing sound version 1
	snd_landing_v2		= sn_attack_v2;				//Sets landing sound version 2
	snd_landing_v3		= sn_attack_v2;				//Sets landing sound version 3
	
	snd_running_v1		= sn_running_v1;			//Sets running sound version 1
	snd_running_v2		= sn_running_v2;			//Sets running sound version 2
	
	snd_swallow_v1		= sn_swallow_v1;			//Sets swallow sound version 1
	snd_swallow_v2		= sn_swallow_v2;			//Sets swallow sound version 2
	
	snd_death_v1		= sn_death_v1;				//Sets death sound version 1
	snd_death_v2		= sn_death_v2;				//Sets death sound version 2
	snd_death_v3		= sn_death_v3;				//Sets death sound version 3
	snd_death_v4		= sn_death_v4;				//Sets death sound version 4
	
	snd_takinghit_v1	= sn_oww;					//Sets taking hit sound version 1
	snd_takinghit_v2	= sn_death_v2;				//Sets taking hit sound version 2
	snd_takinghit_v3	= sn_death_v2;				//Sets taking hit sound version 3
	#endregion
	
	#region Animation
	
		#region General Animations
		ani_idle = spr_boy_idle;			//Gets objects idle animation
		ani_walk = spr_boy_walk;			//Gets objects walk animation
		ani_run = spr_boy_run;				//Gets objects run animation
		ani_jump = spr_boy_jump;			//Gets objects jump animation
		ani_fall = spr_boy_fall;			//Gets objects fall animation
		ani_slide = spr_boy_slide;			//Gets objects slide animation
		ani_gslide = spr_boy_gslide;		//Gets objects ground slide animation
		ani_dodge = spr_boy_dodge;			//Gets objects dodge animation
		ani_interact = spr_boy_interact;	//Gets objects grab animation
		ani_throw = spr_boy_throw;			//Gets objects throw animation
		ani_push = spr_boy_push;			//Gets objects push animation
		ani_dead = spr_boy_dead;			//Gets objects death animation
		ani_sitting = spr_boy_sitting;		//Gets objects sitting animation
		#endregion
	
		#region Attack Animations
	
			#region Attack Stage 1 Animations & Hitbox
		
				#region Club
				//Run Attack
				ani_clb_runatk = spr_boy_clb_runatk;					//Gets run club attack animation
				ani_clb_runatk_hb = spr_boy_clb_runatk_hb;				//Gets run club attack hitbox
			
				//Normal Attack On Ground & In Air
				ani_clb_na_g_stg1 = spr_boy_clb_na_g_stg1;				//Gets stage 1 normal attack ground club attack animation
				ani_clb_na_g_stg1_hb = spr_boy_clb_na_g_stg1_hb;		//Gets stage 1 normal attack ground club attack hitbox
				ani_clb_na_a_stg1 = spr_boy_clb_na_a_stg1;				//Gets stage 1 normal attack air club attack animation
				ani_clb_na_a_stg1_hb = spr_boy_clb_na_a_stg1_hb;		//Gets stage 1 normal attack air club attack hitbox
			
				//Special Attack On Ground & In Air
				ani_clb_sa_g_stg1 = spr_boy_clb_sa_g_stg1;				//Gets stage 1 special attack ground club attack animation
				ani_clb_sa_g_stg1_hb = spr_boy_clb_sa_g_stg1_hb;		//Gets stage 1 special attack ground club attack hitbox
				ani_clb_sa_a_stg1 = spr_boy_clb_sa_a_stg1;				//Gets stage 1 special attack air club attack animation
				ani_clb_sa_a_stg1_hb = spr_boy_clb_sa_a_stg1_hb;		//Gets stage 1 special attack air club attack hitbox
				#endregion
			
				#region Stun Boomerang
				//Run Attack
				ani_stb_runatk = spr_boy_stb_runatk;					//Gets run stun boomerang attack animation
			
				//Normal Attack On Ground & In Air
				ani_stb_na_g = spr_boy_stb_na_g;						//Gets normal attack ground stun boomerang attack animation
				ani_stb_na_a = spr_boy_stb_na_a;						//Gets normal attack air stun boomerang attack animation
			
				//Special Attack On Ground & In Air
				ani_stb_sa_g_stg1 = spr_boy_stb_sa_g_stg1;				//Gets stage 1 special attack ground stun boomerang attack animation
				ani_stb_sa_g_stg1_hb = spr_boy_stb_sa_g_stg1_hb;		//Gets stage 1 special attack ground stun boomerang attack hitbox
				ani_stb_sa_a_stg1 = spr_boy_stb_sa_a_stg1;				//Gets stage 1 special attack air stun boomerang attack animation
				ani_stb_sa_a_stg1_hb = spr_boy_stb_sa_a_stg1_hb;		//Gets stage 1 special attack air stun boomerang attack hitbox
				#endregion
			
				#region Spear
				//Run Attack
				ani_spr_runatk = spr_boy_spr_runatk;					//Gets run spear attack animation
			
				//Normal Attack On Ground & In Air
				ani_spr_na_g = spr_boy_spr_na_g;						//Gets normal attack ground spear attack animation
				ani_spr_na_a = spr_boy_spr_na_a;						//Gets normal attack air spear attack animation
			
				//Special Attack On Ground & In Air
				ani_spr_sa_g_stg1 = spr_boy_spr_sa_g_stg1;				//Gets stage 1 special attack ground spear attack animation
				ani_spr_sa_g_stg1_hb = spr_boy_spr_sa_g_stg1_hb;		//Gets stage 1 special attack ground spear attack hitbox
				ani_spr_sa_a_stg1 = spr_boy_spr_sa_a_stg1;				//Gets stage 1 special attack air spear attack animation
				ani_spr_sa_a_stg1_hb = spr_boy_spr_sa_a_stg1_hb;		//Gets stage 1 special attack air spear attack hitbox
				#endregion
			
				#region Kill Boomerang
				//Run Attack
				ani_klb_runatk = spr_boy_klb_runatk;					//Gets run kill boomerang attack animation
			
				//Normal Attack On Ground & In Air
				ani_klb_na_g = spr_boy_klb_na_g;						//Gets normal attack ground kill boomerang attack animation
				ani_klb_na_a = spr_boy_klb_na_a;						//Gets normal attack air kill boomerang attack animation
			
				//Special Attack On Ground & In Air
				ani_klb_sa_g_stg1 = spr_boy_klb_sa_g_stg1;				//Gets stage 1 special attack ground kill boomerang attack animation
				ani_klb_sa_g_stg1_hb = spr_boy_klb_sa_g_stg1_hb;		//Gets stage 1 special attack ground kill boomerang attack hitbox
				ani_klb_sa_a_stg1 = spr_boy_klb_sa_a_stg1;				//Gets stage 1 special attack air kill boomerang attack animation
				ani_klb_sa_a_stg1_hb = spr_boy_klb_sa_a_stg1_hb;		//Gets stage 1 special attack air kill boomerang attack hitbox
				#endregion
			
			#endregion
	
			#region Attack Stage 2 Animations & Hitbox
			
				#region Club			
				//Normal Attack On Ground & In Air
				//ani_clb_na_g_stg2 = spr_boy_clb_na_g_stg2;			//Gets stage 2 normal attack ground club attack animation
				//ani_clb_na_g_stg2_hb = spr_boy_clb_na_g_stg2_hb;		//Gets stage 2 normal attack ground club attack hitbox
				//ani_clb_na_a_stg2 = spr_boy_clb_na_a_stg2;			//Gets stage 2 normal attack air club attack animation
				//ani_clb_na_a_stg2_hb = spr_boy_clb_na_a_stg2_hb;		//Gets stage 2 normal attack air club attack hitbox
			
				//Special Attack On Ground & In Air
				//ani_clb_sa_g_stg2 = spr_boy_clb_sa_g_stg2;			//Gets stage 2 special attack ground club attack animation
				//ani_clb_sa_g_stg2_hb = spr_boy_clb_sa_g_stg2_hb;		//Gets stage 2 special attack ground club attack hitbox
				//ani_clb_sa_a_stg2 = spr_boy_clb_sa_a_stg2;			//Gets stage 2 special attack air club attack animation
				//ani_clb_sa_a_stg2_hb = spr_boy_clb_sa_a_stg2_hb;		//Gets stage 2 special attack air club attack hitbox
				#endregion
			
				#region Stun Boomerang
				//Special Attack On Ground & In Air
				//ani_stb_sa_g_stg2 = spr_boy_stb_sa_g_stg2;			//Gets stage 2 special attack ground stun boomerang attack animation
				//ani_stb_sa_g_stg2_hb = spr_boy_stb_sa_g_stg2_hb;		//Gets stage 2 special attack ground stun boomerang attack hitbox
				//ani_stb_sa_a_stg2 = spr_boy_stb_sa_a_stg2;			//Gets stage 2 special attack air stun boomerang attack animation
				//ani_stb_sa_a_stg2_hb = spr_boy_stb_sa_a_stg2_hb;		//Gets stage 2 special attack air stun boomerang attack hitbox
				#endregion
			
				#region Spear
				//Special Attack On Ground & In Air
				//ani_spr_sa_g_stg2 = spr_boy_spr_sa_g_stg2;			//Gets stage 2 special attack ground spear attack animation
				//ani_spr_sa_g_stg2_hb = spr_boy_spr_sa_g_stg2_hb;		//Gets stage 2 special attack ground spear attack hitbox
				//ani_spr_sa_a_stg2 = spr_boy_spr_sa_a_stg2;			//Gets stage 2 special attack air spear attack animation
				//ani_spr_sa_a_stg2_hb = spr_boy_spr_sa_a_stg2_hb;		//Gets stage 2 special attack air spear attack hitbox
				#endregion
			
				#region Kill Boomerang
				//Special Attack On Ground & In Air
				//ani_klb_sa_g_stg2 = spr_boy_klb_sa_g_stg2;			//Gets stage 2 special attack ground kill boomerang attack animation
				//ani_klb_sa_g_stg2_hb = spr_boy_klb_sa_g_stg2_hb;		//Gets stage 2 special attack ground kill boomerang attack hitbox
				//ani_klb_sa_a_stg2 = spr_boy_klb_sa_a_stg2;			//Gets stage 2 special attack air kill boomerang attack animation
				//ani_klb_sa_a_stg2_hb = spr_boy_klb_sa_a_stg2_hb;		//Gets stage 2 special attack air kill boomerang attack hitbox
				#endregion
			
			#endregion
	
			#region Attack Stage 3 Animations & Hitbox
		
				#region Club			
				//Normal Attack On Ground & In Air
				//ani_clb_na_a_stg3 = spr_boy_clb_na_a_stg3;			//Gets stage 3 normal attack air club attack animation
				//ani_clb_na_a_stg3_hb = spr_boy_clb_na_a_stg3_hb;		//Gets stage 3 normal attack air club attack hitbox
			
				//Special Attack In Air
				//ani_clb_sa_a_stg3 = spr_boy_clb_sa_a_stg3;			//Gets stage 3 special attack air club attack animation
				//ani_clb_sa_a_stg3_hb = spr_boy_clb_sa_a_stg3_hb;		//Gets stage 3 special attack air club attack hitbox
				#endregion
			
				#region Stun Boomerang
				//Special Attack On Ground & In Air
				//ani_stb_sa_g_stg3 = spr_boy_stb_sa_g_stg3;			//Gets stage 3 special attack ground stun boomerang attack animation
				//ani_stb_sa_g_stg3_hb = spr_boy_stb_sa_g_stg3_hb;		//Gets stage 3 special attack ground stun boomerang attack hitbox
				//ani_stb_sa_a_stg3 = spr_boy_stb_sa_a_stg3;			//Gets stage 3 special attack air stun boomerang attack animation
				//ani_stb_sa_a_stg3_hb = spr_boy_stb_sa_a_stg3_hb;		//Gets stage 3 special attack air stun boomerang attack hitbox
				#endregion
			
				#region Spear
				//Special Attack On Ground & In Air
				//ani_spr_sa_g_stg3 = spr_boy_spr_sa_g_stg3;			//Gets stage 3 special attack ground spear attack animation
				//ani_spr_sa_g_stg3_hb = spr_boy_spr_sa_g_stg3_hb;		//Gets stage 3 special attack ground spear attack hitbox
				//ani_spr_sa_a_stg3 = spr_boy_spr_sa_a_stg3;			//Gets stage 3 special attack air spear attack animation
				//ani_spr_sa_a_stg3_hb = spr_boy_spr_sa_a_stg3_hb;		//Gets stage 3 special attack air spear attack hitbox
				#endregion
			
				#region Kill Boomerang
				//Special Attack On Ground & In Air
				//ani_klb_sa_g_stg3 = spr_boy_klb_sa_g_stg3;			//Gets stage 3 special attack ground kill boomerang attack animation
				//ani_klb_sa_g_stg3_hb = spr_boy_klb_sa_g_stg3_hb;		//Gets stage 3 special attack ground kill boomerang attack hitbox
				//ani_klb_sa_a_stg3 = spr_boy_klb_sa_a_stg3;			//Gets stage 3 special attack air kill boomerang attack animation
				//ani_klb_sa_a_stg3_hb = spr_boy_klb_sa_a_stg3_hb;		//Gets stage 3 special attack air kill boomerang attack hitbox
				#endregion
			
			#endregion
		
		#endregion
	
	#endregion
	
#endregion