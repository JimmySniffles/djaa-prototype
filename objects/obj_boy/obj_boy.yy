{
    "id": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boy",
    "eventList": [
        {
            "id": "6be63efd-74fe-4258-bbe9-214c87e907af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "6df2c8f0-cce1-4874-8543-0c597a2fe3d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "5da60985-0e4e-46cf-8fe5-affdf0ce96e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "3338f933-5824-4e3e-8913-31ca37d65cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "10e3be05-d712-4a09-9399-19af0febc1c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "dd83a01a-6baf-468c-93e9-4bec98b2ee84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "2f5d9e68-f4bf-4614-abaf-a139db1fcdee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c27cd1d-35fb-432a-bd5a-734a907ec783",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        },
        {
            "id": "9a69a438-4dee-4b01-bbf7-978d17c23a29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "750c9283-bbbf-4316-b7d0-ee6ba7dc84e1"
        }
    ],
    "maskSpriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
    "visible": true
}