/// @description Cooldown Set & Berry Boost Reversion
cooldown = 0;

#region Berry Boost Reversion
if (berryeaten = string("yellow")) || (berryeaten = string("black")) {
	hsp_msp = 3;				
	hsp_maxsp = 3;				
	vsp_jump = -8;			    
}

if (berryeaten = string("orange")) {
	obj_b_wloadout.ply_dmg = obj_b_wloadout.ply_dmg / 1.3;
	obj_b_wloadout.ani_dmg = obj_b_wloadout.ani_dmg / 1.3;
	obj_b_wloadout.env_dmg = obj_b_wloadout.env_dmg / 1.3;
}

if (berryeaten = string("white")) {
	obj_b_wloadout.ply_dmg = obj_b_wloadout.ply_dmg * 1.3;
	obj_b_wloadout.ani_dmg = obj_b_wloadout.ani_dmg / 1.3;
	obj_b_wloadout.env_dmg = obj_b_wloadout.env_dmg * 1.3;
	
}

if (berryeaten = string("lemon_myrtle")) { invincible = false; }
#endregion