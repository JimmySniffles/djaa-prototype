/// @description Mechanics, Sounds, Effects and Collision Detection

switch(state) {
	case killboomerang_states.thrown:
	{
		#region Sound Effect When Flying
		if (instance_exists(obj_boy)) {
			if (distance_to_object(obj_boy) < 500)
			{
				if (!audio_is_playing(sn_boomerang_air)) audio_play_sound(sn_boomerang_air,5,false);
				audio_sound_gain(sn_boomerang_air,(500-distance_to_object(obj_boy))/500,0);
			}
		}
		#endregion
		
		#region Checks If The Boomerang Has Travelled The Max Distance And If So Then Change To Returning State
		if (abs(x - startx) > maxdistance) || (abs(y - starty) > maxdistance) { state = killboomerang_states.returning; }
		#endregion
		
		#region Collision Detection With Tile Collision And Objects And If So Then Change To Returning State, Play Sound, Screen Shake, Create dirt
		if (scr_infloor(global.tilemap, x, y) >= 0) || (place_meeting(x,y,par_objects))
		{
			if (!audio_is_playing(sn_woodhitgrass_v1)) && (!audio_is_playing(sn_woodhitgrass_v2)) && (!audio_is_playing(sn_woodhitgrass_v3)) {
				audio_play_sound(choose(sn_woodhitgrass_v1,sn_woodhitgrass_v2,sn_woodhitgrass_v3),5,false);
			}
		scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
		repeat(8) { scr_dirtparticle(x, y, dirtpart.jumping); }
		state = killboomerang_states.returning;
		}
		#endregion
		
		#region Collision Detection With Enemies And Animals
		var enemyhit = instance_place(x,y,par_enemy);
		with (enemyhit)
		{
			if (state != e_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ene_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ene_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		var animalhit = instance_place(x,y,par_animal);
		with (animalhit)
		{
			if (state != a_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ani_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ani_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		#endregion
		
		#region Collision Detection For Damageable Objects
		var ip_rockhit = instance_place(x,y,obj_dmgrock);
		with (ip_rockhit)
		{
			audio_play_sound(choose(sn_woodhitrock_v1,sn_woodhitrock_v2,sn_woodhitrock_v3),5,false);
			scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
			repeat(obj_b_wloadout.env_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
			repeat(obj_b_wloadout.env_particles) { scr_dirtparticle(other.x, other.y, dirtpart.jumping); }
			hp -= obj_b_wloadout.env_dmg;
			other.state = killboomerang_states.returning;
		}

		var ip_roothit = instance_place(x,y,obj_dmgroot);
		with (ip_roothit)
		{
			audio_play_sound(choose(sn_woodhitroot_v1,sn_woodhitroot_v2,sn_woodhitroot_v3),5,false);
			scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
			repeat(obj_b_wloadout.env_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
			repeat(obj_b_wloadout.env_particles) { scr_dirtparticle(other.x, other.y, dirtpart.jumping); }
			hp -= obj_b_wloadout.env_dmg;
			other.state = killboomerang_states.returning;
		}
		#endregion
	}
	break;
	
	case killboomerang_states.returning:
	{		
		#region Returns Weapon To Player Position
		direction = point_direction(x,y, obj_boy.x, obj_boy.y);
		speed = returnairspeed;
		#endregion
		
		#region Sound Effect When Flying
		if (instance_exists(obj_boy)) {
			if (distance_to_object(obj_boy) < 500)
			{
				if (!audio_is_playing(sn_boomerang_air)) audio_play_sound(sn_boomerang_air,5,false);
				audio_sound_gain(sn_boomerang_air,(500-distance_to_object(obj_boy))/500,0);
			}
		}
		#endregion
		
		#region Collision Detection With Enemies And Animals
		var enemyhit = instance_place(x,y,par_enemy);
		with (enemyhit)
		{
			if (state != e_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ene_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ene_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		var animalhit = instance_place(x,y,par_animal);
		with (animalhit)
		{
			if (state != a_states.death)
			{
				takingdamage = true;
				if (!invincible) {
					audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
					scr_screenshake(obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm);
					repeat(obj_b_wloadout.ani_particles) { instance_create_layer(other.x,other.y,"Effects",obj_hitspark); }
					hp -= obj_b_wloadout.ani_dmg/3;
					stun = obj_b_wloadout.stun;
				} else {
					audio_play_sound(sn_invincibility,5,false);
				}
			}
		}
		#endregion
			
		#region Collision With Player
		if (place_meeting(x,y,obj_boy))
		{
			if (!audio_is_playing(sn_weaponcatch_v1)) && (!audio_is_playing(sn_weaponcatch_v2)) && (!audio_is_playing(sn_weaponcatch_v3)) {
				audio_play_sound(choose(sn_weaponcatch_v1,sn_weaponcatch_v2,sn_weaponcatch_v3),5,false);
			}
			scr_screenshake(2,5);
			with (obj_b_wloadout) { ammo[wep_type.killboomerang] = -1; }
			instance_destroy();
		}
		#endregion	
	}
	break;
}