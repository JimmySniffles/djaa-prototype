{
    "id": "9e8001fa-c278-4e01-ae3a-6807861aa70d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_killboomerang",
    "eventList": [
        {
            "id": "f2901712-b71e-4346-a315-f6c8ca5211c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e8001fa-c278-4e01-ae3a-6807861aa70d"
        },
        {
            "id": "ea519284-4f95-47d3-b566-57caec880cc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e8001fa-c278-4e01-ae3a-6807861aa70d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
    "visible": true
}