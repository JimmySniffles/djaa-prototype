/// @description Intializing Variables

#region Setting State
state = killboomerang_states.thrown;
#endregion

#region Coordinates And Speed Variable Intialization
startx = x;
starty = y;
maxdistance = obj_b_wloadout.proj_dist;
vsp = 0;
returnairspeed = obj_b_wloadout.proj_spd;
#endregion