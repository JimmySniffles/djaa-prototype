{
    "id": "45510db6-658a-4983-ba47-c62c4495b4d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bat",
    "eventList": [
        {
            "id": "84d39d90-834d-4864-b5aa-1e59fd19d324",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45510db6-658a-4983-ba47-c62c4495b4d0"
        },
        {
            "id": "7de7a19c-3640-430f-847b-9c01e8657814",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45510db6-658a-4983-ba47-c62c4495b4d0"
        },
        {
            "id": "2d05928a-8e5d-4d97-b3e1-014efa21d7e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "45510db6-658a-4983-ba47-c62c4495b4d0"
        },
        {
            "id": "c53426f6-e12c-4ba9-b0db-a432997e9732",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "45510db6-658a-4983-ba47-c62c4495b4d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
    "visible": true
}