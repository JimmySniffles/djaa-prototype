/// @description Intializing Variables

#region Random Movement And Hearing Range Variables
startx = x;
starty = y;
bored = 0;
hearingrange = 400;
idletimer = 0;
#endregion

#region Horizontal, Vertical Variables & Fractions
hsp = 0;					//Horizontal Speed
vsp = 0;					//Vertical Speed
hsp_frac = 0;				//Horizontal Fraction
vsp_frac = 0;				//Vertical Fraction
hsp_carry = 0;				//Horizontal carry on moving platforms
vsp_carry = 0;				//Vertical carry on moving platforms
my_platform = noone;
#endregion

onground = false;			//Returning true or false whether on the ground
onwall = 0;					//Setting variable if whether the animal is on a wall from the right or the left
collided = false;			//Checking if collided with a collision wall
	
#region Horizontal & Vertical Movement Variables
hsp_msp = 4;				//Horizontal movement speed
hsp_maxsp = 4;				//Maximum horizontal movement speed
hsp_acc = 1;				//Horizontal acceleration

vsp_msp = 3;				//Vertical movement speed
vsp_maxsp = 3;				//Maximum vertical movement speed
vsp_acc = 1;				//Vertical acceleration
#endregion