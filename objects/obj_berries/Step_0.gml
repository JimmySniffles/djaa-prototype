/// @description Variable Change For Player If Berry Is Collected

#region Flash Timer For Shader
if (flashtimer < room_speed * 1.5) { flashtimer ++; }
if (flashtimer >= room_speed * 1.5) { flashtimer = 0 whiteflash = 1; }
#endregion

#region Berry Mechanics

	#region Finger Lime Berry Mechanic | Increases Health If Health Is Below Max
	if (instance_exists(player)) {
		if (image_index == berrytype.fingerlime) && (collected) {
			with (player) {
				if (hp >= max_hp * 0.5) {
					hp = max_hp;
					greenflash = 1;
				}
				else { hp += max_hp * 0.5; greenflash = 1; }
			}
		instance_destroy();
		}
	}
	#endregion
	
	#region Lemon Myrtle Berry Mechanic | Invincibility For A Period Of Time
	if (instance_exists(player)) {
		if (image_index == berrytype.lemonmyrtle) && (collected) {
			with (player) {
				whiteflash		= 1;
				cooldown		= 10;
				invincible		= true;
				berryeaten		= string("lemon_myrtle");
				if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
			}
		instance_destroy();	
		}
	}
	#endregion
	
#endregion

#region Old Berry Mechanics
/*
	#region Blue Berry Mechanic | Increase Energy
	if (instance_exists(player)) {
		if (image_index = 0) && (collected) {
			with (player) {
				if (energy >= max_energy * 0.5) {
					energy = max_energy;
				}
				else { energy += max_energy * 0.5; }
			}
			instance_destroy();
		}
	}
	#endregion
		
	#region Yellow Berry Mechanic | Increase Movement Speed and Jump Height For A Period Of Time
	if (instance_exists(player)) {
		if (image_index = 1) && (collected) {
			with (player) {
				cooldown		= 10;
				boostmultiplier	= 1.3;
				hsp_msp			= hsp_msp * boostmultiplier;
				hsp_maxsp		= hsp_maxsp * boostmultiplier;
				vsp_jump		= vsp_jump * boostmultiplier;
				berryeaten		= string("yellow");
				if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
			}
			instance_destroy();
		}
	}
	#endregion

	#region Orange Berry Mechanic | Increases Damage For Brother And Increases Jump Amount For Hope For A Period Of Time
	if (instance_exists(player)) {
		if (image_index = 2) && (collected) {
			if (player = obj_boy) {
				with (obj_boy) {
					cooldown		= 10;
					boostmultiplier	= 1.3;
					obj_b_wloadout.ply_dmg = obj_b_wloadout.ply_dmg * boostmultiplier;
					obj_b_wloadout.ani_dmg = obj_b_wloadout.ani_dmg * boostmultiplier;
					obj_b_wloadout.env_dmg = obj_b_wloadout.env_dmg * boostmultiplier;
					berryeaten		= string("orange");
					if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
				}	
			}
			if (player = obj_girl) {
				with (obj_girl) {
					cooldown = 10;
					//Adds Extra Jump
					jumpsmax += 1;
					berryeaten		= string("orange");
					if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
				}
			}
			instance_destroy();
		}
	}
	#endregion

	#region Green Berry Mechanic | Increase Health
	if (instance_exists(player)) {
		if (image_index = 3) && (collected) {
			with (player) {
				if (hp >= max_hp * 0.5) {
					hp = max_hp;
				}
				else { hp += max_hp * 0.5; }
			}
			instance_destroy();
		}
	}
	#endregion

	#region Purple Berry Mechanic | Invincibility For A Period Of Time
	if (instance_exists(player)) {
		if (image_index = 4) && (collected) {
			with (player) {
				cooldown		= 10;
				invincible		= true;
				berryeaten		= string("purple");
				if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
			}
		instance_destroy();
		}
	}
	#endregion

	#region Red Berry Mechanic | Decrease Health and Energy
	if (instance_exists(player)) {
		if (image_index = 5) && (collected) {
			with (player) {
				hp -= max_hp * 0.4;
				energy -= max_energy * 0.4;
				show_ui_hp = true;
			}
			instance_destroy();
		}
	}
	#endregion

	#region White Berry Mechanic | Decrease Damage For Budburra & Decrease Jump Amount For Hope For A Period Of Time
	if (instance_exists(player)) {
		if (image_index = 6) && (collected) {
			if (player = obj_boy) {
				with (obj_boy) {
					cooldown		= 10;
					boostmultiplier	= 1.3;
					obj_b_wloadout.ply_dmg = obj_b_wloadout.ply_dmg * boostmultiplier;
					obj_b_wloadout.ani_dmg = obj_b_wloadout.ani_dmg * boostmultiplier;
					obj_b_wloadout.env_dmg = obj_b_wloadout.env_dmg * boostmultiplier;
					berryeaten		= string("white");
					if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
				}
			}
			if (player = obj_girl) {
				with (obj_girl) {
					cooldown = 10;
					jumpsmax -= 1;
					berryeaten		= string("white");
					if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
				}
			}
			instance_destroy();
		}
	}	
	#endregion

	#region Black Berry Mechanic | Decrease Movement Speed and Jump Height For A Period Of Time
	if (instance_exists(player)) {
		if (image_index = 7) && (collected) {
			with (player) {
				cooldown		= 10;
				boostmultiplier	= 1.3;
				hsp_msp			= hsp_msp / boostmultiplier;
				hsp_maxsp		= hsp_maxsp / boostmultiplier;
				vsp_jump		= vsp_jump / boostmultiplier;
				berryeaten		= string("black");
				if (alarm[0] == -1) { alarm[0] = cooldown * room_speed; }
			}
			instance_destroy();
		}
	}
	#endregion
*/
#endregion