/// @description Initalizing Variables

#region Setting Variables
collected = false;
flashtimer = 0;
ui_y_offset = 25;
image_speed = 0;

scr_shader_flash_int();
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }
#endregion

#region The Berry Types Enums & List String Input
enum berrytype {
	fingerlime		= 0,
	lemonmyrtle		= 1
}

if (berry_type == "randomize") { image_index = irandom_range(0,image_number-1); }
if (berry_type == "finger_lime") { image_index = berrytype.fingerlime; }
if (berry_type == "lemon_myrtle") { image_index = berrytype.lemonmyrtle; }
#endregion