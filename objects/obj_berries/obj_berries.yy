{
    "id": "693671fd-18f9-401d-8463-4aa7a0de1b71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_berries",
    "eventList": [
        {
            "id": "01905320-463c-4c63-b4f5-8927c4374193",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "693671fd-18f9-401d-8463-4aa7a0de1b71"
        },
        {
            "id": "66125691-3b9c-4102-bc6b-7e81c1468e27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "693671fd-18f9-401d-8463-4aa7a0de1b71"
        },
        {
            "id": "fde2d5e7-840f-4819-af11-627ce7f73c6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "693671fd-18f9-401d-8463-4aa7a0de1b71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6cb528f1-21c8-445f-b17d-9993637717e3",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"randomize\"",
                "\"finger_lime\"",
                "\"lemon_myrtle\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"randomize\"",
            "varName": "berry_type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "9fa29ca7-e7f6-40fa-bd41-4646eaefaf03",
    "visible": true
}