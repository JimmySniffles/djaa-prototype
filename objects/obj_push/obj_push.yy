{
    "id": "6b51e4a7-d4a2-4847-aed9-302e0eb2e30a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_push",
    "eventList": [
        {
            "id": "ab1ab378-6ac1-4635-b537-8dde893530de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b51e4a7-d4a2-4847-aed9-302e0eb2e30a"
        },
        {
            "id": "f677881f-6841-4d28-b00f-09295c56f7c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b51e4a7-d4a2-4847-aed9-302e0eb2e30a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e33155-72d3-424f-b670-5fd749d1b34d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2a2fb6cc-3acb-480b-9ee5-247a6194cd64",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "e525fdf5-15ef-40a3-977b-56cb60dc2727",
    "visible": true
}