/// @description Setting Sprite Image Index, Movement, Collision & Visual Effects Variables
if (type == "nw") { image_index = 0; }
if (type == "sw") { image_index = 1; }
image_speed = 0;

if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }

#region Horizontal & Horizontal Carry, Vertical & Vertical Carry (For Moving Platforms) Variables & Fractions. Setting Gravity
hsp = 0;						//Horizontal speed
hsp_frac = 0;					//Horizontal fraction
hsp_carry = 0;					//Horizontal carry on moving platforms

hsp_pushsp = 1.5;				//Horizontal push speed
hsp_fric_ground = 0.50;			//Horizontal friction on the ground
hsp_fric_air = 0.15;			//Horizontal friction in the air

vsp = 0;						//Vertical speed
vsp_max = 12;					//Vertical maximum speed downwards (known as terminal velocity)
vsp_frac = 0;					//Vertical fraction
vsp_carry = 0;					//Vertical carry on moving platforms

grv = 0.3;						//Standard Gravity
my_platform = noone;			//Vertical platform variable. Checks if your on a platform
#endregion

#region Collision Detection Variables
collided = false;				//Checking if collided with a collision wall horizontally
onground = false;				//Returning true or false whether on the ground
onplatform = false;				//Returning true or false whether on a platform
onobject = false;				//Returning true or false whether on a generic object
onwall = 0;						//Setting variable if whether the object is on a wall from the right or the left
landed_check = 0;				//A check if the object has just landed
player_crushed = noone;			//Checking player object instance was crushed under this object
enemy_crushed = noone;			//Checking enemy object instance was crushed under this object
animal_crushed = noone;			//Checking animal object instance was crushed under this object
#endregion

#region Visual Effects Variables
dirt = 0;						//Setting whether or not to put in dirt after period of time touching the wall (used when on a wall)
hearingrange = 700;				//Hearing range for sound and visual effects
scrshk_landing_mag = 2;			//Set screenshake landing magnitude when landing after jump or fall
scrshk_landing_frms = 20;		//Set screenshake landing frames when landing after jump or fall
dirt_landing = 10;				//Set amount of dirt pacticles when landing after jump or fall
#endregion