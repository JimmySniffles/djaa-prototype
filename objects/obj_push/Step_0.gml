/// @description Setting Gravity & Collisions

#region Setting Horizontal, Vertical Movement & Gravity
var hsp_fric_final = hsp_fric_ground;
if (!onground) hsp_fric_final = hsp_fric_air;
hsp = scr_approach(hsp,0,hsp_fric_final);
hsp = clamp(hsp,-hsp_pushsp,hsp_pushsp);

vsp += grv;
vsp = clamp(vsp,-vsp_max,vsp_max);
#endregion

#region Calculate Current Status
//Is my middle center touching the floor at the start of this frame?
onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0); 
onplatform = (place_meeting(x,y+1,par_oneway_platform));
onobject = (place_meeting(x,y+1,par_objects));
var walljpr; walljpr = (scr_infloor(global.tilemap,bbox_right+1,y) >= 0) || place_meeting(x+1,y,par_objects);				
var walljpl; walljpl = (scr_infloor(global.tilemap,bbox_left-1,y) >= 0) || place_meeting(x-1,y,par_objects); 
onwall = (walljpr == 1) - (walljpl == 1);	
#endregion

#region Crushing Being Objects
var final_crushed_object = noone; //Gets the final crushed object
if (instance_exists(player)) { player_crushed = instance_place(x,y,player); if (player_crushed != noone) { final_crushed_object = player_crushed; } }
if (instance_exists(par_enemy)) { enemy_crushed = instance_place(x,y,par_enemy); if (enemy_crushed != noone) { final_crushed_object = enemy_crushed; } }
if (instance_exists(par_animal)) { animal_crushed = instance_place(x,y,par_animal); if (animal_crushed != noone) { final_crushed_object = animal_crushed; } }
if (final_crushed_object != noone) {
	if (final_crushed_object.y >= y) && (!onground) && (!onplatform) && (!onobject) && (landed_check == 1) {
		with (final_crushed_object) { hp = 0; }
	}
}
#endregion

#region Wallside, Groundslide & Landing Visual Effects & Sound

	#region Wall Slide Visual Effects & Sound
	if (!onground) && (!onobject) && (!onplatform)
	{
		if (onwall != 0)
		{
			var side = bbox_left;  //Bboxes are the x and y coordinates of the players hitbox.
			if (onwall == 1) side = bbox_right;
			
			if (distance_to_object(player) <= hearingrange) {
				dirt++;
				if (!audio_is_playing(sn_slide_v1)) && (!audio_is_playing(sn_slide_v2)) && (!audio_is_playing(sn_slide_v3)) && (!audio_is_playing(sn_slide_v4)) {
					audio_play_sound(choose(sn_slide_v1,sn_slide_v2,sn_slide_v3,sn_slide_v4),3,false);
				}
				if ((dirt > 1) && (vsp > 0)) with (instance_create_layer(side,bbox_top,"Effects",obj_dirtslide)) //Adding dirt if its over 2 and vertical speed is greater than zero.
				{
					other.dirt = 0;
					hspeed = -other.onwall*0.5;
				}
			}
		}
		else
		{
			if (distance_to_object(player) <= hearingrange) {
				if (audio_is_playing(sn_slide_v1)) || (audio_is_playing(sn_slide_v2)) || (audio_is_playing(sn_slide_v3)) || (audio_is_playing(sn_slide_v4)) {
					audio_stop_sound(sn_slide_v1) audio_stop_sound(sn_slide_v2) audio_stop_sound(sn_slide_v3) audio_stop_sound(sn_slide_v4);
				}
			}
			dirt = 0;
		}
	}
	#endregion
	
	#region Ground Slide Visual Effects & Sound
	var side, side_2; if (hsp > 0) { side = bbox_left; side_2 = bbox_right; } else { side = bbox_right; side_2 = bbox_left; }
	if (distance_to_object(player) <= hearingrange) {
		if (hsp != 0) { 
			instance_create_layer(side,bbox_bottom,"Effects",obj_dirtslide); 
			with (instance_create_layer(x,bbox_bottom,"Effects",obj_dirtslide)) { vspeed = 0; } 
			with (instance_create_layer(side_2,bbox_bottom,"Effects",obj_dirtslide)) { vspeed = 0; } 
		}
	}
	#endregion

	#region Landing Visual Effects & Sound
	if (!onground) && (!onplatform) && (!onobject) { landed_check = 1; }
	if (onground) || (onplatform) || (onobject) {
		if (landed_check == 1)
		{
			if (distance_to_object(player) <= hearingrange) {
				var landing = audio_play_sound(choose(sn_glanding_v1,sn_glanding_v2,sn_glanding_v3),4,false); 
				audio_sound_pitch(landing, choose(0.8,0.9,1,1.1,1.2));
				scr_screenshake(scrshk_landing_mag, scrshk_landing_frms);
				repeat(dirt_landing) { scr_dirtparticle(x, bbox_bottom, dirtpart.landing); }
			}
			landed_check = 0;
		}
	}
	#endregion

#endregion

//Resetting Fractions & Setting Collisions
scr_fractions(); 
scr_collision();