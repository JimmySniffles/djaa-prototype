//---You can update variables here!---//
reset_dialogue_defaults();
/*
if (room = rm_level_1) {
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Hello? What are you doing here?";
		myNextLine[i]	= 1;
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 0;
		
		//Line 1
		i++;
		myText[i]		= "Just observing nature and then heading off to talk to Djagi.";
		myNextLine[i]	= 2;
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 2
		i++;
		myText[i]		= "Wonderful place this is, isn't it.";
		myNextLine[i]	= 3;
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 3
		i++;
		myText[i]		= "I noticed your father walking across, setting up weaponry to hunt.";
		myNextLine[i]	= 4;
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 4
		i++;
		myText[i]		= "I assume you are about old enough to do the rite of passage, yes?";
		myNextLine[i]	= 5;
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 5
		i++;
		myText[i]		= "Sure am. Looking forward to it but I overslept.";
		myNextLine[i]	= 6;
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 0;
		
		//Line 6
		i++;
		myText[i]		= "That's a shame. I remember when I did mine.";
		myNextLine[i]	= 7;
		mySpeaker[i]	= id;
		myEmotion[i]	= 0;
		
		//Line 7
		i++;
		myText[i]		= "Favourite part was using the boomerang.";
		myNextLine[i]	= 8;
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 8
		i++;
		myText[i]		= "Really? I'm looking forward to that too. Such a awesome weapon.";
		myNextLine[i]	= 9;
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		
		//Line 9
		i++;
		myText[i]		= "Perhaps I can show you some neat tricks with it some day.";
		myNextLine[i]	= 10;
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		
		//Line 10
		i++;
		myText[i]		= "That would be great. My father is waiting though so I better head off.";
		myNextLine[i]	= 11;
		mySpeaker[i]	= obj_boy;
		myEmotion[i]	= 1;
		
		//Line 11
		i++;
		myText[i]		= "By all means. I'm sure we will meet again. Good luck.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;
		myEmotion[i]	= 1;
		myScripts[i]	= [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
		#endregion
		
		#region Consequences For The Dialogue
		case "dialogueplayed":
			var i = 0;
			//Line 0
			myText[i]		= "See you around.";
			mySpeaker[i]	= id;
			myEmotion[i]	= 1;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
		}
#endregion
}