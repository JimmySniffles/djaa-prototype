{
    "id": "0927d6a0-6d3f-4beb-a0ac-99641fcddc12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Wakan",
    "eventList": [
        {
            "id": "9efd1b3d-1e65-4eee-9aa6-4be6cb883631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0927d6a0-6d3f-4beb-a0ac-99641fcddc12"
        },
        {
            "id": "96c1e1b0-5d1a-4ffe-bdd0-e0c6b4be9d34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0927d6a0-6d3f-4beb-a0ac-99641fcddc12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cb49855c-3406-460f-8522-7f365b424945",
    "visible": true
}