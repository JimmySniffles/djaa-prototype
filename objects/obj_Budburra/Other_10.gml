//---You can update variables here!---//
reset_dialogue_defaults();
/*
if (room = rm_level_2b) {
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i] = "Today's been amazing, I saw so many new things! How did you go, Budburra?";
        mySpeaker[i] = obj_girl;
        myEmotion[i] = 1;

         //line 1
         i++;
         myText[i] = "I learned how to properly hunt,";
         mySpeaker[i] = id;
         myEmotion[i] = 1;
		 
		 //line 2
         i++;
         myText[i] = "and to be more respectful of animals and the land.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;

         //line 3
         i++;
         myText[i] = "Shouldn't you know all that already?";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;

         //line 4
         i++;
         myText[i] = "How'd running around go?";
         mySpeaker[i] = id;
         myEmotion[i] = 0;
		 
		 //line 5
         i++;
         myText[i] = "Lot's to learn about putting one foot in front of the other.";
         mySpeaker[i] = id;
         myEmotion[i] = 0;

         //line 6
         i++;
         myText[i] = "You have no idea. We should head back home. I'll race you.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;
		 
		 //line 7
         i++;
         myText[i] = "Before we do. Where's mum?";
         mySpeaker[i] = id;
         myEmotion[i] = 0;
		 
		 //line 8
         i++;
         myText[i] = "She said she will head off on her own.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 0;
		 
		 //line 9
         i++;
         myText[i] = "Back at the festival with the berries that was collected.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;
		 
		 //line 10
         i++;
         myText[i] = "Oh I see. Well we better head back.";
         mySpeaker[i] = id;
         myEmotion[i] = 1;
		 
		 //line 11
         i++;
         myText[i] = "Race you.";
         mySpeaker[i] = obj_girl;
         myEmotion[i] = 1;
		 myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
		break;
	#endregion
	
	#region Consequences For The Dialogue
          case "dialogueplayed":
            var i = 0;
                //Line 0
                myText[i] = "I thought we were racing?";
                mySpeaker[i] = id;
                myEmotion[i] = 1;

                //Line 1
				i++;
                myText[i] = "I was just checking if you started or not.";
                mySpeaker[i] = obj_girl;
                myEmotion[i] = 1;
				
				//Line 2
				i++;
                myText[i] = "Clever.";
                mySpeaker[i] = id;
                myEmotion[i] = 1;
				//uncommenting this will make the first conversation begin again
				//choice_variable	= -1;
				break;
		}
		#endregion
#endregion
}