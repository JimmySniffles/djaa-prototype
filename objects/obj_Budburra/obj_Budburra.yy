{
    "id": "69d01bcf-9d26-4f44-9d11-302665e032ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Budburra",
    "eventList": [
        {
            "id": "7c895ad3-c1aa-4b22-acf0-6e0236fcc483",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69d01bcf-9d26-4f44-9d11-302665e032ed"
        },
        {
            "id": "edf4cebd-9ad7-442b-912b-5140ca3a46b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "69d01bcf-9d26-4f44-9d11-302665e032ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
    "visible": true
}