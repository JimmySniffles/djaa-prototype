//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region The Dialogue
switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Budburra. Where is your sister Hope?";
		mySpeaker[i]	= id;
		
		//Line 1
		i++;
		myText[i]		= ["Not sure, I'll go find her.", "Who cares.","I think shes with mother."];
		myTypes[i]		= 1;
		myNextLine[i]	= [2,3,4];
		myScripts[i]	= [[change_variable, id, "choice_variable", "not sure"], [change_variable, id, "choice_variable", "who cares"], [change_variable, id, "choice_variable", "mother"]];
		mySpeaker[i]	= obj_boy;
		
		//Line 2
		i++;
		myText[i]		= "Thank you sweety. Be safe.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;

		//Line 3
		i++;
		myText[i]		= "I care. Go look for her please.";
		myNextLine[i]	= -1;
		mySpeaker[i]	= id;
		
		//Line 4
		i++;
		myText[i]		= "Oh ok, she should be ok then but to be sure go check if she's alright.";
		mySpeaker[i]	= id;
		break;
	#endregion
	
	#region Consequences For The Dialogue
		case "not sure":
		#region If you chose not sure
			var i = 0;
			//Line 0
			myText[i]		= "Be safe.";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
	
		case "who cares":
		#region If you chose who cares
			var i = 0;
			//Line 0
			myText[i]		= "I don't like your attitude young man but please look for your sister.";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
	
		case "mother":
		#region If you chose mother
			var i = 0;
			//Line 0
			myText[i]		= "Hopefully your sister Hope is doing alright with your mother.";
			mySpeaker[i]	= id;
		
			//uncommenting this will make the first conversation begin again
			//choice_variable	= -1;
		#endregion
		break;
		}
		#endregion
#endregion