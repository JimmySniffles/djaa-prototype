{
    "id": "e68b66e9-870f-4b5f-8fd4-f6a5f06fc3f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Lowanna",
    "eventList": [
        {
            "id": "4b0140e9-e8c7-49dd-8fc6-0c57ad2c4eae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e68b66e9-870f-4b5f-8fd4-f6a5f06fc3f4"
        },
        {
            "id": "6015728f-305f-4701-a655-48a89e06bd4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e68b66e9-870f-4b5f-8fd4-f6a5f06fc3f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "39df7097-9027-4917-8d5d-e8c697c55069",
    "visible": true
}