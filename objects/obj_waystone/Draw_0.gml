/// @description Drawing Self & Collectable Indicator

#region Drawing Self & Collectable UI
draw_self();
if (drawn_text == false) { scr_interactable(x,y-ui_y_offset); }
#endregion

#region Drawing Text Box With Text
if (drawn_text) {
	var halfw = width * 0.5;

	//Draw The Box
	draw_set_color(c_black); // Colour Set To Black
	draw_set_alpha(0.5); // Setting Transparency
	draw_roundrect_ext(x-halfw-border,y-text_box_yoffset-height-(border*2),x+halfw+border,y-text_box_yoffset,15,15,false);
	draw_sprite(spr_text_tut_marker,0,x,y-text_box_yoffset);
	draw_set_alpha(1);

	//Draw text
	scr_drawsettext(c_white,fnt_tut_text, fa_center, fa_top);
	draw_text_transformed(x,y-text_yoffset-height-border,text_current,scale,scale,image_angle);
}
#endregion