/// @description Intializing Variables

#region Intialized Variables
ui_y_offset		= 25;
image_speed		= 0;

if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }

if (type == "nw") { image_index = 0; }
if (type == "sw") { image_index = 1; }
#endregion

#region Text Variables
text_box_yoffset	= 50;						//Sets the offset for the text box to be drawn
text_yoffset		= 50;						//Sets offset amount for the text to be drawn
drawn_text			= false;					//Checks if text is being drawn
detect_radius		= 100;						//Detection radius amount for the text to be drawn
spd					= 0.3;						//Speed of the text drawn.
letters				= 0;						//Determine how many letters should be currently drawing to the screen.
text				= "";						//The string of the text that will be drawn.
length				= string_length(text);		//Returns how long the text being drawn is in characters. Includes spaces.
text_current		= "";						//This gets the sum of text depending upon how many letters are being drawn currently. So if letters equals 4 for example than that will get 'Stop' (4 characters).
width				= 0;						//Width of the text box.
height				= 0;						//Height of the text box.
border				= 10;						//Border is the buffer between the text being drawn and the background text box.
scale				= 0.5;						//Sets scale for text to be drawn
#endregion

#region Setting Text Depending Upon Selection Of text_display_type

	#region Jump Tutorial Text
	if (text_display_type == "jump_tut") {
		//if (global.computer_use) { text = "Press Space to Jump.\n Hold to jump higher."; }
		if (global.computer_use) { text = "Tap bottom of screen to Jump."; }
		if (global.mobile_use) { text = "Tap bottom of screen to Jump."; }
	}
	#endregion
	
	#region Interact Tutorial Text
	if (text_display_type == "interact_tut") {
		//if (global.computer_use) { text = "Press E or Left Mouse Button\n next to people to interact with them."; }
		if (global.computer_use) { text = "Double tap on people to talk to them."; }
		if (global.mobile_use) { text = "Double tap on people to talk to them."; }
	}
	#endregion

	#region Pick Up Tutorial Text
	if (text_display_type == "pickup_tut") {
		//if (global.computer_use) { text = "Plants can help you.\n Press E or Left Mouse Button to pick them up."; }
		if (global.computer_use) { text = "Some plants can help you.\n Only eat plants you know are safe."; }
		if (global.mobile_use) { text = "Some plants can help you.\n Only eat plants you know are safe."; }
	}
	#endregion
	
	#region Lemon Myrtle Tutorial Text
	if (text_display_type == "lemonmyrtle_tut") {
		//if (global.computer_use) { text = "Plants can help you.\n Press E or Left Mouse Button to pick them up."; }
		if (global.computer_use) { text = "Lemon myrtle protects you for a short time.\n Tap to eat it."; }
		if (global.mobile_use) { text = "Lemon myrtle protects you for a short time.\n Tap to eat it."; }
	}
	#endregion
	
	#region Finger Lime Tutorial Text
	if (text_display_type == "fingerlime_tut") {
		//if (global.computer_use) { text = "Plants can help you.\n Press E or Left Mouse Button to pick them up."; }
		if (global.computer_use) { text = "Finger limes increase your health.\n Tap to eat it."; }
		if (global.mobile_use) { text = "Finger limes increase your health.\n Tap to eat it."; }
	}
	#endregion
	
	#region Dodge & Slide Tutorial Text
	if (text_display_type == "dge_sld_tut") {
		if (global.computer_use) { text = "You can dodge or slide\n by pressing W or S key\n while running."; }
		if (global.mobile_use) { text = "You can dodge or slide\n by swiping up or down\n while running."; }
	}
	#endregion

	#region Wall Jump Tutorial Text
	if (text_display_type == "walljump_tut") {
		//if (global.computer_use) { text = "You can wall jump by jumping on wall."; }
		if (global.computer_use) { text = "You can jump from wall to wall."; }
		if (global.mobile_use) { text = "You can jump from wall to wall."; }	
	}
	#endregion

	#region Climbing Tutorial Text
	if (text_display_type == "climbing_tut") {
		if (global.computer_use) { text = "Climb on vines by pressing W or E key on a vine.\n You can also jump off them."; }
		if (global.mobile_use) { text = "Climb on a vine by touching it.\n You can also jump off them."; }	
	}
	#endregion
	
	#region Pushing Object Tutorial Text
	if (text_display_type == "pushobject_tut") {
		if (global.computer_use) { text = "You can move certain objects\n by moving into them.\n You can use it to crush your foes."; }
		if (global.mobile_use) { text = "You can move certain objects\n by moving into them.\n You can use it to crush your foes."; }	
	}
	#endregion

#endregion