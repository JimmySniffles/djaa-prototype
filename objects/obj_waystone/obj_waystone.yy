{
    "id": "851dc90e-7fc5-41ae-ad02-a6ef6ddb73a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_waystone",
    "eventList": [
        {
            "id": "398825af-70ba-4491-94d1-9f3085ca1010",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "851dc90e-7fc5-41ae-ad02-a6ef6ddb73a9"
        },
        {
            "id": "78202907-d6cb-4470-879d-d8e2ff8d92d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "851dc90e-7fc5-41ae-ad02-a6ef6ddb73a9"
        },
        {
            "id": "7a358e58-c1f9-453c-9d2d-e2f25cc66c54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "851dc90e-7fc5-41ae-ad02-a6ef6ddb73a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "7889e06d-4d10-44dd-8fb5-b4a3ba1582b1",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"nw\"",
                "\"sw\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"nw\"",
            "varName": "type",
            "varType": 6
        },
        {
            "id": "d26ff35d-594f-48b1-a4ad-7ae7c4b521df",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"jump_tut\"",
                "\"pickup_tut\"",
                "\"interact_tut\"",
                "\"dge_sld_tut\"",
                "\"walljump_tut\"",
                "\"climbing_tut\"",
                "\"pushobject_tut\"",
                "\"fingerlime_tut\"",
                "\"lemonmyrtle_tut\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"jump_tut\"",
            "varName": "text_display_type",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "2e7605ea-de3a-4a37-a8aa-4c8378c851a8",
    "visible": true
}