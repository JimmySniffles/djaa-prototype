/// @description Text/ Dialogue Display Upon Interaction With Player
//Use create_textevent if you wish to use the dialogue system to display text

#region Creating & Progressing Text
//Create Letters Upon Close Distance
if (instance_exists(player)) {
	if (point_in_circle(player.x,player.y,x,y,detect_radius)) {
		drawn_text = true;
		if (drawn_text) {
			letters += spd;
			text_current = string_copy(text,1,floor(letters));

			draw_set_font(fnt_tut_text);
			if (height == 0) { height = string_height(text) * scale; }
			width = string_width(text_current) * scale;
		}
		//with (obj_camera) { follow = other.id; }
	} else { drawn_text = false; /*with (obj_camera) { follow = other.player; } */ } 
}
#endregion