/// @description Sets Fade Of Notifications To False
//Can call this event whenever a new notification is added.
fade_away	= false;
noti_alpha	= 1;
alarm[0]	= room_speed * seconds;	