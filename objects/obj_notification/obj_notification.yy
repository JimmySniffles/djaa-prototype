{
    "id": "5c1971a8-7336-4550-8643-cf06a8b09969",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_notification",
    "eventList": [
        {
            "id": "9de288ac-e0d6-4545-845a-1c1175b4b70a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c1971a8-7336-4550-8643-cf06a8b09969"
        },
        {
            "id": "9fd5086a-84b9-4e6f-9375-591be975224d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c1971a8-7336-4550-8643-cf06a8b09969"
        },
        {
            "id": "c8d364ad-edbe-4442-944b-74e97809fe0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5c1971a8-7336-4550-8643-cf06a8b09969"
        },
        {
            "id": "f5409949-5ac0-41ae-b078-f20a3ec17955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5c1971a8-7336-4550-8643-cf06a8b09969"
        },
        {
            "id": "8bc5a3a2-f6b0-49bb-bd81-58c3bc96a3e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5c1971a8-7336-4550-8643-cf06a8b09969"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}