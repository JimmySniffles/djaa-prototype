/// @description Setting Variables

#region Notification Variables
gui_height			= display_get_gui_height();		//The height of the display (returns number, example being 1080).
gui_width			= display_get_gui_width();		//The width of the display (returns number, example being 1920).
fade_away			= false;						//Sets fade away to false. Change this to true when the alarm goes off.
noti_alpha			= 1;							//Setting the notification alpha. Noti being short for notification.
noti_alpha_spd		= 0.1;							//Sets speed for alpha change.
noti_font			= fnt_dialogue;					//Setting font for notifications.
seconds				= 1.5;							//Setting the seconds when the alarm will trigger.
alarm[0]			= room_speed * seconds;			//The alarm is going to want the steps not the seconds. Using room_speed (currently set at 60, 60 frames is 1 second) then multiplying it the amount of seconds you want it to trigger.

//If they are no obj_notification in the world or pickup a tomato after awhile its going to create this notification object, data structure and then slot in the picked up item.
//The creation of the object and data structure will be done externally and not from itself.
ds_notifications	= -1;
noti_text_x			= 100;							//The x position of notification text display. Text is anchored top left for where its drawn.
noti_text_y			= gui_height * 0.5;				//The y position of notification text display. Currently in the middle of the display screen.
str_height			= string_height("M");			//Gets height of the letter string from a set font in pixels. Capital used to get the maximum height the string could be.
#endregion

#region To use sprites for notifications than text
/*
You could have an additional line in the "info" grid that stores the sprite_index of the picture (or perhaps the image_index, if you want to put all your "icons" into one sprite). 
Then, instead of pulling out the string from the grid, pull out the information about the sprite/image_index and use draw_sprite. Hope that makes sense!﻿
*/
#endregion