/// @description Drawing Notifications

//Variable for notification grid
var noti_grid = ds_notifications;
//The height of the grid is the amount of times we got to loop through the grid. How many notifications to draw.
var grid_height = ds_grid_height(noti_grid);

//Colour of text
var c = c_white;

//Drawing Text Font Every Frame
draw_set_font(noti_font);

//Loop to display notifications
var yy = 0; repeat(grid_height) {
	//Display + beside a gain item notification
	var noti_sign = ""; //By default, it is made as an empty string. Only change this when we find it to be true.
	if (noti_grid[# 0, yy] > 0) { noti_sign = "+"; } //If a positive number change to a plus.
		
	draw_text_color(
	//You can play with this line to get all kinds of different notifications displayed.
		noti_text_x, 
		noti_text_y + (yy * str_height) - (grid_height * str_height),
		noti_sign + string(noti_grid[# 0, yy]) + " " + string(noti_grid[# 1, yy]),
		c, c, c, c, noti_alpha
	);
	yy++
}

#region Fade Away
if (fade_away) { noti_alpha -= noti_alpha_spd; }
if (noti_alpha <= 0) { instance_destroy(); }
#endregion