/// @description GUI/Vars/Menu Setup

#region Saving Screenshot & Deactivating Objects Based Upon Layer During Gameplay
if (!file_exists("Screenshot.png")) { screen_save("Screenshot.png"); }

instance_deactivate_layer("Effects");
instance_deactivate_layer("Platforms");
instance_deactivate_layer("Destructibles");
instance_deactivate_layer("Projectiles");
instance_deactivate_layer("Player");
instance_deactivate_layer("NPCs");
instance_deactivate_layer("Enemies");
instance_deactivate_layer("Animals");
instance_deactivate_layer("Interactive");
#endregion

#region Menu Variables
disp_width				= display_get_width();						//The width of the display.
disp_height				= display_get_height();						//The height of the display.
gui_width				= display_get_gui_width();					//The width of the gui display which is 1920.
gui_height				= display_get_gui_height();					//The height of the gui display which is 1080.

menu_itemwidth			= font_get_size(fnt_menu);					//The menu font size.
menu_x_buffer			= gui_width * 0.1;							//Buffer amount for the menu text options to begin drawing horizontally from left of the screen.
menu_x_buffer_scale		= 10;										//Scale gab for each menu item that is then multiplied by menu_itemwidth.
controls_x_buffer_scale	= 5.2;										//Scale gab for each menu item for the controls menu page that is then multiplied by menu_itemwidth.
menu_y_buffer			= 120;										//Buffer amount of where the menu is drawn vertically from the bottom of the screen.

start_menu_x			= gui_width - menu_x_buffer;				//The X (horizontal) position for the menu. Adding to this will animate the text from this position to the target position from right to the left.
start_menu_y			= gui_height + 150;							//The Y (Vertical) position for the menu (default is 1080). Adding to this will animate the text from this position to the target position from bottom to top.

menu_y_target			= gui_height - menu_y_buffer;				//Where the menu position will be. The target menu y position.
menu_speed				= 25;										//Movement speed of the animation. Lower is faster.
menu_commited			= -1;										//Set whether or not the menu option is selected.
menu_control			= true;										//Set whether or not the player has control of the menu.
inputting				= false;									//Checks whether or not the player is inputting/changing a value in the settings.
escape_pause_menu		= false;									//Checks whether or not the player has decided to escape the pause menu via button press. This needs to be separate from the script_execute unless a better alternative is found.

alpha					= 0;										//Current transparency (alpha), a fading effect for the screen or whatever else.
alphamax				= 0.05;										//Setting alpha max for the alpha to go towards.
fadeout					= false;									//Variable to check whether fadeout is ready (true) or not (false).
fadingin_inc			= 0.005;									//Sets the fading in amount per frame.
fadingout_inc			= 0.005;									//Sets the fading out amount per frame.
max_fade_in				= 0;										//Sets the maximum fade in value for the alpha value.
max_fade_out			= 1;										//Sets the maximum fade out value for the alpha value.

background = sprite_add("Screenshot.png",0,false,true,disp_width,disp_height);	//Setting background variable to be the saved screenshot file as a sprite. Setting the dimensions to the size of the window display.
#endregion

#region Creating enumerators (Enumerators are a class of variables) Note: To add more entrys add it before the height variable.
//The pause menu pages. This needs to be separate from the menu_page enum otherwise it causes the changing of pages to be off.
enum pause_menu_page {
	main,
	settings,
	audio,
	graphics,
	controls,
	height
}
#endregion

#region Creating Menu Pages & Menu Option Selection
ds_menu_main = scr_create_menu_page(
	["Exit",				menu_element_type.script_runner,	scr_game_exit],
	["Restart",				menu_element_type.script_runner,	scr_restart],
	["Settings",			menu_element_type.page_transfer,	pause_menu_page.settings],
	["Continue",			menu_element_type.script_runner,	scr_continue_game]
);

ds_settings = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	pause_menu_page.main],
	["Controls",			menu_element_type.page_transfer,	pause_menu_page.controls],
	["Graphics",			menu_element_type.page_transfer,	pause_menu_page.graphics],
	["Audio",				menu_element_type.page_transfer,	pause_menu_page.audio]
);

ds_menu_audio = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	pause_menu_page.settings],
	["Voice\nOver",			menu_element_type.slider,			scr_change_volume,			global.voice_over_vol,			[0,1]],
	["Sound\nEffects",		menu_element_type.slider,			scr_change_volume,			global.sound_effects_vol,		[0,1]],
	["Background\nMusic",	menu_element_type.slider,			scr_change_volume,			global.background_music_vol,	[0,1]]
);

ds_menu_graphics = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	pause_menu_page.settings],
	["Window\nMode",		menu_element_type.toggle,			scr_change_window_mode,		global.windowsmode,				["Window","Fullscreen"]]
);

ds_menu_controls = scr_create_menu_page(
	["Back",				menu_element_type.page_transfer,	pause_menu_page.settings],
	["Interact",			menu_element_type.input,			"key_interact",				ord("E")],
	["Special\nAttack",		menu_element_type.input,			"key_spattack",				mb_right],
	["Attack",				menu_element_type.input,			"key_attack",				mb_left],
	["Sprint",				menu_element_type.input,			"key_sprint",				vk_shift],
	["Up",					menu_element_type.input,			"key_up",					ord("W")],
	["Down",				menu_element_type.input,			"key_down",					ord("S")],
	["Jump",				menu_element_type.input,			"key_jump",					vk_space],
	["Right",				menu_element_type.input,			"key_right",				ord("D")],
	["Left",				menu_element_type.input,			"key_left",					ord("A")]
);

//Setting up the pages to transfer between.
page = 0;
menu_pages = [ds_menu_main, ds_settings, ds_menu_audio, ds_menu_graphics, ds_menu_controls]

//Setting up the current options within the pages.
var i = 0, array_len = array_length_1d(menu_pages);
repeat (array_len) {
	menu_option[i] = 0;
	i++;
}

//Setting up old menu option for playing sound effect when it no longer equals the current menu option for the mouse control.
old_menu_option = menu_option[page];

//Setting menu option default.
menu_option[page] = 3;
#endregion