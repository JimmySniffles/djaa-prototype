{
    "id": "9670e8a8-078b-4e2a-9910-f34c18091ad3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause_menu",
    "eventList": [
        {
            "id": "a29c2b67-3683-442c-8379-0b0d515bdb35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9670e8a8-078b-4e2a-9910-f34c18091ad3"
        },
        {
            "id": "2960feb1-3e7d-4df9-bda7-38df22a64cd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9670e8a8-078b-4e2a-9910-f34c18091ad3"
        },
        {
            "id": "f17371d1-cba3-4068-91b3-85ae4f334633",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9670e8a8-078b-4e2a-9910-f34c18091ad3"
        },
        {
            "id": "bbbdc23e-186c-4d52-a20d-f58ade1f0836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9670e8a8-078b-4e2a-9910-f34c18091ad3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}