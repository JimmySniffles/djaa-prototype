/// @description Draw Menu & Background

#region Drawing Screenshot Background & Slightly Transparent Black Background
draw_sprite(background,0,disp_width,disp_height);
draw_sprite_ext(spr_bg_gui,0,0,0,disp_width,disp_height,0,c_black,alpha);
#endregion

#region Intializing Variables. Getting Access To The Grid & Its Height.
//ds_grid is used to access what page were on in the menu_pages array whether its the settings page or the main page or the audio page etc.
//ds_height gets the height of the grid because that gives us the number of elements or options on a single page.
//So for the main page for example you got continue game (1), new game (2), settings (3), credits (4) and exit (5).
var ds_grid	= menu_pages[page], ds_height = ds_grid_height(ds_grid);

var final_menu_x_buffer_scale; //Setting the final_menu_x_buffer_scale.
if (page != ds_menu_controls) { final_menu_x_buffer_scale = menu_x_buffer_scale; }
else {							final_menu_x_buffer_scale = controls_x_buffer_scale; }
#endregion

#region Bottom Section: Draw Text & Overlay Select Sprite
//Setting drawn elements alignment
draw_set_halign(fa_right);
draw_set_valign(fa_middle);

//btx is bottom text x position and bty is bottom text y position.
//c is the colour of the menu text. yo is the y offset for the selected menu option. alpha_ is the alpha of the drawn elements.
var btx, bty = start_menu_y, c, yo, alpha_;
//xx is used to loop through the menu options to place the menu options across the x axis. So yy would be used if the menu options go downwards along the y axis.
var xx = 0; repeat (ds_height) {
	//This adds each option of the menu multiplied by whatever the buffer amount is which is now the menu_itemwidth * final_menu_x_buffer_scale from the start_menu_x position.
	btx = start_menu_x - (xx*(menu_itemwidth * final_menu_x_buffer_scale));
	
	//Settings the numbers for the alpha, text colour and y offset for the text and setting font.
	alpha_ = 0.5; c = c_white; yo = 0; draw_set_font(fnt_menu);
		
	//If a menu option is selected, then you can change the colour, set font, add a sprite, set the y offset and set alpha_.
	if (xx == menu_option[page]) {
		alpha_ = 1; c = c_white; yo = 25; draw_set_font(fnt_menuselected);
		draw_sprite_ext(spr_menuoverlay,0,btx+35,gui_height,1,1,0,c_white,1);
	}
	//Draws the text to the screen based upon x and y positions, strings for the text options using ds_grid variable to access the grid, colours and alpha.
	//To access just the text the ds_grid variable uses # (accessor) 0 because 0 is the first entry which has all the strings for the text for the menu.
	draw_text_color(btx, bty - yo, ds_grid[# 0, xx], c,c,c,c, alpha_);
	xx++;	//Adding each menu option only to the height of the grid.
}
#endregion

#region Upper Section: Draw Menu Elements
//utx is upper text x position and uty is upper text y position.
var utx, uty = start_menu_y - menu_y_buffer; draw_set_font(fnt_menu);		//Resetting font to menu font after setting the font to selected font.
xx = 0; repeat (ds_height) {
	utx = start_menu_x - (xx*(menu_itemwidth * final_menu_x_buffer_scale));
	
	switch (ds_grid[# 1, xx]) {	
		#region Slider Menu Element Type
		case menu_element_type.slider:
			c = c_white;
			var length = 200;						//Length of the slider
			var current_val = ds_grid[# 3, xx];		//Gets current value from grid.
			var current_array = ds_grid[# 4, xx];	//Gets the array from the grid.
			var circle_pos = ((current_val - current_array[0]) / (current_array[1] - current_array[0])); //Gets circle position percentage based upon calculation.

			draw_set_alpha(0.5);
			draw_roundrect_colour((utx-50) - 10, uty, (utx-50) + 10, uty - length, c_black, c_black, false);
			draw_set_alpha(1);
			draw_roundrect_colour((utx-50) - 10, uty, (utx-50) + 10, lerp(uty,uty - length, (current_val)), c, c, false);
			if (inputting && xx == menu_option[page]) { c = c_yellow; }
			draw_circle_color((utx-50), uty - (circle_pos * length), 16, c_black,c_black, false);
			draw_circle_color((utx-50), uty - (circle_pos * length), 15, c,c, false);
			draw_text_color(utx, uty - (length*1.2), string(floor(circle_pos*100))+"%", c,c,c,c, 1);
		break;
		#endregion
			
		#region Toggle Menu Element Type
		case menu_element_type.toggle:
			c = c_white;
			var current_val = ds_grid[# 3, xx];
			var c1, c2;
			if (inputting && xx == menu_option[page]) { c = c_yellow; }
			
			if (current_val == 0) { c1 = c; c2 = c_dkgray; } else { c1 = c_dkgray; c2 = c; }
			
			draw_text_color(utx, uty - 50, "Fullscreen", c2,c2,c2,c2, 1);
			draw_text_color(utx, uty - 110, "Windows", c1,c1,c1,c1, 1);
		break;
		#endregion
			
		#region Input Menu Element Type
		case menu_element_type.input:
			var current_val = ds_grid[# 3, xx];
			switch (current_val) {
				#region Drawing Possible and Current Input
				case mb_right:		current_val = "Right\nMouse";	break;
				case mb_middle:		current_val = "Middle\nMouse";	break;
				case mb_left:		current_val = "Left\nMouse";	break;
				case vk_shift:		current_val = "Shift";			break;
				case vk_space:		current_val = "Space";			break;
				case vk_tab:		current_val = "Tab";			break;
				case vk_backspace:	current_val = "Back\nSpace";	break;
				case vk_up:			current_val = "Up";				break;
				case vk_right:		current_val = "Right";			break;
				case vk_down:		current_val = "Down";			break;
				case vk_left:		current_val = "Left";			break;
				default:			current_val = chr(current_val);			//This is for the letter character inputs
				#endregion
			}
			c = c_white;
			if (inputting && xx == menu_option[page]) { c = c_yellow; }
			draw_text_color(utx, uty, current_val, c,c,c,c, 1);
		break;
		#endregion
	}	
	xx++;
}

//Resetting back to default
draw_set_halign(fa_left);		
draw_set_valign(fa_top);
#endregion