/// @description Destroys data structures, screenshot and reactivates everything
var i = 0, array_len = array_length_1d(menu_pages);
repeat (array_len) {
	ds_grid_destroy(menu_pages[i]);
	i++;
}
if (file_exists("Screenshot.png")) { file_delete("Screenshot.png"); }
instance_activate_all();