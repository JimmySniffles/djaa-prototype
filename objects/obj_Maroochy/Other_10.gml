//---You can update variables here!---//
reset_dialogue_defaults();
/*
#region Level 3 Dialogue
if (room = rm_level_3) {
	#region The Dialogue
	switch(choice_variable) {
		case -1:
		#region First Dialogue
			//Line 0
			var i = 0;
	          myText[i] = "Hello? I'm looking for Maroochy. Lowanna told me to find her.";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;
			  
			  //line 1
	          i++;
	          myText[i] = "I need her help";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;

	          //line 2
	          i++;
	          myText[i] = "What help do you need, Hope? You've walked far, so it must be dire.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 3
	          i++;
	          myText[i] = "Are you Maroochy?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;

	          //line 4
	          i++;
	          myText[i] = "Yes. I've lived here for a long time. Most know of me from the river.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 5
	          i++;
	          myText[i] = "I haven't found the river, yet. It’s still a fair walk, isn't it?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;

	          //line 6
	          i++;
	          myText[i] = "This lake isn't joined with it but the river isn't far.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;
		  
			  //line 7
	          i++;
	          myText[i] = "You said you need help?";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 8
	          i++;
	          myText[i] = "Why would the Elders send me to you for help?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;

	          //line 9
	          i++;
	          myText[i] = "I have been here for many many years.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;
		  
			  //line 10
	          i++;
	          myText[i] = "Their Elders, and theirs before them came to me for help.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 11
	          i++;
	          myText[i] = "I'd like to help you, too.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 1;

	          //line 12
	          i++;
	          myText[i] = "Who is Wakan?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;

	          //line 13
	          i++;
	          myText[i] = "I do not know this Wakan.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;
		  
			  //line 14
	          i++;
	          myText[i] = "If you tell me more about him, I might be able to help.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 15
	          i++;
	          myText[i] = "Did he do something, and it's with what he's done that you need help?";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 16
	          i++;
	          myText[i] = "He attacked my home. The Elders said that his followers killed my parents.";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;

	          //line 17
	          i++;
	          myText[i] = "They said he took my little sister, Gira.";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;

	          //line 18
	          i++;
	          myText[i] = "That is awful, I am so sorry that this happened to your family.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 19
	          i++;
	          myText[i] = "He took your sister? I wonder why he would do that?";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 20
	          i++;
	          myText[i] = "I don't know. Do you know anyone who might want to take her?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;
		  
			  //line 21
	          i++;
	          myText[i] = "She's only a little girl.";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 2;

	          //line 22
	          i++;
	          myText[i] = "The only person I've ever known to be so awful is . .";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;
		  
			  //line 23
	          i++;
	          myText[i] = "he is trapped in his mountain.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 24
	          i++;
	          myText[i] = "The Elders long ago sealed him away. He cannot be freed. Unless.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 25
	          i++;
	          myText[i] = "Does Gira have powers?";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;

	          //line 26
	          i++;
	          myText[i] = "I . . don't understand. Powers?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;
		  
			  //line 27
	          i++;
	          myText[i] = "If Gira has powers, he might use them to try free Ninderry.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;


	          //line 28
	          i++;
	          myText[i] = "Who is Ninderry? Why would Wakan want to free him?";
	          mySpeaker[i] = obj_girl;
	          myEmotion[i] = 0;

	          //line 29
	          i++;
	          myText[i] = "I do not know why, but we cannot let it happen. We must get to Coolum.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 0;
		  
			  //line 30
	          i++;
	          myText[i] = "Head forth, I will meet you with Coolum when ready.";
	          mySpeaker[i] = id;
	          myEmotion[i] = 1;
			  myScripts[i] = [change_variable, id, "choice_variable", "dialogueplayed"];
			break;
		#endregion
	
		#region Consequences For The Dialogue
			case "dialogueplayed":
			#region If you chose alright
				var i = 0;
				//Line 0
				myText[i]		= "Go ahead Hope, we will meet again.";
				mySpeaker[i]	= id;
				myEmotion[i]	= 1;
		
				//uncommenting this will make the first conversation begin again
				//choice_variable	= -1;
			break;
			#endregion
			}
	#endregion
	}
	#endregion
#endregion