{
    "id": "509d685c-e438-485c-ab1e-753c5a0259dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Maroochy",
    "eventList": [
        {
            "id": "c842f1db-527a-4ca6-a902-3ff6c6fb60e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "509d685c-e438-485c-ab1e-753c5a0259dd"
        },
        {
            "id": "1a060bba-af13-419d-bcda-ba996d60cd8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "509d685c-e438-485c-ab1e-753c5a0259dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6fb0bb24-058d-4379-bc01-aa476d4b6c68",
    "visible": true
}