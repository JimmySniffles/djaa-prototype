/// @description Changing weapons

#region Changing Held Weapons
if (instance_exists(obj_boy)) { 
	if (obj_boy.state != states.attack_stg1) && (obj_boy.state != states.attack_stg2) && (obj_boy.state != states.attack_stg3) {
	//Mobile
	//if (ammo[wep_type.club] = -1) && (scr_sprite_button_detect(club_button_x,club_button_y,spr_club_button)) { obj_boy.weaponswitch = true scr_changeweapon(wep_type.club); }
	
	//Computer
	var mousewhlup    = mouse_wheel_up();	if (mousewhlup)	{ obj_boy.weaponswitch = true	scr_changeweapon(clamp(weapon - 1,0,weaponamount)); }
	var mousewhldown  = mouse_wheel_down();	if (mousewhldown) { obj_boy.weaponswitch = true	scr_changeweapon(clamp(weapon + 1,0,weaponamount)); }

	if (keyboard_check_pressed(ord("5")))				  { scr_changeweapon(wep_type.unarmed); }
	if (keyboard_check_pressed(ord("1")) && ammo[1] = -1) { obj_boy.weaponswitch = true scr_changeweapon(wep_type.club); }
	if (keyboard_check_pressed(ord("2")) && ammo[2] = -1) { obj_boy.weaponswitch = true scr_changeweapon(wep_type.stunboomerang); }
	if (keyboard_check_pressed(ord("3")) && ammo[3] = -1) { obj_boy.weaponswitch = true scr_changeweapon(wep_type.spear); }
	if (keyboard_check_pressed(ord("4")) && ammo[4] = -1) { obj_boy.weaponswitch = true scr_changeweapon(wep_type.killboomerang); }
	}
}
#endregion