/// @description Defining Different Types of Weapons

#region Getting Display GUI Amount & Setting UI Buttons X & Y Positions
gui_width		= display_get_gui_width();					//The width of the display
gui_height		= display_get_gui_height();					//The height of the display

//Weapon Button Positions
button_x_offset	= 30;
button_y_offset	= 50;
club_button_x	= gui_width - button_x_offset;				//The x position of the club
club_button_y	= button_y_offset;							//The y position of the club
#endregion

#region Setting Weapon Types
enum wep_type {
	unarmed			= 0,
	club			= 1,
	stunboomerang	= 2,
	spear			= 3,
	killboomerang	= 4,
	height	
}
#endregion

#region Weapon Types

	#region Unarmed
	weapons[wep_type.unarmed] = ds_map_create();
	ds_map_add(weapons[wep_type.unarmed], "ene_dmg",0);								//The damage of the weapon to enemies
	ds_map_add(weapons[wep_type.unarmed], "ani_dmg",0);								//The damage of the weapon to animals
	ds_map_add(weapons[wep_type.unarmed], "env_dmg",0);								//The damage of the weapon to environment objects

	ds_map_add(weapons[wep_type.unarmed], "attack_spd",0);							//The attack speed
	ds_map_add(weapons[wep_type.unarmed], "attack_energy",0);						//The amount of energy used for the attack
	ds_map_add(weapons[wep_type.unarmed], "stun",false);							//States whether or not the attack can stun
	ds_map_add(weapons[wep_type.unarmed], "knockback",false);						//States whether or not the attack can knockback
	ds_map_add(weapons[wep_type.unarmed], "knockback_str",0);						//The amount of strength of the knockback
		
	ds_map_add(weapons[wep_type.unarmed], "scrshk_mag",0);							//The amount of screenshake magnitude
	ds_map_add(weapons[wep_type.unarmed], "scrshk_frm",0);							//The amount of screenshake frames
	ds_map_add(weapons[wep_type.unarmed], "ene_particles",0);						//The amount of particles on attack impact with enemies
	ds_map_add(weapons[wep_type.unarmed], "ani_particles",0);						//The amount of particles on attack impact with animal
	ds_map_add(weapons[wep_type.unarmed], "env_particles",0);						//The amount of particles on attack impact with environment
		
	ds_map_add(weapons[wep_type.unarmed], "projectile",-1);							//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(weapons[wep_type.unarmed], "proj_spd",0);							//The speed of the projectile.
	ds_map_add(weapons[wep_type.unarmed], "proj_dist",0);							//The max distance of the projectile.
	//ds_map_add(weapons[wep_type.unarmed], "length",0);							//Length of the sprite from origin to the far right. So it creates a bullet not at the center of the player but offset by a given amount.
	#endregion

	#region Club
	weapons[wep_type.club] = ds_map_create();
	ds_map_add(weapons[wep_type.club], "ene_dmg",20);								//The damage of the weapon to enemies
	ds_map_add(weapons[wep_type.club], "ani_dmg",25);								//The damage of the weapon to animals
	ds_map_add(weapons[wep_type.club], "env_dmg",35);								//The damage of the weapon to environment objects

	ds_map_add(weapons[wep_type.club], "attack_spd",1);								//The attack speed
	ds_map_add(weapons[wep_type.club], "attack_energy",10);							//The amount of energy used for the attack
	ds_map_add(weapons[wep_type.club], "stun",false);								//States whether or not the attack can stun
	ds_map_add(weapons[wep_type.club], "knockback",true);							//States whether or not the attack can knockback
	ds_map_add(weapons[wep_type.club], "knockback_str",15);							//The amount of strength of the knockback
		
	ds_map_add(weapons[wep_type.club], "scrshk_mag",2);								//The amount of screenshake magnitude
	ds_map_add(weapons[wep_type.club], "scrshk_frm",15);							//The amount of screenshake frames
	ds_map_add(weapons[wep_type.club], "ene_particles",2);							//The amount of particles on attack impact with enemies
	ds_map_add(weapons[wep_type.club], "ani_particles",3);							//The amount of particles on attack impact with animal
	ds_map_add(weapons[wep_type.club], "env_particles",4);							//The amount of particles on attack impact with environment

	ds_map_add(weapons[wep_type.club], "projectile",-1);							//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(weapons[wep_type.club], "proj_spd",0);								//The speed of the projectile.
	ds_map_add(weapons[wep_type.club], "proj_dist",0);								//The max distance of the projectile.
	//ds_map_add(weapons[wep_type.unarmed], "length",0);							//Length of the sprite from origin to the far right. So it creates a bullet not at the center of the player but offset by a given amount.		
	#endregion

	#region Stun Boomerang
	weapons[wep_type.stunboomerang] = ds_map_create();
	ds_map_add(weapons[wep_type.stunboomerang], "ene_dmg",15);						//The damage of the weapon to enemies
	ds_map_add(weapons[wep_type.stunboomerang], "ani_dmg",15);						//The damage of the weapon to animals
	ds_map_add(weapons[wep_type.stunboomerang], "env_dmg",10);						//The damage of the weapon to environment objects

	ds_map_add(weapons[wep_type.stunboomerang], "attack_spd",1);					//The attack speed
	ds_map_add(weapons[wep_type.stunboomerang], "attack_energy",10);				//The amount of energy used for the attack
	ds_map_add(weapons[wep_type.stunboomerang], "stun",true);						//States whether or not the attack can stun
	ds_map_add(weapons[wep_type.stunboomerang], "knockback",false);					//States whether or not the attack can knockback
	ds_map_add(weapons[wep_type.stunboomerang], "knockback_str",0);					//The amount of strength of the knockback
		
	ds_map_add(weapons[wep_type.stunboomerang], "scrshk_mag",1);					//The amount of screenshake magnitude
	ds_map_add(weapons[wep_type.stunboomerang], "scrshk_frm",10);					//The amount of screenshake frames
	ds_map_add(weapons[wep_type.stunboomerang], "ene_particles",1);					//The amount of particles on attack impact
	ds_map_add(weapons[wep_type.stunboomerang], "ani_particles",1);					//The amount of particles on attack impact with animal
	ds_map_add(weapons[wep_type.stunboomerang], "env_particles",1);					//The amount of particles on attack impact with environment
		
	ds_map_add(weapons[wep_type.stunboomerang], "projectile",obj_stunboomerang);	//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(weapons[wep_type.stunboomerang], "proj_spd",15);						//The speed of the projectile.
	ds_map_add(weapons[wep_type.stunboomerang], "proj_dist",600);					//The max distance of the projectile.
	//ds_map_add(weapons[wep_type.unarmed], "length",0);							//Length of the sprite from origin to the far right. So it creates a bullet not at the center of the player but offset by a given amount.			
	#endregion

	#region Spear
	weapons[wep_type.spear] = ds_map_create();
	ds_map_add(weapons[wep_type.spear], "ene_dmg",35);								//The damage of the weapon to enemies
	ds_map_add(weapons[wep_type.spear], "ani_dmg",35);								//The damage of the weapon to animals
	ds_map_add(weapons[wep_type.spear], "env_dmg",25);								//The damage of the weapon to environment objects

	ds_map_add(weapons[wep_type.spear], "attack_spd",1);							//The attack speed
	ds_map_add(weapons[wep_type.spear], "attack_energy",15);						//The amount of energy used for the attack
	ds_map_add(weapons[wep_type.spear], "stun",false);								//States whether or not the attack can stun
	ds_map_add(weapons[wep_type.spear], "knockback",true);							//States whether or not the attack can knockback
	ds_map_add(weapons[wep_type.spear], "knockback_str",20);						//The amount of strength of the knockback
		
	ds_map_add(weapons[wep_type.spear], "scrshk_mag",1);							//The amount of screenshake magnitude
	ds_map_add(weapons[wep_type.spear], "scrshk_frm",15);							//The amount of screenshake frames
	ds_map_add(weapons[wep_type.spear], "ene_particles",4);							//The amount of particles on attack impact with enemies
	ds_map_add(weapons[wep_type.spear], "ani_particles",4);							//The amount of particles on attack impact with animal
	ds_map_add(weapons[wep_type.spear], "env_particles",3);							//The amount of particles on attack impact with environment

	ds_map_add(weapons[wep_type.spear], "projectile",obj_spear);					//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(weapons[wep_type.spear], "proj_spd",12);								//The speed of the projectile.
	ds_map_add(weapons[wep_type.spear], "proj_dist",200);							//The max distance of the projectile.
	//ds_map_add(weapons[wep_type.unarmed], "length",0);							//Length of the sprite from origin to the far right. So it creates a bullet not at the center of the player but offset by a given amount.			
	#endregion

	#region Kill Boomerang
	weapons[wep_type.killboomerang] = ds_map_create();
	ds_map_add(weapons[wep_type.killboomerang], "ene_dmg",30);						//The damage of the weapon to enemies
	ds_map_add(weapons[wep_type.killboomerang], "ani_dmg",30);						//The damage of the weapon to animals
	ds_map_add(weapons[wep_type.killboomerang], "env_dmg",15);						//The damage of the weapon to environment objects

	ds_map_add(weapons[wep_type.killboomerang], "attack_spd",0.8);					//The attack speed
	ds_map_add(weapons[wep_type.killboomerang], "attack_energy",15);				//The amount of energy used for the attack
	ds_map_add(weapons[wep_type.killboomerang], "stun",false);						//States whether or not the attack can stun
	ds_map_add(weapons[wep_type.killboomerang], "knockback",true);					//States whether or not the attack can knockback
	ds_map_add(weapons[wep_type.killboomerang], "knockback_str",10);				//The amount of strength of the knockback
		
	ds_map_add(weapons[wep_type.killboomerang], "scrshk_mag",2);					//The amount of screenshake magnitude
	ds_map_add(weapons[wep_type.killboomerang], "scrshk_frm",15);					//The amount of screenshake frames
	ds_map_add(weapons[wep_type.killboomerang], "ene_particles",3);					//The amount of particles on attack impact with enemies
	ds_map_add(weapons[wep_type.killboomerang], "ani_particles",3);					//The amount of particles on attack impact with animal
	ds_map_add(weapons[wep_type.killboomerang], "env_particles",2);					//The amount of particles on attack impact with environment
		
	ds_map_add(weapons[wep_type.killboomerang], "projectile",obj_killboomerang);	//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(weapons[wep_type.killboomerang], "proj_spd",8);						//The speed of the projectile.
	ds_map_add(weapons[wep_type.killboomerang], "proj_dist",300);					//The max distance of the projectile.
	//ds_map_add(weapons[wep_type.unarmed], "length",0);							//Length of the sprite from origin to the far right. So it creates a bullet not at the center of the player but offset by a given amount.				
	#endregion

#endregion

#region Setting Weapon Amount & Ammo Variables
weapon = 0;										//Sets the weapon to zero
weaponamount = 4;								//Amount of Weapons
ammo[array_length_1d(weapons)-1] = 0;			//Gets the amount of arrays
ammo[0] = 0;									//Set the ammo to -1 if you want infinite ammo. Great for melee weapons etc. Set to zero for no ammo and a positive value for the actual ammo.

scr_changeweapon(wep_type.unarmed);				//Add this code to change weapon. Change the number to the number the weapon is listed as.
#endregion