/// @description Draw Fade In Rectangle/ Text/ Skip Message

#region Example Of Drawing Scrolling Text
//Narration example cutscene
/*if (room == cutscene[2]) {
	scr_drawsettext(c_white,fnt_cutscene,fa_center,fa_top);
	draw_text(text_display_x, text_display_y, print);
}*/
#endregion

#region Drawing Background Cutscene Sprite & Text For Each Cutscene
if (room == cutscene[cutscene_type.acknowledge]) || (room == cutscene[cutscene_type.inspiration_client]) || (room == cutscene[cutscene_type.continue_]) {
	draw_sprite_ext(spr_bg_gui,0,0,0,gui_width,gui_height,0,c_black,1); //Drawing black background to hide the tile intialisation drawing.
	draw_sprite_stretched(cutscene_sprite,0,0,0,gui_width,gui_height);
}

if (room == cutscene[cutscene_type.cutscene_1]) || (room == cutscene[cutscene_type.cutscene_2]) || (room == cutscene[cutscene_type.cutscene_3]) || (room == cutscene[cutscene_type.cutscene_4]) { 
	//Getting the next image number into a variable. Stopping the changing of frames to the last frame of the sprite.
	var next_image_check = next_image; if (next_image >= cutscene_sprite_no) { next_image_check = cutscene_sprite_no - 1; }
	//Displaying the cutscene sprite and its changing frames/ shots.
	draw_sprite_stretched(cutscene_sprite,next_image_check,0,0,gui_width,gui_height);
	if (next_image >= cutscene_sprite_no) { fadeout = 1; }
}

#region Text For Cut Scene 1
if (room == cutscene[cutscene_type.cutscene_1]) {
	scr_drawsettext(c_black,fnt_cutscene,fa_center,fa_middle);
	if (next_image == 0) { // first panel in cutscene
		draw_text(text_display_x,text_display_y,"A brother and sister are called to a Council of Elders.")
	}
	if (next_image == 1) { // second panel in cutscene
		draw_text(text_display_x,text_display_y,chr(34) + "The Bunya Festival is approaching" +chr(34) + ", the Elders say.\n" + chr(34) + "Budburra, Hope, help us prepare.\nLet's make it the best one yet!" + chr(34))
	}
	if (next_image == 2) { // third panel in cutscene
		draw_text(text_display_x,text_display_y, chr(34) + "Your first task is to collect a feather from a white-bellied \nsea eagle. You will find a roost at the top of Mount Coolum." + chr(34))
	}
	if (next_image == 3) { // fourth panel in cutscene
		draw_text(text_display_x,text_display_y,chr(34)+ "You have different skills, you must work together. "+ chr(34))
	}	
	if (next_image == 4) { // fifth panel in cutscene
		draw_text(text_display_x,text_display_y,chr(34)+ "You each have different tasks to complete.\nThe journey will not be easy!\nBe careful, and help each other."+ chr(34))
	}
	if (next_image == 5) { // sixth panel in cutscene
		draw_text(middle_text_display_x,middle_text_display_y,"So the brother and sister\nset out on their journey.")
	}	
	if (next_image == 6) { // sixth panel in cutscene
		draw_text(middle_text_display_x,middle_text_display_y,"Budburra follows the coast \nacross the Maroochy River.\nHope travels through \nthe hinterland.")
	}	
	if (next_image >= 7) { // seventh panel in cutscene
		draw_text(middle_text_display_x,middle_text_display_y,"They will meet at the\ntop of Mount Coolum\nto find the eagle.")
	}	
} //end text for cut scene 1
#endregion

#region Text For Cut Scene 2
if (room == cutscene[cutscene_type.cutscene_2]) {
	scr_drawsettext(c_black,fnt_cutscene,fa_center,fa_middle);
	if (next_image == 0) { // first panel in cutscene
		draw_text(text_display_x,alt_text_display_y,"The giant red belly black snake hunted,\nBudburra's first task was done.")
	}
	if (next_image >= 1) { // second panel in cutscene
		draw_text(text_display_x,text_display_y,chr(34) + "If you hunt it, you must not waste it.\nA snake this big can feed the whole mob!" +chr(34) + ", the Elders said.")
	}

} //end text for cut scene 2
#endregion

#endregion

#region Drawing The Input Text To Change Frame/ Shot/ Display Skip Text & A Rectangle For Fade In
//Narration example cutscene
/*
if (room == cutscene[2]) {
	if (skip > skip_display)
	{
		draw_set_alpha(scr_wave(0.2,0.8,1,0));	
		scr_drawsettext(c_black,fnt_cutscene,fa_center,fa_middle);
		draw_text(skip_display_x, skip_display_y, "Hold To Skip");
		draw_set_alpha(1);
	}
}
*/
//Display the input text to continue
if (room == cutscene[cutscene_type.continue_]) {
	if (next_image_display == 1) {
		draw_set_alpha(scr_wave(0.4,0.9,2,0));	
		scr_drawsettext(c_black,fnt_cutscene_thin,fa_right,fa_middle);
		if (global.computer_use) { draw_text_transformed(skip_display_x, skip_display_y, "Hold any button\n to continue", 1, 1, image_angle); }
		if (global.mobile_use) { draw_text_transformed(skip_display_x, skip_display_y, "Hold to continue", 1, 1, image_angle); }
		draw_set_alpha(1);
	}
}

//Displaying the input text to change frame/ shot (to continue through cutscene).
if (room == cutscene[cutscene_type.cutscene_1]) || (room == cutscene[cutscene_type.cutscene_2]) || (room == cutscene[cutscene_type.cutscene_3]) || (room == cutscene[cutscene_type.cutscene_4]) { 
	if (next_image_display == 1) {
		draw_set_alpha(scr_wave(0.4,0.9,2,0));	
		scr_drawsettext(c_black,fnt_cutscene_thin,fa_center,fa_middle);
		if (global.computer_use) { draw_text_transformed(skip_display_x, skip_display_y, "Press any button\n to continue", 0.7, 0.7, image_angle); }
		if (global.mobile_use) { draw_text_transformed(skip_display_x, skip_display_y, "Tap to continue", 0.7, 0.7, image_angle); }
		draw_set_alpha(1);
	}
}
//Drawing the rectangle for fade in
draw_sprite_ext(spr_bg_gui,0,0,0,gui_width,gui_height,0,c_black,alpha);
#endregion