/// @description Fading In & Out/ Transition/ Dialogue/ Text

#region Camera Movement Setting
/*
//X Position movement
if (room == cutscene[2]) || (room == cutscene[6]) {
	camera_set_view_pos(view_camera[0], xpos, ypos);
	xpos = min(xpos + cam_pos_inc, target_cam_pos);   //Moves the camera to the right (+) or to the left (-). Min is for moving the camera right without it exiting the room and max is for moving the camera left without leaving the room. Just set the second value to 0.
}

//Y Position Movement
if (room == cutscene[4]) || (room == cutscene[5]) {
	camera_set_view_pos(view_camera[0], xpos, ypos);
	ypos = min(ypos+ cam_pos_inc, target_cam_pos);   //Moves the camera to the right (+) or to the left (-). Min is for moving the camera right without it exiting the room and max is for moving the camera left without leaving the room. Just set the second value to 0.
}*/
#endregion

#region Fading Effect | Changing The Alpha
if (!fadeout) {
	alpha = max(alpha - fadingin_inc, max_fade_in); 
}
else {
	alpha = min(alpha + fadingout_inc, max_fade_out);
}
#endregion

#region Text Display
/*if (room == cutscene[2]) {
	l += l_spd;
	print = string_copy(str,1,l);							//string_copy copies some of the contents of one string and can be all of it. 1st number value is where to start the coping. Having it set to 1 sets it to start coping on the first letter.

	if (l > string_length(str)+100) && (next < array_length_1d(strings)-1)
	{
		l = 0;
		next++;												//++ and -- is another way of saying +1 or -1
		if (next == array_length_1d(strings)-1) skip++;		//Moves hold space to one.	
	}
	str = strings[next];									//Moves to the next string of text.
}*/
#endregion

#region Dialogue Display
	//Use create_textevent to create dialogue during a cutscene

	#region Example of a dialogue cutscene event
		/*if (room = cutscene[6]) {
			if (xpos = target_cam_pos) && (dialogue = 0) {
				dialogue = 1;
				var Djagi = obj_Djagi, Lowanna = obj_Lowanna, Rain = obj_Rain, Solace = obj_Solace, Budburra = obj_Budburra, Hope = obj_Hope;
				create_textevent(
				//MyText
				[
					"Wh.. what happened here?",
					"Wakan was here. His followers burned everything.",
					"Where is mum and dad?",
					"I am sorry.",
					"Wakan . .  he . . .",
					"They stood up to him.",
					"His followers killed them.",
					"Where is Gira?",
					"They tried to stop him, but he took her.",
					"We don't know where he went, but we tried to follow him.",
					"He has left his followers behind him. They will stop anyone looking for Wakan.",
					"We know this is a lot to ask of you so soon, but you must find him.",
					"Yes. We have to find Gira.",
					"Where do we start?",
					"Budburra, you must go to Coolum and ask him how to fight Wakan.",
					"Hope, you must go to Maroochy and ask her about Gira's power.",
					"Wakan's followers will try to stop you. Do not let them."
			    ],
				//mySpeakers
				[Hope, Rain, Budburra, Djagi, Lowanna, Solace, Djagi, Hope, Rain, Lowanna, Solace, Djagi, Hope, Budburra, Solace, Lowanna, Rain], 
				//myEffects
			    -1,   
				//myTextSpeed
			    -1, 
				//myTypes
			    -1,                                                                   
				//myNextLine
			    -1,                                                            
				//myScripts
			    -1,                                                                                            
				//myTextCol
			    -1,                                                                                            
				//myEmotion
			    [2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 3]
				);	
			}
		if (dialogue = 1) && (!instance_exists(obj_textevent)) { fadeout = 1; }
		}	*/
	#endregion

#endregion

#region Checking Input For Skipping Or Timer Reached A Max Amount
if (room == cutscene[cutscene_type.acknowledge]) || (room == cutscene[cutscene_type.inspiration_client]) {
	if (keyboard_check(vk_anykey)) || (mouse_check_button(mb_any)) { skip++; }
	timer += 1;
	if (skip > max_skip) || (timer > max_timer) { fadeout = 1; }
}

if (room == cutscene[cutscene_type.continue_]) {
	//Setting timer to display the text to change to the next frame/ shot
	image_change_timer ++;
	if (image_change_timer >= max_image_change_timer) {
		next_image_display = 1;
	}
	if (keyboard_check(vk_anykey)) || (mouse_check_button(mb_any)) { skip++; }
	if (skip > max_skip) { fadeout = 1; }
}

//Display press anything to continue text timer
if (room == cutscene[cutscene_type.cutscene_1]) || (room == cutscene[cutscene_type.cutscene_2]) || (room == cutscene[cutscene_type.cutscene_3]) || (room == cutscene[cutscene_type.cutscene_4]) {
	
	//Setting timer to display the text to change to the next frame/ shot
	image_change_timer ++;
	if (image_change_timer >= max_image_change_timer) {
		next_image_display = 1;
	}
	
	//Changing sprite index based upon button/ mouse press
	if (global.computer_use) { if (keyboard_check_pressed(vk_anykey)) || (mouse_check_button_pressed(mb_any)) { next_image ++; next_image_display = 2; } }
	if (global.mobile_use) { if (device_mouse_check_button_pressed(0, mb_left)) || (device_mouse_check_button_pressed(1, mb_left)) { next_image ++; next_image_display = 2; } }
	
	//Resetting press anything timer and press to continue text
	if (next_image_display == 2) {
		image_change_timer = 0; next_image_display = 0;
	}
}

//Destroying transition object if it exists so fade in and out works as intended
if (instance_exists(obj_transition)) { instance_destroy(obj_transition); }
#endregion

//Transition To Target Room
if (alpha == 1) && (fadeout == 1) { room_goto(targetroom); }