/// @description Setting Variables

#region Cutscene Variables
gui_height				= display_get_gui_height();				//The height of the gui display (returns number, example being 1080).
gui_width				= display_get_gui_width();				//The width of the gui display (returns number, example being 1920).
alpha					= 1;									//Current transparency (alpha), a fading effect for the screen or whatever else.
skip					= 0;									//Sets how long the player must press an input to skip the cutscene.
skip_display_x			= gui_width - (gui_width / 12);			//Sets the x position of the skip display message.
skip_display_y			= gui_height - (gui_height / 9);		//Sets the x position of the skip display message.
fadeout					= 0;									//Variable to check whether fadeout is ready (1) or not (0).
timer					= 0;									//Setting timer for cutscene transition.
xpos					= 0;									//The position number for x coordinate in a room.
ypos					= 0;									//The position number for y coordinate in a room.
str						= "";									//Represent the line of text on the bottom thats gonna contain the current text that we want to draw.
print					= "";									//Printing the text so it shows one letter at a time. Contains and shows what its up to in terms of the text so it can show a whole line or a few words or single letters at a time.
//text_display_x		= gui_height - (gui_height / 9);		//Sets x position of text that will be displayed. Separated from skip y position display variable for flexibility.
text_display_x			= gui_width - (gui_width / 2);			//Sets x position of text that will be displayed. Separated from skip y position display variable for flexibility.
text_display_y			= gui_height - (gui_height / 9);		//Sets y position of text that will be displayed. Separated from skip y position display variable for flexibility.
alt_text_display_y		= gui_height - (gui_height / 15);
middle_text_display_x	= gui_width / 2;						//Sets x position of text that will be displayed. Separated from skip y position display variable for flexibility.
middle_text_display_y	= gui_height / 2;						//Sets y position of text that will be displayed. Separated from skip y position display variable for flexibility.


l						= 0;									//Represents the letter we are up to. So how many letters of string is to put into print.
next					= 0;									//Decides which string or array are the next one in line to draw. Once it finishes with one string (text) it goes to the next one.
dialogue_end			= 0;									//Dialogue variable to end the cutscene once dialogue is complete.
#endregion

#region Cutscenes In The Game | In Enum & Array List Structure
enum cutscene_type {
	acknowledge			= 0,
	inspiration_client	= 1,
	continue_			= 2,
	cutscene_1			= 3,
	cutscene_2			= 4,
	cutscene_3			= 5,
	cutscene_4			= 6,
	height
}

cutscene[cutscene_type.cutscene_4]			= rm_cutscene_4;
cutscene[cutscene_type.cutscene_3]			= rm_cutscene_3;
cutscene[cutscene_type.cutscene_2]			= rm_cutscene_2;
cutscene[cutscene_type.cutscene_1]			= rm_cutscene_1;
cutscene[cutscene_type.continue_]			= rm_continue;
cutscene[cutscene_type.inspiration_client]	= rm_inspiration_client;
cutscene[cutscene_type.acknowledge]			= rm_acknowledgment;
#endregion

#region Setting Unique Variables For Each Cutscene

	#region Acknowledgement
	if (room == cutscene[cutscene_type.acknowledge]) {
		cutscene_sprite = spr_acknowledgement;							//Gets which sprite to display for cutscene
		max_skip = 50;													//Sets the max skip count
		skip_display = 0;												//Sets the amount the player must hold to display the skip message
		max_timer = room_speed * 10;									//Sets the max timer count
		fadingin_inc = 0.003;											//Sets the fading in amount per frame
		fadingout_inc = 0.005;											//Sets the fading out amount per frame
		max_fade_in = 0.01;												//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_inspiration_client;								//Sets the target room after cutscene is complete
	}
	#endregion
	
	#region Inspiration_Client
	if (room == cutscene[cutscene_type.inspiration_client]) {
		cutscene_sprite = spr_inspiration_client;						//Gets which sprite to display for cutscene
		max_skip = 50;													//Sets the max skip count
		skip_display = 0;												//Sets the amount the player must hold to display the skip message
		max_timer = room_speed * 10;									//Sets the max timer count
		fadingin_inc = 0.003;											//Sets the fading in amount per frame
		fadingout_inc = 0.005;											//Sets the fading out amount per frame
		max_fade_in = 0.01;												//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_continue;										//Sets the target room after cutscene is complete
	}
	#endregion
	
	#region Continue
	if (room == cutscene[cutscene_type.continue_]) {
		cutscene_sprite = spr_continue;									//Gets which sprite to display for cutscene
		max_skip = 15;													//Sets the max skip count
		next_image_display = 0;											//Checks to display the text to change to the next image
		image_change_timer = 0;											//Sets the timer for the display of text telling the player to press a button to change to the next image
		max_image_change_timer = room_speed * 4;						//Sets the max timer for the display of text
		fadingin_inc = 0.006;											//Sets the fading in amount per frame
		fadingout_inc = 0.008;											//Sets the fading out amount per frame
		max_fade_in = 0.01;												//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_menu;											//Sets the target room after cutscene is complete
	}
	#endregion
	
	#region Cutscene 1
	if (room == cutscene[cutscene_type.cutscene_1]) {
		cutscene_sprite = spr_cutscene_1;								//Gets which sprite to display for cutscene
		cutscene_sprite_no = sprite_get_number(cutscene_sprite);		//Gets the number of frames/ shots from the cutscene image.
		next_image = 0;													//Sets the next image count to display
		next_image_display = 0;											//Checks to display the text to change to the next image
		image_change_timer = 0;											//Sets the timer for the display of text telling the player to press a button to change to the next image
		max_image_change_timer = room_speed * 5;						//Sets the max timer for the display of text
		fadingin_inc = 0.006;											//Sets the fading in amount per frame
		fadingout_inc = 0.008;											//Sets the fading out amount per frame
		max_fade_in = 0.001;											//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_level_1a;										//Sets the target room after cutscene is complete
		
		//Text Variables
		//Contain the lines of text.
		strings[0] = "This is a story of a brother and sister";  
		strings[1] = "This is a story of talking mountains and rivers of tears.";
		strings[2] = "This is a story of a lost child";
		strings[3] = "and the reawakening of evil past.";
		strings[4] = "This is";
		l_spd = 0.7;			//Sets the speed of the letter being shown (having it 0.5 sets it to show one letter every two frames) If you want two or more letters showing, set it to '2 or 3 or 4' etc.
	}
	#endregion
	
	#region Cutscene 2
	if (room == cutscene[cutscene_type.cutscene_2]) {
		cutscene_sprite = spr_cutscene_2;								//Gets which sprite to display for cutscene
		cutscene_sprite_no = sprite_get_number(cutscene_sprite);		//Gets the number of frames/ shots from the cutscene image.
		next_image = 0;													//Sets the next image count to display
		next_image_display = 0;											//Checks to display the text to change to the next image
		image_change_timer = 0;											//Sets the timer for the display of text telling the player to press a button to change to the next image
		max_image_change_timer = room_speed * 5;						//Sets the max timer for the display of text
		fadingin_inc = 0.006;											//Sets the fading in amount per frame
		fadingout_inc = 0.008;											//Sets the fading out amount per frame
		max_fade_in = 0.001;											//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_level_1b;										//Sets the target room after cutscene is complete
		
		//Text Variables
		//Contain the lines of text.
		strings[0] = "This is a story of a brother and sister";  
		strings[1] = "This is a story of talking mountains and rivers of tears.";
		strings[2] = "This is a story of a lost child";
		strings[3] = "and the reawakening of evil past.";
		strings[4] = "This is";
		l_spd = 0.7;				//Sets the speed of the letter being shown.
	}
	#endregion
	
	#region Cutscene 3
	if (room == cutscene[cutscene_type.cutscene_3]) {
		cutscene_sprite = spr_cutscene_3;								//Gets which sprite to display for cutscene
		cutscene_sprite_no = sprite_get_number(cutscene_sprite);		//Gets the number of frames/ shots from the cutscene image.
		next_image = 0;													//Sets the next image count to display
		next_image_display = 0;											//Checks to display the text to change to the next image
		image_change_timer = 0;											//Sets the timer for the display of text telling the player to press a button to change to the next image
		max_image_change_timer = room_speed * 5;						//Sets the max timer for the display of text
		fadingin_inc = 0.006;											//Sets the fading in amount per frame
		fadingout_inc = 0.008;											//Sets the fading out amount per frame
		max_fade_in = 0.001;											//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_mcredits;										//Sets the target room after cutscene is complete
		
		//Text Variables
		//Contain the lines of text.
		strings[0] = "This is a story of a brother and sister";  
		strings[1] = "This is a story of talking mountains and rivers of tears.";
		strings[2] = "This is a story of a lost child";
		strings[3] = "and the reawakening of evil past.";
		strings[4] = "This is";
		l_spd = 0.7;			//Sets the speed of the letter being shown (having it 0.5 sets it to show one letter every two frames) If you want two or more letters showing, set it to '2 or 3 or 4' etc.
	}
	#endregion
	
	#region Cutscene 4
	if (room == cutscene[cutscene_type.cutscene_4]) {
		cutscene_sprite = spr_cutscene_4;								//Gets which sprite to display for cutscene
		cutscene_sprite_no = sprite_get_number(cutscene_sprite);		//Gets the number of frames/ shots from the cutscene image.
		next_image = 0;													//Sets the next image count to display
		next_image_display = 0;											//Checks to display the text to change to the next image
		image_change_timer = 0;											//Sets the timer for the display of text telling the player to press a button to change to the next image
		max_image_change_timer = room_speed * 5;						//Sets the max timer for the display of text
		fadingin_inc = 0.006;											//Sets the fading in amount per frame
		fadingout_inc = 0.008;											//Sets the fading out amount per frame
		max_fade_in = 0.001;											//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;												//Sets the maximum fade out value for the alpha value
		targetroom = rm_level_2a;										//Sets the target room after cutscene is complete
		
		//Text Variables
		//Contain the lines of text.
		strings[0] = "This is a story of a brother and sister";  
		strings[1] = "This is a story of talking mountains and rivers of tears.";
		strings[2] = "This is a story of a lost child";
		strings[3] = "and the reawakening of evil past.";
		strings[4] = "This is";
		l_spd = 0.7;				//Sets the speed of the letter being shown.
	}
	#endregion
	
	#region Example of cutscene with scrolling text
	/*if (room == cutscene[2]) {
		max_skip = 200;			//Sets the max skip count
		skip_display = 35;		//Sets the amount the player must hold to display the skip message
		max_timer = 785;		//Sets the max timer count
		fadingin_inc = 0.007;	//Sets the fading in amount per frame
		fadingout_inc = 0.008;	//Sets the fading out amount per frame
		max_fade_in = 0.02;		//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;		//Sets the maximum fade out value for the alpha value
		cam_pos_inc = 2.5;		//Sets position increment speed per frame for camera movement
		target_cam_pos = 2220;	//Sets target position for camera
		//targetroom = rm_cutscene_ajlogo;	//Sets the target room after cutscene is complete
		
		//Text Variables
		//Contain the lines of text.
		strings[0] = "This is a story of a brother and sister";  
		strings[1] = "This is a story of talking mountains and rivers of tears.";
		strings[2] = "This is a story of a lost child";
		strings[3] = "and the reawakening of evil past.";
		strings[4] = "This is";
		l_spd = 0.7;			//Sets the speed of the letter being shown (having it 0.5 sets it to show one letter every two frames) If you want two or more letters showing, set it to '2 or 3 or 4' etc.
	} */
	#endregion
	
	#region Example of camera moving to target movement
	/*if (room == cutscene[6]) {
		max_skip = 100;			//Sets the max skip count
		skip_display = 0;		//Sets the amount the player must hold to display the skip message
		max_timer = 550;		//Sets the max timer count
		fadingin_inc = 0.003;	//Sets the fading in amount per frame
		fadingout_inc = 0.005;	//Sets the fading out amount per frame
		max_fade_in = 0.08;		//Sets the maximum fade in value for the alpha value
		max_fade_out = 1;		//Sets the maximum fade out value for the alpha value
		xpos = 40;				//The position number for x coordinate in a room.
		ypos = 2880;			//The position number for y coordinate in a room.
		cam_pos_inc = 1.1;		//Sets position increment speed per frame for camera movement
		target_cam_pos = 450;	//Sets target position for camera
		//targetroom = rm_level_3;	//Sets the target room after cutscene is complete
	}*/
	#endregion
	
#endregion