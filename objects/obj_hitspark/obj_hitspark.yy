{
    "id": "0ccc19ea-46fe-4753-816c-f934c63ee2a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitspark",
    "eventList": [
        {
            "id": "bdd7e110-285b-4788-b57e-2173158fd6c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ccc19ea-46fe-4753-816c-f934c63ee2a5"
        },
        {
            "id": "07848c07-3b95-470b-9d38-af7d45f13366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ccc19ea-46fe-4753-816c-f934c63ee2a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
    "visible": true
}