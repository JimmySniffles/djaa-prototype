/// @description Creating One Way Climbing Platforms & Setting XScale To In-Game XScale
if (!place_meeting(x,y,par_oneway_platform)) { with (instance_create_layer(bbox_left,bbox_top,"Interactive",par_oneway_platform)) { image_xscale = other.image_xscale * 2; } }

/* Debugging Purposes. Place In Step Event
amount = instance_number(par_oneway_platform);
show_debug_message(amount);