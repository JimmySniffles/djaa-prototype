{
    "id": "51757014-fa71-4849-9e24-f32f4eb01404",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_climb",
    "eventList": [
        {
            "id": "d03c60ea-f374-429f-8765-44d34da74199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "51757014-fa71-4849-9e24-f32f4eb01404"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "eec0faef-1a47-4d3e-8816-64bd26c52d39",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0123a09-5820-4a8c-8998-111b416baf22",
    "visible": false
}