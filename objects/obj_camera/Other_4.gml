/// @description Setting Camera Position To Player At Beginning Of Level

#region Setting Position Of Camera To Whats Being Followed At Room Start
if (follow != 0) {
	if (instance_exists(follow)) {	
		//This follows the objects x and y position
		x = follow.x;																
		y = follow.y;
	
		//Keep Camera Center Inside Room
		x = clamp(x, view_width + buffer, room_width - view_width - buffer);
		y = clamp(y, view_height + buffer, room_height - view_height - buffer);

		//Update Camera View. This can only be used when you set up a viewport and get camera ID.
		camera_set_view_pos(global.camera_view, x - view_width, y - view_height);
	}
}
#endregion