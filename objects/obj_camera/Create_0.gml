/// @description Set Up Camera

#region Camera Variable Set Up
//Setting zoom for camera
reset_view_zoom			= false;
max_zoom_in				= 0.5;
max_zoom_out			= 5;
view_zoom				= 1;
zoom_timer_delay		= 0;
max_zoom_timer_delay	= room_speed * 5;
reached_max_zoom_timer	= false;

//Following variable. Setting default follow value based upon which room were in
follow = 0;
if (room == rm_dev) || (room == rm_level_1a) || (room == rm_level_1b) || (room == rm_level_2a) { 
	if (instance_exists(obj_boy)) { follow = obj_boy; }
	if (instance_exists(obj_girl)) { follow = obj_girl; } //Checking girl character for debugging
}

//This is to centre the camera to whichever position & set ideal width and height for camera view size
ideal_width				= 640;																//Sets ideal width for the camera display properties
ideal_height			= 360;																//Sets ideal height for the camera display properties
x_camera_ext			= 80;																//Sets the camera's x extension amount that the camera can move by the mouse.
y_camera_ext			= 50;																//Sets the camera's y extension amount that the camera can move by the mouse.
height_offset			= 40;																//Sets offset amount for the height camera view position.
width_offset			= 80;																//Sets offset amount for the width camera view position.
view_width				= camera_get_view_width(global.camera_view) * 0.5 - width_offset;	//Gets camera width in pixels and multiplies by 0.5 which is also dividing it by 2. This is to centre the camera.
view_height				= camera_get_view_height(global.camera_view) * 0.5 + height_offset;	//Gets camera height in pixels and multiplies by 0.5. + is to increase the offset to be higher and - to offset to be lower.

//The X and Y coordinate position for the camera to move towards variables
xTowards				= xstart;																
yTowards				= ystart;																
#endregion

#region Screenshake Variables
//These values will be used changed dynamically with the the script (scr_screenshake) to shake the camera that is called. The default values are set here.
shake_length			= 0;			//Variable for how long the screenshake will happen.
shake_magnitude			= 0;			//Variable for the magnitude of the screenshake. Higher number shakes more violently. If for example this is set to 5 the camera will move 5 pixels in either the left, top, right or bottom in random direction.
shake_remain			= 0;			//Variable for the shake remain. This is to set how much shake remains which will be the same as the magnitude and decreased by 1 until it hits zero.
buffer					= 32;			//The buffer amount. Adds a buffer to the camera position to make sure to not shake outside of the screen. Make sure to set this to be higher than the highest screenshake that you are going to have.
#endregion

#region Layer Check
if (layer_exists("Mountains_BG")) { MountainsBGLayer = layer_get_id("Mountains_BG"); }
MountainsBG_x_offset	= 600;
MountainsBG_y_offset	= 700;
#endregion