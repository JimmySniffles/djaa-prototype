/// @description Update Camera

#region Setting Camera Zoom & Camera View Size
//Zooming the camera if the player is not touching any input after a period of time.
var check_input = false;
if (global.computer_use) { check_input = (keyboard_check_pressed(vk_anykey)) || (keyboard_check(vk_anykey)) || (mouse_check_button_pressed(mb_any)) || (mouse_check_button(mb_any)); }
if (global.mobile_use) { check_input = (device_mouse_check_button_pressed(0,mb_left)) || (device_mouse_check_button(0,mb_left)) || (device_mouse_check_button_pressed(1,mb_left)) || (device_mouse_check_button(1,mb_left)); }

if (!check_input) && (reset_view_zoom = false) {
	if (zoom_timer_delay < max_zoom_timer_delay) { zoom_timer_delay ++; }
	if (zoom_timer_delay >= max_zoom_timer_delay) { reached_max_zoom_timer = true; }
	if (reached_max_zoom_timer) { scr_zoom_view(0.005,0.95); zoom_timer_delay = 0; }
} else { zoom_timer_delay = 0; }

if (check_input) && (reached_max_zoom_timer) { reset_view_zoom = true; reached_max_zoom_timer = false; }

//Setting Camera View Size, Clamping view_zoom To Min & Max & Resetting view_zoom Upon Command
if (view_zoom != 1) && (reset_view_zoom = true) { scr_zoom_view(0.1,1); }
if (view_zoom = 1) { reset_view_zoom = false; }
view_zoom = clamp(view_zoom, max_zoom_in, max_zoom_out);
camera_set_view_size(global.camera_view,ideal_width*view_zoom,ideal_height*view_zoom);
#endregion

#region Update Destination While Also Being Able To Move Camera Using Mouse & Update Object Position
//Updates Destination to follow whats set as follow plus allow for the player to move the mouse to a given radius
if (instance_exists(follow))
{	
	//This follows the objects x and y position
	xTowards = follow.x;																
	yTowards = follow.y;	
	
	//This code is for allowing the player to move the camera with the mouse.
	if (global.computer_use) { 
	//This is to set the cameras direction to face the position of the mouse's x and y coordinates
	direction = point_direction(x,y,mouse_x,mouse_y);
	//Lengthdir is length in a direction. Min is the smallest amount thats placed inside the function. This is so the camera does not always just go to the side of the radius if the mouse is inside the maximum radius.
	//This is needed to be executed for both the x and y.
	xTowards = follow.x + lengthdir_x(min(x_camera_ext, distance_to_point(mouse_x, mouse_y)), direction);
	yTowards = follow.y + lengthdir_y(min(y_camera_ext, distance_to_point(mouse_x, mouse_y)), direction);
	}
}

//Updates The Camera's Object Position
//This gets the current camera position then adding to that position by subtracting the target position by the current camera position and dividing it by 25.
//Dividing it by 25 is so the camera moves per frame a 25th of the distance between the current camera position and the target position. Allows for the camera to move smoothly without snapping to the target position. It slows to a halt when it gets closer.
x += (xTowards - x) / 25;
y += (yTowards - y) / 25;
#endregion

#region Keep Camera Center Inside Room
x = clamp(x, view_width + buffer, room_width - view_width - buffer);
y = clamp(y, view_height + buffer, room_height - view_height - buffer);
#endregion

#region Screenshake
//Moves the camera position randomly
//If for example shake remain is 5 it gets a random value between -5 and 5.
x += random_range(-shake_remain, shake_remain);
y += random_range(-shake_remain, shake_remain);

//Decreases shake remain every frame and not go below zero.
shake_remain = max(0, shake_remain -((1/shake_length) * shake_magnitude));
#endregion

#region Update Camera View. This can only be used when you set up a viewport and get camera ID.
camera_set_view_pos(global.camera_view, x - view_width, y - view_height);
#endregion

#region Parallax Background
//Parallax Background (BG layer moves slower than foreground layer) The Further You Divide, The Less You Get From X (Camera Position) The Closer The Layer Will Look. X By Itself Is Maximum Depth

if (room == rm_level_1a) || (room == rm_level_1b) || (room == rm_level_2a)
{
	layer_x(MountainsBGLayer,x-MountainsBG_x_offset/1.1);
	layer_y(MountainsBGLayer,y-MountainsBG_y_offset/1.1);
}
#endregion

#region Switching Characters Mechanic
if (global.key_switch) { 
	if (follow == obj_boy) && (instance_exists(obj_girl)) { follow = obj_girl; } 
	else if (follow == obj_girl) && (instance_exists(obj_boy)) { follow = obj_boy; }
}
#endregion