/// @description Draw Fade In Rectangle/ Text/ Skip Message

#region Drawing The Text
scr_drawsettext(c_white,fnt_credits,fa_center,fa_middle);
draw_text(gui_width * 0.5,y+2400,string_hash_to_newline(credits));
if ( y < -string_height(string_hash_to_newline(credits))) { y = display_get_gui_height(); }
#endregion

#region Drawing Hold Space Text
if (skip > skip_display) {
	draw_set_alpha(scr_wave(0.2,0.8,1,0));	
	draw_text(skip_display_x, skip_display_y,"Hold To Skip");
	draw_set_alpha(1);
}
#endregion

//Drawing the rectangle for fade in
draw_sprite_ext(spr_bg_gui,0,0,0,gui_width,gui_height,0,c_black,alpha);