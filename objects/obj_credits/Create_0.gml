/// @description Credits Text + Setting Variables

#region Credits Variables
skip			= 0;								//Sets how long the player must press an input to skip the cutscene.
fadeout			= 0;								//Variable to check whether fadeout is ready (1) or not (0).
alpha			= 1;								//Current transparency (alpha), a fading effect for the screen or whatever else.
timer			= 0;								//Setting timer for cutscene transition.
max_timer		= room_speed * 56;					//Sets the max timer count.
gui_width		= display_get_gui_width();			//Gets display gui width
gui_height		= display_get_gui_height();			//Gets display gui height
skip_display_x	= gui_width - (gui_width / 12);		//Sets the x position of the skip display message.
skip_display_y	= gui_height - (gui_height / 9);	//Sets the x position of the skip display message.
skip_display	= 35;								//Sets the amount the player must hold to display the skip message.
text_display_x	= gui_height - (gui_height / 9);	//Sets x position of text that will be displayed. Separated from skip y position display variable for flexibility.
text_display_y	= gui_height - (gui_height / 9);	//Sets y position of text that will be displayed. Separated from skip y position display variable for flexibility.
max_skip		= 200;								//Sets the max skip count.
fadingin_inc	= 0.003;							//Sets the fading in amount per frame.
fadingout_inc	= 0.005;							//Sets the fading out amount per frame.
max_fade_in		= 0.15;								//Sets the maximum fade in value for the alpha value.
max_fade_out	= 1;								//Sets the maximum fade out value for the alpha value.
targetroom		= rm_menu;							//Sets the target room after cutscene is complete.
y				= display_get_gui_height();			//The height of the display (returns number, example being 1080).
vspeed			= -1.2;								//The higher the negative number the faster the credits speed.
#endregion

#region Credits Text
credits		 =	"Game Design#Croc Collab##";
credits		 += "Game Director#Christopher Haight##";
credits		 += "Lead Programmer#Jamie Pasnin##";
credits		 += "Lead Artist#Travis Dever##";
credits		 += "Supporting Artists#Jamie Pasnin#Liam McLaren##";
credits		 += "Lead Designer#Jamie Pasnin##";
credits		 += "Supporting Designers#Christopher Haight#Travis Dever#Callum Howard#Liam McLaren##";
credits		 += "Level Designers#Liam McLaren#Jamie Pasnin##";
credits		 += "Lead Graphic Artist#Travis Dever##";
credits		 += "Supporting Graphic Artist#Jamie Pasnin##";
credits		 += "Lead Writer#Callum Howard##";
credits		 += "Supporting Writers#Christopher Haight#Travis Dever##";
credits		 += "Visual Effects#Jamie Pasnin##";
credits		 += "Marketer#Liam McLaren##";
credits		 += "Sales Manager#Liam McLaren##";
credits		 += "Researchers#Christopher Haight#Jamie Pasnin#Travis Dever#Callum Howard#Liam McLaren##";
credits		 += "Producers#Christopher Haight#Kerry Neill##";
credits		 += "Consultant#Kerry Neill##";
credits		 += "Art Consultant#Hope O'Chin##";
credits		 += "Audio Director#Jamie Pasnin##";
credits		 += "Sound Designer#Jamie Pasnin##";
credits		 += "Music | Provided By#Hope O'Chin#Kerry Neill##";
credits		 += "Other Music | Provided As#Royalty Free##";
credits		 += "Voices#In Order of Appearance##";
credits		 += "Cutscene Narrator | Kerry Neill##";
credits		 += "Budburra | Kerry Neill##";
credits		 += "Hope | Emily Pasnin##";
credits		 += "Followers of Ninderry | Jamie Pasnin##";
credits		 += "Special Thanks#University of the Sunshine Coast#Colleen Stieler-Hunt#Hope O'Chin#Kerry Neill#For Making This Game Possible##";
credits		 += "Legal#";
credits		 += "Ancestor's Journey (C) 2018 Croc Collab, University undergraduate group project.#Croc Collab and related logos are trademarks in Australia and/or other countries. All Rights Reserved.##";
credits		 += "Game Engine#Game Maker Studio 2##";
credits		 += "Ancestors' Journey##";
credits		 += "Thank You For Playing#You're Awesome##";
credits		 += "   ##    ##     ##     ##     ##";			//Buffer at the end so it doesn't disappear early
#endregion