/// @description Fading In & Out & Transition

#region Fading Effect | Changing The Alpha
if (!fadeout) { 
	alpha = max(alpha - fadingin_inc, max_fade_in); 
} 
else { 
	alpha = min(alpha + fadingout_inc, max_fade_out); 
}
#endregion

#region Checking Input For Skipping Or Timer Reached A Max Amount
if (keyboard_check(vk_anykey)) || (mouse_check_button(mb_any)) { skip++; }
timer += 1;
if (skip > max_skip) || (timer > max_timer) { fadeout = 1; }
#endregion

//Transition To Target Room
if (alpha == 1) && (fadeout == 1) { room_goto(targetroom); }