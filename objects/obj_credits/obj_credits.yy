{
    "id": "51cd6372-06f8-43fd-ba58-d1b0d208d129",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credits",
    "eventList": [
        {
            "id": "ab3bb13e-9831-415c-be7f-cc29f7794e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51cd6372-06f8-43fd-ba58-d1b0d208d129"
        },
        {
            "id": "57ae9676-c867-457a-a88b-a114acbe22a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "51cd6372-06f8-43fd-ba58-d1b0d208d129"
        },
        {
            "id": "94e5b4eb-15bf-44f0-8da4-a97caf9da1fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51cd6372-06f8-43fd-ba58-d1b0d208d129"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}