/// @description Destroy Data Structure Grids Below Upon Destroy Of This Object
//Checks to see if specific data structure exists. May need to be used
//if (ds_exists(ds_inventory, ds_type_grid)) { ds_grid_destroy(ds_inventory); }
ds_grid_destroy(ds_inventory);
ds_grid_destroy(ds_items_info);