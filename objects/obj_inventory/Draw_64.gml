/// @description Drawing Inventory

//If show inventory is false, exit and not execute the draw code.
if(show_inventory = false) exit;

#region Drawing Inventory Background
//Draws part of a sprite (in this case the inventory UI sprite) at a given position with scaling, blending and alpha options.
draw_sprite_part_ext(
	spr_inv_ui, 0, cell_size, 0, inv_ui_width, inv_ui_height, 
	inv_ui_x, inv_ui_y, scale, scale, c_white, 1
);

//Use var to make a temporary local variable for accessing data structures. Makes it faster in terms of performance and easier to read.
//Example: var info_grid = ds_player_info;
#endregion

#region Drawing Slots, Items In Slots and Number Of Items In A Slot
//Intialising temporary local variables. 
var ii , ix, iy, xx, yy, sx, sy, c, iitem, inv_grid;
ii = 0; ix = 0; iy = 0; c = c_black; inv_grid = ds_inventory;
//ii is the current item index were on and will be incremented.
//ix and iy is the current cell or inventory slot that we are seeing.
//xx and yy are the location or position of the x and y of the current cell or inventory slot by top left of said slot.
//sx and sy is the sprite location or position of the x and y for the items.
//iitem is the current item we are on that we pull from the grid.
//inv_grid is declared as a local variable for the same reason mentioned above about having local variables for data structures reference.


//Repeat the amount of slots given and get the set things below
repeat(inv_slots){
	//x,y location for slot
	//This will get the inv slot x position plus the cell size plus the x buffer times by the horizontal coordinate for the slot (ix) times the scale. Same goes for the y axis.
	xx = inv_slots_x + ((cell_size + x_buffer) * ix * scale);
	yy = inv_slots_y + ((cell_size + y_buffer) * iy * scale);
	
	//Item
	//This grid starts at zero because thats where we store what the current item is and then what the ii current item is. 
	//This will get one of our items from our grid.
	iitem = inv_grid[# 0, ii];
	//This will get whatever the item number is divided by the total number of columns in the items sprite. This will get the x and y position in the inventory.
	sx = (iitem mod spr_inv_items_columns) * cell_size;
	sy = (iitem div spr_inv_items_columns) * cell_size;
	
	//Draws Slots and Items
	//Draws slots coloured that is filled by an item.
	draw_sprite_part_ext(spr_inv_ui, 0, 0, 0, cell_size, cell_size, xx, yy, scale, scale, c_white, 1);
	//Draws the item by the top left for x and y of each cell or slot.
	//Switching to whether to draw item if the player has selected or not selected item.
	//If we get to where ii is equl to the selected slot:
	switch(ii) {
		//In a number of situations you want to let your instances complete an action depending on a particular value. You can do this using a number of consecutive "if" statements but when the possible choices gets above two or three it is usually easier to use the "switch" statement.
		//If the player has selected item 
		case selected_slot:
		if (iitem > 0) draw_sprite_part_ext(
			spr_inv_items, 0, sx, sy, cell_size, cell_size, xx, yy, scale, scale, c_white, 1
		);
		//This effect is to make the sprite item that is hovered over be brighter and blended with the background
		gpu_set_blendmode(bm_add);
		draw_sprite_part_ext(spr_inv_ui, 0, 0, 0, cell_size, cell_size, xx, yy, scale, scale, c_white, 0.3);
		gpu_set_blendmode(bm_normal);
		break;
		
		//If the player has picked up an item in the inventory. Display visually what is coded below.
		case pickup_slot:
		if (iitem > 0) draw_sprite_part_ext(
			spr_inv_items, 0, sx, sy, cell_size, cell_size, xx, yy, scale, scale, c_white, 0.2
		);
		break;
		
		default:
		//Draws item if player has not selected item
		if (iitem > 0) draw_sprite_part_ext(
			spr_inv_items, 0, sx, sy, cell_size, cell_size, xx, yy, scale, scale, c_white, 1
		);
		//The "break" statement is used to end prematurely a for, repeat, while, do... until loop of some kind, or to tell a switch statement to end at that point, or to prematurely end a with call.
		break;
	}
	//Draw item Number
	//If the item number is zero it won't draw the number.
	if(iitem > 0){
		var number = inv_grid[# 1, ii];
		draw_text_color(xx, yy, string(number), c,c,c,c, 1);
	}
	
	//Increment
	//How this works
	//For example if ii is 9, inv_slots_width = 8. We know that 9 goes into 8 once, with 1 remainder.
	//The 'mod' of 9/8 is asking only what the remainder is. So the mod of 9/8 = 1.
	//Same goes for div of iy. Div line will give a row 1.
	ii += 1;
	//mod gives you only the remainder of a division. Note that you can only div or mod using integer values.
	ix = ii mod inv_slots_width;
	//div gives you the amount a value can be divided into producing only an integer quotient.
	iy = ii div inv_slots_width;
}

//Draw Item Description
//Again using a local variable to access a data structure tends to be faster and easier to write repeatedly.
var iinfo_grid = ds_items_info, description = "";
iitem = inv_grid[# 0, selected_slot]; //Pulls item type out of our inventory

if (iitem > 0) {
	draw_set_font(desc_font);
	description = iinfo_grid[# 0, iitem] + ". " + iinfo_grid[# 1, iitem]; //What to print on the screen. Ends the item name in full stop
	c = c_white;
	//Draw text with ext is used so that the text wraps at the end of the inventory UI on the right side.
	draw_text_ext_color(desc_x, desc_y, description, desc_gab_height, desc_length, c, c, c, c, 1);
}



//Picking up a slot with an item, getting item from the grid and displaying sprite and the number of said item.
if (pickup_slot != -1) {
	//This will get one of our items from our grid.
	iitem = inv_grid[# 0, pickup_slot];
	//This will get whatever the item number is divided by the total number of columns in the items sprite. This will get the x and y position in the inventory.
	sx = (iitem mod spr_inv_items_columns) * cell_size;
	sy = (iitem div spr_inv_items_columns) * cell_size;
	if (iitem > 0) draw_sprite_part_ext(
		spr_inv_items, 0, sx, sy, cell_size, cell_size, mousex, mousey, scale, scale, c_white, 1
	);
	//Drawing number beside item that is picked up
	var inum = inv_grid[# 1, pickup_slot];
	draw_text_color(mousex + (cell_size * scale * 0.5), mousey, string(inum), c,c,c,c, 1);
}
#endregion