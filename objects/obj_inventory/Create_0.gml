/// @description Setting Variables

#region Drawing Background Inventory Variables
show_inventory	= false;								//Set whether or not to show the inventory.
gui_width		= display_get_gui_width();				//The width of the display (returns number, example being 1920).
gui_height		= display_get_gui_height();				//The height of the display (returns number, example being 1080).
depth			= -1;									//Setting depth so the inventory UI displays over every thing
scale			= 2;									//Setting scale of the inventory. Setting it to 2 is so that it will double in scale for the viewport. Make sure the camera is half the viewport size if this is set to 2.
spr_inv_ui		= spr_inventory_ui;						//Setting variable to equal inventory UI sprite.
cell_size		= 116;									//Setting the cell size for each slot in the inventory. Size inside the outline of the inventory slot.
inv_ui_width	= 1250;									//The width size of the inventory.
inv_ui_height	= 770;									//The height size of the inventory.
inv_x_offset	= 250;									//The offset value for x horizontal position for inventory.
inv_ui_x		= (gui_width * 0.5) - (inv_ui_width * 0.5 * scale) + inv_x_offset;		//The position of x (horizontal) inventory value.
inv_ui_y		= (gui_height * 0.5) - (inv_ui_height * 0.5 * scale);					//The position of y (vertical) inventory value.
#endregion

#region Drawing Items Variables
spr_inv_items			= spr_inventory_items;									//Setting variable to equal inventory items sprite.
spr_inv_items_columns	= sprite_get_width(spr_inv_items) / cell_size;			//Calculating the amount of columns of items
spr_inv_items_rows		= sprite_get_height(spr_inv_items) / cell_size;			//Calculating the amount of rows in the items sprite
#endregion

#region Drawing Slots and Selected, Pickup item, Variables
//Position of X and Y is on the top left pixel inside the slot window

	#region Inventory Slots
	selected_slot			= 0;										//The current slot in the inventory thats being selected.
	pickup_slot				= -1;										//-1 means the player is not picking up any item. If the player selects an item, the number will change to the item number.
	mouse_slot_x			= 0;										//This gets the current x (horizontal) mouse position.
	mouse_slot_y			= 0;										//This gets the current y (vertical) mouse position.
	inv_slots_x				= inv_ui_x + (67 * scale);					//The x (horizontal) position of the top left slot (aka first slot) for inventory.
	inv_slots_y				= inv_ui_y + (437 * scale);					//The y (vertical) position of the top left slot (aka first slot) for inventory.
	inv_slots				= 10;										//The amount of inventory slots.
	inv_slots_width			= 5;										//The amount of slots horizontally in the inventory.
	inv_slots_height		= 2;										//The amount of slots vertically in the inventory.
	x_buffer				= 38;										//The amount of pixels in the x axis (horizontally) between each inventory slot.
	y_buffer				= 38;										//The amount of pixels in the y axis (vertically) between each inventory slot.
	#endregion

	#region Equipable Inventory Slot | May Not Be Necessary To Declare Separately
	equip_slots_x			= inv_ui_x + (67 * scale);					//The x (horizontal) position of the top left equipped slot for inventory.
	equip_slots_y			= inv_ui_y + (221 * scale);					//The y (vertical) position of the top left equipped slot for inventory.
	equip_inv_slots			= 5;										//The amount of equippable inventory slots.
	equip_inv_slots_width	= 5;										//The amount of slots horizontally in the equippable inventory.
	equip_inv_slots_height	= 1;										//The amount of slots vertically in the equippable inventory.
	#endregion

#endregion

#region Get Player Existing ID & Display Player Information On Inventory UI
budburra		= obj_boy;
hope			= obj_girl;
if instance_exists(budburra) { player = budburra; }
if instance_exists(hope) { player = hope; }

//Example of stats to display on UI and how to do it as a data structure
//0 = GOLD
//1 = SILVER
//2 = BRONZE
//3 = NAME

//Create data structure using function ds_grid_create (ds stands for data structure). The first number is the width (amount of columns horizontally) and second is the height (amount of rows vertically).
//Use excel spreadsheet to organise and create these.
//A ds_grid is basically a type of two-dimensional array. You define the grid by setting its integer width and height, 
//which then sets the total number of "cells" within the grid (the width and height multiplied) 
//and these cells are then used to hold different data values (real numbers or strings).

//IMPORTANT! When you create a data structure, the index value to identify it is an integer value starting at 0. 
//This means that data structures of different types can have the same index value, so if in doubt you should be using the ds_exists function before accessing them. 
//Also note that indices are re-used, so a destroyed data structure index value may be used by a newly created one afterwards.

//NOTE: As with all dynamic resources, data structures take up memory and so should always be destroyed when no longer needed to prevent memory leaks which will slow down and eventually crash your game.
//i in front of random_range means it will be a whole number (int).

/*
ds_player_info = ds_grid_create(2, 4);
ds_player_info[# 0, 0] = "Gold";
ds_player_info[# 0, 1] = "Silver";
ds_player_info[# 0, 2] = "Copper";
ds_player_info[# 0, 3] = "Name";

ds_player_info[# 1, 0] = irandom_range(0,99);
ds_player_info[# 1, 1] = irandom_range(0,99);
ds_player_info[# 1, 2] = irandom_range(0,99);
ds_player_info[# 1, 3] = "Player";
*/
#endregion

#region Allocation Of Inventory Items & Creation Of Slots
//The data structure has two columns, item and the number of said item.
//0 = Item
//1 = Number of items

//Creation of inventory data structure (2 is the number of columns and height is the number of inventory slots).
ds_inventory = ds_grid_create(2, inv_slots);

	#region Every Item
	//Note, make sure the order of items in the enum is the same as the item order in your items sprite (from left to right).
	//Height at the bottom of enum is used to get the number of entrys in a enum since there is no other way to code to get the height.
		enum item {
			none		= 0,
			tomato		= 1,
			potato		= 2,
			carrot		= 3,
			artichoke	= 4,
			chilli		= 5,
			gourd		= 6,
			corn		= 7,
			wood		= 8,
			stone		= 9,
			bucket		= 10,
			chair		= 11,
			picture		= 12,
			axe			= 13,
			potion		= 14,
			starfish	= 15,
			mushroom	= 16,
			height		= 17,
		}
	#endregion

	#region To Add A Item To The Inventory
	/* Example: To add a bucket to a cell
	Add item first
	ds_inventory[# 0, 0] = item.bucket;
	Then the number of said item. In this example five was added.
	ds_inventory[# 1, 0] = 5;
	So there are 5 buckets in first slot of inventory.
	
	This code is to loop through and add random items to each slot
	var yy = 0; repeat(inv_slots){
		ds_inventory[# 0, yy] = irandom_range(1, item.height-1);
		ds_inventory[# 1, yy] = irandom_range(1, 10);
	
		yy += 1;
	}
	*/
	#endregion

#endregion

#region Description Of Inventory Item Variables & Data Structure Intialisation
desc_x				= inv_slots_x;						//Setting description x position anchored on top left
desc_y				= inv_ui_y + (45 * scale);			//Setting description y position anchored on top left
desc_gab_height		= string_height("M");				//Sets the description height gab between lines.
desc_length			= inv_ui_width * scale - 432;		//Setting description length across the inventory ui.
desc_font			= fnt_dialogue;						//Sets font for description of item in inventory.
ds_items_info		= ds_grid_create(2, item.height);	//Creating items info data structure

	#region Item Names
	//These temporary variables are used so that i increments by 1 once code is executed. Better than just numbering down.
	var z = 0, i = 0;

	ds_items_info[# z, i++] = "Nothing";
	ds_items_info[# z, i++] = "Tomato";
	ds_items_info[# z, i++] = "Potato";
	ds_items_info[# z, i++] = "Carrot";
	ds_items_info[# z, i++] = "Artichoke";
	ds_items_info[# z, i++] = "Chilli";
	ds_items_info[# z, i++] = "Gourd";
	ds_items_info[# z, i++] = "Corn";
	ds_items_info[# z, i++] = "Wood";
	ds_items_info[# z, i++] = "Stone";
	ds_items_info[# z, i++] = "Bucket";
	ds_items_info[# z, i++] = "Chair";
	ds_items_info[# z, i++] = "Picture";
	ds_items_info[# z, i++] = "Axe";
	ds_items_info[# z, i++] = "Potion";
	ds_items_info[# z, i++] = "Starfish";
	ds_items_info[# z, i++] = "Mushroom";
	#endregion

	#region Item Descriptions
	//These temporary variables are used so that i increments by 1 once code is executed. Better than just numbering down.
	var z = 1, i = 0;

	ds_items_info[# z, i++] = "Empty";
	ds_items_info[# z, i++] = "Do-Do";
	ds_items_info[# z, i++] = "SohCaNiki";
	ds_items_info[# z, i++] = "Poo";
	ds_items_info[# z, i++] = "Dance";
	ds_items_info[# z, i++] = "Hello";
	ds_items_info[# z, i++] = "ManMan";
	ds_items_info[# z, i++] = "Fan";
	ds_items_info[# z, i++] = "Dan";
	ds_items_info[# z, i++] = "Wheres My Pan";
	ds_items_info[# z, i++] = "God Damn";
	ds_items_info[# z, i++] = "Lamb";
	ds_items_info[# z, i++] = "Bam";
	ds_items_info[# z, i++] = "Yo!";
	ds_items_info[# z, i++] = "Bro";
	ds_items_info[# z, i++] = "Ko";
	ds_items_info[# z, i++] = "Do Do";
	#endregion
	
#endregion