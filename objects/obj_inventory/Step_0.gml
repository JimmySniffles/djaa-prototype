/// @description Inventory Selecting & Stacking

//If show inventory is false, exit and not execute the following code.
if(show_inventory = false) exit;

#region Mouse Selection Variables & Functionality
//The device_mouse_x/y_to_gui() returns the x or y position of the mouse relative to the GUI layer when pressed.
mousex = device_mouse_x_to_gui(0);
mousey = device_mouse_y_to_gui(0);
//To get mouse functionality you got to break up the mouse's position into increments of the list such as for this, increments of cell size plus buffer.

//A temporary variable getting the total number of cell size plus buffer amount for x and y.
var cell_xbuff = (cell_size + x_buffer) * scale;
var cell_ybuff = (cell_size + y_buffer) * scale;

//This is a temporary variable for getting the mouse's x and y position on inventory slots.
//If these values are larger than zero than we know that the mouse is in the lower section cutting out the top and left blank sections of the inventory.
var i_mouse_x = mousex - inv_slots_x;
var i_mouse_y = mousey - inv_slots_y;

//Div is used to get a whole number not a decimal.
//nx and ny gets the coordinates on the slot that the player is currently in
var nx = i_mouse_x div cell_xbuff
var ny = i_mouse_y div cell_ybuff

//Setting it so that the selection using the mouse only works within the inventory UI
//nx being zero is the very start that it can be
var mouse_in_inventory = true; //Checks whether the mouse is in the inventory UI
if (nx >= 0 && nx < inv_slots_width && ny >= 0 && ny < inv_slots_height && i_mouse_x > 0 && i_mouse_y > 0) 
{
	//This is so that the buffer between each inventory slot does not change when mouse hovers between slots
	//This will take away that rounded down value from div earlier so we get the raw mouse position on that slot. Then we just got to check if it is less than the slot size then we know that it is within the boundary of the slot and not in the boundary of the buffer.
	var sx = i_mouse_x - (nx * cell_xbuff);
	var sy = i_mouse_y - (ny * cell_ybuff);
	
	if ((sx < cell_size * scale) && (sy < cell_size * scale)) {
		//Setting mouse_slot to equal nx and ny
		mouse_slot_x = nx;
		mouse_slot_y = ny;
	}
} else { mouse_in_inventory = false; }

//Set selected slot to mouse position. This gives the slot we are currently selecting. This gives the number of slots or inventory items.
selected_slot = min(inv_slots-1, mouse_slot_x + (mouse_slot_y * inv_slots_width));
#endregion

#region Stacking Variables & Functionality
//Picking up item
//Temporary variable to access data structure.
var inv_grid = ds_inventory;
//ss stands for selected slot. Gets the actual item that the player is selecting.
var ss_item = inv_grid[# 0, selected_slot];
//Checks whether to create notification. Primarily for dropping items.
var create_notification = false;

//Else if is for when you pick something up you must decide what you got to do with it before picking up another one.
//This is to drop something down once picked up
if (pickup_slot != -1) { //Checks whether a pick up slot is not empty
	if (!mouse_in_inventory) {
		//Drop item into game world if the mouse is dragged outside of the inventory UI	
		var create_notification = true;
		
		//Reduce item number by 1
		var pitem = inv_grid[# 0, pickup_slot]; //Pick item
		var in = pitem;
		
		inv_grid[# 1, pickup_slot] -= 1;
		
		//Destroy item in inventory if it was the last one. Setting pickup to -1 to say now the pick up slot is empty we are not picking anything up.
			if (inv_grid[# 1, pickup_slot] == 0) { inv_grid[# 0, pickup_slot] = item.none; pickup_slot = -1; }
		
			//Create Item In Game World
			var inst = instance_create_layer(player.x, player.y, "Items", obj_items);
				with (inst) {
					//Even though with a 'with' statement you cannot access the calling objects variables you can still use local variables.
					item_num = pitem;
					//Reminder mod function gives you the remainder of a division eg. 8 mod 5 = 3 while div function gives us the division without a remainder eg. 8 div 5 = 1.
					//If our item num (item number) is 1: x_frame = 1 mod (number of items), x_frame = 1.
					x_frame = item_num mod (spr_items_width/ cell_size); //This is going to get us the number of frames that are in the width of the item sprite sheet.
					//if our item_num is 1: x_frame = 1 div (number of items), x_frame = 0.
					y_frame = item_num div (spr_items_width/ cell_size);		
				}
	}
	else if (mouse_check_button_pressed(mb_left)) {
		//This will add the selected item to the empty slot.
		if (ss_item == item.none) {
			//In this case then we want to change the contents of the inventory grid to be our pickup item.
			inv_grid[# 0, selected_slot] = inv_grid[# 0, pickup_slot]; //Item
			inv_grid[# 1, selected_slot] = inv_grid[# 1, pickup_slot]; //Number of items
			
			//Remove entry from where we picked it up. Setting it to nothing for item is item.none (which is zero) and number of items is zero.
			inv_grid[# 0, pickup_slot] = item.none; //Item
			inv_grid[# 1, pickup_slot] = 0; //Number of items
			
			//Have to change the pickup_slot variable back to -1 because the player is no longer picking anything up.
			pickup_slot = -1;
		}
		//Checks if the selected slot the player is on is equal to the same type of item our pickup slot is currently holding. 
		//Than we stack on top of each other.
		else if (ss_item == inv_grid[# 0, pickup_slot]) {
			//There are two possibilities, either be putting down the slot to the original spot that we picked it up from than we just got to turn off that the player is picking it up.
			//Or its a different slot with the same item then we want to stack on top of it.
				if (selected_slot != pickup_slot) {
				//This is for the second possibility of moving a picked up slot to a same type then stacking it by adding.
				inv_grid[# 1, selected_slot] += inv_grid[# 1, pickup_slot]; //Number of items
			
				//Remove entry from where we picked it up. Setting it to nothing for item is item.none (which is zero) and number of items is zero.
				inv_grid[# 0, pickup_slot] = item.none; //Item
				inv_grid[# 1, pickup_slot] = 0; //Number of items
				}
			//This is for the first possibility
			pickup_slot = -1;
		}
		//If the player is on a slot that has something in it but its not the same item type.
		else {
			//What this will do is that if the player select another slot the picked up item is placed back down to its original spot and the new item is picked up.
			//Before we set it we got to save the number of items first.
			var ss_item_num = inv_grid[# 1, selected_slot];
			inv_grid[# 0, selected_slot] = inv_grid[# 0, pickup_slot]; //Item
			inv_grid[# 1, selected_slot] = inv_grid[# 1, pickup_slot]; //Number of items
			
			//Setting it to the item and the number of items
			inv_grid[# 0, pickup_slot] = ss_item; //Item
			inv_grid[# 1, pickup_slot] = ss_item_num; //Number of items
			
			//Have to change the pickup_slot variable back to -1 because the player is no longer picking anything up.
			//However perhaps you want it so that if you place an item down such as a spear on a boomerang, the boomerang contents will go to the spear content slot but you will pick up that boomerang. If so disable the pickup_slot = -1; code.
			//pickup_slot = -1;
		}	
	}
}
//Checks if it is an item that we can pick up in the inventory slot. Could make it so that the player cannot pickup certain items from the inventory.
else if (ss_item != item.none) {
	//Drop item. Leave it as a press so you don't keep dropping items when holding the button unless you want that
	if (mouse_check_button_pressed(mb_left)) {
		var create_notification = true;
		var in = inv_grid[# 0, selected_slot];
		//Reduce item number by 1
		inv_grid[# 1, selected_slot] -= 1;
		//Destroy item in inventory if it was the last one
		if (inv_grid[# 1, selected_slot] == 0) { inv_grid[# 0, selected_slot] = item.none; }
		
		//Create Item In Game World
		var inst = instance_create_layer(player.x, player.y, "Items", obj_items);
		with (inst) {
			//Even though with a 'with' statement you cannot access the calling objects variables you can still use local variables.
			item_num = ss_item;
			//Reminder mod function gives you the remainder of a division eg. 8 mod 5 = 3 while div function gives us the division without a remainder eg. 8 div 5 = 1.
			//If our item num (item number) is 1: x_frame = 1 mod (number of items), x_frame = 1.
			x_frame = item_num mod (spr_items_width/ cell_size); //This is going to get us the number of frames that are in the width of the item sprite sheet.
			//if our item_num is 1: x_frame = 1 div (number of items), x_frame = 0.
			y_frame = item_num div (spr_items_width/ cell_size);		
		}
	}
	
	//Drop pickup item into new slot
	if (mouse_check_button_pressed(mb_left)) {
		pickup_slot = selected_slot;		
	}
}

	#region Create Notification
	if (create_notification) {
	//Creation Notification
		if (!instance_exists(obj_notification)) { instance_create_layer(0,0, "instances", obj_notification) }
			
			with (obj_notification) {
				//Check if data structure exists and if not create one
				if (!ds_exists(ds_notifications, ds_type_grid)) {
					ds_notifications = ds_grid_create(2, 1);
					var noti_grid = ds_notifications;
					noti_grid[# 0, 0] = -1;
					noti_grid[# 1, 0] = obj_inventory.ds_items_info[# 0, in];
				}
				else {
					event_perform(ev_other, ev_user0);
					//Add to the grid
					var noti_grid = ds_notifications;
					
					var grid_height = ds_grid_height(noti_grid);
					var name = obj_inventory.ds_items_info[# 0, in];
					var in_grid = false; //Checks if we are in the grid.
					
					var yy = 0; repeat(grid_height) {
						//Checking second column of ds list. The name of the item to find.
						if (name == noti_grid[# 1, yy]) {
							noti_grid[# 0, yy] -= 1; //Adding 1 to current count.
							in_grid = true;
							break;
						}
						yy++;
					}
					//Checks if were not in a grid.
					if (!in_grid) {
						ds_grid_resize(noti_grid, 2, grid_height + 1);
						noti_grid[# 0, grid_height] = -1;
						noti_grid[# 1, grid_height] = obj_inventory.ds_items_info[# 0, in];
						
					}
				}
		}
	}
#endregion
#endregion