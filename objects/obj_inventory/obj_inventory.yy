{
    "id": "95f0e1c2-dacd-48ea-ba98-81b78b76134c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory",
    "eventList": [
        {
            "id": "a9e5f4ca-44ba-4a9a-9d99-4788b3f7bc76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "95f0e1c2-dacd-48ea-ba98-81b78b76134c"
        },
        {
            "id": "06cab319-7aa5-4ac8-be30-09b7e5d1fc11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95f0e1c2-dacd-48ea-ba98-81b78b76134c"
        },
        {
            "id": "0289cd17-6431-418d-b03f-262af2d23100",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "95f0e1c2-dacd-48ea-ba98-81b78b76134c"
        },
        {
            "id": "6fc4b0d1-b863-407f-89ea-bc397222b110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95f0e1c2-dacd-48ea-ba98-81b78b76134c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}