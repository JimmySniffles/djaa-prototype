/// @description Movement of item once droppped

#region Droping Item To A Certain Position At Random At A Given Speed
if (drop_move) {
	//The lerp function will return a number that is the position between two values for a given percentage.
	//If we lerp 0 and 10 at 40% (0.4), we get the value 4. We can keep 'lerping' a value towards another to make it 'smoothly' approach the new value.
	x = lerp(x, spr_target_x, itemmove_spd);
	y = lerp(y, spr_target_y, itemmove_spd);
	//This function returns the absolute value of the input argument, so if it's a positive value then it will remain the same, but if it's negative it will be multiplied by -1 to make it positive.
	//If the difference between the item's x and y position and target x and y position is less than 1 than its treated as if its meant its target position.
	if (abs(x - spr_target_x) < 1 && abs(y - spr_target_y) < 1) { drop_move = false; }
}
#endregion

#region Pick Up Item
//If not pressing the E key do not do the following code.

if (!keyboard_check(ord("E"))) exit;
//Apparently rectangle collisions seem to be more performance friendly.
var playerx = player.x;
var playery = player.y;
var radius = 32;
if (point_in_rectangle(playerx,playery,x - radius, y - radius, x + radius, y + radius)) {
	radius = 5;
	if (!point_in_rectangle(playerx,playery,x - radius, y - radius, x + radius, y + radius)) {
		//Move towards the player for pickup
		x = lerp(x, playerx, itemmove_spd);
		y = lerp(y, playery, itemmove_spd);
	}
	else {
		//Is item touching the player	
		//Need to make item number a local variable because the rest will jump into the inventory itself. Remember local variables can go in 'with' statements.
		var in = item_num;
		
		with (obj_inventory) {
			//Check if item exists in inventory already.
			var ds_inv = ds_inventory;	//inventory data structure grid
			var picked_up = false;		//If we find that its okay to pick up an item this is set to true other its set to false.
			
			//To check if an item exists in an inventory we got to loop through it
			var yy = 0; repeat(inv_slots) {
				//Checks if an item exists. If there multiple of the same object than it will stack to the first one.
				if (ds_inv[# 0, yy] == in) {
					ds_inv[# 1, yy] += 1;
					picked_up = true;
					break;
					} else {
					//If its not true then this will keep looping through the grid.
					yy += 1;	
					}
				}
				//Otherwise, add item to an empty slot if there is one
				if (!picked_up) { 
					yy = 0; repeat(inv_slots) {
						if (ds_inv[# 0, yy] == item.none) {
							ds_inv[# 0, yy] = in;
							ds_inv[# 1, yy] += 1;
							picked_up = true;
							break;
						} else {
							//If its not true then this will keep looping through the grid.
							yy += 1;	
						}
					}
				}
				//If no slots are available, the item will not be picked up.
			}
		#region Display Notification & Destroy item if picked up by player
		if (picked_up) {
			// Creation Notification
			if (!instance_exists(obj_notification)) { instance_create_layer(0,0, "instances", obj_notification) }
			
			var in = item_num;
			with (obj_notification) {
				//Check if data structure exists and if not create one
				if (!ds_exists(ds_notifications, ds_type_grid)) {
					ds_notifications = ds_grid_create(2, 1);
					var noti_grid = ds_notifications;
					noti_grid[# 0, 0] = 1;
					noti_grid[# 1, 0] = obj_inventory.ds_items_info[# 0, in];
				}
				else {
					event_perform(ev_other, ev_user0);
					//Add to the grid
					var noti_grid = ds_notifications;
					
					var grid_height = ds_grid_height(noti_grid);
					var name = obj_inventory.ds_items_info[# 0, in];
					var in_grid = false; //Checks if we are in the grid.
					
					var yy = 0; repeat(grid_height) {
						//Checking second column of ds list. The name of the item to find.
						if (name == noti_grid[# 1, yy]) {
							noti_grid[# 0, yy] += 1; //Adding 1 to current count.
							in_grid = true;
							break;
						}
						
						yy++;
					}
					//Checks if were not in a grid.
					if (!in_grid) {
						ds_grid_resize(noti_grid, 2, grid_height + 1);
						noti_grid[# 0, grid_height] = 1;
						noti_grid[# 1, grid_height] = obj_inventory.ds_items_info[# 0, in];
						
					}
				}
				
			}
			instance_destroy();		
		}
		#endregion
	}
}
#endregion