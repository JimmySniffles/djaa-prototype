/// @description Item In Game World Variables

#region Setting Base Variables
cell_size			= 116;									//Setting the cell size for each slot in the item cell. Must be same size as inventory cell_size.
spr_items			= spr_inventory_items;					//Setting variable containing inventory items sprites.
spr_items_width		= sprite_get_width(spr_items);			//Getting the item sprite width.
spr_items_height	= sprite_get_height(spr_items);			//Getting the item sprite height.
x_offset			= cell_size * 0.5;						//Setting the x (horizontal) offset for cell size of the item to be drawned.
y_offset			= cell_size;							//Setting the y (vertical) offset for cell size of the item to be drawned.
item_num			= -1;									//This is for when you want to place an item into the game world. So you want to specifiy what kind of item its going to be with the item number from inventory.
x_frame				= 0;									//The X position of each frame. So for example the second image on the top across the items sprite sheet whould be on the third x frame because of the no sprite gab at the beginning top left but also zero on y frame because its on the top.
y_frame				= 0;									//The Y position of each frame.
#endregion

#region Check Which Character The Player Is Controlling
budburra			= obj_boy;
hope				= obj_girl;
if instance_exists(budburra) { player = budburra; }
if instance_exists(hope) { player = hope; }
#endregion

#region Have The Dropped Item Fly In Random Direction
drop_move			= true;									//Intialally set to true. It will say if we just dropped it will be true and move towards target and once we reach the target x and y position its set to false.
itemmove_spd		= 0.1;									//Sets the speed for the item to move towards target using lerp function in step event.
var itemdir			= irandom(359);							//Random Direction Set.
var len				= 32;									//Radius distance the object will fly towards.
spr_target_x		= x + lengthdir_x(len, itemdir);		//Target x position of where the item sprite will end up. Lengthdir functions give us the horizontal and vertical components of the vector we have defined with our item direction and length.
spr_target_y		= y + lengthdir_y(len, itemdir);		//Target y position of where the item sprite will end up
#endregion