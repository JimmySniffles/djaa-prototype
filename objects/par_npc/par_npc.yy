{
    "id": "9e01effd-2be6-4d1c-9943-be65d50560b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_npc",
    "eventList": [
        {
            "id": "de64852c-ca75-4546-a390-9cd53c87957c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e01effd-2be6-4d1c-9943-be65d50560b2"
        },
        {
            "id": "58ee580b-d973-4a71-a918-169562363954",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e01effd-2be6-4d1c-9943-be65d50560b2"
        },
        {
            "id": "b236be01-1efd-4aad-ae33-0d640065a1a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9e01effd-2be6-4d1c-9943-be65d50560b2"
        },
        {
            "id": "772e2c68-7923-4d33-b016-664c6f15135b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9e01effd-2be6-4d1c-9943-be65d50560b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bacfa89f-a1ce-4276-93c3-7cba4cb8ddb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c62d3cad-c8c7-428f-ad92-653990b08148",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "\"standing\"",
                "\"sitting\""
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"standing\"",
            "varName": "state",
            "varType": 6
        },
        {
            "id": "5ddca98d-5aa7-4fa6-9444-2c719d89a1c2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "tut_text",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}