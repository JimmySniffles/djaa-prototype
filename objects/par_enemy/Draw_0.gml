/// @description Drawing Self & Stats
draw_self();
if (state != e_states.death) { scr_stats_display(x-hp_disp_x, y-hp_disp_y, x-energy_disp_x, y-energy_disp_y, x-breath_disp_x, y-breath_disp_y); }

#region Flash Effects
//Invincibility flash pulse effect
if (whiteflash > 0) && (state != e_states.death) { scr_shader_flash(0.05,1.0,1.0,1.0); }

//Death flash & fade effect
if (whiteflash > 0) && (state == e_states.death) { scr_shader_flash(0.01,1.0,1.0,1.0); }

if (redflash > 0) { scr_shader_flash(0.05,1.0,0,0); }
#endregion