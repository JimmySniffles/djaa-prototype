/// @description Setting Enemies Attack Stats & Setting Intial X Position

#region Checking Which Player Exists, Saving Original X & Y Positions, Setting hitbyattack DS List, Setting Direction & X Anchor Range
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }
hitbyattack = ds_list_create();								//This variable keeps DS List of objects that are attacked.

dir = 0;													//Direction of the enemy. 1 is direction to right, -1 is to the left.
returnable_to_x_anchor = 0;									//Checks if returnable to original x position upon intialisation.
#endregion

#region Setting Enemy Type & Enemies Weapons Types
enum e_type_wep {
	grunt_club		= 0,
	grunt_spear		= 1,
	grunt_boomerang	= 2,
	magic			= 3,
	height
}	
#endregion

#region Setting Enemy Type & Enemies Weapons Types Stats As DS Map

	#region FoN Grunt Weapons Stats
	
		#region Club
		enemy_attack[e_type_wep.grunt_club] = ds_map_create();
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "ply_dmg",15);				//The damage of the weapon to the player
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "ani_dmg",20);				//The damage of the weapon to animals
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "env_dmg",25);				//The damage of the weapon to environment objects
		
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "attack_spd",0.7);			//The attack speed
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "attack_energy",7);			//The amount of energy used for the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "stun",false);				//States whether or not the weapon can stun
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "knockback",true);			//States whether or not the weapon can knockback
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "knockback_str",10);		//The amount of strength of the knockback
		
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "scrshk_mag",2);			//The amount of screenshake magnitude
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "scrshk_frm",10);			//The amount of screenshake frames
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "ply_particles",2);			//The amount of particles on attack impact with player
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "ani_particles",2);			//The amount of particles on attack impact with animal
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "env_particles",3);			//The amount of particles on attack impact with environment
		
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "projectile",-1);			//The name of the projectile you shoot. -1 if there is no projectile of the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "proj_spd",0);				//The speed of the projectile.
		ds_map_add(enemy_attack[e_type_wep.grunt_club], "proj_dist",0);				//The max distance of the projectile.
		#endregion
	
		#region Spear
		enemy_attack[e_type_wep.grunt_spear] = ds_map_create();
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "ply_dmg",20);				//The damage of the weapon to the player
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "ani_dmg",25);				//The damage of the weapon to animals
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "env_dmg",15);				//The damage of the weapon to environment objects
		
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "attack_spd",0.6);			//The attack speed
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "attack_energy",9);		//The amount of energy used for the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "stun",false);				//States whether or not the weapon can stun
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "knockback",true);			//States whether or not the weapon can knockback
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "knockback_str",8);		//The amount of strength of the knockback
		
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "scrshk_mag",1);			//The amount of screenshake magnitude
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "scrshk_frm",15);			//The amount of screenshake frames
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "ply_particles",3);		//The amount of particles on attack impact with player
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "ani_particles",3);		//The amount of particles on attack impact with animal
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "env_particles",2);		//The amount of particles on attack impact with environment
		
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "projectile",-1);			//The name of the projectile you shoot. -1 if there is no projectile of the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "proj_spd",0);				//The speed of the projectile.
		ds_map_add(enemy_attack[e_type_wep.grunt_spear], "proj_dist",0);			//The max distance of the projectile.
		#endregion
		
		#region Boomerang
		enemy_attack[e_type_wep.grunt_boomerang] = ds_map_create();
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "ply_dmg",10);			//The damage of the weapon to the player
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "ani_dmg",15);			//The damage of the weapon to animals
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "env_dmg",10);			//The damage of the weapon to environment objects
		
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "attack_spd",0.7);		//The attack speed
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "attack_energy",5);	//The amount of energy used for the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "stun",true);			//States whether or not the weapon can stun
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "knockback",false);	//States whether or not the weapon can knockback
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "knockback_str",0);	//The amount of strength of the knockback
		
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "scrshk_mag",1);		//The amount of screenshake magnitude
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "scrshk_frm",10);		//The amount of screenshake frames
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "ply_particles",1);	//The amount of particles on attack impact with player
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "ani_particles",1);	//The amount of particles on attack impact with animal
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "env_particles",1);	//The amount of particles on attack impact with environment
		
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "projectile",obj_egrunt_boomerang);		//The name of the projectile you shoot. -1 if there is no projectile of the attack
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "proj_spd",10);		//The speed of the projectile.
		ds_map_add(enemy_attack[e_type_wep.grunt_boomerang], "proj_dist",400);		//The max distance of the projectile.
		#endregion
	
	#endregion

	#region FoN Mage Weapon Stats

	#endregion

	#region FoN Brute Weapon Stats

	#endregion

#endregion