/// @description Setting Animal Attack Stats

#region Checking Which Player Exists, Saving Original X & Y Positions, Setting hitbyattack DS List, Setting Direction & X Anchor Range
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }
hitbyattack = ds_list_create();								//This variable keeps DS List of objects that are attacked.

dir = 0;													//Direction of the animal. 1 is direction to right, -1 is to the left.
returnable_to_x_anchor = 0;									//Checks if returnable to original x position upon intialisation.
#endregion

#region Setting Animal Types
enum a_type {
	kangaroo	= 0,
	snake		= 1,
	snake_boss	= 2,
	height	
}
#endregion

#region Setting Animal Type Attack Stats As DS Map

	#region Kangaroo
	animal_attack[a_type.kangaroo] = ds_map_create();
	ds_map_add(animal_attack[a_type.kangaroo], "ply_dmg",20);				//The damage of the attack to the player
	ds_map_add(animal_attack[a_type.kangaroo], "ene_dmg",20);				//The damage of the attack to enemies
	ds_map_add(animal_attack[a_type.kangaroo], "ani_dmg",20);				//The damage of the attack to animals
		
	ds_map_add(animal_attack[a_type.kangaroo], "attack_spd",0.8);			//The attack speed
	ds_map_add(animal_attack[a_type.kangaroo], "attack_energy",10);			//The amount of energy used for the attack
	ds_map_add(animal_attack[a_type.kangaroo], "stun",false);				//States whether or not the attack can stun
	ds_map_add(animal_attack[a_type.kangaroo], "knockback",true);			//States whether or not the attack can knockback
	ds_map_add(animal_attack[a_type.kangaroo], "knockback_str",15);			//The amount of strength of the knockback
		
	ds_map_add(animal_attack[a_type.kangaroo], "scrshk_mag",2);				//The amount of screenshake magnitude
	ds_map_add(animal_attack[a_type.kangaroo], "scrshk_frm",10);			//The amount of screenshake frames
	ds_map_add(animal_attack[a_type.kangaroo], "ply_particles",3);			//The amount of particles on attack impact with player
	ds_map_add(animal_attack[a_type.kangaroo], "ene_particles",3);			//The amount of particles on attack impact with enemy
	ds_map_add(animal_attack[a_type.kangaroo], "ani_particles",3);			//The amount of particles on attack impact with animal
	
	ds_map_add(animal_attack[a_type.kangaroo], "projectile",-1);			//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(animal_attack[a_type.kangaroo], "proj_spd",0);				//The speed of the projectile.
	ds_map_add(animal_attack[a_type.kangaroo], "proj_dist",0);				//The max distance of the projectile.
	#endregion
	
	#region Snake
	animal_attack[a_type.snake] = ds_map_create();
	ds_map_add(animal_attack[a_type.snake], "ply_dmg",15);					//The damage of the attack to the player
	ds_map_add(animal_attack[a_type.snake], "ene_dmg",15);					//The damage of the attack to enemies
	ds_map_add(animal_attack[a_type.snake], "ani_dmg",10);					//The damage of the attack to animals
		
	ds_map_add(animal_attack[a_type.snake], "attack_spd",1);				//The attack speed
	ds_map_add(animal_attack[a_type.snake], "attack_energy",8);				//The amount of energy used for the attack
	ds_map_add(animal_attack[a_type.snake], "stun",false);					//States whether or not the attack can stun
	ds_map_add(animal_attack[a_type.snake], "knockback",true);				//States whether or not the attack can knockback
	ds_map_add(animal_attack[a_type.snake], "knockback_str",5);				//The amount of strength of the knockback
		
	ds_map_add(animal_attack[a_type.snake], "scrshk_mag",1);				//The amount of screenshake magnitude
	ds_map_add(animal_attack[a_type.snake], "scrshk_frm",10);				//The amount of screenshake frames
	ds_map_add(animal_attack[a_type.snake], "ply_particles",2);				//The amount of particles on attack impact with player
	ds_map_add(animal_attack[a_type.snake], "ene_particles",2);				//The amount of particles on attack impact with enemy
	ds_map_add(animal_attack[a_type.snake], "ani_particles",1);				//The amount of particles on attack impact with animal
		
	ds_map_add(animal_attack[a_type.snake], "projectile",-1);				//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(animal_attack[a_type.snake], "proj_spd",0);					//The speed of the projectile.
	ds_map_add(animal_attack[a_type.snake], "proj_dist",0);					//The max distance of the projectile.
	#endregion
		
	#region Snake Boss
	animal_attack[a_type.snake_boss] = ds_map_create();
	ds_map_add(animal_attack[a_type.snake_boss], "ply_dmg",30);				//The damage of the attack to the player
	ds_map_add(animal_attack[a_type.snake_boss], "ene_dmg",30);				//The damage of the attack to enemies
	ds_map_add(animal_attack[a_type.snake_boss], "ani_dmg",30);				//The damage of the attack to animals
		
	ds_map_add(animal_attack[a_type.snake_boss], "attack_spd",0.5);			//The attack speed
	ds_map_add(animal_attack[a_type.snake_boss], "attack_energy",10);		//The amount of energy used for the attack
	ds_map_add(animal_attack[a_type.snake_boss], "stun",false);				//States whether or not the attack can stun
	ds_map_add(animal_attack[a_type.snake_boss], "knockback",true);			//States whether or not the attack can knockback
	ds_map_add(animal_attack[a_type.snake_boss], "knockback_str",25);		//The amount of strength of the knockback
		
	ds_map_add(animal_attack[a_type.snake_boss], "scrshk_mag",2);			//The amount of screenshake magnitude
	ds_map_add(animal_attack[a_type.snake_boss], "scrshk_frm",15);			//The amount of screenshake frames
	ds_map_add(animal_attack[a_type.snake_boss], "ply_particles",4);		//The amount of particles on attack impact with player
	ds_map_add(animal_attack[a_type.snake_boss], "ene_particles",4);		//The amount of particles on attack impact with enemy
	ds_map_add(animal_attack[a_type.snake_boss], "ani_particles",4);		//The amount of particles on attack impact with animal
		
	ds_map_add(animal_attack[a_type.snake_boss], "projectile",-1);			//The name of the projectile you shoot. -1 if there is no projectile of the attack
	ds_map_add(animal_attack[a_type.snake_boss], "proj_spd",0);				//The speed of the projectile.
	ds_map_add(animal_attack[a_type.snake_boss], "proj_dist",0);			//The max distance of the projectile.
	#endregion
	
#endregion