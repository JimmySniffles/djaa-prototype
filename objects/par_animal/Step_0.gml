/// @description Switching States & General Mechanics

#region States The Animal Can Switch To
switch (state)
{
	case a_states.normal:	scr_animal_normal(); break;		//Normal state for the animal
	case a_states.alert:	scr_animal_alert();  break;		//Alert state for the animal
	case a_states.scared:	scr_animal_scared(); break;		//Scared state for the animal
	case a_states.chase:	scr_animal_chase();	 break;		//Chase state for the animal
	case a_states.attack:	scr_animal_attack(); break;		//Attack state for the animal
	case a_states.death:	scr_animal_death();	 break;		//Death state for the animal
}
#endregion

#region Setting Direction Animation Of Animal
if (hsp != 0) { image_xscale = sign(hsp); }
#endregion

#region Stating What Happens When Health, Energy & Breath Is Equal Too Or Less Than Zero
if (hp <= 0) { state = a_states.death; }
if (energy <= 0) { moveable = false; }
if (breath <= 0) { hp = 0; }
#endregion

#region Disappear UI Of Health, Energy and Breath When Reached Maximum Amount
if (show_ui_hp) && (hp = max_hp) { show_ui_hp = false; }
if (show_ui_energy) && (energy = max_energy) { show_ui_energy = false; }
if (show_ui_breath) && (breath = max_breath) { show_ui_breath = false; }
#endregion

#region Health Recovery
if (!takingdamage) { if (hp < max_hp) { hp_recovery_timer -= 1; } }

if (hp_recovery_timer <= 0) && (!takingdamage) {
	//Add 10% of hp if hp is less than or equal to 90%
	if (hp <= max_hp - (max_hp * 0.1)) { hp += max_hp * 0.1; show_ui_hp = true; } 
	//Sets hp to maximum hp if above 90% and less than 100% of maximum hp
	else { if (hp > max_hp - (max_hp * 0.1)) && (hp < max_hp) { hp = max_hp; show_ui_hp = true; } }
	hp_recovery_timer = max_hp_recovery_timer;
}
#endregion

#region Energy Running Degradation, Checking Low Energy & Recovery
if (energy < max_energy) || (show_ui_energy) { energy_recovery_timer -= 1; }

if (energy_recovery_timer <= 0) {
	if (state != a_states.normal) && (hsp != 0) { energy -= e_runuse; show_ui_energy = true; }

	if (sprite_index == ani_idle) || (hsp == 0) { 
		//Add 15% of energy if energy is less than or equal to 85%
		if (energy <= max_energy - (max_energy * 0.15)) { energy += max_energy * 0.15; show_ui_energy = true; } 
		//Sets energy to maximum energy if above 85% and less than 100% of maximum energy
		else { if (energy > max_energy - (max_energy * 0.15)) && (energy < max_energy) { energy = max_energy; show_ui_energy = true; } }
	}
	energy_recovery_timer = max_energy_recovery_timer;
}

	#region Low Energy Check 
	if (energy < (max_energy * 0.1))
	{
		if (distance_to_object(player) < hearingrange) {
			if (!audio_is_playing(snd_running_v1)) && (!audio_is_playing(snd_running_v2)) {
				audio_play_sound(choose(snd_running_v1,snd_running_v2),5,false);
			}
		}
	}
	#endregion
	
#endregion

#region Breath Recovery
if (is_water_surface_collision) && (inandoutofwater = 1) { inandoutofwater = 2; }
if (inandoutofwater = 2) { breath_recovery_timer -= 1; }
if (breath = max_breath) { inandoutofwater = 0; }

if (breath_recovery_timer <= 0) {
	//Add 10% of breath if breath is less than or equal to 90% and set breath degrade timer to maximum breath timer
	if (breath <= max_breath - (max_breath * 0.1)) { breath += max_breath * 0.1; breath_timer = max_breath_timer; show_ui_breath = true; } 
	//Sets breath to maximum breath if above 90% and less than 100% of maximum breath
	else { if (breath > max_breath - (max_breath * 0.1)) && (breath < max_breath) { breath = max_breath; breath_timer = max_breath_timer; show_ui_breath = true; } } 
	breath_recovery_timer = max_breath_recovery_timer;
} 
#endregion

#region Taking Damage
if (takingdamage)
{
	if (instance_exists(player)) {
		if (distance_to_object(player) <= hearingrange) {
			if (!invincible) {
				if (scr_chance(0.75)) {
					audio_play_sound(choose(snd_takinghit_v1,snd_takinghit_v2,snd_takinghit_v3),5,false);
				}
				redflash = 1;
				show_ui_hp = true;
			} else {
				whiteflash = 1;
			}
		}
	}
	takingdamage = false;
}
#endregion