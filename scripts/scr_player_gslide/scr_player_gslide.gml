///@description Players Ground Slide State

#region Setting Movement
if (hsp >= 1) { hsp = scr_approach(hsp,gslide_spd,gslide_fric); } else { hsp = scr_approach(hsp,-gslide_spd,gslide_fric); }
onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0); onplatform = (place_meeting(x,y+1,par_oneway_platform)); onobject = (place_meeting(x,y+1,par_objects));
#endregion

#region Ground Slide Animation, Sounds & Effects
	//Setting sprite animation
	sprite_index = ani_gslide;
	image_speed = gslide_ani_spd;
	
	//The Ground Slide Visual Effects & Sound
	if (image_index > 2) && (image_index < 17)
	{
		instance_create_layer(x,bbox_bottom,"Effects",obj_dirtslide);
		if (!audio_is_playing(sn_slide_v1)) && (!audio_is_playing(sn_slide_v2)) && (!audio_is_playing(sn_slide_v3)) && (!audio_is_playing(sn_slide_v4)) {
			audio_play_sound(choose(sn_slide_v1,sn_slide_v2,sn_slide_v3,sn_slide_v4),3,false);
		}
	}
	
	if (image_index > 14) && (image_index < 16) {
		if (scr_chance(0.1)) {
			if (!audio_is_playing(snd_slidecall_v1)) && (!audio_is_playing(snd_slidecall_v2)) && (!audio_is_playing(snd_slidecall_v3)) {
				audio_play_sound(choose(snd_slidecall_v1,snd_slidecall_v2, snd_slidecall_v3),4,false); 
			}
		}
	}
#endregion

#region End Ground Slide Effects
if (scr_animation_end()) { image_index = 0; image_speed = 1; state = states.normal; }
if (collided) { image_index = 0; image_speed = 1; state = states.normal; }
if (!onground) && (!onplatform) && (!onobject) { image_index = 0; image_speed = 1; state = states.normal; }
#endregion

scr_collision();