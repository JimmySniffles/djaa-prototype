///@description Animal Animation

#region Universal Animation Effects | Idle, Landing, Falling, Jumping
	
	#region Falling & Jumping Animation, Visual Effects & Sound
	if (jumpbuffer <= 0)
	{
		sprite_index = ani_fall;
		if (sign(vsp) > 0) sprite_index = ani_fall; else sprite_index = ani_jump;
	}
	#endregion
	
	#region Landing Visual Effects & Sound
	if (sprite_index == ani_fall) && (jumpbuffer > 0) {
		if (distance_to_object(player) < hearingrange) {
			if (scr_chance(0.2)) {
				if (!audio_is_playing(snd_landing_v1)) && (!audio_is_playing(snd_landing_v2)) && (!audio_is_playing(snd_landing_v3)) {
					audio_play_sound(choose(snd_landing_v1,snd_landing_v2, snd_landing_v3),4,false); 
				}
			}
			var landing = audio_play_sound(choose(sn_glanding_v1,sn_glanding_v2,sn_glanding_v3),4,false); 
			audio_sound_pitch(landing, choose(0.8,0.9,1,1.1,1.2));
			scr_screenshake(scrshk_landing_mag, scrshk_landing_frms);
			repeat(dirt_landing) { scr_dirtparticle(x, bbox_bottom, dirtpart.landing); }
		}
	 }
	 #endregion
	 
	#region Idle Animation, Visual Effects & Sound
	if (collided) || (hsp == 0) && (dir == 0) && (jumpbuffer > 0)
	{
		sprite_index = ani_idle;
		if (distance_to_object(player) < hearingrange) && (state != a_states.scared) && (state != a_states.alert) && (state != a_states.chase) {
			idletimer++;
			if (idletimer >= room_speed * 15) {
				if (scr_chance(0.25)) {
					if (!audio_is_playing(snd_idle_v1)) && (!audio_is_playing(snd_idle_v2)) && (!audio_is_playing(snd_idle_v3)) && (!audio_is_playing(snd_idle_v4)) {
						audio_play_sound(choose(snd_idle_v1, snd_idle_v2, snd_idle_v3, snd_idle_v4),5,false);
					}
				}
			idletimer = 0;
			}
		}
	}
	#endregion

#endregion

#region Specific Animation Effects | Walk & Sprint
	 
	#region Walk Movement Animation, Visual Effects & Sound
	if (hsp != 0) && (!collided) && (state = a_states.normal) && (jumpbuffer > 0) { sprite_index = ani_walk; }
	
	#region Kangaroo Walking Effects
	if (animal_type = "kangaroo") {
		if (sprite_index = ani_walk)
		{
			if (distance_to_object(player) < hearingrange) {
				if (image_index >= 6) && (image_index <= 7) {
					if (!audio_is_playing(sn_gstep_v1)) && (!audio_is_playing(sn_gstep_v2)) && (!audio_is_playing(sn_gstep_v3)) && (!audio_is_playing(sn_gstep_v4))  && (!audio_is_playing(sn_gstep_v5)) { 
						var moving = audio_play_sound(choose(sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5),3,false);
						audio_sound_pitch(moving, choose(0.9,1,1.1,1.2));
					}
					repeat(dirt_moving) { scr_dirtparticle(x, bbox_bottom, dirtpart.movement); }
				}
			}
		}
	}
	#endregion
	
	#region Snake Walking Effects
	if (animal_type = "snake") {
		if (sprite_index = ani_walk)
		{
			if (distance_to_object(player) < hearingrange) {
				with (instance_create_layer(x,bbox_bottom,"Effects",obj_dirtslide)) { vspeed = 0; }
			}
		}
	}
	#endregion
	
	#endregion
	
	#region Sprint Movement Animation, Visual Effects & Sound
	
	#region Kangaroo Running Effects
	if (animal_type = "kangaroo") {
		if (state = a_states.scared) && (!collided) && (hsp != 0) && (dir != 0) && (jumpbuffer > 0)
		{
			sprite_index = ani_run;
			runningtimer++;
			if (runningtimer >= engy_exert_breath) {
				if (distance_to_object(player) < hearingrange) {
					if (!audio_is_playing(snd_running_v1)) && (!audio_is_playing(snd_running_v2)) {
						audio_play_sound(choose(snd_running_v1,snd_running_v2),5,false);
					}
				}
				runningtimer = 0;
			}
		}
		if (sprite_index = ani_run)
		{
			if (distance_to_object(player) < hearingrange) {
				if (image_index >= 6) && (image_index <= 7) {
					if (!audio_is_playing(sn_gstep_v1)) && (!audio_is_playing(sn_gstep_v2)) && (!audio_is_playing(sn_gstep_v3)) && (!audio_is_playing(sn_gstep_v4))  && (!audio_is_playing(sn_gstep_v5)) { 
						var moving = audio_play_sound(choose(sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5),3,false);
						audio_sound_pitch(moving, choose(0.9,1,1.1,1.2));
					}
					repeat(dirt_moving) { scr_dirtparticle(x, bbox_bottom, dirtpart.movement); }
				}
			}
		}
	}
	#endregion
	
	#region Snake Running Effects
	if (animal_type = "snake") {
		if (state != a_states.normal) && (!collided) && (hsp != 0) && (dir != 0) && (jumpbuffer > 0)
		{
			sprite_index = ani_run;
			runningtimer++;
			if (runningtimer >= engy_exert_breath) {
				if (distance_to_object(player) < hearingrange) {
					if (!audio_is_playing(snd_running_v1)) && (!audio_is_playing(snd_running_v2)) {
						audio_play_sound(choose(snd_running_v1,snd_running_v2),5,false);
					}
				}
				runningtimer = 0;
			}
		}
		if (sprite_index = ani_run)
		{
			if (distance_to_object(player) < hearingrange) {
				with (instance_create_layer(x,bbox_bottom,"Effects",obj_dirtslide)) { vspeed = 0; }
			}
		}
	}
	#endregion
	
	#endregion

#endregion