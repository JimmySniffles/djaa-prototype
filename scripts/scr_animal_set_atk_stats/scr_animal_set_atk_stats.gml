///@description Setting the animal attack stats
/// @param animal_type

a_type					= argument0;
var animal_wp_map		= animal_attack[a_type];

//The ? is a accessor to find the value of ds map. Its the same as ds_map_find_value which find the value of the specified item.
ply_dmg					= animal_wp_map[? "ply_dmg"];
ene_dmg					= animal_wp_map[? "ene_dmg"];
ani_dmg					= animal_wp_map[? "ani_dmg"];

attack_spd				= animal_wp_map[? "attack_spd"];
attack_energy			= animal_wp_map[? "attack_energy"];
stun					= animal_wp_map[? "stun"];
knockback				= animal_wp_map[? "knockback"];
knockback_str			= animal_wp_map[? "knockback_str"];

scrshk_mag				= animal_wp_map[? "scrshk_mag"];
scrshk_frm				= animal_wp_map[? "scrshk_frm"];
ply_particles			= animal_wp_map[? "ply_particles"];
ene_particles			= animal_wp_map[? "ene_particles"];
ani_particles			= animal_wp_map[? "ani_particles"];

projectile				= animal_wp_map[? "projectile"];
proj_spd				= animal_wp_map[? "proj_spd"];
proj_dist				= animal_wp_map[? "proj_dist"];