///@description Death Sequence For Player

#region Setting Movement
hascontrol = false;

hsp = scr_approach(hsp,0,hsp_fric_ground);
vsp += grv;
vsp = clamp(vsp,-vsp_max,vsp_max);
#endregion

#region Death Animation, Sounds & Effects
if (deathplayed == 0) {
	deathplayed = 1;

	//Setting sprite animation
	sprite_index = ani_dead;
	mask_index = ani_dead;
	
	//Adding sound and visual effects
	scr_screenshake(2,20);
	if (!audio_is_playing(snd_death_v1)) && (!audio_is_playing(snd_death_v2)) && (!audio_is_playing(snd_death_v3)) && (!audio_is_playing(snd_death_v4)) {
		audio_play_sound(choose(snd_death_v1,snd_death_v2,snd_death_v3,snd_death_v4),8,false);
	}
	game_set_speed(30,gamespeed_fps);
}
#endregion

#region End Death Effects
//Setting end death animation to last frame
if (scr_animation_end()) { 
	image_index = image_number -1; 
		image_alpha -= 0.07;
		if (whiteflash < 0.7) { whiteflash += 0.025; }
		with (instance_create_layer(x,bbox_bottom,"Effects",obj_death)) { x = random_range(other.x-20,other.x+20); }
}

//Setting timer for dead player & setting what happens when timer reaches zero
if (deathtimer > 0) { deathtimer -= 1; }
if (deathtimer <= 0) { deathtimer = max_deathtimer; game_set_speed(60,gamespeed_fps); scr_transition(TRANS_TYPE.CENTRE,TRANS_MODE.DEATH); }
#endregion

scr_collision();