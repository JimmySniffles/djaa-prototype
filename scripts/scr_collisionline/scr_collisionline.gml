/// @desc scr_collisionline(x1,y1,x2,y2,object,prec,notme)

//  Returns the instance id of an object colliding with a given line and
//  closest to the first point, or noone if no instance found.
//  The solution is found in log2(range) collision checks.
//
/// @arg x1
/// @arg y1
/// @arg x2
/// @arg y2
/// @arg object
/// @arg prec
/// @arg notme

/// From GMLscripts.com, modifications by ShaunJS
{
    var ox,oy,dx,dy,object,prec,notme,sx,sy,inst,i,angle;
    ox = argument0;
    oy = argument1;
    dx = argument2;
    dy = argument3;
    object = argument4;
    prec = argument5;
    notme = argument6;
    sx = dx - ox;
    sy = dy - oy;
	angle = point_direction(ox,oy,dx,dy);
    inst = collision_line(ox,oy,dx,dy,object,prec,notme);
    if (inst != noone) {
        while ((abs(sx) >= 1) || (abs(sy) >= 1)) {
            sx /= 2;
            sy /= 2;
            i = collision_line(ox,oy,dx,dy,object,prec,notme);
            if (i) {
                dx -= sx;
                dy -= sy;
                inst = i;
            }else{
                dx += sx;
                dy += sy;
            }
        }
    
		while (collision_point(dx,dy,object,prec,notme) != noone)
		{
			dx -= lengthdir_x(1,angle);
			dy -= lengthdir_y(1,angle);
		}
	}
	
	var array = [inst,dx,dy];
    return array;
}
