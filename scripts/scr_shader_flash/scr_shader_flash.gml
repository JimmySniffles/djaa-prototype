/// @description scr_shader_flash(decay_spd, red_value, blue_value, green_value)
/// @param decay_spd	= 0;
/// @param red_value	= 1;
/// @param blue_value	= 2;
/// @param green_value	= 3;

if (global.shaders == true) {
	//Speed of flash alpha effect decaying
	var alpha;

	//Setting Colours & Alpha
	if (whiteflash > 0) { whiteflash -= argument0; alpha = whiteflash; }
	if (redflash > 0) { redflash -= argument0; alpha = redflash; }
	if (greenflash > 0) { greenflash -= argument0; alpha = greenflash; }
	if (blueflash > 0) { blueflash -= argument0; alpha = blueflash; }
	
	gpu_set_blendmode(bm_add);
	shader_set(shd_flash);
	
	shd_red = shader_get_uniform(shd_flash,"red"); shd_green = shader_get_uniform(shd_flash,"green");
	shd_blue = shader_get_uniform(shd_flash,"blue"); shd_alpha = shader_get_uniform(shd_flash,"alpha");
	shader_set_uniform_f(shd_red,argument1); shader_set_uniform_f(shd_blue,argument2);
	shader_set_uniform_f(shd_green,argument3); shader_set_uniform_f(shd_alpha,alpha);

	draw_self();
	
	shader_reset();
	gpu_set_blendmode(bm_normal);
}