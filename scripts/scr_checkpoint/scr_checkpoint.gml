///@description Checkpoint Position Set And Reseting Player Stats

#region Checks which player object exists
if (instance_exists(obj_boy)) { player = obj_boy; }
if (instance_exists(obj_girl)) { player = obj_girl; }
#endregion

#region If Player Exists Reset Stats, Set X & Y Position
if (instance_exists(player)) {
	with (player) {
		if (global.checkpointR == room) { x = global.checkpointx; y = global.checkpointy; }
		else { x = startx; y = starty; }
		
		if (state = states.death) {
			mask_index = ani_idle;
			image_alpha = 1;
			whiteflash = 0.7;
			state = states.normal;
			hascontrol = true;
			deathplayed = 0;
			hp = max_hp;
			energy = max_energy;
			breath = max_breath;
		}
	}
}
#endregion