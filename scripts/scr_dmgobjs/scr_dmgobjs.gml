/// @description Creating Damageable Objects Mechanics

#region Setting Up Animation, Visual, Sound Effects and Its Own Destruction

	#region Object Breaking Stages | Animation, Visual & Sound Effects
	//Checking if hp is around 80% then change frame
	if (hp <= max_hp - (max_hp / 5)) && (hp > max_hp - ((max_hp / 5) * 2)) { image_index = 1; }
	//Checking if hp is around 60% then change frame
	if (hp <= max_hp - ((max_hp / 5) * 2)) && (hp > max_hp - ((max_hp / 5) * 3)) { image_index = 2; }
	//Checking if hp is around 40% then change frame
	if (hp <= max_hp - ((max_hp / 5) * 3)) && (hp > max_hp - ((max_hp / 5) * 4)) { image_index = 3; }
	//Checking if hp is around 20% then change frame
	if (hp <= max_hp - ((max_hp / 5) * 4)) && (hp > 0) { image_index = 4; }

	//Setting Sounds When Switching Frame After New Breaking Stage
	if (play_sound = true) && (last_img != image_index)
	{
		audio_play_sound(choose(snd_breaking_v1,snd_breaking_v2,snd_breaking_v3),3,false);
		play_sound = false;
		last_img = image_index;
	}
	else {
		play_sound = true;		
	}
	#endregion

	#region Object Reaching Zero HP | Destroy Object
	if (hp <= 0) { 
		audio_play_sound(choose(snd_destroyed_v1,snd_destroyed_v2),4,false);
		repeat(dirt_destroy) { scr_dirtparticle(x, y, dirt_type); }
		instance_destroy();
	}
	#endregion

#endregion