///@description Change Window Mode
///@arg Value
switch (argument0) {
	case 0: window_set_fullscreen(false) variable_global_set(string("windowsmode"), argument0); break;
	case 1: window_set_fullscreen(true) variable_global_set(string("windowsmode"), argument0); break;
}	