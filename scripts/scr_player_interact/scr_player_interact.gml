///@description Players Interact State

#region Setting Movement
hsp = scr_approach(hsp,0,hsp_fric_ground);
vsp = scr_approach(vsp,0,hsp_fric_air);
#endregion

#region Interact Animation, Sounds & Effects
	//Setting sprite animation & setting detect radius
	sprite_index = ani_interact
	var detect_radius = 100;
	
	//Collection Depending Upon Image__Index
	#region Collection Of Berry
	if (instance_exists(obj_berries)) {
	var withinberrydist = (collision_circle(x,y,detect_radius,obj_berries,false,true));
	if (image_index >= 6) && (withinberrydist != noone) {
		with (withinberrydist) { 
			if (image_index == berrytype.fingerlime) {
				if (!audio_is_playing(sn_finger_lime)) { audio_play_sound(sn_finger_lime,5,false); }
				audio_play_sound(sn_hp_increase,5,false);
			}
			if (image_index == berrytype.lemonmyrtle) {
				if (!audio_is_playing(sn_invincibility)) { audio_play_sound(sn_invincibility,5,false); }
			}
		}
		if (!audio_is_playing(snd_swallow_v1)) && (!audio_is_playing(snd_swallow_v2)) {
				audio_play_sound(choose(snd_swallow_v1,snd_swallow_v2),5,false);
		}
		withinberrydist.collected = true;
		}
	}
	#endregion
	
	#region Collection Of Weapons
	if (instance_exists(obj_boy)) {
		if (instance_exists(obj_weapon_pkup)) {
			var withinweaponpickupdist = (collision_circle(x,y,detect_radius,obj_weapon_pkup,false,true));
				if (image_index >= 6) && (withinweaponpickupdist != noone) {
				if (!audio_is_playing(snd_pickup_v1)) && (!audio_is_playing(snd_pickup_v2)) {
						audio_play_sound(choose(snd_pickup_v1,snd_pickup_v2),5,false);
				}
				audio_play_sound(sn_weaponpickup,5,false);
				withinweaponpickupdist.collected = true;
			}
		}
	}
	#endregion
	
#endregion

#region End Interact State
if (scr_animation_end()) { state = states.normal; }
#endregion
	
scr_collision();