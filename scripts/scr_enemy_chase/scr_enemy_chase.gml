///@description Enemies Chase State

#region Calling Enemy Scripts
scr_enemy_move();					//Getting enemies calcuation movement script
scr_enemy_ani();					//Getting enemies animation, visual and sound effects script
#endregion

#region If Player Exists
if (instance_exists(player))
{
	#region Setting Catchphrase
	if (catchphrase) {
		if (instance_exists(obj_boy)) {
			if (!audio_is_playing(snd_chase_v1)) && (!audio_is_playing(snd_chase_v2)) && (!audio_is_playing(snd_chase_v3)) && (!audio_is_playing(snd_chase_b_v1)) && (!audio_is_playing(snd_chase_b_v2)) {
				audio_play_sound(choose(snd_chase_v1,snd_chase_v2,snd_chase_v3,snd_chase_b_v1,snd_chase_b_v2),5,false);
			}
		}
		if (instance_exists(obj_girl)) {
			if (!audio_is_playing(snd_chase_v1)) && (!audio_is_playing(snd_chase_v2)) && (!audio_is_playing(snd_chase_v3)) && (!audio_is_playing(snd_chase_g_v1)) && (!audio_is_playing(snd_chase_g_v2)) {
				audio_play_sound(choose(snd_chase_v1,snd_chase_v2,snd_chase_v3,snd_chase_g_v1,snd_chase_g_v2),5,false);
			}
		}	
		catchphrase = false;
	}
	#endregion
	
	#region Setting Direction
	//Gets direction of the player. Subtracts the x position of the player by the x position of the enemy | -1 moving to the left and +1 moving to the right
	if (energy > 0) { dir = sign(player.x - x); } else { dir = -sign(player.x - x); }

	//Stops enemy close by to the player instead of exactly at the players x position
	if (abs(player.x - x) < attackrange) { hsp = 0; }
	#endregion
	
	#region State Transitions
	if (distance_to_object(player) <= attackrange) && (jumpbuffer > 0) && (energy > 0) && (player.state != states.death) {
		energy -= attack_energy;
		state = e_states.attack;
	}
	if (distance_to_object(player) >= outofsight) || (player.state == states.death) { returnable_to_x_anchor = 1; state = e_states.normal; }
	#endregion
}
#endregion

scr_collision();