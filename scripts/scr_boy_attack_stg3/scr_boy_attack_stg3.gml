///@description Budburra's Attack Stage 3 State
/*
#region Getting Inputs & Checking Whether On Ground
scr_get_inputs();
onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0) || (place_meeting(x,y+1,par_wall)) || (place_meeting(x,y+1,par_dmgobjs));
#endregion

#region Weapon Attacks

	#region Club Attacks
	if (obj_b_wloadout.weapon = wep_type.club) {
	
		#region Normal Attack
		
			#region On Ground
			if (onground && jumpbuffer > 0) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//Attack
			scr_melee_attack(ani_clb_na_g_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_clb_na_g_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			if (key_attack) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg1; energy -= obj_b_wloadout.energy; }
			}
			#endregion
		
			#region In Air
			if (!onground && jumpbuffer = 0) {
		
			//Setting Movement
			scr_player_move();
		
			//Attack
			scr_melee_attack(ani_clb_na_a_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_clb_na_a_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
			}
			#endregion
		
		#endregion
	
		#region Special Attack
		
			#region In Air
			if (!onground && jumpbuffer = 0) {
			
			//Setting Movement & Special Attack To False
			spattack_act = false;
			scr_player_move();
		
			//Attack
			scr_melee_attack(ani_clb_sa_a_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_clb_sa_a_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
			}
			#endregion
		
		#endregion
	}
	#endregion

	#region Stun Boomerang Attacks
	if (obj_b_wloadout.weapon = wep_type.stunboomerang) {
		
		#region Special Attack
	
			#region On Ground
			if (onground && jumpbuffer > 0) {
			
			//Setting Movement & Special Attack To False
			spattack_act = false;
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//Attack
			scr_melee_attack(ani_stb_sa_g_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_stb_sa_g_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			if (key_spattack) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg1; energy -= obj_b_wloadout.energy; spattack_act = true; }
			}
			#endregion
		
			#region In Air
			if (!onground && jumpbuffer = 0) {
		
			//Setting Movement & Special Attack To False
			spattack_act = false;
			scr_player_move();
		
			//Attack
			scr_melee_attack(ani_stb_sa_a_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_stb_sa_a_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
			}
			#endregion
		
		#endregion
	}
	#endregion

	#region Spear Attacks
	if (obj_b_wloadout.weapon = wep_type.spear) {
	
		#region Special Attack
	
			#region On Ground
			if (onground && jumpbuffer > 0) {
			
			//Setting Movement & Special Attack To False
			spattack_act = false;
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//Attack
			scr_melee_attack(ani_spr_sa_g_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_spr_sa_g_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			if (key_spattack) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg1; energy -= obj_b_wloadout.energy; spattack_act = true; }
			}
			#endregion
		
			#region In Air
			if (!onground && jumpbuffer = 0) {
		
			//Setting Movement & Special Attack To False
			spattack_act = false;
			scr_player_move();
		
			//Attack
			scr_melee_attack(ani_spr_sa_a_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_spr_sa_a_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
			}
			#endregion
		
		#endregion
	}
	#endregion

	#region Kill Boomerang Attacks
	if (obj_b_wloadout.weapon = wep_type.killboomerang) {
	
		#region Special Attack
	
			#region On Ground
			if (onground && jumpbuffer > 0) {
			
			//Setting Movement & Special Attack To False
			spattack_act = false;
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//Attack
			scr_melee_attack(ani_klb_sa_g_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_klb_sa_g_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			if (key_spattack) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg1; energy -= obj_b_wloadout.energy; spattack_act = true; }
			}
			#endregion
		
			#region In Air
			if (!onground && jumpbuffer = 0) {
		
			//Setting Movement & Special Attack To False
			spattack_act = false;
			scr_player_move();
		
			//Attack
			scr_melee_attack(ani_klb_sa_a_stg3,ani_idle,obj_b_wloadout.attack_spd,ani_klb_sa_a_stg3_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_hitsparks,obj_b_wloadout.ani_hitsparks,obj_b_wloadout.env_hitsparks,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
			}
			#endregion
		
		#endregion
	}
	#endregion

#endregion
	
#region End Attack Stage 3 Effects
if (scr_animation_end()) { spattack_act = false; image_speed = 1; state = states.normal; }
#endregion

scr_collision();