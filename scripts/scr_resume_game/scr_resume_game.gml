///@description Resume Game
if (!file_exists(SAVEFILE)) {
	scr_transition(TRANS_TYPE.CENTRE,TRANS_MODE.GOTO,rm_cutscene_1);	
}
else {
	var file = file_text_open_read(SAVEFILE);
	var target = file_text_read_real(file);
	file_text_close(file);
	scr_transition(TRANS_TYPE.CENTRE,TRANS_MODE.GOTO,target);
}	