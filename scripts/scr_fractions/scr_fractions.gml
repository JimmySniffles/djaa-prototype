//Dump fractions and get final integer speeds
//Re apply fractions
hsp += hsp_frac;
vsp += vsp_frac;

//Store and Remove fractions
//Important: go into collision with whole integers ONLY!
hsp_frac = hsp - (floor(abs(hsp)) * sign(hsp));
hsp -= hsp_frac;
vsp_frac = vsp - (floor(abs(vsp)) * sign(vsp));
vsp -= vsp_frac;


//Note to self, whenever changing speeds directly, also clear fraction.