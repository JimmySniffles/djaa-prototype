///@description scr_sprite_button_detect(sprite_xposition, sprite_yposition, sprite)
/// @param sprite_xposition		= 0;
/// @param sprite_yposition		= 1;
/// @param sprite				= 2;

//Script returns true if mouse position is within sprite x and y position
var mobile_x_gui_0	= device_mouse_x_to_gui(0);
var mobile_x_gui_1	= device_mouse_x_to_gui(1);
var mobile_y_gui_0	= device_mouse_y_to_gui(0);
var mobile_y_gui_1	= device_mouse_y_to_gui(1);
var detection		= false;
	
if (mobile_x_gui_0 >= argument0) && (mobile_x_gui_0 <= argument0+sprite_get_width(argument2)) || (mobile_x_gui_1 >= argument0) && (mobile_x_gui_1 <= argument0+sprite_get_width(argument2)) { 
	if (mobile_y_gui_0 <= argument1+sprite_get_height(argument2)) && (mobile_y_gui_0 >= argument1) || (mobile_y_gui_1 <= argument1+sprite_get_height(argument2)) && (mobile_y_gui_1 >= argument1) {
		if (device_mouse_check_button_pressed(0,mb_left)) || (device_mouse_check_button_pressed(1,mb_left)) { detection = true; } else { detection = false; }
	}
}

return (detection);