///@description Climbing Objects Mechanic

#region Getting Inputs & Movement Calculations
scr_get_inputs();

if (global.computer_use) {
	key_down = keyboard_check(ord("S")) || keyboard_check(vk_down); key_up = keyboard_check(ord("W")) || keyboard_check(vk_up);
	key_left = keyboard_check(ord("A")) || keyboard_check(vk_left); key_right = keyboard_check(ord("D")) || keyboard_check(vk_right);
	var horizontal_dir = key_right - key_left; var vertical_dir = key_down - key_up; 
}
if (global.mobile_use) { var vertical_dir = key_left - key_right; }

//Setting horizontal calculations
if (global.computer_use) {
	hsp_cmsp = scr_approach(hsp_cmsp,hsp_cmaxsp,hsp_acc);
	hsp += horizontal_dir * hsp_cmsp;
	if (horizontal_dir == 0) { hsp = scr_approach(hsp,0,hsp_fric_ground); }
	hsp = clamp(hsp,-hsp_cmsp,hsp_cmsp);
}
if (global.mobile_use) { if (!key_jump) { hsp = scr_approach(hsp,0,hsp_fric_ground); } }

//Setting vertical calculations
vsp_cmsp = scr_approach(vsp_cmsp,vsp_cmaxsp,vsp_cacc);
vsp += vertical_dir * vsp_cmsp;
if (vertical_dir == 0) { vsp = scr_approach(vsp,0,hsp_fric_air); }
vsp = clamp(vsp,-vsp_cmsp,vsp_cmsp);
#endregion

#region Climbing Animation, Sounds & Effects
//Setting sprite
sprite_index = ani_climb;

//Setting sprite speed animation
if (global.computer_use) {
	if (key_up) || (key_down) || (key_left) || (key_right) 
	{ image_speed = 0.7; } else { image_speed = 0; }
}

if (global.mobile_use) {
	if (key_left) || (key_right) 
	{ image_speed = 0.7; } else { image_speed = 0; }
}

//Setting sounds

//Setting visual effects
var horizontal_side; if (hsp < 0) { horizontal_side = bbox_right; } else { horizontal_side = bbox_left; }
if (hsp != 0) { instance_create_layer(horizontal_side,bbox_top,"Effects",obj_dirtslide); instance_create_layer(horizontal_side,bbox_bottom,"Effects",obj_dirtslide) }
var vertical_side, particle_speed; if (vsp < 0) { vertical_side = bbox_bottom; particle_speed = random_range(0.1,1); } else { vertical_side = bbox_top; particle_speed = random_range(-0.1,-1); }
if (vsp != 0) { with (instance_create_layer(bbox_left,vertical_side,"Effects",obj_dirtslide)) { vspeed = particle_speed; } with (instance_create_layer(bbox_right,vertical_side,"Effects",obj_dirtslide)) { vspeed = particle_speed; } }
#endregion

#region If No Longer Colliding With Object Or Jump Off
if (!place_meeting(x,y,par_climb)) {
	//Stop climbing sound
	
	image_speed = 1;
	state = states.normal;
}

//Checking whether on ground
if (global.computer_use) {
	if (onground) && (key_down) {
		image_speed = 1; 
		state = states.normal; 
	}
}
if (global.mobile_use) {
	if (onground) && (key_left) {
		image_speed = 1; 
		state = states.normal; 
	}
}

if (key_jump) && (energy > 0) {
	if (scr_chance(0.1)) {
	if (!audio_is_playing(snd_jump_v1)) && (!audio_is_playing(snd_jump_v2)) && (!audio_is_playing(snd_jump_v3)) 
	audio_play_sound(choose(snd_jump_v1,snd_jump_v2,snd_jump_v3),5,false);
	}
	
	repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
	
	if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
		var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		audio_sound_pitch(jump, choose(1,1.1,1.2));
	}
	
	if (global.mobile_use) { climb_delay = room_speed/2; }
	vsp = vsp_jump; energy -= e_jumpuse; jumps -= 1; show_ui_energy = true; vsp_frac = 0; onground = false; image_speed = 1; state = states.normal;
}
#endregion

scr_collision();