//General movement calculation script of player objects.

#region General Player Movement, Status Relative To Ground & Walls

	#region Checking If There Is A Dialogue Present, If So Player Has No Control
	if (instance_exists(obj_textbox)) { hascontrol = false } else { hascontrol = true; }
	#endregion
	
	#region Calculate Horizontal Movement | Budburra
	if (myName = "Budburra") 
	{
		//Toggling Horizontal Movement Between Walk and Sprint
		if (key_sprint) sprinting = !sprinting;
		if (sprinting) && (energy > 0)
		{
			hsp_msp = scr_approach(hsp_msp,(key_right - key_left) * hsp_maxsp,hsp_acc);
			hsp = hsp_msp;
			if (hsp == 0)
			{
				var hsp_fric_final = hsp_fric_ground;
				if (!onground) hsp_fric_final = hsp_fric_air;
				hsp = scr_approach(hsp,0,hsp_fric_final);
			}
		hsp = clamp(hsp,-hsp_msp,hsp_msp);
		}
		else
		{
			hsp_wmsp = scr_approach(hsp_wmsp,(key_right - key_left) * hsp_wmaxsp,hsp_acc);
			hsp = hsp_wmsp;
			if (hsp == 0)
			{
				var hsp_fric_final = hsp_fric_ground;
				if (!onground) hsp_fric_final = hsp_fric_air;
				hsp = scr_approach(hsp,0,hsp_fric_final);
			}
		hsp = clamp(hsp,-hsp_wmsp,hsp_wmsp);
		}
	}
	#endregion
	
	#region Calculate Horizontal Movement | Hope
	if (myName = "Hope")
	{
		walljumpdelay = max(walljumpdelay-1,0);
		if (walljumpdelay == 0)
		{
			//Toggling Horizontal Movement Between Walk and Sprint
			if (key_sprint) sprinting = !sprinting; 
			if (sprinting) && (energy > 0)
			{
				var dir = key_right - key_left;
				hsp_msp = scr_approach(hsp_msp,hsp_maxsp,hsp_acc);
				hsp += dir * hsp_acc;
				if (dir == 0)
				{
					var hsp_fric_final = hsp_fric_ground;
					if (!onground) hsp_fric_final = hsp_fric_air;
					hsp = scr_approach(hsp,0,hsp_fric_final);
				}
			hsp = clamp(hsp,-hsp_msp,hsp_msp);
			}
			else
			{
				var dir = key_right - key_left;
				hsp_wmsp = scr_approach(hsp_wmsp,hsp_wmaxsp,hsp_acc);
				hsp += dir * hsp_acc;
				if (dir == 0)
				{
					var hsp_fric_final = hsp_fric_ground;
					if (!onground) hsp_fric_final = hsp_fric_air;
					hsp = scr_approach(hsp,0,hsp_fric_final);
				}
			hsp = clamp(hsp,-hsp_wmsp,hsp_wmsp);
			}
		}
	}
	#endregion
		
	#region Calculate Vertical Movement
	var grv_final = grv;
	var vsp_max_final = vsp_max;
	if (onwall != 0) && (vsp > 0)
	{
		grv_final = grv_wall;
		vsp_max_final = vsp_max_wall;
	}
	if (state = states.attack_stg1) || (state = states.attack_stg2)
	{
		grv_final = grv_atk;
		vsp_max_final = vsp_max_atk;
	}
	vsp += grv_final;
	vsp = clamp(vsp,-vsp_max_final,vsp_max_final);
	#endregion
	
	#region Calculate Currant Status
	jumpbuffer = max(jumpbuffer-1,0);
	//Is my middle center touching the floor at the start of this frame?
	onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0); 
	onplatform = (place_meeting(x,y+1,par_oneway_platform));
	onobject = (place_meeting(x,y+1,par_objects));
	if (onground) || (onplatform) || (onobject) || (scr_infloor(global.tilemap,bbox_left,bbox_bottom+1) >= 0) || (scr_infloor(global.tilemap,bbox_right,bbox_bottom+1) >= 0)
	{
		jumpbuffer = jumpbuffer_max;
	}
	if (onplatform) && (key_down) { y ++; }
	if (myName = "Hope") { if (onground) || (onplatform) || (onobject) { jumps = jumpsmax; } }
	
	var walljpr, walljpl; walljpr = (scr_infloor(global.tilemap,bbox_right+1,y) >= 0); walljpl = (scr_infloor(global.tilemap,bbox_left-1,y) >= 0); 
	onwall = (walljpr == 1) - (walljpl == 1);	
	#endregion
	
	#region Storing Fractions For Non Integer Speeds
	scr_fractions();
	#endregion
	
#endregion