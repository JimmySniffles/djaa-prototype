///@description Budburra's Normal State

#region Calling Player Scripts
scr_get_inputs();					//Getting inputs script
scr_player_move();					//Getting player's calcuation movement script
scr_player_ani();					//Getting players animation, visual and sound effects script
#endregion

#region Ground Jump
if (key_jump) && (jumpbuffer > 0) && (vsp >= 0) && (energy > 0)
{
	if (scr_chance(0.2)) {
	if (!audio_is_playing(snd_jump_v1)) audio_play_sound(snd_jump_v1,5,false);
	}
	
	repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
	
	if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
		var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		audio_sound_pitch(jump, choose(1,1.1,1.2));
	}
	vsp = vsp_jump; energy -= e_jumpuse; show_ui_energy = true; vsp_frac = 0; onground = false; onplatform = false; onobject = false;
}
//Variable Jump
if (vsp < 0) && (!key_jump_held) vsp = max(vsp,vsp_jump/2);
#endregion

#region Rolling & Ground Sliding

	#region Rolling
	if (key_up) && (jumpbuffer > 0) && (energy > 0) && (sprinting) && (hsp != 0) { 
		if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		}
		if (scr_chance(0.3)) {
			if (!audio_is_playing(snd_jump_v1)) audio_play_sound(snd_jump_v1,5,false);
		}
		repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
		energy -= e_rolluse; show_ui_energy = true; image_index = 0; state = states.dodge; 
	}
	#endregion
	
	#region Ground Sliding
	if (key_down) && (jumpbuffer > 0) && (energy > 0) && (sprinting) && (hsp != 0) && (!collided) { 
		energy -= e_gslideuse; show_ui_energy = true; image_index = 0; state = states.gslide; 
	}
	#endregion
	
#endregion

#region Weapon Attacks	
	
	#region Run Attack
	/*
	if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
		if (key_runattack) && (energy > 0) && (onground) {
			runattack_act = true;
			energy -= obj_b_wloadout.attack_energy;
			show_ui_energy = true;
			state = states.attack_stg1;
		}
	}
	*/
	#endregion
	
	#region Normal Attack
	if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
		if (key_attack) && (energy > 0) && (onground) {
			nmattack_act = true;
			energy -= obj_b_wloadout.attack_energy;
			show_ui_energy = true;
			state = states.attack_stg1;
		}
	}
	#endregion
	
	#region Special Attack
	if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) && (obj_b_wloadout.weapon = wep_type.spear)  {
		if (key_spattack) && (energy > 0) && (onground) {
			spattack_act = true;
			energy -= obj_b_wloadout.attack_energy;
			show_ui_energy = true;
			state = states.attack_stg1;
		}
	}
	#endregion

#endregion

#region Interaction

	#region Interaction With Berries
	if (instance_exists(obj_berries)) {
		var withinberrydist = (collision_circle(x,y,30,obj_berries,false,true));
		if (key_interact) && (onground) && (cooldown = 0) && (withinberrydist != noone)
		{
			state = states.interact;
		}
	}
	#endregion
	
	#region Interaction With Weapon Pick Ups
	if (instance_exists(obj_weapon_pkup)) {
		var withinweaponpickupdist = (collision_circle(x,y,30,obj_weapon_pkup,false,true));
		if (key_interact) && (onground) && (withinweaponpickupdist != noone)
		{
			state = states.interact;
		}
	}
	#endregion

#endregion

scr_collision();