/// scr_interactable(x, y);
/// @description Creating interactable sprite display
/// @param x
/// @param y

#region Creation Of Interactable Object UI

#region Interactable Variables	
var player = noone;													//Setting player variable
var disp_UI_rad	= 180;												//Setting display ui radius amount
var disp_UI_inner_rad = 100;										//Setting display ui collection radius amount
if (instance_exists(obj_boy)) { player = obj_boy; }					//Checking if boy player object exists and if so it sets player to obj_boy
if (instance_exists(obj_girl)) { player = obj_girl; }				//Checking if girl player object exists and if so it sets player to obj_girl
#endregion

if (player != noone) {
	if (collision_circle(argument0, argument1, disp_UI_rad, player, false, true)) {	
		draw_sprite_ext(spr_marker,0,argument0,scr_wave(argument1-30,argument1-25,2,0),1,1,0,c_white,0.5);
	}
	if (collision_circle(argument0, argument1, disp_UI_inner_rad, player, false, true)) {
		draw_sprite_ext(spr_marker,1,argument0,scr_wave(argument1-30,argument1-25,2,0),1,1,0,c_white,0.5);
	}
} 
#endregion