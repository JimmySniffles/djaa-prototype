///@description Enemies Normal State

#region Calling Enemy Scripts
scr_enemy_move();					//Getting enemies calcuation movement script
scr_enemy_ani();					//Getting enemies animation, visual and sound effects script
#endregion

#region Random Movement & Catch Phrase Re-Intialized
if (catchphrase == false) { catchphrase = true; }
if (moveable)
{
	if (returnable_to_x_anchor == 0) {
		//Idle Timer.
		moveidletimer += 1;
	
		//Max timer amount for random choice between idle and random movement.
		if (moveidletimer >= room_speed * 5) {
			//Chance to change to idle (0) or random wandering movement (1).
			var change; if (collided) { change = choose(2); } else { change = choose(0, 1); }
			switch (change) {
				case 0: dir = 0; moveidletimer = irandom_range(0, room_speed * 2); break;
				case 1: dir = choose(-1,1); moveidletimer = irandom_range(0, room_speed * 2); break;
				case 2:	dir = -dir; moveidletimer = irandom_range(0, room_speed * 2); break;
			}
		}
		//Setting max range for the enemy to move between and if it reaches outside of its range it moves in the opposite direction.
		if (dir == 1 && x >= (x_anchor_range + max_range)) || (dir == -1 && x <= (x_anchor_range - max_range)) { dir = -dir; }
	}
	
	//Setting direction to return to anchor x position. If ever collided with something than ignore moving to original position.
	if (collided) && (returnable_to_x_anchor == 1) { 
		if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
		if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; }
	}
	
	if (returnable_to_x_anchor == 1) {
		if (x > x_anchor_range + max_range) || (x < x_anchor_range - max_range) { dir = sign(x_anchor_range - x); }
		if (abs(x_anchor_range - x) < max_range) { returnable_to_x_anchor = 0; }
	}
	
	//Setting direction to opposite direction if on the edge of the ground.
	var side; if (image_xscale >= 1) { side = bbox_right+hsp; } else { side = -hsp-bbox_left; }
	if (onground) && (!collided) && (afraidofheights) && (scr_infloor(global.tilemap,side,bbox_bottom+1) < 0) { 
		if (returnable_to_x_anchor == 1) { 
			if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
			if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; }
		}
		dir = -dir; 
	}
}
#endregion

#region State Transitions
if (moveable) && (instance_exists(player))
{
	if (distance_to_object(player) > aggrorange) && (takingdamage) && (energy > 0) {
		show_ui_energy = true;
		state = e_states.alert;
	}
		
	if (distance_to_object(player) <= attackrange) && (jumpbuffer > 0) && (energy > 0) && (player.state != states.death) {
		energy -= attack_energy;
		show_ui_energy = true;
		state = e_states.attack;
	}

	if (distance_to_object(player) <= aggrorange) && (energy > 0) && (player.state != states.death) {
		show_ui_energy = true;
		state = e_states.chase;
	}
}
#endregion

scr_collision();