#region Colliding With Water, Losing Breath And Breath Recovery
	if is_water_surface_collision {
	var __vsp = (y-yprevious)/3,		//This instance's vertical speed
	__pos = mean(bbox_left,bbox_right); //And it's horizontal center 
		with other {
		__pos -= x;						//Compared to the water's placement
		__pos = clamp(round(__pos/sprite_width*nodes),0,nodes-1); //Converted to placement in array
		vsp[__pos] += __vsp;
		}
	} 
	else {
	//Just for visually slowing down in the water
		x = mean(x,xprevious);
		y = mean(y,yprevious);
	}
	inandoutofwater = 1;
	breath_timer -= 1;
	if (breath_timer <= 0) { breath -= max_breath * 0.1; breath_timer = max_breath_timer; show_ui_breath = true; }
#endregion