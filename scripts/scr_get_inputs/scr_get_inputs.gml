//Gets Player Input
if (hascontrol)
{
	key_left		= global.key_left;	
	key_right		= global.key_right;	
	key_sprint		= global.key_sprint;	
	key_jump		= global.key_jump;
	key_jump_held	= global.key_jump_held;
	key_down		= global.key_down;
	key_up			= global.key_up;
	key_attack		= global.key_attack;
	key_runattack	= global.key_runattack
	key_spattack	= global.key_spattack;
	key_interact	= global.key_interact;
}

if (instance_exists(obj_camera)) {
	if (!hascontrol) || (obj_camera.follow != player_type) {
		key_left = 0;
		key_right = 0;
		key_sprint = 0;
		key_jump = 0;
		key_down = 0;
		key_up = 0;
		key_attack = 0;
		key_runattack = 0;
		key_spattack = 0;
		key_interact = 0;
	}
}