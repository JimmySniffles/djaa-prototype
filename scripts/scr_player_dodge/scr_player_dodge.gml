///@description Players Dodge Roll State

#region Setting Movement
if (hsp >= 1) { hsp = scr_approach(hsp,roll_spd,hsp_fric_ground); } else { hsp = scr_approach(hsp,-roll_spd,hsp_fric_ground); }
onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0); onplatform = (place_meeting(x,y+1,par_oneway_platform)); onobject = (place_meeting(x,y+1,par_objects));
#endregion

#region Dodge Roll Animation, Sounds & Effects
	//Setting sprite animation
	sprite_index = ani_dodge;
	image_speed = roll_ani_spd;
	var img_index_1, img_index_2;
	if (myName == "Budburra") { img_index_1 = 4; img_index_2 = 5; }
	if (myName == "Hope") { img_index_1 = 4; img_index_2 = 5; }
	
	//The Dodge Landing Visual Effects & Sound
	if (image_index >= img_index_1) && (image_index <= img_index_2) {
		if (exe_dodge_e_once == false) {
			if (scr_chance(0.3)) {
				if (!audio_is_playing(snd_landing_v1)) && (!audio_is_playing(snd_landing_v2)) && (!audio_is_playing(snd_landing_v3)) {
					audio_play_sound(choose(snd_landing_v1,snd_landing_v2, snd_landing_v3),4,false); 
				}
			}
			var landing = audio_play_sound(choose(sn_glanding_v1,sn_glanding_v2,sn_glanding_v3),4,false); 
			audio_sound_pitch(landing, choose(0.8,0.9,1,1.1,1.2));
			scr_screenshake(scrshk_dodge_mag, scrshk_dodge_frms);
			repeat(dirt_dodge) { scr_dirtparticle(x, bbox_bottom, dirtpart.landing); }
			exe_dodge_e_once = true;
		}
	}
#endregion

#region End Dodge Roll Effects
if (scr_animation_end()) { image_index = 0; image_speed = 1; exe_dodge_e_once = false; state = states.normal; }
if (!onground) && (!onplatform) && (!onobject) { image_index = 0; image_speed = 1; state = states.normal; }
#endregion

scr_collision();