///@description Enemy Animation

#region Universal Animation Effects | Idle, Wall Slide, Landing, Falling, Jumping
	
	#region Wall Slide, Falling & Jumping Animation. Wall Slide Visual Effects & Sound
	if (jumpbuffer <= 0)
	{
		if (onwall != 0)
		{
			sprite_index = ani_slide //Sprite section for the sprite to touch the wall as it collides with the wall.
			image_xscale = onwall;   //Check origins and hitboxes so you don't flip into a wall or get stuck in a wall.
		
			var side = bbox_left;  //Bboxes are the x and y coordinates of the players hitbox.
			if (onwall == 1) side = bbox_right;
			
			if (distance_to_object(player) < hearingrange) {
				dirt++;
				onwalltimer++;
				if (!audio_is_playing(sn_slide_v1)) && (!audio_is_playing(sn_slide_v2)) && (!audio_is_playing(sn_slide_v3)) && (!audio_is_playing(sn_slide_v4)) {
					audio_play_sound(choose(sn_slide_v1,sn_slide_v2,sn_slide_v3,sn_slide_v4),3,false);
				}
				if ((dirt > 1) && (vsp > 0)) with (instance_create_layer(side,bbox_top,"Effects",obj_dirtslide)) //Adding dirt if its over 2 and vertical speed is greater than zero.
				{
					other.dirt = 0;
					hspeed = -other.onwall*0.5;
				}
				if (onwalltimer >= room_speed * 10) && (vsp > 0) {
					if (scr_chance(0.1)) {
						if (!audio_is_playing(snd_slidecall_v1)) && (!audio_is_playing(snd_slidecall_v2)) && (!audio_is_playing(snd_slidecall_v3)) {
							audio_play_sound(choose(snd_slidecall_v1,snd_slidecall_v2,snd_slidecall_v3),5,false);
						}
						onwalltimer = 0;
					}
				}
			}
		}
		else
		{
			if (distance_to_object(player) < hearingrange) {
				if (audio_is_playing(sn_slide_v1)) || (audio_is_playing(sn_slide_v2)) || (audio_is_playing(sn_slide_v3)) || (audio_is_playing(sn_slide_v4)) {
					audio_stop_sound(sn_slide_v1) audio_stop_sound(sn_slide_v2) audio_stop_sound(sn_slide_v3) audio_stop_sound(sn_slide_v4);
				}
			}
			dirt = 0;
			sprite_index = ani_fall;
			if (sign(vsp) > 0) sprite_index = ani_fall; else sprite_index = ani_jump;
		}
	}
	#endregion
	
	#region Landing Visual Effects & Sound
	if (sprite_index == ani_fall) && (jumpbuffer > 0) {
		if (distance_to_object(player) < hearingrange) {
			if (scr_chance(0.2)) {
				if (!audio_is_playing(snd_landing_v1)) && (!audio_is_playing(snd_landing_v2)) && (!audio_is_playing(snd_landing_v3)) {
					audio_play_sound(choose(snd_landing_v1,snd_landing_v2, snd_landing_v3),4,false); 
				}
			}
			var landing = audio_play_sound(choose(sn_glanding_v1,sn_glanding_v2,sn_glanding_v3),4,false); 
			audio_sound_pitch(landing, choose(0.8,0.9,1,1.1,1.2));
			scr_screenshake(scrshk_landing_mag, scrshk_landing_frms);
			repeat(dirt_landing) { scr_dirtparticle(x, bbox_bottom, dirtpart.landing); }
		}
	 }
	 #endregion
	 
	#region Idle Animation, Visual Effects & Sound
	if (collided) || (hsp == 0) && (dir == 0) && (jumpbuffer > 0)
	{
		sprite_index = ani_idle;
		if (distance_to_object(player) < hearingrange) && (state != e_states.alert) && (state != e_states.alert) {
			idletimer++;
			if (idletimer >= room_speed * 20) && (!instance_exists(obj_textbox)) {
				if (scr_chance(0.25)) {
					if (!audio_is_playing(snd_idle_v1)) && (!audio_is_playing(snd_idle_v2)) && (!audio_is_playing(snd_idle_v3)) && (!audio_is_playing(snd_idle_v4)) {
						audio_play_sound(choose(snd_idle_v1, snd_idle_v2, snd_idle_v3, snd_idle_v4),5,false);
					}
				}
			idletimer = 0;
			}
		}
	}
	#endregion

#endregion

#region Specific Animation Effects | Walk & Sprint
	 
	#region Walk Movement Animation, Visual Effects & Sound
	if (hsp != 0) && (!collided) && (state = e_states.normal) && (jumpbuffer > 0) { sprite_index = ani_walk; }

	if (sprite_index = ani_walk)
	{
		if (distance_to_object(player) < hearingrange) {
			if (image_index >= 4) && (image_index <= 5) || (image_index >= 12) && (image_index <= 13) {
				if (!audio_is_playing(sn_gstep_v1)) && (!audio_is_playing(sn_gstep_v2)) && (!audio_is_playing(sn_gstep_v3)) && (!audio_is_playing(sn_gstep_v4))  && (!audio_is_playing(sn_gstep_v5)) { 
					var moving = audio_play_sound(choose(sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5),3,false);
					audio_sound_pitch(moving, choose(0.9,1,1.1,1.2));
				}
				var side; if (hsp >= 0) { side = bbox_right } else { side = bbox_left }
				repeat(dirt_moving_wlk) { scr_dirtparticle(side, bbox_bottom, dirtpart.movement); }
			}
		}
	}
	#endregion
	
	#region Sprint Movement Animation, Visual Effects & Sound
	if (state = e_states.chase) || (state = e_states.alert) {
		if (hsp != 0) && (dir != 0) && (!collided) && (jumpbuffer > 0)
		{
			sprite_index = ani_run;
			runningtimer++;
			if (runningtimer >= engy_exert_breath) {
				if (distance_to_object(player) < hearingrange) {
					if (!audio_is_playing(snd_running_v1)) && (!audio_is_playing(snd_running_v2)) {
						audio_play_sound(choose(snd_running_v1,snd_running_v2),5,false);
					}
				}
				runningtimer = 0;
			}
		}
	}
	if (sprite_index = ani_run)
	{
		if (distance_to_object(player) < hearingrange) {
			if (image_index >= 4) && (image_index <= 5) || (image_index >= 12) && (image_index <= 13) {
				if (!audio_is_playing(sn_gstep_v1)) && (!audio_is_playing(sn_gstep_v2)) && (!audio_is_playing(sn_gstep_v3)) && (!audio_is_playing(sn_gstep_v4))  && (!audio_is_playing(sn_gstep_v5)) { 
					var moving = audio_play_sound(choose(sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5),3,false);
					audio_sound_pitch(moving, choose(0.9,1,1.1,1.2));
				}
				var side; if (hsp >= 0) { side = bbox_right } else { side = bbox_left }
				repeat(dirt_moving_run) { scr_dirtparticle(side, bbox_bottom, dirtpart.movement); }
			}
		}
	}
	#endregion
	
#endregion