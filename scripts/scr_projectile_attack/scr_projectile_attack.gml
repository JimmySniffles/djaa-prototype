///@description scr_projectile_attack
/// @param animation	= 0;
/// @param attack_spd	= 1;	
/// @param img_ind_1	= 2;
/// @param img_ind_2	= 3;
/// @param Length		= 4;
/// @param direction	= 5;
/// @param projectile	= 6;
/// @param speed		= 7;

#region Animation & Animation Speed
if (sprite_index != argument0) {
	sprite_index = argument0;
	image_index = 0;
	image_speed = argument1;
}
#endregion

#region Creating Projectile
if (image_index >= argument2) && (image_index <= argument3) {
	with (instance_create_layer(x+lengthdir_x(argument4,argument5),y+lengthdir_y(argument4,argument5),"Projectiles",argument6))
	{
		direction = argument5;
		image_angle = direction;
		speed = argument7;
	}
}
#endregion