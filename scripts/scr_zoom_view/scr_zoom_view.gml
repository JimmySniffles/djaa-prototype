/// @desc scr_view_zoom(zoom_speed, target_zoom_amount)
/// @arg zoom_speed					sets the speed to zoom in or out
/// @arg target_zoom_amount			sets the target zoom amount

var zoom_speed			= argument0;
var target_zoom_amount	= argument1;

if (instance_exists(obj_camera)) {
	with (obj_camera) { 
		if (target_zoom_amount > max_zoom_out) { exit; }
		if (target_zoom_amount < max_zoom_in) { exit; }
		view_zoom += (target_zoom_amount - view_zoom) * zoom_speed; 
	}
}