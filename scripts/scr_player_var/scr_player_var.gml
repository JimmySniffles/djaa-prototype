//This script is for the global variables for in-game movement objects.

#region Horizontal & Horizontal Carry, Vertical & Vertical Carry (For Moving Platforms) Variables & Fractions
hsp = 0;					//Horizontal speed
hsp_frac = 0;				//Horizontal fraction
hsp_carry = 0;				//Horizontal carry on moving platforms

vsp = 0;					//Vertical speed
vsp_frac = 0;				//Vertical fraction
vsp_carry = 0;				//Vertical carry on moving platforms

my_platform = noone;		//Vertical platform variable. Checks if your on a platform
#endregion

#region Object Status
state = states.normal;		//Setting default state of the player to the normal state
hascontrol = true;			//Setting whether the player has control or not
sprinting = true;			//Setting whether the player is sprinting or not
runattack_act = false;		//Checking if the run attack is activated
nmattack_act = false;		//Checking if the normal attack is activated
spattack_act = false;		//Checking if the special attack is activated

collided = false;			//Checking if collided with a collision wall horizontally

exe_dodge_e_once = false;	//Checking so that the dodge effect happens only once

onground = false;			//Returning true or false whether on the ground
onplatform = false;			//Returning true or false whether on a platform
onobject = false;			//Returning true or false whether on a generic object
onwall = 0;					//Setting variable if whether the player is on a wall from the right or the left

dirt = 0;					//Setting whether or not to put in dirt after period of time touching the wall (used when on a wall)
idletimer = 0;				//A idle timer
runningtimer = 0;			//A running timer
onwalltimer = 0;			//A on wall timer

inandoutofwater = 0;		//Checks if in and out of water. 0 default, 1 is in water and 2 is out of water which then sets to 0 (default) after breath = 100
is_water_surface_collision = false; //Checking if colliding with surface of water
jumpbuffer = 0;				//Buffer of time when off the ground to jump
jumpbuffer_max = 6;			//Maximum buffer time when jumping off the ground

takingdamage = false;		//Variable to check if taken damage
invincible = false;			//Variable to check if invincible
invincible_flash_timer = 0; //A invincible flash timer
scr_shader_flash_int();		//Gets shader flash variables
boostmultiplier = 0;		//Setting boostmultiplier
deathplayed = 0;			//Checking if the death effects have played whilst dead
max_deathtimer = room_speed * 2;	//Sets the max death timer to count down when the player has died
deathtimer = max_deathtimer;		//Sets the death timer to equal max death timer

show_ui_hp = false;			//If true it shows the UI of the health bar
show_ui_energy = false;		//If true it shows the UI of the energy bar
show_ui_breath = false;		//If true it shows the UI of the breath bar
#endregion

#region BG Combat Music & Berry Variables
berryeaten = 0;				//Checking which berry the player has eaten
cooldown = 0;				//Setting cooldown for berry
combatmusic = false;		//A variable used to fade in and out the combat music
combat_music = 0;			//Getting unique id of combat music played
combatmusicrange = 150;		//Variable used to check the distance to the enemy for combat music to play
endingmusictimer = 0;		//Timer to stop the bg combat music once faded out
#endregion	

#region Saving Original X & Y Positions During Intialisation, Setting Global Tilemap & Checkpoint Set X,Y
startx = x;
starty = y;
if (global.checkpointR == room) { x = global.checkpointx; y = global.checkpointy; }
global.tilemap = layer_tilemap_get_id("Collision");
#endregion	