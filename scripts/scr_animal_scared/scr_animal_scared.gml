///@description Animals Scared State

#region Calling Animal Scripts
scr_animal_move();					//Getting animals calcuation movement script
scr_animal_ani();					//Getting animals animation, visual and sound effects script
#endregion

#region If Player Exists
if (instance_exists(player))
{
	#region Setting Catchphrase
	if (catchphrase) {
		if (!audio_is_playing(snd_scared_v1)) && (!audio_is_playing(snd_scared_v2)) && (!audio_is_playing(snd_scared_v3)) {
			audio_play_sound(choose(snd_scared_v1,snd_scared_v2,snd_scared_v3),5,false);
		}
	}
	catchphrase = false;
	#endregion
	
	#region Setting Direction
	//Gets direction of the player. Subtracts the x position of the player by the x position of the animal and negates it so it moves in the opposite direction | -1 moving to the left and +1 moving to the right
	dir = -sign(player.x - x); 
	#endregion
	
	#region State Transitions
	//Scared Timer Decrease
	scaredtimer -= 1;
	
	#region Passive
	if (passive) {
		if (distance_to_object(player) >= hearingrange) || (player.state == states.death) || (scaredtimer <= 0) && (takingdamage == false) { 
			if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
			if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; }
			scaredtimer = max_scaredtimer; state = a_states.normal; 
		}
		if (energy <= 0) { 
			if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
			if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; } 
			state = a_states.normal; 
		}
	}
	#endregion
	
	#region Passive, Aggressive
	if (passive_aggressive) {
		if (distance_to_object(player) <= attackrange) && (jumpbuffer > 0) && (player.state != states.death) {
			energy -= attack_energy;
			state = a_states.attack;
		}	
		if (distance_to_object(player) >= hearingrange) || (player.state == states.death) || (scaredtimer <= 0) && (takingdamage == false) { 
			if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
			if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; }
			scaredtimer = max_scaredtimer; state = a_states.normal; 
		}
		if (energy <= 0) { 
			if (dir == -1) { x_anchor_range = x + max_range; returnable_to_x_anchor = 0; }
			if (dir == 1) { x_anchor_range = x - max_range; returnable_to_x_anchor = 0; } 
			state = a_states.normal; 
		}
	}
	#endregion
	
	#endregion
}
#endregion

scr_collision();