///@description Death Sequence For Enemy

#region Setting Movement
moveable = false;

hsp = scr_approach(hsp,0,hsp_fric_ground);
vsp += grv;
vsp = clamp(vsp,-vsp_max,vsp_max);
#endregion

#region Death Animation, Sounds & Effects
if (deathplayed == 0) {
	deathplayed = 1;

	//Setting sprite animation
	sprite_index = ani_dead;
	
	//Adding sound and visual effects
	if (instance_exists(player)) {
		if (distance_to_object(player) <= hearingrange) {
			audio_play_sound(choose(snd_death_v1,snd_death_v2,snd_death_v3),5,false);
		}
	}	
	
	//Dropping Experience
	//repeat (8) { instance_create_layer(x+random_range(-20,+20),y+random_range(-2,2),"Effects",obj_experience); }
}
#endregion

#region End Death Effects
//Setting end death animation to last frame
if (scr_animation_end()) { 
	image_index = image_number -1; 
	image_alpha -= 0.015;
	if (whiteflash < 0.7) { whiteflash += 0.05; }
	with (instance_create_layer(x,bbox_bottom,"Effects",obj_death)) { x = random_range(other.x-20,other.x+20); }
}

//Setting timer for dead enemy & setting what happens when timer reaches zero
if (deathtimer > 0) { deathtimer -= 1; }
if (deathtimer <= 0) { deathtimer = max_deathtimer; instance_destroy(); }
#endregion

scr_collision();