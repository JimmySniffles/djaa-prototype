///@description Budburra's Attack Stage 1 State

#region Getting Inputs, Checking Whether On Ground, Setting Attack Sound Effect Chance
//scr_get_inputs();
//onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0) || (place_meeting(x,y+1,par_wall)) || (place_meeting(x,y+1,par_dmgobjs));
var run_atk_snd_chance = 0.5, normal_atk_snd_chance = 0.3, special_atk_snd_chance = 0.7;
#endregion

#region Weapon Attacks
		
	#region Club Attacks
	if (obj_b_wloadout.weapon = wep_type.club) {
	
		#region Running Attack
		//if (runattack_act) && (onground) {
		
		//Setting Movement
		//if (hsp >= 1) { hsp = scr_approach(hsp,gslide_spd,gslide_fric); } else { hsp = scr_approach(hsp,-gslide_spd,gslide_fric); }
		
		//if (image_index >= 1) && (image_index <= 2) {
		////Setting Sounds
		//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
		//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		//}
		//if (scr_chance(run_atk_snd_chance)) {
		//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
		//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
		//	}
		//}
		//}
			
		//Attack
		//scr_melee_attack(ani_clb_runatk,ani_idle,obj_b_wloadout.attack_spd,ani_clb_runatk_hb,
		//				  false,true,true,true,
		//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
		//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
		//				  );
		//}
		#endregion
	
		#region Normal Attack
		
			#region On Ground
			if (nmattack_act) && (onground) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			if (image_index >= 1) && (image_index <= 2) {
			//Setting Sounds
			if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
				audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			}
			if (scr_chance(normal_atk_snd_chance)) {
				if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
					audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
				}
			}
			}
			
			//Attack
			scr_melee_attack(ani_clb_na_g_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_clb_na_g_stg1_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			//if (key_attack) && (nmattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			}
			#endregion
		
			#region In Air
			//if (nmattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(normal_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
		
			//Attack
			//scr_melee_attack(ani_clb_na_a_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_clb_na_a_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_attack) && (nmattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
		#endregion
	
		#region Special Attack
	
			#region On Ground
			//if (spattack_act) && (onground) {
			
			//Setting Movement
			//hsp = scr_approach(hsp,0,hsp_fric_ground);
			//vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//scr_melee_attack(ani_clb_sa_g_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_clb_sa_g_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
			#region In Air
			//if (spattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
		
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//scr_melee_attack(ani_clb_sa_a_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_clb_sa_a_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
		#endregion
	}
	#endregion

	#region Stun Boomerang Attacks
	if (obj_b_wloadout.weapon = wep_type.stunboomerang) {
	
		#region Running Attack
		//if (runattack_act) && (onground) {
		
		//Setting Movement
		//scr_player_move();
		
		//if (image_index >= 1) && (image_index <= 2) {
		////Setting Sounds
		//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
		//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		//}
		//if (scr_chance(run_atk_snd_chance)) {
		//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
		//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
		//	}
		//}
		//}
		
		//Attack
		//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
		//	scr_projectile_attack(ani_stb_runatk,obj_b_wloadout.attack_spd,4,5,0, point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
		//	if (instance_exists(obj_stunboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
		//}
		//}
		#endregion
	
		#region Normal Attack
		
			#region On Ground
			if (nmattack_act) && (onground) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			if (image_index >= 1) && (image_index <= 2) {
			//Setting Sounds
			if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
				audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			}
			if (scr_chance(normal_atk_snd_chance)) {
				if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
					audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
				}
			}
			}
			
			//Attack
			if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
				scr_projectile_attack(ani_stb_na_g,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
				if (instance_exists(obj_stunboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			}
			}
			#endregion
		
			#region In Air
			//if (nmattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(normal_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
			//	scr_projectile_attack(ani_stb_na_a,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
			//	if (instance_exists(obj_stunboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			//}
			//}
			#endregion
		
		#endregion
	
		#region Special Attack
	
			#region On Ground
			//if (spattack_act) && (onground) {
			
			//Setting Movement
			//hsp = scr_approach(hsp,0,hsp_fric_ground);
			//vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//scr_melee_attack(ani_stb_sa_g_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_stb_sa_g_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
			#region In Air
			//if (spattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//scr_melee_attack(ani_stb_sa_a_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_stb_sa_a_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
		#endregion
	}
	#endregion

	#region Spear Attacks
	if (obj_b_wloadout.weapon = wep_type.spear) {
	
		#region Running Attack
		//if (runattack_act) && (onground) {
		
		//Setting Movement
		//scr_player_move();
		
		//if (image_index >= 1) && (image_index <= 2) {
		////Setting Sounds
		//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
		//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		//}
		//if (scr_chance(run_atk_snd_chance)) {
		//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
		//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
		//	}
		//}
		//}
		
		//Attack
		//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
		//	scr_projectile_attack(ani_spr_runatk,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
		//	if (instance_exists(obj_spear)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
		//}
		//}
		#endregion
	
		#region Normal Attack
		
			#region On Ground
			if (nmattack_act) && (onground) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			if (image_index >= 1) && (image_index <= 2) {
			//Setting Sounds
			if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
				audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			}
			if (scr_chance(normal_atk_snd_chance)) {
				if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
					audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
				}
			}
			}
			
			//Attack
			if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
				scr_projectile_attack(ani_spr_na_g,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
				if (instance_exists(obj_spear)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			}
			}
			#endregion
		
			#region In Air
			//if (nmattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(normal_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
			//	scr_projectile_attack(ani_spr_na_a,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
			//  if (instance_exists(obj_spear)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			//}
			//}
			#endregion
		
		#endregion
	
		#region Special Attack
	
			#region On Ground
			if (spattack_act) && (onground) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			if (image_index >= 1) && (image_index <= 2) {
			//Setting Sounds
			if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
				audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			}
			if (scr_chance(special_atk_snd_chance)) {
				if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
					audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
				}
			}
			}
			
			//Attack
			scr_melee_attack(ani_spr_sa_g_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_spr_sa_g_stg1_hb,
							false,true,true,true,
							obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
							0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
							);
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			}
			#endregion
		
			#region In Air
			//if (spattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
		
			//Attack
			//scr_melee_attack(ani_spr_sa_a_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_spr_sa_a_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
		#endregion
	}
	#endregion

	#region Kill Boomerang Attacks
	if (obj_b_wloadout.weapon = wep_type.killboomerang) {
	
		#region Running Attack
		//if (runattack_act) && (onground) {
		
		////Setting Movement
		//scr_player_move();
		
		//if (image_index >= 1) && (image_index <= 2) {
		////Setting Sounds
		//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
		//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		//}
		//if (scr_chance(run_atk_snd_chance)) {
		//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
		//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
		//	}
		//}
		//}
		
		//Attack
		//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
		//	scr_projectile_attack(ani_klb_runatk,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
		//	if (instance_exists(obj_killboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
		//}
		//}
		#endregion
	
		#region Normal Attack
		
			#region On Ground
			if (nmattack_act) && (onground) {
			
			//Setting Movement
			hsp = scr_approach(hsp,0,hsp_fric_ground);
			vsp = scr_approach(vsp,0,hsp_fric_air);
			
			if (image_index >= 1) && (image_index <= 2) {
			//Setting Sounds
			if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
				audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			}
			if (scr_chance(normal_atk_snd_chance)) {
				if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
					audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
				}
			}
			}
			
			//Attack
			if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
				scr_projectile_attack(ani_klb_na_g,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
				if (instance_exists(obj_killboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			}
			}
			#endregion
		
			#region In Air
			//if (nmattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(normal_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//if (obj_b_wloadout.ammo[obj_b_wloadout.weapon] != 0) {
			//	scr_projectile_attack(ani_klb_na_a,obj_b_wloadout.attack_spd,4,5,0,point_direction(x,y,mouse_x,mouse_y),obj_b_wloadout.projectile,obj_b_wloadout.proj_spd);
			//	if (instance_exists(obj_killboomerang)) { obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0; }
			//}
			//}
			#endregion
		
		#endregion
	
		#region Special Attack
	
			#region On Ground
			//if (spattack_act) && (onground) {
			
			//Setting Movement
			//hsp = scr_approach(hsp,0,hsp_fric_ground);
			//vsp = scr_approach(vsp,0,hsp_fric_air);
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
			
			//Attack
			//scr_melee_attack(ani_klb_sa_g_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_klb_sa_g_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
			#region In Air
			//if (spattack_act) && (!onground) {
		
			//Setting Movement
			//scr_player_move();
			
			//if (image_index >= 1) && (image_index <= 2) {
			////Setting Sounds
			//if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			//	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			//}
			//if (scr_chance(special_atk_snd_chance)) {
			//	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)){
			//		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
			//	}
			//}
			//}
		
			//Attack
			//scr_melee_attack(ani_klb_sa_a_stg1,ani_idle,obj_b_wloadout.attack_spd,ani_klb_sa_a_stg1_hb,
			//				  false,true,true,true,
			//				  obj_b_wloadout.scrshk_mag,obj_b_wloadout.scrshk_frm,
			//				  0,obj_b_wloadout.ene_particles,obj_b_wloadout.ani_particles,obj_b_wloadout.env_particles,0,obj_b_wloadout.ene_dmg,obj_b_wloadout.ani_dmg,obj_b_wloadout.env_dmg
			//				  );
		
			//Trigger Next Attack Stage
			//if (key_spattack) && (spattack_act) && (energy > 0) && (onground) && (image_index > 3) { state = states.attack_stg2; energy -= obj_b_wloadout.energy; }
			//}
			#endregion
		
		#endregion
	}
	#endregion
	
#endregion
	
#region End Attack Stage 1 Effects
if (scr_animation_end()) { runattack_act = false; nmattack_act = false; spattack_act = false; image_speed = 1; state = states.normal; }
#endregion

scr_collision();


/*



	#region Setting Movement
    hsp = scr_approach(hsp,0,hsp_fric_ground);
    vsp = scr_approach(vsp,0,hsp_fric_air);
	#endregion
	
	//Animation & Speed
	if (sprite_index != ani_attack) {
	sprite_index = ani_attack;
	image_index = 0;
	image_speed = obj_b_wloadout.casting;
	ds_list_clear(hitbyattack);
	}

	//Use Attack Hitbox & Check For Hits
	mask_index = spr_boy_hitbox2;
	var hitbyattack_now = ds_list_create();				//Stores ID of every instance that we hit on this frame
	
	//Returns the raw hits that are collided with
	var enemy_hits = instance_place_list(x,y,par_enemy,hitbyattack_now,false);		//If ordered is true it will return the ID's in the order of closest to furthest. Great if you want to get a bunch of collisions and have certain things happen based upon the order of distance.
	var animal_hits = instance_place_list(x,y,par_animal,hitbyattack_now,false);
	var dmgobjs_hits = instance_place_list(x,y,par_dmgobjs,hitbyattack_now,false);
	
	if (animal_hits > 0) {
		//What this for loop does is that it checks the amount of hits and loops to reach the amount of hits than stops
		for (var i = 0; i < animal_hits; i++) {
			//If this instance has not yet been hit by this attack. What the | does is that its a quick way to find a particular entry in a DS List
			var animal_hitID = hitbyattack_now[| i];
			//Checks if a particular value actually exists in the DS List. Checks if something does not already exist in the list that we attacked so we don't hit the same instance more than once.
			if (ds_list_find_index(hitbyattack,animal_hitID) == -1) {
				ds_list_add(hitbyattack,animal_hitID);
				with (animal_hitID) {
					if (state != a_states.death) { 
						if (!audio_is_playing(sn_meleehitwd_ai_v1)) && (!audio_is_playing(sn_meleehitwd_ai_v2)) && (!audio_is_playing(sn_meleehitwd_ai_v3)) && (!audio_is_playing(sn_meleehitwd_ai_v4)) && (!audio_is_playing(sn_meleehitwd_ai_v5)) && (!audio_is_playing(sn_meleehitwd_ai_v6)) {
							audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
						}
						scr_screenshake(2,20);
						repeat(3) { instance_create_layer(x,bbox_top,"Effects",obj_hitspark); }
						hp -= obj_b_wloadout.damage;
					}
				}
			}
		}	
	}
	ds_list_destroy(hitbyattack_now);			//Destroy list that we have at this step
	mask_index = ani_idle;						//Returns To Original Hit Mask
}
#endregion	

//State Transition
//Trigger next attack within a certain image_index

if (key_attack) && (energy > 0) && (image_index > 2) { 
	if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
	}
	if (scr_chance(0.3)) {
		if (!audio_is_playing(sn_attack_v1)) && (!audio_is_playing(sn_attack_v2)) && (!audio_is_playing(sn_throw)){
			audio_play_sound(choose(sn_attack_v1,sn_attack_v2,sn_throw),5,false);
		}
	}
	energy -= obj_b_wloadout.energy;
	state = states.attack_g_2; 
}

if (scr_animation_end()) { state = states.normal; notthrown = true; spattackactivated = false; image_speed = 1; }

scr_collision();
	
	if (image_index >= 3) && (image_index <= 5)
	{
		with (instance_create(x,y,obj_hitbox))
		{
			image_xscale = other.image_xscale;
			with (instance_place(x,y,par_enemy))
			{
				if (state != e_states.death) { 
					if (takinghit == 0)
					{
						if (!audio_is_playing(sn_meleehitwd_ai_v1)) && (!audio_is_playing(sn_meleehitwd_ai_v2)) && (!audio_is_playing(sn_meleehitwd_ai_v3)) && (!audio_is_playing(sn_meleehitwd_ai_v4)) && (!audio_is_playing(sn_meleehitwd_ai_v5)) && (!audio_is_playing(sn_meleehitwd_ai_v6)) {
							audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
						}
						scr_screenshake(2,20);
						repeat(3) { instance_create_layer(x,bbox_top,"Effects",obj_hitspark); }
						hp -= obj_b_wloadout.damage/3;
						takinghit = 3;
						hitfrom = other.direction;
					}
				}
			}
			with (instance_place(x,y,obj_dmgrock))
			{
				if (envhit == 0)
				{
					if (!audio_is_playing(sn_meleehitrock_v1)) && (!audio_is_playing(sn_meleehitrock_v2)) && (!audio_is_playing(sn_meleehitrock_v3)) {
						audio_play_sound(choose(sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3),5,false);
					}
					scr_screenshake(4,20);
					if (x < obj_boy.x) {
					//Than melee swing is swinging from the right side
					repeat(5) { instance_create_layer(bbox_right,bbox_top + 75,"Effects",obj_hitspark); }
					repeat(5) { scr_dirtparticle(bbox_right, bbox_top + 75, dirtpart.jumping); }
					}
					if (x > obj_boy.x) {
					//Than melee swing is swinging from the left side
					repeat(5) { instance_create_layer(bbox_left,bbox_top + 75,"Effects",obj_hitspark); }
					repeat(5) { scr_dirtparticle(bbox_left, bbox_top + 75, dirtpart.jumping); }
					}
					hp -= obj_b_wloadout.envdamage/3;
					envhit = 3;
				}
			}
			with (instance_place(x,y,obj_dmgroot)) {
				if (envhit == 0)
				{
					audio_play_sound(choose(sn_meleehitroot_v1,sn_meleehitroot_v2),5,false);
					scr_screenshake(1,15);
					if (x < obj_boy.x) {
					//Than melee swing is swinging from the right side
					repeat(5) { instance_create_layer(bbox_right - 5,bbox_top + 140,"Effects",obj_hitspark); }
					repeat(5) { scr_dirtparticle(bbox_right - 5, bbox_top + 140, dirtpart.jumping); }
					}
					if (x > obj_boy.x) {
					//Than melee swing is swinging from the left side
					repeat(5) { instance_create_layer(bbox_left + 5,bbox_top + 140,"Effects",obj_hitspark); }
					repeat(5) { scr_dirtparticle(bbox_left - 5, bbox_top + 140, dirtpart.jumping); }
					}
					hp -= obj_b_wloadout.envdamage/3;
					envhit = 3;
				}
			}
		}
	}
}
#endregion

#region Stun Boomerang Weapon
if (obj_b_wloadout.weapon = 2) {
	
	//Friction
    hsp = scr_approach(hsp,0,hsp_fric_ground);
    vsp = scr_approach(vsp,0,hsp_fric_air);
	
	//Animate & Speed
	sprite_index = spr_boy_throw_b;
	image_speed = obj_b_wloadout.casting;
	
	if (image_index >= 4) && (image_index <= 5) {
		if (notthrown) {
			notthrown = false;
			hasweapon = false;
			with (instance_create_layer(x+lengthdir_x(obj_b_wloadout.length,obj_b_wloadout.direction),y+lengthdir_y(obj_b_wloadout.length,obj_b_wloadout.direction),"Projectiles",obj_b_wloadout.projectile))
				{
					direction = obj_b_wloadout.direction;
					speed = obj_b_wloadout.airspeed;
				}
		}
	}
}
#endregion

#region Spear Weapon
if (obj_b_wloadout.weapon = 3) {
	if (spattackactivated = false) {
		
		//Friction
	    hsp = scr_approach(hsp,0,hsp_fric_ground);
	    vsp = scr_approach(vsp,0,hsp_fric_air);
	
		//Animate & Speed
		sprite_index = spr_boy_spear;
		image_speed = obj_b_wloadout.casting;

		//Hitbox
		if (image_index >= 2) && (image_index <= 4)
		{
			with (instance_create(x,y,obj_s_hitbox))
			{
				image_xscale = other.image_xscale;
				with (instance_place(x,y,par_enemy))
				{
					if (state != e_states.death)  {
						if (takinghit == 0)
						{
							if (!audio_is_playing(sn_meleehitwd_ai_v1)) && (!audio_is_playing(sn_meleehitwd_ai_v2)) && (!audio_is_playing(sn_meleehitwd_ai_v3)) && (!audio_is_playing(sn_meleehitwd_ai_v4)) && (!audio_is_playing(sn_meleehitwd_ai_v5)) && (!audio_is_playing(sn_meleehitwd_ai_v6)) {
								audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
							}
							scr_screenshake(2,15);
							repeat(4) { instance_create_layer(x,bbox_top,"Effects",obj_hitspark); }
							hp -= obj_b_wloadout.damage/3;
							takinghit = 3;
							hitfrom = other.direction;
						}
					}
				}
				with (instance_place(x,y,obj_dmgrock))
				{
					if (envhit == 0)
					{
						if (!audio_is_playing(sn_meleehitrock_v1)) && (!audio_is_playing(sn_meleehitrock_v2)) && (!audio_is_playing(sn_meleehitrock_v3)) {
							audio_play_sound(choose(sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3),5,false);
						}
						scr_screenshake(3,15);
						if (x < obj_boy.x) {
						//Than melee swing is swinging from the right side
						repeat(4) { instance_create_layer(bbox_right,bbox_top + 95,"Effects",obj_hitspark); }
						repeat(4) { scr_dirtparticle(bbox_right, bbox_top + 95, dirtpart.jumping); }
						}
						if (x > obj_boy.x) {
						//Than melee swing is swinging from the left side
						repeat(4) { instance_create_layer(bbox_left,bbox_top + 95,"Effects",obj_hitspark); }
						repeat(4) { scr_dirtparticle(bbox_left, bbox_top + 95, dirtpart.jumping); }
						}
						hp -= obj_b_wloadout.envdamage/3;
						envhit = 3;
					}
				}
				with (instance_place(x,y,obj_dmgroot)) {
					if (envhit == 0)
					{
						audio_play_sound(choose(sn_meleehitroot_v1,sn_meleehitroot_v2),5,false);
						scr_screenshake(1,15);
						if (x < obj_boy.x) {
						//Than melee swing is swinging from the right side
						repeat(4) { instance_create_layer(bbox_right - 30,bbox_top + 160,"Effects",obj_hitspark); }
						repeat(4) { scr_dirtparticle(bbox_right - 30, bbox_top + 160, dirtpart.jumping); }
						}
						if (x > obj_boy.x) {
						//Than melee swing is swinging from the left side
						repeat(4) { instance_create_layer(bbox_left + 30,bbox_top + 160,"Effects",obj_hitspark); }
						repeat(4) { scr_dirtparticle(bbox_left - 30, bbox_top + 160, dirtpart.jumping); }
						}
						hp -= obj_b_wloadout.envdamage/3;
						envhit = 3;
					}
				}
			}
		}	
	}
	if (spattackactivated) {
	
		//Friction
	    hsp = scr_approach(hsp,0,hsp_fric_ground);
	    vsp = scr_approach(vsp,0,hsp_fric_air);
	
		//Animate & Speed
		sprite_index = spr_boy_throw_s;
		image_speed = obj_b_wloadout.casting;
		
		if (image_index >= 4) && (image_index <= 5) {
			if (notthrownspear) {
				notthrownspear = false;
				with (instance_create_layer(x+lengthdir_x(obj_b_wloadout.length,obj_b_wloadout.direction),y+lengthdir_y(obj_b_wloadout.length,obj_b_wloadout.direction),"Projectiles",obj_b_wloadout.projectile))
					{
						direction = obj_b_wloadout.direction;
						speed = obj_b_wloadout.airspeed;
						image_angle = direction;
						obj_b_wloadout.ammo[obj_b_wloadout.weapon] = 0;
					}
			}
		}
	}
}
#endregion

#region Killing Boomerang Weapon
if (obj_b_wloadout.weapon = 4) {
	
	//Friction
    hsp = scr_approach(hsp,0,hsp_fric_ground);
    vsp = scr_approach(vsp,0,hsp_fric_air);
	
	//Animate & Speed
	sprite_index = spr_boy_throw_kb;
	image_speed = obj_b_wloadout.casting;
	
	if (image_index >= 4) && (image_index <= 5) {
		if (notthrown) {
			notthrown = false;
			hasweapon = false;
			with (instance_create_layer(x+lengthdir_x(obj_b_wloadout.length,obj_b_wloadout.direction),y+lengthdir_y(obj_b_wloadout.length,obj_b_wloadout.direction),"Projectiles",obj_b_wloadout.projectile))
				{
					direction = obj_b_wloadout.direction;
					speed = obj_b_wloadout.airspeed;
				}
		}
	}
}
#endregion

//State Transition
if (scr_animation_end()) { state = states.normal; notthrown = true; spattackactivated = false; image_speed = 1; }

scr_collision();