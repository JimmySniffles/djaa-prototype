///@description Hope's normal state

#region Calling player scripts
scr_get_inputs();					//Getting inputs script
scr_player_move();					//Getting player's calcuation movement script
scr_player_ani();					//Getting players animation, visual and sound effects script
#endregion

#region Jumping Abilities

	#region Ground Jump
	if (key_jump) && (jumpbuffer > 0) && (vsp >= 0) && (jumps > 0) && (energy > 0)
	{
		if (scr_chance(0.1)) {
		if (!audio_is_playing(snd_jump_v1)) && (!audio_is_playing(snd_jump_v2)) && (!audio_is_playing(snd_jump_v3)) 
		audio_play_sound(choose(snd_jump_v1,snd_jump_v2,snd_jump_v3),5,false);
		}
	
		repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
	
		if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
			var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			audio_sound_pitch(jump, choose(1,1.1,1.2));
		}
		vsp = vsp_jump; energy -= e_jumpuse; jumps -= 1; show_ui_energy = true; vsp_frac = 0; onground = false; onplatform = false; onobject = false;
	}
	#endregion

	#region Double Jump
	if (!onground) && (key_jump) && (jumps > 0) && (energy > 0)
	{
		if (scr_chance(0.1)) {
		if (!audio_is_playing(snd_jump_v1)) && (!audio_is_playing(snd_jump_v2)) && (!audio_is_playing(snd_jump_v3)) 
		audio_play_sound(choose(snd_jump_v1,snd_jump_v2,snd_jump_v3),5,false);
		}
	
		if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
			var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			audio_sound_pitch(jump, choose(1,1.1,1.2));
		}
	
		jumps -= 1; vsp = vsp_jump; energy -= e_jumpuse; show_ui_energy = true; onground = false; onplatform = false; onobject = false; vsp_frac = 0;
	}
	//Variable Jump
	if (vsp < 0) && (!key_jump_held) vsp = max(vsp,vsp_jump/2);
	#endregion

	#region Wall Jump
	if (onwall != 0) && (jumpbuffer <= 0) && (key_jump) && (energy > 0)
	{
		walljumpdelay = walljumpdelay_max;
		hsp = -onwall * hsp_wjump;
		vsp = vsp_wjump;
	
		if (scr_chance(0.3)) {
		if (!audio_is_playing(snd_jump_v1)) && (!audio_is_playing(snd_jump_v2)) && (!audio_is_playing(snd_jump_v3)) 
		audio_play_sound(choose(snd_jump_v1,snd_jump_v2,snd_jump_v3),5,false);
		}
	
		var side = bbox_left;
		if (onwall == 1) side = bbox_right;
		repeat(dirt_walljumping) { scr_dirtparticle(side, y, dirtpart.jumping); }
	
		if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
			var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
			audio_sound_pitch(jump, choose(1,1.1,1.2));
		}
		jumps -= 1; energy -= e_jumpuse; show_ui_energy = true; hsp_frac = 0; vsp_frac = 0;
	}
	#endregion

#endregion

#region Rolling & Ground Sliding

	#region Rolling
	if (key_up) && (jumpbuffer > 0) && (energy > 0) && (sprinting) && (hsp != 0) { 
		if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
			audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		}
		if (scr_chance(0.3)) {
			if (!audio_is_playing(snd_jump_v1)) audio_play_sound(snd_jump_v1,5,false);
		}
		repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
		energy -= e_rolluse; show_ui_energy = true; image_index = 0; state = states.dodge; 
	} 
	#endregion
	
	#region Ground Sliding
	if (key_down) && (jumpbuffer > 0) && (energy > 0) && (sprinting) && (hsp != 0) && (!collided) { 
		energy -= e_gslideuse; show_ui_energy = true; image_index = 0; state = states.gslide; 
	}
	#endregion
	
#endregion

#region Pushing Objects
if (jumpbuffer > 0) {
	if (key_right) || (key_left) { if (instance_place(x+hsp,y,obj_push)) { state = states.push; } }
}
#endregion

#region Climbing Objects
//Change to climbing state if on a climbable object and the key_up is pressed
var climbing_object_check_up = false; var climbing_object_check_down = false;
if (global.computer_use) { 
	if (place_meeting(x,y,par_climb)) && (energy > 0) && (key_up) { climbing_object_check_up = true; } else { climbing_object_check_up = false; }
	if (place_meeting(x,y,par_climb)) && (energy > 0) && (key_down) && (!onground) { climbing_object_check_down = true; } else { climbing_object_check_down = false; }
}
if (global.mobile_use) {
	if (climb_delay > 0) { climb_delay --; }
	if (place_meeting(x,y,par_climb)) && (energy > 0) && (climb_delay <= 0) { climbing_object_check_up = true; } else { climbing_object_check_up = false; }
	if (place_meeting(x,y,par_climb)) && (energy > 0) && (climb_delay <= 0) && (!onground) { climbing_object_check_down = true; } else { climbing_object_check_down = false; }
}
if (climbing_object_check_up) || (climbing_object_check_down)
{
	hsp = scr_approach(hsp,0,hsp_fric_ground);
	vsp = scr_approach(vsp,0,hsp_fric_air);
	if (!onground) && (!onplatform) && (!onobject) { 
		var climb_grab_particles = 2;
		repeat(climb_grab_particles) { scr_dirtparticle(bbox_left, bbox_top, dirtpart.jumping); }
		repeat(climb_grab_particles) { scr_dirtparticle(bbox_right, bbox_top, dirtpart.jumping); }
	}
	energy -= e_rolluse; show_ui_energy = true; image_speed = 0; state = states.climb;
}
#endregion

#region Interaction

	#region Interaction With Berries
	if (instance_exists(obj_berries)) {
		var withinberrydist = (collision_circle(x,y,30,obj_berries,false,true));
		if (key_interact) && (onground) && (cooldown = 0) && (withinberrydist != noone)
		{
			state = states.interact;
		}
	}
	#endregion
	
#endregion

scr_collision();