///@descripton Calculating Collision

#region Intialize Variables
var bbox_side, t_col1, t_col2;
var hsp_final = hsp + hsp_carry;
var vsp_final = vsp + vsp_carry;
hsp_carry = 0; vsp_carry = 0;				//Resets carry so the object does not keep boosted speed from moving platform
#endregion

#region Horizontal Collision

	#region Tilemap Horizontal Collision
	if (hsp_final > 0) bbox_side = bbox_right; else bbox_side = bbox_left;
	t_col1 = tilemap_get_at_pixel(global.tilemap,bbox_side+hsp_final,bbox_top);
	t_col2 = tilemap_get_at_pixel(global.tilemap,bbox_side+hsp_final,bbox_bottom);
	if (tilemap_get_at_pixel(global.tilemap,x,bbox_bottom) > 1) t_col2 = 0; //Ignore bottom side tiles if on a slope.
	if (t_col1 == 1) || (t_col2 == 1) {
		if (hsp_final > 0) x = x - (x mod TILE_SIZE) + (TILE_SIZE - 1) - (bbox_right - x);				//TILE_SIZE	is a set macro in the initialize room. It is currently set to 64 for the 64 pixels for each tile.
		else x = x - (x mod TILE_SIZE) - (bbox_left - x);
		hsp_final = 0;
		hsp_frac = 0;	
		collided = true;
	}
	#endregion

	#region Objects Horizontal Collision
	if (place_meeting(x+hsp_final,y,par_objects)) {
		var onepixel = sign(hsp_final);
		while (!place_meeting(x+onepixel,y,par_objects)) { x += onepixel; }
		hsp_final = 0;
		hsp_frac = 0;
		collided = true;
	}
	#endregion
	
if (t_col1 == 0) && (t_col2 == 0) && (!place_meeting(x+1,y,par_objects)) && (!place_meeting(x-1,y,par_objects)) { collided = false; }
x += hsp_final;

#endregion

#region Vertical Collision

	#region Tilemap Vertical Collision
	//Is this not a slope?
	if (tilemap_get_at_pixel(global.tilemap,x,bbox_bottom+vsp_final) <= 1) {
		if (vsp_final >= 0) bbox_side = bbox_bottom; else bbox_side = bbox_top;
		t_col1 = tilemap_get_at_pixel(global.tilemap,bbox_left,bbox_side+vsp_final);
		t_col2 = tilemap_get_at_pixel(global.tilemap,bbox_right,bbox_side+vsp_final);
		if (t_col1 == 1) || (t_col2 == 1) {
			if (vsp_final >= 0) y = y - (y mod TILE_SIZE) + (TILE_SIZE - 1) - (bbox_bottom - y);		
			else y = y - (y mod TILE_SIZE) - (bbox_top - y);
			vsp_final = 0;
			vsp_frac = 0;	
		}
	}

	var floordist = scr_infloor(global.tilemap,x,bbox_bottom+vsp_final)
	if (floordist >= 0) {
		y += vsp_final;
		y -= (floordist+1);		
		vsp_final = 0;
		vsp_frac = 0;
		floordist = -1;
	}
	#endregion
	
	#region Objects Vertical Collision
	if (place_meeting(x,y+vsp_final,par_objects)) {
		var onepixel = sign(vsp_final);
		while (!place_meeting(x,y+onepixel,par_objects)) { y += onepixel; }
		vsp_final = 0;
		vsp_frac = 0;
	}
	#endregion
	
	#region One Way Platforms Collision
	var nearest_platform = noone;
	if (instance_exists(par_oneway_platform)) { 
		nearest_platform = instance_nearest(x,y+1,par_oneway_platform); 
		if (nearest_platform != noone) {
			if (nearest_platform.bbox_top > bbox_bottom && vsp_final >= 0) {
			    if (place_meeting(x,y+vsp_final,par_oneway_platform)) {
					var onepixel = sign(vsp_final);
			        while (!place_meeting(x,y+onepixel,par_oneway_platform)) { y += onepixel; }
					vsp_final = 0;
					vsp_frac = 0;
			    } 
			} 
		}
	}
	#endregion
	
y += vsp_final;

#endregion

#region Moving Platform Collision & Setting Objects hsp & vsp
//Horizontal Moving Platforms
//var moving_platform = instance_place(x,y+1,obj_hmvingplatform);
//if (instance_exists(moving_platform)) { hsp = moving_platform.hsp; }
	
//Vertical Moving Platforms
//Check for a jump through platform
if (my_platform == noone) { //If you're not already on a platform
var jump_through = instance_place(x,y+vsp_final,obj_movingplatform);
	if (jump_through) { //this is a jumpthrough platform
		if ((vsp_final >= 0) && (!place_meeting(x,y,jump_through))) {	//we are moving down and should collide
			vsp_final = 0;
			vsp_frac = 0;
			my_platform = jump_through;
		}
	}
} else {
	//Set the platform to noone if a object is no longer colliding with it
	if (bbox_right < my_platform.bbox_left || bbox_left > my_platform.bbox_right || vsp_final < 0) { my_platform = noone; }
}
//Snap the player to the top of the platform
if (my_platform) { y += my_platform.bbox_top - bbox_bottom; }
#endregion

#region Walk Down Slopes
if (onground) {
	y += abs(floordist)-1;
	//if at base of current tile
	if ((bbox_bottom mod TILE_SIZE) == TILE_SIZE-1) {
		// if slope continues
		if (tilemap_get_at_pixel(global.tilemap,x,bbox_bottom+1) > 1) {
			//move there
			y += abs(scr_infloor(global.tilemap,x,bbox_bottom+1));
		}
	}
}
#endregion