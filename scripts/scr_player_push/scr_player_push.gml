///@description Pushing Objects Mechanic

#region Getting Inputs & Movement Calculations
scr_get_inputs();
scr_player_move();
#endregion

#region Pushing Animation, Sounds & Effects
//Setting sprite & mask
sprite_index = spr_girl_push;
mask_index = ani_push;

//Particle Effects & Sound
if (image_index >= 4) && (image_index <= 5) || (image_index >= 12) && (image_index <= 13) {
	if (!audio_is_playing(sn_gstep_v1)) && (!audio_is_playing(sn_gstep_v2)) && (!audio_is_playing(sn_gstep_v3)) && (!audio_is_playing(sn_gstep_v4))  && (!audio_is_playing(sn_gstep_v5)) { 
		var moving = audio_play_sound(choose(sn_gstep_v1,sn_gstep_v2,sn_gstep_v3,sn_gstep_v4,sn_gstep_v5),3,false);
		audio_sound_pitch(moving, choose(0.9,1,1.1,1.2));
	}
	var side; if (hsp >= 0) { side = bbox_right-6; } else { side = bbox_left+6; }
	repeat(dirt_moving_wlk) { scr_dirtparticle(side, bbox_bottom, dirtpart.movement); }
}

//Setting sprite speed animation
if (key_left) {
	if (hsp >= 0) { image_speed = -1; } else { image_speed = 1; }
}
if (key_right) {
	if (hsp >= 0) { image_speed = 1; } else { image_speed = -1; }
}
if (!key_left) && (!key_right) { image_speed = 0; }
#endregion

#region Pushing & Pulling Mechanic
var push = 0; var pull = 0; var pushobject;
if (hsp >= 0) && (key_right) { push = 1; } if (hsp < 0) && (key_left) { push = 1; }
if (hsp >= 0) && (key_left) && (keyboard_check(ord("E"))) { pull = 1; } if (hsp < 0) && (key_right) && (keyboard_check(ord("E"))) { pull = 1; }


if (push = 1) { 
	pushobject = instance_place(x+hsp,y,obj_push);
	with (pushobject) { 
		hsp = obj_girl.hsp;
		//Sound Of Object Being Push Along Grass
		if (!collided) {
			if (!audio_is_playing(sn_pushobject_v1)) && (!audio_is_playing(sn_pushobject_v2)) && (!audio_is_playing(sn_pushobject_v3)) {
				audio_play_sound(choose(sn_pushobject_v1,sn_pushobject_v2,sn_pushobject_v3),6,false);
			}
		}
	}
}

if (pull = 1) { 
	pushobject = instance_place(x+1,y,obj_push);
	with (pushobject) { 
		if (!collided) && (x-sprite_width/2 >= other.x) {
			other.image_xscale = 1; x += other.hsp; other.x = x-sprite_width/2; 
		} 
	}
}

//Debugging
//show_debug_message("hsp = "+string(hsp));
//show_debug_message("push = "+string(push));
	//show_debug_message(hsp);
	/*
if (place_meeting(x+1,y,obj_push)) || (place_meeting(x-1,y,obj_push))
{
    var pushobject = instance_place(x+1,y,obj_push)
    if (pushobject == noone) && (keyboard_check(ord("F")))
    {
        pushobject = instance_place(x-1,y,obj_push)
    }
    with (pushobject)
    {
		hsp = obj_girl.hsp;
    }
} */
hsp /= 2;
#endregion

#region If No Longer Colliding With Object Or Jump Off
if (push == 0) && (pull == 0) {
		if (audio_is_playing(sn_pushobject_v1)) || (audio_is_playing(sn_pushobject_v2)) || (audio_is_playing(sn_pushobject_v3)) {
			audio_stop_sound(sn_pushobject_v1); audio_stop_sound(sn_pushobject_v2); audio_stop_sound(sn_pushobject_v3);
		}
	mask_index = ani_idle; image_speed = 1; state = states.normal;
}

if (collided) { 
	if (audio_is_playing(sn_pushobject_v1)) || (audio_is_playing(sn_pushobject_v2)) || (audio_is_playing(sn_pushobject_v3)) {
		audio_stop_sound(sn_pushobject_v1); audio_stop_sound(sn_pushobject_v2); audio_stop_sound(sn_pushobject_v3);
	}
}

//if (!instance_place(x+hsp,y,obj_push)) {
//	if (audio_is_playing(sn_pushobject_v1)) && (audio_is_playing(sn_pushobject_v2)) && (audio_is_playing(sn_pushobject_v3)) {
//		audio_stop_sound(sn_pushobject_v1); audio_stop_sound(sn_pushobject_v2); audio_stop_sound(sn_pushobject_v3);
//	}
//	mask_index = ani_idle; image_speed = 1; state = states.normal;
//}

if (!onground) && (!onplatform) && (!onobject) { mask_index = ani_idle; image_speed = 1; state = states.normal; }

if (key_jump) && (energy > 0) {
	if (scr_chance(0.1)) {
	if (!audio_is_playing(snd_jump_v1)) && (!audio_is_playing(snd_jump_v2)) && (!audio_is_playing(snd_jump_v3)) 
	audio_play_sound(choose(snd_jump_v1,snd_jump_v2,snd_jump_v3),5,false);
	}
	
	repeat(dirt_jumping) { scr_dirtparticle(x, bbox_bottom, dirtpart.jumping); }
	
	if (!audio_is_playing(sn_meleeswing_v1)) || (!audio_is_playing(sn_meleeswing_v2)) || (!audio_is_playing(sn_meleeswing_v3)) {
		var jump = audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
		audio_sound_pitch(jump, choose(1,1.1,1.2));
	}
	vsp = vsp_jump; energy -= e_jumpuse; jumps -= 1; show_ui_energy = true; vsp_frac = 0; 
	onground = false; image_speed = 1; mask_index = ani_idle; state = states.normal;
}
#endregion

scr_collision();