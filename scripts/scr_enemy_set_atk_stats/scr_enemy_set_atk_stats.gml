///@description Setting the enemy weapon attack stats
/// @param enemy_type_wep

e_type_wep				= argument0;
var enemy_wp_map		= enemy_attack[e_type_wep];

//The ? is a accessor to find the value of ds map. Its the same as ds_map_find_value which find the value of the specified item.
ply_dmg					= enemy_wp_map[? "ply_dmg"];
ani_dmg					= enemy_wp_map[? "ani_dmg"];
env_dmg					= enemy_wp_map[? "env_dmg"];

attack_spd				= enemy_wp_map[? "attack_spd"];
attack_energy			= enemy_wp_map[? "attack_energy"];
stun					= enemy_wp_map[? "stun"];
knockback				= enemy_wp_map[? "knockback"];
knockback_str			= enemy_wp_map[? "knockback_str"];

scrshk_mag				= enemy_wp_map[? "scrshk_mag"];
scrshk_frm				= enemy_wp_map[? "scrshk_frm"];
ply_particles			= enemy_wp_map[? "ply_particles"];
ani_particles			= enemy_wp_map[? "ani_particles"];
env_particles			= enemy_wp_map[? "env_particles"];

projectile				= enemy_wp_map[? "projectile"];
proj_spd				= enemy_wp_map[? "proj_spd"];
proj_dist				= enemy_wp_map[? "proj_dist"];