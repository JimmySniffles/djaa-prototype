///@description Setting the players weapon stats
/// @param weapon_type

weapon					= argument0;
var wp_map				= weapons[weapon];

//The ? is a accessor to find the value of ds map. Its the same as ds_map_find_value which find the value of the specified item.
ene_dmg					= wp_map[? "ene_dmg"];
ani_dmg					= wp_map[? "ani_dmg"];
env_dmg					= wp_map[? "env_dmg"];

attack_spd				= wp_map[? "attack_spd"];
attack_energy			= wp_map[? "attack_energy"];
stun					= wp_map[? "stun"];
knockback				= wp_map[? "knockback"];
knockback_str			= wp_map[? "knockback_str"];

scrshk_mag				= wp_map[? "scrshk_mag"];
scrshk_frm				= wp_map[? "scrshk_frm"];
ene_particles			= wp_map[? "ene_particles"];
ani_particles			= wp_map[? "ani_particles"];
env_particles			= wp_map[? "env_particles"];

projectile				= wp_map[? "projectile"];
proj_spd				= wp_map[? "proj_spd"];
proj_dist				= wp_map[? "proj_dist"];