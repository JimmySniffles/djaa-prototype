///@description Animals Chase State

#region Calling Animal Scripts
scr_animal_move();					//Getting animals calcuation movement script
scr_animal_ani();					//Getting animals animation, visual and sound effects script
#endregion

#region If Player Exists
if (instance_exists(player))
{
	#region Setting Catchphrase
	if (catchphrase) {
		audio_play_sound(choose(snd_chase_v1,snd_chase_v2,snd_chase_v3),5,false);
		audio_sound_gain(snd_chase_v1,1,room_speed * 4); audio_sound_gain(snd_chase_v2,1,room_speed * 4); audio_sound_gain(snd_chase_v3,1,room_speed * 4);
		catchphrase = false;
	}
	#endregion
	
	#region Setting Direction
	var side; if (image_xscale >= 1) { side = bbox_right+hsp; } else { side = -hsp-bbox_left; }
	if (onground) && (afraidofheights) && (scr_infloor(global.tilemap,side,bbox_bottom+1) < 0) { 
		dir = -dir; 
	} else {
		//Gets direction of the player. Subtracts the x position of the player by the x position of the enemy | -1 moving to the left and +1 moving to the right
		if (energy > 0) { dir = sign(player.x - x); } else { dir = -sign(player.x - x); }
		//Stops enemy close by to the player instead of exactly at the players x position
		if (abs(player.x - x) < attackrange) { hsp = 0; }
	}
	#endregion
	
	#region State Transitions
	if (distance_to_object(player) <= attackrange) && (jumpbuffer > 0) && (energy > 0) && (player.state != states.death) {
		energy -= attack_energy;
		state = a_states.attack;
	}
	if (distance_to_object(player) >= outofsight) || (player.state == states.death) { 
		audio_sound_gain(snd_chase_v1,0,room_speed * 4); audio_sound_gain(snd_chase_v2,0,room_speed * 4); audio_sound_gain(snd_chase_v3,0,room_speed * 4);
		returnable_to_x_anchor = 1; state = a_states.normal;  
	}
	#endregion
}
#endregion

scr_collision();