///@description Change Volume
///@arg Value
//Use audio_master_gain(argument0); if you wish to have a master of changing all audio group volumes.
var type = menu_option[page];

switch (type) {
	case 1: audio_group_set_gain(AG_VoiceOver, argument0, 0) variable_global_set(string("voice_over_vol"), argument0); break;
	case 2: audio_group_set_gain(AG_Sound_Effects, argument0, 0) variable_global_set(string("sound_effects_vol"), argument0); break;
	case 3: audio_group_set_gain(AG_BG_Music, argument0, 0) variable_global_set(string("background_music_vol"), argument0); break;
}