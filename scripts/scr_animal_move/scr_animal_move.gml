//General movement calculation script for animal objects.

#region General Animal Movement, Status Relative To Ground & Walls

	#region Checking If There Is A Dialogue Present, If So Animal Cannot Move
	if (instance_exists(obj_textbox)) { moveable = false } else { moveable = true; }
	#endregion
	
	#region Calculate Horizontal Movement
	if (moveable) { 
		if (state = a_states.chase) || (state = a_states.scared) || (state = a_states.alert)
		{
			hsp_msp = scr_approach(hsp_msp,dir * hsp_maxsp,hsp_acc);
			hsp = hsp_msp;
			if (hsp == 0)
			{
				var hsp_fric_final = hsp_fric_ground;
				if (!onground) hsp_fric_final = hsp_fric_air;
				hsp = scr_approach(hsp,0,hsp_fric_final);
			}
		hsp = clamp(hsp,-hsp_msp,hsp_msp);
		}
		else
		{ 
			if (state = a_states.normal)
			{
				hsp_wmsp = scr_approach(hsp_wmsp,dir * hsp_wmaxsp,hsp_acc);
				hsp = hsp_wmsp;
				if (hsp == 0)
				{
					var hsp_fric_final = hsp_fric_ground;
					if (!onground) hsp_fric_final = hsp_fric_air;
					hsp = scr_approach(hsp,0,hsp_fric_final);
				}
			hsp = clamp(hsp,-hsp_wmsp,hsp_wmsp);
			}
		}
	}
	#endregion

	#region Calculate Vertical Movement
	vsp += grv;
	vsp = clamp(vsp, -vsp_max, vsp_max);
	#endregion

	#region Calculate Current Status
	jumpbuffer = max(jumpbuffer-1,0);
	//Is my middle center touching the floor at the start of this frame?
	onground = (scr_infloor(global.tilemap,x,bbox_bottom+1) >= 0);
	onplatform = (place_meeting(x,y+1,par_oneway_platform));
	onobject = (place_meeting(x,y+1,par_objects));
	if (onground) || (onplatform) || (onobject) || (scr_infloor(global.tilemap,bbox_left,bbox_bottom+1) >= 0) || (scr_infloor(global.tilemap,bbox_right,bbox_bottom+1) >= 0)
	{
		jumpbuffer = jumpbuffer_max;
	}
	#endregion

	#region Storing Fractions For Non Integer Speeds
	scr_fractions();
	#endregion

#endregion