///@description Enemies Alert State

#region Calling Enemy Scripts
scr_enemy_move();					//Getting enemies calcuation movement script
scr_enemy_ani();					//Getting enemies animation, visual and sound effects script
#endregion

#region Random Movement & Catch Phrase Re-Intialized
if (catchphrase = false) { catchphrase = true; }
if (moveable)
{
	//Idle Timer
	moveidletimer += 1;
	
	//Alert Timer Decrease
	alerttimer -= 1;
	
	//Max timer amount for random choice between idle and random movement
	if (moveidletimer >= room_speed * 3) {
		//Chance to change to idle (0) or random wandering movement (1)
		var change; if (collided) { change = choose(1); } else { change = choose(0); }
		switch (change) {
			case 0:	dir = choose(-1,1); moveidletimer = irandom_range(0, room_speed * 1); break;
			case 1: dir = -dir; moveidletimer = irandom_range(0, room_speed * 1); break;
		}
	}
	
	//Setting direction to opposite direction if on the edge of the ground
	var side; if (image_xscale >= 1) { side = bbox_right+hsp; } else { side = -hsp-bbox_left; }
	if (onground) && (!collided) && (afraidofheights) && (scr_infloor(global.tilemap,side,bbox_bottom+1) < 0) { dir = -dir; }
}
#endregion

#region State Transitions
if (moveable) && (instance_exists(player))
{
	if (distance_to_object(player) <= attackrange) && (jumpbuffer > 0) && (energy > 0) && (player.state != states.death) {
		energy -= attack_energy;
		state = e_states.attack;
	}

	if (distance_to_object(player) <= aggrorange) && (energy > 0) && (player.state != states.death) {
		state = e_states.chase;
	}
	
	if (energy <= 0) { returnable_to_x_anchor = 1; state = e_states.normal; }	
	if (alerttimer <= 0) && (takingdamage == false) { alerttimer = max_alerttimer; returnable_to_x_anchor = 1; state = e_states.normal; }
}
#endregion

scr_collision();