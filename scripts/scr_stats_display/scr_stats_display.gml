/// @description Drawing Health, Energy & Breath Bars | scr_stats_display(hp_x, hp_y, energy_x, energy_y, breath_x, breath_y);
/// @param hp_x
/// @param hp_y
/// @param energy_x
/// @param energy_y
/// @param breath_x
/// @param breath_y

#region Getting Sprite Width, Stat Percentage, Timer Delay For Inner
var hp_sprite_width			= sprite_get_width(spr_healthbar);
var hp_sprite_height		= sprite_get_height(spr_healthbar);
var hp_percentage			= hp/ max_hp;
var oldhp_percentage		= old_hp/ max_hp;
if (old_hp != hp) { hp_bar_dec_delay --; }
if (hp_bar_dec_delay <= 0) { old_hp = scr_approach(old_hp,hp,0.5); 
	if (old_hp == hp) { hp_bar_dec_delay = max_hp_bar_dec_delay; }
}

var energy_sprite_width		= sprite_get_width(spr_energybar);
var energy_sprite_height	= sprite_get_height(spr_energybar);
var energy_percentage		= energy/ max_energy;
var oldenergy_percentage	= old_energy/ max_energy;
if (old_energy != energy) { energy_bar_dec_delay --; }
if (energy_bar_dec_delay <= 0) { old_energy = scr_approach(old_energy,energy,0.3); 
	if (old_energy == energy) { energy_bar_dec_delay = max_energy_bar_dec_delay; }
}

var breath_sprite_width		= sprite_get_width(spr_breathbar);
var breath_sprite_height	= sprite_get_height(spr_breathbar);
var breath_percentage		= breath/ max_breath;
var oldbreath_percentage	= old_breath/ max_breath;
if (old_breath != breath) { breath_bar_dec_delay --; }
if (breath_bar_dec_delay <= 0) { old_breath = scr_approach(old_breath,breath,0.3); 
	if (old_breath == breath) { breath_bar_dec_delay = max_breath_bar_dec_delay; }
}
#endregion

#region Drawing Health, Energy & Breath Bars
if (show_ui_hp) { 
	draw_sprite_ext(spr_healthbar, 0, argument0, argument1, 1,1,0,c_white,1); 
	draw_sprite_part(spr_healthbar, 1, 0, 0, hp_sprite_width*oldhp_percentage, hp_sprite_height, argument0, argument1);
	draw_sprite_part(spr_healthbar, 2, 0, 0, hp_sprite_width*hp_percentage, hp_sprite_height, argument0, argument1);
}

if (show_ui_energy) { 
	draw_sprite_ext(spr_energybar, 0, argument2, argument3,1,1,0,c_white,0); 
	//draw_sprite_part(spr_energybar, 1, 0, 0, energy_sprite_width*oldenergy_percentage, energy_sprite_height, argument2, argument3);
	//draw_sprite_part(spr_energybar, 2, 0, 0, energy_sprite_width*energy_percentage, energy_sprite_height, argument2, argument3);
}

if (show_ui_breath) { 
	draw_sprite_ext(spr_breathbar, 0, argument4, argument5,1,1,0,c_white,0); 
	//draw_sprite_part(spr_breathbar, 1, 0, 0, breath_sprite_width*oldbreath_percentage, breath_sprite_height, argument4, argument5);
	//draw_sprite_part(spr_breathbar, 2, 0, 0, breath_sprite_width*breath_percentage, breath_sprite_height, argument4, argument5);
}
#endregion