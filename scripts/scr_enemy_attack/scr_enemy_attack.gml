///@description Enemies Attack State

#region Setting Movement & Sounds
//Setting Movement
hsp = scr_approach(hsp,0,hsp_fric_ground);
vsp = scr_approach(vsp,0,hsp_fric_air);

if (image_index >= 1) && (image_index <= 2) {
//Setting Sounds
if (!audio_is_playing(sn_meleeswing_v1)) && (!audio_is_playing(sn_meleeswing_v2)) && (!audio_is_playing(sn_meleeswing_v3)) {
	audio_play_sound(choose(sn_meleeswing_v1,sn_meleeswing_v2,sn_meleeswing_v3),4,false);
}
if (scr_chance(0.1)) {
	if (!audio_is_playing(snd_attack_v1)) && (!audio_is_playing(snd_attack_v2)) && (!audio_is_playing(snd_attack_v3)) {
		audio_play_sound(choose(snd_attack_v1,snd_attack_v2,snd_attack_v3),5,false);
	}
}
}
#endregion

#region Attack
//Projectile Type Attack
if (projectile != -1) {
	if (has_boomerang) {
		scr_projectile_attack(ani_attack,attack_spd,4,5,0,player.x - x,projectile,proj_spd);
		has_boomerang = false;
	}
} else {
//Melee Type Attack
	scr_melee_attack(ani_attack,ani_idle,attack_spd,ani_attack_hb,
					true,false,true,true,
					scrshk_mag,scrshk_frm,ply_particles,0,ani_particles,env_particles,ply_dmg,0,ani_dmg,env_dmg
					);	
}
#endregion

#region End Attack Effects
if (scr_animation_end()) { image_speed = 1; state = e_states.chase; }
#endregion

scr_collision();