/// scr_dirtparticle(x, y, dirtpart.particle_type);
/// @description Creating dirt particle parameters
/// @param x
/// @param y
/// @param dirtpart.particle_type

#region Setting Dirt Particle Effect Types As Enums
enum dirtpart {
	jumping = 0,
	landing = 1,
	movement = 2,
	rock = 3,
	root = 4,
	height = 5
}
#endregion

#region Creation Of Dirt Particle Object & Its Parameters
var dirt_part = (instance_create_layer(argument0,argument1,"Effects",obj_dirt)); 

with (dirt_part)
{
	#region Jumping Particle Effect Parameters
	if (argument2 = dirtpart.jumping) {
	img_sp_1	= 0.5;
	img_sp_2	= 1;
	img_ind_1	= 0;
	img_ind_2	= 6;
	hsp_1		= -2;
	hsp_2		= 2;
	vsp_1		= -2;
	vsp_2		= -1;
	grv_1		= 0;
	grv_2		= 0;
	xscale_1	= -1;
	xscale_2	= 1;
	yscale_1	= -1;
	yscale_2	= 1;
	}
	#endregion
	
	#region Landing Particle Effect Parameters
	if (argument2 = dirtpart.landing) {
	img_sp_1	= 0.5;
	img_sp_2	= 1;
	img_ind_1	= 0;
	img_ind_2	= 6;
	hsp_1		= -2;
	hsp_2		= 2;
	vsp_1		= 0;
	vsp_2		= 0;
	grv_1		= 0;
	grv_2		= 0;
	xscale_1	= -1;
	xscale_2	= 1;
	yscale_1	= -1;
	yscale_2	= 1;
	}
	#endregion
	
	#region Movement Particle Effect Parameters
	if (argument2 = dirtpart.movement) {
	img_sp_1	= 0.4;
	img_sp_2	= 0.6;
	img_ind_1	= 0;
	img_ind_2	= 6;
	hsp_1		= -0.5;
	hsp_2		= 0.5;
	vsp_1		= -0.3;
	vsp_2		= -0.5;
	grv_1		= 0;
	grv_2		= 0;
	xscale_1	= -0.5;
	xscale_2	= 0.5;
	yscale_1	= -0.5;
	yscale_2	= 0.5;
	}
	#endregion
	
	#region Rock Explosion Particle Effect Parameters
	if (argument2 = dirtpart.rock) {
	img_sp_1	= 0.1;
	img_sp_2	= 0.3;
	img_ind_1	= 0;
	img_ind_2	= 6;
	hsp_1		= -10;
	hsp_2		= 10;
	vsp_1		= -10;
	vsp_2		= -30;
	grv_1		= 0.2;
	grv_2		= 0.3;
	xscale_1	= -1;
	xscale_2	= 1;
	yscale_1	= -1;
	yscale_2	= 1;
	}
	#endregion
	
	#region Root Breaking Particle Effect Parameters
	if (argument2 = dirtpart.root) {
	img_sp_1	= 0.2;
	img_sp_2	= 0.4;
	img_ind_1	= 3;
	img_ind_2	= 6;
	hsp_1		= -4;
	hsp_2		= 4;
	vsp_1		= 1;
	vsp_2		= -7;
	grv_1		= 0.1;
	grv_2		= 0.3;
	xscale_1	= -1;
	xscale_2	= 1;
	yscale_1	= -1;
	yscale_2	= 1;
	}
	#endregion
	
	image_speed = random_range(img_sp_1, img_sp_2);
	image_index = random_range(img_ind_1, img_ind_2);
	hsp = random_range(hsp_1, hsp_2);
	vsp = random_range(vsp_1, vsp_2);
	grv = random_range(grv_1, grv_2);
	image_xscale = choose(xscale_1, xscale_2);
	image_yscale = choose(yscale_1, yscale_2);
}

//Scripts can also return a value, so that they can be used in expressions. For this you would use the return statement:
//return <expression>
//It should be noted that the execution of the script ends at the return statement, meaning that any code which comes after the return has been called will not be run.
//Use return if you want to pass variables using a script
return (dirt_part);
#endregion