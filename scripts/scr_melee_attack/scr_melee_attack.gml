///@description scr_melee_attack | 16 arguments
/// @param animation			= 0;
/// @param idle_animation		= 1;
/// @param attack_spd			= 2;
/// @param hitbox				= 3;
/// @param atk_player?			= 4;
/// @param atk_enemy?			= 5;
/// @param atk_animal?			= 6;
/// @param atk_environment?		= 7;
/// @param scrshk_mag			= 8;
/// @param scrshk_frm			= 9;
/// @param ply_particles		= 10;
/// @param ene_particles		= 11;
/// @param ani_particles		= 12;
/// @param env_particles		= 13;
/// @param ply_damage			= 14;
/// @param ene_damage			= 15;
/// @param ani_damage			= 16;
/// @param env_damage			= 17;

#region Animation, Animation Speed
if (sprite_index != argument[0]) {
	sprite_index = argument[0];
	image_index = 0;
	image_speed = argument[2];
	ds_list_clear(hitbyattack);
}
#endregion

#region Hitbox & Setting Object Collision Checks
//Use Attack Hitbox & Check For Hits
mask_index = argument[3];
var hitbyattack_now = ds_list_create();												//Stores ID of every instance that we hit on this frame
	
//Returns the raw hits that are collided with
//If ordered is true it will return the ID's in the order of closest to furthest. Great if you want to get a bunch of collisions and have certain things happen based upon the order of distance.
if (argument[4] = true) { if (instance_exists(obj_boy)) { player = obj_boy; } if (instance_exists(obj_girl)) { player = obj_girl; } var player_hit = instance_place_list(x,y,player,hitbyattack_now,false); }	
if (argument[5] = true) { var enemy_hit = instance_place_list(x,y,par_enemy,hitbyattack_now,false); }	
if (argument[6] = true) { var animal_hit = instance_place_list(x,y,par_animal,hitbyattack_now,false); }	
if (argument[7] = true) { var environment_hit = instance_place_list(x,y,par_dmgobjs,hitbyattack_now,false); }	
#endregion

#region Attack Check Mechanic

	#region Attacking Player If Attackable Set To True
	if (argument[4] = true) {
		if (player_hit > 0) {
			//What this for loop does is that it checks the amount of hits and loops to reach the amount of hits than stops
			for (var i = 0; i < player_hit; i++) {
			//If this instance has not yet been hit by this attack. What the | does is that its a quick way to find a particular entry in a DS List
			var player_hitID = hitbyattack_now[| i];
			//Checks if a particular value actually exists in the DS List. Checks if something does not already exist in the list that we attacked so we don't hit the same instance more than once.
				if (ds_list_find_index(hitbyattack,player_hitID) == -1) {
					ds_list_add(hitbyattack,player_hitID);
					with (player_hitID) {
						if (state != states.death) && (state != states.dodge) { 
							takingdamage = true;
							if (!invincible) {
								audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
								scr_screenshake(argument[8],argument[9]);
								var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
								repeat(argument[10]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
								hp -= argument[14];
							} else {
								audio_play_sound(sn_invincibility,5,false);
							}
						}
					}
				}
			}	
		}
	}
	#endregion
	
	#region Attacking Enemy If Attackable Set To True
	if (argument[5] = true) {
		if (enemy_hit > 0) {
			//What this for loop does is that it checks the amount of hits and loops to reach the amount of hits than stops
			for (var i = 0; i < enemy_hit; i++) {
			//If this instance has not yet been hit by this attack. What the | does is that its a quick way to find a particular entry in a DS List
			var enemy_hitID = hitbyattack_now[| i];
			//Checks if a particular value actually exists in the DS List. Checks if something does not already exist in the list that we attacked so we don't hit the same instance more than once.
				if (ds_list_find_index(hitbyattack,enemy_hitID) == -1) {
					ds_list_add(hitbyattack,enemy_hitID);
					with (enemy_hitID) {
						if (state != e_states.death) { 
							takingdamage = true;
							if (!invincible) {
								audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
								scr_screenshake(argument[8],argument[9]);
								var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
								repeat(argument[11]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
								hp -= argument[15];
							} else {
								audio_play_sound(sn_invincibility,5,false);
							}
						}
					}
				}
			}	
		}
	}
	#endregion
	
	#region Attacking Animal If Attackable Set To True
	if (argument[6] = true) {
		if (animal_hit > 0) {
			//What this for loop does is that it checks the amount of hits and loops to reach the amount of hits than stops
			for (var i = 0; i < animal_hit; i++) {
			//If this instance has not yet been hit by this attack. What the | does is that its a quick way to find a particular entry in a DS List
			var animal_hitID = hitbyattack_now[| i];
			//Checks if a particular value actually exists in the DS List. Checks if something does not already exist in the list that we attacked so we don't hit the same instance more than once.
				if (ds_list_find_index(hitbyattack,animal_hitID) == -1) {
					ds_list_add(hitbyattack,animal_hitID);
					with (animal_hitID) {
						if (state != a_states.death) { 
							takingdamage = true;
							if (!invincible) {
								audio_play_sound(choose(sn_meleehitwd_ai_v1,sn_meleehitwd_ai_v2,sn_meleehitwd_ai_v3,sn_meleehitwd_ai_v4,sn_meleehitwd_ai_v5,sn_meleehitwd_ai_v6),5,false);
								scr_screenshake(argument[8],argument[9]);
								var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
								repeat(argument[12]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
								hp -= argument[16];
							} else {
								audio_play_sound(sn_invincibility,5,false);
							}
						}
					}
				}
			}	
		}
	}
	#endregion
	
	#region Attacking Environment If Attackable Set To True
	if (argument[7] = true) {
		if (environment_hit > 0) {
			//What this for loop does is that it checks the amount of hits and loops to reach the amount of hits than stops
			for (var i = 0; i < environment_hit; i++) {
			//If this instance has not yet been hit by this attack. What the | does is that its a quick way to find a particular entry in a DS List
			var environment_hitID = hitbyattack_now[| i];
			//Checks if a particular value actually exists in the DS List. Checks if something does not already exist in the list that we attacked so we don't hit the same instance more than once.
				if (ds_list_find_index(hitbyattack,environment_hitID) == -1) {
					ds_list_add(hitbyattack,environment_hitID);
					with (environment_hitID) {
						if (env_object = obj_dmgrock) { 
							audio_play_sound(choose(sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3),5,false);
							scr_screenshake(argument[8],argument[9]);
							var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
							repeat(argument[13]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
							repeat(argument[13]) { scr_dirtparticle(side,other.y,dirtpart.jumping); }
							hp -= argument[17];
						}
						if (env_object = obj_dmgroot) { 
							audio_play_sound(choose(sn_meleehitroot_v1,sn_meleehitroot_v2),5,false);
							scr_screenshake(argument[8],argument[9]);
							var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
							repeat(argument[13]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
							repeat(argument[13]) { scr_dirtparticle(side,other.y,dirtpart.jumping); }
							hp -= argument[17];
						}
						if (env_object = obj_dmgtree) { 
							audio_play_sound(choose(sn_meleehitrock_v1,sn_meleehitrock_v2,sn_meleehitrock_v3),5,false);
							scr_screenshake(argument[8],argument[9]);
							var side; if (other.image_xscale = 1) { side = bbox_left; } else { side = bbox_right; }
							repeat(argument[13]) { instance_create_layer(side,other.y,"Effects",obj_hitspark); }
							repeat(argument[13]) { scr_dirtparticle(side,other.y,dirtpart.jumping); }
							hp -= argument[17];
						}
					}
				}
			}
		}	
	}
	#endregion
	
#endregion

#region Destroying List, & Resetting To Default Idle Hit Mask Sprite
ds_list_destroy(hitbyattack_now);			//Destroy list that we have at this step
mask_index = argument[1];					//Returns To Original Hit Mask
#endregion