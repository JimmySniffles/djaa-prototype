/// @description scr_transition(type, mode, targetroom)
/// @arg Type sets which type of transition.
/// @arg Mode sets transition mode between next, restart and goto. Leave as 0 if the transition type is side.
/// @arg Target sets target room when using the goto mode.

with (obj_transition) {
	type = argument[0];
	mode = argument[1];
	//argument_count. This read-only variable returns the number of "arguments" that are passed through to a script.
	if (argument_count > 2) { target = argument[2]; }
}

// Useful code if you want a object to go to the current room
// scr_transition(TRANS_TYPE.CENTRE,TRANS_MODE.GOTO,room);