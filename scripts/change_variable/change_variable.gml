///@description change_variable
///@arg object
///@arg variable_name_as_string
///@arg new_value

with (argument0) { var object_id = id; }
variable_instance_set(object_id, argument1, argument2);