{
    "id": "5236fd14-2be1-4d65-a056-aad6c507839b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_sa_a_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e7f8ca7-cd98-4bf4-ab92-e18082544d04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "bb87874e-6c12-4432-952b-e60a7ecf21ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e7f8ca7-cd98-4bf4-ab92-e18082544d04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c0c7e1-ab45-4430-956d-a18ef3051d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7f8ca7-cd98-4bf4-ab92-e18082544d04",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "57b8ac00-3bb0-4a8d-b117-aa4620c21b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7f8ca7-cd98-4bf4-ab92-e18082544d04",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        },
        {
            "id": "41161cef-9745-4eb7-a2fc-506a632b97b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "1b5cad9f-bc1e-4c57-ab26-94489112fe9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41161cef-9745-4eb7-a2fc-506a632b97b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1f5d4a-0178-4029-94e9-e3c7b2db3aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41161cef-9745-4eb7-a2fc-506a632b97b8",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "3d6056a6-e081-4ea2-8a33-5feb8020a8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41161cef-9745-4eb7-a2fc-506a632b97b8",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        },
        {
            "id": "f528c922-25ba-4808-938d-c60f7c5f060b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "f8cc6a32-21ee-4f8e-8b40-116f6e274067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f528c922-25ba-4808-938d-c60f7c5f060b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c997db5-a9d5-41b6-bd1e-43db8c20309a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f528c922-25ba-4808-938d-c60f7c5f060b",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "07937faf-814f-4c47-8167-92961301bda1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f528c922-25ba-4808-938d-c60f7c5f060b",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        },
        {
            "id": "54e995dc-505e-48ba-8b82-131971be6d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "1f5771ff-1c06-4254-9e06-0729fe4f5944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e995dc-505e-48ba-8b82-131971be6d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a41ab7-0a5e-46a7-96b3-53461abb62a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e995dc-505e-48ba-8b82-131971be6d8b",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "df936a77-b8c2-4fe1-a3b6-5d0810dad0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e995dc-505e-48ba-8b82-131971be6d8b",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        },
        {
            "id": "3c244627-423a-4fba-9cf0-39156d7a8745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "7543c5e3-00c9-4e6f-a7af-c00bf64f9e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c244627-423a-4fba-9cf0-39156d7a8745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2a413a-2e39-4ef2-9c12-7af531a134cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c244627-423a-4fba-9cf0-39156d7a8745",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "6854de97-39ad-42f4-a2ca-47ade9fbc357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c244627-423a-4fba-9cf0-39156d7a8745",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        },
        {
            "id": "b0224b99-8299-46b2-9130-fcd9fff5d0d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "compositeImage": {
                "id": "668253d7-ca96-4e37-9ee5-6c090a780422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0224b99-8299-46b2-9130-fcd9fff5d0d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd30c61-1155-46ff-800b-c36b5cb97410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0224b99-8299-46b2-9130-fcd9fff5d0d0",
                    "LayerId": "d8507197-9386-4cfd-8b89-ce4162c9f193"
                },
                {
                    "id": "6dabc15b-fb18-4b5a-b306-8638ee24313b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0224b99-8299-46b2-9130-fcd9fff5d0d0",
                    "LayerId": "a85fc850-3353-449c-a347-60a6d48592dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d8507197-9386-4cfd-8b89-ce4162c9f193",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a85fc850-3353-449c-a347-60a6d48592dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5236fd14-2be1-4d65-a056-aad6c507839b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}