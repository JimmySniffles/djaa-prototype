{
    "id": "b7ab49b3-29f3-4f3d-9b66-41d59e0e06b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sr_longgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 1022,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e839dab-a130-4715-94a6-f7fb496a6806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7ab49b3-29f3-4f3d-9b66-41d59e0e06b6",
            "compositeImage": {
                "id": "5a5f0413-ff1c-4e9a-a415-513dd286bcd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e839dab-a130-4715-94a6-f7fb496a6806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e403bb-5ab1-47e1-8ea0-e7f726539e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e839dab-a130-4715-94a6-f7fb496a6806",
                    "LayerId": "b0bd1fe8-55ff-44af-b55b-8875970dbd06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b0bd1fe8-55ff-44af-b55b-8875970dbd06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7ab49b3-29f3-4f3d-9b66-41d59e0e06b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}