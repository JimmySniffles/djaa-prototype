{
    "id": "c51cb027-78ce-4f2d-b72a-98477f7938ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_babun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 3,
    "bbox_right": 113,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80938c72-7e46-42fc-9c04-76771a21cd0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c51cb027-78ce-4f2d-b72a-98477f7938ea",
            "compositeImage": {
                "id": "6762ec55-e3e9-4988-8365-c0e6f66f2421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80938c72-7e46-42fc-9c04-76771a21cd0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b532407-39ab-4890-b3c1-5ca9c53f8186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80938c72-7e46-42fc-9c04-76771a21cd0b",
                    "LayerId": "bb6ce2b9-8b33-4dfa-982b-79a78b635250"
                }
            ]
        },
        {
            "id": "438d94cd-f748-4806-9191-ff44b59dd910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c51cb027-78ce-4f2d-b72a-98477f7938ea",
            "compositeImage": {
                "id": "9b0f1c6d-4c1e-4592-934d-78f5f8cef4a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "438d94cd-f748-4806-9191-ff44b59dd910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54461336-f379-4e49-b1d7-fcec7dece242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "438d94cd-f748-4806-9191-ff44b59dd910",
                    "LayerId": "bb6ce2b9-8b33-4dfa-982b-79a78b635250"
                }
            ]
        },
        {
            "id": "8843ec36-ec8b-4244-a2a2-fbf73e6606fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c51cb027-78ce-4f2d-b72a-98477f7938ea",
            "compositeImage": {
                "id": "2a0359e9-14cb-4ad7-82f1-55337dabe3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8843ec36-ec8b-4244-a2a2-fbf73e6606fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d497faf-1f43-490c-91b3-514b8c6b649a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8843ec36-ec8b-4244-a2a2-fbf73e6606fd",
                    "LayerId": "bb6ce2b9-8b33-4dfa-982b-79a78b635250"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "bb6ce2b9-8b33-4dfa-982b-79a78b635250",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c51cb027-78ce-4f2d-b72a-98477f7938ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}