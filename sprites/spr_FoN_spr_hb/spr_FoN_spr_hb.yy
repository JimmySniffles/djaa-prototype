{
    "id": "6c106a9c-4922-405f-ad45-538578dae12d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_spr_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7fb22fc-b1e3-48df-8279-b95e891197af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "167a3021-ea7c-4ddd-97fa-a2b70b5b988d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fb22fc-b1e3-48df-8279-b95e891197af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b73874-706d-4a03-88f7-0c6a5988aad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fb22fc-b1e3-48df-8279-b95e891197af",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "68793354-399e-4e51-ab48-99be61a3cb14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fb22fc-b1e3-48df-8279-b95e891197af",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "9e9faef0-cc39-452e-b7ae-37a56707d430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "864d79de-dc9e-48d8-ac0a-2d2e7779f99b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e9faef0-cc39-452e-b7ae-37a56707d430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5c7da3-960e-43dd-8d89-b344ba491975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9faef0-cc39-452e-b7ae-37a56707d430",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "ab5d8ca9-a397-4220-b1b8-cf50ea7b957a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9faef0-cc39-452e-b7ae-37a56707d430",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "a99f128d-84a2-4f0f-a973-68a4cc503ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "4e20371c-62a0-40fb-b222-cdef24fb67f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99f128d-84a2-4f0f-a973-68a4cc503ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98acc14b-a2e8-4acb-bdd4-a8ee23abefe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99f128d-84a2-4f0f-a973-68a4cc503ad0",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "95fde358-4943-4b1a-b35a-b4e231dc2577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99f128d-84a2-4f0f-a973-68a4cc503ad0",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "93e0c88d-3210-436f-b6bb-0bd45cdb1949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "221dc217-8cab-4dc7-a437-dbe39e41ea77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e0c88d-3210-436f-b6bb-0bd45cdb1949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2197ffc5-f9d6-43ae-a781-7fc323d0ca28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e0c88d-3210-436f-b6bb-0bd45cdb1949",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "9277aaff-c097-4e4d-9a47-9b8001a21af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e0c88d-3210-436f-b6bb-0bd45cdb1949",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "54e75733-b90b-4ed9-aa18-0f9246323389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "230db447-5b23-46d5-9a49-f6094073a439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e75733-b90b-4ed9-aa18-0f9246323389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf50564b-5f5c-4615-bd8f-13da3c828d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e75733-b90b-4ed9-aa18-0f9246323389",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "d7965a86-2601-4292-8a7e-4a0cdec85ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e75733-b90b-4ed9-aa18-0f9246323389",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "b59f3907-46c9-4696-a44a-1f553106c2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "17e62147-b7f5-41cc-868d-d510e0e82176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b59f3907-46c9-4696-a44a-1f553106c2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024746fb-29b2-44eb-8e4b-6e962ab4c842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59f3907-46c9-4696-a44a-1f553106c2e4",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "01ce86cb-e7b6-449c-ae23-9167e7d23369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59f3907-46c9-4696-a44a-1f553106c2e4",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "2302c462-d088-44e3-91af-51c73dbe88ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "ce3af31b-753d-4710-aff1-4084e17874c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2302c462-d088-44e3-91af-51c73dbe88ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c457e82-e312-4814-97bf-189e3f06bbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2302c462-d088-44e3-91af-51c73dbe88ce",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "73f93399-1be1-42c0-800d-d22eb0d517b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2302c462-d088-44e3-91af-51c73dbe88ce",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        },
        {
            "id": "e75af812-90dd-4007-b3bc-81071cd00e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "compositeImage": {
                "id": "346a127c-5d4b-465a-8869-251a443d716f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e75af812-90dd-4007-b3bc-81071cd00e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425e88b7-f850-47d8-a5e8-7aa2b2fb42b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75af812-90dd-4007-b3bc-81071cd00e04",
                    "LayerId": "b5f0a773-3254-4188-8a86-3d29adac9a4d"
                },
                {
                    "id": "0bb8c06e-c476-4b68-91b6-2774600caba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e75af812-90dd-4007-b3bc-81071cd00e04",
                    "LayerId": "cb617db6-66a1-458e-80fc-7a076242248e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cb617db6-66a1-458e-80fc-7a076242248e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b5f0a773-3254-4188-8a86-3d29adac9a4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c106a9c-4922-405f-ad45-538578dae12d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}