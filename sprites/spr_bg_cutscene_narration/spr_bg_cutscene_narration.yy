{
    "id": "a21f515e-9863-477c-b895-3678106980e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_cutscene_narration",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 3999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b86517f9-0415-4ac8-ae69-007586e713bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21f515e-9863-477c-b895-3678106980e6",
            "compositeImage": {
                "id": "ab1b23ab-e4d1-464a-a1ff-fd703d4727f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86517f9-0415-4ac8-ae69-007586e713bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7823c95-50d8-400c-acf5-d1b06388585c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86517f9-0415-4ac8-ae69-007586e713bb",
                    "LayerId": "38348738-7a14-4e8d-bacf-bb13fd959c95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "38348738-7a14-4e8d-bacf-bb13fd959c95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a21f515e-9863-477c-b895-3678106980e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4000,
    "xorig": 0,
    "yorig": 0
}