{
    "id": "44300b5e-6877-49e4-8437-0971d5f32061",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_berrybushes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 27,
    "bbox_right": 502,
    "bbox_top": 94,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1555e040-2196-4086-b1b5-3f9eb559d624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44300b5e-6877-49e4-8437-0971d5f32061",
            "compositeImage": {
                "id": "50c9a46d-4780-49b2-b9e6-67d186110c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1555e040-2196-4086-b1b5-3f9eb559d624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6ee96f-61d5-44a4-a623-a155edef734d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1555e040-2196-4086-b1b5-3f9eb559d624",
                    "LayerId": "d26aa857-3c1f-420f-a2ad-f1bd5d40be5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "d26aa857-3c1f-420f-a2ad-f1bd5d40be5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44300b5e-6877-49e4-8437-0971d5f32061",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}