{
    "id": "07a1acfd-ac0b-42c0-be6c-ea006301a3e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 7,
    "bbox_right": 1848,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53ee9e55-1609-4d1f-88f9-7332ddc97a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a1acfd-ac0b-42c0-be6c-ea006301a3e6",
            "compositeImage": {
                "id": "f15f2cb4-1041-444a-a573-1b9b451309e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ee9e55-1609-4d1f-88f9-7332ddc97a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55fa5263-cae1-424c-8bfd-b7ecf24a6b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ee9e55-1609-4d1f-88f9-7332ddc97a82",
                    "LayerId": "a2ac77ce-860f-48a2-a202-1ffc7ba27ce2"
                }
            ]
        }
    ],
    "gridX": 116,
    "gridY": 116,
    "height": 348,
    "layers": [
        {
            "id": "a2ac77ce-860f-48a2-a202-1ffc7ba27ce2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a1acfd-ac0b-42c0-be6c-ea006301a3e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1856,
    "xorig": 0,
    "yorig": 0
}