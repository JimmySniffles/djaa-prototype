{
    "id": "cb49855c-3406-460f-8522-7f365b424945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wakan_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 37,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bef6251-f8fb-4659-8dee-d2998c5bbd50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb49855c-3406-460f-8522-7f365b424945",
            "compositeImage": {
                "id": "e6d024a3-30a1-4a8b-833e-38dbdddb6088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bef6251-f8fb-4659-8dee-d2998c5bbd50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed599d3-ffbe-45fc-9317-9215da546704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bef6251-f8fb-4659-8dee-d2998c5bbd50",
                    "LayerId": "99defe55-3382-4af3-a392-3131d8522a40"
                }
            ]
        },
        {
            "id": "f456fe7d-140a-4349-8142-9716c77b7349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb49855c-3406-460f-8522-7f365b424945",
            "compositeImage": {
                "id": "2b8821e3-54f4-4500-927e-9f10731ec3c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f456fe7d-140a-4349-8142-9716c77b7349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2f8588-08eb-48fc-893e-ac5d8f103474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f456fe7d-140a-4349-8142-9716c77b7349",
                    "LayerId": "99defe55-3382-4af3-a392-3131d8522a40"
                }
            ]
        },
        {
            "id": "b734d31b-ae4e-45d5-9607-3915ffe87bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb49855c-3406-460f-8522-7f365b424945",
            "compositeImage": {
                "id": "63e8e2a7-f90f-4fe6-9d36-76dbc70aff11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b734d31b-ae4e-45d5-9607-3915ffe87bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0c630a7-db8a-4e29-b986-ce10e89d95de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b734d31b-ae4e-45d5-9607-3915ffe87bf2",
                    "LayerId": "99defe55-3382-4af3-a392-3131d8522a40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99defe55-3382-4af3-a392-3131d8522a40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb49855c-3406-460f-8522-7f365b424945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}