{
    "id": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_rbsnake_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a44bdc11-2f01-4bf4-a82f-6b6216584e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "49393226-feee-4db4-8330-a5c32515e1ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44bdc11-2f01-4bf4-a82f-6b6216584e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51f67d7-1853-42da-a95e-f3d87dbdb3e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44bdc11-2f01-4bf4-a82f-6b6216584e78",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "e7c0b999-87fe-48ef-bef6-4c26d9bb4a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "5e7d01d0-5472-44cd-9e22-d38e13040023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c0b999-87fe-48ef-bef6-4c26d9bb4a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90cde07b-dbb2-4761-b049-76425fd6f8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c0b999-87fe-48ef-bef6-4c26d9bb4a5c",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "4d7771c6-9fe2-4f1d-a229-517c007adfa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "c835623c-1a9c-4279-ba01-e032f3709519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7771c6-9fe2-4f1d-a229-517c007adfa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "206a6c35-d572-4f1c-b033-a8c4be0b4d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7771c6-9fe2-4f1d-a229-517c007adfa8",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "c10ab517-6892-40e1-a775-ce0a3d563f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "e5e3b8a6-f55a-4360-9a5a-eb9a9287e450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10ab517-6892-40e1-a775-ce0a3d563f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb841041-2d19-40f0-8baa-7c56eb86cd41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10ab517-6892-40e1-a775-ce0a3d563f02",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "ced41e34-e920-4b12-b55c-dba2dd66ce6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "9184b567-b149-4a89-b119-fc2c1eef567a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced41e34-e920-4b12-b55c-dba2dd66ce6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4399072a-d654-43af-914d-a4d6a91bfdfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced41e34-e920-4b12-b55c-dba2dd66ce6b",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "d0ff2fe0-dfd7-48fb-b926-534fa7b1a26b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "ccabe7c7-5e54-4afb-8947-2318fd2263fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ff2fe0-dfd7-48fb-b926-534fa7b1a26b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7be960-041c-48dc-90b8-01137c79442b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ff2fe0-dfd7-48fb-b926-534fa7b1a26b",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "50db8335-5c34-49c2-8771-451646ccb408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "1cad90fe-76d1-404c-8fac-a097a2c0b57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50db8335-5c34-49c2-8771-451646ccb408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d934e521-cbce-4490-b61b-571052d5404f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50db8335-5c34-49c2-8771-451646ccb408",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "7f8385b2-c164-4912-92d5-058b9e100f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "bb9666b5-8236-4bdb-8cc4-e1cbacf37fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8385b2-c164-4912-92d5-058b9e100f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1253e683-a2ad-4f88-9ed5-e264af994b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8385b2-c164-4912-92d5-058b9e100f8b",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "51af69a8-46ee-4a84-b45a-911c76bb6439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "a27c9a50-d5b5-4640-9a94-7825747979ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51af69a8-46ee-4a84-b45a-911c76bb6439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa40c9a-87e5-4e56-98cf-647f8e2a9513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51af69a8-46ee-4a84-b45a-911c76bb6439",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "2fad925a-ab41-464d-9402-47170648a3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "4974ca5a-5749-48dc-8a5a-fd0cb8dcc865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fad925a-ab41-464d-9402-47170648a3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea61c49-5763-4da7-9df3-6a9634b01ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fad925a-ab41-464d-9402-47170648a3dd",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "318a8ea0-6925-4b55-8f67-ac631dfb3882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "b8a80297-7fea-4103-800e-40e92e9f6114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "318a8ea0-6925-4b55-8f67-ac631dfb3882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a51d3c-6bb5-4453-a269-6fb71f2d6832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "318a8ea0-6925-4b55-8f67-ac631dfb3882",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "e48df715-5ffd-410d-91c4-5817b30eaa7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "993de7a5-5199-472a-805c-460723caa8d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48df715-5ffd-410d-91c4-5817b30eaa7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "717f6ab0-4b49-4e63-a1cc-261bc4c3132c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48df715-5ffd-410d-91c4-5817b30eaa7b",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        },
        {
            "id": "297a0dfd-6944-4474-bf45-f1d2f180c6f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "compositeImage": {
                "id": "b9b9a6e7-105a-4da3-ae16-602d518c6d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "297a0dfd-6944-4474-bf45-f1d2f180c6f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9c80d5-c7af-4d06-9df8-3eed137217a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "297a0dfd-6944-4474-bf45-f1d2f180c6f9",
                    "LayerId": "275d77e3-b4dc-4b82-a596-8eeec8d2980c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "275d77e3-b4dc-4b82-a596-8eeec8d2980c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad78ffb1-d914-4ada-bed6-697862f61ba3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}