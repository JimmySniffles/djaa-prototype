{
    "id": "e525fdf5-15ef-40a3-977b-56cb60dc2727",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stickstack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11e48130-5c20-4223-8f53-5b1ccbd5bd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e525fdf5-15ef-40a3-977b-56cb60dc2727",
            "compositeImage": {
                "id": "94a4a5f8-5cb8-45d9-8875-63d2b98e65e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e48130-5c20-4223-8f53-5b1ccbd5bd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b630100-6573-4040-a07a-96a824f61cc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e48130-5c20-4223-8f53-5b1ccbd5bd61",
                    "LayerId": "b929f037-aaf9-43da-b6cd-0ae20638fb10"
                }
            ]
        },
        {
            "id": "99927e77-4217-4831-a256-0e1b0d50affb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e525fdf5-15ef-40a3-977b-56cb60dc2727",
            "compositeImage": {
                "id": "377ee5d1-20b0-4a9f-8b23-95248425b6e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99927e77-4217-4831-a256-0e1b0d50affb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db676cf8-7bd9-490e-a439-5709169477a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99927e77-4217-4831-a256-0e1b0d50affb",
                    "LayerId": "b929f037-aaf9-43da-b6cd-0ae20638fb10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b929f037-aaf9-43da-b6cd-0ae20638fb10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e525fdf5-15ef-40a3-977b-56cb60dc2727",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}