{
    "id": "39df7097-9027-4917-8d5d-e8c697c55069",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lowanna_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 40,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0df8293c-56bf-4a9c-9084-cabf4f2dc262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df7097-9027-4917-8d5d-e8c697c55069",
            "compositeImage": {
                "id": "3fc13c3f-62fb-41df-9965-baec25320136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0df8293c-56bf-4a9c-9084-cabf4f2dc262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1115a283-fa87-48d3-855a-a7cb08d1e268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0df8293c-56bf-4a9c-9084-cabf4f2dc262",
                    "LayerId": "b9d7b608-b2af-4fa9-9671-16bda159768c"
                }
            ]
        },
        {
            "id": "66ac34eb-24bd-45c5-a790-134f66d90fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df7097-9027-4917-8d5d-e8c697c55069",
            "compositeImage": {
                "id": "3ed6e242-24c7-4a7a-b212-34cfa18c8ca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ac34eb-24bd-45c5-a790-134f66d90fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "234e10ad-5c1c-4461-9dc3-881c5da1c4f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ac34eb-24bd-45c5-a790-134f66d90fca",
                    "LayerId": "b9d7b608-b2af-4fa9-9671-16bda159768c"
                }
            ]
        },
        {
            "id": "97e977f9-6cc5-48f9-873d-f4ad4aa9b5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39df7097-9027-4917-8d5d-e8c697c55069",
            "compositeImage": {
                "id": "06ce1363-a1cc-456e-ac33-416ecf6e2c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e977f9-6cc5-48f9-873d-f4ad4aa9b5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e129f2-41e8-4515-b2d3-2e47deee53e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e977f9-6cc5-48f9-873d-f4ad4aa9b5c6",
                    "LayerId": "b9d7b608-b2af-4fa9-9671-16bda159768c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b9d7b608-b2af-4fa9-9671-16bda159768c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39df7097-9027-4917-8d5d-e8c697c55069",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}