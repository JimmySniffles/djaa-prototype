{
    "id": "98648d8d-e54a-485c-89d4-4e4975a2f8d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkang_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72c4cf11-daad-49cb-a785-61cfd2ee273f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98648d8d-e54a-485c-89d4-4e4975a2f8d0",
            "compositeImage": {
                "id": "ba4edcf2-a45f-4509-9fe5-a803ac0e0cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c4cf11-daad-49cb-a785-61cfd2ee273f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74273f99-1a28-453e-8334-21761d910865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c4cf11-daad-49cb-a785-61cfd2ee273f",
                    "LayerId": "216faa59-bcbf-4060-90a4-ee5cfed34514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "216faa59-bcbf-4060-90a4-ee5cfed34514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98648d8d-e54a-485c-89d4-4e4975a2f8d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}