{
    "id": "b9cf725d-78fb-4335-94b9-707ab843e22a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_gslide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87258e29-bc89-4de7-82bd-d0b40fb1da3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "3e45ac78-8893-4d5a-ba78-229857b40eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87258e29-bc89-4de7-82bd-d0b40fb1da3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "079276ad-eadd-493b-9463-cc95ca01da30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87258e29-bc89-4de7-82bd-d0b40fb1da3c",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "67747853-a249-4689-9aaf-5791241ce856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "44499243-f107-4a8f-8b7f-4465a8a162bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67747853-a249-4689-9aaf-5791241ce856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb6e13b-a18a-4710-a599-1a2e7290677f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67747853-a249-4689-9aaf-5791241ce856",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "2330ceb3-6e24-46a9-bc38-75bb7f73475f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "21521578-9b96-4e99-ba5d-7c2111701d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2330ceb3-6e24-46a9-bc38-75bb7f73475f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "509d9700-6449-45e6-9b61-26baaabd2568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2330ceb3-6e24-46a9-bc38-75bb7f73475f",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "14b6408c-bfd3-4e0c-839c-333e4d749ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "53c63ecf-8847-491e-8528-df888a119a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b6408c-bfd3-4e0c-839c-333e4d749ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0e929b-7085-40eb-af9c-64759d97fa6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b6408c-bfd3-4e0c-839c-333e4d749ac8",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "a8f2006d-e0de-42a2-8ff6-9f8b64b599e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "8540b376-fa5b-4a4f-ab1b-bd0105eab49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f2006d-e0de-42a2-8ff6-9f8b64b599e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138ae9ff-17c4-4c89-8b59-09eed5884bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f2006d-e0de-42a2-8ff6-9f8b64b599e9",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "18324fb0-ecd3-4275-bd6b-60f31c8f4fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "87ce9e58-88e7-43cb-844b-e851cf30253f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18324fb0-ecd3-4275-bd6b-60f31c8f4fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ae1f70-0006-4aba-b47b-dbb481de5f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18324fb0-ecd3-4275-bd6b-60f31c8f4fc6",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "79493f8f-819d-430c-8cba-1a311e6ade77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "8e57e1d8-8af7-410c-b399-3fb4b2d95508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79493f8f-819d-430c-8cba-1a311e6ade77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa02caae-ce69-4cca-a162-a1724f99b522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79493f8f-819d-430c-8cba-1a311e6ade77",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "c3a6f549-e574-4888-8f66-6cc27730f8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "79b648ce-d877-4d7a-99a7-56db6e588b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a6f549-e574-4888-8f66-6cc27730f8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e186f1-b856-4ab0-b5ae-11975d573710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a6f549-e574-4888-8f66-6cc27730f8eb",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "ab85913d-73e4-40a0-a163-68843dd9ed02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "d6d576e7-9afe-459b-898c-fff43aa42f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab85913d-73e4-40a0-a163-68843dd9ed02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be1107fe-7d51-46f6-8227-c0f921f47fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab85913d-73e4-40a0-a163-68843dd9ed02",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "bfed7813-a008-4377-8c27-d3d1790c3b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "9f153192-6098-43bc-99f4-48a11b5bd8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfed7813-a008-4377-8c27-d3d1790c3b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4266fb74-5dcb-464a-a913-57ed63c97ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfed7813-a008-4377-8c27-d3d1790c3b50",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "c3748785-86e1-42e1-9a6e-070dccd4a156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "c15a1f81-4a37-4163-a3d7-e3c1898e37c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3748785-86e1-42e1-9a6e-070dccd4a156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2668d8b2-393f-4c4c-8ad4-f81ed8d6925f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3748785-86e1-42e1-9a6e-070dccd4a156",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "61a55910-e6ed-4885-93ab-8694bb0d4e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "7646890a-4d2f-4776-ac52-87471e5f34a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a55910-e6ed-4885-93ab-8694bb0d4e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c4b185-a2c6-45e0-8c1e-06b9d346f5da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a55910-e6ed-4885-93ab-8694bb0d4e7d",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "e006fb2a-f75e-4530-a883-5fa3594bbe19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "9d4a937a-9e7e-4451-bbbb-2329ddfe0b9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e006fb2a-f75e-4530-a883-5fa3594bbe19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b88a48-a790-4a13-80cd-fc1a043c163b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e006fb2a-f75e-4530-a883-5fa3594bbe19",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "784dfe68-be7b-4cc3-80d2-af3062f414dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "7073d39f-b9ce-47b9-b462-a4989fe150a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784dfe68-be7b-4cc3-80d2-af3062f414dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "122a39aa-baf6-4f42-9fe2-cec33e13a570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784dfe68-be7b-4cc3-80d2-af3062f414dd",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "5552496d-9b6a-4220-a59a-e9f874b7d2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "fe317904-cb93-4561-af92-9b0a6bc05f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5552496d-9b6a-4220-a59a-e9f874b7d2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f25db26-a05c-466f-a1cc-645715afca2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5552496d-9b6a-4220-a59a-e9f874b7d2ac",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "741db28b-1b68-4794-bb2f-d6fa420d22e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "b3d4a505-36ba-4a71-8e4c-04b0677beb68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "741db28b-1b68-4794-bb2f-d6fa420d22e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed473f7a-902e-4f30-8b2c-8b0d8e433eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "741db28b-1b68-4794-bb2f-d6fa420d22e3",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "be126b3c-1307-4b59-bd26-5be0bf5873cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "9b56679c-ac15-404a-9842-a63db9f00baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be126b3c-1307-4b59-bd26-5be0bf5873cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea1380c-b58f-4f0f-8764-881933a0dd2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be126b3c-1307-4b59-bd26-5be0bf5873cf",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "d14f3e6b-559b-45f3-a849-6ab3e6cecb48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "5f4c2a7d-caad-4e05-8022-0ecf825b61a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14f3e6b-559b-45f3-a849-6ab3e6cecb48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1164caf4-d56f-468d-8ece-64ab7b6af438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14f3e6b-559b-45f3-a849-6ab3e6cecb48",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "d74dc333-486f-4c3a-b18e-c7d00d6d1753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "75e2570c-5bb2-4772-a0fd-4ae64ac08f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74dc333-486f-4c3a-b18e-c7d00d6d1753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b564f1-bdf3-4e24-94fa-01de3f5e01c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74dc333-486f-4c3a-b18e-c7d00d6d1753",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        },
        {
            "id": "bcff9be5-3e7f-45d6-9518-18dd48f0a2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "compositeImage": {
                "id": "47d07bad-a7d4-4588-a57e-63ca603278d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcff9be5-3e7f-45d6-9518-18dd48f0a2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb148b3-2bb5-47be-9794-8202b8878d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcff9be5-3e7f-45d6-9518-18dd48f0a2cf",
                    "LayerId": "31c7106a-8be7-4de1-a4fb-b009c83bced0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "31c7106a-8be7-4de1-a4fb-b009c83bced0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9cf725d-78fb-4335-94b9-707ab843e22a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}