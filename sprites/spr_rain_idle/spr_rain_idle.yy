{
    "id": "9811d33a-5932-4f9b-bff6-088f66ee837d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rain_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 40,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e5ff24e-0a64-40c3-bc58-5b12a1e96f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9811d33a-5932-4f9b-bff6-088f66ee837d",
            "compositeImage": {
                "id": "5ff80afc-f42d-41ed-9a92-1a0a605ec28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5ff24e-0a64-40c3-bc58-5b12a1e96f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ddb092-9f95-4137-9a4f-89ba191ce9b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5ff24e-0a64-40c3-bc58-5b12a1e96f53",
                    "LayerId": "142a1fa3-6645-4ebf-9d69-4c062a4412a3"
                }
            ]
        },
        {
            "id": "e4098773-904e-42bf-a63d-2c6f902ca461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9811d33a-5932-4f9b-bff6-088f66ee837d",
            "compositeImage": {
                "id": "32f0504d-5de7-4bde-bfc4-5098073064a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4098773-904e-42bf-a63d-2c6f902ca461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e58addd-8382-49fe-bbf2-3e54c2c0f33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4098773-904e-42bf-a63d-2c6f902ca461",
                    "LayerId": "142a1fa3-6645-4ebf-9d69-4c062a4412a3"
                }
            ]
        },
        {
            "id": "c0aeffa5-44a9-44db-bf3c-c7a9dca3ea48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9811d33a-5932-4f9b-bff6-088f66ee837d",
            "compositeImage": {
                "id": "6718147a-5bd6-4f8d-993f-ed0a8f83e2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0aeffa5-44a9-44db-bf3c-c7a9dca3ea48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefd3ff5-c7ba-47ff-9f38-64b6b72e9537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0aeffa5-44a9-44db-bf3c-c7a9dca3ea48",
                    "LayerId": "142a1fa3-6645-4ebf-9d69-4c062a4412a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "142a1fa3-6645-4ebf-9d69-4c062a4412a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9811d33a-5932-4f9b-bff6-088f66ee837d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}