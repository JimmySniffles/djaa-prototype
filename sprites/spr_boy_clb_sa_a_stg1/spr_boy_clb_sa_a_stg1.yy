{
    "id": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_sa_a_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ba2dae5-43ad-49a6-8f03-7d467311c051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "f14a136b-a0d4-4750-a5ee-22e837e024f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba2dae5-43ad-49a6-8f03-7d467311c051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c816ae1-8221-424a-ac52-adcf18e6fd69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba2dae5-43ad-49a6-8f03-7d467311c051",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "12952e3c-b40a-498d-88a2-372d619285de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba2dae5-43ad-49a6-8f03-7d467311c051",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        },
        {
            "id": "30bea415-8433-4cdf-8dd1-2586e7e75c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "355a2e00-067a-4827-8541-c0caa4ef73bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30bea415-8433-4cdf-8dd1-2586e7e75c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7294f9d-a0cf-4307-82e0-034ec095749e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30bea415-8433-4cdf-8dd1-2586e7e75c1a",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "a8de79fb-51c0-424f-8a01-5a84385cd998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30bea415-8433-4cdf-8dd1-2586e7e75c1a",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        },
        {
            "id": "2e8bcf12-cb50-4254-9572-027bd343524d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "34f9bad1-ca6b-47a1-a250-3a8a924b83e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8bcf12-cb50-4254-9572-027bd343524d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22937bf-5489-4d04-886e-920b879109fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8bcf12-cb50-4254-9572-027bd343524d",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "c2872a26-8256-43bf-913e-7b2038f7454b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8bcf12-cb50-4254-9572-027bd343524d",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        },
        {
            "id": "ab06f271-b652-43fd-aa33-040ea4c953d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "c9de26c9-5503-4940-8b07-0722506071db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab06f271-b652-43fd-aa33-040ea4c953d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05474bf9-0592-4109-816d-e3bb66d4418b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab06f271-b652-43fd-aa33-040ea4c953d7",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "1dd9b6a1-928b-4e11-8a2f-c4c2ab072c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab06f271-b652-43fd-aa33-040ea4c953d7",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        },
        {
            "id": "db222c0e-afdf-4fe5-bc01-7cbf8379a568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "a45b3ba8-9f07-4113-a27f-b913825586a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db222c0e-afdf-4fe5-bc01-7cbf8379a568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b6c547-e2e7-4f8f-a16e-91cb9af0856f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db222c0e-afdf-4fe5-bc01-7cbf8379a568",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "3b73fc2b-bbbe-4f3c-b042-7785fb6f335e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db222c0e-afdf-4fe5-bc01-7cbf8379a568",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        },
        {
            "id": "e21d7516-8635-4631-8a02-c7f9270faa50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "compositeImage": {
                "id": "4e66c967-ae37-4707-b119-81ec7ffc6a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21d7516-8635-4631-8a02-c7f9270faa50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92434f61-d54e-408f-8622-3078290d25fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21d7516-8635-4631-8a02-c7f9270faa50",
                    "LayerId": "dcb2321b-0293-4311-8ffd-8a5d48aa3720"
                },
                {
                    "id": "bbe5dbc2-c8cd-4df8-b91d-aaacfa90d0b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21d7516-8635-4631-8a02-c7f9270faa50",
                    "LayerId": "f86aa363-ed20-4bae-971c-98dab8fede82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dcb2321b-0293-4311-8ffd-8a5d48aa3720",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f86aa363-ed20-4bae-971c-98dab8fede82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7a87fe9-8fc5-4b9d-ae83-27e0d4462c60",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}