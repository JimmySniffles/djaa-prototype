{
    "id": "021d31ab-dad8-4a33-b6bd-f0813e2112b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_runatk_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d54af6e-3890-47bb-a0d4-a0e15e28c229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "021d31ab-dad8-4a33-b6bd-f0813e2112b6",
            "compositeImage": {
                "id": "036f636b-8ba0-436c-aa36-b8da91f0c545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d54af6e-3890-47bb-a0d4-a0e15e28c229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0040e916-5766-4bca-a72c-2c74d8abe091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d54af6e-3890-47bb-a0d4-a0e15e28c229",
                    "LayerId": "0b2b369c-a950-4509-baa9-7887893119c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b2b369c-a950-4509-baa9-7887893119c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "021d31ab-dad8-4a33-b6bd-f0813e2112b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}