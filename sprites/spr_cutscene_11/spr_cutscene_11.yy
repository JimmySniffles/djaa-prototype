{
    "id": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54915249-1b09-413d-885e-b91782f9095b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
            "compositeImage": {
                "id": "1b0a6bbb-c3ec-471e-a620-2c054993382b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54915249-1b09-413d-885e-b91782f9095b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e59ef025-b930-4b76-ae8e-f8692eb7e88f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54915249-1b09-413d-885e-b91782f9095b",
                    "LayerId": "508b2416-febd-419e-8949-841a5448b3c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "508b2416-febd-419e-8949-841a5448b3c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 556,
    "yorig": 369
}