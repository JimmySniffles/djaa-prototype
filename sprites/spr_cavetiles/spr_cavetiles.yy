{
    "id": "a8df99be-4856-4990-9489-f14dbe1f19f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cavetiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38d9c3bf-a499-45b4-9c49-ce02722ac7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8df99be-4856-4990-9489-f14dbe1f19f4",
            "compositeImage": {
                "id": "037cd2dd-b3e6-468e-8ae1-3a7fb531cff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d9c3bf-a499-45b4-9c49-ce02722ac7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f2d8b8-471b-42e6-b8cb-543fa1696c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d9c3bf-a499-45b4-9c49-ce02722ac7d6",
                    "LayerId": "24b9401e-7ecd-4930-87de-6d4d17351015"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "24b9401e-7ecd-4930-87de-6d4d17351015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8df99be-4856-4990-9489-f14dbe1f19f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}