{
    "id": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solace_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 40,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd642967-44e2-4f7c-9e8e-10a955ff30a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
            "compositeImage": {
                "id": "867318f1-3e81-4700-bb2b-c14dac09523c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd642967-44e2-4f7c-9e8e-10a955ff30a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893b3ae3-a609-4e3e-9b86-54e810fbd813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd642967-44e2-4f7c-9e8e-10a955ff30a2",
                    "LayerId": "25a57f15-1185-431d-b563-b91c550a3975"
                }
            ]
        },
        {
            "id": "343e46a1-65d1-4c07-ac5f-1262eda47efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
            "compositeImage": {
                "id": "7c4b1479-91c9-41e4-9f7d-9105de9dac65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "343e46a1-65d1-4c07-ac5f-1262eda47efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5681f22b-bbcd-4ddc-8d11-98af8e205981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "343e46a1-65d1-4c07-ac5f-1262eda47efb",
                    "LayerId": "25a57f15-1185-431d-b563-b91c550a3975"
                }
            ]
        },
        {
            "id": "9c1e757e-726b-4ae0-82cd-221e73b4625b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
            "compositeImage": {
                "id": "1f5bc2f1-eca9-4495-9c03-5346061381d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c1e757e-726b-4ae0-82cd-221e73b4625b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b869d9c-a93b-4378-b5e9-383d732b4967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c1e757e-726b-4ae0-82cd-221e73b4625b",
                    "LayerId": "25a57f15-1185-431d-b563-b91c550a3975"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "25a57f15-1185-431d-b563-b91c550a3975",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53bcc4af-9d41-441a-a3c3-0d08a06cb8a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}