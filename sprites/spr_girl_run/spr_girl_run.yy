{
    "id": "30e21cdf-36ed-4868-8732-5946241009e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13c6734d-ebb3-41e4-9f87-2c0cb0c0c87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "3c7db51d-6bab-48dd-af1b-2ce044f6b381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c6734d-ebb3-41e4-9f87-2c0cb0c0c87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1768608c-efa1-47db-ad6c-4d33a370053d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c6734d-ebb3-41e4-9f87-2c0cb0c0c87e",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "90efbf63-197e-45de-8ff2-91ba26353c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "5b019427-88c3-4a49-b429-dcab890a059e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90efbf63-197e-45de-8ff2-91ba26353c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8f5e25-5ad4-4001-b911-11b5a4c993eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90efbf63-197e-45de-8ff2-91ba26353c84",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "5ecd01b0-9cd3-4958-8c40-458aa0473211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "6b481303-32a4-42fd-aae8-9a6dbd749fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ecd01b0-9cd3-4958-8c40-458aa0473211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb2d687-7469-4e9b-b04e-bf4f1ca783b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ecd01b0-9cd3-4958-8c40-458aa0473211",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "6ba1ad88-afc0-4298-8fdb-5ec07c253f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "37879f37-721a-4471-be77-6700ae1bbae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba1ad88-afc0-4298-8fdb-5ec07c253f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf16672-9881-4590-8427-573580fbad05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba1ad88-afc0-4298-8fdb-5ec07c253f7a",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "8ea09992-e893-4696-af95-a93247687bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "976321dd-6112-4dea-978d-0d042618b5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea09992-e893-4696-af95-a93247687bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7108f71-1661-492c-ba31-59c962b60929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea09992-e893-4696-af95-a93247687bdb",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "e8ebe489-dfb4-42f2-b90c-1539c6a46fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "e065061f-d255-48d0-a589-7dec2fc13750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ebe489-dfb4-42f2-b90c-1539c6a46fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8872a29b-a9d2-4e65-b616-934a6d341965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ebe489-dfb4-42f2-b90c-1539c6a46fb0",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "9b2adaf6-f530-41a8-abad-3f13d9f80788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "7c7d7461-f1d7-4c21-bc65-30df66510c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2adaf6-f530-41a8-abad-3f13d9f80788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b343845-1429-4df0-a0a0-f8dd9b94f9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2adaf6-f530-41a8-abad-3f13d9f80788",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "08e6f743-0a11-420e-b350-75596f468fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "12705c06-43f4-45d4-b843-5b9c7b9ab2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08e6f743-0a11-420e-b350-75596f468fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b181e226-5049-49af-8f0a-43a780a65f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08e6f743-0a11-420e-b350-75596f468fde",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "87aebf25-ecb3-4fec-b955-2519f7e12444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "83bc0f95-1bdc-40f0-bbd2-9b0f2edca7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87aebf25-ecb3-4fec-b955-2519f7e12444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a8e33e-3ba8-4736-93b0-5b4d47b556a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87aebf25-ecb3-4fec-b955-2519f7e12444",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "ec7cf072-2e32-4af6-8ecf-60b5bdf45768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "a506084f-1f61-44f0-9dc7-0562ca338d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7cf072-2e32-4af6-8ecf-60b5bdf45768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f13b0cc-2be6-44f5-bcd9-7fb36e698140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7cf072-2e32-4af6-8ecf-60b5bdf45768",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "54dcd8c2-4acb-42ca-9763-52f9d5dc1438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "a5d5489c-d3a4-49c7-a077-6f9d7106c55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54dcd8c2-4acb-42ca-9763-52f9d5dc1438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea792de4-875c-4ff9-bd06-4e559e106b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54dcd8c2-4acb-42ca-9763-52f9d5dc1438",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "cbea58c5-7647-4482-8f51-dc1146c9dfad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "200e0a58-c148-44f5-88bb-3a63ce2572eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbea58c5-7647-4482-8f51-dc1146c9dfad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f992d043-c7de-45c4-b69f-5a7c359d33f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbea58c5-7647-4482-8f51-dc1146c9dfad",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "cc0a69a4-c456-4e8f-b414-226183213038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "d32e96e9-af21-4f8d-b224-52fa95290085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0a69a4-c456-4e8f-b414-226183213038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ba19b52-2004-4021-aab1-6a0b79b378e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0a69a4-c456-4e8f-b414-226183213038",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "36b184b4-1d99-49a2-beae-15e5a0740127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "d1295c9b-3fb8-4292-a798-0425ede150cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b184b4-1d99-49a2-beae-15e5a0740127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b3fe39-080f-44bd-99eb-6a14fd98bcc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b184b4-1d99-49a2-beae-15e5a0740127",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "b5e5f9a2-f099-4dea-b981-cfb573611db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "9013acfd-d394-4cd7-971b-62f6b38d5478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e5f9a2-f099-4dea-b981-cfb573611db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "568fb495-7316-42f6-9594-92b22d24e6c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e5f9a2-f099-4dea-b981-cfb573611db8",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        },
        {
            "id": "e687a51f-ac93-4571-9e56-4357d4b16aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "compositeImage": {
                "id": "17eb010a-a1ad-45f9-88fa-00afe3c705e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e687a51f-ac93-4571-9e56-4357d4b16aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929b2ccd-a21e-45c2-bb40-96e8bb43668f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e687a51f-ac93-4571-9e56-4357d4b16aa8",
                    "LayerId": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a6c53a5c-affd-4184-8c4d-3ce7dbc23353",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30e21cdf-36ed-4868-8732-5946241009e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}