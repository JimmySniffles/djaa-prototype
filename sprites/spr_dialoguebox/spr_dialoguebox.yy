{
    "id": "737014f7-fe43-4787-af15-308aabd39b98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialoguebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57200978-2bb6-41d7-bcee-ba0ce8585875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "737014f7-fe43-4787-af15-308aabd39b98",
            "compositeImage": {
                "id": "43e55ea9-ef22-4436-9317-c905d9fbd2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57200978-2bb6-41d7-bcee-ba0ce8585875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa745aec-80ce-4014-8920-3ec753f301a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57200978-2bb6-41d7-bcee-ba0ce8585875",
                    "LayerId": "a8d12d2e-f196-4c38-8bab-951674f837e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "a8d12d2e-f196-4c38-8bab-951674f837e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "737014f7-fe43-4787-af15-308aabd39b98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}