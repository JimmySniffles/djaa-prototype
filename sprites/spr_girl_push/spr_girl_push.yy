{
    "id": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_push",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 45,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc7a8814-5cc2-48e2-99f0-224fd4f1af43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "eb5a48c3-a17f-4aa3-b9c1-7008b4d89d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc7a8814-5cc2-48e2-99f0-224fd4f1af43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "222b9d54-f7f1-4eb9-9ed3-7e1a5c0337f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc7a8814-5cc2-48e2-99f0-224fd4f1af43",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "ae2f11f3-93c6-4cfa-ac2e-9c54db47b896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "77b21559-e836-4d00-be6d-c7fefaca0551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae2f11f3-93c6-4cfa-ac2e-9c54db47b896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2613e414-39bc-4e30-b9e8-44d3c4b3e6b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae2f11f3-93c6-4cfa-ac2e-9c54db47b896",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "19cfe29c-48a3-4f1a-af89-f8a944c858fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "419dc3bb-de2d-4107-97b5-58f3c410a4e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19cfe29c-48a3-4f1a-af89-f8a944c858fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d226a374-6d19-45fe-9d27-4473989daabe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19cfe29c-48a3-4f1a-af89-f8a944c858fb",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "637122bf-dee1-4db3-a2db-d991c652f928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "e51f5f23-83d1-427f-a050-b6a5b0a744a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "637122bf-dee1-4db3-a2db-d991c652f928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "031fdc69-138c-4b8f-b25e-48162bd2c306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "637122bf-dee1-4db3-a2db-d991c652f928",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "8a67d92b-340c-4460-b252-fe3e7aa93ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "ec46c8d8-e8dd-45ef-83c6-588c5535d20b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a67d92b-340c-4460-b252-fe3e7aa93ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51cc16f5-da75-403f-9e26-8628ebecc317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a67d92b-340c-4460-b252-fe3e7aa93ccb",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "58ed0bea-0a4d-4314-a6bd-1f521dbdaeec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "68559c1e-2742-4b10-baa2-b8df64da97aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ed0bea-0a4d-4314-a6bd-1f521dbdaeec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c62a74-55f6-4eb9-b2c6-37d85b0f1b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ed0bea-0a4d-4314-a6bd-1f521dbdaeec",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "888a459b-1cf8-46d4-9727-ea9ed198aa50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "017fa494-5758-4498-8be1-aa031d376d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888a459b-1cf8-46d4-9727-ea9ed198aa50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a5e207-2282-4efc-bcc0-180e9ef62376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888a459b-1cf8-46d4-9727-ea9ed198aa50",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "612eb7ea-91e4-41ce-b91a-8d20f456e915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "d14907d8-7c59-49ca-b198-f577a0d4703d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612eb7ea-91e4-41ce-b91a-8d20f456e915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689e7682-6755-45a1-bbe4-47a2fe4311bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612eb7ea-91e4-41ce-b91a-8d20f456e915",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "6036ec8f-ddc7-4b6c-ba2c-713fe2ee854f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "4f0d095c-13aa-407a-b3c8-cc5eb8a7bcda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6036ec8f-ddc7-4b6c-ba2c-713fe2ee854f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5535180-40b3-459b-a1d4-7e5b4ad6f702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6036ec8f-ddc7-4b6c-ba2c-713fe2ee854f",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "9395a501-2c36-465f-9ab6-37b2bfae2007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "ed1f5bd4-e81c-474a-b29f-76e4f3a8418e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9395a501-2c36-465f-9ab6-37b2bfae2007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718d4677-590f-481c-8d40-4d1362dcfaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9395a501-2c36-465f-9ab6-37b2bfae2007",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "e21d16a2-ac7f-4f88-a695-5f3ec8aafe01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "e024be82-f13f-4718-b7f6-e94a77c9fcad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21d16a2-ac7f-4f88-a695-5f3ec8aafe01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e2e3cf-87c7-49b9-aa0a-f2b730b2af2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21d16a2-ac7f-4f88-a695-5f3ec8aafe01",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "dc34cc34-5438-409b-ba2d-a60b974df05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "1569334a-7413-480d-919e-7a8d3eb1c11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc34cc34-5438-409b-ba2d-a60b974df05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030a7704-05ca-4d7a-9955-dff2592e6392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc34cc34-5438-409b-ba2d-a60b974df05d",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "7cf3ed80-4aac-42af-a34c-6e13fbbc8e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "54203d13-a4d7-4168-afd8-ed14f60ffef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf3ed80-4aac-42af-a34c-6e13fbbc8e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c77f6b-cf94-42a4-8237-7bbb0212ed6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf3ed80-4aac-42af-a34c-6e13fbbc8e2c",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "c56599be-19dd-47f7-912c-0f1ad98e16af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "67aabc87-aeb0-47ad-a2b2-3551b1be5165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56599be-19dd-47f7-912c-0f1ad98e16af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e3cd39-bbcd-4dce-bd72-f80de4b8cd6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56599be-19dd-47f7-912c-0f1ad98e16af",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "ca93795b-5d5a-4fc5-bd54-674c721de57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "8fc83fd6-be54-42d3-a5c3-aafbe2b07093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca93795b-5d5a-4fc5-bd54-674c721de57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664a7816-fb90-4c83-8864-e4275d49b812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca93795b-5d5a-4fc5-bd54-674c721de57b",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "579ec110-b3b3-4a17-9d55-8a190ec64fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "4fe5ed32-8dc4-4bb3-a69b-aa63af38cf90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579ec110-b3b3-4a17-9d55-8a190ec64fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb01194-916d-48fd-80dc-47881689ff3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579ec110-b3b3-4a17-9d55-8a190ec64fd4",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "a81e2b94-6787-4ed2-a28a-9f18aaaf8180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "a74baa31-5ac4-4cea-a925-baf8ac0048ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81e2b94-6787-4ed2-a28a-9f18aaaf8180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8343dc62-8fb4-4ff4-8820-7858c8ad5233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81e2b94-6787-4ed2-a28a-9f18aaaf8180",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        },
        {
            "id": "9885c51e-c71f-4941-99c3-dd6996113b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "compositeImage": {
                "id": "4104fa26-58a0-4037-a0f0-6dae5aee06b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9885c51e-c71f-4941-99c3-dd6996113b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a1ce8b-c2b8-4a7d-8f64-84060a24e39b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9885c51e-c71f-4941-99c3-dd6996113b4c",
                    "LayerId": "93ac88a4-83c1-4e61-9e12-c06025e89b58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "93ac88a4-83c1-4e61-9e12-c06025e89b58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "092f85dc-28cb-451a-a1b9-3a9ebb6e2422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}