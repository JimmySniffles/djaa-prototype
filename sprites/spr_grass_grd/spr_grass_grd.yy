{
    "id": "fc6015c6-fdf1-4a8e-9d66-803f91f13e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass_grd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "133d46f5-6b06-4e21-bb12-ef92bb59788b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc6015c6-fdf1-4a8e-9d66-803f91f13e62",
            "compositeImage": {
                "id": "77e87b22-1734-4f4b-9a8b-054e61b624b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133d46f5-6b06-4e21-bb12-ef92bb59788b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3233a4-0d85-4b6a-bcee-9fc8b6701e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133d46f5-6b06-4e21-bb12-ef92bb59788b",
                    "LayerId": "673bd37d-544a-422e-a529-07a72e66ec98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "673bd37d-544a-422e-a529-07a72e66ec98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc6015c6-fdf1-4a8e-9d66-803f91f13e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 192
}