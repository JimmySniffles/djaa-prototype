{
    "id": "06942b7e-13df-4ed7-a7b9-c593d4eb8f06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_slide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "806be05b-e740-4a7a-bc9b-8ec542f21596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06942b7e-13df-4ed7-a7b9-c593d4eb8f06",
            "compositeImage": {
                "id": "aafcaa50-3701-48f7-9655-72c42fa98936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "806be05b-e740-4a7a-bc9b-8ec542f21596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1cdbe6-2d5c-4408-9c51-f0f8b2dc6643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "806be05b-e740-4a7a-bc9b-8ec542f21596",
                    "LayerId": "6de9854f-a417-4da4-bfc6-531ccafa2c45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6de9854f-a417-4da4-bfc6-531ccafa2c45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06942b7e-13df-4ed7-a7b9-c593d4eb8f06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}