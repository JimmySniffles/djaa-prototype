{
    "id": "5b3f7266-5668-4d22-9b14-7a79221c5e78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06e1619a-4267-4119-addd-38c20e8631f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b3f7266-5668-4d22-9b14-7a79221c5e78",
            "compositeImage": {
                "id": "073aca64-6c9d-4e14-9e4c-c83d1f7a73d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e1619a-4267-4119-addd-38c20e8631f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1346c7-37b7-4e3e-81a3-e22f84b42c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e1619a-4267-4119-addd-38c20e8631f0",
                    "LayerId": "8a746ee0-0916-4d31-aa09-5fc145077ec2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a746ee0-0916-4d31-aa09-5fc145077ec2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b3f7266-5668-4d22-9b14-7a79221c5e78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}