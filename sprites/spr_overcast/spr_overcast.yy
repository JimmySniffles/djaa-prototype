{
    "id": "8d40c1c7-38f1-404c-827f-6636e862569a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overcast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 419,
    "bbox_left": 0,
    "bbox_right": 1199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b44163d9-142d-42a7-8505-69b5d9235adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d40c1c7-38f1-404c-827f-6636e862569a",
            "compositeImage": {
                "id": "487844a0-a4a9-4286-8620-2c49702ff693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b44163d9-142d-42a7-8505-69b5d9235adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a05acc7-c0ee-45bf-bc97-06fd971c3b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b44163d9-142d-42a7-8505-69b5d9235adb",
                    "LayerId": "59a7e7dd-1b28-4644-8d54-e30472b62433"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 420,
    "layers": [
        {
            "id": "59a7e7dd-1b28-4644-8d54-e30472b62433",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d40c1c7-38f1-404c-827f-6636e862569a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1200,
    "xorig": 0,
    "yorig": 0
}