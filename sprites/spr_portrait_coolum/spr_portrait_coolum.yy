{
    "id": "a4ae7f2d-36b4-46ab-af31-2a4eb2567897",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_coolum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0040fcc-2c5d-4883-8a7a-83ae440abf9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4ae7f2d-36b4-46ab-af31-2a4eb2567897",
            "compositeImage": {
                "id": "f93fccca-cae5-4496-ae0a-a8e0191021e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0040fcc-2c5d-4883-8a7a-83ae440abf9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041729ef-5fc2-4711-aa65-62373d8928e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0040fcc-2c5d-4883-8a7a-83ae440abf9c",
                    "LayerId": "de13589d-08ca-45c4-92cc-4829e395c641"
                }
            ]
        },
        {
            "id": "8303110e-8820-4213-8a47-d5cc8131dd0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4ae7f2d-36b4-46ab-af31-2a4eb2567897",
            "compositeImage": {
                "id": "aed94cd0-14f1-4a1e-ae1b-3763dd9c643d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8303110e-8820-4213-8a47-d5cc8131dd0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a3fb23-10a6-4e6c-b750-2f1a8e5c47e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8303110e-8820-4213-8a47-d5cc8131dd0f",
                    "LayerId": "de13589d-08ca-45c4-92cc-4829e395c641"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "de13589d-08ca-45c4-92cc-4829e395c641",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4ae7f2d-36b4-46ab-af31-2a4eb2567897",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}