{
    "id": "a1402bfe-2775-4d07-b9e3-0d961749514c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mountains",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 1199,
    "bbox_top": 373,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45ad81c0-ca95-4e36-a2a1-a1deabecd0a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1402bfe-2775-4d07-b9e3-0d961749514c",
            "compositeImage": {
                "id": "ee312de5-4e15-4b65-ae42-d4f27088791f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ad81c0-ca95-4e36-a2a1-a1deabecd0a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150b5122-0353-4d64-aaae-4f5a31030d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ad81c0-ca95-4e36-a2a1-a1deabecd0a6",
                    "LayerId": "c7f1d9a0-de60-4e19-8b0e-1a634204a0d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "c7f1d9a0-de60-4e19-8b0e-1a634204a0d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1402bfe-2775-4d07-b9e3-0d961749514c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1200,
    "xorig": 0,
    "yorig": 0
}