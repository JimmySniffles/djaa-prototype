{
    "id": "f8838995-9512-4bdf-b554-a1def3aed5f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_sa_a_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b48e7d01-d76f-4e74-b742-0156ab6c15aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "06c67a16-bc94-453b-b137-86545e5f180c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48e7d01-d76f-4e74-b742-0156ab6c15aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbedeab-3ea2-4f5a-a650-249e29dfc16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48e7d01-d76f-4e74-b742-0156ab6c15aa",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "fa361ced-bf84-40f9-96c0-ddfad1c7841b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "bc637165-aee0-4abe-8056-f6f3c359f198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa361ced-bf84-40f9-96c0-ddfad1c7841b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6452947-d9c9-4ad6-8e34-d2ebe41ff12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa361ced-bf84-40f9-96c0-ddfad1c7841b",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "c3402385-fcc5-471a-83ca-eaffb36c7a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "0992ce3b-a584-4e3e-963f-6d0e418a906a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3402385-fcc5-471a-83ca-eaffb36c7a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "438418d5-a754-491b-8b6a-3fc110104439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3402385-fcc5-471a-83ca-eaffb36c7a32",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "e090be12-5cb6-483a-a8a2-7fed36e2d80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "f5da53b5-3117-4322-bec1-d52eae69f77f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e090be12-5cb6-483a-a8a2-7fed36e2d80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118981fc-91e0-416a-a576-df42a4fac5f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e090be12-5cb6-483a-a8a2-7fed36e2d80b",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "13afa1eb-eaae-49cb-97b5-446f6b1c183a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "624fb62f-3aa1-4862-a2ec-1a74f0b9776d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13afa1eb-eaae-49cb-97b5-446f6b1c183a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db24f686-8c97-44ba-8d83-7559348c5c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13afa1eb-eaae-49cb-97b5-446f6b1c183a",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "d8262f27-a277-4da2-ad64-59d8d06f0ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "754b14ef-9c1b-4ddf-bf26-39867d486336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8262f27-a277-4da2-ad64-59d8d06f0ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87fac743-57b9-4242-b930-1e75c2c128e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8262f27-a277-4da2-ad64-59d8d06f0ad6",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "8b0a36e8-db40-4d09-a317-3ceee5c9ec8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "ef871f76-2dc1-468b-aa55-9d509bc81d09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0a36e8-db40-4d09-a317-3ceee5c9ec8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84dada0c-66cd-4d2b-baf1-5d9c4e48d0f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0a36e8-db40-4d09-a317-3ceee5c9ec8e",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        },
        {
            "id": "a4454e20-0999-4a9e-91ae-5c43586c4269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "compositeImage": {
                "id": "2b4ae8b9-e3dd-4f2c-938d-dd883b36bbe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4454e20-0999-4a9e-91ae-5c43586c4269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07fe909d-2e2b-4fae-b976-d6c379f141ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4454e20-0999-4a9e-91ae-5c43586c4269",
                    "LayerId": "d22dc186-daa7-49b2-b798-81d30c8f079d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d22dc186-daa7-49b2-b798-81d30c8f079d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8838995-9512-4bdf-b554-a1def3aed5f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}