{
    "id": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_runatk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e05564e9-e548-4efc-93d1-8436acdb7ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "fce89aa1-7e92-4769-8a93-3bfdd04176ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05564e9-e548-4efc-93d1-8436acdb7ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aaddc14-faae-42a6-8f77-cc5c25125846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05564e9-e548-4efc-93d1-8436acdb7ee6",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "49d6004a-b9af-4d88-9d40-7a0951375ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05564e9-e548-4efc-93d1-8436acdb7ee6",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        },
        {
            "id": "022d806b-68ba-4578-9604-5dfc705e08cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "511fe872-ded9-4265-8a4a-8b9dbbca5653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "022d806b-68ba-4578-9604-5dfc705e08cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4690ab33-9feb-46ee-8e30-8a1d408bde1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "022d806b-68ba-4578-9604-5dfc705e08cf",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "c57c4c2a-567f-4e4c-8096-383988b26e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "022d806b-68ba-4578-9604-5dfc705e08cf",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        },
        {
            "id": "57d7f684-5bd4-49df-bdef-860c7f91bfb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "71e2e7aa-26b1-43ec-afa0-14795244f3ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d7f684-5bd4-49df-bdef-860c7f91bfb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80c4326a-1ea2-4d65-8c93-6e9aafad5941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d7f684-5bd4-49df-bdef-860c7f91bfb2",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "48d90c97-7bf2-4cc8-a906-fd25c5b3a1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d7f684-5bd4-49df-bdef-860c7f91bfb2",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        },
        {
            "id": "e2fcfb94-ebb1-41bb-8a30-2509a3e52142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "2c7666a0-b1b3-4390-b2d9-424dbbb0ba95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2fcfb94-ebb1-41bb-8a30-2509a3e52142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "818691f4-daae-410a-8a76-6e5bdb97533d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fcfb94-ebb1-41bb-8a30-2509a3e52142",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "c638b82d-a1a7-487d-9902-2842a5b60196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fcfb94-ebb1-41bb-8a30-2509a3e52142",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        },
        {
            "id": "b04fd610-7035-4530-872a-2b0b275beabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "4055f54c-c687-4fdb-ac54-60f0c50f6810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04fd610-7035-4530-872a-2b0b275beabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e01efc-0c6f-40bf-aab3-3c0ebba173b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04fd610-7035-4530-872a-2b0b275beabf",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "e7b31e5d-89ef-46f6-8cbd-6b8d2d9eca6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04fd610-7035-4530-872a-2b0b275beabf",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        },
        {
            "id": "409177b6-742d-4f0f-90bb-3e6e9076eb9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "compositeImage": {
                "id": "3836d2d7-6f3e-46d6-bcad-188dec51497b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "409177b6-742d-4f0f-90bb-3e6e9076eb9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39cb7703-d1a9-4dd2-873d-03aecc632869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "409177b6-742d-4f0f-90bb-3e6e9076eb9d",
                    "LayerId": "341613a3-be9e-4f56-96b6-974c46c40887"
                },
                {
                    "id": "bc4df55e-ef2a-4af2-99c0-5db29058c5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "409177b6-742d-4f0f-90bb-3e6e9076eb9d",
                    "LayerId": "8060ffca-a805-4b09-8c2f-cd26f069f170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "341613a3-be9e-4f56-96b6-974c46c40887",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8060ffca-a805-4b09-8c2f-cd26f069f170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f92387c-8af5-48fd-83b5-63ea5ebb9779",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}