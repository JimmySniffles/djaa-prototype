{
    "id": "740658b2-5267-4052-9039-3588435fab64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkoala",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8644c66-0c20-46ed-8c43-733dc007af8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740658b2-5267-4052-9039-3588435fab64",
            "compositeImage": {
                "id": "f87c90a4-113e-4213-bc4d-fa817d007830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8644c66-0c20-46ed-8c43-733dc007af8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75828a32-7789-41b8-af85-ada5ad32bcc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8644c66-0c20-46ed-8c43-733dc007af8c",
                    "LayerId": "491dcbb4-e15d-4159-86ce-7f6b3060dbfb"
                }
            ]
        },
        {
            "id": "b0c37312-f52d-40e7-9b08-41777947dbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740658b2-5267-4052-9039-3588435fab64",
            "compositeImage": {
                "id": "4c3a4a9b-2d7a-4cdd-a692-f8b4d432845c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c37312-f52d-40e7-9b08-41777947dbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e61525-dbf9-4452-b28b-96312315412d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c37312-f52d-40e7-9b08-41777947dbca",
                    "LayerId": "491dcbb4-e15d-4159-86ce-7f6b3060dbfb"
                }
            ]
        },
        {
            "id": "598d63ec-b94e-4e29-9758-3a93ccc97487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740658b2-5267-4052-9039-3588435fab64",
            "compositeImage": {
                "id": "1af97cdf-5e57-46b3-92a5-a78a62799b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598d63ec-b94e-4e29-9758-3a93ccc97487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688c25e4-e62c-40a4-b1e5-c7c8d9ea897c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598d63ec-b94e-4e29-9758-3a93ccc97487",
                    "LayerId": "491dcbb4-e15d-4159-86ce-7f6b3060dbfb"
                }
            ]
        },
        {
            "id": "caf2b6e9-32ee-4346-8acb-9d5a3f45c060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740658b2-5267-4052-9039-3588435fab64",
            "compositeImage": {
                "id": "7712fd66-0baa-43be-8002-f8cdb24d489f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf2b6e9-32ee-4346-8acb-9d5a3f45c060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ec9045-0cfa-4539-adc2-c7b664a781cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf2b6e9-32ee-4346-8acb-9d5a3f45c060",
                    "LayerId": "491dcbb4-e15d-4159-86ce-7f6b3060dbfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "491dcbb4-e15d-4159-86ce-7f6b3060dbfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "740658b2-5267-4052-9039-3588435fab64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}