{
    "id": "10e35adc-b308-476a-af7b-444686dded97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_trees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 431,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c52443-8dea-4e8a-ae33-8a90964a7d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e35adc-b308-476a-af7b-444686dded97",
            "compositeImage": {
                "id": "c96d680d-2bd6-44ac-a077-66f0bab7e735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c52443-8dea-4e8a-ae33-8a90964a7d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fa30a3-bbf6-46df-9f2c-bcddd67e8622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c52443-8dea-4e8a-ae33-8a90964a7d7d",
                    "LayerId": "86fa775b-953f-4e31-9d11-d4a06e028888"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "86fa775b-953f-4e31-9d11-d4a06e028888",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e35adc-b308-476a-af7b-444686dded97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 448,
    "xorig": 0,
    "yorig": 0
}