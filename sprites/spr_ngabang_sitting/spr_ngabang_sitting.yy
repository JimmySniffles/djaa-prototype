{
    "id": "a34e21c2-e548-49dc-906c-8111ce615d94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ngabang_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 21,
    "bbox_right": 45,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c835a85e-3127-45a1-a636-87bc63edbf09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a34e21c2-e548-49dc-906c-8111ce615d94",
            "compositeImage": {
                "id": "2409b8b5-5241-41f4-aae3-b2ef17e1daac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c835a85e-3127-45a1-a636-87bc63edbf09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4420cb8a-a881-4de6-b063-64cee3c70b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c835a85e-3127-45a1-a636-87bc63edbf09",
                    "LayerId": "91708ef3-2a54-4613-b120-b45ef2cc509c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "91708ef3-2a54-4613-b120-b45ef2cc509c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a34e21c2-e548-49dc-906c-8111ce615d94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}