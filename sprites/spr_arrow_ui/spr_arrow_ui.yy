{
    "id": "6001e9d9-b60b-460d-a82c-eb633e1c5330",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a192d1-996f-4fb2-a054-0f942c436470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6001e9d9-b60b-460d-a82c-eb633e1c5330",
            "compositeImage": {
                "id": "6f8c29c0-056d-463c-bb47-50a2456f8ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a192d1-996f-4fb2-a054-0f942c436470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba73bf7-3043-4760-8f30-9b251d2279f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a192d1-996f-4fb2-a054-0f942c436470",
                    "LayerId": "54362f74-03ac-4b5e-a639-27a1d3b5f4d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "54362f74-03ac-4b5e-a639-27a1d3b5f4d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6001e9d9-b60b-460d-a82c-eb633e1c5330",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 75
}