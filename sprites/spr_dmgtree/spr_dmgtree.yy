{
    "id": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dmgtree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 259,
    "bbox_left": 42,
    "bbox_right": 81,
    "bbox_top": 107,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00047fca-b21c-410f-9018-b3365460545a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "23c4dcab-0a5d-4ea0-8f97-de9330129491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00047fca-b21c-410f-9018-b3365460545a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed450431-9e85-47af-b907-dcbc32cc9c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00047fca-b21c-410f-9018-b3365460545a",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "87913d78-21dc-4055-aa49-17e562fa9e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "79fc584d-6c90-4e2a-91ce-c9fd7db190c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87913d78-21dc-4055-aa49-17e562fa9e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8a7885-5692-42cb-9d3e-a5d4ffd6f022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87913d78-21dc-4055-aa49-17e562fa9e63",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "caefa440-ede6-45bb-b7a4-0699c4f2871a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "c7cf2592-bf15-4537-b072-c39b73c9b066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caefa440-ede6-45bb-b7a4-0699c4f2871a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9854fb61-2e65-488f-9ec0-60c705634213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caefa440-ede6-45bb-b7a4-0699c4f2871a",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "9bccfd56-4ede-4511-93fe-ea48337e8490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "a3ed7a5a-a60e-466e-a32d-7c78af15e0ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bccfd56-4ede-4511-93fe-ea48337e8490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e17520fd-90bd-486b-8873-323d18bed43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bccfd56-4ede-4511-93fe-ea48337e8490",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "02b0b1e9-c371-4d3a-9493-7c2c2f570580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "393607f3-594a-49ff-8d35-62d5c878625b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b0b1e9-c371-4d3a-9493-7c2c2f570580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0845bee5-1ef1-43e0-bbd5-3eee451ddef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b0b1e9-c371-4d3a-9493-7c2c2f570580",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "3c5c14ae-548c-475e-8f85-2b3f7f99c11b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "d4624823-7ff1-4cad-bf39-b6008f0c6de7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c5c14ae-548c-475e-8f85-2b3f7f99c11b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5a191b-a7e5-44a0-b934-7c7d5c519a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5c14ae-548c-475e-8f85-2b3f7f99c11b",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "800c7bc9-51f2-49ce-9f80-86f6311ff69d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "7ab9bfe2-fa18-4539-8034-d86938ec634b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "800c7bc9-51f2-49ce-9f80-86f6311ff69d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c09080-63d6-4a75-9a90-dc04e118e15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "800c7bc9-51f2-49ce-9f80-86f6311ff69d",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "bbd34bcd-12c2-42d9-93c3-8478a604d79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "0f1fbd44-30d3-4b5b-bae4-1c315a371e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd34bcd-12c2-42d9-93c3-8478a604d79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d951d50c-7417-4b82-98b9-36ba1b4f53af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd34bcd-12c2-42d9-93c3-8478a604d79c",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "fb6e1bbb-67fd-4974-87a7-e1b76940cea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "85612d89-9271-481f-948f-5535dbb42ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6e1bbb-67fd-4974-87a7-e1b76940cea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a7f53b-ff31-41c9-a698-4e236914103e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6e1bbb-67fd-4974-87a7-e1b76940cea6",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "e6a02b21-4933-447c-8420-96cce414a37b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "23da71a8-7fed-48af-8a95-4df3d42aea9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a02b21-4933-447c-8420-96cce414a37b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b877ca-8f51-4ad2-8895-3bc257e478f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a02b21-4933-447c-8420-96cce414a37b",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "7f8e9d38-5a44-4469-bd46-73d8b34d7b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "de052a14-8a21-4b7f-b009-f7b306ada814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8e9d38-5a44-4469-bd46-73d8b34d7b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2511246-1614-40e3-9428-a5eff48852ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8e9d38-5a44-4469-bd46-73d8b34d7b3c",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "10f5b5ed-e0c0-4efb-aa5d-29f495a7b890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "19c818ad-29e1-4ce2-ba52-216d2dfbc39d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f5b5ed-e0c0-4efb-aa5d-29f495a7b890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4478ac3e-c5b4-4404-83a3-2fe8d850ea6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f5b5ed-e0c0-4efb-aa5d-29f495a7b890",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        },
        {
            "id": "411feb3d-b313-4298-ae6f-966a017d476c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "compositeImage": {
                "id": "b1d1eb24-d8b0-467b-bc38-9a0aac61947e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "411feb3d-b313-4298-ae6f-966a017d476c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5dec427-d0e4-410f-bdef-9587e25f1e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "411feb3d-b313-4298-ae6f-966a017d476c",
                    "LayerId": "1138b24e-e182-4acc-946c-a47c720d8391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1138b24e-e182-4acc-946c-a47c720d8391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18d8af42-8279-455f-a4b2-3ac3584ca44f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 351,
    "yorig": 352
}