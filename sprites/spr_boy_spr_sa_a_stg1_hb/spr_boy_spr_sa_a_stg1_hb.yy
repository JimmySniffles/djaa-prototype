{
    "id": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_sa_a_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "211804a2-d64c-49ad-b0f1-d6e17699d495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "63977703-3777-4a94-a80c-5d37e989af91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "211804a2-d64c-49ad-b0f1-d6e17699d495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bcedd9-056e-4c31-839b-f16e5fa736cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "211804a2-d64c-49ad-b0f1-d6e17699d495",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "56b9e226-aa19-4954-bdd5-acd966ace9ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "211804a2-d64c-49ad-b0f1-d6e17699d495",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "ce68086f-5323-451f-a8ad-affa821b0487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "77db19c2-293a-4db0-bdd6-12e76df546bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce68086f-5323-451f-a8ad-affa821b0487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ba6273-b9c4-463f-a0ba-2e8ea43c8afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce68086f-5323-451f-a8ad-affa821b0487",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "e2b2b0f7-16bb-45a2-a6e1-21046b3521f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce68086f-5323-451f-a8ad-affa821b0487",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "56cd1a3d-d5a0-484b-ad18-f131ec6b8bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "63f9110f-7f15-4edf-a9e8-ef79b38cb7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cd1a3d-d5a0-484b-ad18-f131ec6b8bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8bf4be-db78-485d-9ad5-d8333a696bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cd1a3d-d5a0-484b-ad18-f131ec6b8bc1",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "cb4f7920-8f83-42a0-bb96-994d7115ce58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cd1a3d-d5a0-484b-ad18-f131ec6b8bc1",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "b2315ab5-2a1a-4d05-8a23-15fe527e6c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "e8978710-7926-4309-8405-a76d1cb3e0b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2315ab5-2a1a-4d05-8a23-15fe527e6c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21fde7a-ce99-4e4e-817d-1d653eb76846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2315ab5-2a1a-4d05-8a23-15fe527e6c1d",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "c8081d7e-1d0f-474b-8d23-6efa776e8149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2315ab5-2a1a-4d05-8a23-15fe527e6c1d",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "9a70825d-bd5f-4c98-b668-5206c97a364b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "a2002381-776a-433e-8691-82b2071939a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a70825d-bd5f-4c98-b668-5206c97a364b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa9f46a4-bb7b-440f-81e7-770195ef783c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a70825d-bd5f-4c98-b668-5206c97a364b",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "d4ea5573-5661-4871-8775-942b76520953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a70825d-bd5f-4c98-b668-5206c97a364b",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "e9f17c40-bb09-4b35-8367-7cf33fd35795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "a6a36acb-0f13-4cfe-afb1-e53f8b708fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f17c40-bb09-4b35-8367-7cf33fd35795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a5d2a7-6912-4cda-abfa-d1e7df0be36f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f17c40-bb09-4b35-8367-7cf33fd35795",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "caae6916-8514-4b5f-9077-db39c79e4b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f17c40-bb09-4b35-8367-7cf33fd35795",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "3d5f4e94-712a-4a35-8d57-ef2a24615a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "9db104de-7607-43de-a39f-449753e4339d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5f4e94-712a-4a35-8d57-ef2a24615a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f33937-1eb3-412b-8d4f-db0ae5284be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5f4e94-712a-4a35-8d57-ef2a24615a86",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "26ac0536-dd74-4a58-b369-fcca38c4c1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5f4e94-712a-4a35-8d57-ef2a24615a86",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        },
        {
            "id": "9dcfbbb0-edf8-4844-92ed-e833b6f17cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "compositeImage": {
                "id": "ebbdabae-0c0d-48eb-8f07-d93fe30ec92a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dcfbbb0-edf8-4844-92ed-e833b6f17cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7003ce73-571d-4219-8a3d-7d740cadeefd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dcfbbb0-edf8-4844-92ed-e833b6f17cbb",
                    "LayerId": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee"
                },
                {
                    "id": "8ec48501-3f15-4fae-8443-f6199fbeeb68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dcfbbb0-edf8-4844-92ed-e833b6f17cbb",
                    "LayerId": "67779f4c-1aba-4872-97d2-6b417da5969b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7ecdd56a-cb8c-44dd-863e-bb30e87810ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "67779f4c-1aba-4872-97d2-6b417da5969b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8252fab6-aca6-41fb-9d2d-a2e229e30521",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}