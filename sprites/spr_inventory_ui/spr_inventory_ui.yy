{
    "id": "da8fb914-4e45-42ff-80a8-521d5eda0946",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 769,
    "bbox_left": 0,
    "bbox_right": 1369,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1be48fcf-9d06-42e7-bbbf-2fecf7946335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da8fb914-4e45-42ff-80a8-521d5eda0946",
            "compositeImage": {
                "id": "8aaf7c7d-9c99-46b5-854b-8d4b99e07cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be48fcf-9d06-42e7-bbbf-2fecf7946335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af9d0b4e-98ae-4383-9ef2-fbc9143337a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be48fcf-9d06-42e7-bbbf-2fecf7946335",
                    "LayerId": "822f6bec-d190-4bda-8347-ec586bf7b342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 770,
    "layers": [
        {
            "id": "822f6bec-d190-4bda-8347-ec586bf7b342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da8fb914-4e45-42ff-80a8-521d5eda0946",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1370,
    "xorig": 0,
    "yorig": 0
}