{
    "id": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_djagi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 6,
    "bbox_right": 108,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "945de63c-132a-4de3-bca9-a8448915df07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
            "compositeImage": {
                "id": "23b1b641-8ad2-4d33-9f96-e08aa93acdcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945de63c-132a-4de3-bca9-a8448915df07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24fafdbf-21ab-4087-89f2-9d4be6b03fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945de63c-132a-4de3-bca9-a8448915df07",
                    "LayerId": "3d330474-9e5a-466c-98cd-f8b6d21421b9"
                }
            ]
        },
        {
            "id": "50f9c556-827e-46ed-857e-4e8aa34ee856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
            "compositeImage": {
                "id": "9cbc43f9-2f37-407c-ada5-7469ab5b1bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f9c556-827e-46ed-857e-4e8aa34ee856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd6ab08-43de-492b-bf36-fd7d93ed0100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f9c556-827e-46ed-857e-4e8aa34ee856",
                    "LayerId": "3d330474-9e5a-466c-98cd-f8b6d21421b9"
                }
            ]
        },
        {
            "id": "63ef901c-671f-40f4-b2d7-f6d50a053a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
            "compositeImage": {
                "id": "ce0c2545-d000-4ab5-b842-2d2242d4f112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ef901c-671f-40f4-b2d7-f6d50a053a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e669e017-e936-4834-8411-ea48a9171092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ef901c-671f-40f4-b2d7-f6d50a053a86",
                    "LayerId": "3d330474-9e5a-466c-98cd-f8b6d21421b9"
                }
            ]
        },
        {
            "id": "93a9bdb6-920d-4403-9ecc-cdadb0aeaa64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
            "compositeImage": {
                "id": "0a20b228-1fb7-4499-a6b6-af39a90ac220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a9bdb6-920d-4403-9ecc-cdadb0aeaa64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "196f6053-6bb0-4b4c-aa3a-402bb186df9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a9bdb6-920d-4403-9ecc-cdadb0aeaa64",
                    "LayerId": "3d330474-9e5a-466c-98cd-f8b6d21421b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "3d330474-9e5a-466c-98cd-f8b6d21421b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7225712e-cbe5-48ee-9c8d-c2a2ad2e113c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}