{
    "id": "2566e09c-2787-478d-875f-6a7c23b48ed3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boomerang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84a3a604-b4cd-4cf2-988d-c823a2258354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "c06bd67d-0d1e-489c-92ab-f3655af5edd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a3a604-b4cd-4cf2-988d-c823a2258354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b17998c6-658a-48c9-961b-7aa9f18d0672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a3a604-b4cd-4cf2-988d-c823a2258354",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "0c8dd2f6-e34d-4588-bc35-4142bf0653c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "ab904812-469d-462f-a8d3-8adca815730e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8dd2f6-e34d-4588-bc35-4142bf0653c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead3faae-e930-4ac3-80a0-1acbd91d4e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8dd2f6-e34d-4588-bc35-4142bf0653c5",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "86207cf3-8e9d-4a18-a9eb-13a29e1d0446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "bf9bec34-6f3c-43b2-b8ec-bdb5d7138189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86207cf3-8e9d-4a18-a9eb-13a29e1d0446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd114db7-fbb9-4dd2-9b50-f8dee7b20cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86207cf3-8e9d-4a18-a9eb-13a29e1d0446",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "3a73025e-5748-4b0e-94b4-2f4a95c2c3fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "050db2fc-1ba3-4cd9-b741-3bee26576dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a73025e-5748-4b0e-94b4-2f4a95c2c3fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdeff65-97c3-4216-b589-c30d06954b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a73025e-5748-4b0e-94b4-2f4a95c2c3fe",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "02c720f8-f65f-448a-92c7-4c07d230bfed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "c784f778-a986-4039-b4c5-372c9e0508cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c720f8-f65f-448a-92c7-4c07d230bfed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290d345a-0095-464c-aabf-18845d6beaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c720f8-f65f-448a-92c7-4c07d230bfed",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "422f5b10-9d09-46d9-b2b3-368f1f0bde0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "74dfebe0-b6e5-4e63-bdad-a20269e84113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "422f5b10-9d09-46d9-b2b3-368f1f0bde0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd02699b-1710-45fb-b78c-8b351c502dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "422f5b10-9d09-46d9-b2b3-368f1f0bde0e",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "c4e0bec9-d752-4997-a6cc-92b15a9c756f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "8459bcc3-bb67-4711-871d-4ab192ec751d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e0bec9-d752-4997-a6cc-92b15a9c756f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "300cf9f8-bc77-4f65-ba72-c1ef45ec6659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e0bec9-d752-4997-a6cc-92b15a9c756f",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "5f4c2be4-fb9a-4bc5-b866-50c2e67d3b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "e950560b-6d34-4e77-ab49-95661c53a5f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f4c2be4-fb9a-4bc5-b866-50c2e67d3b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf7e3f7-1ac8-4865-85a0-bee2d7401ef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f4c2be4-fb9a-4bc5-b866-50c2e67d3b31",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "fbe0be08-d8df-4eaa-a7a2-239ff2421b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "b0c4cebe-87d6-4364-ac12-de3f6cd48216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe0be08-d8df-4eaa-a7a2-239ff2421b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a800147-afa7-4b76-b2e1-1da24bef0bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe0be08-d8df-4eaa-a7a2-239ff2421b80",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "df55f8a2-1a26-48b4-b3c9-1480093e72d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "ebae6df7-f67f-4357-b6be-47f585aabeff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df55f8a2-1a26-48b4-b3c9-1480093e72d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c390acf-63b4-4e65-bec5-d21203165747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df55f8a2-1a26-48b4-b3c9-1480093e72d1",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "93f6ec95-2a9f-493a-b54e-047462d6ce57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "1e483872-60b4-4514-874e-1cbebabc1654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93f6ec95-2a9f-493a-b54e-047462d6ce57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfe5473-ed2a-4b59-a42f-137a0a67bb0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93f6ec95-2a9f-493a-b54e-047462d6ce57",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "67e3e797-a5e7-4e2a-b57d-2a75dae7adb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "416e4249-f417-4587-990b-5fbe8afe3f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e3e797-a5e7-4e2a-b57d-2a75dae7adb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b1b1ad1-e1d4-4f8a-8483-f4e8f37d509c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e3e797-a5e7-4e2a-b57d-2a75dae7adb6",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "2336bce3-f592-49bc-857a-2760a0370903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "7346d145-5b40-443e-be5b-11b0b909beb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2336bce3-f592-49bc-857a-2760a0370903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a42b612-9e47-49b3-904f-1033df80f05b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2336bce3-f592-49bc-857a-2760a0370903",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "47f6d2a8-5bf2-4e97-ace9-dada8d185394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "18d9737c-a9f1-4fd4-9be2-8159f4b37a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f6d2a8-5bf2-4e97-ace9-dada8d185394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4155acd0-087b-4cd0-b621-3eaa18c5776a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f6d2a8-5bf2-4e97-ace9-dada8d185394",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "69dbc4d5-871b-4187-983b-9b0af319b3ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "4e930685-fe59-4bf4-8c16-c8ce49ee2bc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69dbc4d5-871b-4187-983b-9b0af319b3ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadb3965-3b25-48bc-ac33-b60cf1d80246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69dbc4d5-871b-4187-983b-9b0af319b3ec",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        },
        {
            "id": "27f36d99-95ee-4892-b6cc-55ae2c72036d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "compositeImage": {
                "id": "b4c10cd0-3a8d-469c-86c6-cc99e24d2e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27f36d99-95ee-4892-b6cc-55ae2c72036d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ad558e4-60bf-43f6-9b6d-cbebe733653f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27f36d99-95ee-4892-b6cc-55ae2c72036d",
                    "LayerId": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "8b3d7e27-c089-4a69-8e8a-17e7f6f4fa40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2566e09c-2787-478d-875f-6a7c23b48ed3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}