{
    "id": "9e50b78e-3dc6-490e-9559-6093f61abad0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healthinc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 128,
    "bbox_left": 0,
    "bbox_right": 128,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "742f26c4-6685-4666-a2ac-77f165d0b2c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e50b78e-3dc6-490e-9559-6093f61abad0",
            "compositeImage": {
                "id": "cc25d566-6656-4c2e-85c3-573ce6a8a637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742f26c4-6685-4666-a2ac-77f165d0b2c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ca934b-ff1f-4c47-9564-27975bcdc7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742f26c4-6685-4666-a2ac-77f165d0b2c5",
                    "LayerId": "675391bb-d7e8-49d0-a00f-d4f1c81e250d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 129,
    "layers": [
        {
            "id": "675391bb-d7e8-49d0-a00f-d4f1c81e250d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e50b78e-3dc6-490e-9559-6093f61abad0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 129,
    "xorig": 0,
    "yorig": 128
}