{
    "id": "21ab2fc0-643e-423b-8b38-97002acef969",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b0011b6-8db2-434d-908c-d2746020b7f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "compositeImage": {
                "id": "0dcbfb85-69be-470c-b206-d86868a8eda3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0011b6-8db2-434d-908c-d2746020b7f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c3dc92a-af7a-4a18-b5f1-ba8c0919d408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0011b6-8db2-434d-908c-d2746020b7f7",
                    "LayerId": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8"
                }
            ]
        },
        {
            "id": "c77c8e87-b955-4f4d-8397-b3fe23e6a266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "compositeImage": {
                "id": "0999913f-f3aa-4eef-bf55-f146a6bf530d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77c8e87-b955-4f4d-8397-b3fe23e6a266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55faea00-97c8-4d33-88ad-a552a0f9f710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77c8e87-b955-4f4d-8397-b3fe23e6a266",
                    "LayerId": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8"
                }
            ]
        },
        {
            "id": "c1de3c73-257c-4e1a-a676-18057e811e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "compositeImage": {
                "id": "2a129fb0-18b2-4aa2-bc18-e44f124bbc1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1de3c73-257c-4e1a-a676-18057e811e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6c8d51-3fc1-4fea-9c7d-93e9251f435e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1de3c73-257c-4e1a-a676-18057e811e53",
                    "LayerId": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8"
                }
            ]
        },
        {
            "id": "3aa6a253-9cad-489a-96c2-615b6c11b6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "compositeImage": {
                "id": "1f7c54f2-4e5b-4d3b-ac1a-3687cb191ff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aa6a253-9cad-489a-96c2-615b6c11b6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc853db6-f512-4dac-afe5-27930a9a4d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aa6a253-9cad-489a-96c2-615b6c11b6a6",
                    "LayerId": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8"
                }
            ]
        },
        {
            "id": "3abcb97f-11d4-499d-a6c9-445e25910fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "compositeImage": {
                "id": "3e427108-dec6-47c6-915c-40a7f02ca8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abcb97f-11d4-499d-a6c9-445e25910fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1861b1c1-4a6f-4f70-b21f-fda2850669f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abcb97f-11d4-499d-a6c9-445e25910fcc",
                    "LayerId": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "2238cc1d-bbfa-4911-b58c-df4a339c4fd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21ab2fc0-643e-423b-8b38-97002acef969",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 2
}