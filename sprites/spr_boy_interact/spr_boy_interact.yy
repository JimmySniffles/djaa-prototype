{
    "id": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_interact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5fa2d53-68ec-472b-84d9-9e7dafe81b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "5d5f9312-c9d5-4d02-b15b-5b42e677c327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5fa2d53-68ec-472b-84d9-9e7dafe81b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25c4e91-01fe-4712-af3b-adddeb075508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5fa2d53-68ec-472b-84d9-9e7dafe81b2c",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "f334c27a-1d7e-41a7-b102-7046e6b3846a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "9cd2f84e-26a0-4748-a87a-064da9b8b329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f334c27a-1d7e-41a7-b102-7046e6b3846a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3b5f8d-a252-40e3-b200-e60986736ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f334c27a-1d7e-41a7-b102-7046e6b3846a",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "47f3b9ba-0a1e-43e2-9158-fc5c219b1fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "78d000f4-251e-4660-9c7c-1b11ebb1573e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f3b9ba-0a1e-43e2-9158-fc5c219b1fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e436e0-02a4-402d-be10-0cd6063863ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f3b9ba-0a1e-43e2-9158-fc5c219b1fe5",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "d73bc733-c08d-406f-8b3b-ed48d2deb489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "d9f16c2a-1154-41e9-b2ee-61b1c769db87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73bc733-c08d-406f-8b3b-ed48d2deb489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40877cc7-1a75-45db-9bfb-e6a2101b8e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73bc733-c08d-406f-8b3b-ed48d2deb489",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "75608834-86b3-46f3-ab3d-bc871fc40c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "e2edcbd9-7afe-4aa6-bcda-d76b9dd1bc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75608834-86b3-46f3-ab3d-bc871fc40c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e9e3f6-7051-4a01-8144-120fa776b545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75608834-86b3-46f3-ab3d-bc871fc40c4c",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "36c21100-1a86-4331-bcba-5b13b2f7fe71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "5bc02d89-d085-4d0a-a2ac-22d963781b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c21100-1a86-4331-bcba-5b13b2f7fe71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f855003-486f-4087-82dd-b9f441e31f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c21100-1a86-4331-bcba-5b13b2f7fe71",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "f75ca0ce-1279-408d-bb30-49a41326f324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "46218980-2c9f-460d-885f-9009ba17f525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f75ca0ce-1279-408d-bb30-49a41326f324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11aee774-1721-4b98-b9d1-b401e306d2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f75ca0ce-1279-408d-bb30-49a41326f324",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "c1abf96e-2dec-4173-b8e6-afc286976ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "c21d0083-f7f1-4d93-b8ca-751a5f0ff2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1abf96e-2dec-4173-b8e6-afc286976ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c94748a-b0e5-468c-9379-80954593ca43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1abf96e-2dec-4173-b8e6-afc286976ab5",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "cc4570fb-5152-4f01-b27c-0b9155097ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "86f5168b-bcce-45e3-80be-b9a10cd36dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4570fb-5152-4f01-b27c-0b9155097ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f1d742f-9178-4f5d-b732-c8a225301b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4570fb-5152-4f01-b27c-0b9155097ee7",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "42f4f310-e760-46de-abc9-75d6519244ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "7d37fa57-1ce6-44db-be85-f618f2d84e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f4f310-e760-46de-abc9-75d6519244ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e6bd55a-6af2-4827-b94c-a6e663f89539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f4f310-e760-46de-abc9-75d6519244ca",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "b00dc227-e35f-43c8-b7d9-423c5b519164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "35338453-dd32-4feb-b187-aa82c8ac9efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00dc227-e35f-43c8-b7d9-423c5b519164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcdf7365-2e6f-4d41-8bc6-df914e6bd3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00dc227-e35f-43c8-b7d9-423c5b519164",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "f514bcb3-2367-4786-8b60-12c29ffd088e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "5b24ab99-2294-44b0-a126-e21a62386bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f514bcb3-2367-4786-8b60-12c29ffd088e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e9000c-06ef-42f1-b327-86f691abbc40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f514bcb3-2367-4786-8b60-12c29ffd088e",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "38f4cc91-88b6-4411-80ab-3ff2a6b2b962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "1572fd35-9328-4ad0-8f49-e100b3c6fd41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f4cc91-88b6-4411-80ab-3ff2a6b2b962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2bd7608-0fc3-4ddb-a311-f54e5114bc09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f4cc91-88b6-4411-80ab-3ff2a6b2b962",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "163e97a7-033c-4c11-a75e-191fb3abe385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "f2c7d334-f5d8-417c-8ff8-6dbec25145ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163e97a7-033c-4c11-a75e-191fb3abe385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f646560-79a1-42d7-87dc-d54bb4d7e16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163e97a7-033c-4c11-a75e-191fb3abe385",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        },
        {
            "id": "4cbbd6aa-c692-4cd4-b651-0e7c54d5c471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "compositeImage": {
                "id": "c5ecf8c2-ad03-4e98-9c5d-6c6206d338ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cbbd6aa-c692-4cd4-b651-0e7c54d5c471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fb1b0eb-cb0f-4ae9-8842-0563ce66bf32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cbbd6aa-c692-4cd4-b651-0e7c54d5c471",
                    "LayerId": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1bfdaaef-982d-4ba5-bfb7-b47f5a423dd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bde3fa6a-f108-42bd-9ef6-fcb06791e778",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}