{
    "id": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_killboomerang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16d636ec-9e27-4c93-b415-308f3c4b1575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "5cad57e9-1878-4fa8-b80e-81798fe878b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d636ec-9e27-4c93-b415-308f3c4b1575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad10f96-0700-4e18-b08a-a4e981ccf29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d636ec-9e27-4c93-b415-308f3c4b1575",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "12e7b137-1ea8-40e6-bced-cc894df6218b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "e19854c1-760c-4934-abe7-868dfa9fb2d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e7b137-1ea8-40e6-bced-cc894df6218b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43aa850b-002b-4196-aca7-e6b67f11ebf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e7b137-1ea8-40e6-bced-cc894df6218b",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "ea684787-a73c-4cd9-a6ff-3eb0b49b4a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "98df44c2-b59e-4969-8eaf-bd96910166c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea684787-a73c-4cd9-a6ff-3eb0b49b4a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "975dbfb0-2f07-4c50-bc17-1169800168f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea684787-a73c-4cd9-a6ff-3eb0b49b4a5a",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "2063c9dc-1292-4e7f-b142-b0b33039153c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "6add14b2-965e-471d-9c8e-f5cfae2b4690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2063c9dc-1292-4e7f-b142-b0b33039153c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d1259b-6661-4e51-8b3a-927c541810b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2063c9dc-1292-4e7f-b142-b0b33039153c",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "3de11189-70b6-43ce-86f2-cc526f425fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "d091cc73-90d6-45d9-9f9a-307c5882aaa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de11189-70b6-43ce-86f2-cc526f425fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2bb369-39c4-4c40-b419-87047c17b225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de11189-70b6-43ce-86f2-cc526f425fcd",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "1fb3ef4f-7c0d-405a-a230-999ea22db461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "9efee206-dc40-4817-bae2-961d9b3cd2c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb3ef4f-7c0d-405a-a230-999ea22db461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9df85297-d760-4803-b238-b3af61153466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb3ef4f-7c0d-405a-a230-999ea22db461",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "f60d9f28-ff08-4bfa-a33d-da0b6bf2a22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "cbb89c8c-7627-4a46-b991-351e90d53377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60d9f28-ff08-4bfa-a33d-da0b6bf2a22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ddf5c-5653-4c89-bcd7-180ae2b5cc66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60d9f28-ff08-4bfa-a33d-da0b6bf2a22f",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "f8b7f22f-acfb-45f3-b795-cfef0b4d1eb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "122a07ee-f62a-456f-92a0-cb3a4c7d4553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8b7f22f-acfb-45f3-b795-cfef0b4d1eb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61ee714-14b5-46b0-a3ba-a8527808a5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8b7f22f-acfb-45f3-b795-cfef0b4d1eb4",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "6a7a62f4-56d0-495c-a084-46efefd516cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "854962fc-84a6-4c9f-b60e-d5b6498b48a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7a62f4-56d0-495c-a084-46efefd516cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b372d1-a473-4b2a-a7bf-13269c08fffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7a62f4-56d0-495c-a084-46efefd516cc",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "f356452d-1e64-46a3-a396-19a7e925a120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "2e28166f-e790-49c9-8946-262ef726d950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f356452d-1e64-46a3-a396-19a7e925a120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc97e34f-b148-47d1-ba38-16e5aa288278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f356452d-1e64-46a3-a396-19a7e925a120",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "3ea45802-ff68-4359-bcbb-67f11dd6995e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "76287d3c-a185-4d7d-8fe4-b3ff9b5d620d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea45802-ff68-4359-bcbb-67f11dd6995e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b89ebcf-889c-40a7-89be-f8ab0e2fe125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea45802-ff68-4359-bcbb-67f11dd6995e",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "98a2b39d-9612-4e43-8bae-8d754cc3c0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "9d493d95-815e-4a35-824f-450b327117f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98a2b39d-9612-4e43-8bae-8d754cc3c0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16902855-9027-47d8-a680-1135b7d87cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98a2b39d-9612-4e43-8bae-8d754cc3c0b6",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "bc913d75-de32-4730-b5e4-141f07a4aa3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "316d07f5-ea19-4158-9968-b48888a3a487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc913d75-de32-4730-b5e4-141f07a4aa3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1345c3-7de9-4986-bcfd-7aa3b0a80787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc913d75-de32-4730-b5e4-141f07a4aa3d",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "29693f09-99d9-4d3d-b077-cc88528e413b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "fb95800d-bbb6-4331-8a85-15302f67a36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29693f09-99d9-4d3d-b077-cc88528e413b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03750697-930a-432f-a599-c4b4dfaa0a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29693f09-99d9-4d3d-b077-cc88528e413b",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "781d8fe6-12ea-4643-a119-c6a6ff7a3194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "dcae86ac-5ce0-402a-a447-c44bd5940b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "781d8fe6-12ea-4643-a119-c6a6ff7a3194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08b547f-4303-4192-9bca-c1469cc8ff26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "781d8fe6-12ea-4643-a119-c6a6ff7a3194",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        },
        {
            "id": "73064bc0-5f5f-4606-a6ee-fe0e3a310c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "compositeImage": {
                "id": "98280cd1-f5e2-43d3-a817-3a68a83f08f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73064bc0-5f5f-4606-a6ee-fe0e3a310c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c453340-e2b5-41b8-b747-8c5bbd339590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73064bc0-5f5f-4606-a6ee-fe0e3a310c14",
                    "LayerId": "04539ca6-afcf-4c3f-8019-0a1f15c01c69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "04539ca6-afcf-4c3f-8019-0a1f15c01c69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9eeec83b-31f0-4e95-9c01-d7ea36726b0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}