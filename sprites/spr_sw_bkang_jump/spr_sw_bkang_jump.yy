{
    "id": "51e58362-deef-459f-a931-a434deff9283",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkang_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4fcffb5-6d2f-4238-87c2-5dedf1c64a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51e58362-deef-459f-a931-a434deff9283",
            "compositeImage": {
                "id": "17b4e4cf-510d-4a75-95c8-e1c61e387499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4fcffb5-6d2f-4238-87c2-5dedf1c64a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2999296c-0524-4208-aa0f-4f570111b513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4fcffb5-6d2f-4238-87c2-5dedf1c64a39",
                    "LayerId": "5d0703ae-4563-4a4c-b19c-4676f425b179"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "5d0703ae-4563-4a4c-b19c-4676f425b179",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51e58362-deef-459f-a931-a434deff9283",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}