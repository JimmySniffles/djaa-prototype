{
    "id": "b75b8f7e-73d3-4fec-91dd-17bd5dc1bb1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nw_env",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 16,
    "bbox_right": 637,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa523e20-6db1-45d0-aa2b-ba387d97d1ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b75b8f7e-73d3-4fec-91dd-17bd5dc1bb1f",
            "compositeImage": {
                "id": "9db6dd9b-36ca-4546-8b6a-64a039ad86b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa523e20-6db1-45d0-aa2b-ba387d97d1ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bcb3b25-0116-40b3-b316-85554430e6ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa523e20-6db1-45d0-aa2b-ba387d97d1ee",
                    "LayerId": "81b39be2-3889-4f63-9f9a-b6c2da88c601"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "81b39be2-3889-4f63-9f9a-b6c2da88c601",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b75b8f7e-73d3-4fec-91dd-17bd5dc1bb1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}