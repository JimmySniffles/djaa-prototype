{
    "id": "a53758c1-5783-4cc9-8aa0-806b7eedf16c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkang_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8662ebed-df0e-42b4-852d-14f77d141996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a53758c1-5783-4cc9-8aa0-806b7eedf16c",
            "compositeImage": {
                "id": "7088c273-aa85-4324-85a0-73e6076ca1fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8662ebed-df0e-42b4-852d-14f77d141996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158c657c-d3aa-4ea4-99b7-00e658b1b620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8662ebed-df0e-42b4-852d-14f77d141996",
                    "LayerId": "20b3f260-ff99-4c36-82c5-830620651c55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "20b3f260-ff99-4c36-82c5-830620651c55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a53758c1-5783-4cc9-8aa0-806b7eedf16c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}