{
    "id": "e8c786f0-ae20-448f-a4ec-0d6e0ec519b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b74e86a-f239-416e-9c33-a34b3bb9ef14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c786f0-ae20-448f-a4ec-0d6e0ec519b3",
            "compositeImage": {
                "id": "e8067b2b-80ae-4667-8618-ddd335e94b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b74e86a-f239-416e-9c33-a34b3bb9ef14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed594a73-d197-41a1-bc1b-0da1bfe19f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b74e86a-f239-416e-9c33-a34b3bb9ef14",
                    "LayerId": "c388282c-427a-4c91-ad37-c3adc85f0002"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c388282c-427a-4c91-ad37-c3adc85f0002",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8c786f0-ae20-448f-a4ec-0d6e0ec519b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}