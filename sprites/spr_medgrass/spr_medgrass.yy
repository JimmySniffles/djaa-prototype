{
    "id": "a541f5fc-c998-4278-b63b-56abc43f63cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_medgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 1022,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "759e3acb-46fe-4ff9-9a0c-ca769e0a6330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a541f5fc-c998-4278-b63b-56abc43f63cf",
            "compositeImage": {
                "id": "2912391c-73fc-4c96-9084-cadd6c9b737c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "759e3acb-46fe-4ff9-9a0c-ca769e0a6330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e110567b-0873-4db5-996e-184bfd74b7d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "759e3acb-46fe-4ff9-9a0c-ca769e0a6330",
                    "LayerId": "c43c05bb-e278-4fe8-a12b-275eeff8658e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c43c05bb-e278-4fe8-a12b-275eeff8658e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a541f5fc-c998-4278-b63b-56abc43f63cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}