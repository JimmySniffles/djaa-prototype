{
    "id": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_sa_a_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4575089f-6cb0-4793-8edc-7542894aed9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "cf5bc94f-c4b3-425a-8448-67212a371797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4575089f-6cb0-4793-8edc-7542894aed9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e657bc-57d3-4e40-9664-e07215ce8e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4575089f-6cb0-4793-8edc-7542894aed9a",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "62328d93-25e9-4e4e-bb2a-380467e7e8e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4575089f-6cb0-4793-8edc-7542894aed9a",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        },
        {
            "id": "8de1be7e-4d4a-481a-846f-e140f4450295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "662494d8-58cd-4f5c-8ddc-81dde53b5161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de1be7e-4d4a-481a-846f-e140f4450295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f827b7b-3baa-4a53-8633-024434ccbdec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de1be7e-4d4a-481a-846f-e140f4450295",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "9271f727-9333-420f-bd98-3789e3dd9e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de1be7e-4d4a-481a-846f-e140f4450295",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        },
        {
            "id": "20b507d1-ee50-4eb6-9c7b-ac47098f6667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "d8adfafe-f314-4159-8144-2d7c00a33910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b507d1-ee50-4eb6-9c7b-ac47098f6667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e32ffad-656d-4556-9112-cfe9ec4ac6c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b507d1-ee50-4eb6-9c7b-ac47098f6667",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "d36a9330-5910-415b-b3f8-a9d963d9ec82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b507d1-ee50-4eb6-9c7b-ac47098f6667",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        },
        {
            "id": "895b4c78-1734-45ab-b2f0-c952d3a3ed11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "ea14b14e-7850-4608-a9d3-23e1491cd1f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895b4c78-1734-45ab-b2f0-c952d3a3ed11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92e5009f-6327-442e-9610-3a9c0bfe5dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895b4c78-1734-45ab-b2f0-c952d3a3ed11",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "db66ced3-22fd-4160-87b1-dd03d4a42a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895b4c78-1734-45ab-b2f0-c952d3a3ed11",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        },
        {
            "id": "f8faa95e-253a-4722-b980-925ed12156ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "bfee8b26-bb9c-44a6-aefd-5286d0d586ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8faa95e-253a-4722-b980-925ed12156ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a86450-1503-4d7e-a300-9b70da314c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8faa95e-253a-4722-b980-925ed12156ba",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "723420fa-e8d1-4b54-9237-ea0e1fa47440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8faa95e-253a-4722-b980-925ed12156ba",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        },
        {
            "id": "8c9ae920-8ddf-46c6-a96e-0a72e0d17990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "compositeImage": {
                "id": "4395080a-d481-41ac-b757-b4af2cb008b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c9ae920-8ddf-46c6-a96e-0a72e0d17990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0513d22a-809b-4fbc-a3f4-28d4a4332e75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9ae920-8ddf-46c6-a96e-0a72e0d17990",
                    "LayerId": "39047364-6aae-41d5-8278-6a5e33d9082c"
                },
                {
                    "id": "c4c47b3b-118b-4fbc-a05a-d99d864ab53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9ae920-8ddf-46c6-a96e-0a72e0d17990",
                    "LayerId": "fe61bbfd-9ad3-4b76-b151-68d865989740"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "39047364-6aae-41d5-8278-6a5e33d9082c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fe61bbfd-9ad3-4b76-b151-68d865989740",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aba3d0b-aa0a-49aa-8229-d7d3dc799dbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}