{
    "id": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_sa_g_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82002b45-b381-4d33-9204-71c1013b0433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "f89b9122-193f-497e-8e0e-29212ba88512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82002b45-b381-4d33-9204-71c1013b0433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1902e8f7-9c06-43f3-a1b1-ac0c0e4f8279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82002b45-b381-4d33-9204-71c1013b0433",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "05ab8e4d-cc61-4dde-8781-c543900952f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82002b45-b381-4d33-9204-71c1013b0433",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        },
        {
            "id": "338b03d8-3931-4835-838e-54fca625ed71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "7fecbdf2-61f8-4052-ac63-0988c63e2c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338b03d8-3931-4835-838e-54fca625ed71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bece2d76-0e63-49dd-9fbc-bcd86ea07028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338b03d8-3931-4835-838e-54fca625ed71",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "7b76b87e-933a-4a1d-968f-c96ccf5bdf9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338b03d8-3931-4835-838e-54fca625ed71",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        },
        {
            "id": "f0c68fc3-5dad-4a52-b038-52648a0e8b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "e613c6d6-f280-4c30-9060-fafeddd23aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c68fc3-5dad-4a52-b038-52648a0e8b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c6a81b-bc4f-4500-94da-e35672072b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c68fc3-5dad-4a52-b038-52648a0e8b62",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "b2bcc251-7584-464c-a06f-fc58efac9059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c68fc3-5dad-4a52-b038-52648a0e8b62",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        },
        {
            "id": "560da6bc-4498-4701-a793-056982b81026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "c7a6cd45-a913-42ae-9574-db7175063509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560da6bc-4498-4701-a793-056982b81026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c63915-2d5f-447f-890d-04f8981bb057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560da6bc-4498-4701-a793-056982b81026",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "ff4bc9c2-c041-49f2-90b7-0c52a572a4bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560da6bc-4498-4701-a793-056982b81026",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        },
        {
            "id": "cc5cc028-fd79-4d5f-a794-4f4428d6ecd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "c486a141-8462-41ed-8b56-9483b424ad71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc5cc028-fd79-4d5f-a794-4f4428d6ecd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce1b658-4549-4e00-8de8-d21630dc00d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc5cc028-fd79-4d5f-a794-4f4428d6ecd3",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "715c4777-6d89-460b-a565-fe988d3c6c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc5cc028-fd79-4d5f-a794-4f4428d6ecd3",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        },
        {
            "id": "648f61f6-b7f7-4d29-9315-1974c96fb8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "compositeImage": {
                "id": "cec60852-6df5-440c-bc88-efe45e0ebb41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648f61f6-b7f7-4d29-9315-1974c96fb8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41030b99-a18f-4dee-bec5-5e49ace8cf72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648f61f6-b7f7-4d29-9315-1974c96fb8eb",
                    "LayerId": "4ca99d9d-4b7b-430c-8a66-24e9625d0478"
                },
                {
                    "id": "9764eb3f-6d0f-46eb-9b11-ae2baefae5b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648f61f6-b7f7-4d29-9315-1974c96fb8eb",
                    "LayerId": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4ca99d9d-4b7b-430c-8a66-24e9625d0478",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7abfc3d0-4f98-4f9a-a4b1-9598befee7e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cf47022-dfd4-42de-9c22-4ae7ea773397",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}