{
    "id": "51aa6acb-6eaf-421b-9eed-09c66a81d39e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nw_climbable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 78,
    "bbox_right": 112,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d943997-15e3-4fa3-b65e-25dd91703808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51aa6acb-6eaf-421b-9eed-09c66a81d39e",
            "compositeImage": {
                "id": "96598059-0f12-4811-ab5c-f28452e1f727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d943997-15e3-4fa3-b65e-25dd91703808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2731e7af-e2e9-4038-8def-9bb962cf3d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d943997-15e3-4fa3-b65e-25dd91703808",
                    "LayerId": "33b90121-7cdd-4bd7-bac6-c743ece70b39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "33b90121-7cdd-4bd7-bac6-c743ece70b39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51aa6acb-6eaf-421b-9eed-09c66a81d39e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}