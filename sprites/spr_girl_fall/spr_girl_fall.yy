{
    "id": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "536fa16a-d869-4342-b5c3-f0d962f0a476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
            "compositeImage": {
                "id": "88125889-7d5f-43f2-a7e5-3424adf783a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536fa16a-d869-4342-b5c3-f0d962f0a476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c005189-966c-43e4-9be8-fcfaa84e0681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536fa16a-d869-4342-b5c3-f0d962f0a476",
                    "LayerId": "95fd45d1-919a-4acd-8f55-e64a9f27cb1a"
                }
            ]
        },
        {
            "id": "d78c3655-42fb-4ec5-928a-5c66ac7e88dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
            "compositeImage": {
                "id": "90ca8337-17b0-4840-bed9-0f7adff63e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d78c3655-42fb-4ec5-928a-5c66ac7e88dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3dce68-56a1-4c62-bd6e-d843236f2576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d78c3655-42fb-4ec5-928a-5c66ac7e88dc",
                    "LayerId": "95fd45d1-919a-4acd-8f55-e64a9f27cb1a"
                }
            ]
        },
        {
            "id": "4e8ce485-ea6f-446f-8223-9f342a9afe02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
            "compositeImage": {
                "id": "6510a868-75d0-4f2e-8039-53766349121c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e8ce485-ea6f-446f-8223-9f342a9afe02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea48844-a9fe-4e5d-807b-c43bc5719c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e8ce485-ea6f-446f-8223-9f342a9afe02",
                    "LayerId": "95fd45d1-919a-4acd-8f55-e64a9f27cb1a"
                }
            ]
        },
        {
            "id": "5e2dd569-ecea-4a68-90c7-2a03888c1ab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
            "compositeImage": {
                "id": "be3651f2-a476-4d7b-8438-0332a7ee8d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2dd569-ecea-4a68-90c7-2a03888c1ab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc264a1-9bde-47b4-93a7-7d7363ef6c57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2dd569-ecea-4a68-90c7-2a03888c1ab4",
                    "LayerId": "95fd45d1-919a-4acd-8f55-e64a9f27cb1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "95fd45d1-919a-4acd-8f55-e64a9f27cb1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22fe6319-b987-4a5c-aa12-411cc9f1639e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}