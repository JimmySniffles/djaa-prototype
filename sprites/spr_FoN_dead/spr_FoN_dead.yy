{
    "id": "f3f86268-e633-47d8-9e50-4318b99248df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af7e07e3-c69a-4b31-b47d-599ba49597a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "cdf7c362-1c7b-4ca8-a270-d9a52d04bb04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af7e07e3-c69a-4b31-b47d-599ba49597a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8470b61-6eb2-47cb-a74f-2877a2ce7794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af7e07e3-c69a-4b31-b47d-599ba49597a9",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "11dde526-2144-45f4-8f16-6358d090c203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "dabc151b-64e1-4d08-855b-9412093b11ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11dde526-2144-45f4-8f16-6358d090c203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "113c8255-348b-4310-8bbd-4dbf8837130d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11dde526-2144-45f4-8f16-6358d090c203",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "0e979613-3a47-4c21-a39a-d31a31d43422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "87279a57-d785-4752-a099-276beff8a62f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e979613-3a47-4c21-a39a-d31a31d43422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "504978b8-78d5-4bfe-9f35-e9ef9b3177f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e979613-3a47-4c21-a39a-d31a31d43422",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "5b859905-3174-4be1-8929-e1f2c7a138bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "6c26938a-5a89-451f-96cf-97fc52c8f4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b859905-3174-4be1-8929-e1f2c7a138bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d522b417-3f3b-466a-a972-79ad6c05b9cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b859905-3174-4be1-8929-e1f2c7a138bc",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "eb334423-334b-49f5-8fe2-7cff434c03d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "496ee290-be3f-422b-bf9f-314800d2b36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb334423-334b-49f5-8fe2-7cff434c03d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67023c4-a297-492d-a2f4-0facd9866909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb334423-334b-49f5-8fe2-7cff434c03d6",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "2cca1581-4057-4b84-b1f5-2fc557a91b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "60831e84-1244-4551-a012-b33fd89c9667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cca1581-4057-4b84-b1f5-2fc557a91b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a072629-5b7e-4459-a5a6-e7a5cba515ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cca1581-4057-4b84-b1f5-2fc557a91b23",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "8a8f086a-d885-4942-9792-4b9fb3cf68dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "3c90bed8-a323-4287-b68c-0109c3dccb07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8f086a-d885-4942-9792-4b9fb3cf68dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d8ae34-a889-4c37-8531-c7c0152deaed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8f086a-d885-4942-9792-4b9fb3cf68dd",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        },
        {
            "id": "22aa957a-c02e-44e2-bd05-1fae96b15632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "compositeImage": {
                "id": "54fe771d-2c11-4337-8b09-ec3b6874e919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22aa957a-c02e-44e2-bd05-1fae96b15632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd194ad-648a-4c3b-8ea5-1f03687c48e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22aa957a-c02e-44e2-bd05-1fae96b15632",
                    "LayerId": "0aa883fd-af1c-45ac-9e7c-db21a6129977"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0aa883fd-af1c-45ac-9e7c-db21a6129977",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f86268-e633-47d8-9e50-4318b99248df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}