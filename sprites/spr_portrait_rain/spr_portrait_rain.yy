{
    "id": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_rain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 11,
    "bbox_right": 108,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9437e04b-b0f6-45b5-a581-b9af80bbd0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
            "compositeImage": {
                "id": "dee70614-45ac-44c4-b77d-01b19efddb31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9437e04b-b0f6-45b5-a581-b9af80bbd0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf4e7597-0931-4abe-bf57-572ffddd8d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9437e04b-b0f6-45b5-a581-b9af80bbd0d2",
                    "LayerId": "aeecf1b0-c815-41ae-8364-f0103ba24665"
                }
            ]
        },
        {
            "id": "1f0d046a-769f-48e7-8676-011e3184e949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
            "compositeImage": {
                "id": "49e1297c-a6e9-4cc5-83c1-6a3b4fd945d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f0d046a-769f-48e7-8676-011e3184e949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bbd34f7-469c-4e36-a19e-7abbb6f797d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f0d046a-769f-48e7-8676-011e3184e949",
                    "LayerId": "aeecf1b0-c815-41ae-8364-f0103ba24665"
                }
            ]
        },
        {
            "id": "f666da29-fc3b-4d22-9ce1-2e895c645ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
            "compositeImage": {
                "id": "a6f4414d-819f-4ebd-b901-d9e746d2664f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f666da29-fc3b-4d22-9ce1-2e895c645ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269a9809-15f3-4e6d-ba05-c5e48845a7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f666da29-fc3b-4d22-9ce1-2e895c645ae0",
                    "LayerId": "aeecf1b0-c815-41ae-8364-f0103ba24665"
                }
            ]
        },
        {
            "id": "a86ee881-07c4-4658-b26a-40dd32c2d848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
            "compositeImage": {
                "id": "73378215-8fff-46cf-8c80-e1696f406bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86ee881-07c4-4658-b26a-40dd32c2d848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1c242c-c156-4e4a-9aa3-383cb2d92af9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86ee881-07c4-4658-b26a-40dd32c2d848",
                    "LayerId": "aeecf1b0-c815-41ae-8364-f0103ba24665"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "aeecf1b0-c815-41ae-8364-f0103ba24665",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84bccf29-cb40-4fd0-8ffc-124e2ace836d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}