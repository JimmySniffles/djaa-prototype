{
    "id": "a3918c75-b161-4b95-b78a-3158eb83ce22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightsky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 524,
    "bbox_left": 0,
    "bbox_right": 1499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06ec9a87-325a-4d0d-a2fe-6956df0eebfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3918c75-b161-4b95-b78a-3158eb83ce22",
            "compositeImage": {
                "id": "15bb1fa4-6ae4-4a58-9980-d39a124e6fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ec9a87-325a-4d0d-a2fe-6956df0eebfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047feb0b-6e2d-4701-93b9-0f9ace917971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ec9a87-325a-4d0d-a2fe-6956df0eebfd",
                    "LayerId": "cbe774e6-3748-40d4-823c-ce1ff4c4b51a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 525,
    "layers": [
        {
            "id": "cbe774e6-3748-40d4-823c-ce1ff4c4b51a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3918c75-b161-4b95-b78a-3158eb83ce22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1500,
    "xorig": 0,
    "yorig": 0
}