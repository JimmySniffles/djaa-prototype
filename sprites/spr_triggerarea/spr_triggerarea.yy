{
    "id": "2db4e53c-0189-4288-ade7-b518f9e11f9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_triggerarea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a072d137-36a1-4a6e-b4ca-d9731eead77f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2db4e53c-0189-4288-ade7-b518f9e11f9a",
            "compositeImage": {
                "id": "9043ba75-2d2b-4173-ae60-9b6e140044d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a072d137-36a1-4a6e-b4ca-d9731eead77f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92fb087-e8f2-4498-b765-8a663d02677e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a072d137-36a1-4a6e-b4ca-d9731eead77f",
                    "LayerId": "1694400b-c6f8-4794-8665-178dc31f2a92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1694400b-c6f8-4794-8665-178dc31f2a92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2db4e53c-0189-4288-ade7-b518f9e11f9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}