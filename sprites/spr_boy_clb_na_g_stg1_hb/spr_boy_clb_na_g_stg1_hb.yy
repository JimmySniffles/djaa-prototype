{
    "id": "65540dd7-cfe9-4253-8b40-d30857025152",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_na_g_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0da10abb-0e47-4ff0-9156-a95e537d554b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "607b2e3b-1099-4f94-a3b9-202c02d5aff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da10abb-0e47-4ff0-9156-a95e537d554b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96de3703-c6c5-477a-828d-79a50e0aa729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da10abb-0e47-4ff0-9156-a95e537d554b",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "e4ce754d-d317-4f5e-8171-6ecefa432f83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da10abb-0e47-4ff0-9156-a95e537d554b",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        },
        {
            "id": "ab386a5c-e263-4014-9ce1-1d2e7e099b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "13f56ff3-ad83-4462-b3a4-bd9eaf431ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab386a5c-e263-4014-9ce1-1d2e7e099b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7852c82d-eda5-4bd4-839d-d2022d38241a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab386a5c-e263-4014-9ce1-1d2e7e099b30",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "446e1606-baf6-46e2-80a9-5c382bb86bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab386a5c-e263-4014-9ce1-1d2e7e099b30",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        },
        {
            "id": "857cde70-d8bf-45a7-b63f-2c3889642fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "be1100de-0695-4b8b-88b0-d68826b9a3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "857cde70-d8bf-45a7-b63f-2c3889642fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd4c8bd-aab3-4d24-b346-69cbb1a7f5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857cde70-d8bf-45a7-b63f-2c3889642fe8",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "0edaed59-1330-4c66-ba76-e88527e8807f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857cde70-d8bf-45a7-b63f-2c3889642fe8",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        },
        {
            "id": "885e48ad-84df-4328-912d-c2c5264bb964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "4da8e6c7-8574-4103-8f83-04d89b43dccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885e48ad-84df-4328-912d-c2c5264bb964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d69a5cf-2254-4bf6-8f3e-e3323014512c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885e48ad-84df-4328-912d-c2c5264bb964",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "f608eb95-43cb-4c24-8ae9-7d0cc23239c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885e48ad-84df-4328-912d-c2c5264bb964",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        },
        {
            "id": "0e54d167-2a1a-45c4-9a4e-c7ccfb88bbab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "e3ce0638-f3d3-49c1-92b0-9ed522d07e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e54d167-2a1a-45c4-9a4e-c7ccfb88bbab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f606029-3460-4440-a982-2482cdb7c3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e54d167-2a1a-45c4-9a4e-c7ccfb88bbab",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "98af2353-e0e3-48e3-baa0-d353891de01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e54d167-2a1a-45c4-9a4e-c7ccfb88bbab",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        },
        {
            "id": "fde2c3f7-afe5-4ffb-81ae-59b725241093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "compositeImage": {
                "id": "81eb5c80-84f5-424d-a8aa-e8246c71ea36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde2c3f7-afe5-4ffb-81ae-59b725241093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffcdfdc8-17c4-4b44-9463-fb444b28784c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde2c3f7-afe5-4ffb-81ae-59b725241093",
                    "LayerId": "d654fb99-1a84-4469-8353-a70b6ce88b1c"
                },
                {
                    "id": "34b32a1f-07df-4750-b968-a8d7a18fc93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde2c3f7-afe5-4ffb-81ae-59b725241093",
                    "LayerId": "31111d88-ef7a-416a-896d-fd90ea256715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "31111d88-ef7a-416a-896d-fd90ea256715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "blendMode": 0,
            "isLocked": false,
            "name": "Hitbox",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d654fb99-1a84-4469-8353-a70b6ce88b1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65540dd7-cfe9-4253-8b40-d30857025152",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}