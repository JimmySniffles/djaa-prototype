{
    "id": "2539de85-2cbd-4d63-8833-5d06becc5830",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkang_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75be01cc-2b59-47b7-97b4-d8f9459ef24b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2539de85-2cbd-4d63-8833-5d06becc5830",
            "compositeImage": {
                "id": "51ec6c76-03ea-4b6c-8b6b-40f3c7de0720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75be01cc-2b59-47b7-97b4-d8f9459ef24b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "793eaffb-6c81-4252-b872-0eed684d7670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75be01cc-2b59-47b7-97b4-d8f9459ef24b",
                    "LayerId": "d2dc3566-2df4-4591-8fa4-23263c636e5e"
                }
            ]
        },
        {
            "id": "06243a7f-ed48-4a64-9f27-f5e8ce731913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2539de85-2cbd-4d63-8833-5d06becc5830",
            "compositeImage": {
                "id": "0fac618d-c6d0-483d-b6bc-4976e94334fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06243a7f-ed48-4a64-9f27-f5e8ce731913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296fff44-da4a-4bfc-a121-b6bdc0e2c0c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06243a7f-ed48-4a64-9f27-f5e8ce731913",
                    "LayerId": "d2dc3566-2df4-4591-8fa4-23263c636e5e"
                }
            ]
        },
        {
            "id": "6f4dbfed-9f12-436f-8e01-bc28a6249268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2539de85-2cbd-4d63-8833-5d06becc5830",
            "compositeImage": {
                "id": "c2cb1a2a-18a9-494a-aba8-f8559a1992f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4dbfed-9f12-436f-8e01-bc28a6249268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5499d13a-0fda-4125-97e7-54f5444303ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4dbfed-9f12-436f-8e01-bc28a6249268",
                    "LayerId": "d2dc3566-2df4-4591-8fa4-23263c636e5e"
                }
            ]
        },
        {
            "id": "639c83bc-58c7-41f9-9d7c-5fc3b63ffcab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2539de85-2cbd-4d63-8833-5d06becc5830",
            "compositeImage": {
                "id": "acc897ed-a716-4f61-955b-ffd515a48630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "639c83bc-58c7-41f9-9d7c-5fc3b63ffcab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2870d3d-66f3-47ce-a4e9-a26d57a859c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "639c83bc-58c7-41f9-9d7c-5fc3b63ffcab",
                    "LayerId": "d2dc3566-2df4-4591-8fa4-23263c636e5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "d2dc3566-2df4-4591-8fa4-23263c636e5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2539de85-2cbd-4d63-8833-5d06becc5830",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}