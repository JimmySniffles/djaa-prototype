{
    "id": "1f0e2737-3b69-4b36-8d02-fca92b634b36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_levelicon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 175,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "311f1b3a-7739-42bf-8519-e92734c0a962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0e2737-3b69-4b36-8d02-fca92b634b36",
            "compositeImage": {
                "id": "cb6a95fa-6cb1-4322-a3f2-069cc8142376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311f1b3a-7739-42bf-8519-e92734c0a962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df632b68-eec6-43d7-9a13-9029dc07c885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311f1b3a-7739-42bf-8519-e92734c0a962",
                    "LayerId": "a124fc5d-c7c8-4a03-9318-403e69b836dc"
                }
            ]
        },
        {
            "id": "8339800e-1c01-42d0-8ded-236fc52f1391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0e2737-3b69-4b36-8d02-fca92b634b36",
            "compositeImage": {
                "id": "03bd15de-b305-49bd-bec8-9990b868f666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8339800e-1c01-42d0-8ded-236fc52f1391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5212cbc-c2c9-4a1c-b0e5-5409f9b4cc31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8339800e-1c01-42d0-8ded-236fc52f1391",
                    "LayerId": "a124fc5d-c7c8-4a03-9318-403e69b836dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "a124fc5d-c7c8-4a03-9318-403e69b836dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f0e2737-3b69-4b36-8d02-fca92b634b36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 176,
    "xorig": 0,
    "yorig": 175
}