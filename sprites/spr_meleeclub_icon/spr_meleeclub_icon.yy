{
    "id": "1dd9b9ba-c6e1-4349-a198-35610edf6b64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_meleeclub_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ed18760-158b-4ed3-a783-a8a87740dcde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd9b9ba-c6e1-4349-a198-35610edf6b64",
            "compositeImage": {
                "id": "1e1fb062-1077-4253-8ca7-9736e44ae1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed18760-158b-4ed3-a783-a8a87740dcde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5cddfb-9bcf-402e-b421-e8f8650b1683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed18760-158b-4ed3-a783-a8a87740dcde",
                    "LayerId": "f6ec4c0a-8cd7-42bb-996b-d5ded7f51353"
                }
            ]
        },
        {
            "id": "498620b6-7bde-4d82-8699-7ee7330d49cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd9b9ba-c6e1-4349-a198-35610edf6b64",
            "compositeImage": {
                "id": "3d3dc192-8930-440e-a1c7-927298bdef10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "498620b6-7bde-4d82-8699-7ee7330d49cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22729a7-e2ef-400b-b980-259663896187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "498620b6-7bde-4d82-8699-7ee7330d49cd",
                    "LayerId": "f6ec4c0a-8cd7-42bb-996b-d5ded7f51353"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "f6ec4c0a-8cd7-42bb-996b-d5ded7f51353",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dd9b9ba-c6e1-4349-a198-35610edf6b64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 1,
    "yorig": 3
}