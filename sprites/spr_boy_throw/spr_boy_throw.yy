{
    "id": "e611bdc8-1f76-4408-ae86-229c92c384cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_throw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d028970-7f3f-47e8-a92d-d271f2364084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "f754aa78-719d-4457-a80d-841e5aed34ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d028970-7f3f-47e8-a92d-d271f2364084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c35695-41a4-490d-9eff-bb8e524b4006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d028970-7f3f-47e8-a92d-d271f2364084",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        },
        {
            "id": "5644b187-ff57-4183-8add-75c4c7f96879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "9c5eff63-8421-48b5-98af-8b1acd748591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5644b187-ff57-4183-8add-75c4c7f96879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af1a13f-a3fe-4279-bf8a-907a7adf88a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5644b187-ff57-4183-8add-75c4c7f96879",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        },
        {
            "id": "a788e3ab-1f8a-4170-a325-e2dfe7fe57f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "4c8ea57a-72c3-4c4f-b91f-38e66af6c7e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a788e3ab-1f8a-4170-a325-e2dfe7fe57f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15521775-0dae-41c7-86dc-dede62fcc67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a788e3ab-1f8a-4170-a325-e2dfe7fe57f3",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        },
        {
            "id": "9a538ffd-bc95-4ec1-b7bb-ce2fdeda5c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "1c589ec3-b174-4dc9-993d-5e0e91e7b31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a538ffd-bc95-4ec1-b7bb-ce2fdeda5c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba4c585-4bc7-4438-a855-c062cb48da2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a538ffd-bc95-4ec1-b7bb-ce2fdeda5c22",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        },
        {
            "id": "02997e06-2c1f-4d86-96ae-8433ea70d9d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "f3cda4a4-e982-4db2-9da2-2a244f519c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02997e06-2c1f-4d86-96ae-8433ea70d9d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa689df4-adb7-44f8-a919-4a5bf214bb8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02997e06-2c1f-4d86-96ae-8433ea70d9d3",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        },
        {
            "id": "54dbac87-c362-4aa8-8955-1d6c5dbd624d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "compositeImage": {
                "id": "1df8cc14-c367-4417-99ef-7d7958fde841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54dbac87-c362-4aa8-8955-1d6c5dbd624d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaaa5bb6-b3cc-4344-a7fa-81d8fa1eec60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54dbac87-c362-4aa8-8955-1d6c5dbd624d",
                    "LayerId": "0b670951-eefa-4154-b8a0-3224edad4797"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b670951-eefa-4154-b8a0-3224edad4797",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e611bdc8-1f76-4408-ae86-229c92c384cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}