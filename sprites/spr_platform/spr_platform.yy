{
    "id": "a398c061-459c-415c-a9f0-6e035d004bb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "378d9ea3-e8e9-46f5-8112-3ee39eee703d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a398c061-459c-415c-a9f0-6e035d004bb8",
            "compositeImage": {
                "id": "c0655e4b-547f-4f67-99c9-9bbae9258f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378d9ea3-e8e9-46f5-8112-3ee39eee703d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f416e8-993f-4ecf-a243-06d233b398d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378d9ea3-e8e9-46f5-8112-3ee39eee703d",
                    "LayerId": "e98849ba-81af-4bfa-9c14-809ec19f0147"
                }
            ]
        },
        {
            "id": "8e1f9e20-e5d7-4d5a-a622-6ba6f5aaea41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a398c061-459c-415c-a9f0-6e035d004bb8",
            "compositeImage": {
                "id": "1971358a-1d2b-4db5-9fa6-1d3e1714b7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1f9e20-e5d7-4d5a-a622-6ba6f5aaea41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81fc76c-38ef-47ef-b67e-961a138f71c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1f9e20-e5d7-4d5a-a622-6ba6f5aaea41",
                    "LayerId": "e98849ba-81af-4bfa-9c14-809ec19f0147"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e98849ba-81af-4bfa-9c14-809ec19f0147",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a398c061-459c-415c-a9f0-6e035d004bb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}