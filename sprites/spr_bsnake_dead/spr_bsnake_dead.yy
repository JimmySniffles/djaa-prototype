{
    "id": "37c58337-4282-42c1-95fd-8aa78dce16d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bsnake_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bd0f6cb-74de-46b9-8806-6ec8a6682453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "compositeImage": {
                "id": "fcca284d-092d-4a58-910b-c0400739b11c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd0f6cb-74de-46b9-8806-6ec8a6682453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "204ed94d-4d42-44d9-a162-fb56dbc79d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd0f6cb-74de-46b9-8806-6ec8a6682453",
                    "LayerId": "9347ecb0-2019-48f4-b1a8-b318eff272f5"
                }
            ]
        },
        {
            "id": "83c90fae-853d-4dbe-9c3b-acdcfd732b02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "compositeImage": {
                "id": "60b11df3-d19e-4449-8dec-e57a5ea824f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c90fae-853d-4dbe-9c3b-acdcfd732b02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a4ad45-7d30-413d-ab84-b407648fd521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c90fae-853d-4dbe-9c3b-acdcfd732b02",
                    "LayerId": "9347ecb0-2019-48f4-b1a8-b318eff272f5"
                }
            ]
        },
        {
            "id": "85ff0335-b056-4fb2-b7c7-860a2edbb078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "compositeImage": {
                "id": "bef5178c-ed4e-4b0e-8cc9-5d232525c1ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ff0335-b056-4fb2-b7c7-860a2edbb078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83274ec7-ff3e-4375-bc7c-637e7cd0f185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ff0335-b056-4fb2-b7c7-860a2edbb078",
                    "LayerId": "9347ecb0-2019-48f4-b1a8-b318eff272f5"
                }
            ]
        },
        {
            "id": "b035d13a-3ef0-4429-b3e2-87b292a39c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "compositeImage": {
                "id": "02f9521d-00ed-485c-88bb-155cf1f437d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b035d13a-3ef0-4429-b3e2-87b292a39c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2d67dd-2ecf-4167-aaa3-8243ccf6063d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b035d13a-3ef0-4429-b3e2-87b292a39c94",
                    "LayerId": "9347ecb0-2019-48f4-b1a8-b318eff272f5"
                }
            ]
        },
        {
            "id": "29d908dc-6514-4eef-85a8-cda6dba58c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "compositeImage": {
                "id": "3135da8d-f57b-4207-910c-aaa80fd9fef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d908dc-6514-4eef-85a8-cda6dba58c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb044e4b-8162-4094-baa3-b02899346289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d908dc-6514-4eef-85a8-cda6dba58c56",
                    "LayerId": "9347ecb0-2019-48f4-b1a8-b318eff272f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "9347ecb0-2019-48f4-b1a8-b318eff272f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37c58337-4282-42c1-95fd-8aa78dce16d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}