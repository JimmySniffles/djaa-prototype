{
    "id": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_solace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 7,
    "bbox_right": 107,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0e713a4-8b9f-43e1-bfc5-38f9a474310f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
            "compositeImage": {
                "id": "79f1323b-57d6-43d5-b23b-56af6e1ff3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e713a4-8b9f-43e1-bfc5-38f9a474310f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc91f266-8b21-4db7-a99d-95bb96ebc2ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e713a4-8b9f-43e1-bfc5-38f9a474310f",
                    "LayerId": "228c96d6-48eb-4f21-8754-974355beac87"
                }
            ]
        },
        {
            "id": "cb4edc1a-adfb-4dc9-9e29-d3e97ccd5bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
            "compositeImage": {
                "id": "c2966ab7-87b7-4ea5-a1e7-19b4defa6dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb4edc1a-adfb-4dc9-9e29-d3e97ccd5bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a85d5fc-39e6-494d-95d9-d5671077a381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb4edc1a-adfb-4dc9-9e29-d3e97ccd5bd0",
                    "LayerId": "228c96d6-48eb-4f21-8754-974355beac87"
                }
            ]
        },
        {
            "id": "ed50fb23-8977-4285-b7e8-63b770651622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
            "compositeImage": {
                "id": "6f9cc846-1a8a-4e8e-b7cd-ba2965aaac35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed50fb23-8977-4285-b7e8-63b770651622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21db6e57-d033-4c28-aa1b-668ae03b45b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed50fb23-8977-4285-b7e8-63b770651622",
                    "LayerId": "228c96d6-48eb-4f21-8754-974355beac87"
                }
            ]
        },
        {
            "id": "aed32a31-9a54-4480-b2db-00a5f7dab745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
            "compositeImage": {
                "id": "603682b6-ee3d-45a0-8bd1-0514709972c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed32a31-9a54-4480-b2db-00a5f7dab745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03813823-f15d-4ba0-9302-a099629fa5da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed32a31-9a54-4480-b2db-00a5f7dab745",
                    "LayerId": "228c96d6-48eb-4f21-8754-974355beac87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "228c96d6-48eb-4f21-8754-974355beac87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1883ff7a-e219-400c-81b4-cca6fcdf3c8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}