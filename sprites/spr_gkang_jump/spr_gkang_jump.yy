{
    "id": "a67f35b3-2ae6-4e07-8a19-c5efd26eeaff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkang_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "978af2c3-4c64-4bb2-99ab-830e45260fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67f35b3-2ae6-4e07-8a19-c5efd26eeaff",
            "compositeImage": {
                "id": "f33e86d0-f4fe-437e-8ddb-054102e5661b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978af2c3-4c64-4bb2-99ab-830e45260fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dab52722-7b64-4831-b3be-9d3dd351a1aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978af2c3-4c64-4bb2-99ab-830e45260fd7",
                    "LayerId": "71fddf78-1bd6-46a5-bfb4-68a9a319a380"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "71fddf78-1bd6-46a5-bfb4-68a9a319a380",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a67f35b3-2ae6-4e07-8a19-c5efd26eeaff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}