{
    "id": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcded2ee-5865-4b2f-bc7e-1ef561b0a8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
            "compositeImage": {
                "id": "b4ba813e-b776-4b4e-9b6e-236d51c44141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcded2ee-5865-4b2f-bc7e-1ef561b0a8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4cdb79-286f-4e4f-a476-3f4c3ecfecf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcded2ee-5865-4b2f-bc7e-1ef561b0a8b3",
                    "LayerId": "3cda1dbb-18af-46dd-83df-9eddbca1f367"
                }
            ]
        },
        {
            "id": "e8a8262d-45a5-4d2c-8de7-0e91df56d28f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
            "compositeImage": {
                "id": "1446ba03-178d-4a64-9f7f-2c98f736b6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a8262d-45a5-4d2c-8de7-0e91df56d28f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3cd5c79-4878-443b-8472-1796c19afe6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a8262d-45a5-4d2c-8de7-0e91df56d28f",
                    "LayerId": "3cda1dbb-18af-46dd-83df-9eddbca1f367"
                }
            ]
        },
        {
            "id": "9c75037d-4496-47c6-9f76-7b08340f60f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
            "compositeImage": {
                "id": "ac0ba9aa-c557-41ee-897f-0801439f989b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c75037d-4496-47c6-9f76-7b08340f60f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2f7eea-3249-4b6b-a56f-44250dbe3ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c75037d-4496-47c6-9f76-7b08340f60f0",
                    "LayerId": "3cda1dbb-18af-46dd-83df-9eddbca1f367"
                }
            ]
        },
        {
            "id": "c1a3999f-561d-4196-8df1-fc6267a6a639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
            "compositeImage": {
                "id": "e51b128e-fbfa-4a89-bae2-c05fe6c0d166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a3999f-561d-4196-8df1-fc6267a6a639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b192019e-5a2f-42ac-bb33-751f8a60dca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a3999f-561d-4196-8df1-fc6267a6a639",
                    "LayerId": "3cda1dbb-18af-46dd-83df-9eddbca1f367"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3cda1dbb-18af-46dd-83df-9eddbca1f367",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b1fa9e9-3e2b-4b69-8690-40ed95fb4b71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}