{
    "id": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_sa_g_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98e73ac2-5bc7-4bb8-bd90-2d2fb5e7b09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "eb3cfb16-cb4f-4fc1-8444-eaf7301f7949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98e73ac2-5bc7-4bb8-bd90-2d2fb5e7b09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09513ba5-a76f-436c-8ed6-61fa92519c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e73ac2-5bc7-4bb8-bd90-2d2fb5e7b09d",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "ff2e7501-7e9f-433d-9685-a327d3cb41fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e73ac2-5bc7-4bb8-bd90-2d2fb5e7b09d",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        },
        {
            "id": "e7d773ad-dcc1-4766-a9df-26be548e95b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "bfbd5b85-ee6b-4f7f-b6be-61a9cc763024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d773ad-dcc1-4766-a9df-26be548e95b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55dc66f0-1db6-4e11-b043-9fe86c729520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d773ad-dcc1-4766-a9df-26be548e95b8",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "30d26615-5d0f-4f07-ab9b-a1249889f0c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d773ad-dcc1-4766-a9df-26be548e95b8",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        },
        {
            "id": "65f20358-27c2-40e9-81b6-c8789ba537b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "9b7e3112-366d-42cd-8390-89b04dc3c1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f20358-27c2-40e9-81b6-c8789ba537b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9ab519a-30da-4abe-a136-aaf614b24eb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f20358-27c2-40e9-81b6-c8789ba537b0",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "7f0d9edc-7a42-4c6a-8b8b-98b2c7353cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f20358-27c2-40e9-81b6-c8789ba537b0",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        },
        {
            "id": "151543d0-49de-4e2c-85a8-60ffb5a6a7bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "2cbcb7f8-f570-4094-82b1-1cac7f677935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "151543d0-49de-4e2c-85a8-60ffb5a6a7bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2726cab0-4d58-4d89-866c-1184c7a759e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151543d0-49de-4e2c-85a8-60ffb5a6a7bd",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "3aaef44b-4f11-49be-89e2-07cc8eb54c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151543d0-49de-4e2c-85a8-60ffb5a6a7bd",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        },
        {
            "id": "1a3c8a5b-90c6-4d18-b7c6-3027697d8ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "ca21c265-e674-429a-b96c-fa646a1b3d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3c8a5b-90c6-4d18-b7c6-3027697d8ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3375432-9f99-4c74-b78d-e7f78cde8f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3c8a5b-90c6-4d18-b7c6-3027697d8ab5",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "7810d492-3998-4211-8e2d-5fa1fc82d094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3c8a5b-90c6-4d18-b7c6-3027697d8ab5",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        },
        {
            "id": "944bba6f-e02e-49af-b0ff-2016c3331ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "compositeImage": {
                "id": "2e109a94-742e-402b-9575-b723006f243d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944bba6f-e02e-49af-b0ff-2016c3331ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f91830-2462-495d-b9ee-918ee60ce739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944bba6f-e02e-49af-b0ff-2016c3331ca5",
                    "LayerId": "2047eec2-1825-4448-a970-9958384d3337"
                },
                {
                    "id": "56fe8256-264e-424d-800e-c3bb3af659bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944bba6f-e02e-49af-b0ff-2016c3331ca5",
                    "LayerId": "d58ee79b-f50c-4f88-a115-d2488d3a065a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2047eec2-1825-4448-a970-9958384d3337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d58ee79b-f50c-4f88-a115-d2488d3a065a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8babbab-4efb-4238-bff2-47093ce8e2cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}