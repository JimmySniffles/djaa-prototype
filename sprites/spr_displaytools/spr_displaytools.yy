{
    "id": "724611b2-848c-4238-9ad0-150e672d46ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_displaytools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 313,
    "bbox_left": 207,
    "bbox_right": 2713,
    "bbox_top": 102,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d349e3f2-08d0-4a8b-b9d0-ef22e3c6efcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "724611b2-848c-4238-9ad0-150e672d46ac",
            "compositeImage": {
                "id": "2081daac-7579-43b6-a2e2-07c8011ae1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d349e3f2-08d0-4a8b-b9d0-ef22e3c6efcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef6cb2d-aafa-471b-bc81-9a870c3852c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d349e3f2-08d0-4a8b-b9d0-ef22e3c6efcf",
                    "LayerId": "7a594de8-32a6-44fb-ab78-fb181c45ea7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 417,
    "layers": [
        {
            "id": "7a594de8-32a6-44fb-ab78-fb181c45ea7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "724611b2-848c-4238-9ad0-150e672d46ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2917,
    "xorig": 1458,
    "yorig": 208
}