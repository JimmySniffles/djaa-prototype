{
    "id": "a7976193-0c2b-4fb1-938e-ab67d06f0cfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e0ff3ea-98b0-400a-b39b-1ee0a12ad7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7976193-0c2b-4fb1-938e-ab67d06f0cfa",
            "compositeImage": {
                "id": "ef611051-a3e7-4b79-9665-3c28d86db22b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0ff3ea-98b0-400a-b39b-1ee0a12ad7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a88bdcbb-4b73-4a7a-a57f-6cf94e609b8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0ff3ea-98b0-400a-b39b-1ee0a12ad7d6",
                    "LayerId": "9060207e-b311-4561-9d32-229bf7f68087"
                }
            ]
        },
        {
            "id": "6d80695f-7ce4-4f41-934c-067049d88abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7976193-0c2b-4fb1-938e-ab67d06f0cfa",
            "compositeImage": {
                "id": "db03c9af-c421-4ccc-8652-7ba29d5fec38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d80695f-7ce4-4f41-934c-067049d88abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c374fab-84c4-4e18-ba08-56c47e3cf75c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d80695f-7ce4-4f41-934c-067049d88abe",
                    "LayerId": "9060207e-b311-4561-9d32-229bf7f68087"
                }
            ]
        },
        {
            "id": "7aa9e01a-ab3d-4149-b506-4dc09617786b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7976193-0c2b-4fb1-938e-ab67d06f0cfa",
            "compositeImage": {
                "id": "b3099cdd-8c42-4ba9-9126-9e15a2ecb596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa9e01a-ab3d-4149-b506-4dc09617786b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323285b4-ab80-4fe3-8849-eaea27eac6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa9e01a-ab3d-4149-b506-4dc09617786b",
                    "LayerId": "9060207e-b311-4561-9d32-229bf7f68087"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9060207e-b311-4561-9d32-229bf7f68087",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7976193-0c2b-4fb1-938e-ab67d06f0cfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}