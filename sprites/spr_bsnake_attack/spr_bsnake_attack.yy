{
    "id": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bsnake_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6032f92-4208-44f5-b890-7339283ba497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "37a863ce-3837-411c-80bf-f45f240084db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6032f92-4208-44f5-b890-7339283ba497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4c2f25-3bf6-47ea-b7b2-1cc469fd4d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6032f92-4208-44f5-b890-7339283ba497",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "e98bffd1-daa5-463e-b8ad-485cd7089fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "9955cbbc-fd3e-40dc-be2f-6d1431687965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98bffd1-daa5-463e-b8ad-485cd7089fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d83445d-d214-4bf6-a08c-4b3d282c259c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98bffd1-daa5-463e-b8ad-485cd7089fe0",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "0684f3d4-68c6-4653-9b22-291e387f3a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "66f1343c-fa9a-476f-bf6b-5bc96f15b67d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0684f3d4-68c6-4653-9b22-291e387f3a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f376af-da67-4284-bc40-7dc625e52919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0684f3d4-68c6-4653-9b22-291e387f3a66",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "3d2822a9-960f-449f-9145-19eb9d8f46ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "c0b31e15-c849-43a5-9ed4-23a909c425e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2822a9-960f-449f-9145-19eb9d8f46ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a2b950-6da0-46eb-a9a2-479f3963f5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2822a9-960f-449f-9145-19eb9d8f46ca",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "006793f6-f469-4c83-8bfc-540fd93f268c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "8675d46e-d411-4c49-9406-5f2eca1e7001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006793f6-f469-4c83-8bfc-540fd93f268c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d972cefa-1420-400c-8470-476241b2cf18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006793f6-f469-4c83-8bfc-540fd93f268c",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "e71b2644-c736-4edc-9fd8-c2b53eda359f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "ac06977f-c6e4-434f-ae6e-304bb4382ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71b2644-c736-4edc-9fd8-c2b53eda359f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2e4079-c4be-4dfc-98f5-981f9f5e909e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71b2644-c736-4edc-9fd8-c2b53eda359f",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "43ee33ed-dedc-4ba7-81a5-68f9ed30a62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "da4a8b3c-731d-4d40-93c0-f47f95a760ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ee33ed-dedc-4ba7-81a5-68f9ed30a62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc215d73-5ebb-4430-965d-97a9f1d420f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ee33ed-dedc-4ba7-81a5-68f9ed30a62f",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        },
        {
            "id": "cd9d9b6b-ad72-4f78-946e-1d323d6830fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "compositeImage": {
                "id": "2537274e-fa48-4cbd-8a2f-496b8d59300e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9d9b6b-ad72-4f78-946e-1d323d6830fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd514a9-3f9c-4cbf-8edf-08fd66b2a44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9d9b6b-ad72-4f78-946e-1d323d6830fb",
                    "LayerId": "99df34b3-49d5-444f-8d6d-015c27fd97c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "99df34b3-49d5-444f-8d6d-015c27fd97c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21dd99fa-d80d-43d6-9b5c-9ca3c87197c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}