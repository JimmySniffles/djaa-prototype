{
    "id": "14b874bc-72cb-48d6-9758-6f5ffaff3e4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass_slopes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f41b0bad-d133-4e6d-97e0-a44c55065ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14b874bc-72cb-48d6-9758-6f5ffaff3e4c",
            "compositeImage": {
                "id": "54fbdc94-6f60-4637-9fa1-16de6ced5156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f41b0bad-d133-4e6d-97e0-a44c55065ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8cd9e9-ea32-442d-91a6-1eb4558e4881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f41b0bad-d133-4e6d-97e0-a44c55065ccb",
                    "LayerId": "84f1280b-620b-4321-be05-9c470ad6605c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "84f1280b-620b-4321-be05-9c470ad6605c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14b874bc-72cb-48d6-9758-6f5ffaff3e4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 384,
    "yorig": 32
}