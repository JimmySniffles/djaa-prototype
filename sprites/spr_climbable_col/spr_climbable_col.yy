{
    "id": "e0123a09-5820-4a8c-8998-111b416baf22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_climbable_col",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2102d964-88bb-41f0-96f7-fb8673a71f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0123a09-5820-4a8c-8998-111b416baf22",
            "compositeImage": {
                "id": "bc070589-6888-48bf-85be-29b01953934c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2102d964-88bb-41f0-96f7-fb8673a71f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7ce1e9-6997-470c-82b8-ecc7666239f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2102d964-88bb-41f0-96f7-fb8673a71f17",
                    "LayerId": "eee4b009-e49a-42c1-b552-050ac06e1814"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eee4b009-e49a-42c1-b552-050ac06e1814",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0123a09-5820-4a8c-8998-111b416baf22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}