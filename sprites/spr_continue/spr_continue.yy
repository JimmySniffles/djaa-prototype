{
    "id": "842acf34-be33-4bb0-b07b-c792515f952a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ce57d81-d65b-4fa8-b5df-229e3043f833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "842acf34-be33-4bb0-b07b-c792515f952a",
            "compositeImage": {
                "id": "9d9daa44-9210-4fd0-876b-5b2bc31ab86f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce57d81-d65b-4fa8-b5df-229e3043f833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531cd6fa-f899-44c1-816f-18619007d9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce57d81-d65b-4fa8-b5df-229e3043f833",
                    "LayerId": "12c26aef-b360-48d4-8827-19f30775c281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "12c26aef-b360-48d4-8827-19f30775c281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "842acf34-be33-4bb0-b07b-c792515f952a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}