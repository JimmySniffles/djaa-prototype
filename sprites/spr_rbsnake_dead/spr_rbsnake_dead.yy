{
    "id": "045c9f73-68c3-4666-817f-0260dcaabf46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rbsnake_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8c3ace9-fbd8-4455-8825-815c8f37550d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "compositeImage": {
                "id": "2602e3c0-663b-42ab-8e07-e9cbc5fd4eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c3ace9-fbd8-4455-8825-815c8f37550d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e58317b-23b5-44fd-8bf9-bde24f3a817f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c3ace9-fbd8-4455-8825-815c8f37550d",
                    "LayerId": "f6361f64-d871-48d8-af13-fbb6488252bf"
                }
            ]
        },
        {
            "id": "5656c6cf-6513-42f8-9959-17ad7fa45720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "compositeImage": {
                "id": "f3f5d909-1f87-4d53-a959-da16c5db4f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5656c6cf-6513-42f8-9959-17ad7fa45720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e50abf6-2c5d-48b2-a182-cd7704d3ec0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5656c6cf-6513-42f8-9959-17ad7fa45720",
                    "LayerId": "f6361f64-d871-48d8-af13-fbb6488252bf"
                }
            ]
        },
        {
            "id": "e6a90d0c-cf53-485a-a526-d4fa04a619a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "compositeImage": {
                "id": "b1b15854-4a07-4d74-a97b-b96d0a2c997b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a90d0c-cf53-485a-a526-d4fa04a619a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0b8d3f-ccfe-476b-919f-f668482421b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a90d0c-cf53-485a-a526-d4fa04a619a9",
                    "LayerId": "f6361f64-d871-48d8-af13-fbb6488252bf"
                }
            ]
        },
        {
            "id": "442f4720-3977-44a2-99cf-d37a8b13ea4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "compositeImage": {
                "id": "f18717e1-7339-42d0-97fb-3c89105d46c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "442f4720-3977-44a2-99cf-d37a8b13ea4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "661ff54e-86f9-4bb0-b913-2a2e1c2ec633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "442f4720-3977-44a2-99cf-d37a8b13ea4a",
                    "LayerId": "f6361f64-d871-48d8-af13-fbb6488252bf"
                }
            ]
        },
        {
            "id": "4f8b36af-c8d9-460b-b8d1-5f7966a31b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "compositeImage": {
                "id": "f97ea6bd-146a-4fbb-89a9-a82c626fdaf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8b36af-c8d9-460b-b8d1-5f7966a31b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56f8db6-761e-4e8e-abff-7587ce169081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8b36af-c8d9-460b-b8d1-5f7966a31b05",
                    "LayerId": "f6361f64-d871-48d8-af13-fbb6488252bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "f6361f64-d871-48d8-af13-fbb6488252bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "045c9f73-68c3-4666-817f-0260dcaabf46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}