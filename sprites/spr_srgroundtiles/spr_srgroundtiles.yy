{
    "id": "4d2b2c60-c0b3-4b54-a056-6836a53d3961",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_srgroundtiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ec724d2-adf3-4f15-8f8f-032201283ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d2b2c60-c0b3-4b54-a056-6836a53d3961",
            "compositeImage": {
                "id": "cf8ba816-6f23-41ac-b664-ac445b7e80b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec724d2-adf3-4f15-8f8f-032201283ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82984477-ac81-4f16-8f56-20f6c619530e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec724d2-adf3-4f15-8f8f-032201283ae5",
                    "LayerId": "f9a790a6-1d2c-41fc-ad67-be435d37bb0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "f9a790a6-1d2c-41fc-ad67-be435d37bb0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d2b2c60-c0b3-4b54-a056-6836a53d3961",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 192
}