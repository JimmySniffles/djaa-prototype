{
    "id": "f751c876-ac13-4443-bd6a-ebb1457967d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mud_slopes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e85e358-2ce1-44a8-8cf6-76944a1948ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f751c876-ac13-4443-bd6a-ebb1457967d1",
            "compositeImage": {
                "id": "ce9cee61-f57e-45b6-a3a0-d2229ed47323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e85e358-2ce1-44a8-8cf6-76944a1948ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bec68d0-f069-4bb3-88ac-b83e185f2e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e85e358-2ce1-44a8-8cf6-76944a1948ed",
                    "LayerId": "a690ea1b-d79e-4a0e-baa3-9e962810c2b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a690ea1b-d79e-4a0e-baa3-9e962810c2b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f751c876-ac13-4443-bd6a-ebb1457967d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 384,
    "yorig": 32
}