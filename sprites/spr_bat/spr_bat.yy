{
    "id": "a162174c-e098-4eaf-94de-cfc7265a06aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe40d37d-90e8-4425-beaf-5b5c99d65829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "ec27b3f5-ddb7-446e-8ed9-56263271fb91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe40d37d-90e8-4425-beaf-5b5c99d65829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935a95be-704f-422e-ac2d-ccf1a563f7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe40d37d-90e8-4425-beaf-5b5c99d65829",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        },
        {
            "id": "363f7778-e197-494c-adce-f3ea142ad615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "694baebd-fbfb-49dd-93a7-34d47d75158d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363f7778-e197-494c-adce-f3ea142ad615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb538bc-47ff-4892-ae25-31db65a4e926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363f7778-e197-494c-adce-f3ea142ad615",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        },
        {
            "id": "7cd588b5-5295-418f-8e58-bc6fcacd4558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "4b6bf65c-6e88-4827-a216-b96603e070a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd588b5-5295-418f-8e58-bc6fcacd4558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d2c07fb-4e6b-4d9a-994b-af48bba6e07c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd588b5-5295-418f-8e58-bc6fcacd4558",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        },
        {
            "id": "78fac2d6-d184-4956-8aee-52986889bcf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "57435a8d-2bd5-4238-8b26-a062d0cbd6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fac2d6-d184-4956-8aee-52986889bcf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826c1b9b-fb4a-4e47-813c-65f81af368fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fac2d6-d184-4956-8aee-52986889bcf3",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        },
        {
            "id": "0ff36901-8337-449c-84ea-cb5c782f1b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "4ac44595-6783-48bf-9c50-2c14e639f7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff36901-8337-449c-84ea-cb5c782f1b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8027c976-e0ec-4e82-a3ea-b421f9edf39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff36901-8337-449c-84ea-cb5c782f1b9f",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        },
        {
            "id": "1b87f429-df1e-4c91-8ddd-1ef710b78de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "compositeImage": {
                "id": "9feb5327-75e0-4bda-823f-da32e7cb59a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b87f429-df1e-4c91-8ddd-1ef710b78de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043fa454-87c0-4fb3-951e-a61ad02b4b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b87f429-df1e-4c91-8ddd-1ef710b78de6",
                    "LayerId": "eb348ed0-e051-4866-a2e9-d30307f10ddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "eb348ed0-e051-4866-a2e9-d30307f10ddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a162174c-e098-4eaf-94de-cfc7265a06aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}