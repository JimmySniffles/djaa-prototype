{
    "id": "1fb2ae6a-5612-487e-b0c5-3f206ee892f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_srgroundslopes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d0edbb9-c328-4d4b-a576-6a4a7c596739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fb2ae6a-5612-487e-b0c5-3f206ee892f6",
            "compositeImage": {
                "id": "d611eec5-e6b0-469a-8943-6ee11968669a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0edbb9-c328-4d4b-a576-6a4a7c596739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eca7818-52a5-4ed3-b879-6db32c315845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0edbb9-c328-4d4b-a576-6a4a7c596739",
                    "LayerId": "046d425d-a783-4bf4-ac27-5ac8aaf08137"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "046d425d-a783-4bf4-ac27-5ac8aaf08137",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fb2ae6a-5612-487e-b0c5-3f206ee892f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 384,
    "yorig": 32
}