{
    "id": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dmgroots",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 60,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b11905c4-5e72-47da-9da5-80ca7cdb7b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "compositeImage": {
                "id": "d3be79e8-226b-443c-897c-f11cdafe5975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11905c4-5e72-47da-9da5-80ca7cdb7b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466135ee-3c64-443c-af71-141b00154cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11905c4-5e72-47da-9da5-80ca7cdb7b25",
                    "LayerId": "954966d8-5df3-43b6-9e1c-1bb4f2da573c"
                }
            ]
        },
        {
            "id": "ad378f88-1707-4bbb-b7f6-a4b8851191a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "compositeImage": {
                "id": "7a4599a0-235f-40f4-b700-b66cff32dfdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad378f88-1707-4bbb-b7f6-a4b8851191a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7acba3e-9ae1-4f9f-ad74-454069a00f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad378f88-1707-4bbb-b7f6-a4b8851191a1",
                    "LayerId": "954966d8-5df3-43b6-9e1c-1bb4f2da573c"
                }
            ]
        },
        {
            "id": "51a136d5-e03e-4ab8-aa09-f1b0df0f68e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "compositeImage": {
                "id": "e01b0158-3a27-4890-99eb-afa507051311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a136d5-e03e-4ab8-aa09-f1b0df0f68e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e79f2b6-961b-4c8e-afb5-ca4a2a24016f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a136d5-e03e-4ab8-aa09-f1b0df0f68e0",
                    "LayerId": "954966d8-5df3-43b6-9e1c-1bb4f2da573c"
                }
            ]
        },
        {
            "id": "6811ca15-418f-45f0-92f1-8772608826ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "compositeImage": {
                "id": "c959d879-29ac-405e-8920-c5257261dc03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6811ca15-418f-45f0-92f1-8772608826ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb10ac3-2bbd-47d7-9769-d177f9903d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6811ca15-418f-45f0-92f1-8772608826ea",
                    "LayerId": "954966d8-5df3-43b6-9e1c-1bb4f2da573c"
                }
            ]
        },
        {
            "id": "bc354725-5715-4162-8b88-d023d077d840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "compositeImage": {
                "id": "312dfad4-e5e7-4d87-9619-763618101fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc354725-5715-4162-8b88-d023d077d840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "666c2bfd-60bd-49ac-8cfc-0b67eaabf7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc354725-5715-4162-8b88-d023d077d840",
                    "LayerId": "954966d8-5df3-43b6-9e1c-1bb4f2da573c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "954966d8-5df3-43b6-9e1c-1bb4f2da573c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9153609c-8c9a-4844-a57d-15f35a4f9a3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 0
}