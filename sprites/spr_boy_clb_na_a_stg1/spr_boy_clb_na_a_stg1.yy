{
    "id": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_na_a_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "101eaf22-d37f-4d45-9920-c15a00d63727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "35d2f989-549a-4c5b-b548-85b8129a0356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101eaf22-d37f-4d45-9920-c15a00d63727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4366c94e-a8b0-459a-a5da-7a1d4afcfc4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101eaf22-d37f-4d45-9920-c15a00d63727",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "bf4dda7e-1c72-46ff-8d52-42912dbfb133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101eaf22-d37f-4d45-9920-c15a00d63727",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        },
        {
            "id": "a28b16ac-d343-4edb-b2cb-6a0fa42f9243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "0e1f812d-e3bd-4c0c-b9d4-fcdc8d0e1603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28b16ac-d343-4edb-b2cb-6a0fa42f9243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7ccbd1-2a7a-4134-b2f2-c55055f0cef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28b16ac-d343-4edb-b2cb-6a0fa42f9243",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "55bfbc82-ebbc-4a09-a0ba-f01e83d8c099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28b16ac-d343-4edb-b2cb-6a0fa42f9243",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        },
        {
            "id": "6a794a72-fad0-4dad-8c9a-ea9f2c5f9bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "726ceac2-5027-4a38-9807-531d397f6619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a794a72-fad0-4dad-8c9a-ea9f2c5f9bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "652912c3-3d42-482d-99f6-f170e1177519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a794a72-fad0-4dad-8c9a-ea9f2c5f9bc4",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "16f4644c-b39c-483d-90db-74c2deb67155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a794a72-fad0-4dad-8c9a-ea9f2c5f9bc4",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        },
        {
            "id": "f379da57-80c0-4560-a04a-50b200f0e465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "05359834-8eb3-4191-b1be-1272a67adde5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f379da57-80c0-4560-a04a-50b200f0e465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ab02d68-0427-4722-bedf-5820bcadbf0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f379da57-80c0-4560-a04a-50b200f0e465",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "3cc1a7f1-80c8-45f3-b645-f1be4e8ab63b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f379da57-80c0-4560-a04a-50b200f0e465",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        },
        {
            "id": "71c3d01c-e8e2-4394-b05c-2e6e3584c7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "ddfe787b-1899-494f-b688-1c02424a987e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c3d01c-e8e2-4394-b05c-2e6e3584c7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf4caca-69ca-494c-b616-0ade934910b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c3d01c-e8e2-4394-b05c-2e6e3584c7fd",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "7991520d-d96c-445b-b0b9-85cbe2589fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c3d01c-e8e2-4394-b05c-2e6e3584c7fd",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        },
        {
            "id": "899266eb-257a-45e3-a5b2-08abef3dbcdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "compositeImage": {
                "id": "fcc33556-2e1c-4acd-a4f3-f0f5f2423086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899266eb-257a-45e3-a5b2-08abef3dbcdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "397f9be7-c807-4304-81af-4e2810706e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899266eb-257a-45e3-a5b2-08abef3dbcdf",
                    "LayerId": "05366076-4815-40e0-8bf8-9263b343c804"
                },
                {
                    "id": "a8089e88-ac86-4324-8ed6-114efb273ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899266eb-257a-45e3-a5b2-08abef3dbcdf",
                    "LayerId": "74024dba-2d7b-435f-8f88-1cca0f54c141"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "05366076-4815-40e0-8bf8-9263b343c804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "74024dba-2d7b-435f-8f88-1cca0f54c141",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a67d5f8e-bbc2-4a11-909b-6a4f479e4c54",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}