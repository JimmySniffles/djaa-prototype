{
    "id": "ab7d01f1-ce66-43ae-96ed-7f3fce25aff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_budburra",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 3,
    "bbox_right": 112,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dd622a6-2801-4bf9-83c5-05f99b917c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab7d01f1-ce66-43ae-96ed-7f3fce25aff8",
            "compositeImage": {
                "id": "7e4550eb-4819-43bc-80f6-2dbc65969c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd622a6-2801-4bf9-83c5-05f99b917c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1913b060-1dee-47c0-ae3d-373845928f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd622a6-2801-4bf9-83c5-05f99b917c32",
                    "LayerId": "1bcc208f-6dbb-4569-ba18-c042a7df7ed0"
                }
            ]
        },
        {
            "id": "79ad8372-070d-4f37-8100-6dd91287adbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab7d01f1-ce66-43ae-96ed-7f3fce25aff8",
            "compositeImage": {
                "id": "af2d8fd1-7e1c-4634-a2b9-35717b2c341d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ad8372-070d-4f37-8100-6dd91287adbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e998e8-c91c-4533-8a70-78a182464bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ad8372-070d-4f37-8100-6dd91287adbc",
                    "LayerId": "1bcc208f-6dbb-4569-ba18-c042a7df7ed0"
                }
            ]
        },
        {
            "id": "59c90e24-9cf5-4bb0-8284-9f6fc8e913c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab7d01f1-ce66-43ae-96ed-7f3fce25aff8",
            "compositeImage": {
                "id": "51dedb91-39c8-4d71-8680-ea832eea0515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c90e24-9cf5-4bb0-8284-9f6fc8e913c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716b08d0-779c-4036-aba2-9ca88db03673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c90e24-9cf5-4bb0-8284-9f6fc8e913c2",
                    "LayerId": "1bcc208f-6dbb-4569-ba18-c042a7df7ed0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "1bcc208f-6dbb-4569-ba18-c042a7df7ed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab7d01f1-ce66-43ae-96ed-7f3fce25aff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}