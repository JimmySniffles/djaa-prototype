{
    "id": "4cd4d886-83bd-4193-93ae-df00019bbd04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_throw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "836a064f-4f0c-4cb3-a532-1e52f9cb1b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "193ce9e8-69f2-4c49-a0cb-60f3e3ffec53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "836a064f-4f0c-4cb3-a532-1e52f9cb1b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96a358fd-c725-488a-b399-8e6c481648a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "836a064f-4f0c-4cb3-a532-1e52f9cb1b1b",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        },
        {
            "id": "acfa77ae-83c9-4127-b99e-11422c274324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "07510788-e621-4c33-a3b8-e5089082c942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfa77ae-83c9-4127-b99e-11422c274324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d01014-4416-4882-826c-3ad0d6420c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfa77ae-83c9-4127-b99e-11422c274324",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        },
        {
            "id": "5340c0e9-cf20-4516-bc66-04bc19cc420c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "fd8f07f0-6194-441a-8f09-b250a638df18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5340c0e9-cf20-4516-bc66-04bc19cc420c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4bad048-d3c9-4da1-8091-d28bdd767296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5340c0e9-cf20-4516-bc66-04bc19cc420c",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        },
        {
            "id": "e792bc0a-3bbc-4e6a-9d88-a53b88752ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "6297fc26-d092-4685-97ed-a535702fc1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e792bc0a-3bbc-4e6a-9d88-a53b88752ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23620631-d3ae-4ca1-8799-bb73206a85e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e792bc0a-3bbc-4e6a-9d88-a53b88752ddf",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        },
        {
            "id": "620ecb23-0be0-4737-8e3e-025034fd9f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "f52d7fbc-18be-41d3-b707-fd2f5d87b52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "620ecb23-0be0-4737-8e3e-025034fd9f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f772e0-371b-41e3-81e2-70e67a2a360d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "620ecb23-0be0-4737-8e3e-025034fd9f99",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        },
        {
            "id": "b472a2b8-0c00-44cb-bff2-c2f9aa9e1386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "compositeImage": {
                "id": "fa6bb569-a395-4aaa-96d9-7d67b6b57b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b472a2b8-0c00-44cb-bff2-c2f9aa9e1386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a03d30-60e0-4b37-8bc9-1c443fbd7617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b472a2b8-0c00-44cb-bff2-c2f9aa9e1386",
                    "LayerId": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a7fdc63c-71d7-4de5-bae1-1ce05f575f02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cd4d886-83bd-4193-93ae-df00019bbd04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}