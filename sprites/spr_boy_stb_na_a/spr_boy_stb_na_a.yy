{
    "id": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_na_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "827e0db5-1a9b-4422-8851-1b6dd1360ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "05facaff-c5d3-4cdf-914d-a392bf83ccfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827e0db5-1a9b-4422-8851-1b6dd1360ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f727f3a0-f6ae-43f5-9df8-b17c228a5b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827e0db5-1a9b-4422-8851-1b6dd1360ce8",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "0bdcb832-ca7c-48c6-949b-7b1ca381162d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827e0db5-1a9b-4422-8851-1b6dd1360ce8",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        },
        {
            "id": "9f621bb9-0a9c-42ff-ab78-da14701b3d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "1a3e33f1-0608-4b5a-9758-a4dd34185519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f621bb9-0a9c-42ff-ab78-da14701b3d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c915d2-2160-4f2e-8e55-d94b7ce28141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f621bb9-0a9c-42ff-ab78-da14701b3d6e",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "52b09b64-9f94-4d44-a899-b33ff3602e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f621bb9-0a9c-42ff-ab78-da14701b3d6e",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        },
        {
            "id": "70182130-0725-4536-98c8-f16dafe61502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "060513ca-957d-4756-970a-5659691433c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70182130-0725-4536-98c8-f16dafe61502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f7c7d3-24e7-4387-86b2-212d4bd5355d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70182130-0725-4536-98c8-f16dafe61502",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "4858cc86-76dc-408a-9792-0dadda09801f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70182130-0725-4536-98c8-f16dafe61502",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        },
        {
            "id": "7a6f85f1-5a8b-44fb-8dd5-629017aafa19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "ff0e2a80-b944-41c2-a092-c7ee9f241491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a6f85f1-5a8b-44fb-8dd5-629017aafa19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272c4012-3d1b-4138-a42f-2a4814e62f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a6f85f1-5a8b-44fb-8dd5-629017aafa19",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "d6e51d8a-d05d-4c6b-a04f-ea6eb040f98f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a6f85f1-5a8b-44fb-8dd5-629017aafa19",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        },
        {
            "id": "538d459f-9902-4149-8a5b-cc330a0a6e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "20920820-5c4b-47ba-8589-4d8e10f058d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538d459f-9902-4149-8a5b-cc330a0a6e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c65b78-aba4-4389-8453-3f8b0079402d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538d459f-9902-4149-8a5b-cc330a0a6e25",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "95fcd7b0-6c89-4888-9cf8-2f7ef8570636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538d459f-9902-4149-8a5b-cc330a0a6e25",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        },
        {
            "id": "fea9a8d8-bea0-4f22-a73d-6d1bfdeb7eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "compositeImage": {
                "id": "a6955d24-f91c-4973-8cb1-ebac803ef2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea9a8d8-bea0-4f22-a73d-6d1bfdeb7eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29a7246e-e01e-4ec4-8f52-11c4f92fa02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea9a8d8-bea0-4f22-a73d-6d1bfdeb7eea",
                    "LayerId": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd"
                },
                {
                    "id": "6e3fa997-038d-43f4-bec5-2bab28d1ed3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea9a8d8-bea0-4f22-a73d-6d1bfdeb7eea",
                    "LayerId": "ad202cc0-e710-4d35-b41e-9d5926d06035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7eaf9e7-2f5e-43e6-b0ea-0bf95264cbcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ad202cc0-e710-4d35-b41e-9d5926d06035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fa46a12-f1c1-495c-a141-522fbf433d1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}