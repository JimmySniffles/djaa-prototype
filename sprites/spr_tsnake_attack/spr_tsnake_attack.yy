{
    "id": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tsnake_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd17782d-85da-4743-9f73-ff4bb626a258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "b2e1b404-6a62-42a6-a304-4557bae71a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd17782d-85da-4743-9f73-ff4bb626a258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb12ea4-f757-459e-ade4-a5829dd9f18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd17782d-85da-4743-9f73-ff4bb626a258",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "a5d0e0bb-6f8c-46a6-a11c-b5930f9e5c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "3447e110-4f0e-4834-8a0a-ed14e881f13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d0e0bb-6f8c-46a6-a11c-b5930f9e5c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6b2cba-969b-4ca1-9b67-a0c57e67f5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d0e0bb-6f8c-46a6-a11c-b5930f9e5c82",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "afa9230f-7fa3-4db1-9b87-51085291f491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "309a236c-e827-4b92-981e-d5556b915c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa9230f-7fa3-4db1-9b87-51085291f491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f493671d-3417-44b5-9e42-a2a14d23673b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa9230f-7fa3-4db1-9b87-51085291f491",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "f856af45-d812-4500-8d3f-2166590a7859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "e8b7e5c4-461d-4b4d-a538-7089cd0535ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f856af45-d812-4500-8d3f-2166590a7859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb15dd1-d852-4319-8469-6e5e3c29b5b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f856af45-d812-4500-8d3f-2166590a7859",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "2285c651-149a-44a7-9b96-d977f5fab090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "54e9b931-5c62-4876-912b-44064d518f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2285c651-149a-44a7-9b96-d977f5fab090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7933c6-0641-455c-a00a-5361337f4293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2285c651-149a-44a7-9b96-d977f5fab090",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "3cfeb90b-4fdf-49a5-a03c-d8dd080e0b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "e4e8410c-1920-4c3e-8595-2a2db2ec1b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfeb90b-4fdf-49a5-a03c-d8dd080e0b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71b2e7d7-c302-4521-8b96-9ebf41141a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfeb90b-4fdf-49a5-a03c-d8dd080e0b4b",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "7024bc5a-68db-4d0c-b6ff-17fec38772f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "1f743840-8d84-4aff-b3ed-439e8d91d1df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7024bc5a-68db-4d0c-b6ff-17fec38772f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be95e046-de1e-4986-8638-d0734b9e9948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7024bc5a-68db-4d0c-b6ff-17fec38772f5",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        },
        {
            "id": "c761464c-69ba-417f-bb70-e4e4d4048354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "compositeImage": {
                "id": "7309239a-1c36-4ce0-9f9d-96c3a714abaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c761464c-69ba-417f-bb70-e4e4d4048354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1560d25-d7ec-4e97-b89f-cd04719d5328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c761464c-69ba-417f-bb70-e4e4d4048354",
                    "LayerId": "8aee34d5-92c1-4355-951c-c19525e7723b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "8aee34d5-92c1-4355-951c-c19525e7723b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d45aa14-c4f8-4aad-b7a5-fd71804bec9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}