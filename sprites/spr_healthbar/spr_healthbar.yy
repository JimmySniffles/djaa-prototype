{
    "id": "c2640c08-8236-4919-8dba-5ceb032681a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fb183af-b4ba-410c-9c04-4d8fb82a826e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2640c08-8236-4919-8dba-5ceb032681a2",
            "compositeImage": {
                "id": "c8d0e83d-4c31-4547-a7d3-83a47d0f259e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb183af-b4ba-410c-9c04-4d8fb82a826e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7355ff6d-81b3-433e-87e1-f070c0f7ac4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb183af-b4ba-410c-9c04-4d8fb82a826e",
                    "LayerId": "096b821f-9911-4ede-8a31-cbc0d2babe43"
                }
            ]
        },
        {
            "id": "16f4e9fe-38e4-4d78-bac6-c31006245370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2640c08-8236-4919-8dba-5ceb032681a2",
            "compositeImage": {
                "id": "b4159030-1f9a-4205-aa7b-b1c1ef8be64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f4e9fe-38e4-4d78-bac6-c31006245370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e60c729-c22a-441a-8827-4d1c1bfcc754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f4e9fe-38e4-4d78-bac6-c31006245370",
                    "LayerId": "096b821f-9911-4ede-8a31-cbc0d2babe43"
                }
            ]
        },
        {
            "id": "ad8aa768-2367-4637-b8cf-86168040ab0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2640c08-8236-4919-8dba-5ceb032681a2",
            "compositeImage": {
                "id": "b26dc8f9-798e-4547-8637-6b502de5e8db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8aa768-2367-4637-b8cf-86168040ab0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23eb7e22-4d33-46cf-845e-396bcfd0c0ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8aa768-2367-4637-b8cf-86168040ab0e",
                    "LayerId": "096b821f-9911-4ede-8a31-cbc0d2babe43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "096b821f-9911-4ede-8a31-cbc0d2babe43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2640c08-8236-4919-8dba-5ceb032681a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}