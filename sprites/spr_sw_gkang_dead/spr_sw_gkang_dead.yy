{
    "id": "23173efa-e441-44a3-a62d-54aead45b7ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkang_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c7a4db3-8823-48f4-ac51-e8fb21b924d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23173efa-e441-44a3-a62d-54aead45b7ba",
            "compositeImage": {
                "id": "3b6a303d-ec65-4fbb-a5f0-2f27c419da96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7a4db3-8823-48f4-ac51-e8fb21b924d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095fc4c9-2e53-481f-99bc-6537bb52a4af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7a4db3-8823-48f4-ac51-e8fb21b924d8",
                    "LayerId": "b87529fd-ad18-4c97-9e4e-2a1975049ace"
                }
            ]
        },
        {
            "id": "f2c90bdf-2b63-4f79-97d4-b3629857891f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23173efa-e441-44a3-a62d-54aead45b7ba",
            "compositeImage": {
                "id": "3492be2c-f531-4af5-a3e6-c711353a2b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c90bdf-2b63-4f79-97d4-b3629857891f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c0eae6-b3e1-402b-bd07-9cd27679f180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c90bdf-2b63-4f79-97d4-b3629857891f",
                    "LayerId": "b87529fd-ad18-4c97-9e4e-2a1975049ace"
                }
            ]
        },
        {
            "id": "6855ea8b-5eb2-4926-a364-de388bd7dfd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23173efa-e441-44a3-a62d-54aead45b7ba",
            "compositeImage": {
                "id": "07486925-3c85-43a8-8470-194a59013467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6855ea8b-5eb2-4926-a364-de388bd7dfd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f88bbe-c39f-4d74-b11c-f69948b3d4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6855ea8b-5eb2-4926-a364-de388bd7dfd1",
                    "LayerId": "b87529fd-ad18-4c97-9e4e-2a1975049ace"
                }
            ]
        },
        {
            "id": "b2539ca4-60ee-4cf3-816e-c66150f9acd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23173efa-e441-44a3-a62d-54aead45b7ba",
            "compositeImage": {
                "id": "a7eb74e1-7415-4138-be0b-94f7527d5201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2539ca4-60ee-4cf3-816e-c66150f9acd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0424a470-6200-4f8b-94bf-0fedeb28d52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2539ca4-60ee-4cf3-816e-c66150f9acd9",
                    "LayerId": "b87529fd-ad18-4c97-9e4e-2a1975049ace"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "b87529fd-ad18-4c97-9e4e-2a1975049ace",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23173efa-e441-44a3-a62d-54aead45b7ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}