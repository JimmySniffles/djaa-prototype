{
    "id": "6235ffa9-0c9c-47d7-94c8-218b10354fd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_berrybushes_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 40,
    "bbox_right": 488,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa7b75fe-1a84-47f6-80e7-a32dc7a3fbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6235ffa9-0c9c-47d7-94c8-218b10354fd9",
            "compositeImage": {
                "id": "1cf40026-5722-410e-a6b2-bd3345da2d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7b75fe-1a84-47f6-80e7-a32dc7a3fbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ada39d-1c9a-41d8-b70e-f76c1af172ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7b75fe-1a84-47f6-80e7-a32dc7a3fbd5",
                    "LayerId": "16b3c479-80ab-46df-a52b-6147462fb4dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "16b3c479-80ab-46df-a52b-6147462fb4dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6235ffa9-0c9c-47d7-94c8-218b10354fd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}