{
    "id": "e190ec58-ffd7-4200-b8c3-747350490c81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83bef0dd-6800-4957-a42d-d582e38c3af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "4cb06965-878e-4f12-83b3-2f336c518ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83bef0dd-6800-4957-a42d-d582e38c3af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a03a2b7-8170-4c59-841c-eac6bcbc92c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83bef0dd-6800-4957-a42d-d582e38c3af0",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "6163d8fe-9e0d-4c43-972f-0d2e496dc488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "8745642c-8f99-4f6a-839b-cb29de0030a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6163d8fe-9e0d-4c43-972f-0d2e496dc488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb9b73eb-38df-4e43-b348-9ea4f2726aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6163d8fe-9e0d-4c43-972f-0d2e496dc488",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "16ee14f0-5e4e-4bb8-9d35-6ddff529ff38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "1c74bbf3-071e-4b03-9acd-c388ddc650b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16ee14f0-5e4e-4bb8-9d35-6ddff529ff38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e0a6d9-f7c8-4a81-9b79-a92de459046f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16ee14f0-5e4e-4bb8-9d35-6ddff529ff38",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "69fbaa44-5e4a-41f8-b5df-623e48abc107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "54f4cde4-4fd9-4af5-8574-97502b625d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69fbaa44-5e4a-41f8-b5df-623e48abc107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de035c79-5e79-46d4-9fb9-57afe36b3be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69fbaa44-5e4a-41f8-b5df-623e48abc107",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "e48176cc-f7db-4f6f-b0c6-278452e99ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "6512c506-9b6f-4ecd-a470-d07cf6a588e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48176cc-f7db-4f6f-b0c6-278452e99ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e620ad5-627c-490d-aa9c-deae518f403a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48176cc-f7db-4f6f-b0c6-278452e99ea6",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "9036e4ac-a20e-4cdb-9dd8-3dd7de9f3c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "a8761b31-b957-4823-944b-8013fdd38ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9036e4ac-a20e-4cdb-9dd8-3dd7de9f3c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6950e847-c188-4dc1-847d-f911371efef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9036e4ac-a20e-4cdb-9dd8-3dd7de9f3c60",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "44c71363-61cd-4e2f-a492-d033c7a5e622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "41e5cffa-07ab-495a-9d20-6c6ee6a8d0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c71363-61cd-4e2f-a492-d033c7a5e622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a3d202-287a-4e87-b946-ef9d8ca6bd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c71363-61cd-4e2f-a492-d033c7a5e622",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "c5209120-9cf9-4727-93ae-c6f426fa189c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "860e9f2a-5dfb-4115-b80b-9504e81b1cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5209120-9cf9-4727-93ae-c6f426fa189c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e55995-fe14-46c1-ae6a-04ba05e9e48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5209120-9cf9-4727-93ae-c6f426fa189c",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "7617e664-7a01-426f-914f-6db28733779a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "bd00eb2b-8cb4-4aaa-bac5-40c802fd8817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7617e664-7a01-426f-914f-6db28733779a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5ea8ac-ae40-4fbe-bada-2328dae8e651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7617e664-7a01-426f-914f-6db28733779a",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "692c31b8-3b52-484d-a608-50c6deafa939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "5426c4a3-3998-46da-b094-08464d584b6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692c31b8-3b52-484d-a608-50c6deafa939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e032c0f1-d3db-4e70-af5e-4181de5b0adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692c31b8-3b52-484d-a608-50c6deafa939",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "74672fc6-118c-43e5-8aea-7d8e98ab2dc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "3892a79f-2817-47aa-b667-ba13088c70bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74672fc6-118c-43e5-8aea-7d8e98ab2dc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3667bc7b-4d11-425b-b9c7-161498b0f343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74672fc6-118c-43e5-8aea-7d8e98ab2dc6",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "7a6bae54-b4b4-44dc-9271-c6fcd05ec5e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "be5af2ad-a9fa-4e6c-8336-15447ad24cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a6bae54-b4b4-44dc-9271-c6fcd05ec5e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "486ee469-2962-4dec-8a89-68742a09e358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a6bae54-b4b4-44dc-9271-c6fcd05ec5e4",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "08b037b3-a186-472e-b22b-aa211019c714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "519f55aa-6882-42fa-bf1b-c868d56a5cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08b037b3-a186-472e-b22b-aa211019c714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16dc2519-5c97-497f-b009-800ce43793a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08b037b3-a186-472e-b22b-aa211019c714",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "35a72058-64c9-4d32-a848-c68bc40cccf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "5abdf966-6201-492a-a815-ce64c9cbe0c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a72058-64c9-4d32-a848-c68bc40cccf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "963a511a-300b-459b-8a59-b1290e9c70ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a72058-64c9-4d32-a848-c68bc40cccf5",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "31dfabfb-1986-4e16-a048-fcde3263e7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "72745573-691c-4625-886b-27a8eb109ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31dfabfb-1986-4e16-a048-fcde3263e7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e2f584-eef8-4d6a-8f2c-2dd791b96125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31dfabfb-1986-4e16-a048-fcde3263e7d0",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        },
        {
            "id": "6aad152f-8c09-49fd-a5dd-c626d17184ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "compositeImage": {
                "id": "c82f4e31-632b-4388-8df0-f8b7c43341f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aad152f-8c09-49fd-a5dd-c626d17184ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7680ec61-c0ad-4254-9706-4247f4d33f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aad152f-8c09-49fd-a5dd-c626d17184ce",
                    "LayerId": "07d593e0-1678-4be2-89d0-5366d2debfd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "07d593e0-1678-4be2-89d0-5366d2debfd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e190ec58-ffd7-4200-b8c3-747350490c81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}