{
    "id": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8270dc52-28d6-4386-acd0-36bccdd4cab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "254bcd79-2c87-42d5-9fe8-d14df454916b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8270dc52-28d6-4386-acd0-36bccdd4cab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a46988a-6a7f-4775-8a17-675816b8438e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8270dc52-28d6-4386-acd0-36bccdd4cab7",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "805c9f98-3808-4d36-9720-c0d0851bcf16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "94c8cd7c-580b-4766-a592-3378935a1c5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "805c9f98-3808-4d36-9720-c0d0851bcf16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb4cbdd-ade2-4353-902b-7732c4c078fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "805c9f98-3808-4d36-9720-c0d0851bcf16",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "9be34cf2-93f2-41a1-a7d0-149597899a15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "934d4671-551d-4fda-a9b3-07e638dfaf69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be34cf2-93f2-41a1-a7d0-149597899a15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc316ae-4710-449d-83a5-8a3ca1d10220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be34cf2-93f2-41a1-a7d0-149597899a15",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "c69714a4-f671-4ffe-9411-a89da92d80ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "f9ab0701-222c-47da-a1bd-1bd99e74e034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c69714a4-f671-4ffe-9411-a89da92d80ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d72854-3ce3-47f2-8be8-cd6b52d2dac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c69714a4-f671-4ffe-9411-a89da92d80ff",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "b7d03c92-ee42-46a0-92f9-25b10dfbd413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "1c173f2f-94fd-406a-9f51-0cc2f1da4041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d03c92-ee42-46a0-92f9-25b10dfbd413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae59a497-326b-4975-934d-bea743039cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d03c92-ee42-46a0-92f9-25b10dfbd413",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "0dea9c44-d7b9-4a2c-b34c-c6ab8a400b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "62e41ad4-1a95-4724-bd8c-be9ec094ffb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dea9c44-d7b9-4a2c-b34c-c6ab8a400b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb7a562-a799-4afc-9d70-9cce080fcbbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dea9c44-d7b9-4a2c-b34c-c6ab8a400b5c",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "c3f30f3b-a5f6-4999-bef0-a4429d7db765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "55c8c6c3-efac-4585-ade2-b1bfc07a0ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f30f3b-a5f6-4999-bef0-a4429d7db765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c8b680-6b8a-4ca1-9838-d75c31b0f8ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f30f3b-a5f6-4999-bef0-a4429d7db765",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "2568ca24-309d-413f-ac73-a1393d533238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "a2054cc6-8f05-452e-b35b-661712b36684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2568ca24-309d-413f-ac73-a1393d533238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03823024-82d3-4592-bf20-c279583b74db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2568ca24-309d-413f-ac73-a1393d533238",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "5a621194-ade8-4b94-9d8e-9d4737845745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "00b42086-164c-4f8a-b806-6a7fc6294354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a621194-ade8-4b94-9d8e-9d4737845745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d01a41cd-e8a6-41c8-bbeb-c4a2759afa4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a621194-ade8-4b94-9d8e-9d4737845745",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "49dc0cd7-f677-438f-9f0a-93623b1eaf10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "0da2831a-60f9-42fb-bfcc-b00c8991247b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49dc0cd7-f677-438f-9f0a-93623b1eaf10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b946b69f-1b4b-4b24-80b7-dcfa6081b6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49dc0cd7-f677-438f-9f0a-93623b1eaf10",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "d069a796-edf0-4301-a4f2-4c9875316845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "0979449b-1aa3-4ce0-9e4b-737a25e5111d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d069a796-edf0-4301-a4f2-4c9875316845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d68acabf-b8a4-48ef-b74c-d906981bb532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d069a796-edf0-4301-a4f2-4c9875316845",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "291e9dad-2c0e-480a-b85b-48769ebc5f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "59294fe3-1593-4a83-bfb9-2ef7bef534e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "291e9dad-2c0e-480a-b85b-48769ebc5f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c196d3-93b3-44f7-9383-da46b1d13cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "291e9dad-2c0e-480a-b85b-48769ebc5f70",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "d3f2c4a2-fcdd-463d-9f41-6ca7a9d8fabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "17a63778-c801-4208-9ca0-143213108737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3f2c4a2-fcdd-463d-9f41-6ca7a9d8fabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90852f73-f832-43bb-bf3e-be78e4e443f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3f2c4a2-fcdd-463d-9f41-6ca7a9d8fabd",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "2aa1d41e-dae8-45b4-a3a5-28bf27f0e47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "98e4dd15-fd29-42d0-96d8-6a869c0f3ec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa1d41e-dae8-45b4-a3a5-28bf27f0e47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a46fe9-b5c3-4523-aee3-4417937e9beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa1d41e-dae8-45b4-a3a5-28bf27f0e47b",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "934646c8-87e1-408e-99b2-a82ff2217144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "eb028976-6f3d-47cb-b309-4df8f9c7c9c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934646c8-87e1-408e-99b2-a82ff2217144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c5fcdc-ca1b-449d-be66-6a8bbf773864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934646c8-87e1-408e-99b2-a82ff2217144",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "3030b934-325b-47d2-a39d-c22e6b7eac07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "57d11dc4-8824-4d1a-b3ba-bb52ef530ef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3030b934-325b-47d2-a39d-c22e6b7eac07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6036b4bc-53d1-4f23-910b-f3a526df3ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3030b934-325b-47d2-a39d-c22e6b7eac07",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "5565b542-af3f-4744-8351-259eb53ceb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "1a723410-6cf4-40e6-8142-299c2662e99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5565b542-af3f-4744-8351-259eb53ceb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8692c7de-6e64-49b7-bc29-1dfb4ce8e621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5565b542-af3f-4744-8351-259eb53ceb46",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        },
        {
            "id": "a2db183c-b31a-40f8-88aa-7a544f3efeb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "compositeImage": {
                "id": "99165980-7f79-49c2-bf30-fc898c12d4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2db183c-b31a-40f8-88aa-7a544f3efeb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a5eb5e-b8df-4ff5-b7b4-f7f55c665959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2db183c-b31a-40f8-88aa-7a544f3efeb0",
                    "LayerId": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fd588de5-5ac3-4739-ac69-a5ff9ce3a3c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43a30ce2-c66d-4728-a872-c3d44d55d83c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}