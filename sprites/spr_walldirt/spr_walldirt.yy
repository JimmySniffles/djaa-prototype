{
    "id": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walldirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3720beed-bb90-44aa-8787-fe3caf296f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
            "compositeImage": {
                "id": "62ed1d6d-8244-4e76-a43b-3ff6ad20bdd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3720beed-bb90-44aa-8787-fe3caf296f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbcaf5d5-1787-41a6-82c6-8e730647ca3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3720beed-bb90-44aa-8787-fe3caf296f9f",
                    "LayerId": "09be5884-d973-49d8-85b6-8dc8e4d45d40"
                }
            ]
        },
        {
            "id": "dd799f67-7b48-4ce8-820a-f9b9a385dc72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
            "compositeImage": {
                "id": "fc24a427-2a16-41bb-b652-c8cf1b504cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd799f67-7b48-4ce8-820a-f9b9a385dc72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92551cc0-b115-48db-9fa7-c2680b2dab78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd799f67-7b48-4ce8-820a-f9b9a385dc72",
                    "LayerId": "09be5884-d973-49d8-85b6-8dc8e4d45d40"
                }
            ]
        },
        {
            "id": "b31ea5ad-51aa-4ed8-8e8a-dfef0b8b188d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
            "compositeImage": {
                "id": "0ccbd102-7df1-4ae8-934d-756f6e4b96a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31ea5ad-51aa-4ed8-8e8a-dfef0b8b188d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750ea472-167c-47c1-930a-5fd4895c2524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31ea5ad-51aa-4ed8-8e8a-dfef0b8b188d",
                    "LayerId": "09be5884-d973-49d8-85b6-8dc8e4d45d40"
                }
            ]
        },
        {
            "id": "a4d6ca94-06f7-4888-b037-76cfe4d42915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
            "compositeImage": {
                "id": "00750669-d999-40f9-90b4-250d5be39129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d6ca94-06f7-4888-b037-76cfe4d42915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2fe387-6e32-4f7d-8dfe-901d6d35891b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d6ca94-06f7-4888-b037-76cfe4d42915",
                    "LayerId": "09be5884-d973-49d8-85b6-8dc8e4d45d40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "09be5884-d973-49d8-85b6-8dc8e4d45d40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c13cd19-1c7d-4e22-9fa6-ca8bf0fb8228",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}