{
    "id": "a419a5cb-163c-40de-9a1c-2453a30c60fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boomerang_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffa185fb-b438-4d9c-9383-5672ce651476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a419a5cb-163c-40de-9a1c-2453a30c60fd",
            "compositeImage": {
                "id": "d2d20048-9f58-43fb-8cab-87a541042c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa185fb-b438-4d9c-9383-5672ce651476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ecbde2-853e-4693-98d4-1d32db78a4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa185fb-b438-4d9c-9383-5672ce651476",
                    "LayerId": "1e471372-8ea7-491a-9633-575955af4d05"
                }
            ]
        },
        {
            "id": "a7b43bb5-91d1-4582-a73e-82b48bea7a13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a419a5cb-163c-40de-9a1c-2453a30c60fd",
            "compositeImage": {
                "id": "a86b5bc4-497b-4577-b5aa-b1636d8f87ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b43bb5-91d1-4582-a73e-82b48bea7a13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46254963-ced4-4840-83bc-834cd804c764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b43bb5-91d1-4582-a73e-82b48bea7a13",
                    "LayerId": "1e471372-8ea7-491a-9633-575955af4d05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "1e471372-8ea7-491a-9633-575955af4d05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a419a5cb-163c-40de-9a1c-2453a30c60fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}