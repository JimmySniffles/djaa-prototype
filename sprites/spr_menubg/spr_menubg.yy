{
    "id": "92f6c239-f7b5-487c-b84a-be850234cf9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menubg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb118a8c-b7ce-46bd-b838-29ef80fda5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92f6c239-f7b5-487c-b84a-be850234cf9e",
            "compositeImage": {
                "id": "b246ff56-3faa-4ff3-a4d8-1f47c6380c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb118a8c-b7ce-46bd-b838-29ef80fda5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121f8b73-083a-402b-990a-64892a7a8812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb118a8c-b7ce-46bd-b838-29ef80fda5c0",
                    "LayerId": "e427192e-e230-4f72-a3c4-f848520c0a6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "e427192e-e230-4f72-a3c4-f848520c0a6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92f6c239-f7b5-487c-b84a-be850234cf9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}