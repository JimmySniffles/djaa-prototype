{
    "id": "5dd1192d-058b-4205-a618-27a1601f3a69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_acknowledgement",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 371,
    "bbox_right": 1544,
    "bbox_top": 255,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6e3cbe3-d11b-4ab5-9567-d231cf3d2d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd1192d-058b-4205-a618-27a1601f3a69",
            "compositeImage": {
                "id": "f6566dab-fb51-46db-995e-db66bbc72915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e3cbe3-d11b-4ab5-9567-d231cf3d2d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46320144-007c-42e9-a20d-d9c701de49b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e3cbe3-d11b-4ab5-9567-d231cf3d2d42",
                    "LayerId": "a4d0b94f-553f-4116-bf3e-470fa7c36e57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "a4d0b94f-553f-4116-bf3e-470fa7c36e57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dd1192d-058b-4205-a618-27a1601f3a69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}