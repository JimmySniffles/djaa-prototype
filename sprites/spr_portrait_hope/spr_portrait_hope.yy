{
    "id": "83146b0d-0564-4844-a7e2-843f9e37e16d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_hope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 13,
    "bbox_right": 98,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "097708b6-3f10-4ee1-90e2-cae2ef4f52b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83146b0d-0564-4844-a7e2-843f9e37e16d",
            "compositeImage": {
                "id": "c97b9b33-c1b2-44b7-8097-d1b88cc49822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097708b6-3f10-4ee1-90e2-cae2ef4f52b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7e4311-30bb-4a37-877c-e4153367acdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097708b6-3f10-4ee1-90e2-cae2ef4f52b8",
                    "LayerId": "73dd145e-dc99-4ae0-92c4-0372fd97d38c"
                }
            ]
        },
        {
            "id": "fc06412c-7789-4b7d-b359-8add4b587970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83146b0d-0564-4844-a7e2-843f9e37e16d",
            "compositeImage": {
                "id": "d212b20c-54e6-4a7a-86f1-183fb134d952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc06412c-7789-4b7d-b359-8add4b587970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57573b3c-181d-42f7-9288-7830fe267bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc06412c-7789-4b7d-b359-8add4b587970",
                    "LayerId": "73dd145e-dc99-4ae0-92c4-0372fd97d38c"
                }
            ]
        },
        {
            "id": "9ed7735e-4c90-4a1f-812f-e399567a3f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83146b0d-0564-4844-a7e2-843f9e37e16d",
            "compositeImage": {
                "id": "0549be2e-9652-45b4-b563-358d32f03c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ed7735e-4c90-4a1f-812f-e399567a3f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a07c70-ec8c-4a4a-9bce-14799d005bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed7735e-4c90-4a1f-812f-e399567a3f5b",
                    "LayerId": "73dd145e-dc99-4ae0-92c4-0372fd97d38c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "73dd145e-dc99-4ae0-92c4-0372fd97d38c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83146b0d-0564-4844-a7e2-843f9e37e16d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}