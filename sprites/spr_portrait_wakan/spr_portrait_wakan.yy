{
    "id": "e30e0240-2ac2-4b63-839d-fc2a6f5c1ecf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_wakan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa12f8af-7928-4a37-9875-0fd9ea692349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e30e0240-2ac2-4b63-839d-fc2a6f5c1ecf",
            "compositeImage": {
                "id": "6212d794-8727-4b36-a50f-3b58838ad607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa12f8af-7928-4a37-9875-0fd9ea692349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e95274b-b8cb-4739-ab2a-99e5ed5c1f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa12f8af-7928-4a37-9875-0fd9ea692349",
                    "LayerId": "bba82811-107f-473f-8d75-4e6afd2bb4d6"
                }
            ]
        },
        {
            "id": "35c8d0b2-b31c-41f3-9408-bfef86e52a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e30e0240-2ac2-4b63-839d-fc2a6f5c1ecf",
            "compositeImage": {
                "id": "147a64c0-e46d-4169-8975-fc74ea5cc677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c8d0b2-b31c-41f3-9408-bfef86e52a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "366306e6-e570-4fda-a1b3-81da1c46f361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c8d0b2-b31c-41f3-9408-bfef86e52a07",
                    "LayerId": "bba82811-107f-473f-8d75-4e6afd2bb4d6"
                }
            ]
        },
        {
            "id": "4293fab9-9632-494d-8506-1e76e355c199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e30e0240-2ac2-4b63-839d-fc2a6f5c1ecf",
            "compositeImage": {
                "id": "73856c74-eb69-45a7-bdcb-b8df7f7ab51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4293fab9-9632-494d-8506-1e76e355c199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0707887-c452-4e72-a57c-6e28b65b6114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4293fab9-9632-494d-8506-1e76e355c199",
                    "LayerId": "bba82811-107f-473f-8d75-4e6afd2bb4d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "bba82811-107f-473f-8d75-4e6afd2bb4d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e30e0240-2ac2-4b63-839d-fc2a6f5c1ecf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}