{
    "id": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1cedbb8-0e27-4c49-8e39-43db09a32fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "153b1242-450b-4acc-a56f-1ea29b7b737d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1cedbb8-0e27-4c49-8e39-43db09a32fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0c6efd-b426-4440-a31d-0942e51a0ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1cedbb8-0e27-4c49-8e39-43db09a32fde",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "d20a1c56-0c10-4f6e-8198-661ff43fb699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "33f9d97c-bd4d-47cf-be80-a1faaaa1b688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20a1c56-0c10-4f6e-8198-661ff43fb699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ede1899-7fbe-463b-aed4-d382654b5fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20a1c56-0c10-4f6e-8198-661ff43fb699",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "0a974ee1-2e2f-4d9d-8eac-396693b81f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "364daca9-30b2-4726-9425-d6e5c9c25f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a974ee1-2e2f-4d9d-8eac-396693b81f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ced795-8ae6-4b9a-808d-648919236b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a974ee1-2e2f-4d9d-8eac-396693b81f88",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "1716b871-90ed-4b2e-91cf-e53fa5f2b881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "fd9be228-74a7-4201-9ec9-0b0b29a99ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1716b871-90ed-4b2e-91cf-e53fa5f2b881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200d0c73-dffd-4884-96a0-245f9aa2d512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1716b871-90ed-4b2e-91cf-e53fa5f2b881",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "d14481ac-2f62-4e9d-836f-4a90a60a923c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "21345087-4035-4da8-8bed-651e5c0421a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14481ac-2f62-4e9d-836f-4a90a60a923c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6a8bbdd-8535-40a2-8f7a-e8306d432233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14481ac-2f62-4e9d-836f-4a90a60a923c",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "2b7421a6-b592-48f7-a26c-9ef9a8d59375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "4a1a6c72-f49a-4f60-838b-79ab6d4df037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7421a6-b592-48f7-a26c-9ef9a8d59375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa598db9-436a-468b-9e74-c8b7ca7409e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7421a6-b592-48f7-a26c-9ef9a8d59375",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "7ca1e4e9-f2dd-40b7-ad81-0af1d1efb4e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "eb42ac12-54e6-4afe-bff2-99ef4b490721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca1e4e9-f2dd-40b7-ad81-0af1d1efb4e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa89cbb2-9be5-4179-9c40-73e2c03ecc4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca1e4e9-f2dd-40b7-ad81-0af1d1efb4e4",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "52d58ea2-8990-4788-b7fb-7e2793ca2f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "683373cf-1139-4824-b55f-25c43f01859f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d58ea2-8990-4788-b7fb-7e2793ca2f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc714e8-7761-4cc9-b60f-344129d814e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d58ea2-8990-4788-b7fb-7e2793ca2f2c",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "7f84e847-c7e9-4228-b10f-9e4bbcedc440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "d4aa8fbb-5a1b-43b9-bd11-95e0592f6958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f84e847-c7e9-4228-b10f-9e4bbcedc440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94ef4b4-183e-4b20-8b49-e09d9bb3a17f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f84e847-c7e9-4228-b10f-9e4bbcedc440",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "63cd3ad4-ad44-48f4-a0c0-8ac795c8f97f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "3da2af13-8423-45d5-9194-e49e993c35df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63cd3ad4-ad44-48f4-a0c0-8ac795c8f97f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f45d78c-6776-4e63-b3e0-000a2ab9604b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63cd3ad4-ad44-48f4-a0c0-8ac795c8f97f",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "e6662966-ff44-4d65-92ec-4e9ac6d1b429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "0498526a-1d19-4683-8f84-f2897b791630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6662966-ff44-4d65-92ec-4e9ac6d1b429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcad96f8-d6c2-4483-8777-be752e87b517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6662966-ff44-4d65-92ec-4e9ac6d1b429",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "417169ad-483f-42af-8370-2ac29fc9e4ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "2e45d0f4-f058-4709-8d40-6633b7ddb9f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417169ad-483f-42af-8370-2ac29fc9e4ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b888f714-e3b8-44f1-af89-bab9e5239098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417169ad-483f-42af-8370-2ac29fc9e4ae",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "40753dc1-5d7c-49e5-9f0d-711d67b48f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "0da2c610-3a5c-4895-a957-e69e3fbb76db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40753dc1-5d7c-49e5-9f0d-711d67b48f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a38a8b1-2ce7-43e3-82b3-3e65672561b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40753dc1-5d7c-49e5-9f0d-711d67b48f77",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "55ad525d-cdfb-4896-a897-1b08df6909ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "1c983543-c3fb-44df-8366-d8737fcf61ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ad525d-cdfb-4896-a897-1b08df6909ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b523797-d1aa-4cac-9678-32765cae227e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ad525d-cdfb-4896-a897-1b08df6909ba",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "a3a22000-006c-4f85-8e13-e8af0d949da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "1d520658-9fc7-405c-b2b7-eb9e0b005a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a22000-006c-4f85-8e13-e8af0d949da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd49134e-cf59-4265-8213-a3b0a18925f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a22000-006c-4f85-8e13-e8af0d949da2",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        },
        {
            "id": "60580e01-2a26-4e23-91ba-92c24479236c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "compositeImage": {
                "id": "80770cc8-ffaf-4a63-a19e-b3f6e448cd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60580e01-2a26-4e23-91ba-92c24479236c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6151a9e2-e524-476b-938d-6ea54f656753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60580e01-2a26-4e23-91ba-92c24479236c",
                    "LayerId": "b28e4b34-c387-4321-94f8-bed81833268d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b28e4b34-c387-4321-94f8-bed81833268d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2f75f8d-dff6-46d1-8d44-5ef74a9772a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}