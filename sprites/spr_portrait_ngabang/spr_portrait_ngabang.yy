{
    "id": "a36170e8-1849-41b5-a623-f82ad37fc64a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_ngabang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 7,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c1f1e70-a0ba-4c53-ad82-80232be2bcfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36170e8-1849-41b5-a623-f82ad37fc64a",
            "compositeImage": {
                "id": "b9fce819-4957-4f31-82b6-202b44ac2022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1f1e70-a0ba-4c53-ad82-80232be2bcfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acaab587-e7cd-4cc6-a97d-7a2cf123ba52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1f1e70-a0ba-4c53-ad82-80232be2bcfb",
                    "LayerId": "151e5631-1778-47c8-a241-16e8975961ce"
                }
            ]
        },
        {
            "id": "ae709a78-bc31-46f5-b4cb-5b3315db224d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36170e8-1849-41b5-a623-f82ad37fc64a",
            "compositeImage": {
                "id": "6d9e1647-fe32-4b53-9dc1-a91f655ce31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae709a78-bc31-46f5-b4cb-5b3315db224d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4f890f-a1ba-43a4-aa77-b2d1750c897c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae709a78-bc31-46f5-b4cb-5b3315db224d",
                    "LayerId": "151e5631-1778-47c8-a241-16e8975961ce"
                }
            ]
        },
        {
            "id": "ae64adae-61eb-4c15-bb36-83fd79f0add1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36170e8-1849-41b5-a623-f82ad37fc64a",
            "compositeImage": {
                "id": "06b2be4b-b670-44fb-aa73-913cdf8c8e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae64adae-61eb-4c15-bb36-83fd79f0add1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7366e143-ec1b-45a3-a40c-3f02709fa0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae64adae-61eb-4c15-bb36-83fd79f0add1",
                    "LayerId": "151e5631-1778-47c8-a241-16e8975961ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "151e5631-1778-47c8-a241-16e8975961ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a36170e8-1849-41b5-a623-f82ad37fc64a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}