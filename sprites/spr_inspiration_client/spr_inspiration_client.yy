{
    "id": "c1375793-1c82-4033-934e-8f96ff6e688f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inspiration_client",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 628,
    "bbox_left": 292,
    "bbox_right": 1631,
    "bbox_top": 292,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a81c7015-368b-4d36-88d6-ce007bbb0a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1375793-1c82-4033-934e-8f96ff6e688f",
            "compositeImage": {
                "id": "84044358-c447-4740-a092-aad055a19a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81c7015-368b-4d36-88d6-ce007bbb0a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5eb03b6-e790-40b4-b6b0-d90c17438abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81c7015-368b-4d36-88d6-ce007bbb0a56",
                    "LayerId": "aa475b91-c4cc-42cf-a38d-1d989d08fd65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "aa475b91-c4cc-42cf-a38d-1d989d08fd65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1375793-1c82-4033-934e-8f96ff6e688f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}