{
    "id": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rbsnake_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67797eaf-0b9d-4b18-971d-b96688b8edc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "9336f885-99ed-4451-bbe4-854e2d0146bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67797eaf-0b9d-4b18-971d-b96688b8edc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89dac51f-b02d-4bea-b169-a62c9cbc56c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67797eaf-0b9d-4b18-971d-b96688b8edc0",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "66c55232-cf3d-4582-82d6-dce4f334219a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "c70fb9b8-1844-4273-b504-12ce1ac7e757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c55232-cf3d-4582-82d6-dce4f334219a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0fc9d1-1a01-4a89-a5b0-b00903539b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c55232-cf3d-4582-82d6-dce4f334219a",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "c2039fb2-e36f-4634-8ead-59ba0e43d9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "906f2afa-7471-4e5d-a7c2-4dd2e3e851a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2039fb2-e36f-4634-8ead-59ba0e43d9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cbcb51-5712-4055-914c-3a2671faaad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2039fb2-e36f-4634-8ead-59ba0e43d9b6",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "09ade689-5f58-42e8-8bf9-eff2c1b5c84e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "d9ecadb1-a93c-42ed-b6a8-53488398f082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ade689-5f58-42e8-8bf9-eff2c1b5c84e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac1489e7-904b-4e22-826d-18cf1cd233f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ade689-5f58-42e8-8bf9-eff2c1b5c84e",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "585f4652-4552-49b6-90ca-a83a7cdaca61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "7244bf92-0acb-42b9-9485-de56fe07eba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585f4652-4552-49b6-90ca-a83a7cdaca61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc03656-16da-4d7c-85c4-5ad0d9445ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585f4652-4552-49b6-90ca-a83a7cdaca61",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "091d0a5d-bf96-4d3f-b923-32342d5c3bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "c6330972-be4a-4f9c-8301-43a3d1998d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091d0a5d-bf96-4d3f-b923-32342d5c3bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b640aa2-d43c-4050-a1d1-5d2e45d7b3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091d0a5d-bf96-4d3f-b923-32342d5c3bd8",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "19548494-b5a6-4ff8-81bb-686099e7be09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "dd41e419-c402-476f-bb94-c78c57560d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19548494-b5a6-4ff8-81bb-686099e7be09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d462ae29-3717-43e2-a5ad-579da900957f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19548494-b5a6-4ff8-81bb-686099e7be09",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        },
        {
            "id": "70e62fa6-17cf-49fb-9419-b666afd49b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "compositeImage": {
                "id": "35ba1e72-1d27-458e-9f2a-0703cd2d7f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e62fa6-17cf-49fb-9419-b666afd49b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3755095d-150a-4659-a566-851050a121ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e62fa6-17cf-49fb-9419-b666afd49b5f",
                    "LayerId": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "eea65dad-a143-4f4f-86eb-433a4f6e9d1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7888ffe-ec56-4492-b40f-0bc0c778a74a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}