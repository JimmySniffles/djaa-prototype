{
    "id": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weaponpickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 30,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d761cd57-1932-4b3d-b608-df541c5d4178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
            "compositeImage": {
                "id": "fe6a37bc-d3df-46a7-8736-e98f2861164f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d761cd57-1932-4b3d-b608-df541c5d4178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9007393a-82b9-41d5-81d1-5a6fcd4d887d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d761cd57-1932-4b3d-b608-df541c5d4178",
                    "LayerId": "a73b1ac2-6ebc-4f43-a2ab-693e36d03e2d"
                }
            ]
        },
        {
            "id": "fb7f30b1-e03a-4080-a5f5-42b0a0bfa6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
            "compositeImage": {
                "id": "02333f83-2f79-469f-bd1d-cc44b451b6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7f30b1-e03a-4080-a5f5-42b0a0bfa6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5420a8ea-446c-4aeb-b2ef-38a90a16d11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7f30b1-e03a-4080-a5f5-42b0a0bfa6cf",
                    "LayerId": "a73b1ac2-6ebc-4f43-a2ab-693e36d03e2d"
                }
            ]
        },
        {
            "id": "2f4582cf-4c11-4b50-a378-6686126f8086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
            "compositeImage": {
                "id": "0259c9aa-41ec-4e4d-ab09-5779613b0a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4582cf-4c11-4b50-a378-6686126f8086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a21ba7c-708b-4c55-81b6-b0235e5c7f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4582cf-4c11-4b50-a378-6686126f8086",
                    "LayerId": "a73b1ac2-6ebc-4f43-a2ab-693e36d03e2d"
                }
            ]
        },
        {
            "id": "0d15e7a7-f9bf-401a-bdec-00210945242f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
            "compositeImage": {
                "id": "d40ffada-dfe0-4a65-ad41-8ec62b34ecdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d15e7a7-f9bf-401a-bdec-00210945242f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7197f710-2f63-4ce3-9316-b09a6886151b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d15e7a7-f9bf-401a-bdec-00210945242f",
                    "LayerId": "a73b1ac2-6ebc-4f43-a2ab-693e36d03e2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a73b1ac2-6ebc-4f43-a2ab-693e36d03e2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32d7bc9f-df35-4649-8347-6d6bfac7a35b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}