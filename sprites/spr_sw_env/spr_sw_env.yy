{
    "id": "02e03a1a-1ebb-4253-8943-a1cbe8c35b4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_env",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 5,
    "bbox_right": 483,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a18ea7ba-b8b1-4e58-92f2-d978ed67a092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02e03a1a-1ebb-4253-8943-a1cbe8c35b4b",
            "compositeImage": {
                "id": "efc7a846-95d3-467e-9df5-b811056507f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18ea7ba-b8b1-4e58-92f2-d978ed67a092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302901bf-d95a-449b-a8f0-5faafd7d57b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18ea7ba-b8b1-4e58-92f2-d978ed67a092",
                    "LayerId": "7bdfcf96-07a9-456c-886a-9dd30949c643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "7bdfcf96-07a9-456c-886a-9dd30949c643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02e03a1a-1ebb-4253-8943-a1cbe8c35b4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}