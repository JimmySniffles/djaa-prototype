{
    "id": "ef400f2d-6527-4eef-ad43-5020c14be3e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bsnake_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95624f12-3d6d-4a19-a398-33b73afba255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef400f2d-6527-4eef-ad43-5020c14be3e2",
            "compositeImage": {
                "id": "ff62eb70-0458-4f2a-b43d-bc28ff5f8bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95624f12-3d6d-4a19-a398-33b73afba255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5e990b-4d28-43bd-9426-3e6ebd48cb24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95624f12-3d6d-4a19-a398-33b73afba255",
                    "LayerId": "eb7419d8-bc61-4023-b030-752b7238e710"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "eb7419d8-bc61-4023-b030-752b7238e710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef400f2d-6527-4eef-ad43-5020c14be3e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}