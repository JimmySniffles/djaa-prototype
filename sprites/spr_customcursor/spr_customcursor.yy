{
    "id": "f8ed15fa-64b2-4795-bb75-3e3be04dfc9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_customcursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e5936ce-b0c6-4a10-b333-2c675045d2ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ed15fa-64b2-4795-bb75-3e3be04dfc9c",
            "compositeImage": {
                "id": "d5642d78-d808-4a5b-8e8a-bed55e195c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5936ce-b0c6-4a10-b333-2c675045d2ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdb8fb7-f5e1-454e-99db-853858ef32e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5936ce-b0c6-4a10-b333-2c675045d2ae",
                    "LayerId": "510f6b08-70ab-4adc-aad8-eda43538bfca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "510f6b08-70ab-4adc-aad8-eda43538bfca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8ed15fa-64b2-4795-bb75-3e3be04dfc9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 6
}