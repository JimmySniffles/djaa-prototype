{
    "id": "b11ebdb5-b290-436a-b914-6767dd367623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkoala",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afb2617b-5a9a-49ac-a4ae-e5ea4dbe2a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b11ebdb5-b290-436a-b914-6767dd367623",
            "compositeImage": {
                "id": "072cb2e7-d815-4330-9051-782daa1c8ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb2617b-5a9a-49ac-a4ae-e5ea4dbe2a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48487fc-8f69-44ad-bd07-80888f30a47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb2617b-5a9a-49ac-a4ae-e5ea4dbe2a48",
                    "LayerId": "3f113d8b-d4d6-4de0-ab34-3ace60619625"
                }
            ]
        },
        {
            "id": "ca80f381-ede3-4400-a88c-e5395853e7fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b11ebdb5-b290-436a-b914-6767dd367623",
            "compositeImage": {
                "id": "a6c6f96c-dddf-4a02-8574-b85ddd9267d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca80f381-ede3-4400-a88c-e5395853e7fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d124265-89f0-4a66-be06-85c8dd9db775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca80f381-ede3-4400-a88c-e5395853e7fa",
                    "LayerId": "3f113d8b-d4d6-4de0-ab34-3ace60619625"
                }
            ]
        },
        {
            "id": "365ff2ea-98d2-4c6c-8f68-c9a07f456ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b11ebdb5-b290-436a-b914-6767dd367623",
            "compositeImage": {
                "id": "0480ca9a-1efa-4066-88a9-3edeedd6a671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365ff2ea-98d2-4c6c-8f68-c9a07f456ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5971ab-8de0-4f40-937b-c7a7b748082f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365ff2ea-98d2-4c6c-8f68-c9a07f456ad0",
                    "LayerId": "3f113d8b-d4d6-4de0-ab34-3ace60619625"
                }
            ]
        },
        {
            "id": "8b1814cc-bb2f-4511-9399-ac9b953673cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b11ebdb5-b290-436a-b914-6767dd367623",
            "compositeImage": {
                "id": "c81a8c51-6aa7-471e-933b-772b1accf780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1814cc-bb2f-4511-9399-ac9b953673cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd49cceb-bcd8-497d-b942-04955e94028c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1814cc-bb2f-4511-9399-ac9b953673cd",
                    "LayerId": "3f113d8b-d4d6-4de0-ab34-3ace60619625"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3f113d8b-d4d6-4de0-ab34-3ace60619625",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b11ebdb5-b290-436a-b914-6767dd367623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}