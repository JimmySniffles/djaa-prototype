{
    "id": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkpoints",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 19,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af9b1a97-e94c-4752-8886-04f3108a5da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
            "compositeImage": {
                "id": "d0ed6e7e-61fd-4e02-ae3a-2978c664b61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9b1a97-e94c-4752-8886-04f3108a5da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78624645-e2e9-4749-88b7-488757f24c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9b1a97-e94c-4752-8886-04f3108a5da5",
                    "LayerId": "7e31d9ed-3169-4dd0-870d-d5cad7eedf7f"
                }
            ]
        },
        {
            "id": "874e394e-e8f8-475b-93fa-dbb5a9472c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
            "compositeImage": {
                "id": "ad58fa60-7400-491c-9168-560a41b28c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874e394e-e8f8-475b-93fa-dbb5a9472c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a492e869-0a59-495e-b148-110cef7e0cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874e394e-e8f8-475b-93fa-dbb5a9472c37",
                    "LayerId": "7e31d9ed-3169-4dd0-870d-d5cad7eedf7f"
                }
            ]
        },
        {
            "id": "0543f179-3bf4-4bc7-9c41-1e6216449111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
            "compositeImage": {
                "id": "683e6831-4cfd-455b-80ca-aee754a3f0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0543f179-3bf4-4bc7-9c41-1e6216449111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bead93b-3e57-4e95-9ab0-4c090a0640af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0543f179-3bf4-4bc7-9c41-1e6216449111",
                    "LayerId": "7e31d9ed-3169-4dd0-870d-d5cad7eedf7f"
                }
            ]
        },
        {
            "id": "1fb432e5-8f98-4788-a559-5bfe283c1b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
            "compositeImage": {
                "id": "d8482c85-52ff-421f-a6c4-84cb36a1e509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb432e5-8f98-4788-a559-5bfe283c1b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb0b72f-53e4-45ee-9909-029d466b7d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb432e5-8f98-4788-a559-5bfe283c1b1c",
                    "LayerId": "7e31d9ed-3169-4dd0-870d-d5cad7eedf7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 124,
    "layers": [
        {
            "id": "7e31d9ed-3169-4dd0-870d-d5cad7eedf7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74c7da91-1e3e-4eb1-8d7f-6eabcb6ac280",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 32,
    "yorig": 123
}