{
    "id": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_sa_g_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afcf7e1a-dfce-4c7a-9902-9cc598c237ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "ef0dddb7-8c36-4f17-8654-ea5b2ebaf6aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcf7e1a-dfce-4c7a-9902-9cc598c237ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31838980-97d2-4f5d-9676-fbba06178dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcf7e1a-dfce-4c7a-9902-9cc598c237ab",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "2c5c22c7-9d64-454d-a206-ec182845b284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcf7e1a-dfce-4c7a-9902-9cc598c237ab",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        },
        {
            "id": "4f02562c-3e70-4c2a-bcd0-1b84ccd86cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "3eaca941-46a2-426e-81e4-0668fb5aeaf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f02562c-3e70-4c2a-bcd0-1b84ccd86cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "854b7bad-d706-4134-ada5-0f08b95ad92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f02562c-3e70-4c2a-bcd0-1b84ccd86cc0",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "87bf27b2-08d6-468b-8172-328d28bbfb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f02562c-3e70-4c2a-bcd0-1b84ccd86cc0",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        },
        {
            "id": "9d0bfcec-6087-47e4-903a-0ca94e232cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "12dd2fac-c125-44cc-8ec1-e301ce60ded6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d0bfcec-6087-47e4-903a-0ca94e232cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb90af5-e3de-4c63-8708-e29b41cca1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0bfcec-6087-47e4-903a-0ca94e232cdf",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "9eb2952c-b4ce-455e-8328-242f5d573b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0bfcec-6087-47e4-903a-0ca94e232cdf",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        },
        {
            "id": "f53d9e6c-c957-4955-8a35-bb93844752ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "13b6aee2-e557-4cd7-98ac-6722d7620e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53d9e6c-c957-4955-8a35-bb93844752ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb80918-7e3e-4e55-9f58-bfe58deca173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53d9e6c-c957-4955-8a35-bb93844752ce",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "f4fe009f-0f77-4d83-aa94-29ca86ca61da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53d9e6c-c957-4955-8a35-bb93844752ce",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        },
        {
            "id": "65d80cae-998f-4ecb-8e9f-540518bca4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "0f278f9e-174e-4550-bb27-978d3ab0f6a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d80cae-998f-4ecb-8e9f-540518bca4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5673baac-5f38-4dbf-a4bf-99b63a565308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d80cae-998f-4ecb-8e9f-540518bca4c2",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "1f540c9f-6af8-4e15-b8fb-191cdf95c8a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d80cae-998f-4ecb-8e9f-540518bca4c2",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        },
        {
            "id": "c7b9efa5-0d2c-479a-adb0-625a69d5cf39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "compositeImage": {
                "id": "d7d6d236-7666-4d91-b70c-127929e4c412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7b9efa5-0d2c-479a-adb0-625a69d5cf39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2c7bcf-1e9a-4727-9e8c-2f8bc359590a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b9efa5-0d2c-479a-adb0-625a69d5cf39",
                    "LayerId": "e04aafda-133e-41a7-8989-bd85de3352c3"
                },
                {
                    "id": "ed565953-35a7-495b-8154-440237d116c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b9efa5-0d2c-479a-adb0-625a69d5cf39",
                    "LayerId": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e04aafda-133e-41a7-8989-bd85de3352c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "45f11c9f-0b7b-43ed-8994-c4dc9f78a2b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50481154-8d37-4e65-ace2-17ef2e8c0f0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}