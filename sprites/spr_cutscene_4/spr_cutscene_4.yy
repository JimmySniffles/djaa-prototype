{
    "id": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba8301f2-5b66-449d-ac1c-412cde286884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
            "compositeImage": {
                "id": "f40537f1-e678-407b-961b-f126935c070e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8301f2-5b66-449d-ac1c-412cde286884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "999ad82b-768c-4f70-82cf-5760756be3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8301f2-5b66-449d-ac1c-412cde286884",
                    "LayerId": "1124c1c4-c5d3-49d8-95ad-e5b5c18e2c43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "1124c1c4-c5d3-49d8-95ad-e5b5c18e2c43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c81f4c-bcc5-49e9-ab8c-9b349314d864",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}