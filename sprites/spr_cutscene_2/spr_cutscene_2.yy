{
    "id": "83981bcd-4933-4c72-a50f-c9b66bcae855",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97acf98c-9cb0-44db-8992-671fd8145afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83981bcd-4933-4c72-a50f-c9b66bcae855",
            "compositeImage": {
                "id": "f723565a-7fd9-44fb-9429-0159af503ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97acf98c-9cb0-44db-8992-671fd8145afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6de91b-3df9-4714-8d1a-ad8a3818e603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97acf98c-9cb0-44db-8992-671fd8145afb",
                    "LayerId": "703937bb-e443-4593-a598-907c6be80816"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "703937bb-e443-4593-a598-907c6be80816",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83981bcd-4933-4c72-a50f-c9b66bcae855",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}