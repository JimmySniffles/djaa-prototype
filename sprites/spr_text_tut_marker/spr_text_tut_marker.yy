{
    "id": "0b39eada-a463-4fab-98a1-dcc88c64eca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text_tut_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b76fc334-0bab-43c1-b987-fa469b0f198f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b39eada-a463-4fab-98a1-dcc88c64eca9",
            "compositeImage": {
                "id": "e0632c9b-8006-426b-9974-c32ec7f581e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76fc334-0bab-43c1-b987-fa469b0f198f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd4bbb1a-ecdf-405a-9500-bd0d2d5b9ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76fc334-0bab-43c1-b987-fa469b0f198f",
                    "LayerId": "0cd6cc1d-f045-4386-9605-cc9ead5447bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "0cd6cc1d-f045-4386-9605-cc9ead5447bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b39eada-a463-4fab-98a1-dcc88c64eca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": -1
}