{
    "id": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_sa_g_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d68647e-ef1f-457a-b252-a2eaeea81440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "7185da42-1795-41e5-a9d1-c361ae7799c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d68647e-ef1f-457a-b252-a2eaeea81440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b853f0d-8c80-4740-af0f-217c8efb4b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d68647e-ef1f-457a-b252-a2eaeea81440",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "b8744f9c-1149-4005-8817-9c24477335d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "3c3da060-87a4-43f9-b139-9acadece7019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8744f9c-1149-4005-8817-9c24477335d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78b3d48-a35a-4537-89a5-b44995185afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8744f9c-1149-4005-8817-9c24477335d0",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "c4a73716-e7a2-4871-b8d6-108c94a727b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "116739ff-1387-470f-8d8e-57eb4dbd6e9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a73716-e7a2-4871-b8d6-108c94a727b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b43c50-3098-4c55-bd50-4d3f983901e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a73716-e7a2-4871-b8d6-108c94a727b8",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "22790425-3b7a-4366-afd9-20ba5507501b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "52e1e897-59f6-407b-b3f7-2260d017cc24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22790425-3b7a-4366-afd9-20ba5507501b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436edf52-c811-4ac4-9a5f-1505094fa5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22790425-3b7a-4366-afd9-20ba5507501b",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "ef0c2e27-2817-48d4-96c6-eff9796a859c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "f1113c4f-28bc-42e1-9776-7e5447e603cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0c2e27-2817-48d4-96c6-eff9796a859c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb19fec-a3d9-439d-bee2-b51a855705d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0c2e27-2817-48d4-96c6-eff9796a859c",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "c42501da-1f46-4296-a89e-e65682eddaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "16b24750-8442-4a44-8d4d-15c3d6d9fee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c42501da-1f46-4296-a89e-e65682eddaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6929d36e-78f8-43fd-8f5b-d5dd382053a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c42501da-1f46-4296-a89e-e65682eddaf2",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "1a86be1a-14b0-4ef7-8adf-3576410ac4ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "3feb5984-bb94-4157-b34c-55657f004f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a86be1a-14b0-4ef7-8adf-3576410ac4ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c8e92f-cc3c-4cd8-a90b-80f503a79135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a86be1a-14b0-4ef7-8adf-3576410ac4ac",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        },
        {
            "id": "fb6299ee-fc3d-4caf-92af-0d4f24682b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "compositeImage": {
                "id": "83746293-b0c7-46e3-a4c8-fd238a06ebb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6299ee-fc3d-4caf-92af-0d4f24682b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b35d61-6be8-4495-99a5-54ae97758908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6299ee-fc3d-4caf-92af-0d4f24682b68",
                    "LayerId": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d352ebf-85bc-4cb9-a7e8-d6c3a7a86f33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cf8be3b-ee0e-4906-815c-4a7a0f9dbc5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}