{
    "id": "a015b326-801f-4926-80fb-f2b5c91f48c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkang_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "792f66c3-ddef-47cc-be3d-be04f3d55528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015b326-801f-4926-80fb-f2b5c91f48c2",
            "compositeImage": {
                "id": "04a39354-89ab-4f8d-a0b9-3a676f486acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792f66c3-ddef-47cc-be3d-be04f3d55528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0e93527-68eb-4b14-a8c6-0e298cb4db6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792f66c3-ddef-47cc-be3d-be04f3d55528",
                    "LayerId": "2adcfdca-4a3c-417c-b1e2-5938800cd97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "2adcfdca-4a3c-417c-b1e2-5938800cd97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a015b326-801f-4926-80fb-f2b5c91f48c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}