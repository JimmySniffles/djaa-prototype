{
    "id": "4840a785-e6bd-4bbe-ad5e-f6a41a27362f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_treeline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 22,
    "bbox_right": 1901,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c77300f4-d9bf-449d-8b19-986cb99ec4ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4840a785-e6bd-4bbe-ad5e-f6a41a27362f",
            "compositeImage": {
                "id": "38b41e61-2c46-4e9c-96f2-a614fccd0e5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77300f4-d9bf-449d-8b19-986cb99ec4ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1851d01c-d9b4-4cf6-8122-b38bf61cacba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77300f4-d9bf-449d-8b19-986cb99ec4ae",
                    "LayerId": "767ed111-7f34-49e6-892e-8e7924421e05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "767ed111-7f34-49e6-892e-8e7924421e05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4840a785-e6bd-4bbe-ad5e-f6a41a27362f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}