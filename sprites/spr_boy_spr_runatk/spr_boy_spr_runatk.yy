{
    "id": "f7614b65-2e37-44c4-b514-8800c11379e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_runatk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67a2cf71-de8e-4e34-9ab9-75cddb9315f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "e964c80e-7975-4aff-b15b-4a3ec7f01dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a2cf71-de8e-4e34-9ab9-75cddb9315f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9674a5fb-5c14-48f1-ae31-fb9654bf14a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a2cf71-de8e-4e34-9ab9-75cddb9315f3",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "639e5ea7-3851-4500-9d3d-0b0012a72237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a2cf71-de8e-4e34-9ab9-75cddb9315f3",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        },
        {
            "id": "cfe5a701-7b8d-4de4-8b7b-e16460e7a828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "ce40a7ee-7440-4464-ae16-12af485581a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe5a701-7b8d-4de4-8b7b-e16460e7a828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec16beb-a581-4dc4-a508-755784050542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe5a701-7b8d-4de4-8b7b-e16460e7a828",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "f22aec4a-06ee-485e-bb78-51e1b7bb96f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe5a701-7b8d-4de4-8b7b-e16460e7a828",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        },
        {
            "id": "8ec04657-5ff3-4d44-92ae-d26eff3ddee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "71263e9d-dd18-45d7-9b95-b3581f4de69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec04657-5ff3-4d44-92ae-d26eff3ddee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49b02a1-2167-46d3-bdb5-8a16221a524f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec04657-5ff3-4d44-92ae-d26eff3ddee3",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "5d6b3df6-fd70-44a8-bc39-9ba11e82e2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec04657-5ff3-4d44-92ae-d26eff3ddee3",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        },
        {
            "id": "a9028027-851b-40ce-9231-3937404ca539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "2053aa6e-3d55-43f7-83af-ec95459e8fae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9028027-851b-40ce-9231-3937404ca539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b517619-c9a0-4aac-9e77-12995d94f00c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9028027-851b-40ce-9231-3937404ca539",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "fbe34080-7c9a-4357-bbcc-901796483be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9028027-851b-40ce-9231-3937404ca539",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        },
        {
            "id": "8da44b8e-8d0b-41c9-8ddf-c77e1a700026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "0c3e3560-1a16-4f1b-a627-39f1fdfff741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da44b8e-8d0b-41c9-8ddf-c77e1a700026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c39578-4b4d-4c74-88e9-102872a72076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da44b8e-8d0b-41c9-8ddf-c77e1a700026",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "63a5599f-f0bd-4d8f-b92e-196a30e12bd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da44b8e-8d0b-41c9-8ddf-c77e1a700026",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        },
        {
            "id": "d62a577d-01c5-40e2-a95e-2bf71406c106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "compositeImage": {
                "id": "076eabac-334e-4ce5-bc84-6bc2d8793acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62a577d-01c5-40e2-a95e-2bf71406c106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3a50e5-5144-4f86-a1c9-a51aa84266fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62a577d-01c5-40e2-a95e-2bf71406c106",
                    "LayerId": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8"
                },
                {
                    "id": "c23a62da-f775-4ace-9024-8876846c9a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62a577d-01c5-40e2-a95e-2bf71406c106",
                    "LayerId": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "55b1e8f0-b9e3-474d-bdce-f55e94cd9ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4d8e0c54-fa99-4208-b1ed-47c737ffdd3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7614b65-2e37-44c4-b514-8800c11379e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}