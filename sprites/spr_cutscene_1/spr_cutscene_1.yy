{
    "id": "1dda8ac3-d179-4006-a644-d9b0e2228384",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048cd6db-2b73-463a-a7b3-4564bbfa74d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "8a63df3d-eba2-4a0e-9bd5-6bb05652e6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048cd6db-2b73-463a-a7b3-4564bbfa74d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7379eae0-4bab-4cf1-8515-97786380887d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048cd6db-2b73-463a-a7b3-4564bbfa74d3",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "a17fd777-6914-44e6-b59c-1d3a4e03e4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "3a49dd1e-9014-4103-bc8c-a97e7e42dee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17fd777-6914-44e6-b59c-1d3a4e03e4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce86d0e-d611-4d2f-a8b5-c09e24fb7a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17fd777-6914-44e6-b59c-1d3a4e03e4a5",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "50db4c06-4dda-4fed-aeb5-ac8fd848f765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "308d7c0f-79d4-475f-b1cb-a41f1a91a44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50db4c06-4dda-4fed-aeb5-ac8fd848f765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faaebb07-4aa2-4c5c-b40a-5efcc33ecc10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50db4c06-4dda-4fed-aeb5-ac8fd848f765",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "6ce93c4e-65e6-4f7f-9899-4242e3da1933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "e91af02e-3eba-4f9a-b222-3e3bbf629088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce93c4e-65e6-4f7f-9899-4242e3da1933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cbafd06-c8ee-4d98-bb57-9a771e20c618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce93c4e-65e6-4f7f-9899-4242e3da1933",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "30df3dee-2709-49c1-9700-1cef778278c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "cf4986ba-0d25-4926-bb50-563de4056677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30df3dee-2709-49c1-9700-1cef778278c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8cf3818-e8d0-4fba-99b1-a26ab2eadc81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30df3dee-2709-49c1-9700-1cef778278c0",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "e4f86043-ec25-4f8b-88a7-5a1c163f2a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "44268d63-464b-48be-9489-fee40641e91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f86043-ec25-4f8b-88a7-5a1c163f2a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfba55ef-440e-4a0f-aebf-e148719f7ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f86043-ec25-4f8b-88a7-5a1c163f2a21",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "f41c8d3b-04fc-46a9-8a16-9c97e36fa08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "11b58c7a-6769-4a34-8813-dbe32b70ef23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f41c8d3b-04fc-46a9-8a16-9c97e36fa08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1371610b-4fdf-4ca7-bbac-06d56555a789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f41c8d3b-04fc-46a9-8a16-9c97e36fa08e",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        },
        {
            "id": "7b9dc02d-3c3c-42a5-bd78-fa909854f214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "compositeImage": {
                "id": "f3c1f591-e969-4a09-8645-90643f37638a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9dc02d-3c3c-42a5-bd78-fa909854f214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f95114f-4fe6-4cf2-9007-0d9f12706d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9dc02d-3c3c-42a5-bd78-fa909854f214",
                    "LayerId": "882c2b6b-e86b-41c6-a646-06282ad3ce79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "882c2b6b-e86b-41c6-a646-06282ad3ce79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dda8ac3-d179-4006-a644-d9b0e2228384",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}