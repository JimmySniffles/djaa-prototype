{
    "id": "72dd7e15-0595-4736-875f-3088f77efa76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deathparticle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 1,
    "bbox_right": 4,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22ea52a8-e1c7-4e30-934e-d8f404883247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "d3b01102-7824-4c2c-b90c-7248641dfa8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ea52a8-e1c7-4e30-934e-d8f404883247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85a6d11-03cd-47de-afc8-a24dd43c1108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ea52a8-e1c7-4e30-934e-d8f404883247",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "d1f64158-0738-4b8c-9846-9e5f587a8fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "3bcb6079-f99b-4bcb-9bd6-7084aaeddf74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f64158-0738-4b8c-9846-9e5f587a8fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f5b396b-5514-47b9-bb2d-c75f9ccae247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f64158-0738-4b8c-9846-9e5f587a8fed",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "bcf6d5ab-2f1c-4cca-be82-9a209cb5dc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "235e73ad-182d-45a3-a65c-67fc735bca8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf6d5ab-2f1c-4cca-be82-9a209cb5dc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a4667d-3633-4b01-8553-4033688ba448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf6d5ab-2f1c-4cca-be82-9a209cb5dc04",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "8b48c82d-68d8-47a8-b720-7a1c2cdbf772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "8c8f713c-a791-4c63-a692-ec255746ccb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b48c82d-68d8-47a8-b720-7a1c2cdbf772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638136fa-2a23-41f0-a71e-ca07ced50dd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b48c82d-68d8-47a8-b720-7a1c2cdbf772",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "285dc8d2-23c9-461e-8ca1-0290018bf769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "4516fc23-02b2-49ee-8e3b-f9e386e83eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285dc8d2-23c9-461e-8ca1-0290018bf769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eadedbb-8362-409a-8abd-477166b95c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285dc8d2-23c9-461e-8ca1-0290018bf769",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "190c95ef-c3f5-4fb7-96bb-401864ece487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "955381ce-0c14-4ab5-a345-e19458941b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190c95ef-c3f5-4fb7-96bb-401864ece487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab61f1a-462a-4daf-b2bf-a522116245e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190c95ef-c3f5-4fb7-96bb-401864ece487",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "0e1d75ba-640b-4b81-b730-19787d21fa26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "adb14a4f-b535-41a6-97c2-5d8806a3824a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1d75ba-640b-4b81-b730-19787d21fa26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584121b8-7210-4567-afef-72344b308aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1d75ba-640b-4b81-b730-19787d21fa26",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "e85a2502-abd5-479b-9144-12fc1d44a272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "dc1fe5fe-0642-4072-baf1-de9c7bbf52e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85a2502-abd5-479b-9144-12fc1d44a272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a552ef00-6435-419d-b444-e2beb6896693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85a2502-abd5-479b-9144-12fc1d44a272",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "e3f83775-c97f-488a-9c36-51eca79be7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "9f623956-f2d9-4af1-a902-913553fa89e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f83775-c97f-488a-9c36-51eca79be7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e796ea8-1d19-4af0-8c9d-45d51c860f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f83775-c97f-488a-9c36-51eca79be7cc",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "31fe4082-8761-4686-bf31-17bc3a8c36d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "71703514-89fe-4e32-b991-6301ef82e7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31fe4082-8761-4686-bf31-17bc3a8c36d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e96359-d96a-4449-be9b-6e3ed2f19405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31fe4082-8761-4686-bf31-17bc3a8c36d0",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        },
        {
            "id": "33e78975-0a96-42b9-b993-f1dc39d9b13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "compositeImage": {
                "id": "c0c748df-7dd5-423f-8e72-2a5590b95cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e78975-0a96-42b9-b993-f1dc39d9b13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c49048f7-46ee-4127-bfa6-35b7fb9d2ded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e78975-0a96-42b9-b993-f1dc39d9b13d",
                    "LayerId": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "3dc09860-3579-4afe-b4e4-9b1b8e2bee79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72dd7e15-0595-4736-875f-3088f77efa76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}