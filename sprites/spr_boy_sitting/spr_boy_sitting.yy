{
    "id": "7b45421d-247f-4b8e-917a-4109a716ac6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 47,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e63fa965-dd01-4f36-969e-86e7c0c20b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b45421d-247f-4b8e-917a-4109a716ac6e",
            "compositeImage": {
                "id": "6c72fef3-b3b3-4e96-8b0c-ad59cc2ca99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63fa965-dd01-4f36-969e-86e7c0c20b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823f6cdb-65d8-41ad-ab05-b8a2e51f6ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63fa965-dd01-4f36-969e-86e7c0c20b4d",
                    "LayerId": "f900b869-59c6-45c7-a760-5043f81f1a16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f900b869-59c6-45c7-a760-5043f81f1a16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b45421d-247f-4b8e-917a-4109a716ac6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}