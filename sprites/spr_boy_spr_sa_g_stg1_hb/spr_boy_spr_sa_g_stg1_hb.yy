{
    "id": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_sa_g_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72565888-2761-4ee8-8598-56218fbabe07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "48f4113d-8490-4f04-a02e-f84920e1f419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72565888-2761-4ee8-8598-56218fbabe07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f417d5-d347-4847-a658-fdaafa0e277b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72565888-2761-4ee8-8598-56218fbabe07",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "1fabab81-df16-42e9-adc0-5b0203fa6aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72565888-2761-4ee8-8598-56218fbabe07",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "7d39563a-2aef-4dc1-8c5e-432e19cc9feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "d6f21cbb-f275-433d-b21e-a6468631cb3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d39563a-2aef-4dc1-8c5e-432e19cc9feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0016b2-5d03-4438-b361-eb37d7a4fb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d39563a-2aef-4dc1-8c5e-432e19cc9feb",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "eda33f0f-fc0a-4277-86fd-7acfd7c63b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d39563a-2aef-4dc1-8c5e-432e19cc9feb",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "3ce4227f-a835-4a36-9556-3704e7fe3b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "a8a9c0d4-cbcc-4954-9b60-0499240fdb84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce4227f-a835-4a36-9556-3704e7fe3b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd43db4-7d89-4cd4-9467-498b11e8b874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce4227f-a835-4a36-9556-3704e7fe3b93",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "b121c59f-ddf4-42c3-af5d-fa7a900298df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce4227f-a835-4a36-9556-3704e7fe3b93",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "97e7fed9-f257-4109-a828-ef20ec770fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "f674128e-2b7b-456b-8d11-40de59d151c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e7fed9-f257-4109-a828-ef20ec770fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70ca938-3060-4a43-9ed2-7ee548f8d8da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e7fed9-f257-4109-a828-ef20ec770fa3",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "8cc235dd-4623-49ad-9ab3-32ca32e7f762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e7fed9-f257-4109-a828-ef20ec770fa3",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "e3bd55e4-b0d2-41ec-a2cd-2dc0be56c95e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "c70cc52a-6a91-435e-9cfa-128660d8257d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3bd55e4-b0d2-41ec-a2cd-2dc0be56c95e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f4a606-2f75-4757-be5b-fba664464729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3bd55e4-b0d2-41ec-a2cd-2dc0be56c95e",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "dfe6fdee-e7c9-4b79-8bfd-ea420ab5576b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3bd55e4-b0d2-41ec-a2cd-2dc0be56c95e",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "96096312-a12e-4847-baa0-6f7c6e0b1774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "1244ef34-ad96-4435-abdd-5cd16afcc2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96096312-a12e-4847-baa0-6f7c6e0b1774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7d1aef-e3f1-48ca-8e77-44bbe6cc5701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96096312-a12e-4847-baa0-6f7c6e0b1774",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "3d39165a-f80d-4fc1-b1c6-55fb9fde71cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96096312-a12e-4847-baa0-6f7c6e0b1774",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "77c9af87-15e9-443a-aa0b-b6613acfc703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "864a5c1a-2e0c-4d9c-906b-ae95ef4902c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77c9af87-15e9-443a-aa0b-b6613acfc703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac7ba90-c5be-433e-9bb3-ec0c6b034821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c9af87-15e9-443a-aa0b-b6613acfc703",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "8d3a04a8-f2c6-4146-99a9-3fbd71a9e4f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c9af87-15e9-443a-aa0b-b6613acfc703",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        },
        {
            "id": "5e88fc38-811e-45a0-b2cd-7c54bc500bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "compositeImage": {
                "id": "af3386fb-1fd7-40fe-883b-a7c961c2f7bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e88fc38-811e-45a0-b2cd-7c54bc500bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ee48b4-599d-42c8-8642-ef0b7eb429bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e88fc38-811e-45a0-b2cd-7c54bc500bdd",
                    "LayerId": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339"
                },
                {
                    "id": "43284e19-250d-4e14-92c9-e1679502c11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e88fc38-811e-45a0-b2cd-7c54bc500bdd",
                    "LayerId": "f441270a-f689-4b7f-97f5-8037426dad80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f441270a-f689-4b7f-97f5-8037426dad80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "HitBox",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b5f578fb-9e5b-4aef-a4f7-7465ed27d339",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7de5ac90-39fc-4f84-9c80-8b4d5e1c08fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}