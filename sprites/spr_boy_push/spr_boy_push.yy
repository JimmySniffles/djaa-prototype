{
    "id": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_push",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 45,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66f5bb44-ffaf-46c6-9ffc-941e5ea2e369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "2936afc6-20ee-4c1c-8ccc-0d11822ebf73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f5bb44-ffaf-46c6-9ffc-941e5ea2e369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c898a2ff-bb46-49e8-96fa-b61ea5710ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f5bb44-ffaf-46c6-9ffc-941e5ea2e369",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "3660b2c4-4323-43f5-a14e-b290955216b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "dc03d7d7-207b-47f2-8801-f3b806357776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3660b2c4-4323-43f5-a14e-b290955216b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ea3498-bf04-4a65-a610-22d2b841e1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3660b2c4-4323-43f5-a14e-b290955216b3",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "de4252bd-22c8-45de-bd96-972acb55f00e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "78aa35bb-a817-452c-90de-8026591b21f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4252bd-22c8-45de-bd96-972acb55f00e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779e6f16-57da-4981-a8f9-9da93cee1235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4252bd-22c8-45de-bd96-972acb55f00e",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "5ca00cae-05c6-48f0-909f-10e1a882a6b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "15e53139-47fd-4e48-be59-0ed5faf94722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca00cae-05c6-48f0-909f-10e1a882a6b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d48f0ebf-3d8d-41c7-8949-b574587652d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca00cae-05c6-48f0-909f-10e1a882a6b3",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "638b86fa-7dc6-4bd1-8bbb-007a116f49cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "4686d1dc-64ff-445a-86c6-bd4b850a4ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638b86fa-7dc6-4bd1-8bbb-007a116f49cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d94678-9a3d-480d-a422-f1c4abe4edb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638b86fa-7dc6-4bd1-8bbb-007a116f49cc",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "85a8111d-caa8-4f0a-80d4-ec84ef141d20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "9328a7cd-4f3d-4a29-a5d6-c02f2d84bdab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a8111d-caa8-4f0a-80d4-ec84ef141d20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b45ec8c-4b2d-4a16-9c32-2d286ab71b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a8111d-caa8-4f0a-80d4-ec84ef141d20",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "d8d35293-4ea8-4462-8b43-0b379ccec80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "578e1eca-b645-43d9-8d45-d9821c2159ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8d35293-4ea8-4462-8b43-0b379ccec80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d5ff61-740e-457a-b5f7-228c7dfe3be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8d35293-4ea8-4462-8b43-0b379ccec80d",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "887b4e6c-a93c-49ac-976b-08362120d9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "6d6adcff-c21e-4c73-b574-4bd42f38e5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887b4e6c-a93c-49ac-976b-08362120d9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97899d24-83fa-4467-a6ac-ace5bfed9fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887b4e6c-a93c-49ac-976b-08362120d9f7",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "d3c031cb-3192-42bb-ae2d-f17b7b988dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "6221235e-bf8b-4477-9462-24412c65046d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3c031cb-3192-42bb-ae2d-f17b7b988dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04003b6c-c345-4f04-bbc2-38861c481fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3c031cb-3192-42bb-ae2d-f17b7b988dd0",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "ff02a632-8930-4423-8d6d-1a659c82469a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "c33188df-f411-4273-9945-ee91979a8002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff02a632-8930-4423-8d6d-1a659c82469a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b28553c-73cc-4720-a8e0-35d78de9678b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff02a632-8930-4423-8d6d-1a659c82469a",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "1263c577-b83c-4f4e-aaef-eb5626d723cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "74ff36fb-ad23-4d32-960c-13770f446127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1263c577-b83c-4f4e-aaef-eb5626d723cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7832641-93eb-4c97-a962-225eaf7d20b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1263c577-b83c-4f4e-aaef-eb5626d723cd",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        },
        {
            "id": "75e6ee68-b61e-4a9d-9ae1-95abed78c554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "compositeImage": {
                "id": "e8155f1f-26b0-4987-9c18-1985c72f6f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e6ee68-b61e-4a9d-9ae1-95abed78c554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd580958-b1f3-44e2-ba25-6f00d334c532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e6ee68-b61e-4a9d-9ae1-95abed78c554",
                    "LayerId": "6f54e46f-e0e8-4860-897f-302d6c84e08a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6f54e46f-e0e8-4860-897f-302d6c84e08a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a48a1e1e-87a8-4e58-b39b-cd48bccc4a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}