{
    "id": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13dacd93-263b-4aa1-a995-b90f5a23a43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "840b8320-7041-42de-9dcb-b8e33f3c93e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13dacd93-263b-4aa1-a995-b90f5a23a43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c7fb2a-2cf8-4e69-908c-14f6fb31732b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13dacd93-263b-4aa1-a995-b90f5a23a43e",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        },
        {
            "id": "2e16a895-c0a5-42e5-8ced-df652caeebf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "b5bca7d8-0541-4608-9d7a-46911bd510be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e16a895-c0a5-42e5-8ced-df652caeebf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ce26df-93e3-4de3-8dcf-51d556c423e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e16a895-c0a5-42e5-8ced-df652caeebf8",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        },
        {
            "id": "fb91211e-432c-4465-bf85-e3d30ec44441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "1d7d7646-fa57-4e79-924a-b392f0672d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb91211e-432c-4465-bf85-e3d30ec44441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678ed22d-2194-44eb-8736-84941bc5d917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb91211e-432c-4465-bf85-e3d30ec44441",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        },
        {
            "id": "a388f517-cd73-492e-8f71-3705c660e4ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "909711c4-e1ee-457a-a2cf-6b0c8ec25e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a388f517-cd73-492e-8f71-3705c660e4ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896c3995-1d43-4247-9e35-d8c0763d350b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a388f517-cd73-492e-8f71-3705c660e4ff",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        },
        {
            "id": "c16cb5cc-56fc-470b-b627-3c5596ab4329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "3dbcf00f-77e4-40b3-8c82-52daf079bee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16cb5cc-56fc-470b-b627-3c5596ab4329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7ae3b5-08e5-4ef7-a1df-36af8f1ec4f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16cb5cc-56fc-470b-b627-3c5596ab4329",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        },
        {
            "id": "f96f856f-9ac1-41ed-ada9-ee6c2c2eaddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "compositeImage": {
                "id": "e0c251dc-47d4-4ce8-a8b1-6e8c2bcd7987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96f856f-9ac1-41ed-ada9-ee6c2c2eaddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1385fa6-e45d-4b26-97ac-35d8c3e2b112",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96f856f-9ac1-41ed-ada9-ee6c2c2eaddf",
                    "LayerId": "99fb66f3-8bcf-4fd9-be27-b070f4873903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99fb66f3-8bcf-4fd9-be27-b070f4873903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a83be37c-b6ff-4d45-8771-eb943e8ba73e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}