{
    "id": "615946ae-fc72-4f94-8807-06aecaf4d856",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_sa_a_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f199b242-1cff-4126-bc64-f11fc02e7cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "d9a3d541-f2fe-46f1-be4b-423384194e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f199b242-1cff-4126-bc64-f11fc02e7cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb68f8d-e854-495a-b934-e83d4b07c09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f199b242-1cff-4126-bc64-f11fc02e7cc6",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "bd1b8132-cf5d-4065-8a57-8327f68a2142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f199b242-1cff-4126-bc64-f11fc02e7cc6",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        },
        {
            "id": "31f5db75-a172-48fb-8138-07b611a5eb53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "24b76f4f-9099-4add-b25e-9aa6cd1b334f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f5db75-a172-48fb-8138-07b611a5eb53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ffa8d1-0b24-4d50-a14a-28673eddf3a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f5db75-a172-48fb-8138-07b611a5eb53",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "5025a08f-247a-40e8-bf06-55a658c2e276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f5db75-a172-48fb-8138-07b611a5eb53",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        },
        {
            "id": "65898925-9b42-479b-8e5b-dbe9ac871e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "b562b9cd-48f5-4da1-a04c-038e8e147746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65898925-9b42-479b-8e5b-dbe9ac871e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41f2c6c-9c3f-4dcb-938a-edfbd0bafa13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65898925-9b42-479b-8e5b-dbe9ac871e12",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "a6e70275-d5b8-4c21-9988-146d578534af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65898925-9b42-479b-8e5b-dbe9ac871e12",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        },
        {
            "id": "6f568489-6df3-48f9-99e7-afe021b9f61f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "432f4a75-4896-4751-9e5c-79722a97e054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f568489-6df3-48f9-99e7-afe021b9f61f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baad7d63-72a6-4003-9e73-a00bf45e8ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f568489-6df3-48f9-99e7-afe021b9f61f",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "57a2a6e1-c371-40ef-a4f6-5f5b4968787f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f568489-6df3-48f9-99e7-afe021b9f61f",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        },
        {
            "id": "85bdd601-4f16-4942-b767-e8854ad9c45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "bbfc5b0e-e79c-42a5-8837-a0a84d142735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bdd601-4f16-4942-b767-e8854ad9c45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1558c7b-f6d4-4c88-80d9-b85ed99fadb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bdd601-4f16-4942-b767-e8854ad9c45d",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "a1871c70-a2db-4f79-8975-8fead5f51595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bdd601-4f16-4942-b767-e8854ad9c45d",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        },
        {
            "id": "feeb7e1e-2283-49e1-b1cb-fda219fac26c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "compositeImage": {
                "id": "4c9decf7-a739-4343-8e1d-daf975f9cb65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feeb7e1e-2283-49e1-b1cb-fda219fac26c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd87f125-55fe-4cb7-ade4-67876a2b06f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feeb7e1e-2283-49e1-b1cb-fda219fac26c",
                    "LayerId": "4853face-6324-491b-920e-ac9c5a903166"
                },
                {
                    "id": "9b46f2a0-d764-4bb3-931a-b57cb8512ede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feeb7e1e-2283-49e1-b1cb-fda219fac26c",
                    "LayerId": "22650d70-5509-4b0c-b409-cb761b21be59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4853face-6324-491b-920e-ac9c5a903166",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "22650d70-5509-4b0c-b409-cb761b21be59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615946ae-fc72-4f94-8807-06aecaf4d856",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}