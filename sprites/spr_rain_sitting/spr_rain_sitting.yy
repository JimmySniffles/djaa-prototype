{
    "id": "1fadb218-5db7-4148-9ace-ffe60f205544",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rain_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 46,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b7f0c20-c810-430b-92b0-3aeef1e2b4af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fadb218-5db7-4148-9ace-ffe60f205544",
            "compositeImage": {
                "id": "63796838-1c5f-49cf-8c90-61d2e3a95d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7f0c20-c810-430b-92b0-3aeef1e2b4af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c8c178-c447-4548-a5cc-436e59877a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7f0c20-c810-430b-92b0-3aeef1e2b4af",
                    "LayerId": "560304e8-3b5f-4dc0-9537-c5035ed815bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "560304e8-3b5f-4dc0-9537-c5035ed815bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fadb218-5db7-4148-9ace-ffe60f205544",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}