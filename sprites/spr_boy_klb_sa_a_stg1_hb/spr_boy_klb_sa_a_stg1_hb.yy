{
    "id": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_sa_a_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1062db7-bcd9-48c3-9d8f-17424ce5dfbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "945d1984-13d3-4817-99af-b5616cdc3a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1062db7-bcd9-48c3-9d8f-17424ce5dfbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45a6a34-52f8-4080-b9cb-f8c02ae11fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1062db7-bcd9-48c3-9d8f-17424ce5dfbc",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "e2a5855c-b6db-4601-b31f-f1007ad8949f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1062db7-bcd9-48c3-9d8f-17424ce5dfbc",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        },
        {
            "id": "af5b7ae8-da01-4120-8109-67803ffccb0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "a6c0c618-f5aa-4da7-a21d-cb5a0e4b7c19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5b7ae8-da01-4120-8109-67803ffccb0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3857628-292e-48ed-b555-4ca2930e3385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5b7ae8-da01-4120-8109-67803ffccb0a",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "bb3f19c2-dd9c-4cab-9c36-48788713a513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5b7ae8-da01-4120-8109-67803ffccb0a",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        },
        {
            "id": "e9ff9f37-fffd-46de-ab32-5e4fad44ab48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "bc69d9d4-1cd7-4307-af0c-9622b16ecf7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ff9f37-fffd-46de-ab32-5e4fad44ab48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f748d3d-fe43-411c-af73-054788b8126a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ff9f37-fffd-46de-ab32-5e4fad44ab48",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "721d89be-c388-488d-84ab-af8f42fc1eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ff9f37-fffd-46de-ab32-5e4fad44ab48",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        },
        {
            "id": "30085268-0bf7-4d7f-8e8d-0d113db6c3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "2461d8b1-dac5-467e-85d7-15e17ed73561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30085268-0bf7-4d7f-8e8d-0d113db6c3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f87b7a-ddc3-41d4-993a-6a6bfe9a6c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30085268-0bf7-4d7f-8e8d-0d113db6c3f2",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "514d2ea7-e9ed-49b1-a132-ccadb7577239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30085268-0bf7-4d7f-8e8d-0d113db6c3f2",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        },
        {
            "id": "7f83ecae-77d7-45ac-8b20-dbd861418fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "883568ac-df61-4108-be78-d09156254a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f83ecae-77d7-45ac-8b20-dbd861418fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aca7c136-b844-4b15-b4e1-542a76d6304c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f83ecae-77d7-45ac-8b20-dbd861418fe1",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "f2f6f6a9-d81c-4bce-9ed6-d3588b4594c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f83ecae-77d7-45ac-8b20-dbd861418fe1",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        },
        {
            "id": "62d13e11-c8b5-4e12-ada0-581deb95c200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "compositeImage": {
                "id": "2279a9df-57d3-4809-81c3-098e5b369da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d13e11-c8b5-4e12-ada0-581deb95c200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82de4b24-6945-44b6-99a6-3f6fa55e2b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d13e11-c8b5-4e12-ada0-581deb95c200",
                    "LayerId": "60633e75-9e64-4e7f-8893-e6214d120a4c"
                },
                {
                    "id": "902ed386-6616-41a5-92a4-a050ac14f7c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d13e11-c8b5-4e12-ada0-581deb95c200",
                    "LayerId": "68e9b41b-ef1a-4518-903c-7e889d1cf408"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "60633e75-9e64-4e7f-8893-e6214d120a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "68e9b41b-ef1a-4518-903c-7e889d1cf408",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ad5c061-6d05-4797-b9ff-837ff1516a97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}