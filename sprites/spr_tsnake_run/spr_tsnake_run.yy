{
    "id": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tsnake_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c355d507-fd94-4e66-bcc9-ee14a4bb66e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "4c987bce-7234-446b-8d10-d8307a3dcc2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c355d507-fd94-4e66-bcc9-ee14a4bb66e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa95ab01-00bf-4e99-b0c2-88e3d33d3e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c355d507-fd94-4e66-bcc9-ee14a4bb66e0",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "2dd0b577-b15d-432c-805e-6704b4d02477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "aaa85d09-40ba-4962-9d45-f8b492747d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd0b577-b15d-432c-805e-6704b4d02477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0486ac2-89e5-4137-8f3c-b15d0d0033f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd0b577-b15d-432c-805e-6704b4d02477",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "b129ee3d-21db-4f68-a0f8-931f72a00ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "88feec74-e4a8-4914-9b06-f41ef06cfa94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b129ee3d-21db-4f68-a0f8-931f72a00ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d737d393-5942-42fc-b3c3-28f042eddfa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b129ee3d-21db-4f68-a0f8-931f72a00ee5",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "5436e06a-1472-4712-a4ba-dbd94e830d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "1a69d8b8-5390-437b-aa8c-320f9b63b78a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5436e06a-1472-4712-a4ba-dbd94e830d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b989b843-18c7-46b4-99fd-6ba5e9a83104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5436e06a-1472-4712-a4ba-dbd94e830d26",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "70fcc51e-2a63-4448-a86c-25ac302b5e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "ebaf89f6-c72d-484f-b105-187f39778276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70fcc51e-2a63-4448-a86c-25ac302b5e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4149abf5-cbf2-4ba5-a666-76b7b87fcebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70fcc51e-2a63-4448-a86c-25ac302b5e5b",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "9185fa54-cdc1-49be-bdea-b58d573ef01a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "475879f7-3fef-4e23-9e20-0238eca86101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9185fa54-cdc1-49be-bdea-b58d573ef01a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e1f9760-6acd-4091-a592-7e7ea1d59881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9185fa54-cdc1-49be-bdea-b58d573ef01a",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "cb867f51-e37f-440f-84ea-af70bb1b92fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "a101817b-193b-42ee-8c12-40549e7a6676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb867f51-e37f-440f-84ea-af70bb1b92fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977582fe-a807-4a02-abb8-b49d8200016e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb867f51-e37f-440f-84ea-af70bb1b92fa",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        },
        {
            "id": "400d3ae7-d8d5-4bc9-9671-8ca5491472ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "compositeImage": {
                "id": "78c9b91b-582d-4599-851e-f37d38017802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400d3ae7-d8d5-4bc9-9671-8ca5491472ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f66e42-6bed-4c8b-861e-f3e3883b303f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400d3ae7-d8d5-4bc9-9671-8ca5491472ba",
                    "LayerId": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "5a5541d2-4df9-4b56-acd9-f5ca8cfb4b33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55dc75d2-d22d-425a-bf42-af50da5f8dae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}