{
    "id": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkang_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6301f316-55f8-49f2-b1fc-02588e8e57a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
            "compositeImage": {
                "id": "f19b26a3-4cae-4a0a-9026-822a9704009d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6301f316-55f8-49f2-b1fc-02588e8e57a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ab0d1f-615a-448b-99b1-312b55f5e542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6301f316-55f8-49f2-b1fc-02588e8e57a4",
                    "LayerId": "00c102c3-4968-48f8-8da7-ce195b935802"
                }
            ]
        },
        {
            "id": "c02bceeb-17be-455c-9030-ebb92ace8108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
            "compositeImage": {
                "id": "bb956eee-0703-4f1e-95cb-31d44769ee3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c02bceeb-17be-455c-9030-ebb92ace8108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ee015b-4f50-4997-8b63-62f57a2ccbff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c02bceeb-17be-455c-9030-ebb92ace8108",
                    "LayerId": "00c102c3-4968-48f8-8da7-ce195b935802"
                }
            ]
        },
        {
            "id": "8cdd9121-6417-463b-b997-1a81c98789f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
            "compositeImage": {
                "id": "a60c925c-0316-415b-abae-53eed6aa14d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdd9121-6417-463b-b997-1a81c98789f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60170874-db24-48a6-9a4d-bf1009669a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdd9121-6417-463b-b997-1a81c98789f7",
                    "LayerId": "00c102c3-4968-48f8-8da7-ce195b935802"
                }
            ]
        },
        {
            "id": "dc481190-7101-433a-86e1-f992560c252f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
            "compositeImage": {
                "id": "9982ca3e-c451-4c99-8fe7-adbdca65c262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc481190-7101-433a-86e1-f992560c252f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bf8611-ee75-4ae3-ab97-eddf49221f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc481190-7101-433a-86e1-f992560c252f",
                    "LayerId": "00c102c3-4968-48f8-8da7-ce195b935802"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "00c102c3-4968-48f8-8da7-ce195b935802",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d695154-e0dd-4faf-9119-0149f9d5c78f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}