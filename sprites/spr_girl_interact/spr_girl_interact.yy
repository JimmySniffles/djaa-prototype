{
    "id": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_interact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c92c374-0400-45a5-b614-437011c830a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "c26342ba-e00a-47d0-8e2f-2ed147df88af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c92c374-0400-45a5-b614-437011c830a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c91935-f983-4a25-b297-bacfeebaf810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c92c374-0400-45a5-b614-437011c830a2",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "57b1e946-5cd6-405b-8538-bfc495c183f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "af55bb00-74ae-49bf-a791-82ff90209a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b1e946-5cd6-405b-8538-bfc495c183f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815ff52b-cb19-4b6a-ad7d-95c680ef215b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b1e946-5cd6-405b-8538-bfc495c183f0",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "bc6043e9-3c11-4c85-a02e-422e2337f638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "05fdee04-afde-40b8-9d89-0865c3b9c106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6043e9-3c11-4c85-a02e-422e2337f638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14e46841-170c-463e-9207-9f35fa022a86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6043e9-3c11-4c85-a02e-422e2337f638",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "3ed2f97e-5f9e-432b-8050-937ac4ffc990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "82c1ccbb-aa32-4752-a50d-9ede9dc7cee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed2f97e-5f9e-432b-8050-937ac4ffc990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207b17ce-dfa0-4e43-96e7-be1ab4f808ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed2f97e-5f9e-432b-8050-937ac4ffc990",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "0a937af8-68de-42b4-8146-550dcce9352c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "1fc69c9f-93ba-4df3-b14d-7db8791926ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a937af8-68de-42b4-8146-550dcce9352c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f263fcd3-3280-45b8-b7d1-054dab282042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a937af8-68de-42b4-8146-550dcce9352c",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "2ff5c119-e810-4f4c-b938-623512f61828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "ab9a43ab-0f58-4525-82e6-3d6d241a5efa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff5c119-e810-4f4c-b938-623512f61828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778ed3e8-c335-4d5a-87b4-03883088c3b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff5c119-e810-4f4c-b938-623512f61828",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "58721aab-6170-4fd2-a2b6-617ec5807aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "7fd43428-020b-4a99-99bf-08d8e021311a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58721aab-6170-4fd2-a2b6-617ec5807aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66942ad5-b805-4f6a-a424-35af3c56e94c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58721aab-6170-4fd2-a2b6-617ec5807aeb",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "63ed99ae-7081-49d8-99ea-105b60c4a9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "aae72aeb-2add-42ca-b607-dd9c887593a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ed99ae-7081-49d8-99ea-105b60c4a9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2207667d-889a-4d65-8675-c4b71e9c77d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ed99ae-7081-49d8-99ea-105b60c4a9c1",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "07a9e11e-04f9-466c-b406-1d83bb167b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "3db3f693-804a-4e4d-96d1-5c636b00e215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a9e11e-04f9-466c-b406-1d83bb167b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b49ef07-57fa-4757-926d-fefb6958a229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a9e11e-04f9-466c-b406-1d83bb167b27",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "950b7498-40c8-4581-80b9-a564b1a62e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "a8bef492-10af-457a-a649-f18de9ceea9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "950b7498-40c8-4581-80b9-a564b1a62e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc23642f-7dda-4221-a4f9-f0ef522b7f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "950b7498-40c8-4581-80b9-a564b1a62e46",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "b85b9922-3f5e-4de4-a290-e241ff4cb6b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "5ced74d1-297b-4b76-bad7-bd28340fd721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b85b9922-3f5e-4de4-a290-e241ff4cb6b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3058e0f8-ea84-4c8a-8ac8-3026053acd50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b85b9922-3f5e-4de4-a290-e241ff4cb6b6",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "d108fc5b-cf24-46f3-9364-87e1460f4b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "ded4eaf6-93da-44f4-bebe-ac2e837359e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d108fc5b-cf24-46f3-9364-87e1460f4b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d91b76-5888-43dd-acd5-ed33b36826ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d108fc5b-cf24-46f3-9364-87e1460f4b4b",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "e01b4327-2326-4671-badd-ce99047c3aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "efeda1c3-31eb-4a46-9cf1-2286508753de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01b4327-2326-4671-badd-ce99047c3aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff0cb407-d440-4168-b6c2-9ee9edeb0a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01b4327-2326-4671-badd-ce99047c3aad",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "217acecb-1c10-4b4f-ad4a-02b67b768311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "c9ba5ca0-9f5a-48c7-b26e-f1a42a3fc881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217acecb-1c10-4b4f-ad4a-02b67b768311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe83d70-d431-439e-962e-426217fec074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217acecb-1c10-4b4f-ad4a-02b67b768311",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        },
        {
            "id": "4df4d55f-341d-4afc-8d41-961762c8653e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "compositeImage": {
                "id": "4f3dda39-d679-4943-b98f-88c85ccdb7cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df4d55f-341d-4afc-8d41-961762c8653e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf32ae9-782a-4097-941b-468051de3fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df4d55f-341d-4afc-8d41-961762c8653e",
                    "LayerId": "feb2c942-4681-4096-b642-67816f2bdffa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "feb2c942-4681-4096-b642-67816f2bdffa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc1be9e0-c085-4d5c-80ff-b314eed6db74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}