{
    "id": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_na_a_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "addfd086-dba1-4251-801f-09de48169492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "c132b3b6-1a51-409e-8b26-af8eb93e26c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "addfd086-dba1-4251-801f-09de48169492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f2c713-e7dd-4eb6-b1dc-79447623dde2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "addfd086-dba1-4251-801f-09de48169492",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "7cb0495f-4063-4d72-892e-be7dbfdcf6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "addfd086-dba1-4251-801f-09de48169492",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        },
        {
            "id": "bf8d2d2c-0d9c-4f4d-bdc9-13e8d575c1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "318337f0-1a58-4133-a2fe-bdff82b947c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf8d2d2c-0d9c-4f4d-bdc9-13e8d575c1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8a629c-eee2-487f-bee9-3a8b3e772bba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf8d2d2c-0d9c-4f4d-bdc9-13e8d575c1e2",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "28e9aa35-2bfb-4b1d-acb5-961be912b44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf8d2d2c-0d9c-4f4d-bdc9-13e8d575c1e2",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        },
        {
            "id": "46515e9d-fa32-44d7-9628-3b9b76cbd043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "d40483df-5a3e-4486-9165-4f9b60cf9770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46515e9d-fa32-44d7-9628-3b9b76cbd043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16912c0-2b5e-4a8b-8dee-01b75cfdce60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46515e9d-fa32-44d7-9628-3b9b76cbd043",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "ba377ada-9bcc-49df-8f07-238d6cc76157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46515e9d-fa32-44d7-9628-3b9b76cbd043",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        },
        {
            "id": "e79d1d65-a6c0-46d5-9258-4c38d0aad263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "085c0921-0a8f-4c69-a1ca-664717acd54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79d1d65-a6c0-46d5-9258-4c38d0aad263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "742f5d52-823a-4b04-bcd2-b81edf53ce59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79d1d65-a6c0-46d5-9258-4c38d0aad263",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "e4af2f78-2cc0-4d76-a486-e5f116f01b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79d1d65-a6c0-46d5-9258-4c38d0aad263",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        },
        {
            "id": "945c4118-b2b6-4088-88e0-95e9c30ca503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "012e22dc-336e-4ded-9bdf-2a8c625a5528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945c4118-b2b6-4088-88e0-95e9c30ca503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb722c3e-c51d-4d4b-8bac-50f7e6e61e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945c4118-b2b6-4088-88e0-95e9c30ca503",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "40e440d1-927b-4fd3-8ed8-dca309e22f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945c4118-b2b6-4088-88e0-95e9c30ca503",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        },
        {
            "id": "d6f08d7e-1b26-4c0d-8c65-42c77de5983c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "compositeImage": {
                "id": "6fcf713d-96c2-402d-902f-285cef42f53c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f08d7e-1b26-4c0d-8c65-42c77de5983c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a18d325e-9233-4000-9c3e-d14674e78473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f08d7e-1b26-4c0d-8c65-42c77de5983c",
                    "LayerId": "2e7ffb38-400f-438c-b45a-451db5d47c2e"
                },
                {
                    "id": "273fd9b9-df0c-4e19-a1f9-f5abc734064b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f08d7e-1b26-4c0d-8c65-42c77de5983c",
                    "LayerId": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e7ffb38-400f-438c-b45a-451db5d47c2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2dedbc0f-2eee-4f07-b4fc-daf344c885e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57532b6e-e933-4d9b-be0c-bcf136890e0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}