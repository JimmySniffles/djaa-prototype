{
    "id": "59d500e9-2458-4604-b396-145e35a05f13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_rbsnake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c850e146-76a9-4d27-af04-4f54cf619d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d500e9-2458-4604-b396-145e35a05f13",
            "compositeImage": {
                "id": "0331705c-0dfd-42e0-b14e-590b6dc1abae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c850e146-76a9-4d27-af04-4f54cf619d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc3d252-ac43-425e-a1cf-7be11852a6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c850e146-76a9-4d27-af04-4f54cf619d4d",
                    "LayerId": "f419aea4-da65-4855-800b-1642972b524e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "f419aea4-da65-4855-800b-1642972b524e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59d500e9-2458-4604-b396-145e35a05f13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}