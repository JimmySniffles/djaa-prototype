{
    "id": "039c7243-360c-4682-843e-3cde3f3ad474",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2cda62a-5b4a-41dd-89e8-e50cd448b920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "039c7243-360c-4682-843e-3cde3f3ad474",
            "compositeImage": {
                "id": "2b882366-b362-4ec7-81be-9ea50f46f025",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2cda62a-5b4a-41dd-89e8-e50cd448b920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b9a942-d394-4de6-aacf-2b23e1326123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2cda62a-5b4a-41dd-89e8-e50cd448b920",
                    "LayerId": "d8089266-ce34-448c-902f-9565a0af5889"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "d8089266-ce34-448c-902f-9565a0af5889",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "039c7243-360c-4682-843e-3cde3f3ad474",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}