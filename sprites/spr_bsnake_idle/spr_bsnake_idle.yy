{
    "id": "1595166b-620d-496c-a39f-56c8743b665b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bsnake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ed33b24-edd0-4e36-8567-b956b5cfbf11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1595166b-620d-496c-a39f-56c8743b665b",
            "compositeImage": {
                "id": "4dd2ea72-0a7e-4fe0-93fe-51b8662f439a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed33b24-edd0-4e36-8567-b956b5cfbf11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a9bb2d-97d4-4f69-9756-a977d7e3f74a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed33b24-edd0-4e36-8567-b956b5cfbf11",
                    "LayerId": "9cf20c29-228b-4046-993c-9b3de4e68734"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "9cf20c29-228b-4046-993c-9b3de4e68734",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1595166b-620d-496c-a39f-56c8743b665b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}