{
    "id": "22109f99-32ef-4460-b80f-3a5d88f282cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lowanna_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 49,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e6f28e4-06a4-44ce-8fc8-f6585a2b732d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22109f99-32ef-4460-b80f-3a5d88f282cc",
            "compositeImage": {
                "id": "225947ba-9ba4-4ff0-aba9-ab285d64188c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e6f28e4-06a4-44ce-8fc8-f6585a2b732d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73be791e-9213-4d8e-b4f2-ba171d293619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e6f28e4-06a4-44ce-8fc8-f6585a2b732d",
                    "LayerId": "dddc2571-d3bf-4e47-a661-d7c393f6903a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dddc2571-d3bf-4e47-a661-d7c393f6903a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22109f99-32ef-4460-b80f-3a5d88f282cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}