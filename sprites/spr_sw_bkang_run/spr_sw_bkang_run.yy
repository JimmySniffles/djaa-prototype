{
    "id": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkang_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a644ca5-6382-4e86-be97-ca9dbcd5d66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "61c99fe5-a05b-4eb0-8d9d-b5d98ed828a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a644ca5-6382-4e86-be97-ca9dbcd5d66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e74b12-738e-4185-bf62-89b30a7359e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a644ca5-6382-4e86-be97-ca9dbcd5d66f",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "0d30a06c-fc33-4c1c-80a8-e41842903dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "fc666d89-2cb1-4893-bc57-135cf7884745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d30a06c-fc33-4c1c-80a8-e41842903dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0eddca-25c7-45aa-9d5f-a80a10bcca8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d30a06c-fc33-4c1c-80a8-e41842903dd5",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "859f6504-ffdc-4846-a2b8-364e2d188e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "e9f19804-08cf-45a6-9cdc-d1511580f0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "859f6504-ffdc-4846-a2b8-364e2d188e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688c6e56-dee8-4bd4-b13d-44d8e41aa01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "859f6504-ffdc-4846-a2b8-364e2d188e2a",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "71def8a7-2749-416a-9ad0-b99ab324d6da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "18296657-e68d-4539-a795-ae64ce07e211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71def8a7-2749-416a-9ad0-b99ab324d6da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34cf261f-1b4d-42fc-b79f-19702efd5eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71def8a7-2749-416a-9ad0-b99ab324d6da",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "d31882b7-7843-453e-8c3e-188f6187125a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "a92c52e0-44db-42a3-896f-77760f6eb7ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31882b7-7843-453e-8c3e-188f6187125a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4232753d-da79-4ea4-8aa8-c1ac107d188f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31882b7-7843-453e-8c3e-188f6187125a",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "b2ba060e-9d06-4f50-bf0a-51fea647d773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "7a2df650-3354-48fc-9225-018dae00c4a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ba060e-9d06-4f50-bf0a-51fea647d773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5769604-4ae8-412c-bd36-c848f0683975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ba060e-9d06-4f50-bf0a-51fea647d773",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "08a82ebe-7c3d-4ee0-8b28-97a3c221be82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "95216f35-fc15-45d0-8008-969fcc911fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a82ebe-7c3d-4ee0-8b28-97a3c221be82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc514a88-7224-4c1e-a144-97092f690a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a82ebe-7c3d-4ee0-8b28-97a3c221be82",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "71987413-8a07-4395-94ac-fb09d0da2943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "d955053f-7e7a-4733-bbd8-c94b5d958928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71987413-8a07-4395-94ac-fb09d0da2943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cf98b1-3c53-43e1-b035-8ee67c766a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71987413-8a07-4395-94ac-fb09d0da2943",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        },
        {
            "id": "94b31978-36ed-4fb9-b2e2-4f8de8ab4764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "compositeImage": {
                "id": "751c159a-ef55-4e74-a154-6a4b433f631f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b31978-36ed-4fb9-b2e2-4f8de8ab4764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136ed977-b319-4143-af3c-504fb0dd1b1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b31978-36ed-4fb9-b2e2-4f8de8ab4764",
                    "LayerId": "9d82aa18-1b2a-422d-8a73-3d38f70acac5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "9d82aa18-1b2a-422d-8a73-3d38f70acac5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47347545-53fe-49d3-ac1a-2d34a9c4e190",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}