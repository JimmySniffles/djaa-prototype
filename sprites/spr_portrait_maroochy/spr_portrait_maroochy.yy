{
    "id": "fed89d98-2063-4cb6-b092-0a139a1a8cab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_maroochy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 9,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fc5bf64-5970-4c16-9a89-15fef0dba1a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed89d98-2063-4cb6-b092-0a139a1a8cab",
            "compositeImage": {
                "id": "e98908cf-f749-4dc8-97f7-db412bee02c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fc5bf64-5970-4c16-9a89-15fef0dba1a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf2d241-0bc2-4f9e-a4c0-ebb47d776183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fc5bf64-5970-4c16-9a89-15fef0dba1a4",
                    "LayerId": "dc008a7b-89b3-4042-9eff-2bca8d80bddf"
                }
            ]
        },
        {
            "id": "24c7ce7c-ebc2-45c7-be5c-e9530c33af3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed89d98-2063-4cb6-b092-0a139a1a8cab",
            "compositeImage": {
                "id": "61b433d1-22af-415f-a2b5-94c465e80138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c7ce7c-ebc2-45c7-be5c-e9530c33af3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb921a1-2f53-4920-9dc0-90c8c0b2f927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c7ce7c-ebc2-45c7-be5c-e9530c33af3a",
                    "LayerId": "dc008a7b-89b3-4042-9eff-2bca8d80bddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "dc008a7b-89b3-4042-9eff-2bca8d80bddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fed89d98-2063-4cb6-b092-0a139a1a8cab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}