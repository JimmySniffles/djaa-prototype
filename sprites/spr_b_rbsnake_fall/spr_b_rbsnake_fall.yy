{
    "id": "d8280a97-eb49-460e-af72-393002bd4dc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_rbsnake_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9cc9284-d339-4aa5-b387-afbfc8dd328f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "bf7870a1-69c1-4630-9dbc-6dc9476ed943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9cc9284-d339-4aa5-b387-afbfc8dd328f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8ae616-0a26-4173-8e4f-6cb28510fb29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9cc9284-d339-4aa5-b387-afbfc8dd328f",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "58c588de-7330-4e36-b8cd-041df95945a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "3b42895e-871b-4ad6-8463-52a8befa2211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c588de-7330-4e36-b8cd-041df95945a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920f9aa8-85e7-4edf-8cc2-5a7e99821422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c588de-7330-4e36-b8cd-041df95945a1",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "3f7a9070-9260-4225-ac9e-043fb85f9417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "a7f84163-b09c-4e51-806d-e04f1d65fca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f7a9070-9260-4225-ac9e-043fb85f9417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b96572-ef20-4bf9-a832-10ef09545fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f7a9070-9260-4225-ac9e-043fb85f9417",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "dd630d2f-bde9-4248-9078-be5977629437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "4d3e5291-b6c4-4fe3-bf4a-c3469e629d11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd630d2f-bde9-4248-9078-be5977629437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2187e1-6577-46eb-9b0e-295bc2609f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd630d2f-bde9-4248-9078-be5977629437",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "a57c815c-9b54-4cf7-b7b9-046fb543f136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "1e167325-d7a2-460a-9ab3-1c6f2abc4b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a57c815c-9b54-4cf7-b7b9-046fb543f136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3bebc0-85de-434e-9bf1-3e8817422aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a57c815c-9b54-4cf7-b7b9-046fb543f136",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "a2989c8c-deb4-432f-8227-3e2e8ca6b076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "26078902-8d9a-443b-a34e-3472ed6b4635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2989c8c-deb4-432f-8227-3e2e8ca6b076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "885cecc9-3b0e-4cee-a75f-1c5e62ace936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2989c8c-deb4-432f-8227-3e2e8ca6b076",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "ea9a93a6-3dd4-4030-b555-29543debec1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "c2420250-f195-459d-9049-cb3bdf406d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea9a93a6-3dd4-4030-b555-29543debec1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2e22f0-115b-43d6-8066-f77dc8bc25a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea9a93a6-3dd4-4030-b555-29543debec1c",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        },
        {
            "id": "103382f5-c4f9-4d8d-9454-ceeca903d9bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "compositeImage": {
                "id": "3fa0d376-f3f7-4401-b4f9-ac6e81a01f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103382f5-c4f9-4d8d-9454-ceeca903d9bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad027b78-cefe-4e63-b13b-19687c4e2d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103382f5-c4f9-4d8d-9454-ceeca903d9bf",
                    "LayerId": "9fd1fb87-1405-49ed-9d29-b646ed332ef4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "9fd1fb87-1405-49ed-9d29-b646ed332ef4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8280a97-eb49-460e-af72-393002bd4dc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}