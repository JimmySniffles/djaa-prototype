{
    "id": "e5d0e8d7-b957-4237-a503-32481e0bf512",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_snake_attack_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef9277fc-ce51-405c-8bb0-e72ed7dbd1b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "091c09dd-6f67-4bf2-a96d-a662c6f4f97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef9277fc-ce51-405c-8bb0-e72ed7dbd1b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd1eae7-11f0-40a2-a311-243f370baa0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef9277fc-ce51-405c-8bb0-e72ed7dbd1b1",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "04e4b871-8cc5-424c-b9d8-0c7a7a9689c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef9277fc-ce51-405c-8bb0-e72ed7dbd1b1",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "d04c7746-4193-4f61-b377-2b4ee502a356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "af3e2eed-c51d-4d89-b6f3-4abaaac45505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d04c7746-4193-4f61-b377-2b4ee502a356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd3fdd1c-b215-4668-b101-7c230e5e1a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d04c7746-4193-4f61-b377-2b4ee502a356",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "38d23815-5d9a-4716-bf39-ed275f00fb2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d04c7746-4193-4f61-b377-2b4ee502a356",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "91ed6a21-2317-4b18-8168-46064ce2d717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "b272e7a9-b103-40fd-a1f0-8953f61ef281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ed6a21-2317-4b18-8168-46064ce2d717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619bb2c8-9088-4ee5-9209-9ec73cfc3653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ed6a21-2317-4b18-8168-46064ce2d717",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                },
                {
                    "id": "7b880272-a7f8-40e4-a453-7795b3a0caf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ed6a21-2317-4b18-8168-46064ce2d717",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                }
            ]
        },
        {
            "id": "dee160aa-c324-4b29-a0eb-0712d454455d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "075e6ac9-21b3-4c45-85d6-19d4a0a24b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee160aa-c324-4b29-a0eb-0712d454455d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e48d74-44ae-4c7c-b086-6bcd3492d1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee160aa-c324-4b29-a0eb-0712d454455d",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "21102723-c148-430d-9d12-7ce111c7b0f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee160aa-c324-4b29-a0eb-0712d454455d",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "6e30d71a-d5bb-41fa-94b4-3988970297ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "84e0676c-20a6-403f-9bd2-cd70278c1f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e30d71a-d5bb-41fa-94b4-3988970297ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5c3333-8a28-478a-93d5-1cc64500b8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e30d71a-d5bb-41fa-94b4-3988970297ae",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "a71fd587-9581-4fd9-8131-66edf96c2f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e30d71a-d5bb-41fa-94b4-3988970297ae",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "6dfa2027-fde4-4393-b43a-1f854bb2c521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "caadc310-d07f-4d52-95a4-db2395a73e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dfa2027-fde4-4393-b43a-1f854bb2c521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9c1190-9837-4055-a81f-d526ed339162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfa2027-fde4-4393-b43a-1f854bb2c521",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                },
                {
                    "id": "c38a0cbb-016e-45dc-a641-9477c05538c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfa2027-fde4-4393-b43a-1f854bb2c521",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                }
            ]
        },
        {
            "id": "0942affa-ab29-4838-9051-6089ecbffb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "2b875c06-1242-40aa-8c85-11425348a12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0942affa-ab29-4838-9051-6089ecbffb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778637de-ae58-4e36-b798-fcf298ce3c96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0942affa-ab29-4838-9051-6089ecbffb5b",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "08ab479e-b8e9-45d4-8cb0-394855294fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0942affa-ab29-4838-9051-6089ecbffb5b",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "462aba8f-94bc-4a33-96f5-7a5637c49a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "53906bef-cb40-4ea6-a2b7-bb2f4a2076fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462aba8f-94bc-4a33-96f5-7a5637c49a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0694a39-6198-48bb-9532-e8eab1d50220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462aba8f-94bc-4a33-96f5-7a5637c49a96",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "6747e965-7bd5-4a3a-b57c-e19f8a261679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462aba8f-94bc-4a33-96f5-7a5637c49a96",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "f78d2906-e68e-4a14-91e5-8e61ffb3d988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "6e396854-dbae-407c-9bad-73b813b7b2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78d2906-e68e-4a14-91e5-8e61ffb3d988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da64d9c6-173a-4d80-948d-f36eccc63884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78d2906-e68e-4a14-91e5-8e61ffb3d988",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "fc175287-1ab7-4f3c-b39c-e7c1a1f3e1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78d2906-e68e-4a14-91e5-8e61ffb3d988",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "3e290310-bf27-4ead-a1f9-0ac7d889f714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "d0601d61-560e-4177-bb33-e4dbece9382b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e290310-bf27-4ead-a1f9-0ac7d889f714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f9aaa9-cd4d-4417-8e7f-9d4f4db0c8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e290310-bf27-4ead-a1f9-0ac7d889f714",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "e8699d56-7a2e-46f1-9456-c1a520703260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e290310-bf27-4ead-a1f9-0ac7d889f714",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "b5fef6bf-8416-40f0-b269-c36e8da3830a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "439a5bd0-14df-4a3b-b983-e3268bbade50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fef6bf-8416-40f0-b269-c36e8da3830a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefe505e-e0df-468e-a957-54bb2c76349c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fef6bf-8416-40f0-b269-c36e8da3830a",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "7e3fbdd1-090a-4f63-90b5-6b5bcf92ad83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fef6bf-8416-40f0-b269-c36e8da3830a",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "ffcd891d-677c-4675-ac44-980202f20fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "a9fdd2c9-5e3d-4f21-9735-c5e05f1cfe8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffcd891d-677c-4675-ac44-980202f20fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070b2e38-0211-447c-a3c0-9aed6e6c6184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffcd891d-677c-4675-ac44-980202f20fc9",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "87206bab-3bbc-4a23-b969-215dac9f4df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffcd891d-677c-4675-ac44-980202f20fc9",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        },
        {
            "id": "b7d920e9-92d7-4e20-a3d7-04440a6b9f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "compositeImage": {
                "id": "d7524084-6b6b-4e92-a502-5337a12f37cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d920e9-92d7-4e20-a3d7-04440a6b9f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93bf7cf6-e87f-4d67-893a-3f78598329bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d920e9-92d7-4e20-a3d7-04440a6b9f5a",
                    "LayerId": "a9fcd651-633f-4a12-9bfd-68050337cafd"
                },
                {
                    "id": "efc35ec6-7b27-4639-8155-cc85f3065435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d920e9-92d7-4e20-a3d7-04440a6b9f5a",
                    "LayerId": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "17a80ffc-e6ca-4f3e-88cb-3b96898e0b94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a9fcd651-633f-4a12-9bfd-68050337cafd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5d0e8d7-b957-4237-a503-32481e0bf512",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}