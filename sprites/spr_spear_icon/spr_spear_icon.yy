{
    "id": "50515fc2-e0a9-4ff7-8acd-916953987c22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spear_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fbcfa40-bdb5-4973-9770-46b4a581ef57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50515fc2-e0a9-4ff7-8acd-916953987c22",
            "compositeImage": {
                "id": "9ce22713-16be-4514-89b6-d72f5743c290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbcfa40-bdb5-4973-9770-46b4a581ef57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa9bed3-2f3c-466d-9c8e-22da3cd4d6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbcfa40-bdb5-4973-9770-46b4a581ef57",
                    "LayerId": "b54a2591-9690-42bf-8228-ceb3862683cf"
                }
            ]
        },
        {
            "id": "8cbfe46d-631c-464f-bf78-b5aaf9a804ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50515fc2-e0a9-4ff7-8acd-916953987c22",
            "compositeImage": {
                "id": "9a91c163-d062-46c9-be07-ccfdd5887c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbfe46d-631c-464f-bf78-b5aaf9a804ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee06850e-a01f-4efe-9af3-b6c289ce8c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbfe46d-631c-464f-bf78-b5aaf9a804ec",
                    "LayerId": "b54a2591-9690-42bf-8228-ceb3862683cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b54a2591-9690-42bf-8228-ceb3862683cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50515fc2-e0a9-4ff7-8acd-916953987c22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 4
}