{
    "id": "c8196fc0-2da8-4806-a6be-4862e2891eb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portait_ninderry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 117,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f9cf3aa-1866-4cba-bca8-738b760be848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8196fc0-2da8-4806-a6be-4862e2891eb8",
            "compositeImage": {
                "id": "93f0c673-6413-463f-bfdc-f7169e9a68a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9cf3aa-1866-4cba-bca8-738b760be848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3def7dda-31fa-4c58-b749-3534444a4fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9cf3aa-1866-4cba-bca8-738b760be848",
                    "LayerId": "93a03e41-a2d4-4f0b-987b-f2c1f131fd82"
                }
            ]
        },
        {
            "id": "2088d154-2a0b-44a7-9d91-77e00019c2f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8196fc0-2da8-4806-a6be-4862e2891eb8",
            "compositeImage": {
                "id": "5acebd5f-98d0-4d8c-b347-3226ac087d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2088d154-2a0b-44a7-9d91-77e00019c2f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5d2197-1f93-4d79-8322-687eafb655ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2088d154-2a0b-44a7-9d91-77e00019c2f9",
                    "LayerId": "93a03e41-a2d4-4f0b-987b-f2c1f131fd82"
                }
            ]
        },
        {
            "id": "1fdc43f5-b5dc-43ff-b736-c160b2b56c46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8196fc0-2da8-4806-a6be-4862e2891eb8",
            "compositeImage": {
                "id": "e23c21e9-5278-456b-a3ff-4d02d700f682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fdc43f5-b5dc-43ff-b736-c160b2b56c46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ea8f74-d151-4d6e-ad8d-4152cdea600a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fdc43f5-b5dc-43ff-b736-c160b2b56c46",
                    "LayerId": "93a03e41-a2d4-4f0b-987b-f2c1f131fd82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "93a03e41-a2d4-4f0b-987b-f2c1f131fd82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8196fc0-2da8-4806-a6be-4862e2891eb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}