{
    "id": "2e7605ea-de3a-4a37-a8aa-4c8378c851a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_waystones",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52f035f5-dc71-4e0e-906b-74923863f9dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e7605ea-de3a-4a37-a8aa-4c8378c851a8",
            "compositeImage": {
                "id": "da010d6c-4741-4363-ac92-5f58b4869dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f035f5-dc71-4e0e-906b-74923863f9dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7522da3-8da6-437e-9fba-1dcc85307528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f035f5-dc71-4e0e-906b-74923863f9dc",
                    "LayerId": "01041f48-02b2-4a8e-a0b0-e8d9bf814e8d"
                }
            ]
        },
        {
            "id": "0fc5eff2-89ad-45ee-b332-4b9705ccb665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e7605ea-de3a-4a37-a8aa-4c8378c851a8",
            "compositeImage": {
                "id": "a41e4875-ac75-486c-a738-8dc75056c542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc5eff2-89ad-45ee-b332-4b9705ccb665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2eb8bd9-52aa-4d0c-a45e-c051fe45a292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc5eff2-89ad-45ee-b332-4b9705ccb665",
                    "LayerId": "01041f48-02b2-4a8e-a0b0-e8d9bf814e8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01041f48-02b2-4a8e-a0b0-e8d9bf814e8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e7605ea-de3a-4a37-a8aa-4c8378c851a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}