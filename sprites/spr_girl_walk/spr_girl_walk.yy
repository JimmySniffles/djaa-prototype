{
    "id": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04ac8ffc-b735-47e7-969e-81216cc51530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "5a26ec68-fc3a-4006-b159-7d3c0a6f395d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04ac8ffc-b735-47e7-969e-81216cc51530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd167b2-2f56-47b2-bc78-e8a9494e2c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04ac8ffc-b735-47e7-969e-81216cc51530",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "81944908-cb2e-4d38-87e7-419a863fffb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "ffe92287-a46a-4b59-95fe-fefc3f1590ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81944908-cb2e-4d38-87e7-419a863fffb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae9cb87-8a06-4390-8c3b-d199db55d663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81944908-cb2e-4d38-87e7-419a863fffb9",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "598fbbe2-8950-4eac-803b-835cabefd454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "a1181d76-3777-4680-932f-7df507a7dfcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598fbbe2-8950-4eac-803b-835cabefd454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f14f8e-3633-437c-8917-c48c4801e9c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598fbbe2-8950-4eac-803b-835cabefd454",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "0afc85c2-113a-4955-a2af-6c01f17e3ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "968a1bc5-0278-4f3c-be69-9435e5fdb9bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0afc85c2-113a-4955-a2af-6c01f17e3ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5726bd50-2754-428b-bfd4-ca176f414715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0afc85c2-113a-4955-a2af-6c01f17e3ad5",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "b6987893-6013-43fd-9197-bc5c29441a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "4496d72b-7684-467c-92a1-ac9e28e55846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6987893-6013-43fd-9197-bc5c29441a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257cf6fe-5ae1-488e-b337-03508b72a136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6987893-6013-43fd-9197-bc5c29441a78",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "62bb16ec-4d7c-4a04-9c22-d667d5258e60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "27add5fe-b5de-4ae7-8fa4-ec2aa29ac5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62bb16ec-4d7c-4a04-9c22-d667d5258e60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee7fa86-2c98-429c-91b2-e9fef2e656f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62bb16ec-4d7c-4a04-9c22-d667d5258e60",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "e5d1d8dc-31bd-4b5e-998f-6f285dc00277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "6d94d5ef-13af-43b1-aab7-d55f25f743f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d1d8dc-31bd-4b5e-998f-6f285dc00277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c85b3f-caf9-4c18-99d7-89ba96ffc542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d1d8dc-31bd-4b5e-998f-6f285dc00277",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "56200ba5-7575-4e00-9e66-6fa2bfb6c3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "9f70fbe1-ce84-4795-9f84-16f5bf22cb4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56200ba5-7575-4e00-9e66-6fa2bfb6c3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c191213c-e429-460b-94fa-78eeb801fa92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56200ba5-7575-4e00-9e66-6fa2bfb6c3dc",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "d23145e8-f6a2-444c-aaf4-029f52935544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "6e0e0cf2-5772-40c3-b5c6-396c22d2888b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23145e8-f6a2-444c-aaf4-029f52935544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2398f382-43eb-40dc-b32d-03e2f3eefac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23145e8-f6a2-444c-aaf4-029f52935544",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "ce904915-8fe7-47ba-a79e-ed6904345e4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "95ae9068-eabf-4bc2-a082-f646c9f7c796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce904915-8fe7-47ba-a79e-ed6904345e4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd940f8a-a634-43fd-810a-4ffed17ee5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce904915-8fe7-47ba-a79e-ed6904345e4d",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "6c415ca9-0cd4-431a-a139-5e57a4e4a0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "b1af021b-db11-458d-ad78-0b13732e1791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c415ca9-0cd4-431a-a139-5e57a4e4a0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44130d82-56da-436a-917a-0b6e3bf80448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c415ca9-0cd4-431a-a139-5e57a4e4a0e8",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "e39ffe0a-e2f2-4cad-a3cc-c7573e1fe7b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "443b346b-4423-4da5-9797-098fea43f671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39ffe0a-e2f2-4cad-a3cc-c7573e1fe7b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9a3130-623a-444f-8b5d-79f3909e438e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39ffe0a-e2f2-4cad-a3cc-c7573e1fe7b0",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "51e57037-b8da-491e-8878-2dfe576a702a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "5c9f048a-b2a9-424a-8a36-d46f14e4e706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51e57037-b8da-491e-8878-2dfe576a702a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32966093-e6ed-4854-8c17-d45ed039a18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51e57037-b8da-491e-8878-2dfe576a702a",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "4cd15fd2-6c63-4647-aa10-4de362f89de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "f9b711f5-373e-4a84-8e3b-fad9d8e8e8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd15fd2-6c63-4647-aa10-4de362f89de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "908f3c89-2c43-422d-aa19-a512b4fc35b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd15fd2-6c63-4647-aa10-4de362f89de8",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "57a34673-2c5c-4ace-affa-1086050e96f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "ab73b4ee-f814-482e-a7ff-7e0b95d4bace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a34673-2c5c-4ace-affa-1086050e96f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d169a559-07f2-4b0e-8efd-ac29b8f57d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a34673-2c5c-4ace-affa-1086050e96f3",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "149c2ce1-126c-4f98-bf57-eb89994b198c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "aee44815-37b3-4ecd-bc0f-7d44fcd2f7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "149c2ce1-126c-4f98-bf57-eb89994b198c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c886525d-b812-4a7d-b240-900df60bdfc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "149c2ce1-126c-4f98-bf57-eb89994b198c",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "2780f95a-6168-4c1f-98e5-2c4c20dd255e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "e84693fe-b96f-490d-b851-4bef7e35b23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2780f95a-6168-4c1f-98e5-2c4c20dd255e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc8ab9d-e800-42e9-bbe7-770782ef5acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2780f95a-6168-4c1f-98e5-2c4c20dd255e",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        },
        {
            "id": "09ea5c77-263e-4254-900d-154470110d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "compositeImage": {
                "id": "bb19a517-0cfc-4720-acdc-b4d21f26f44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ea5c77-263e-4254-900d-154470110d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434675a4-8766-4120-b027-14c7ddb11f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ea5c77-263e-4254-900d-154470110d28",
                    "LayerId": "a4c60682-d740-4730-849b-678e478b74c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4c60682-d740-4730-849b-678e478b74c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dca4f2e7-0c92-4c7c-953f-43ec0ead5127",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}