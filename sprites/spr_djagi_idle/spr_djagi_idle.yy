{
    "id": "43a53aea-0372-4795-9180-43dabde548af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_djagi_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 40,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5fafc45-4072-46d6-8ae4-fd03a9816ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a53aea-0372-4795-9180-43dabde548af",
            "compositeImage": {
                "id": "e59810a3-de5f-435d-99bb-9b1630021fda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5fafc45-4072-46d6-8ae4-fd03a9816ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff69940-0c3b-4127-b81e-64976850dbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5fafc45-4072-46d6-8ae4-fd03a9816ebf",
                    "LayerId": "835e2d26-669c-44ed-9512-2dc697256a82"
                }
            ]
        },
        {
            "id": "6ef9ef7d-68ea-49d1-aa74-2c2c2adff86a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a53aea-0372-4795-9180-43dabde548af",
            "compositeImage": {
                "id": "3fbe84c1-c6d5-4f7d-acd7-c09d0f27c5e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef9ef7d-68ea-49d1-aa74-2c2c2adff86a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e10a812-5e1f-4a1b-b6b3-9492178dbeaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef9ef7d-68ea-49d1-aa74-2c2c2adff86a",
                    "LayerId": "835e2d26-669c-44ed-9512-2dc697256a82"
                }
            ]
        },
        {
            "id": "6fbe902c-d935-4994-926e-b4dc0b2e019e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43a53aea-0372-4795-9180-43dabde548af",
            "compositeImage": {
                "id": "2e80e601-01fb-4a91-ba73-4482e170ab0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbe902c-d935-4994-926e-b4dc0b2e019e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eeeda42-be92-49f1-ba8b-226fa982c6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbe902c-d935-4994-926e-b4dc0b2e019e",
                    "LayerId": "835e2d26-669c-44ed-9512-2dc697256a82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "835e2d26-669c-44ed-9512-2dc697256a82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43a53aea-0372-4795-9180-43dabde548af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}