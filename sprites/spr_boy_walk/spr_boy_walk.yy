{
    "id": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9c2da45-c09e-4267-85fe-d689601ca5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "81434c59-077e-47e4-a92d-7e9430f7a041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c2da45-c09e-4267-85fe-d689601ca5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8266c55-20e4-4753-8fe3-d7ff6d4e03bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c2da45-c09e-4267-85fe-d689601ca5c0",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "08af302a-9068-4022-8fc3-98cb7cf5f0a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "e8ad9658-446b-40e4-b249-f958365c298e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08af302a-9068-4022-8fc3-98cb7cf5f0a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e505a5-c615-4a97-890d-5f8cea671ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08af302a-9068-4022-8fc3-98cb7cf5f0a7",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "f2d286dc-8443-47ba-862b-69550b924d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "0f55d9a1-cb5a-47ae-951a-89d82f8a4efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d286dc-8443-47ba-862b-69550b924d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d7c165-bc11-4cab-bce5-0fa8fd1e5b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d286dc-8443-47ba-862b-69550b924d00",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "24516a61-9bf5-4f2c-afac-bf37ba0c5f18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "6f768fa7-b134-498e-8016-9443135ca8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24516a61-9bf5-4f2c-afac-bf37ba0c5f18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c78e6bf-685b-4279-a488-d79ebabb3c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24516a61-9bf5-4f2c-afac-bf37ba0c5f18",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "9b1232c5-8999-4283-9222-5163f441d111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "2d018799-0d9b-4705-8a0a-4e8a552d90e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b1232c5-8999-4283-9222-5163f441d111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf11763-469a-4f03-b42a-fd15b9de1236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b1232c5-8999-4283-9222-5163f441d111",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "05bd1724-b3a2-45d5-8710-d5f6eb131c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "61d9eff9-2ab7-4833-ba75-657a9e728405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05bd1724-b3a2-45d5-8710-d5f6eb131c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db525b1-f22c-455e-8d18-0780cbaca5f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05bd1724-b3a2-45d5-8710-d5f6eb131c70",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "b928bd23-e902-494e-b743-77046b51bfd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "9df9ec67-9cfa-4f4b-870d-226e1e555b2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b928bd23-e902-494e-b743-77046b51bfd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63262a37-0618-4bf1-a1dc-c6baf8ae096b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b928bd23-e902-494e-b743-77046b51bfd4",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "63c22920-1e51-4092-9206-4b309bf4b1cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "98e3c8b4-6ead-4142-abee-8f2670da6167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c22920-1e51-4092-9206-4b309bf4b1cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b48163c-695a-42e8-9aa0-a80137641d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c22920-1e51-4092-9206-4b309bf4b1cb",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "bb0d6610-01ac-4c15-944c-a78690802abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "842993da-a300-4ca2-9708-6fe5f1d24f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb0d6610-01ac-4c15-944c-a78690802abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeed1de5-ab33-45fe-ae4e-04de68f35693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb0d6610-01ac-4c15-944c-a78690802abd",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "1e6dcc4f-86f4-4b19-adf2-d52549351c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "d3a0a894-c94b-4f89-8afc-b4f6fe9ff9e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e6dcc4f-86f4-4b19-adf2-d52549351c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5942e4e6-694e-46a9-b90e-bb250ba06588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6dcc4f-86f4-4b19-adf2-d52549351c63",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "f9796e1a-709d-4a53-995f-76a8041d2103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "d490e8c4-7863-4157-a702-7b51b876258f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9796e1a-709d-4a53-995f-76a8041d2103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8f18f2-7afc-4921-af9d-33aba7b708d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9796e1a-709d-4a53-995f-76a8041d2103",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "95f5cade-1afd-4144-a71e-d575258c7b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "81e6f205-66d7-49cd-a677-e9c8543fba7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95f5cade-1afd-4144-a71e-d575258c7b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4529772-145d-439d-a482-cfbb72f0a19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f5cade-1afd-4144-a71e-d575258c7b04",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "20759595-dcbe-4c29-92c8-f1b58503f6b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "08a289a5-4c6b-444a-9d86-0d4704966328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20759595-dcbe-4c29-92c8-f1b58503f6b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a5433b-8954-442f-b2ea-71daa2e71945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20759595-dcbe-4c29-92c8-f1b58503f6b0",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "2f75f9f0-c014-4846-9326-a9cc9b655f41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "77960e78-1447-443a-8993-361c8985320a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f75f9f0-c014-4846-9326-a9cc9b655f41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02410bad-9ca6-4469-b5a9-a66b1b9375bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f75f9f0-c014-4846-9326-a9cc9b655f41",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "a494b0a8-26a1-4acc-9581-44025fa04f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "00893183-24ae-4c80-96e0-0af837b30bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a494b0a8-26a1-4acc-9581-44025fa04f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c7f444e-038a-42d7-8f97-9a215a2caed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a494b0a8-26a1-4acc-9581-44025fa04f75",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "67f8a5a5-8109-4c39-9954-8a74e6c10188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "590d43b5-a4ef-4504-8fe7-ee4d2eab37a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f8a5a5-8109-4c39-9954-8a74e6c10188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "808bce6a-fc77-4e94-bbfa-a4329a38be3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f8a5a5-8109-4c39-9954-8a74e6c10188",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "4541b400-63de-4ad9-80e8-686788d57745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "42971c9b-e495-4b5b-810b-91e5f46430a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4541b400-63de-4ad9-80e8-686788d57745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e64a60e-4bbf-48b8-ab65-5b7ed24c7d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4541b400-63de-4ad9-80e8-686788d57745",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        },
        {
            "id": "a954471f-5f12-4a5b-8874-1169b1da6102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "compositeImage": {
                "id": "a261be37-47aa-4d53-b577-be2bd9826cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a954471f-5f12-4a5b-8874-1169b1da6102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d14ee78-68b4-429f-960f-72bfc330bfab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a954471f-5f12-4a5b-8874-1169b1da6102",
                    "LayerId": "0cd96e2d-e779-48c8-829d-bc4495f56c98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0cd96e2d-e779-48c8-829d-bc4495f56c98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e446639-5ac0-4542-8374-7d835cf4dd9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}