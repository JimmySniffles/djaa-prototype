{
    "id": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_dodge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9a797f9-027c-4908-af20-6136a149bbaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "e96a6cb9-e9bb-4343-96dd-82d5ca9ec680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a797f9-027c-4908-af20-6136a149bbaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994a08ed-0bdc-441d-bde4-1ec9a5558361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a797f9-027c-4908-af20-6136a149bbaf",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "0d840258-ce22-46b4-973b-f74ef8da50c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "c86bee5c-77f8-4272-81a3-b9571538bc2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d840258-ce22-46b4-973b-f74ef8da50c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93676d21-3886-4668-bd0c-15ac375da673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d840258-ce22-46b4-973b-f74ef8da50c4",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "782101e0-cf6e-4e2d-b8d5-d2aae35ca486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "6a73dc46-9c8a-448b-9379-f14733f5ca62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782101e0-cf6e-4e2d-b8d5-d2aae35ca486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68919a0-5f9a-4d01-a84d-dcbc74eb83b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782101e0-cf6e-4e2d-b8d5-d2aae35ca486",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "c58dba48-cf3f-4e40-96db-79d9bc905fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "bae42d31-a923-4f5d-97bb-165778d886ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c58dba48-cf3f-4e40-96db-79d9bc905fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e45c05a-d32b-449a-b09d-1b986e3c2256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c58dba48-cf3f-4e40-96db-79d9bc905fb5",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "8b6c24d3-a4d0-4732-8eb0-6f0c0e4ed7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "143aad3d-6806-4d90-b87f-4b5a9f83ba47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6c24d3-a4d0-4732-8eb0-6f0c0e4ed7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97ff25cd-6630-4db8-85a1-a239399dfe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6c24d3-a4d0-4732-8eb0-6f0c0e4ed7ff",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "d8f60afa-22a2-42db-9591-d425d804fb0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "79e26087-94c3-4d19-a027-718a8c906d40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f60afa-22a2-42db-9591-d425d804fb0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "870e81ea-1685-449c-8620-8227712cb68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f60afa-22a2-42db-9591-d425d804fb0f",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "4a847ccc-d6f4-447c-91d8-b5e39995670c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "1ff72b81-9dca-4f4d-bf4f-3f4173918802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a847ccc-d6f4-447c-91d8-b5e39995670c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae7e4e8-21ff-45f6-ae60-4a1c3753ab0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a847ccc-d6f4-447c-91d8-b5e39995670c",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        },
        {
            "id": "bd82e197-dedb-40c1-afbe-d40e37d23892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "compositeImage": {
                "id": "0d4b7740-925d-4319-a9b5-652ed7a74246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd82e197-dedb-40c1-afbe-d40e37d23892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5fc502-bc38-4f33-9bff-c34ed69756b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd82e197-dedb-40c1-afbe-d40e37d23892",
                    "LayerId": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b18f5cd-1226-4066-9eb5-fe888ff0edbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "629e915a-f0cb-4e28-bf0f-ce752fb1a45f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}