{
    "id": "2af2e334-987e-424e-8630-517636c0c28c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkang_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9229e5d4-649e-4341-932f-fe2c8a1a1a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af2e334-987e-424e-8630-517636c0c28c",
            "compositeImage": {
                "id": "3bd9f5cd-3b5b-4da6-8978-c1fad7df8af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9229e5d4-649e-4341-932f-fe2c8a1a1a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f668bc6a-033d-47e7-a362-8a2bf6c023c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9229e5d4-649e-4341-932f-fe2c8a1a1a16",
                    "LayerId": "b7caa915-74f9-4410-9df5-03523d013a0a"
                }
            ]
        },
        {
            "id": "e0a9bb64-428a-4061-8c38-809a59a5950f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af2e334-987e-424e-8630-517636c0c28c",
            "compositeImage": {
                "id": "f6628a18-01eb-48f8-b887-8f5c0fb2650c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0a9bb64-428a-4061-8c38-809a59a5950f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa03f11f-fd96-461c-b167-4d1b3094673f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0a9bb64-428a-4061-8c38-809a59a5950f",
                    "LayerId": "b7caa915-74f9-4410-9df5-03523d013a0a"
                }
            ]
        },
        {
            "id": "0ca3bb53-0713-4a18-8dd6-28247618e9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af2e334-987e-424e-8630-517636c0c28c",
            "compositeImage": {
                "id": "e2b41e8d-4815-409b-b6cc-c602b2716abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca3bb53-0713-4a18-8dd6-28247618e9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c39f4ca-b2be-44cf-891f-4aa0cd371830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca3bb53-0713-4a18-8dd6-28247618e9e1",
                    "LayerId": "b7caa915-74f9-4410-9df5-03523d013a0a"
                }
            ]
        },
        {
            "id": "164ec7de-c792-4c7c-90c6-7d4b13bbfbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af2e334-987e-424e-8630-517636c0c28c",
            "compositeImage": {
                "id": "73623ca6-d9b5-4576-83ef-1c0e0ca8c38a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164ec7de-c792-4c7c-90c6-7d4b13bbfbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a97825d-2aca-4bac-9c26-efb7cd2085fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164ec7de-c792-4c7c-90c6-7d4b13bbfbbb",
                    "LayerId": "b7caa915-74f9-4410-9df5-03523d013a0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "b7caa915-74f9-4410-9df5-03523d013a0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2af2e334-987e-424e-8630-517636c0c28c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}