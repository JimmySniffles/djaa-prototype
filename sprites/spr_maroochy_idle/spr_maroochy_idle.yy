{
    "id": "6fb0bb24-058d-4379-bc01-aa476d4b6c68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_maroochy_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf174575-0fb6-41d0-8d70-ddadbd653464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb0bb24-058d-4379-bc01-aa476d4b6c68",
            "compositeImage": {
                "id": "c1343b7c-ff54-4ce3-ac4c-c066127b42ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf174575-0fb6-41d0-8d70-ddadbd653464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f718ed23-4941-487a-9a6e-69462a8af9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf174575-0fb6-41d0-8d70-ddadbd653464",
                    "LayerId": "7eb3a24e-23e2-4f4c-b081-f38202f525c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7eb3a24e-23e2-4f4c-b081-f38202f525c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fb0bb24-058d-4379-bc01-aa476d4b6c68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}