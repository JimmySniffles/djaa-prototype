{
    "id": "fb236756-c775-4f40-b923-faf9d8c42141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_sa_a_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "956c691c-79e3-447e-8b91-1a47758ef826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "a4a03df3-f6c1-4f41-a402-61bb65e18de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956c691c-79e3-447e-8b91-1a47758ef826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3173ac41-b56e-4028-808b-91cf8d154fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956c691c-79e3-447e-8b91-1a47758ef826",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "a6617eeb-f301-426b-aba7-5280b0662a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956c691c-79e3-447e-8b91-1a47758ef826",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        },
        {
            "id": "451614ac-5253-4153-9ff1-5b9ba262738f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "d8cf11d0-160c-41e2-8660-490bae4238b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451614ac-5253-4153-9ff1-5b9ba262738f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c91ed6-0f92-44ff-a1df-483fb6b755a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451614ac-5253-4153-9ff1-5b9ba262738f",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "1cdba995-4981-47df-ae71-fe6503d839b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451614ac-5253-4153-9ff1-5b9ba262738f",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        },
        {
            "id": "c1472f0e-38d6-4019-9d7b-3bca3eba2e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "bed4f63d-53ab-401e-864f-f218e8c4745a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1472f0e-38d6-4019-9d7b-3bca3eba2e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd4b278-2f7b-47cb-aa77-5f78a1a98790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1472f0e-38d6-4019-9d7b-3bca3eba2e8a",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "a679827f-8f92-48f6-a973-71ed7668c29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1472f0e-38d6-4019-9d7b-3bca3eba2e8a",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        },
        {
            "id": "292a0845-0c96-4191-ad5c-6911dc2d2523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "7a1a86f7-0668-4cca-8083-c2f88951ed7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292a0845-0c96-4191-ad5c-6911dc2d2523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aaf5ad4-5743-429a-ba54-623a034c5ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292a0845-0c96-4191-ad5c-6911dc2d2523",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "7a3332e5-f2d4-427a-875f-988a8cea29e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292a0845-0c96-4191-ad5c-6911dc2d2523",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        },
        {
            "id": "82f07f9f-4fa8-4713-b8c2-76b015ce5bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "a275f300-a1e0-4253-98d8-8adf681ae420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f07f9f-4fa8-4713-b8c2-76b015ce5bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf99712-897d-4587-b4ed-392c9bcc8c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f07f9f-4fa8-4713-b8c2-76b015ce5bf2",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "d2941c51-bc29-44da-97b3-35c39a169456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f07f9f-4fa8-4713-b8c2-76b015ce5bf2",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        },
        {
            "id": "65f499a2-df9f-4677-9a67-c0db917ff3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "compositeImage": {
                "id": "e4a483b9-99fc-427b-a463-e27d5a642ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f499a2-df9f-4677-9a67-c0db917ff3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b158eb5-7bbb-46cd-85a9-c40648978191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f499a2-df9f-4677-9a67-c0db917ff3e8",
                    "LayerId": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64"
                },
                {
                    "id": "6274387c-2d29-480f-b887-848356b655dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f499a2-df9f-4677-9a67-c0db917ff3e8",
                    "LayerId": "fcfc3009-7c58-44f6-bd30-69922ae43b79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "68b770ec-a72f-4ff9-8c3a-2d92cbf34f64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fcfc3009-7c58-44f6-bd30-69922ae43b79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb236756-c775-4f40-b923-faf9d8c42141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}