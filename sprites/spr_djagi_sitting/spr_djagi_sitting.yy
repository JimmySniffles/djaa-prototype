{
    "id": "992a199a-4d2b-4870-b186-1c062c36a8c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_djagi_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 49,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "277d18fb-f745-4658-bab8-d3b4533ac629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "992a199a-4d2b-4870-b186-1c062c36a8c6",
            "compositeImage": {
                "id": "00c8214f-a6c9-4b5c-8e84-e3564ef5bdf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "277d18fb-f745-4658-bab8-d3b4533ac629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad16eda-765c-4928-a0cf-eff38da4b22f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "277d18fb-f745-4658-bab8-d3b4533ac629",
                    "LayerId": "d1bb4803-0b07-43a1-9c18-c8f343a6ce6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d1bb4803-0b07-43a1-9c18-c8f343a6ce6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "992a199a-4d2b-4870-b186-1c062c36a8c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}