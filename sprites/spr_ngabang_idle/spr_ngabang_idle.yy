{
    "id": "53407673-9982-4fbc-a1aa-cb30a7059310",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ngabang_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 37,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31da020f-c083-425a-a373-16452969db9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53407673-9982-4fbc-a1aa-cb30a7059310",
            "compositeImage": {
                "id": "e6351f1f-6b22-4c74-bb1e-a7ebd2bf448e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31da020f-c083-425a-a373-16452969db9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d0fc06-d6ef-455a-8b71-b57fc11ed846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31da020f-c083-425a-a373-16452969db9b",
                    "LayerId": "3a8a6107-3105-47d3-b274-6df3f459bc5c"
                }
            ]
        },
        {
            "id": "bc773eda-769e-4056-a39e-5636b3ce8e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53407673-9982-4fbc-a1aa-cb30a7059310",
            "compositeImage": {
                "id": "5f5523dd-6fe9-4a9b-aaf3-76e736aed13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc773eda-769e-4056-a39e-5636b3ce8e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2837c0c1-a1cf-4ddd-b04b-d18c29d62925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc773eda-769e-4056-a39e-5636b3ce8e25",
                    "LayerId": "3a8a6107-3105-47d3-b274-6df3f459bc5c"
                }
            ]
        },
        {
            "id": "aa74517e-1778-4a8c-9bb1-4a9a46b560ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53407673-9982-4fbc-a1aa-cb30a7059310",
            "compositeImage": {
                "id": "e3c88f13-fdde-49bd-b572-9f36c183a2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa74517e-1778-4a8c-9bb1-4a9a46b560ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a647570d-bc9d-4dc0-b9ad-17fd086d5a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa74517e-1778-4a8c-9bb1-4a9a46b560ee",
                    "LayerId": "3a8a6107-3105-47d3-b274-6df3f459bc5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3a8a6107-3105-47d3-b274-6df3f459bc5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53407673-9982-4fbc-a1aa-cb30a7059310",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}