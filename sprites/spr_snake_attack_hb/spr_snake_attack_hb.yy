{
    "id": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snake_attack_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dac5276-231a-41e8-a080-1bedf39e53c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "f5d39130-50ae-48e7-b758-71175b4646f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dac5276-231a-41e8-a080-1bedf39e53c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3997842-b421-48da-8485-6e8696ed7c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dac5276-231a-41e8-a080-1bedf39e53c6",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "0a23a35e-fbfe-4eff-bb3f-d5bceda5cb6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dac5276-231a-41e8-a080-1bedf39e53c6",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "59f5d543-cad6-4667-b90d-0b30fab99b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "b0bbc7aa-0b05-430b-b2a1-53a87a27a825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f5d543-cad6-4667-b90d-0b30fab99b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7caae336-c8c9-40c7-96e4-e8479e6eb1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f5d543-cad6-4667-b90d-0b30fab99b6e",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "ce1524bd-3db7-439d-bb34-efc89b2f8547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f5d543-cad6-4667-b90d-0b30fab99b6e",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "ed883a61-a599-4366-b71c-899b6ff72c6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "df3e9f21-76dd-450b-94da-5dee649fda63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed883a61-a599-4366-b71c-899b6ff72c6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6f6143-6c91-4fa4-a6d3-42a09fb19aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed883a61-a599-4366-b71c-899b6ff72c6b",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "d7f30576-b332-48b8-98cb-9b791efd8729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed883a61-a599-4366-b71c-899b6ff72c6b",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "736c0956-636f-456e-b82f-3999abb49ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "510919af-6410-4480-b8ea-0a671b0c858e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "736c0956-636f-456e-b82f-3999abb49ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b81140-365e-4796-b9c5-b1fbe04082a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "736c0956-636f-456e-b82f-3999abb49ff6",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "9b627265-3b64-4700-9361-2e82eb5e3447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "736c0956-636f-456e-b82f-3999abb49ff6",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "c14ea9dd-fd59-47db-9db3-19add9970842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "9c3f89b2-8664-4254-a43a-932e30736008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c14ea9dd-fd59-47db-9db3-19add9970842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16bc37dc-9777-41b5-a9a9-8d7a59650a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c14ea9dd-fd59-47db-9db3-19add9970842",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "d69f1f59-092a-44e5-acee-fbad80530bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c14ea9dd-fd59-47db-9db3-19add9970842",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "931be242-e27d-4cf3-8453-acb0690225f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "baee5807-dee7-40cb-9b58-733716091d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "931be242-e27d-4cf3-8453-acb0690225f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12a29fab-0edb-4dc0-a6a9-ee1947c40a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931be242-e27d-4cf3-8453-acb0690225f6",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "bec65b21-8df1-4341-96c1-203954173383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931be242-e27d-4cf3-8453-acb0690225f6",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "41045b1c-1e49-446f-93c4-d2d9c98dd8b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "b569bd1e-bd92-41b8-827f-0b3c7ab07c82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41045b1c-1e49-446f-93c4-d2d9c98dd8b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d811b8-03e8-489a-9dbe-3e9f3059f194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41045b1c-1e49-446f-93c4-d2d9c98dd8b0",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                },
                {
                    "id": "c3b06eab-c4a8-4c6d-9f03-aee8963d2fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41045b1c-1e49-446f-93c4-d2d9c98dd8b0",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                }
            ]
        },
        {
            "id": "84d1c960-9a68-48fc-8bc6-55cd4a5b4279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "compositeImage": {
                "id": "f09bb9a3-796e-46ab-85c8-6839bbd501f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d1c960-9a68-48fc-8bc6-55cd4a5b4279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e187db5-339e-4969-b1e5-600fafa17a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d1c960-9a68-48fc-8bc6-55cd4a5b4279",
                    "LayerId": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f"
                },
                {
                    "id": "f27100bd-45c4-4d10-99ae-a7666d5f524c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d1c960-9a68-48fc-8bc6-55cd4a5b4279",
                    "LayerId": "c1004f66-9a1d-4acf-b4da-64e7b0397655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "e5a0f1cd-00b6-4dbc-b76c-5ea8c54b563f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c1004f66-9a1d-4acf-b4da-64e7b0397655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cbfeac0-4bfe-4717-957d-7f8a5b920e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}