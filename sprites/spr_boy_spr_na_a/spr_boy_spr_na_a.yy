{
    "id": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_na_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40866420-b938-412d-ac86-37b53a113c64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "f1000fca-bf2f-4ae6-a863-a607e28be0ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40866420-b938-412d-ac86-37b53a113c64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a273ccf0-4c48-4cca-9d36-95de2976a673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40866420-b938-412d-ac86-37b53a113c64",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "260d24ce-4373-45d8-8253-0e1f2ac40a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40866420-b938-412d-ac86-37b53a113c64",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        },
        {
            "id": "19b92d0e-36ed-4251-9f66-70d672ebbf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "c74f2eea-c9ab-4cae-af5f-30760a276875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b92d0e-36ed-4251-9f66-70d672ebbf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6244157a-5843-4f0f-9c56-dcb1720b3140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b92d0e-36ed-4251-9f66-70d672ebbf9e",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "9e12e2b0-304f-433c-b8fd-484e1761d244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b92d0e-36ed-4251-9f66-70d672ebbf9e",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        },
        {
            "id": "fa8e2a4e-d2d8-44ef-ba19-c469ab2e3208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "551cd32c-5c7e-44fb-b127-922c94bb13cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa8e2a4e-d2d8-44ef-ba19-c469ab2e3208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e518b8f9-f48b-403d-81f8-928d478368f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8e2a4e-d2d8-44ef-ba19-c469ab2e3208",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "b8e5da51-3c23-49d3-8d8a-4c8dcbc67fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8e2a4e-d2d8-44ef-ba19-c469ab2e3208",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        },
        {
            "id": "9fa14d77-8c4e-4762-9359-97693a67b591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "5de11dd3-9b97-4b90-9d6c-ad879a8f3b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa14d77-8c4e-4762-9359-97693a67b591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92241277-4a92-4ddc-830d-e9ab1b715775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa14d77-8c4e-4762-9359-97693a67b591",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "c2e12ea6-897c-4161-a518-08c5f45995dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa14d77-8c4e-4762-9359-97693a67b591",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        },
        {
            "id": "1b2e0329-42c3-4fbb-b548-157d31235e4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "0d1d17f0-663c-4c17-a121-c63b1c436690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2e0329-42c3-4fbb-b548-157d31235e4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7211be6e-1a17-48d1-b20b-5074670a5bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2e0329-42c3-4fbb-b548-157d31235e4d",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "f1eb6894-8c45-42f0-a280-7c5f57536d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2e0329-42c3-4fbb-b548-157d31235e4d",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        },
        {
            "id": "fc451b1a-122c-451f-8ceb-962249452a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "compositeImage": {
                "id": "1eebaf01-921f-4e98-9731-557b140b647c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc451b1a-122c-451f-8ceb-962249452a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83028aaa-80d6-4aa7-99b1-32cb55ebeee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc451b1a-122c-451f-8ceb-962249452a62",
                    "LayerId": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5"
                },
                {
                    "id": "4c5bb2ce-6836-4813-abb3-7d10fc84aae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc451b1a-122c-451f-8ceb-962249452a62",
                    "LayerId": "d8390b40-81b3-4e2e-9941-659b50622189"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a431e8af-a4b2-4add-8cff-8cafdd3dd9b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d8390b40-81b3-4e2e-9941-659b50622189",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565b0c4d-98e2-49a9-ab7a-2d8aa6375073",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}