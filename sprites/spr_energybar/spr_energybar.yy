{
    "id": "b258db4b-b357-420f-b194-4748a76eac22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energybar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d072dd5e-fe42-4afc-8241-e8ebf44b3ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b258db4b-b357-420f-b194-4748a76eac22",
            "compositeImage": {
                "id": "1c7d6e05-e69a-4cca-932f-afca24f83ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d072dd5e-fe42-4afc-8241-e8ebf44b3ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e983e1-d2f9-469d-878e-9593fdc6b5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d072dd5e-fe42-4afc-8241-e8ebf44b3ab9",
                    "LayerId": "a013ba00-3e7f-4426-b5ce-4e6ec828ed92"
                }
            ]
        },
        {
            "id": "6aab57e9-2dc8-41fe-a01f-a4f13f9e88bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b258db4b-b357-420f-b194-4748a76eac22",
            "compositeImage": {
                "id": "1b4c43c9-d020-49fd-8293-1b903bf1b817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aab57e9-2dc8-41fe-a01f-a4f13f9e88bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deaede6b-8f59-4334-b177-bdc886cbcfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aab57e9-2dc8-41fe-a01f-a4f13f9e88bd",
                    "LayerId": "a013ba00-3e7f-4426-b5ce-4e6ec828ed92"
                }
            ]
        },
        {
            "id": "f700e40c-29ef-46c1-90e6-790d96c5869f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b258db4b-b357-420f-b194-4748a76eac22",
            "compositeImage": {
                "id": "36544c0d-ea13-4418-9b03-87b333c03988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f700e40c-29ef-46c1-90e6-790d96c5869f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b504d1-1e51-4bbe-93a6-dceba0842a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f700e40c-29ef-46c1-90e6-790d96c5869f",
                    "LayerId": "a013ba00-3e7f-4426-b5ce-4e6ec828ed92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "a013ba00-3e7f-4426-b5ce-4e6ec828ed92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b258db4b-b357-420f-b194-4748a76eac22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}