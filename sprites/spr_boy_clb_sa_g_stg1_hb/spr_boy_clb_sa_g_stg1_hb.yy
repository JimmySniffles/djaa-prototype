{
    "id": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_sa_g_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1646f956-4157-4502-903c-b92997d2b72d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "108b66b1-2413-40f2-a226-82a1f4dab1fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1646f956-4157-4502-903c-b92997d2b72d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd13da14-f044-48a6-8c90-deef19e76464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1646f956-4157-4502-903c-b92997d2b72d",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "915c57f2-1f54-49c9-8b32-705d17726563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1646f956-4157-4502-903c-b92997d2b72d",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        },
        {
            "id": "b4414d48-d8f0-4812-8155-9c9a14b4ff1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "23bf19ab-82ae-430d-86da-8bcd9121e2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4414d48-d8f0-4812-8155-9c9a14b4ff1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be261a61-7dcb-4e44-a175-539215b869c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4414d48-d8f0-4812-8155-9c9a14b4ff1a",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "2d9211d4-a7f4-4514-b972-d6a297c187e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4414d48-d8f0-4812-8155-9c9a14b4ff1a",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        },
        {
            "id": "fe5eefd4-6458-4ef3-89c0-a84baa1f249c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "02a06abe-c831-4de7-bb92-4062ce3b6f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5eefd4-6458-4ef3-89c0-a84baa1f249c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eead2af-9aeb-4901-8fe1-f450898b2aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5eefd4-6458-4ef3-89c0-a84baa1f249c",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "a5238d9d-9163-4c8b-bea4-8252889cb343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5eefd4-6458-4ef3-89c0-a84baa1f249c",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        },
        {
            "id": "46e6440d-a419-40c9-96a2-4228f8865ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "c9fb2f87-75ff-4806-896f-874e8f5efc5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46e6440d-a419-40c9-96a2-4228f8865ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6925bc-423a-46f5-bc78-5386ebc2530b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e6440d-a419-40c9-96a2-4228f8865ae7",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "762485ce-1296-4352-8853-b242ff8a0cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e6440d-a419-40c9-96a2-4228f8865ae7",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        },
        {
            "id": "bbf4fe22-9597-4f61-ba71-f852b9ebbb3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "c734fbcd-72c7-4be0-babd-d21b7a466d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf4fe22-9597-4f61-ba71-f852b9ebbb3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6789be8b-9a4c-4803-8a57-d40d55bb04bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf4fe22-9597-4f61-ba71-f852b9ebbb3b",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "7c73bc43-9589-49ff-ad83-5a112e9ce7a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf4fe22-9597-4f61-ba71-f852b9ebbb3b",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        },
        {
            "id": "def701b2-6aad-4f8a-9b0e-4e80791dd8d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "compositeImage": {
                "id": "0517f1d2-5c86-4d5f-b49f-96cf360d398b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def701b2-6aad-4f8a-9b0e-4e80791dd8d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599f4ded-3d2e-4ae3-a7f1-8c47ef4c1d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def701b2-6aad-4f8a-9b0e-4e80791dd8d2",
                    "LayerId": "ff9d7723-3fda-45a6-97cc-4571fa5e7968"
                },
                {
                    "id": "1eeab96b-491d-4b13-bf80-86f087498f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def701b2-6aad-4f8a-9b0e-4e80791dd8d2",
                    "LayerId": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff9d7723-3fda-45a6-97cc-4571fa5e7968",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8651cc13-fc7e-482b-a9a8-45ed5bb857e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15fe4ad6-24f9-4086-b367-fc4d680354f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}