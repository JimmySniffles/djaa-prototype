{
    "id": "9f87c2cc-e876-4a5c-870e-a4e54acf8689",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4419834c-0288-46eb-85ad-cb794d0edd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f87c2cc-e876-4a5c-870e-a4e54acf8689",
            "compositeImage": {
                "id": "63dd1ec8-5272-4cd8-8187-a898c696c390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4419834c-0288-46eb-85ad-cb794d0edd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c4f617-be64-4357-90df-b577619769d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4419834c-0288-46eb-85ad-cb794d0edd5d",
                    "LayerId": "7c1e3c79-8af0-4825-a863-9e46e0ec7744"
                }
            ]
        },
        {
            "id": "b1b37940-c02c-4007-95f7-352c63e8f807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f87c2cc-e876-4a5c-870e-a4e54acf8689",
            "compositeImage": {
                "id": "3fa9a246-d407-4d23-9de1-040f1559ef8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b37940-c02c-4007-95f7-352c63e8f807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d7f54e3-6ab2-45bf-8831-5148fcdc78b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b37940-c02c-4007-95f7-352c63e8f807",
                    "LayerId": "7c1e3c79-8af0-4825-a863-9e46e0ec7744"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "7c1e3c79-8af0-4825-a863-9e46e0ec7744",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f87c2cc-e876-4a5c-870e-a4e54acf8689",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}