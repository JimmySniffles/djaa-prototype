{
    "id": "6e214949-c240-467a-b7ce-d7c30e88e890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_breathbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d00fb967-9700-4c23-a80e-eb5360e313a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e214949-c240-467a-b7ce-d7c30e88e890",
            "compositeImage": {
                "id": "db1350e9-8cae-437f-b455-dbd23485ae0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d00fb967-9700-4c23-a80e-eb5360e313a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9a4eb47-e93c-412f-8860-66b465dcfc6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d00fb967-9700-4c23-a80e-eb5360e313a0",
                    "LayerId": "9b7f0eda-6832-4535-9d33-27fb1546e7a6"
                }
            ]
        },
        {
            "id": "660ff5a3-e81c-48e3-a31e-9e7beff47154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e214949-c240-467a-b7ce-d7c30e88e890",
            "compositeImage": {
                "id": "097f823d-3e1e-4f70-839b-a4edbf1fc4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660ff5a3-e81c-48e3-a31e-9e7beff47154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d012342f-89a5-4e73-9416-f961a042e036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660ff5a3-e81c-48e3-a31e-9e7beff47154",
                    "LayerId": "9b7f0eda-6832-4535-9d33-27fb1546e7a6"
                }
            ]
        },
        {
            "id": "2f161b04-fb9a-445d-b939-4b473a01ad2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e214949-c240-467a-b7ce-d7c30e88e890",
            "compositeImage": {
                "id": "db5eb683-e75d-41bf-a52b-840f6594431e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f161b04-fb9a-445d-b939-4b473a01ad2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97ff5fd-1c65-4d90-b7f5-66b491f8a293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f161b04-fb9a-445d-b939-4b473a01ad2d",
                    "LayerId": "9b7f0eda-6832-4535-9d33-27fb1546e7a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "9b7f0eda-6832-4535-9d33-27fb1546e7a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e214949-c240-467a-b7ce-d7c30e88e890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}