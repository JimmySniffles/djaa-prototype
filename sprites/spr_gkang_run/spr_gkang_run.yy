{
    "id": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkang_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bde0e975-6a4a-4bc6-8562-6425759cbce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "4f7329f2-0001-40c0-9aa7-045b9b4460ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde0e975-6a4a-4bc6-8562-6425759cbce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "300c1c6a-661b-4f61-bac8-2ea303d16e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde0e975-6a4a-4bc6-8562-6425759cbce7",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "fb4a3869-a9de-488f-b7cb-6f6b4c881eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "61020a30-0aac-4861-9c50-30c7bd40ee89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4a3869-a9de-488f-b7cb-6f6b4c881eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0352c3-6d43-468a-8f3c-7ae40b449bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4a3869-a9de-488f-b7cb-6f6b4c881eb3",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "cde4ee21-d4e6-41ae-bd2c-010332129a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "7ba8979e-8896-44bf-b04d-044db55f6841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cde4ee21-d4e6-41ae-bd2c-010332129a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b70b37c3-db47-4142-8114-6acd3944c535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde4ee21-d4e6-41ae-bd2c-010332129a47",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "91cba765-869b-4a8f-b0a2-b60f341c09a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "b4e7128f-d701-4ab9-8b31-973cd14d4445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91cba765-869b-4a8f-b0a2-b60f341c09a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96436b4c-6424-44d5-9321-0fb407f5139a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91cba765-869b-4a8f-b0a2-b60f341c09a9",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "6ef38781-ab3b-4568-8dcb-2f0d7bbae027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "e06a72c3-37a3-4143-9e79-d65ce6d93179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef38781-ab3b-4568-8dcb-2f0d7bbae027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d47d86-3015-4595-bcca-fcd78b581a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef38781-ab3b-4568-8dcb-2f0d7bbae027",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "14fcfbf6-9c92-4eee-8595-e5985dad81bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "fa65f724-16c7-49cd-899a-d6f65272e80d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fcfbf6-9c92-4eee-8595-e5985dad81bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41613d1d-10b9-4f68-8c47-a57b20bd2fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fcfbf6-9c92-4eee-8595-e5985dad81bb",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "a218d666-ecf1-41ec-839d-846384ae26c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "68d9a707-5d0a-43cc-9d04-34e840a04ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a218d666-ecf1-41ec-839d-846384ae26c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8106fa30-2879-4156-8779-036bfd608400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a218d666-ecf1-41ec-839d-846384ae26c3",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "c26b08e4-35a0-47d9-8a68-1fab18f1dcd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "4f77f09a-dff3-4c4b-adef-d5afbc10fb9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26b08e4-35a0-47d9-8a68-1fab18f1dcd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454f2246-ff3d-4f0f-9663-5804b12e530e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26b08e4-35a0-47d9-8a68-1fab18f1dcd8",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        },
        {
            "id": "c6eb68a2-3afa-4603-b8fb-547599b6efca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "compositeImage": {
                "id": "3d5b28b7-f067-4439-b6fa-e6aacfe803cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6eb68a2-3afa-4603-b8fb-547599b6efca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11463d8f-6e0b-4ab1-b6b2-5214aa2a7870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6eb68a2-3afa-4603-b8fb-547599b6efca",
                    "LayerId": "24198897-ea13-46b4-8efb-19c2bff28548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "24198897-ea13-46b4-8efb-19c2bff28548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81cd9c59-68d0-4be6-a8fd-0c8dd17c20e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}