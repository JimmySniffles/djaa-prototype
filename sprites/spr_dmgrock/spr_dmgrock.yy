{
    "id": "945db470-c48f-4756-8b2c-6b6205864e48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dmgrock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 24,
    "bbox_right": 224,
    "bbox_top": 28,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c2fcfeb-5305-4210-b5fa-ea71122c8532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "compositeImage": {
                "id": "0b0bba37-58cb-416d-b78f-d0f060bfe814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2fcfeb-5305-4210-b5fa-ea71122c8532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8a5d72-d7b0-435a-b30d-c5d0472f7f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2fcfeb-5305-4210-b5fa-ea71122c8532",
                    "LayerId": "211d7aaf-4c69-4a59-940d-b36d4b9e330c"
                }
            ]
        },
        {
            "id": "e1b5849f-2eef-4c09-a5fa-b05b8f400fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "compositeImage": {
                "id": "1ebe38ef-adaa-4a5a-a8e4-944dffae3fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b5849f-2eef-4c09-a5fa-b05b8f400fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be15d48d-9886-4717-aa51-d8fe4fd99564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b5849f-2eef-4c09-a5fa-b05b8f400fdb",
                    "LayerId": "211d7aaf-4c69-4a59-940d-b36d4b9e330c"
                }
            ]
        },
        {
            "id": "1b29fd6e-c4be-43d3-a20f-b90b0da08412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "compositeImage": {
                "id": "9b36d849-1d30-4607-aa17-ba755b3d137b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b29fd6e-c4be-43d3-a20f-b90b0da08412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af101d56-8f2c-4175-a01a-2ffefb4f61ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b29fd6e-c4be-43d3-a20f-b90b0da08412",
                    "LayerId": "211d7aaf-4c69-4a59-940d-b36d4b9e330c"
                }
            ]
        },
        {
            "id": "e970a27c-d872-4f5f-a29c-1bba0746f6c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "compositeImage": {
                "id": "8f56b780-ecc7-4366-8ba5-164640895a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e970a27c-d872-4f5f-a29c-1bba0746f6c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dec320a-beac-433f-a227-835beb241bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e970a27c-d872-4f5f-a29c-1bba0746f6c9",
                    "LayerId": "211d7aaf-4c69-4a59-940d-b36d4b9e330c"
                }
            ]
        },
        {
            "id": "9f09c4bf-d873-4cff-a267-cd5d7825859f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "compositeImage": {
                "id": "5f0e14d2-4109-4908-aca4-1bc106f72011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f09c4bf-d873-4cff-a267-cd5d7825859f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18543680-14bc-49c6-bc7f-1f388a6b60e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f09c4bf-d873-4cff-a267-cd5d7825859f",
                    "LayerId": "211d7aaf-4c69-4a59-940d-b36d4b9e330c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "211d7aaf-4c69-4a59-940d-b36d4b9e330c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "945db470-c48f-4756-8b2c-6b6205864e48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 239
}