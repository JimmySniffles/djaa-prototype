{
    "id": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_na_g_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb30b0cf-51d3-4380-84e7-eb9946f0b10d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "13adacfa-9f18-44e2-add6-b11a02dfceed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb30b0cf-51d3-4380-84e7-eb9946f0b10d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1d7f31-0edd-4fd1-8c62-92323d81c060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb30b0cf-51d3-4380-84e7-eb9946f0b10d",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        },
        {
            "id": "0d12b86b-9fdc-4035-b895-ba663c9dbd1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "fc01edf2-29c2-48e4-a5ad-155e0e48024a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d12b86b-9fdc-4035-b895-ba663c9dbd1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04ddd39-666f-405e-8122-f8caf59c3d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d12b86b-9fdc-4035-b895-ba663c9dbd1e",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        },
        {
            "id": "403ba103-376f-42b7-9b87-8524b5e3b5d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "9c66e643-b897-40e6-93c7-35ffc522001c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "403ba103-376f-42b7-9b87-8524b5e3b5d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205b2455-49e3-414d-8604-19d98bc7911a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "403ba103-376f-42b7-9b87-8524b5e3b5d3",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        },
        {
            "id": "272ed25b-b433-434c-9a7e-a3b8ea472744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "1cf6ed71-dc38-4298-be46-9e9af6f5f2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272ed25b-b433-434c-9a7e-a3b8ea472744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00fdc1d8-0c92-4fa5-9334-f90f54e11b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272ed25b-b433-434c-9a7e-a3b8ea472744",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        },
        {
            "id": "8c19dc8b-469e-45e8-9b55-a54666f23399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "f39b3598-627c-410c-85dd-ece21fdf8192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c19dc8b-469e-45e8-9b55-a54666f23399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e0be668-c2e5-42cb-a827-d8b3c2324548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c19dc8b-469e-45e8-9b55-a54666f23399",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        },
        {
            "id": "a613329f-7625-4cf9-9be6-1118f133986f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "compositeImage": {
                "id": "c8ecde21-5f61-401d-bd46-ae20b3a54ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a613329f-7625-4cf9-9be6-1118f133986f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a948a2f9-61e9-4ad5-8961-c9a12ffe65c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a613329f-7625-4cf9-9be6-1118f133986f",
                    "LayerId": "5bdca830-07f9-476f-9328-d6ddec17de60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5bdca830-07f9-476f-9328-d6ddec17de60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce3a0a30-1d91-4584-98c9-4fcb8dd88997",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}