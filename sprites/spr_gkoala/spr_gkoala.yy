{
    "id": "7eb7246d-2069-4b57-860a-19fde1c37035",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkoala",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "132bf78a-0525-4e91-b3db-04608f362f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb7246d-2069-4b57-860a-19fde1c37035",
            "compositeImage": {
                "id": "e3d9e3db-17e4-4050-895a-ca5670dfb5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132bf78a-0525-4e91-b3db-04608f362f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "107481d0-54f7-4419-83ef-631a4d3a5035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132bf78a-0525-4e91-b3db-04608f362f42",
                    "LayerId": "5bb5fb36-bcaa-4b9e-b0ec-2daeb128b905"
                }
            ]
        },
        {
            "id": "820701c0-f76b-41cd-aa67-4c43e0b3c110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb7246d-2069-4b57-860a-19fde1c37035",
            "compositeImage": {
                "id": "ffc29a04-6d02-419b-add2-20cd8f1131be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "820701c0-f76b-41cd-aa67-4c43e0b3c110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce00689d-ceed-4ac9-8b37-600913f761d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "820701c0-f76b-41cd-aa67-4c43e0b3c110",
                    "LayerId": "5bb5fb36-bcaa-4b9e-b0ec-2daeb128b905"
                }
            ]
        },
        {
            "id": "8cdcb7d5-a2a6-4df8-9c0a-e29dc061893f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb7246d-2069-4b57-860a-19fde1c37035",
            "compositeImage": {
                "id": "7bbd2fc3-3e47-4a64-bc6b-17f0cf903129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdcb7d5-a2a6-4df8-9c0a-e29dc061893f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8709bd6-0330-459f-9681-40b497dbf048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdcb7d5-a2a6-4df8-9c0a-e29dc061893f",
                    "LayerId": "5bb5fb36-bcaa-4b9e-b0ec-2daeb128b905"
                }
            ]
        },
        {
            "id": "bda2d05a-2b38-4cf6-a45a-ca666866fcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb7246d-2069-4b57-860a-19fde1c37035",
            "compositeImage": {
                "id": "4ef423f2-c94f-4cd8-b96f-d703df0d2e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda2d05a-2b38-4cf6-a45a-ca666866fcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bff78bb-ad60-45ae-a662-078c918f9deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda2d05a-2b38-4cf6-a45a-ca666866fcf4",
                    "LayerId": "5bb5fb36-bcaa-4b9e-b0ec-2daeb128b905"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5bb5fb36-bcaa-4b9e-b0ec-2daeb128b905",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eb7246d-2069-4b57-860a-19fde1c37035",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}