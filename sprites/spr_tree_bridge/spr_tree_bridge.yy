{
    "id": "d5462760-2271-4ad7-905e-079bfb7a6db5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_bridge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 64,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fb72dd5-5ef1-4d0d-bf7e-7502bf753401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5462760-2271-4ad7-905e-079bfb7a6db5",
            "compositeImage": {
                "id": "9f8a5c99-bffa-4c0e-aafb-858e3164df5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb72dd5-5ef1-4d0d-bf7e-7502bf753401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a9905d3-c6eb-41c9-b8b3-e7fc93badd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb72dd5-5ef1-4d0d-bf7e-7502bf753401",
                    "LayerId": "b4178d94-c1d5-497d-909d-d8829299c23e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b4178d94-c1d5-497d-909d-d8829299c23e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5462760-2271-4ad7-905e-079bfb7a6db5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 89,
    "yorig": 32
}