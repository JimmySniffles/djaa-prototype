{
    "id": "5f5057e2-60f8-4342-a0ce-45df880b592b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hitspark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21f511e2-73de-469f-bbc7-13679511aec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "3f28792a-10f3-4dfe-8d38-8db4dcd4db1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f511e2-73de-469f-bbc7-13679511aec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32e13b0c-02d2-4a84-badb-2412a322ced0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f511e2-73de-469f-bbc7-13679511aec6",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "7f65d1ca-b1e2-4b58-8e46-59e5b09fcf38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "b2a451ae-5827-48de-925d-72838e9f6e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f65d1ca-b1e2-4b58-8e46-59e5b09fcf38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71fcc925-0b59-4f65-828e-1408b1d070b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f65d1ca-b1e2-4b58-8e46-59e5b09fcf38",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "6d1f8ae1-6e40-4622-9c7b-debc5896d3aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "d056b4a8-0591-47dc-a4b5-1aef542ef507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d1f8ae1-6e40-4622-9c7b-debc5896d3aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2988c6-6620-47b2-8a01-f2d08ba3162a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d1f8ae1-6e40-4622-9c7b-debc5896d3aa",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "6507ab2b-e5f7-452f-81e1-9bb1b498ea0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "7546f01f-809d-4b6e-abc4-7c7477c911bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6507ab2b-e5f7-452f-81e1-9bb1b498ea0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ad8351-447e-4890-89b9-0729cec51102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6507ab2b-e5f7-452f-81e1-9bb1b498ea0f",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "40b7ebfa-f838-4e44-b554-041a8a961310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "9e07bb8c-1da7-44f1-92dc-daa9ca9f4b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b7ebfa-f838-4e44-b554-041a8a961310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e2afa5-1b5f-4fb3-a26c-286b6cdc3460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b7ebfa-f838-4e44-b554-041a8a961310",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "4071b060-d862-4fab-a121-e41ceab44b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "bba54065-0ae5-4a2e-a2d4-852c3afbcb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4071b060-d862-4fab-a121-e41ceab44b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae0e6f7-0c32-4de7-887e-89e0d67768d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4071b060-d862-4fab-a121-e41ceab44b09",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "c8a5470d-a1ad-46a7-adf1-5b925b88e3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "bcc9d74b-70f6-4992-a2e3-4d2920d43fb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a5470d-a1ad-46a7-adf1-5b925b88e3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7553d0ea-b2b1-417b-af29-c1bb195959f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a5470d-a1ad-46a7-adf1-5b925b88e3c6",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        },
        {
            "id": "92d9aa70-4990-4890-bb1f-4deaba3ab496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "compositeImage": {
                "id": "777b0abe-2ea5-444e-8c2c-419b5696bb73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d9aa70-4990-4890-bb1f-4deaba3ab496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4703c0d-6dce-4e27-85ee-82621382da9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d9aa70-4990-4890-bb1f-4deaba3ab496",
                    "LayerId": "d811c196-ae41-475f-8a5a-3f88dbfb7209"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "d811c196-ae41-475f-8a5a-3f88dbfb7209",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f5057e2-60f8-4342-a0ce-45df880b592b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}