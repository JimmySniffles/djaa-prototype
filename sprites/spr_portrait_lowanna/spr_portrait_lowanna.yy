{
    "id": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_lowanna",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 11,
    "bbox_right": 106,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4b8d0af-eb9e-4c47-a2a2-ebbb9fc6aed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
            "compositeImage": {
                "id": "a6535b09-73a0-4be2-a570-12f2015bc151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b8d0af-eb9e-4c47-a2a2-ebbb9fc6aed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66a72a4-2d12-4f98-a0fb-a90bed741c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b8d0af-eb9e-4c47-a2a2-ebbb9fc6aed5",
                    "LayerId": "6b07c742-3eb1-4570-b896-57112c53318e"
                }
            ]
        },
        {
            "id": "f60fd192-02f3-4905-96ee-be112f5c048a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
            "compositeImage": {
                "id": "33ef7a14-628c-4cfd-ae45-c1511e5986eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60fd192-02f3-4905-96ee-be112f5c048a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfad2106-e7f6-4465-82b9-fc7b7fb3db19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60fd192-02f3-4905-96ee-be112f5c048a",
                    "LayerId": "6b07c742-3eb1-4570-b896-57112c53318e"
                }
            ]
        },
        {
            "id": "5b8b465a-1312-49b1-9f8d-2e213b4ece58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
            "compositeImage": {
                "id": "57e7560f-e9b0-43bb-bb31-fbe3ce68ec9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8b465a-1312-49b1-9f8d-2e213b4ece58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157d9f71-77f7-4b60-acb7-f4bf4d76feac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8b465a-1312-49b1-9f8d-2e213b4ece58",
                    "LayerId": "6b07c742-3eb1-4570-b896-57112c53318e"
                }
            ]
        },
        {
            "id": "e9f7d93b-9d12-479c-b0c1-bdbdf90daf2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
            "compositeImage": {
                "id": "c1e22d9c-e586-4c74-ad92-82c706bedb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f7d93b-9d12-479c-b0c1-bdbdf90daf2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b0f0c17-13c5-4937-a80d-e8d3d423c502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f7d93b-9d12-479c-b0c1-bdbdf90daf2c",
                    "LayerId": "6b07c742-3eb1-4570-b896-57112c53318e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "6b07c742-3eb1-4570-b896-57112c53318e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41eb08f8-9eb1-4111-a5f7-c7315455d70a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}