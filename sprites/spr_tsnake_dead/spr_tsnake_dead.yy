{
    "id": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tsnake_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b46b754e-341c-496f-a44d-a321a75084e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "compositeImage": {
                "id": "9ab10952-efa7-4a9a-86f9-c752becc4b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46b754e-341c-496f-a44d-a321a75084e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89021a11-a9ad-447b-89d5-111964de29f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46b754e-341c-496f-a44d-a321a75084e2",
                    "LayerId": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f"
                }
            ]
        },
        {
            "id": "d285de67-79b2-4028-8ae2-93c5c3e48947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "compositeImage": {
                "id": "182ec7eb-4717-4219-abc1-563907de343a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d285de67-79b2-4028-8ae2-93c5c3e48947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643ff83e-3e8c-4564-9493-f39516942bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d285de67-79b2-4028-8ae2-93c5c3e48947",
                    "LayerId": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f"
                }
            ]
        },
        {
            "id": "e8af1e7f-d0bf-409f-8fd8-0f866137b666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "compositeImage": {
                "id": "2eaf1b36-b6f1-4d31-956d-7f7a942116d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8af1e7f-d0bf-409f-8fd8-0f866137b666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a5d7ebe-caaf-4609-8c2f-6e4e4eb2cd4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8af1e7f-d0bf-409f-8fd8-0f866137b666",
                    "LayerId": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f"
                }
            ]
        },
        {
            "id": "26caaec9-6992-479a-a952-f62c6358ee9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "compositeImage": {
                "id": "3932b60c-e856-44c0-ac25-04fca5eff55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26caaec9-6992-479a-a952-f62c6358ee9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af086775-975a-446f-8b9e-345406a6acfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26caaec9-6992-479a-a952-f62c6358ee9b",
                    "LayerId": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f"
                }
            ]
        },
        {
            "id": "eebe10d6-f0aa-4488-a6a6-25e7a1317449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "compositeImage": {
                "id": "5f2f8e66-f6d8-4a28-90a6-828b45cb35d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eebe10d6-f0aa-4488-a6a6-25e7a1317449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e49611a3-b1e2-463b-841a-93a528ea10e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eebe10d6-f0aa-4488-a6a6-25e7a1317449",
                    "LayerId": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "fc2e9db7-b39a-47c7-80dd-57cbadd8c19f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ca42ec9-fc30-4c87-a8eb-5731750f2dd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}