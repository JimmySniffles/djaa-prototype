{
    "id": "d160c2b7-c5f2-4ef7-a0b9-e9ac5a3d84c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mask_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 255,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f78682fa-0c87-4768-aab7-8fbf525940d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d160c2b7-c5f2-4ef7-a0b9-e9ac5a3d84c4",
            "compositeImage": {
                "id": "47a90570-d90f-4dfc-a474-49560de3f130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78682fa-0c87-4768-aab7-8fbf525940d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a308eeb-1ee6-4dfb-abb1-5a34fa3a2f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78682fa-0c87-4768-aab7-8fbf525940d6",
                    "LayerId": "2329f86c-0e51-41e8-b2c4-a6d0c993a078"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2329f86c-0e51-41e8-b2c4-a6d0c993a078",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d160c2b7-c5f2-4ef7-a0b9-e9ac5a3d84c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}