{
    "id": "468df46a-d909-45fb-acb6-554e3c2e24f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkang_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36212131-1227-4961-824f-882318ba6407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "638bc274-7385-4eca-a5a2-28cdaf0368bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36212131-1227-4961-824f-882318ba6407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca727e6-b717-4bdf-b6e0-cddc1c14aea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36212131-1227-4961-824f-882318ba6407",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "7b5c7847-7b67-4d94-98a6-efb645cca066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "b8151d9e-f8a6-4af8-930d-2f030ff95f78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5c7847-7b67-4d94-98a6-efb645cca066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2b932d-c09f-46fa-9a01-33acf86cf92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5c7847-7b67-4d94-98a6-efb645cca066",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "6dca920a-c437-47a3-b63d-6d9796497865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "81383c6a-e9eb-4ac7-a518-f1466d2a8195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dca920a-c437-47a3-b63d-6d9796497865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37249f00-3c48-4827-b881-0921b6b08139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dca920a-c437-47a3-b63d-6d9796497865",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "4dd2600a-8e22-433d-bf68-3a750dc3c4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "113226bf-9d88-4491-a2e8-a624a8ec935a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd2600a-8e22-433d-bf68-3a750dc3c4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25ebecf-16ea-42ea-890f-0744a08de102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd2600a-8e22-433d-bf68-3a750dc3c4e0",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "2a5a6fe6-8530-49e5-bbcd-7034fbc27b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "447a2bb5-b50c-4044-b31f-fe6f53f9c786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a5a6fe6-8530-49e5-bbcd-7034fbc27b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb376be6-3eed-49c3-82b1-e8bc7e96af7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a5a6fe6-8530-49e5-bbcd-7034fbc27b52",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "c2c31121-f73d-4be0-bc45-135b92556040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "1b7e2bf4-f5a9-4227-9467-5564a5a77171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c31121-f73d-4be0-bc45-135b92556040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc72fcea-775f-4d9c-b0e6-4a3027a78dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c31121-f73d-4be0-bc45-135b92556040",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "89d5fa2d-b6d8-4595-a9ad-25ad279ea396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "4b0970fc-14e6-4dab-a45e-0998969373f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d5fa2d-b6d8-4595-a9ad-25ad279ea396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a828112d-d9cc-439f-924e-30d0f45a557b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d5fa2d-b6d8-4595-a9ad-25ad279ea396",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "49dccbba-60bb-4008-adfb-537e59b8ba5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "007f9e36-5eb1-4d77-8a5b-73f1f6c52663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49dccbba-60bb-4008-adfb-537e59b8ba5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5debce3-7cb2-4fae-80a9-253265eb8808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49dccbba-60bb-4008-adfb-537e59b8ba5e",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        },
        {
            "id": "719fcf34-814f-490f-a34b-a5933570fa64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "compositeImage": {
                "id": "ebf75793-79bb-4fd4-a423-0ac8f340672f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719fcf34-814f-490f-a34b-a5933570fa64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619179a1-7d80-4070-9a50-8024eca71147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719fcf34-814f-490f-a34b-a5933570fa64",
                    "LayerId": "0c6cf860-183e-497a-8577-4f95bb483416"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "0c6cf860-183e-497a-8577-4f95bb483416",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "468df46a-d909-45fb-acb6-554e3c2e24f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}