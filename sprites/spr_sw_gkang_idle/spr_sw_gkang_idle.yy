{
    "id": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkang_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e11e0a8-9201-4c0e-9823-c50e8ac0d6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "1a808935-e41d-429d-9526-0055af49b938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e11e0a8-9201-4c0e-9823-c50e8ac0d6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c46cfd3-b594-4f41-a404-02f1efd16199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e11e0a8-9201-4c0e-9823-c50e8ac0d6e5",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "1002d7fa-0a1c-4671-8286-06210e3b9d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "0387abaf-e7f9-49cc-b471-87680943c0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1002d7fa-0a1c-4671-8286-06210e3b9d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c8b209-76d1-4141-b602-96fc3a960ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1002d7fa-0a1c-4671-8286-06210e3b9d5d",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "5a7a363d-c906-4326-ba43-b15fcd35d8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "6629f3c2-e3db-4c7b-9c68-ed414f011e38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7a363d-c906-4326-ba43-b15fcd35d8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "912ee72b-6487-4a3c-a0b9-8a7c26857532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7a363d-c906-4326-ba43-b15fcd35d8f0",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "ccc0fe6e-5c2d-45fe-a5bb-2cc3f27bfe27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "20140bff-8230-4a85-a88e-fa9efb776159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc0fe6e-5c2d-45fe-a5bb-2cc3f27bfe27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d42790-9628-4537-b40a-0d285b420d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc0fe6e-5c2d-45fe-a5bb-2cc3f27bfe27",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "262a7e7b-d507-471c-8ff9-b9fdf1c34de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "21f5506d-77ff-4965-b21b-d85b87fd3327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "262a7e7b-d507-471c-8ff9-b9fdf1c34de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c024770-40c6-449f-af2c-c3acb1b3dd7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "262a7e7b-d507-471c-8ff9-b9fdf1c34de8",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "f5b72d98-0ef7-4756-af96-7da5044441cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "1c1014fe-1334-4eb5-a9f5-8788932a7261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b72d98-0ef7-4756-af96-7da5044441cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13fd3c7-5a56-4998-9720-cc996e58a948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b72d98-0ef7-4756-af96-7da5044441cd",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "9f3d14a7-c6c7-4c47-8373-3239e39d5744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "538c998e-f638-4e5f-ac92-efe6bbefc1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3d14a7-c6c7-4c47-8373-3239e39d5744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b185e16c-1413-4a3f-82c0-fbb7eb2cec14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3d14a7-c6c7-4c47-8373-3239e39d5744",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "332b4012-09c0-4078-bba3-46237c9d2ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "fa8aa200-b16e-494b-b565-d4beb17f63f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "332b4012-09c0-4078-bba3-46237c9d2ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86f054e-891c-4a62-bbcf-8822867887bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "332b4012-09c0-4078-bba3-46237c9d2ffc",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        },
        {
            "id": "f44a5c9d-3f0b-4a91-8380-12fa526f1635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "compositeImage": {
                "id": "097cdfd7-b42e-4f71-b157-c841c2a745ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44a5c9d-3f0b-4a91-8380-12fa526f1635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4b8c73-f4dd-4163-a5b8-b680cba42357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44a5c9d-3f0b-4a91-8380-12fa526f1635",
                    "LayerId": "53b46056-ef9c-4bf6-8243-82cc10a40cc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "53b46056-ef9c-4bf6-8243-82cc10a40cc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8faf04a5-b6ef-4be7-ba04-dc81cbd55df2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}