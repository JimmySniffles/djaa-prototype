{
    "id": "75bc7612-0aef-4c2f-9a71-e8a49d863b27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rbsnake_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a335fd3-1281-4592-aaf2-8b28c848a376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75bc7612-0aef-4c2f-9a71-e8a49d863b27",
            "compositeImage": {
                "id": "48bc827c-daad-460c-aade-b168b1b13ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a335fd3-1281-4592-aaf2-8b28c848a376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785f70bf-4553-41e7-9095-d1f281c11841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a335fd3-1281-4592-aaf2-8b28c848a376",
                    "LayerId": "902bc326-87bc-4c29-b971-92eab97e8c8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "902bc326-87bc-4c29-b971-92eab97e8c8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75bc7612-0aef-4c2f-9a71-e8a49d863b27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}