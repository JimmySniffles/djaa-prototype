{
    "id": "4c2d65ce-5e01-4661-a960-2c55f8c3e4ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rbsnake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a23efce-390c-4529-9450-f6cc18aed048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c2d65ce-5e01-4661-a960-2c55f8c3e4ce",
            "compositeImage": {
                "id": "41ce748b-4671-4d7f-8a6d-80e0eae0f1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a23efce-390c-4529-9450-f6cc18aed048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "378918ac-64c7-4b8d-b498-a0ad37f6c1fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a23efce-390c-4529-9450-f6cc18aed048",
                    "LayerId": "bb77959b-b464-4969-bf58-2b430824c5c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "bb77959b-b464-4969-bf58-2b430824c5c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c2d65ce-5e01-4661-a960-2c55f8c3e4ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}