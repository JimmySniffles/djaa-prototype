{
    "id": "3542020d-ca8d-4bb4-b272-cd1355a8f34a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkang_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9643a12-5ce3-44d5-b017-06626ceb9958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3542020d-ca8d-4bb4-b272-cd1355a8f34a",
            "compositeImage": {
                "id": "a29c75ce-8bef-44aa-bd3f-53ee6a67d6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9643a12-5ce3-44d5-b017-06626ceb9958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a986f3b4-b4cf-41e5-90d0-e285e6a4a1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9643a12-5ce3-44d5-b017-06626ceb9958",
                    "LayerId": "c9897de7-66bd-43e2-8ed1-bf846ab2e1e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "c9897de7-66bd-43e2-8ed1-bf846ab2e1e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3542020d-ca8d-4bb4-b272-cd1355a8f34a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}