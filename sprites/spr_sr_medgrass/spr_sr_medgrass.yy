{
    "id": "eef0da46-785a-4f60-8411-2a84f269c347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sr_medgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 1022,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b58e89b-acfc-45b2-8fad-da114d209acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef0da46-785a-4f60-8411-2a84f269c347",
            "compositeImage": {
                "id": "be78a5fc-dc5b-48ef-aed4-e09d8a02aec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b58e89b-acfc-45b2-8fad-da114d209acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740f9b54-9319-46c0-ba4e-dcbdfd6f4400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b58e89b-acfc-45b2-8fad-da114d209acf",
                    "LayerId": "c9198b09-c9d9-44a2-95f0-2f8ecde1e1b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c9198b09-c9d9-44a2-95f0-2f8ecde1e1b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eef0da46-785a-4f60-8411-2a84f269c347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}