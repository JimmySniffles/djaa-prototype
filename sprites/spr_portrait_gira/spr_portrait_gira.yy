{
    "id": "6ff6962f-15b3-4fbc-a015-4d86c94cc6a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_gira",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 31,
    "bbox_right": 96,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ec1ccd1-b198-4300-b4a6-c2f27a8a07fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff6962f-15b3-4fbc-a015-4d86c94cc6a1",
            "compositeImage": {
                "id": "b3d2d793-3389-4f40-8d4a-f01d3eb1a75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec1ccd1-b198-4300-b4a6-c2f27a8a07fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef96193-e314-42c2-9719-382dfab9295b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec1ccd1-b198-4300-b4a6-c2f27a8a07fd",
                    "LayerId": "96379661-e390-4a25-916a-010a41b1efac"
                }
            ]
        },
        {
            "id": "7188ca6a-e24d-47fb-bd2e-d5f94a6f9154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff6962f-15b3-4fbc-a015-4d86c94cc6a1",
            "compositeImage": {
                "id": "1fbec539-3761-4de2-848f-43885faa2ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7188ca6a-e24d-47fb-bd2e-d5f94a6f9154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ced6e3-7717-4217-b360-109b41345ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7188ca6a-e24d-47fb-bd2e-d5f94a6f9154",
                    "LayerId": "96379661-e390-4a25-916a-010a41b1efac"
                }
            ]
        },
        {
            "id": "ec56b10f-0f51-4199-97ca-169272ecc110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff6962f-15b3-4fbc-a015-4d86c94cc6a1",
            "compositeImage": {
                "id": "5bab80b5-386a-45e9-8b4c-5cb2f7d673ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec56b10f-0f51-4199-97ca-169272ecc110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84d29ad-60aa-44b8-a6b5-09ff4797309b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec56b10f-0f51-4199-97ca-169272ecc110",
                    "LayerId": "96379661-e390-4a25-916a-010a41b1efac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "96379661-e390-4a25-916a-010a41b1efac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ff6962f-15b3-4fbc-a015-4d86c94cc6a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}