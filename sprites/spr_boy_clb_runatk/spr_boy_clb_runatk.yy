{
    "id": "7c298523-d72a-466e-8ad1-22c77903b67b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_runatk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3be2cfc-e5c0-4c71-a02d-ec95412e20ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c298523-d72a-466e-8ad1-22c77903b67b",
            "compositeImage": {
                "id": "d50dc912-2225-434d-a117-179e89cf81e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3be2cfc-e5c0-4c71-a02d-ec95412e20ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83fe8b1e-e228-4930-a8a7-9a1138b3085e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3be2cfc-e5c0-4c71-a02d-ec95412e20ec",
                    "LayerId": "d9ba90ff-1939-4b2d-98df-2df94e5dd4b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d9ba90ff-1939-4b2d-98df-2df94e5dd4b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c298523-d72a-466e-8ad1-22c77903b67b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}