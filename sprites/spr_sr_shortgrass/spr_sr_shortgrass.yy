{
    "id": "d36d6c59-fd91-4cbc-8f14-291f1a43814b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sr_shortgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 1022,
    "bbox_top": 57,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da640e01-de41-4f8d-b3a0-26ca91bb1470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d36d6c59-fd91-4cbc-8f14-291f1a43814b",
            "compositeImage": {
                "id": "ed656a54-eb06-4a18-842f-168012003aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da640e01-de41-4f8d-b3a0-26ca91bb1470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0429b94-1a39-4c40-99d4-0bfbe7eb6ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da640e01-de41-4f8d-b3a0-26ca91bb1470",
                    "LayerId": "be81d521-7686-4ba6-a807-cba24811f262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be81d521-7686-4ba6-a807-cba24811f262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d36d6c59-fd91-4cbc-8f14-291f1a43814b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}