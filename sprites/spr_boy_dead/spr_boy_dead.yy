{
    "id": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56d53149-aa4b-4d2f-ac31-7f1f993ce1dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "4392f2b1-a672-4226-91b8-24532c356388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d53149-aa4b-4d2f-ac31-7f1f993ce1dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d3ba4f-6432-41dc-85eb-910f3f6fc331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d53149-aa4b-4d2f-ac31-7f1f993ce1dc",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "4c6ebc0c-94e7-4941-b54f-d4bfc55b31ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "dd19edbc-aa6e-47a7-8d57-946923650135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c6ebc0c-94e7-4941-b54f-d4bfc55b31ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f2ed02-dd48-44dc-b7a6-b7f717df02b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c6ebc0c-94e7-4941-b54f-d4bfc55b31ab",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "760aa2be-cd8c-4e24-9725-df0bde6206bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "484047de-54e0-4eaf-8d8d-1ff2538dc406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760aa2be-cd8c-4e24-9725-df0bde6206bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21bbae6-3307-48cf-a70a-1d640e6ece37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760aa2be-cd8c-4e24-9725-df0bde6206bb",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "b68bd85e-cad2-41db-96df-bfe5d0ecd575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "7038c3e1-ce67-43b8-aea2-6354a2155bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68bd85e-cad2-41db-96df-bfe5d0ecd575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517d66e7-b537-4324-9b79-7d4a517c0ca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68bd85e-cad2-41db-96df-bfe5d0ecd575",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "4ade290b-9146-4650-a69a-52893a722df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "1dbf2eb0-d543-47d2-ab88-5d8907710ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ade290b-9146-4650-a69a-52893a722df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5721a3-faa0-4f27-ac80-19f68a8c47f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ade290b-9146-4650-a69a-52893a722df1",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "f2839eee-2bd2-4dac-a1cc-c73a167725f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "675aac95-6c49-4999-a170-8f770c7f86db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2839eee-2bd2-4dac-a1cc-c73a167725f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "250a470d-1459-4afb-94be-a6011e3a9f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2839eee-2bd2-4dac-a1cc-c73a167725f2",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "b56a52df-7c33-4a13-abf9-737504e798f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "8a908c96-aefd-4189-9659-52a5b3272782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b56a52df-7c33-4a13-abf9-737504e798f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0db26e9-f567-4468-af62-bc875b959f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b56a52df-7c33-4a13-abf9-737504e798f1",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        },
        {
            "id": "462ad32f-71c4-4290-9e77-844719495f07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "compositeImage": {
                "id": "8ba6247c-2420-4852-8a5b-d87ca9a346ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462ad32f-71c4-4290-9e77-844719495f07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f035df61-03a3-472f-a761-706947a3460f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462ad32f-71c4-4290-9e77-844719495f07",
                    "LayerId": "586c9954-40bc-48c6-b2df-ef075dc42b83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "586c9954-40bc-48c6-b2df-ef075dc42b83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6f9f819-aab1-4ff1-8a98-96311c5868f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}