{
    "id": "59c88261-4e3f-4810-92f7-cebb791f6e92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coolum_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0feb080e-fc1d-433b-86b9-2887379d5e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59c88261-4e3f-4810-92f7-cebb791f6e92",
            "compositeImage": {
                "id": "4f9898aa-c8f1-4fb9-b7f4-c6e71735de90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0feb080e-fc1d-433b-86b9-2887379d5e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fecf7e15-69d8-4b70-a938-0f6b395cbf5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0feb080e-fc1d-433b-86b9-2887379d5e42",
                    "LayerId": "10b1540f-3710-439e-a08e-47ad84d1f9da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "10b1540f-3710-439e-a08e-47ad84d1f9da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59c88261-4e3f-4810-92f7-cebb791f6e92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}