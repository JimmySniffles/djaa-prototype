{
    "id": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6aed58b-9e14-443f-8178-2a59bbb29f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
            "compositeImage": {
                "id": "8b991796-d66a-4ece-9d1e-ee1236c75464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6aed58b-9e14-443f-8178-2a59bbb29f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66cfce21-8971-4fec-aa18-33ced6dea1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6aed58b-9e14-443f-8178-2a59bbb29f5a",
                    "LayerId": "1ee84632-e208-4e92-b496-1bb91f14d2a4"
                }
            ]
        },
        {
            "id": "46ce7688-e2da-45dc-a3ff-ab9eddc371db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
            "compositeImage": {
                "id": "f08b31a5-76e7-4548-b09a-469f4fb33c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ce7688-e2da-45dc-a3ff-ab9eddc371db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6960bef-22d9-4e8d-95ca-5a7fffab12c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ce7688-e2da-45dc-a3ff-ab9eddc371db",
                    "LayerId": "1ee84632-e208-4e92-b496-1bb91f14d2a4"
                }
            ]
        },
        {
            "id": "322d04e7-cfac-4539-baf0-e6ea297fc0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
            "compositeImage": {
                "id": "13742842-de73-40cb-9306-dec0f21167ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322d04e7-cfac-4539-baf0-e6ea297fc0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4a3bf3-c7f5-47e3-9895-e5971bdaded9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322d04e7-cfac-4539-baf0-e6ea297fc0e8",
                    "LayerId": "1ee84632-e208-4e92-b496-1bb91f14d2a4"
                }
            ]
        },
        {
            "id": "5398429d-8fb7-477b-8e69-bd59bf5a51ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
            "compositeImage": {
                "id": "467d42b7-22d6-4ecd-bd6b-2cfec5d46a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5398429d-8fb7-477b-8e69-bd59bf5a51ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5143e209-31be-4fc0-9412-aa8a109890e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5398429d-8fb7-477b-8e69-bd59bf5a51ad",
                    "LayerId": "1ee84632-e208-4e92-b496-1bb91f14d2a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1ee84632-e208-4e92-b496-1bb91f14d2a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0034e70-6edb-4351-ba88-bb7dc7cc2e4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}