{
    "id": "17029909-3f15-4d37-a718-89687781c39e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkoala",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98888c3b-cb45-47fe-84b5-de1e43868dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17029909-3f15-4d37-a718-89687781c39e",
            "compositeImage": {
                "id": "a840dc1a-0118-48f3-a4d2-a79f41d53369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98888c3b-cb45-47fe-84b5-de1e43868dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d4994d-2731-4db6-b530-c094e5a1ab7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98888c3b-cb45-47fe-84b5-de1e43868dcf",
                    "LayerId": "19a830d0-fb10-4799-806b-bc8e280bc652"
                }
            ]
        },
        {
            "id": "368a4a6b-ba44-4c8a-9735-29939cc5a210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17029909-3f15-4d37-a718-89687781c39e",
            "compositeImage": {
                "id": "077b1f86-435d-4804-a15b-1ff7bb2a377f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368a4a6b-ba44-4c8a-9735-29939cc5a210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18712d2-3bb0-44da-bd39-cf53fe6e4ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368a4a6b-ba44-4c8a-9735-29939cc5a210",
                    "LayerId": "19a830d0-fb10-4799-806b-bc8e280bc652"
                }
            ]
        },
        {
            "id": "15ef2f38-4b42-427f-b53b-c9a032b4cbb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17029909-3f15-4d37-a718-89687781c39e",
            "compositeImage": {
                "id": "237cb5fa-c1ab-4006-8818-ce687a1aac2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ef2f38-4b42-427f-b53b-c9a032b4cbb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f5b8ba-21ad-44ca-9cf6-38f78b023661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ef2f38-4b42-427f-b53b-c9a032b4cbb3",
                    "LayerId": "19a830d0-fb10-4799-806b-bc8e280bc652"
                }
            ]
        },
        {
            "id": "7d2091bd-18a6-42dd-af14-27e31e5f9e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17029909-3f15-4d37-a718-89687781c39e",
            "compositeImage": {
                "id": "3acf61d2-b05e-460f-9631-d0040a685801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2091bd-18a6-42dd-af14-27e31e5f9e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7687d9b-8f75-427b-8ea9-8e862206ac28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2091bd-18a6-42dd-af14-27e31e5f9e51",
                    "LayerId": "19a830d0-fb10-4799-806b-bc8e280bc652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "19a830d0-fb10-4799-806b-bc8e280bc652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17029909-3f15-4d37-a718-89687781c39e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}