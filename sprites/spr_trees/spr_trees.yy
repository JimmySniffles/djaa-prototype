{
    "id": "ebf92ea2-9e5f-4907-a4d6-b6e3368876a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2047,
    "bbox_left": 21,
    "bbox_right": 1130,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b414717-088a-462c-ba7c-b89d551cd36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf92ea2-9e5f-4907-a4d6-b6e3368876a1",
            "compositeImage": {
                "id": "1100e573-208c-4fae-a6d7-afd4794fb992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b414717-088a-462c-ba7c-b89d551cd36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a481978-7dab-4bb3-b49f-ab6d69d544fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b414717-088a-462c-ba7c-b89d551cd36a",
                    "LayerId": "0cca763c-3d55-444f-ad1b-e6a698e1ea25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2048,
    "layers": [
        {
            "id": "0cca763c-3d55-444f-ad1b-e6a698e1ea25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebf92ea2-9e5f-4907-a4d6-b6e3368876a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1152,
    "xorig": 0,
    "yorig": 0
}