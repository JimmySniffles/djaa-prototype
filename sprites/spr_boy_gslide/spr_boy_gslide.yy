{
    "id": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_gslide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d4731be-cacc-4ea8-b002-9ca24bcbe032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "14572494-266b-4227-a773-859ae0af3ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4731be-cacc-4ea8-b002-9ca24bcbe032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "808559fa-902b-42fd-8dbd-d6d119911d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4731be-cacc-4ea8-b002-9ca24bcbe032",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "676c057a-9396-4199-9929-088d05874ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "676630c6-d981-4469-9630-ff07ce74e293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676c057a-9396-4199-9929-088d05874ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1fa32c7-0a00-4752-b3c9-aa78f459c854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676c057a-9396-4199-9929-088d05874ec1",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "6754eb8a-f5f3-4efe-8716-eaa64530a582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "27e7da67-892b-422a-83db-8e5d66265336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6754eb8a-f5f3-4efe-8716-eaa64530a582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "609ef219-4a62-4e7d-9686-973c7437f7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6754eb8a-f5f3-4efe-8716-eaa64530a582",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "5ce4ab82-08fc-43f7-bd22-e76f20446d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "ab99a824-73b0-4346-8744-50f1ac79ec3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce4ab82-08fc-43f7-bd22-e76f20446d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff64890-d7be-4fb3-9306-7d1598be11d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce4ab82-08fc-43f7-bd22-e76f20446d85",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "01490889-aed2-48e1-acca-bc4681cbb42b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "ca5b5ddd-affb-42ec-8d71-79840fb6afd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01490889-aed2-48e1-acca-bc4681cbb42b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef88496b-881f-4525-a710-114d9571dcd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01490889-aed2-48e1-acca-bc4681cbb42b",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "ab5ad058-d67f-4d26-af0a-3bb3581cb205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "ca832d6f-7f82-4842-b140-a46c62c7b4cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab5ad058-d67f-4d26-af0a-3bb3581cb205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "378dd550-4ebb-473b-b7df-71139d113e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab5ad058-d67f-4d26-af0a-3bb3581cb205",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "a6c72a14-f541-4329-ba00-64eb31f572c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "ce1c269b-1a76-4334-878f-51be748511a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c72a14-f541-4329-ba00-64eb31f572c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42100dd-4462-4756-9003-8701ded47ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c72a14-f541-4329-ba00-64eb31f572c5",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "8cb44671-a529-496b-9d77-263661b8514a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "9979dfff-ba82-42f4-b8ca-d94bb02ed994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb44671-a529-496b-9d77-263661b8514a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b9dd47-47da-4f5f-b4a7-2c83fc2b9ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb44671-a529-496b-9d77-263661b8514a",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "12f962a6-f950-4a49-bc43-b502e0ecd26e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "f8bccd21-2a54-4922-bae9-62dc7c547f8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f962a6-f950-4a49-bc43-b502e0ecd26e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40407b1e-d784-4788-8559-53f9af79a5d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f962a6-f950-4a49-bc43-b502e0ecd26e",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "b2378231-2b6a-46b5-8f30-2110658f6b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "ac37fdad-f8d1-4490-a04f-bbe5f1926c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2378231-2b6a-46b5-8f30-2110658f6b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b62d9ed-3385-4a8c-baba-455b7027f7ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2378231-2b6a-46b5-8f30-2110658f6b6f",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "f16fe25f-8cd1-4778-9112-7b3442cd8754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "338d76db-49ae-4afb-9a9a-6268caf3261d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16fe25f-8cd1-4778-9112-7b3442cd8754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9890f930-092a-4194-b308-6ddf96a6312e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16fe25f-8cd1-4778-9112-7b3442cd8754",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "e8559a64-8440-4407-b874-8c8fa30f5d08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "73ab6eee-e91b-442f-a6bb-f87386bf64ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8559a64-8440-4407-b874-8c8fa30f5d08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c12e0f7c-f358-417a-bc1a-be979b2bda9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8559a64-8440-4407-b874-8c8fa30f5d08",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "c4327739-24ea-4ddc-97fb-571643275b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "0e46ec94-67f3-4459-b261-5a997bb9250f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4327739-24ea-4ddc-97fb-571643275b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e065f00d-4ba1-4a15-8296-cedfbfa91114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4327739-24ea-4ddc-97fb-571643275b6c",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "978ff511-6378-4a6e-aebe-3130676cc880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "a5561825-d469-4272-b08c-bb5b873cf0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978ff511-6378-4a6e-aebe-3130676cc880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90cf7492-3ad5-4679-b7a8-77101f440154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978ff511-6378-4a6e-aebe-3130676cc880",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "46a6aa82-6eaa-4c35-945e-6a6aa9aaf262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "6eddbbd1-94d8-430e-9ec4-182a04c63c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a6aa82-6eaa-4c35-945e-6a6aa9aaf262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a336da-9079-4446-9611-5fd001813fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a6aa82-6eaa-4c35-945e-6a6aa9aaf262",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "bff79f08-c19d-4986-b829-89c7cd0afa38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "47140c01-d1e5-4cf2-afe2-d5f9286f8612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bff79f08-c19d-4986-b829-89c7cd0afa38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acb9cfe-e59c-47d1-bb00-404e458dba40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bff79f08-c19d-4986-b829-89c7cd0afa38",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "42d09e83-d3dd-48ab-95b8-efc834dd36b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "152c8e68-f3a8-49ad-b99b-3295655403ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42d09e83-d3dd-48ab-95b8-efc834dd36b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f601af-db4b-4425-9112-31e74abace12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d09e83-d3dd-48ab-95b8-efc834dd36b1",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "cba124c3-e4ee-4692-a42b-53300b15ef8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "731adf23-0ab3-4d89-a53f-ce0965066134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba124c3-e4ee-4692-a42b-53300b15ef8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba196f4c-1e82-45ca-a33d-91e7fe6dda65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba124c3-e4ee-4692-a42b-53300b15ef8d",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "12316fa4-3771-472b-89f0-43beaa223452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "bda8e208-3601-43e4-8bb8-3ee2e63c83d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12316fa4-3771-472b-89f0-43beaa223452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6173ec-5b86-48ad-b4cf-1aafca9c9fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12316fa4-3771-472b-89f0-43beaa223452",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        },
        {
            "id": "18921cad-dd00-4163-8fb1-4df155575697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "compositeImage": {
                "id": "17360cf4-c34e-46d9-9528-ffd49f440c13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18921cad-dd00-4163-8fb1-4df155575697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d94f863-5cf1-4071-8e53-865e406e5994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18921cad-dd00-4163-8fb1-4df155575697",
                    "LayerId": "5bac3a08-62bd-447f-a2b0-9cef79a77983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5bac3a08-62bd-447f-a2b0-9cef79a77983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "383db7a7-4579-4f53-93ea-7bb3e3d2c52c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}