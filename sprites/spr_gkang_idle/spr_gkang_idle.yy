{
    "id": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkang_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55e66408-ef84-4a94-bf25-c744b3a07759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "2b114a3c-9692-408c-8cde-c20b622a206a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e66408-ef84-4a94-bf25-c744b3a07759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3002c07f-17b4-4506-b3bc-5534ab889db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e66408-ef84-4a94-bf25-c744b3a07759",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "74dc6d97-0186-48af-ab23-85fc31ab0d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "2eb1980e-79d8-4499-a314-7ae31cafb6cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74dc6d97-0186-48af-ab23-85fc31ab0d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde6eec6-baeb-4d38-8433-8d7eda408c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74dc6d97-0186-48af-ab23-85fc31ab0d33",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "1374d2fe-6165-4845-b0d2-1994e7800337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "5bde960f-e79d-4161-80ae-767b5f8ea5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1374d2fe-6165-4845-b0d2-1994e7800337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed6341ca-853b-4c71-b0ca-be8a8b70e534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1374d2fe-6165-4845-b0d2-1994e7800337",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "bb16af23-03bf-4dcb-96e3-f564b88f5099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "2831b86c-4ba0-408b-9284-663ed50401e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb16af23-03bf-4dcb-96e3-f564b88f5099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93078a71-a930-48c0-9939-b57860f16796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb16af23-03bf-4dcb-96e3-f564b88f5099",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "c9e1372d-20e9-4292-aa66-6e7a72c3f6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "a0137f28-cba8-48e3-be62-10abf6b6518d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e1372d-20e9-4292-aa66-6e7a72c3f6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "300e6491-0d9c-4493-9b6e-7ccf152c02bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e1372d-20e9-4292-aa66-6e7a72c3f6a8",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "78d081f3-84a7-4e43-a2e1-f106bb5a42a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "5b1c9f25-1de7-4308-8dd3-1568b37120ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d081f3-84a7-4e43-a2e1-f106bb5a42a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ad3e18-49fd-44cd-a0b1-5823f481eba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d081f3-84a7-4e43-a2e1-f106bb5a42a6",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "7f4509ba-8b20-4a41-9f90-95f82c701f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "0014f871-0d83-4cf4-bb7f-b27ec21d1835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4509ba-8b20-4a41-9f90-95f82c701f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29def810-2eef-4b89-be3d-c3491d69c142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4509ba-8b20-4a41-9f90-95f82c701f9c",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "7c232da7-e4af-4072-be7d-6673f6747a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "3d7b70bc-bd10-4c59-978d-f069f35fba30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c232da7-e4af-4072-be7d-6673f6747a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b12658ac-26b4-4e92-9927-2b297ef15c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c232da7-e4af-4072-be7d-6673f6747a0a",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        },
        {
            "id": "ed13a63d-3b1d-4c6b-91ac-916e0ddf51c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "compositeImage": {
                "id": "16a53986-c760-4b3d-8e36-f67211127c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed13a63d-3b1d-4c6b-91ac-916e0ddf51c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207aa83a-c8c8-4a01-8461-d5a13606cfde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed13a63d-3b1d-4c6b-91ac-916e0ddf51c5",
                    "LayerId": "c4289888-f603-409e-8b5f-a60a8f1f1993"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "c4289888-f603-409e-8b5f-a60a8f1f1993",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37df2b7d-3d7c-4704-803c-0adc1bf9f075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}