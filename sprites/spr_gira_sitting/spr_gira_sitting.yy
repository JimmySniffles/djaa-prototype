{
    "id": "7ed9dbe8-a2cf-4e28-aa97-a40a728fd6dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gira_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 38,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f345830-7a4d-4390-85a6-eed998cc23d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ed9dbe8-a2cf-4e28-aa97-a40a728fd6dd",
            "compositeImage": {
                "id": "9a06f9a1-1d48-449c-b02d-e34beb68d940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f345830-7a4d-4390-85a6-eed998cc23d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae9e34f-07d6-4b52-9ca4-caeb4203947f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f345830-7a4d-4390-85a6-eed998cc23d3",
                    "LayerId": "3b532eb6-9751-4b3f-83ff-0584c2735804"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3b532eb6-9751-4b3f-83ff-0584c2735804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ed9dbe8-a2cf-4e28-aa97-a40a728fd6dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}