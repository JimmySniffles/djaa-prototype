{
    "id": "3377d68a-10dc-4ac9-a61c-9cfedb027896",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tsnake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b95cd3d-97c1-434c-a8bf-fce7a2f83ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3377d68a-10dc-4ac9-a61c-9cfedb027896",
            "compositeImage": {
                "id": "c8b40c0e-b6e9-4718-bdd5-b1a4d0e64d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b95cd3d-97c1-434c-a8bf-fce7a2f83ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528a8c64-6767-441f-85cd-1a167db40855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b95cd3d-97c1-434c-a8bf-fce7a2f83ffc",
                    "LayerId": "4266ba8e-403b-4876-9000-6b8f3dfeaeb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "4266ba8e-403b-4876-9000-6b8f3dfeaeb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3377d68a-10dc-4ac9-a61c-9cfedb027896",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}