{
    "id": "fdc45548-1597-4c7e-a859-85e85700cb83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_gkang_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9a43646-fb74-4344-a24e-d0fd92b43500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "8cf3c8d1-7597-4d38-ae98-d8af2addcd48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a43646-fb74-4344-a24e-d0fd92b43500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42eec042-6e0d-44df-8cba-589e98e9ec9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a43646-fb74-4344-a24e-d0fd92b43500",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "53750445-a01a-431e-810c-2cc10686cd43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "27822457-fc6a-4bd6-90ff-60cccc4c290b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53750445-a01a-431e-810c-2cc10686cd43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e12c17f-2f4f-41f6-9135-864f00db8da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53750445-a01a-431e-810c-2cc10686cd43",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "ac9a4734-fafe-4e24-8f13-3060a68b1f18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "6676be37-d080-4fb1-a5dd-7f7c98c0330d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9a4734-fafe-4e24-8f13-3060a68b1f18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "840fe51f-ee5f-45b4-9acb-5478bddab612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9a4734-fafe-4e24-8f13-3060a68b1f18",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "eed25b02-e2d5-431b-bc42-97b6ddf1d161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "7a57aa76-eecb-49b2-9095-d626725d659f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed25b02-e2d5-431b-bc42-97b6ddf1d161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4133acb-bcce-4c24-9c0f-2f3232eb0303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed25b02-e2d5-431b-bc42-97b6ddf1d161",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "68e2351d-65e7-40cd-92bf-2223f44505f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "791a7a74-6f40-4019-821a-67efe7031015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e2351d-65e7-40cd-92bf-2223f44505f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531beda0-1fa5-4f6a-8a1f-06e73449946a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e2351d-65e7-40cd-92bf-2223f44505f4",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "4a507cb4-f60c-432e-91d6-5a63ebc48f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "b68293a8-199e-4cf8-b7cb-51328ef9f316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a507cb4-f60c-432e-91d6-5a63ebc48f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a687e42-367e-42f1-9bfc-f3370a37c4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a507cb4-f60c-432e-91d6-5a63ebc48f69",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "aef3ee38-3914-4781-a4dd-8f6047468215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "c24151e2-b9f2-4652-8bc3-2c7918a2c68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef3ee38-3914-4781-a4dd-8f6047468215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f2437a9-30f2-437e-bcd8-f41c59e95f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef3ee38-3914-4781-a4dd-8f6047468215",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "80a4eaaf-c035-44d6-90bb-fae4bb2cd171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "9e8dd388-63c0-4354-899c-87ce829e7c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a4eaaf-c035-44d6-90bb-fae4bb2cd171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca60aaf-20df-42eb-b628-55a55c3b2b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a4eaaf-c035-44d6-90bb-fae4bb2cd171",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        },
        {
            "id": "9321f91c-adf1-4153-862b-9c646bfa3de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "compositeImage": {
                "id": "0951fb41-4bba-4835-8b82-63b0d2f0b64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9321f91c-adf1-4153-862b-9c646bfa3de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41570958-2c77-45b3-a2de-539ab0e024b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9321f91c-adf1-4153-862b-9c646bfa3de5",
                    "LayerId": "8aff94d6-942b-4e66-bd45-2fbb1febc45c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "8aff94d6-942b-4e66-bd45-2fbb1febc45c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdc45548-1597-4c7e-a859-85e85700cb83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}