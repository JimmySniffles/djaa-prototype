{
    "id": "a50b4646-d35f-444c-963d-fcd088f8e24f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menuoverlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34fbcf52-ef5c-430a-b610-ec3f114d410e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a50b4646-d35f-444c-963d-fcd088f8e24f",
            "compositeImage": {
                "id": "42453116-ddb3-4e64-8154-81baae63528b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fbcf52-ef5c-430a-b610-ec3f114d410e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13567f51-7ce6-4716-b270-ca0485e2765d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fbcf52-ef5c-430a-b610-ec3f114d410e",
                    "LayerId": "bd40943b-c2e2-4f4d-bdd9-f07a1f7bca49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "bd40943b-c2e2-4f4d-bdd9-f07a1f7bca49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a50b4646-d35f-444c-963d-fcd088f8e24f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 239,
    "yorig": 299
}