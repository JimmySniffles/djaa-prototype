{
    "id": "ed01022b-ae64-4998-9cbe-e0417a72f0b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_babun_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 47,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63192539-d760-47e4-a0e0-8ac5e7a687f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed01022b-ae64-4998-9cbe-e0417a72f0b9",
            "compositeImage": {
                "id": "b31a5de7-8e7b-4506-a1b9-432800212537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63192539-d760-47e4-a0e0-8ac5e7a687f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b0474e3-ca0f-442b-9326-71f3fd762386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63192539-d760-47e4-a0e0-8ac5e7a687f9",
                    "LayerId": "771f08ae-8f48-4fba-931d-93203462304c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "771f08ae-8f48-4fba-931d-93203462304c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed01022b-ae64-4998-9cbe-e0417a72f0b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}