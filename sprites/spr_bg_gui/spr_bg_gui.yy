{
    "id": "5388d37c-eb52-4c47-ac15-267f103823a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_gui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fab49598-1a1b-4ae4-92d5-4aa038bad85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5388d37c-eb52-4c47-ac15-267f103823a4",
            "compositeImage": {
                "id": "ad48847b-f612-4bc3-b597-5a8b3f0f35af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab49598-1a1b-4ae4-92d5-4aa038bad85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "affc8c7e-eab2-4908-b520-f9899f12a5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab49598-1a1b-4ae4-92d5-4aa038bad85a",
                    "LayerId": "14cb8668-0d9d-488c-b8fc-57257e17be77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "14cb8668-0d9d-488c-b8fc-57257e17be77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5388d37c-eb52-4c47-ac15-267f103823a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}