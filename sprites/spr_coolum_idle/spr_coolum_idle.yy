{
    "id": "dead7c2d-f245-4510-8fda-60a2b6f8c937",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coolum_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b86c197c-e042-459b-bc11-7873adb973f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dead7c2d-f245-4510-8fda-60a2b6f8c937",
            "compositeImage": {
                "id": "21a8378e-853e-492d-8def-036997ff77d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86c197c-e042-459b-bc11-7873adb973f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c98745c-9250-48ed-843d-3032da704b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86c197c-e042-459b-bc11-7873adb973f6",
                    "LayerId": "edf36976-e25d-448d-aed1-a12ccef0bdf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "edf36976-e25d-448d-aed1-a12ccef0bdf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dead7c2d-f245-4510-8fda-60a2b6f8c937",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}