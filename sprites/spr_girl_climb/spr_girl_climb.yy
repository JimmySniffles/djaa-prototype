{
    "id": "3ef336b2-21e5-40b9-a70a-a68a95c4a1fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_climb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d458951-3bd4-4781-b241-967af7f3d839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ef336b2-21e5-40b9-a70a-a68a95c4a1fd",
            "compositeImage": {
                "id": "abab3c43-be1f-42dd-802f-96ad54a67c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d458951-3bd4-4781-b241-967af7f3d839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f32cdfa2-354b-48da-9afc-161ef3f1fc49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d458951-3bd4-4781-b241-967af7f3d839",
                    "LayerId": "bb951662-94e5-42c1-b7cb-986073723465"
                }
            ]
        },
        {
            "id": "6198fcdb-4508-4d63-a5fe-14ada14d3e64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ef336b2-21e5-40b9-a70a-a68a95c4a1fd",
            "compositeImage": {
                "id": "d8967f81-6f8c-4cb9-b694-27a11898032a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6198fcdb-4508-4d63-a5fe-14ada14d3e64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c70b5ad-f8ec-49ea-99f8-468480941024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6198fcdb-4508-4d63-a5fe-14ada14d3e64",
                    "LayerId": "bb951662-94e5-42c1-b7cb-986073723465"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bb951662-94e5-42c1-b7cb-986073723465",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ef336b2-21e5-40b9-a70a-a68a95c4a1fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}