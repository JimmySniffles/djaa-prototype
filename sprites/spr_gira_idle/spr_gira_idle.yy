{
    "id": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gira_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 27,
    "bbox_right": 35,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c08b5525-7a2f-442d-975d-e58264537bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
            "compositeImage": {
                "id": "3372a15c-be4b-4238-abda-4aa53eac2f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08b5525-7a2f-442d-975d-e58264537bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdef2b0a-4fc7-4657-9fea-29604c5356a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08b5525-7a2f-442d-975d-e58264537bfa",
                    "LayerId": "b923e0b7-46a3-4f66-8a1b-59064dfc90c4"
                }
            ]
        },
        {
            "id": "9d12796d-24ea-45dd-8e34-c56e5deab11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
            "compositeImage": {
                "id": "f5dc9ccb-c975-4b32-999d-37fc0d5c5efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d12796d-24ea-45dd-8e34-c56e5deab11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a63001d-b382-47cc-9451-79df7ad440ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d12796d-24ea-45dd-8e34-c56e5deab11d",
                    "LayerId": "b923e0b7-46a3-4f66-8a1b-59064dfc90c4"
                }
            ]
        },
        {
            "id": "2812e077-bd51-48c5-bc7a-956e71c87218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
            "compositeImage": {
                "id": "a5c893e3-824f-4516-9855-4db4870c9644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2812e077-bd51-48c5-bc7a-956e71c87218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e59c53-c0cd-4710-a135-8f7e2d293f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2812e077-bd51-48c5-bc7a-956e71c87218",
                    "LayerId": "b923e0b7-46a3-4f66-8a1b-59064dfc90c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b923e0b7-46a3-4f66-8a1b-59064dfc90c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e0fe238-7cb3-4afa-aada-79c4a416627a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}