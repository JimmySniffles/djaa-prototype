{
    "id": "2c9ec795-9955-43c2-84a2-5ecf179d000e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_maroochy_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bef3385d-64e0-4346-a4fb-4557c50f2b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c9ec795-9955-43c2-84a2-5ecf179d000e",
            "compositeImage": {
                "id": "e0475b96-415a-4723-b5e4-5f1cb171d2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef3385d-64e0-4346-a4fb-4557c50f2b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a9e9be4-5e55-49a0-9da6-53bf84edd1b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef3385d-64e0-4346-a4fb-4557c50f2b93",
                    "LayerId": "7a4366e6-3054-4ab0-be54-02f37a5e98c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7a4366e6-3054-4ab0-be54-02f37a5e98c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c9ec795-9955-43c2-84a2-5ecf179d000e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}