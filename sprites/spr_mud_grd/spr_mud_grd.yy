{
    "id": "ac0e4a8e-0206-4e85-81be-e8e2357e4af4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mud_grd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ebbf04b-bdff-4ffc-990f-b2daff317b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac0e4a8e-0206-4e85-81be-e8e2357e4af4",
            "compositeImage": {
                "id": "01abd34a-7f83-4207-8f14-1a4b24632244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebbf04b-bdff-4ffc-990f-b2daff317b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721a64c8-c6fe-44d2-90f0-58b0dfb85684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebbf04b-bdff-4ffc-990f-b2daff317b5c",
                    "LayerId": "e7c687e4-8a55-4235-986c-abea955d4c93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "e7c687e4-8a55-4235-986c-abea955d4c93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac0e4a8e-0206-4e85-81be-e8e2357e4af4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}