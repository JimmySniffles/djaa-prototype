{
    "id": "a6bf4bd1-e082-4744-8a76-7a52fc002191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_oneway_platform_col",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef6aab2b-3d18-405f-a755-fea81712f7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6bf4bd1-e082-4744-8a76-7a52fc002191",
            "compositeImage": {
                "id": "9e51115a-aa1a-4ebb-9947-e10c49e63f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6aab2b-3d18-405f-a755-fea81712f7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f60f2b-4294-4186-8bb1-969b6fbcf664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6aab2b-3d18-405f-a755-fea81712f7d0",
                    "LayerId": "48820b46-ba17-41ce-b7c7-ac64fa00a782"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48820b46-ba17-41ce-b7c7-ac64fa00a782",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6bf4bd1-e082-4744-8a76-7a52fc002191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}