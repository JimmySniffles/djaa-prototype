{
    "id": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_spr_na_g",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93389a36-0515-4312-8d77-f341377ee2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "c168c1d2-82e6-431d-8313-6518ce395424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93389a36-0515-4312-8d77-f341377ee2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8b0db42-5284-428e-910c-f985da76464d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93389a36-0515-4312-8d77-f341377ee2e4",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "6a272a00-4fa9-4e3e-8007-9821eaadbf1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93389a36-0515-4312-8d77-f341377ee2e4",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        },
        {
            "id": "941e8988-bc24-4a4d-ad55-64029401f686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "316f2fd7-7fcc-4bf1-bab6-17980c1c83c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "941e8988-bc24-4a4d-ad55-64029401f686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "628f5b74-3ea4-40ce-823e-9b3f1d2d0006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "941e8988-bc24-4a4d-ad55-64029401f686",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "e39c79d6-2b1a-4b2c-b014-92b4645fcfb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "941e8988-bc24-4a4d-ad55-64029401f686",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        },
        {
            "id": "b11d765d-734a-497a-8c37-be46b46f6fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "c28d5b85-ac7c-47fa-a9a1-7ec2c96c556d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11d765d-734a-497a-8c37-be46b46f6fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fd303e-c850-41b4-8c70-278771b20e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11d765d-734a-497a-8c37-be46b46f6fbd",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "ec4ce763-fd32-4524-8aa7-34354fea47c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11d765d-734a-497a-8c37-be46b46f6fbd",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        },
        {
            "id": "bbc59c71-6155-49ba-82d2-c2a564a50e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "33726d7f-ef20-4a4e-8c9a-6f9c2393c0d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc59c71-6155-49ba-82d2-c2a564a50e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff4c415-b412-446b-92af-5bbffedaab45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc59c71-6155-49ba-82d2-c2a564a50e09",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "4b2f9e21-3506-40e1-a69e-604edf5e9c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc59c71-6155-49ba-82d2-c2a564a50e09",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        },
        {
            "id": "9ed7d134-92fd-4f30-89e7-9c2e38925deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "c52e9518-d004-498b-998d-9fb7ea94fded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ed7d134-92fd-4f30-89e7-9c2e38925deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a41cfe-a139-446b-b828-e5d11bd26d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed7d134-92fd-4f30-89e7-9c2e38925deb",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "69ae4fc2-419f-4249-86a2-f5b6259ac7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed7d134-92fd-4f30-89e7-9c2e38925deb",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        },
        {
            "id": "2c36879a-335e-45b2-af1f-56cb80612d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "compositeImage": {
                "id": "13d433d4-adad-4c6b-948d-a188dcdd4879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c36879a-335e-45b2-af1f-56cb80612d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7a95ae9-d06e-41e6-9241-30ae01999253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c36879a-335e-45b2-af1f-56cb80612d49",
                    "LayerId": "c7ffbe6f-6d93-4361-9983-beab4973b250"
                },
                {
                    "id": "4ba8332f-d756-4fe4-abd5-5056449db02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c36879a-335e-45b2-af1f-56cb80612d49",
                    "LayerId": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0d0043c8-14ea-48ad-8924-4fe2e8b0ede5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c7ffbe6f-6d93-4361-9983-beab4973b250",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e05e067-1a45-4fa8-a6a7-4860b1d70491",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}