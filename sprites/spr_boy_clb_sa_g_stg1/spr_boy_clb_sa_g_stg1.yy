{
    "id": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_clb_sa_g_stg1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e915ed5-9cd9-4a80-a003-fc7788b61b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "88bcf813-d094-4500-924e-8be9eb862a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e915ed5-9cd9-4a80-a003-fc7788b61b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d259865d-b9f3-463c-b2d0-0a5eba108f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e915ed5-9cd9-4a80-a003-fc7788b61b47",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "8e51fb8a-a01f-4bc0-837c-2c545b59101b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e915ed5-9cd9-4a80-a003-fc7788b61b47",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        },
        {
            "id": "43e9bfc7-8362-4a83-b201-cd754089f4df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "9f20938a-9671-4caf-af62-537a0a407ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e9bfc7-8362-4a83-b201-cd754089f4df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69abb361-ce13-42e0-987c-7c554d67c76e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e9bfc7-8362-4a83-b201-cd754089f4df",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "a1580397-9586-49b0-b963-0817bc17a32e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e9bfc7-8362-4a83-b201-cd754089f4df",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        },
        {
            "id": "48b3f4c3-b433-49a5-9cbf-d31e756f0030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "f9abd3bc-6a1b-445b-8a64-e3d2fbc3e9d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b3f4c3-b433-49a5-9cbf-d31e756f0030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ba3d0d-bd7f-4dae-b036-359477b187d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b3f4c3-b433-49a5-9cbf-d31e756f0030",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "cc1279dd-506c-4cd5-8872-08d299cf6205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b3f4c3-b433-49a5-9cbf-d31e756f0030",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        },
        {
            "id": "c053b976-6c3d-4cb5-b98a-7257c2edf118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "273eb1a9-94e3-4496-b183-4ffa0ea8bd9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c053b976-6c3d-4cb5-b98a-7257c2edf118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea62ab3e-f522-4c4e-b4b1-b9611a69f82f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c053b976-6c3d-4cb5-b98a-7257c2edf118",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "6ede4c6c-8bb3-44f4-9396-8bce71ad16c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c053b976-6c3d-4cb5-b98a-7257c2edf118",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        },
        {
            "id": "cd27dfde-9e1b-415e-b622-ba627801ad59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "2f57119c-927a-4743-acfa-bc6caf8a8fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd27dfde-9e1b-415e-b622-ba627801ad59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b591eb-d0ec-416d-9d9b-86ead95fcfb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd27dfde-9e1b-415e-b622-ba627801ad59",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "13551334-6ebd-4a35-bbc7-2537280118d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd27dfde-9e1b-415e-b622-ba627801ad59",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        },
        {
            "id": "8a049e31-ed6c-45fb-9238-31776d1ff34b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "compositeImage": {
                "id": "79f919e3-5e32-4339-b155-73faad5277bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a049e31-ed6c-45fb-9238-31776d1ff34b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d9f06d-9bea-4d4d-92d8-40833628a397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a049e31-ed6c-45fb-9238-31776d1ff34b",
                    "LayerId": "52cf8efc-633a-4231-bd92-1f1b8b7456ef"
                },
                {
                    "id": "bb0b75ec-768c-4529-a4c0-360776885a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a049e31-ed6c-45fb-9238-31776d1ff34b",
                    "LayerId": "85a5983d-312f-4d8e-a54a-5d847a23ac31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "52cf8efc-633a-4231-bd92-1f1b8b7456ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "85a5983d-312f-4d8e-a54a-5d847a23ac31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "174ba7f2-73cf-4d77-b8d2-574b91687aa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}