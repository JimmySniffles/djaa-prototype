{
    "id": "abf7beec-3862-4dbc-9258-002073c746a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gkang_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "870c4dc0-1e30-493b-8100-1542e1ace94b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abf7beec-3862-4dbc-9258-002073c746a8",
            "compositeImage": {
                "id": "bc15e25d-e979-4a0e-898b-fd2ec7669226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870c4dc0-1e30-493b-8100-1542e1ace94b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b4862a4-d657-4d47-80ba-07cd16b4cff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870c4dc0-1e30-493b-8100-1542e1ace94b",
                    "LayerId": "97d8d8ac-8770-4e60-88da-42a7c00a287a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "97d8d8ac-8770-4e60-88da-42a7c00a287a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf7beec-3862-4dbc-9258-002073c746a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}