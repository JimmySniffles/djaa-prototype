{
    "id": "34d627ff-32f1-428f-bb94-0b25f38641f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c1da702-7468-4f36-88c2-295bd61ba0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34d627ff-32f1-428f-bb94-0b25f38641f4",
            "compositeImage": {
                "id": "56fb24f4-5442-4171-906b-2665e8f526ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1da702-7468-4f36-88c2-295bd61ba0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b7ff4eb-641e-433f-b629-6bcc564fff27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1da702-7468-4f36-88c2-295bd61ba0b8",
                    "LayerId": "53219225-ab86-4ede-b2ec-c6503bbb06ce"
                }
            ]
        },
        {
            "id": "33c6fc81-9e0a-410a-be4a-4211d9762160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34d627ff-32f1-428f-bb94-0b25f38641f4",
            "compositeImage": {
                "id": "26051d4b-742d-4f39-9011-e7591deac751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c6fc81-9e0a-410a-be4a-4211d9762160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d540a9d4-43c4-47d3-a5c0-7bc7dadb0522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c6fc81-9e0a-410a-be4a-4211d9762160",
                    "LayerId": "53219225-ab86-4ede-b2ec-c6503bbb06ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "53219225-ab86-4ede-b2ec-c6503bbb06ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34d627ff-32f1-428f-bb94-0b25f38641f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}