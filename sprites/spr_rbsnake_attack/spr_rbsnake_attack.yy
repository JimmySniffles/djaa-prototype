{
    "id": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rbsnake_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a9ad3fe-bf7c-44b4-b192-34cab8b47895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "3ca55a7d-64c9-4f44-a36f-a923b33939ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9ad3fe-bf7c-44b4-b192-34cab8b47895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb46c3af-4a6b-42d6-bb4b-a361431cc0d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9ad3fe-bf7c-44b4-b192-34cab8b47895",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "f33e8de0-8818-47aa-9ebf-9be9db042882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "0e48c576-0d37-4ada-8f51-b2d2333b048c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f33e8de0-8818-47aa-9ebf-9be9db042882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a93f241-a270-4675-8f09-823311119f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f33e8de0-8818-47aa-9ebf-9be9db042882",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "dfbd543d-58c9-4d1b-ab81-bda8b92a63aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "707c44a9-b075-4fb3-b64b-74ef469351d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbd543d-58c9-4d1b-ab81-bda8b92a63aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502f9494-e356-4c21-a3ec-9e63c07622a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbd543d-58c9-4d1b-ab81-bda8b92a63aa",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "e302815a-a6bf-4e3a-aa37-5d1b096385e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "5f12187c-b239-4786-8b9e-6e6f8186da81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e302815a-a6bf-4e3a-aa37-5d1b096385e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6302b236-c61f-493f-b0d1-59428258b844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e302815a-a6bf-4e3a-aa37-5d1b096385e1",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "31b03adc-ed67-462d-8858-346f0806a763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "11eb58f3-64c0-487b-a64f-c72a03c9a6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b03adc-ed67-462d-8858-346f0806a763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8626679f-1f85-4704-b088-87d1ea0c24cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b03adc-ed67-462d-8858-346f0806a763",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "2f08fd8b-1151-4087-83ac-e8cff870fdf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "4ca80657-2037-423b-b74d-9de9e25de491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f08fd8b-1151-4087-83ac-e8cff870fdf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c367e63-c327-4fbe-bb23-373fb9520f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f08fd8b-1151-4087-83ac-e8cff870fdf4",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "5c954213-6392-4326-bf15-b5fd745f5474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "c4431def-7f4b-4466-841e-3e7d236b2ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c954213-6392-4326-bf15-b5fd745f5474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880b1fc3-c41c-4baa-821d-6a658f787c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c954213-6392-4326-bf15-b5fd745f5474",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        },
        {
            "id": "e8dcf503-ab69-48d3-a4e4-d3063f35c9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "compositeImage": {
                "id": "790cf45a-5141-42ae-aec1-8f1fd957e70f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8dcf503-ab69-48d3-a4e4-d3063f35c9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aff7352-ebef-46f9-bfc5-9c252f2429dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8dcf503-ab69-48d3-a4e4-d3063f35c9f7",
                    "LayerId": "764da541-37a2-4660-a9fa-7767eab19cd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "764da541-37a2-4660-a9fa-7767eab19cd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f93fed3-067c-4acf-9541-fc90679c6ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}