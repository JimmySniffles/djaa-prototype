{
    "id": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bsnake_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d43e8878-4a69-4598-af50-6b19b2260541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "0d85435c-25b5-4c06-ab28-93604ac76815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43e8878-4a69-4598-af50-6b19b2260541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a2c32a5-9b87-42c9-874f-6addd6541166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43e8878-4a69-4598-af50-6b19b2260541",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "9590bc28-d44f-4a17-9fae-95eab83ad5fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "6fe7cae4-1840-40cd-83b7-e89277b3ddc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9590bc28-d44f-4a17-9fae-95eab83ad5fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e41564a-7397-43e2-8816-6e2661e3482d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9590bc28-d44f-4a17-9fae-95eab83ad5fd",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "761a851f-4c28-466f-8b9f-890b84c438e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "550f5f3e-bfab-4d4b-81e5-b85875a822d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "761a851f-4c28-466f-8b9f-890b84c438e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d11b953-908a-431b-abf9-2e6be80b1da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "761a851f-4c28-466f-8b9f-890b84c438e5",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "715ee5ba-7778-4724-89b4-92a5d521b492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "26dc128b-6f89-4055-8c3b-74c3fc71f78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "715ee5ba-7778-4724-89b4-92a5d521b492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b2412f-d7ae-4732-ace9-1d30f55d8a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "715ee5ba-7778-4724-89b4-92a5d521b492",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "f5899f20-44e9-475a-b115-7c08890982de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "4d632555-85c5-4cdc-9bef-250807d74894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5899f20-44e9-475a-b115-7c08890982de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a138d66-4461-42b1-b398-ad769b2f8807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5899f20-44e9-475a-b115-7c08890982de",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "fde21abe-bd34-4e2d-9a3a-9c0ae008b26f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "ff4361f7-13af-43e1-bb2e-41286b5e7c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde21abe-bd34-4e2d-9a3a-9c0ae008b26f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fccdcebe-afa8-41fa-997e-10e8ceb1effc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde21abe-bd34-4e2d-9a3a-9c0ae008b26f",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "d37824f0-1360-477b-a67f-6c3414c79b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "c71149ab-beee-4d2c-9c04-e102c013c907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d37824f0-1360-477b-a67f-6c3414c79b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa698307-f21a-42ba-adc4-fea9dd39d83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d37824f0-1360-477b-a67f-6c3414c79b64",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        },
        {
            "id": "d41611ea-e920-4a43-bd03-5e325057a429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "compositeImage": {
                "id": "a6d100ff-516d-4160-b646-c77fd685643b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41611ea-e920-4a43-bd03-5e325057a429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d436617-a4ed-48a0-afac-957171cf2cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41611ea-e920-4a43-bd03-5e325057a429",
                    "LayerId": "dacfdcf8-c508-4807-9212-f2ffbe21e744"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "dacfdcf8-c508-4807-9212-f2ffbe21e744",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3768b2b4-80e1-47b6-afc0-9d56b812f7d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}