{
    "id": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fceb60f9-1b92-404d-94a0-d1b62ab77643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
            "compositeImage": {
                "id": "0fe87a48-1220-4dbd-85d3-07c56c4cdd2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fceb60f9-1b92-404d-94a0-d1b62ab77643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "420251da-8393-4c19-b269-e8a4123608b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceb60f9-1b92-404d-94a0-d1b62ab77643",
                    "LayerId": "608505ae-68be-4ada-a2c1-c50b75490262"
                }
            ]
        },
        {
            "id": "994cc38b-d639-4f16-9bc8-67e35178754c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
            "compositeImage": {
                "id": "d12cef64-09e1-4de3-beff-7ff32c5db078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994cc38b-d639-4f16-9bc8-67e35178754c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94cf9a28-8ff9-409b-9fbe-30045e23c38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994cc38b-d639-4f16-9bc8-67e35178754c",
                    "LayerId": "608505ae-68be-4ada-a2c1-c50b75490262"
                }
            ]
        },
        {
            "id": "0927a88c-4665-47ff-8c55-53fe0fdd5e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
            "compositeImage": {
                "id": "d884eca5-b406-49fe-9c42-62cd653a26bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0927a88c-4665-47ff-8c55-53fe0fdd5e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54238dd2-13b8-4a04-b261-cab7b908a60c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0927a88c-4665-47ff-8c55-53fe0fdd5e7a",
                    "LayerId": "608505ae-68be-4ada-a2c1-c50b75490262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "608505ae-68be-4ada-a2c1-c50b75490262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ad80cd1-9fe9-4f82-a238-58ee34677ed4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}