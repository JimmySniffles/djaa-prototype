{
    "id": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkang_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37827610-62a0-4a13-af52-67f77d9d869c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "858286f6-ab1f-468c-be03-bd05809bc254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37827610-62a0-4a13-af52-67f77d9d869c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47747a8a-3f15-46ee-b32e-3143f4eaacd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37827610-62a0-4a13-af52-67f77d9d869c",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "818f50cd-e09f-43a3-a885-3c0724a0510a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "0f97f31b-4afc-4167-b554-fd5516aa1885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818f50cd-e09f-43a3-a885-3c0724a0510a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7af49988-3249-4176-8f62-03301d068bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818f50cd-e09f-43a3-a885-3c0724a0510a",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "b9543948-fd13-482e-9ead-09537ba8663d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "9b34a44c-cc30-4e75-9cdf-7e886050de6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9543948-fd13-482e-9ead-09537ba8663d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02502bd0-e6f8-4228-a70d-c9a2c4a77cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9543948-fd13-482e-9ead-09537ba8663d",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "0c7b8dd9-3e93-458e-93af-aa3fee9d7b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "b913b146-2c1e-445d-af33-b9a67f941c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c7b8dd9-3e93-458e-93af-aa3fee9d7b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa31eb5-a80a-457b-b607-b213b430a1eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c7b8dd9-3e93-458e-93af-aa3fee9d7b64",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "22825dca-7757-43b0-9ae9-ac00fe5bdeb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "b646bef7-6ff6-4532-b4e9-871defb56a83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22825dca-7757-43b0-9ae9-ac00fe5bdeb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5fd932-9d65-4c14-a49d-162980c594e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22825dca-7757-43b0-9ae9-ac00fe5bdeb4",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "7f450f21-2d63-41e0-b452-c9983886a2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "8fc069ce-ce75-4715-888e-0ba94618f631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f450f21-2d63-41e0-b452-c9983886a2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2702927-32f7-4fa3-a6ef-2d813dccb9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f450f21-2d63-41e0-b452-c9983886a2d8",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "7f24bd9d-7fd1-4fc9-8f63-1481bd191c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "9e9bf3f6-d441-4155-95e6-e05648009f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f24bd9d-7fd1-4fc9-8f63-1481bd191c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7330a7d-0425-47ba-ac42-b7765beea527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f24bd9d-7fd1-4fc9-8f63-1481bd191c30",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "29c653b6-075e-44c3-bc12-16831126fa0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "da2c5e02-57ab-4de5-b36d-783f9e93fa05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c653b6-075e-44c3-bc12-16831126fa0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83736fe0-e438-4cbd-be65-b5cddbd9f88f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c653b6-075e-44c3-bc12-16831126fa0a",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        },
        {
            "id": "e2d621c8-ded2-4e8c-a28f-397cb2e598f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "compositeImage": {
                "id": "b96b774a-4dee-4e06-a6c0-b3765b41ab14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d621c8-ded2-4e8c-a28f-397cb2e598f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9294f09d-47cc-451c-8e01-90952209159f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d621c8-ded2-4e8c-a28f-397cb2e598f2",
                    "LayerId": "fc2655ec-c853-44a7-b290-3fb2bb723e6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "fc2655ec-c853-44a7-b290-3fb2bb723e6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9dd19b3-7c8c-4709-9de4-0730ab2a8513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}