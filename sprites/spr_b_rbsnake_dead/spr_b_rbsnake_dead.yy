{
    "id": "952b43ba-52ac-463a-8b25-8c98df0c598e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_rbsnake_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae747840-2c5d-42c3-95c5-5e11623b4aa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "aea0e653-d81b-4489-b064-fa3f41390edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae747840-2c5d-42c3-95c5-5e11623b4aa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca376d67-f08f-4e9d-b94e-d44630c9eca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae747840-2c5d-42c3-95c5-5e11623b4aa0",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "4eeda415-5aa6-4298-9694-36bfb075a357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "3e69cf81-5713-4436-8aee-68e1b26722c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eeda415-5aa6-4298-9694-36bfb075a357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0e44ca-4dcd-4f08-84b1-b82b39b96591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eeda415-5aa6-4298-9694-36bfb075a357",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "ff5cf912-8aee-45ee-a669-ab4e8ac6ee9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "aaa11ea0-fcb5-4944-9a22-0e93c1169033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5cf912-8aee-45ee-a669-ab4e8ac6ee9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c997199-9ba6-450f-ac9f-c4476af6cf6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5cf912-8aee-45ee-a669-ab4e8ac6ee9b",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "e1cc9263-806c-44b8-86c8-f348437de14f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "2d63d680-7ffa-48bf-9dad-75626ad8064d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1cc9263-806c-44b8-86c8-f348437de14f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd156a6c-3fc9-4239-aeca-da963533dcb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1cc9263-806c-44b8-86c8-f348437de14f",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "4f4532c3-39c6-4b0a-bc3d-bfbdb61b45e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "eaca0ee9-b0c2-4c89-a23e-04870d9df255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f4532c3-39c6-4b0a-bc3d-bfbdb61b45e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72aa47c-0c54-4940-ba55-2b0951f01e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f4532c3-39c6-4b0a-bc3d-bfbdb61b45e1",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "4e0c93b7-e2fc-4665-9aa1-48fb17cfc9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "2654daea-db49-4ed3-9542-c6d331192a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0c93b7-e2fc-4665-9aa1-48fb17cfc9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf8092f-d9f2-46a3-ac3e-e4a733ef81ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0c93b7-e2fc-4665-9aa1-48fb17cfc9cc",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "cea1e64f-f5a9-4b74-8db2-c213ccd94316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "36b9aa9f-8d9b-4a83-a0a1-a06e46052c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea1e64f-f5a9-4b74-8db2-c213ccd94316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9127500e-04b9-4e30-8f52-08c0232553cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea1e64f-f5a9-4b74-8db2-c213ccd94316",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "411c626a-7ab7-45a7-960d-e86e86c9f821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "aa3913ef-1cd7-471c-99f1-43be5b19c665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "411c626a-7ab7-45a7-960d-e86e86c9f821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5709d05e-684d-406c-9f1d-4efddbea5fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "411c626a-7ab7-45a7-960d-e86e86c9f821",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "9910307c-6626-4748-b512-6992cb3b3164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "d41764d4-2750-49fc-94ec-cfafa4b56962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9910307c-6626-4748-b512-6992cb3b3164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e13f8e7-aba2-4cff-a042-553a53a53876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9910307c-6626-4748-b512-6992cb3b3164",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "d266756b-d662-47f8-87bc-0f1626114adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "465aca43-75ad-4df8-9f8c-e166b9691037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d266756b-d662-47f8-87bc-0f1626114adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb787959-b5bf-46ec-984a-5284e080e7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d266756b-d662-47f8-87bc-0f1626114adf",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "8323984a-c1bf-4b8c-9f64-e9241a371a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "d96f22df-76d6-40a0-a2b1-2481ebbddb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8323984a-c1bf-4b8c-9f64-e9241a371a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1775079-be84-4ba3-aa28-e0bf051db843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8323984a-c1bf-4b8c-9f64-e9241a371a3a",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "796c782e-afd4-4ec3-8c3f-b3fe4f0b868f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "37ea2fff-3232-40ac-9da7-f0e3cd41943e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796c782e-afd4-4ec3-8c3f-b3fe4f0b868f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84d2cfa-6f1b-4f49-a255-3cd756d5d88d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796c782e-afd4-4ec3-8c3f-b3fe4f0b868f",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        },
        {
            "id": "84822274-96cb-427c-b680-b410ec9a2072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "compositeImage": {
                "id": "aeb11f21-7080-4d59-a38e-a1e486c711c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84822274-96cb-427c-b680-b410ec9a2072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82b1c54-8b90-4eac-a51d-e88984dabfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84822274-96cb-427c-b680-b410ec9a2072",
                    "LayerId": "9deb5bfb-1996-4cd3-9888-252de909470d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "9deb5bfb-1996-4cd3-9888-252de909470d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "952b43ba-52ac-463a-8b25-8c98df0c598e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}