{
    "id": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_na_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "565a9725-27f2-4d23-aff1-645539b98a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "ef5f3879-f8f2-451c-abe0-214988d3bdd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565a9725-27f2-4d23-aff1-645539b98a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78382306-1349-467b-948f-fdcb77d6488f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565a9725-27f2-4d23-aff1-645539b98a6a",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "05efca45-c381-4cfe-9a48-a33e00aab17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565a9725-27f2-4d23-aff1-645539b98a6a",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        },
        {
            "id": "87a6c6ca-a877-470b-bebf-58523fd1bc6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "2f1d5fbb-d002-4079-994d-253e8c601e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87a6c6ca-a877-470b-bebf-58523fd1bc6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bbf9c57-126a-454c-b8ea-79b4f62492d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87a6c6ca-a877-470b-bebf-58523fd1bc6a",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "95f70378-211b-4be5-9691-e97df59030b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87a6c6ca-a877-470b-bebf-58523fd1bc6a",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        },
        {
            "id": "3292b85e-2dc5-48f5-ad25-95e399133933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "eedc0530-31db-4e2b-8aa8-e3deab763e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3292b85e-2dc5-48f5-ad25-95e399133933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21615ba0-47c3-4941-8ff2-de9d44953be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3292b85e-2dc5-48f5-ad25-95e399133933",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "e123ff82-ea48-415b-9ed0-b4edf5fe9343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3292b85e-2dc5-48f5-ad25-95e399133933",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        },
        {
            "id": "14688c7d-ef35-4952-8f26-f3c42c4f53a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "395e1bdd-15dd-46c7-8a62-2756ec5aa27e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14688c7d-ef35-4952-8f26-f3c42c4f53a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1408f6c8-a247-49f9-a9d3-7a626e6c878e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14688c7d-ef35-4952-8f26-f3c42c4f53a1",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "59592293-3a26-442b-b43d-e63690a66842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14688c7d-ef35-4952-8f26-f3c42c4f53a1",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        },
        {
            "id": "72a89caf-3cd4-498b-8063-0e663b6f90cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "0df4d8d1-8fc3-425c-a699-d62c965e74c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a89caf-3cd4-498b-8063-0e663b6f90cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45be1ecf-ac21-4138-a15a-f4a0cec6f337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a89caf-3cd4-498b-8063-0e663b6f90cc",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "f65c2628-fe95-4dab-a18c-55b864b15979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a89caf-3cd4-498b-8063-0e663b6f90cc",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        },
        {
            "id": "115e18c0-3cb8-4803-9b65-16450f2b9a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "compositeImage": {
                "id": "a9f1f66c-268d-455e-a3de-5a2c96bdabee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115e18c0-3cb8-4803-9b65-16450f2b9a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9960617-6f86-4a4a-ab4c-2110282d806b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115e18c0-3cb8-4803-9b65-16450f2b9a21",
                    "LayerId": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774"
                },
                {
                    "id": "3487df6b-6d77-4cb1-9de5-ef53a346f5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115e18c0-3cb8-4803-9b65-16450f2b9a21",
                    "LayerId": "5c0a6ccf-2dd0-4848-b492-a1e627beda45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4d36e1a-1ce3-469f-92a6-e7f2ed494774",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5c0a6ccf-2dd0-4848-b492-a1e627beda45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceac3ea8-db82-4c37-bc42-a25f5ce53fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}