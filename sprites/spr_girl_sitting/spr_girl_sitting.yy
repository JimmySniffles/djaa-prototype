{
    "id": "989802e0-6019-4155-9369-be047a42da17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 20,
    "bbox_right": 45,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9ac4e29-796a-489f-bbd9-21a4459bdc6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "989802e0-6019-4155-9369-be047a42da17",
            "compositeImage": {
                "id": "a5dfc1e8-8dbf-487e-924d-dee13bfc6c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9ac4e29-796a-489f-bbd9-21a4459bdc6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2df8f99-c455-41f6-a0d1-e8c9c3e0887c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9ac4e29-796a-489f-bbd9-21a4459bdc6c",
                    "LayerId": "1da58cc3-676b-48e5-993f-d280b6627376"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1da58cc3-676b-48e5-993f-d280b6627376",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "989802e0-6019-4155-9369-be047a42da17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}