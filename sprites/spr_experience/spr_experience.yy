{
    "id": "49e7410d-62e1-405c-aa47-677901846a44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_experience",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "563914da-d723-4ea7-b4b2-4cf2f610b234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49e7410d-62e1-405c-aa47-677901846a44",
            "compositeImage": {
                "id": "60ed97a1-9253-4556-8297-316b8aee209e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563914da-d723-4ea7-b4b2-4cf2f610b234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08affd2-c620-48f9-a843-71eac15792a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563914da-d723-4ea7-b4b2-4cf2f610b234",
                    "LayerId": "8a434050-a75f-4325-ae90-5aaf8fa7a422"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "8a434050-a75f-4325-ae90-5aaf8fa7a422",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49e7410d-62e1-405c-aa47-677901846a44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}