{
    "id": "b868de15-eedd-4c97-843a-319e676f8cae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_slide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3fca4cc-4f34-4acb-a050-739b8f980b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b868de15-eedd-4c97-843a-319e676f8cae",
            "compositeImage": {
                "id": "d0f7827a-c608-46bd-9461-c7d5cc6a69e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3fca4cc-4f34-4acb-a050-739b8f980b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82535273-7389-4d6d-9338-37dc48b5c7bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3fca4cc-4f34-4acb-a050-739b8f980b19",
                    "LayerId": "8ecc62af-bf47-49a6-b651-2e1f67014ae9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ecc62af-bf47-49a6-b651-2e1f67014ae9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b868de15-eedd-4c97-843a-319e676f8cae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}