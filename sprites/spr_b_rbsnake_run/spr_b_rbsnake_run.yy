{
    "id": "dc740212-f053-4fb3-838e-1f1e2af3a799",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_b_rbsnake_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "413b9e5a-2da2-42c4-abd2-9d177270feaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "9d20fbee-0c31-44a8-8cff-c404bd5a4629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413b9e5a-2da2-42c4-abd2-9d177270feaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4381894d-f0ff-463f-9d79-08d1968ccefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413b9e5a-2da2-42c4-abd2-9d177270feaf",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "8c3a5e11-f112-4442-ae10-32866288b211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "36150fc3-57cd-485f-921e-2a186706b402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c3a5e11-f112-4442-ae10-32866288b211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2f6c485-aafa-4b3e-b8ea-5e709876cddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c3a5e11-f112-4442-ae10-32866288b211",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "1edd9f47-8103-4d9e-bc9d-1e09d6e0ba18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "e2766ac5-44d3-4fe6-bc50-cab1edf8d261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1edd9f47-8103-4d9e-bc9d-1e09d6e0ba18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b993d18-33c8-434a-a950-ce0bf5e9b030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1edd9f47-8103-4d9e-bc9d-1e09d6e0ba18",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "e133782b-6734-4cf4-93e5-2457dbce7941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "16a446fa-4550-4da5-910d-dc8b6572a303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e133782b-6734-4cf4-93e5-2457dbce7941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb513677-d114-4788-82f9-7140cdd1dd9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e133782b-6734-4cf4-93e5-2457dbce7941",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "74a62498-ce73-44d8-8aab-ba1520bdfa81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "9945299a-7604-4975-bcd9-b651a27eed80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a62498-ce73-44d8-8aab-ba1520bdfa81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10744d8e-894d-408e-8919-b511688adbd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a62498-ce73-44d8-8aab-ba1520bdfa81",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "35eff4d2-555b-48a8-88ab-be80f0b397f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "755d56f2-791e-4734-bed6-63674311d106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35eff4d2-555b-48a8-88ab-be80f0b397f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35ffd45-fcdc-49f5-9157-48a44eee9857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35eff4d2-555b-48a8-88ab-be80f0b397f4",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "3d4e0cc2-7ce6-4989-b56b-697c7fc40c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "ec5325cd-8146-4df5-bc82-9d630e6c8cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4e0cc2-7ce6-4989-b56b-697c7fc40c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e6cc9e-2dae-41f1-b345-f638f44831ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4e0cc2-7ce6-4989-b56b-697c7fc40c30",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "f4959288-71e2-436d-adf2-db4faed26562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "623798e7-9112-4a64-acf6-89908770dac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4959288-71e2-436d-adf2-db4faed26562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "488b8187-fd60-400a-8529-f9861d432f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4959288-71e2-436d-adf2-db4faed26562",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "93c1d104-bc99-4093-8502-50e2cfb7b169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "a677631e-ada0-4210-9c06-061757fb9b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c1d104-bc99-4093-8502-50e2cfb7b169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425f7cd7-3807-4c28-8c72-f50feedd7d58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c1d104-bc99-4093-8502-50e2cfb7b169",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "a04a41ec-3ac7-4a27-bed5-0df5c942cae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "c332eb83-a358-4dff-b430-8ad57fe365d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04a41ec-3ac7-4a27-bed5-0df5c942cae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0777a49-382e-4f57-97e3-4096d4b1606e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04a41ec-3ac7-4a27-bed5-0df5c942cae8",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "c86baef2-9add-44e7-9eae-929a8ed9f6ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "226e260d-973a-429c-ae11-7573c73dfb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86baef2-9add-44e7-9eae-929a8ed9f6ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e143ce5-9b46-4a01-b198-e33070dee873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86baef2-9add-44e7-9eae-929a8ed9f6ae",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "6a3c1779-88bb-4282-a44a-617ea8f3e98e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "253e3b26-fdce-4c28-a940-582c9e842b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3c1779-88bb-4282-a44a-617ea8f3e98e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bbc7ce-7913-4ddb-856d-8e39dee9805f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3c1779-88bb-4282-a44a-617ea8f3e98e",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "44a7b464-9db6-4078-9fb6-055cfc7bdd9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "9bc95440-2eca-42b1-86e1-7b2c20308349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a7b464-9db6-4078-9fb6-055cfc7bdd9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f15ce26-d254-4433-9f51-55dfe504dc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a7b464-9db6-4078-9fb6-055cfc7bdd9f",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "537bee76-2b43-4276-991d-b6b481fea226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "7b15c773-4458-43fb-bd2c-fb935dddcbff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537bee76-2b43-4276-991d-b6b481fea226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bc71c4-a881-4a5d-8d1b-634994d6bd72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537bee76-2b43-4276-991d-b6b481fea226",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "712139e0-1a23-4bd8-9c80-ff7d329eb9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "1254f3b5-819e-4a29-a6d8-559db3fbbc26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712139e0-1a23-4bd8-9c80-ff7d329eb9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5262084-980c-44c7-99d0-9512af938e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712139e0-1a23-4bd8-9c80-ff7d329eb9e9",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        },
        {
            "id": "27daed4f-6ad5-4934-99a8-6d488245e9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "compositeImage": {
                "id": "c10e9ca1-3fe9-4c07-a5fc-ca7e4774034f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27daed4f-6ad5-4934-99a8-6d488245e9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3f53335-92b0-4c16-b9a9-52e3537eadab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27daed4f-6ad5-4934-99a8-6d488245e9b3",
                    "LayerId": "efe92d3c-6a28-4977-bcf7-688fa866870a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "efe92d3c-6a28-4977-bcf7-688fa866870a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc740212-f053-4fb3-838e-1f1e2af3a799",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 43
}