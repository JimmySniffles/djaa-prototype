{
    "id": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 50,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d6e16e2-b1a5-4774-8cfd-a1ad5b1edd1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "d46b250e-7524-40c4-9c10-a7f7f68ea892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6e16e2-b1a5-4774-8cfd-a1ad5b1edd1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482c208f-df77-4170-960a-a81b3d1e420e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6e16e2-b1a5-4774-8cfd-a1ad5b1edd1a",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "8ff9dff9-dbfa-4984-aa0d-6cc1d17604f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "d6ad98ee-55b5-4744-8733-805925b8e351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff9dff9-dbfa-4984-aa0d-6cc1d17604f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d7ce43-f8ae-4efd-bfc1-1bc8addb25aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff9dff9-dbfa-4984-aa0d-6cc1d17604f9",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "42a96306-189e-4411-9493-386a97cdd73e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "9c7b6616-d27f-4992-86b3-f20d2b64a318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a96306-189e-4411-9493-386a97cdd73e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4259cd4e-b9e2-4f72-8842-c8ff0c7819a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a96306-189e-4411-9493-386a97cdd73e",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "028b5095-0fa3-4b4a-a739-c421b23235d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "5024441a-fbb3-4b66-972c-a697585d4ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028b5095-0fa3-4b4a-a739-c421b23235d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ba6ba0-43ad-4298-8841-55003695f49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028b5095-0fa3-4b4a-a739-c421b23235d3",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "5786200f-8989-41d0-9dfc-709bf7062a01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "36ecc3f2-62f5-41a1-bc3c-98f5545f4ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5786200f-8989-41d0-9dfc-709bf7062a01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c2ef30a-2987-404f-ab9a-812d6c882493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5786200f-8989-41d0-9dfc-709bf7062a01",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "246a285e-a7c2-461b-8c0c-727e9bdac44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "c3a3f697-f14d-4f21-80bb-c0cff3e56b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246a285e-a7c2-461b-8c0c-727e9bdac44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef826c87-3492-4bd3-9153-af8557092a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246a285e-a7c2-461b-8c0c-727e9bdac44b",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "6a906ca3-a9ed-4ee1-8e52-e7d6e4ea0dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "75dfa9ef-17d2-46b6-91c0-cdd3da1cf6e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a906ca3-a9ed-4ee1-8e52-e7d6e4ea0dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9e042c-5620-489c-bc76-d705d57884ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a906ca3-a9ed-4ee1-8e52-e7d6e4ea0dce",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        },
        {
            "id": "f312c040-c1e7-4372-9602-3d7f1c300c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "compositeImage": {
                "id": "060f0fa5-3175-4b1b-be9e-adcd8df2674c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f312c040-c1e7-4372-9602-3d7f1c300c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b313c6-dc8b-464b-9857-dd0e84117e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f312c040-c1e7-4372-9602-3d7f1c300c6c",
                    "LayerId": "f4211607-26fb-4248-a0d0-10a603a1e80d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f4211607-26fb-4248-a0d0-10a603a1e80d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beac2f03-0ac2-4308-bb24-85df0b5687c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}