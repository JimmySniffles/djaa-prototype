{
    "id": "046ac0de-f076-4676-945b-728b3aece4b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f325339-3ad4-4296-815e-acba7595ae8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046ac0de-f076-4676-945b-728b3aece4b1",
            "compositeImage": {
                "id": "991cfa7a-f54d-44d0-a83d-368ee5d9af4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f325339-3ad4-4296-815e-acba7595ae8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653a067f-d2fd-48e5-8ff0-ebed4ac5efc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f325339-3ad4-4296-815e-acba7595ae8f",
                    "LayerId": "bb318399-ee3d-4e0c-8274-da1621b03b3b"
                }
            ]
        },
        {
            "id": "659fa7bc-b272-42da-bca7-84d8e8febdac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046ac0de-f076-4676-945b-728b3aece4b1",
            "compositeImage": {
                "id": "e5650c18-e645-4773-a15c-f45b445db29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659fa7bc-b272-42da-bca7-84d8e8febdac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16547211-4155-4e38-b52b-287146138846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659fa7bc-b272-42da-bca7-84d8e8febdac",
                    "LayerId": "bb318399-ee3d-4e0c-8274-da1621b03b3b"
                }
            ]
        },
        {
            "id": "3e0a8f1c-7ff3-4602-88d1-0d533a00ebc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046ac0de-f076-4676-945b-728b3aece4b1",
            "compositeImage": {
                "id": "321a4204-3dfc-44f0-b587-2e2112c2110e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0a8f1c-7ff3-4602-88d1-0d533a00ebc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4ca23b-240e-4a1e-abc7-a1ff988bda16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0a8f1c-7ff3-4602-88d1-0d533a00ebc4",
                    "LayerId": "bb318399-ee3d-4e0c-8274-da1621b03b3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bb318399-ee3d-4e0c-8274-da1621b03b3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "046ac0de-f076-4676-945b-728b3aece4b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}