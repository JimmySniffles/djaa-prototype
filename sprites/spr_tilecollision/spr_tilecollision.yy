{
    "id": "0902f0d9-634e-4351-b9ac-bb76993ab891",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tilecollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 895,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e26afe47-809e-4d26-8ead-3161b1294044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0902f0d9-634e-4351-b9ac-bb76993ab891",
            "compositeImage": {
                "id": "9db80583-3abe-457c-8479-a3dd474cb036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26afe47-809e-4d26-8ead-3161b1294044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abba984f-0866-4a6d-a3e9-9dc118d5622a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26afe47-809e-4d26-8ead-3161b1294044",
                    "LayerId": "1cf5da26-d318-4eaf-a671-cc4c2721d76c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1cf5da26-d318-4eaf-a671-cc4c2721d76c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0902f0d9-634e-4351-b9ac-bb76993ab891",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 40,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 896,
    "xorig": 448,
    "yorig": 32
}