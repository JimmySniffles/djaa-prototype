{
    "id": "cd03934a-cf14-4f82-8bbe-20837396ac4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_killingboomerang_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d26e8aca-1398-4735-90b4-eccec113c8df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd03934a-cf14-4f82-8bbe-20837396ac4c",
            "compositeImage": {
                "id": "c809c60d-9796-48c4-90a4-6dc7cec18506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d26e8aca-1398-4735-90b4-eccec113c8df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205f7830-4f1e-4ddc-b420-7ae8617ac7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d26e8aca-1398-4735-90b4-eccec113c8df",
                    "LayerId": "7a4efa29-8039-4c0d-90e5-c6ee11fd189e"
                }
            ]
        },
        {
            "id": "ab461e54-b04e-4a58-9de8-a0137269e8dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd03934a-cf14-4f82-8bbe-20837396ac4c",
            "compositeImage": {
                "id": "c6cb9607-cc4a-49f9-a842-a9e6aa75c074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab461e54-b04e-4a58-9de8-a0137269e8dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d69e3f35-ee37-4e14-92a2-7ee5b9017781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab461e54-b04e-4a58-9de8-a0137269e8dd",
                    "LayerId": "7a4efa29-8039-4c0d-90e5-c6ee11fd189e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "7a4efa29-8039-4c0d-90e5-c6ee11fd189e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd03934a-cf14-4f82-8bbe-20837396ac4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}