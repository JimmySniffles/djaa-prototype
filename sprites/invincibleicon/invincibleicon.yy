{
    "id": "2772d625-2cad-4714-b87b-7de4f9672b2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "invincibleicon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 110,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "574f7dca-7df7-466f-8475-e4737078baa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2772d625-2cad-4714-b87b-7de4f9672b2d",
            "compositeImage": {
                "id": "39339fa7-1b68-4a84-9459-b5c7a03fe478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "574f7dca-7df7-466f-8475-e4737078baa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547ee621-5248-4295-a5fd-3baf332ac43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "574f7dca-7df7-466f-8475-e4737078baa1",
                    "LayerId": "fa401c8b-c86f-4296-bd77-bf85f7d7a479"
                }
            ]
        },
        {
            "id": "dd225fb6-fc50-4794-808d-c57066e8f7ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2772d625-2cad-4714-b87b-7de4f9672b2d",
            "compositeImage": {
                "id": "3cb3ff6f-54e2-4a1a-9a05-9488928a8226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd225fb6-fc50-4794-808d-c57066e8f7ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e36e062-a084-4177-b761-947410ab2bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd225fb6-fc50-4794-808d-c57066e8f7ad",
                    "LayerId": "fa401c8b-c86f-4296-bd77-bf85f7d7a479"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "fa401c8b-c86f-4296-bd77-bf85f7d7a479",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2772d625-2cad-4714-b87b-7de4f9672b2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 0,
    "yorig": 131
}