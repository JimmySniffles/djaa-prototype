{
    "id": "001519e8-a346-4d11-aad6-4d99521bc337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_babun_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 37,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ec8334b-056b-4d48-bcc4-c0389d779e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "001519e8-a346-4d11-aad6-4d99521bc337",
            "compositeImage": {
                "id": "135da8d0-5d27-4dca-b096-1fb1484dc46c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec8334b-056b-4d48-bcc4-c0389d779e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2081ae17-4464-4b88-ba8a-ea85925f728b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec8334b-056b-4d48-bcc4-c0389d779e56",
                    "LayerId": "59b9a4fe-cdb6-447a-921f-78f8b6104c7f"
                }
            ]
        },
        {
            "id": "76d4eb32-2079-45dc-b605-abfb8d5d6d29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "001519e8-a346-4d11-aad6-4d99521bc337",
            "compositeImage": {
                "id": "4891215b-e732-4429-a03e-1260747600db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d4eb32-2079-45dc-b605-abfb8d5d6d29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86cd96e-d32f-414a-85a3-1883b745cf96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d4eb32-2079-45dc-b605-abfb8d5d6d29",
                    "LayerId": "59b9a4fe-cdb6-447a-921f-78f8b6104c7f"
                }
            ]
        },
        {
            "id": "a233a013-da49-4e92-a222-ae6f003f8089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "001519e8-a346-4d11-aad6-4d99521bc337",
            "compositeImage": {
                "id": "9ea144dd-9ec6-4127-bfb3-9a76cf3fad8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a233a013-da49-4e92-a222-ae6f003f8089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011e7ff8-c4d6-4b7d-8d98-f788cf9200a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a233a013-da49-4e92-a222-ae6f003f8089",
                    "LayerId": "59b9a4fe-cdb6-447a-921f-78f8b6104c7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "59b9a4fe-cdb6-447a-921f-78f8b6104c7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "001519e8-a346-4d11-aad6-4d99521bc337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}