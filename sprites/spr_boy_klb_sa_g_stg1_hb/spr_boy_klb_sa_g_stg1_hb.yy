{
    "id": "f366115a-030f-41d9-b15f-806a4ffe9a02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_sa_g_stg1_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a290f1b-1fdc-417c-b57b-d2ac53e1d09f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "52409267-1e1d-482d-96df-32a8f69b8285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a290f1b-1fdc-417c-b57b-d2ac53e1d09f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d5f791-d047-4335-945c-069c9374292e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a290f1b-1fdc-417c-b57b-d2ac53e1d09f",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "d6dec91e-1570-449d-a5fd-9d37077d3239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a290f1b-1fdc-417c-b57b-d2ac53e1d09f",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        },
        {
            "id": "2dcff64b-da1f-4831-9e79-45de0e0b44f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "e858006c-0420-40c2-9db0-35267d5be5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dcff64b-da1f-4831-9e79-45de0e0b44f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e814c9c9-bccf-4b29-a9a3-f8cb41b60e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dcff64b-da1f-4831-9e79-45de0e0b44f3",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "798435f6-56ba-4807-a944-b8a8df223c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dcff64b-da1f-4831-9e79-45de0e0b44f3",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        },
        {
            "id": "9227746b-7255-41d1-9dd0-1a2f839b032f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "55fe2525-c2ea-4ac1-a025-2505d2bbb632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9227746b-7255-41d1-9dd0-1a2f839b032f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d890af3-e92a-43c7-bad1-3b65891a32b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9227746b-7255-41d1-9dd0-1a2f839b032f",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "e362355d-b1a8-4dca-bc28-48c3fd1e14d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9227746b-7255-41d1-9dd0-1a2f839b032f",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        },
        {
            "id": "39807df7-91dc-4969-8f43-50590dba26a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "46e78a1a-b95f-422c-9234-114b1cb1ca39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39807df7-91dc-4969-8f43-50590dba26a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c043e6fd-3183-4c6e-91d4-9ec972a8297d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39807df7-91dc-4969-8f43-50590dba26a0",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "b0bd3e76-a70f-40a6-b154-f403e45e7ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39807df7-91dc-4969-8f43-50590dba26a0",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        },
        {
            "id": "287cda09-ee60-48a8-af62-1f94ba949a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "3c5a4382-7330-42a7-a814-ffeaec92551f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287cda09-ee60-48a8-af62-1f94ba949a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6469bf52-9826-4883-a47a-389116113a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287cda09-ee60-48a8-af62-1f94ba949a40",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "6607469b-732e-48c4-b80b-bcd35f2af018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287cda09-ee60-48a8-af62-1f94ba949a40",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        },
        {
            "id": "bf2a6b1a-ae59-43a4-99b5-f98b796076cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "compositeImage": {
                "id": "0d67adaa-f2e2-492f-96ff-39088515f0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf2a6b1a-ae59-43a4-99b5-f98b796076cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123e47f2-3a3d-44b9-8363-1701d6c3798f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2a6b1a-ae59-43a4-99b5-f98b796076cc",
                    "LayerId": "e4a2cdab-c929-40fc-a713-8ecc6313e948"
                },
                {
                    "id": "30171f6c-5b10-4547-96ee-b73639f05687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2a6b1a-ae59-43a4-99b5-f98b796076cc",
                    "LayerId": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e4a2cdab-c929-40fc-a713-8ecc6313e948",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "cbf7d4a0-747e-4efc-a7c3-aae2fa41b3ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f366115a-030f-41d9-b15f-806a4ffe9a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}