{
    "id": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_stb_na_g",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95d2e1dd-2792-44bf-b066-2f112524bce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "03e0b793-84ef-4f31-9af7-46ef4a097c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d2e1dd-2792-44bf-b066-2f112524bce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c926bc7-1d9c-42f7-a249-19b07c20556f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d2e1dd-2792-44bf-b066-2f112524bce1",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "f9d01297-9ecd-4d05-a139-301d072b6d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d2e1dd-2792-44bf-b066-2f112524bce1",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        },
        {
            "id": "49f5d8fb-457e-4e21-8f01-eb43e088405d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "d13dd100-16f6-4a91-ace3-e9e73c5afe56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f5d8fb-457e-4e21-8f01-eb43e088405d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dea86ce-cb39-4124-b795-aa5f4b8282e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f5d8fb-457e-4e21-8f01-eb43e088405d",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "4f086f10-a7a7-4c30-97e9-2c7b51a0d8f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f5d8fb-457e-4e21-8f01-eb43e088405d",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        },
        {
            "id": "81e5347a-d710-4a16-94ce-df5599a2d88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "6ecf1559-86ef-45a1-b5b6-61c0327eec1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e5347a-d710-4a16-94ce-df5599a2d88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523416f5-ebeb-42ec-8136-10eaebeaf163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e5347a-d710-4a16-94ce-df5599a2d88b",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "3cc1ac73-d8ef-46a7-842b-8bd1d0bfb0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e5347a-d710-4a16-94ce-df5599a2d88b",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        },
        {
            "id": "14598629-6637-455d-8073-8d94282f196b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "de21c190-7070-4035-a204-72498878be29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14598629-6637-455d-8073-8d94282f196b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f5f61a-9e98-4f4e-87ba-f188255a1d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14598629-6637-455d-8073-8d94282f196b",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "aa593b25-9990-4590-928b-69edd1db710e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14598629-6637-455d-8073-8d94282f196b",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        },
        {
            "id": "21fc8534-23e5-49d1-91dc-8f72b85cb2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "1aa33d09-0d2a-421b-b11d-2167597e4277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fc8534-23e5-49d1-91dc-8f72b85cb2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a609fd0-9ee9-4a82-9816-79408cdc691b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fc8534-23e5-49d1-91dc-8f72b85cb2f1",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "ac15ef7b-46f9-4532-b74a-9dde8e7ee15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fc8534-23e5-49d1-91dc-8f72b85cb2f1",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        },
        {
            "id": "ecae1b81-134f-4ad3-808c-4b8d8c233fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "compositeImage": {
                "id": "f610a8c9-cdaf-4aba-acef-9b2c78ac0a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecae1b81-134f-4ad3-808c-4b8d8c233fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7619e227-4c11-4df4-bb0f-4d90d6291630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecae1b81-134f-4ad3-808c-4b8d8c233fc9",
                    "LayerId": "9f52a6df-9107-40b6-8a33-31d63b00cee3"
                },
                {
                    "id": "88e05ebf-bfb1-4c10-8b19-acb2b2da61ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecae1b81-134f-4ad3-808c-4b8d8c233fc9",
                    "LayerId": "7f705a57-edbc-4f09-9057-62c76789373b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f705a57-edbc-4f09-9057-62c76789373b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9f52a6df-9107-40b6-8a33-31d63b00cee3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd1399ca-9e02-4046-8c4a-aadc87bd9543",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}