{
    "id": "5fdea197-b98c-47a1-84b9-6c0fffcc856b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_slide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfec6ae9-ae77-48e5-8fbb-7f4ec185a784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdea197-b98c-47a1-84b9-6c0fffcc856b",
            "compositeImage": {
                "id": "26a7ba13-0775-4913-8fd4-bc12c9ff4ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfec6ae9-ae77-48e5-8fbb-7f4ec185a784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "398b2a8e-54b0-4066-b84d-2520ff9b4898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfec6ae9-ae77-48e5-8fbb-7f4ec185a784",
                    "LayerId": "049a5dd8-d526-4da2-bc7b-2f871a1c5932"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "049a5dd8-d526-4da2-bc7b-2f871a1c5932",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fdea197-b98c-47a1-84b9-6c0fffcc856b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}