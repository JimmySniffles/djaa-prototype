{
    "id": "e3ac2635-a76c-432f-ad79-a9a279043d21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24458e83-94be-4c4a-b8fe-b68e8203dba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
            "compositeImage": {
                "id": "c7992e67-83df-4058-90de-7fca25150391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24458e83-94be-4c4a-b8fe-b68e8203dba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2e136c-4b2b-4bcd-907a-e8d2b435c8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24458e83-94be-4c4a-b8fe-b68e8203dba9",
                    "LayerId": "7b2396f0-19cb-4d48-b8e5-336c6205d99d"
                }
            ]
        },
        {
            "id": "4dcdcfe9-2541-4f9d-89af-539087ed751d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
            "compositeImage": {
                "id": "f9f3bc61-1096-4176-bc8c-a4d58fecff4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dcdcfe9-2541-4f9d-89af-539087ed751d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de554fc-422e-4a52-b9c8-408bc1a4b509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dcdcfe9-2541-4f9d-89af-539087ed751d",
                    "LayerId": "7b2396f0-19cb-4d48-b8e5-336c6205d99d"
                }
            ]
        },
        {
            "id": "ee776e7a-9578-4f07-9ce8-67ead8db71ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
            "compositeImage": {
                "id": "a5348c8a-50f7-4816-9785-447399d8d87f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee776e7a-9578-4f07-9ce8-67ead8db71ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ad01ce-179a-4652-a053-dd5a950bd954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee776e7a-9578-4f07-9ce8-67ead8db71ac",
                    "LayerId": "7b2396f0-19cb-4d48-b8e5-336c6205d99d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b2396f0-19cb-4d48-b8e5-336c6205d99d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3ac2635-a76c-432f-ad79-a9a279043d21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}