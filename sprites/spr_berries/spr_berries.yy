{
    "id": "9fa29ca7-e7f6-40fa-bd41-4646eaefaf03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_berries",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "968998a7-625b-45ed-b699-18eda4afddc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fa29ca7-e7f6-40fa-bd41-4646eaefaf03",
            "compositeImage": {
                "id": "fe5f256d-c3be-46b7-9b6f-f47ad1dda2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968998a7-625b-45ed-b699-18eda4afddc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4015e326-1707-48e8-b214-1ee704156662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968998a7-625b-45ed-b699-18eda4afddc2",
                    "LayerId": "625a9e36-b4c4-463e-8d7d-8b89a8d7076c"
                }
            ]
        },
        {
            "id": "29f38fed-86f8-422a-a7e0-d8b364ec8e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fa29ca7-e7f6-40fa-bd41-4646eaefaf03",
            "compositeImage": {
                "id": "167cce05-872f-4dd2-9114-41ec6f91ed82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f38fed-86f8-422a-a7e0-d8b364ec8e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da1e6c1-4eda-4195-be5a-46ca74c5eaf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f38fed-86f8-422a-a7e0-d8b364ec8e7d",
                    "LayerId": "625a9e36-b4c4-463e-8d7d-8b89a8d7076c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "625a9e36-b4c4-463e-8d7d-8b89a8d7076c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fa29ca7-e7f6-40fa-bd41-4646eaefaf03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}