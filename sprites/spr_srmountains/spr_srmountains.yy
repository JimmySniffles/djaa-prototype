{
    "id": "4bf0a5d4-51f5-4c76-8d44-ebd0d7fc6ac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_srmountains",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 1199,
    "bbox_top": 198,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ee8e5f4-1b70-4f47-943b-6607b1380e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bf0a5d4-51f5-4c76-8d44-ebd0d7fc6ac0",
            "compositeImage": {
                "id": "3e42d00a-afec-407e-a394-f34a341964c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee8e5f4-1b70-4f47-943b-6607b1380e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1216affd-bbfa-421b-8546-eac34e92a7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee8e5f4-1b70-4f47-943b-6607b1380e2f",
                    "LayerId": "75a5e04b-9764-474e-b8cc-25c8d00bdb14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "75a5e04b-9764-474e-b8cc-25c8d00bdb14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bf0a5d4-51f5-4c76-8d44-ebd0d7fc6ac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1200,
    "xorig": 600,
    "yorig": 300
}