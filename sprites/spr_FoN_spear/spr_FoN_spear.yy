{
    "id": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_spear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c94c705-bad8-4c60-93e3-a7a37acf04fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "7e12f606-ca49-4667-9f18-207afacbd047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c94c705-bad8-4c60-93e3-a7a37acf04fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e2ff09-cd02-4bd1-85ee-eabae7c245ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c94c705-bad8-4c60-93e3-a7a37acf04fe",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "6330b550-d41d-450b-b665-d2107db41fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "34fe2374-9cbb-4d08-bdf6-1b49f2a4d2da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6330b550-d41d-450b-b665-d2107db41fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c0032b2-8f8b-48ca-9920-eb97861b61ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6330b550-d41d-450b-b665-d2107db41fca",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "57ec9af8-3035-4538-946c-d794d5ab20d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "1744e21f-fba0-423e-9d5a-1268280597c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57ec9af8-3035-4538-946c-d794d5ab20d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cf03cd6-12a6-4562-b7a5-a79eae72afce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57ec9af8-3035-4538-946c-d794d5ab20d7",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "d75ca5b1-357e-4781-8486-9ed0ea84fa8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "56154feb-4ae7-41e8-b70e-622b0c3adedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d75ca5b1-357e-4781-8486-9ed0ea84fa8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92397a23-7041-47a1-90e3-ea3530ce826e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d75ca5b1-357e-4781-8486-9ed0ea84fa8e",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "37da4326-ce6b-4337-bada-a9b1506c648c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "54abf9e0-6ceb-4ea7-9d3c-2d37cfb33f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37da4326-ce6b-4337-bada-a9b1506c648c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e58673d-e7b7-4938-9abc-720637cb00cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37da4326-ce6b-4337-bada-a9b1506c648c",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "680d48a8-1ef6-42fc-befd-a7b00d0ff60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "d15d9f77-6373-4ea3-aed8-654250fd488e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680d48a8-1ef6-42fc-befd-a7b00d0ff60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e5f2c5-e8d7-4c4a-9b51-ad364d2c6856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680d48a8-1ef6-42fc-befd-a7b00d0ff60a",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "5ef66db2-172f-4555-a14f-35cae7795e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "d38421e4-107b-47cb-85c6-a0aa2d835372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef66db2-172f-4555-a14f-35cae7795e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7f70e2-581b-4cda-b3e6-447d9e580cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef66db2-172f-4555-a14f-35cae7795e2d",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        },
        {
            "id": "7dfded30-0a6d-4e0c-852d-880d1fb4d78a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "compositeImage": {
                "id": "2a93319a-f8c7-4839-bae7-4d3c7f56234c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dfded30-0a6d-4e0c-852d-880d1fb4d78a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b0ebe1-ad74-425f-bae2-df2933f6c868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dfded30-0a6d-4e0c-852d-880d1fb4d78a",
                    "LayerId": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b365d6e-ef21-49b8-9ad0-2fa07f7815a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab49b759-cd2c-45a3-8ce9-8e9584fd719d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 32,
    "yorig": 32
}