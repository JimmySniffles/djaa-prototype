{
    "id": "19f0e246-f0b5-4ef3-96bb-eb1e463a56d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_club_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dc6fd33-2987-40a8-a2f4-f5e5da1e2427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19f0e246-f0b5-4ef3-96bb-eb1e463a56d7",
            "compositeImage": {
                "id": "b10aa3b9-2a8a-4873-9680-f4ff0124e9bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc6fd33-2987-40a8-a2f4-f5e5da1e2427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8259ec-e741-4522-9127-8b07c76e45d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc6fd33-2987-40a8-a2f4-f5e5da1e2427",
                    "LayerId": "08abda92-ecf2-4659-939d-d875d1f5589e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "08abda92-ecf2-4659-939d-d875d1f5589e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19f0e246-f0b5-4ef3-96bb-eb1e463a56d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}