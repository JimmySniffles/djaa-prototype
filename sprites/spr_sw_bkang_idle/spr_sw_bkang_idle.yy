{
    "id": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sw_bkang_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "873c1d7d-c45c-4a1b-8c82-70b5a5267219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "b7193ea8-5340-4b55-ad85-18f64e54be92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "873c1d7d-c45c-4a1b-8c82-70b5a5267219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da516d6-8a33-4bac-aada-faab1ea8f2cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "873c1d7d-c45c-4a1b-8c82-70b5a5267219",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "b5efe37c-48d0-435e-bbe4-d9829007ffb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "ca49397b-c2c3-4852-a1ab-25d4c425cba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5efe37c-48d0-435e-bbe4-d9829007ffb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8af732-ceee-4b1f-abfd-f2fac391dab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5efe37c-48d0-435e-bbe4-d9829007ffb9",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "60abb8e4-6598-49c8-a1c0-4f39694aacfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "b14128a9-7cc4-4179-9f65-e8af12cbefcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60abb8e4-6598-49c8-a1c0-4f39694aacfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3090410c-53c6-4066-8350-5b537ccb5e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60abb8e4-6598-49c8-a1c0-4f39694aacfb",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "e9b2b116-ca1d-4431-b84c-5dedabbacd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "2d60ff27-f001-4124-bcdb-bf8889564680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b2b116-ca1d-4431-b84c-5dedabbacd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c380db75-d1d4-4f1f-a5f1-1b53a7221bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b2b116-ca1d-4431-b84c-5dedabbacd6c",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "43239e69-f9ca-4120-b59f-605ef1b30739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "db847127-4d65-4781-ab94-3ceeeeea8745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43239e69-f9ca-4120-b59f-605ef1b30739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d2d7c47-7b40-4013-8a7e-8aad173d8223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43239e69-f9ca-4120-b59f-605ef1b30739",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "56e177d3-90ce-4d21-a52f-5048db8f844c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "c481c4c3-e7c4-4d66-ab8d-b31d3f31fc65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e177d3-90ce-4d21-a52f-5048db8f844c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a07ee73c-926e-45f6-9a8d-1a2a6b171904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e177d3-90ce-4d21-a52f-5048db8f844c",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "6adb1e5e-68fd-435a-8236-4d19afcc8109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "865e09f1-9518-4149-92d7-04c516eb052c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6adb1e5e-68fd-435a-8236-4d19afcc8109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770f640c-cc02-4425-af73-3e55e9ca350a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6adb1e5e-68fd-435a-8236-4d19afcc8109",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "8809996f-3cd1-4330-a3dd-2f2e765b0c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "63b63de7-2795-4078-b05e-da85c176f6b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8809996f-3cd1-4330-a3dd-2f2e765b0c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4b2640-42d4-46d7-bc24-71ffad3d4cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8809996f-3cd1-4330-a3dd-2f2e765b0c95",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        },
        {
            "id": "5cec53d7-7f8a-40d1-898e-0aa29e6af0fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "compositeImage": {
                "id": "dbbca449-e490-4b5d-983e-36da364b1ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cec53d7-7f8a-40d1-898e-0aa29e6af0fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab8fe05-64a2-48ca-af41-dbc9b8db2fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cec53d7-7f8a-40d1-898e-0aa29e6af0fe",
                    "LayerId": "01ed8924-8f9b-457f-a5cc-b0b429c984b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "01ed8924-8f9b-457f-a5cc-b0b429c984b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d50e46bb-77b8-4d88-a17d-c7c4831067aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}