{
    "id": "69b0d25f-3de4-421d-9888-b54b09489900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_srcavetiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bcf6707-bc5e-4239-ae36-965c736f13ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69b0d25f-3de4-421d-9888-b54b09489900",
            "compositeImage": {
                "id": "af71c97b-5c36-4d61-93f8-18bda652b639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bcf6707-bc5e-4239-ae36-965c736f13ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a56d026-17c0-499a-a879-d6b48e643d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bcf6707-bc5e-4239-ae36-965c736f13ae",
                    "LayerId": "dfc20c56-35cb-4720-a9b7-5536c11e813d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "dfc20c56-35cb-4720-a9b7-5536c11e813d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69b0d25f-3de4-421d-9888-b54b09489900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 192
}