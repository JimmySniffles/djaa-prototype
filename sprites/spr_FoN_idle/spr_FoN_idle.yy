{
    "id": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4782cab6-ca10-483f-9668-ec7f3dfc5a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
            "compositeImage": {
                "id": "6d063948-0834-40bc-b280-073e083f88d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4782cab6-ca10-483f-9668-ec7f3dfc5a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f928009-b78f-45ec-b6cf-379458ee84f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4782cab6-ca10-483f-9668-ec7f3dfc5a5f",
                    "LayerId": "101a3d32-ce66-4c24-804a-230d18ab8d18"
                }
            ]
        },
        {
            "id": "45c7ac95-88b6-4849-a2e6-26756aa9f81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
            "compositeImage": {
                "id": "7d7db42a-4fe2-40ef-8237-e5796d05d1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c7ac95-88b6-4849-a2e6-26756aa9f81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4588216-8899-413d-bdfb-aa28b5d1c258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c7ac95-88b6-4849-a2e6-26756aa9f81c",
                    "LayerId": "101a3d32-ce66-4c24-804a-230d18ab8d18"
                }
            ]
        },
        {
            "id": "5e9db487-e525-48ec-a41c-795ca65e2507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
            "compositeImage": {
                "id": "a0d07945-665a-41bb-880c-05e9d93f2470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e9db487-e525-48ec-a41c-795ca65e2507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd0f53ff-6219-4434-9d8d-bf7714004b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e9db487-e525-48ec-a41c-795ca65e2507",
                    "LayerId": "101a3d32-ce66-4c24-804a-230d18ab8d18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "101a3d32-ce66-4c24-804a-230d18ab8d18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c6ae5d8-3bf2-4e8d-b6d0-48e5b178c125",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}