{
    "id": "6833e37e-a3f1-460d-b4eb-5722682e65ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_srpeskytiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c33889c7-8f02-4592-b096-095d3c651f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6833e37e-a3f1-460d-b4eb-5722682e65ed",
            "compositeImage": {
                "id": "a5457e1b-afef-407d-a0d7-f2ad3a654ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33889c7-8f02-4592-b096-095d3c651f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9e6d7b-e5db-4ebf-bcd7-4c9685c12ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33889c7-8f02-4592-b096-095d3c651f05",
                    "LayerId": "0812a8d5-6fb2-4278-befb-a2e0a1dfa34b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0812a8d5-6fb2-4278-befb-a2e0a1dfa34b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6833e37e-a3f1-460d-b4eb-5722682e65ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}