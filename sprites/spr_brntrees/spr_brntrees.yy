{
    "id": "6d11ea41-62be-495f-ad18-235ce97d86df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brntrees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2559,
    "bbox_left": 21,
    "bbox_right": 1130,
    "bbox_top": 75,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e04dd05-d980-4f52-b500-04738cb6577c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d11ea41-62be-495f-ad18-235ce97d86df",
            "compositeImage": {
                "id": "ef2487b2-8d70-44f3-aa4e-a5dccaa179be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e04dd05-d980-4f52-b500-04738cb6577c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a5c29b1-a9ab-4fec-ad99-8debfb2ff30c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e04dd05-d980-4f52-b500-04738cb6577c",
                    "LayerId": "754ecced-322a-4174-8baa-3759d3a46d71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2560,
    "layers": [
        {
            "id": "754ecced-322a-4174-8baa-3759d3a46d71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d11ea41-62be-495f-ad18-235ce97d86df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1152,
    "xorig": 0,
    "yorig": 0
}