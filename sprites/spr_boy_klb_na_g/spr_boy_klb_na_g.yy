{
    "id": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_na_g",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c65759e-712e-4f5f-8a02-f5b1aa756ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "832994cd-a82f-4190-afa6-319d4a57b865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c65759e-712e-4f5f-8a02-f5b1aa756ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccebea1d-bb4f-4cbf-950d-58fcd8161e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c65759e-712e-4f5f-8a02-f5b1aa756ffa",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "fb8748eb-83ef-4f63-a5c5-2cae897228e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c65759e-712e-4f5f-8a02-f5b1aa756ffa",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        },
        {
            "id": "5e4a9d70-13e9-4fd5-a134-6db25eb6c07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "f317db12-88a6-46ed-ab90-ba47b01b1ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e4a9d70-13e9-4fd5-a134-6db25eb6c07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82357a39-5be4-4dd7-85b0-e24e060abbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e4a9d70-13e9-4fd5-a134-6db25eb6c07d",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "3ea27cfc-be0e-49fb-a05d-e87065c7ae98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e4a9d70-13e9-4fd5-a134-6db25eb6c07d",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        },
        {
            "id": "700d1be4-fb39-42de-b013-06ce8287994f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "748bebb6-4800-416f-970f-98e1c4018287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "700d1be4-fb39-42de-b013-06ce8287994f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1d1278-b594-4524-a571-0576661332b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700d1be4-fb39-42de-b013-06ce8287994f",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "0d66b36d-9c3a-4d72-bdfb-7d758a832d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700d1be4-fb39-42de-b013-06ce8287994f",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        },
        {
            "id": "732a3f8e-f09b-43fc-abdd-7dc0adcc0360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "ad674617-f4d7-461d-a506-d5739245179c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "732a3f8e-f09b-43fc-abdd-7dc0adcc0360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eadc64a0-cc65-4cd9-9359-e3c27db0279d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732a3f8e-f09b-43fc-abdd-7dc0adcc0360",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "35038cc0-c479-4dc0-87d6-25943a21355c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732a3f8e-f09b-43fc-abdd-7dc0adcc0360",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        },
        {
            "id": "796b21f9-2de2-4ff4-a737-f2009b106f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "ce3537dc-5974-444f-8232-cc257c347e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796b21f9-2de2-4ff4-a737-f2009b106f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e381b19-811a-49f3-8762-c0018f49ca31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796b21f9-2de2-4ff4-a737-f2009b106f4b",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "2a196bed-88bf-45c9-957c-36d032db59f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796b21f9-2de2-4ff4-a737-f2009b106f4b",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        },
        {
            "id": "88f8f908-512d-4dcb-bbbf-c5c4dd172825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "compositeImage": {
                "id": "b3ad8847-51ce-48db-a2e7-b923c3e79a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f8f908-512d-4dcb-bbbf-c5c4dd172825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ccedaa-141d-4886-bb60-375568979aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f8f908-512d-4dcb-bbbf-c5c4dd172825",
                    "LayerId": "4f63a7e7-af82-4b3a-9877-89e597a110a4"
                },
                {
                    "id": "f895d545-408b-4ab3-9d04-f6dce874c63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f8f908-512d-4dcb-bbbf-c5c4dd172825",
                    "LayerId": "447593f4-1982-48fe-bc6e-c2fdedb65f30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "447593f4-1982-48fe-bc6e-c2fdedb65f30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4f63a7e7-af82-4b3a-9877-89e597a110a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "496a023f-09bb-45ca-9db5-4535c56bd1d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}