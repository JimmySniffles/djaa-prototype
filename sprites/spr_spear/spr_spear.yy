{
    "id": "778b6b2b-b3eb-4a89-8648-e44b134d9a29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee63dcc1-41b9-4eed-bffb-a9e9bcd49e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778b6b2b-b3eb-4a89-8648-e44b134d9a29",
            "compositeImage": {
                "id": "59805096-f150-4430-9657-144dd8021f1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee63dcc1-41b9-4eed-bffb-a9e9bcd49e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a40980a-61c9-4229-9745-501609446c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee63dcc1-41b9-4eed-bffb-a9e9bcd49e31",
                    "LayerId": "bf30a941-51e4-4d84-ad0f-d42b02e137c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "bf30a941-51e4-4d84-ad0f-d42b02e137c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "778b6b2b-b3eb-4a89-8648-e44b134d9a29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 58,
    "yorig": 1
}