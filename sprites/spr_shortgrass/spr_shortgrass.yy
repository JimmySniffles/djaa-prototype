{
    "id": "dbe73862-f528-442d-848b-ce48acb84c69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shortgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 1022,
    "bbox_top": 57,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93eaa0c2-d536-4949-9280-77e371455e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbe73862-f528-442d-848b-ce48acb84c69",
            "compositeImage": {
                "id": "4e6069e3-b559-47ac-b8a1-e29d24a4d79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93eaa0c2-d536-4949-9280-77e371455e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd1b0d8f-73b2-4265-b6fd-0661bc7d90b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93eaa0c2-d536-4949-9280-77e371455e0a",
                    "LayerId": "8a132676-d33b-43be-965c-8988f9d3aa14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8a132676-d33b-43be-965c-8988f9d3aa14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbe73862-f528-442d-848b-ce48acb84c69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}