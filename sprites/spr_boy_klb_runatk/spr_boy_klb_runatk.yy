{
    "id": "f3f139a2-f75d-433e-b468-57be074c5090",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_klb_runatk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53d04a1d-449d-49fa-95e7-d52dad9e05b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "872ffcb5-4894-44d7-8be9-fec3880080ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d04a1d-449d-49fa-95e7-d52dad9e05b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0267e47-2c93-450c-b81c-a007c7b550a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d04a1d-449d-49fa-95e7-d52dad9e05b2",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "51380a40-93ec-47ff-b7b8-bcf9dde54c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d04a1d-449d-49fa-95e7-d52dad9e05b2",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        },
        {
            "id": "bc8fbf59-de17-4995-aa70-662209e6f2a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "e18349a4-0c61-4571-a91b-40ed79676ae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc8fbf59-de17-4995-aa70-662209e6f2a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "635c8c4a-5c7a-4876-b97d-77212c3cf269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8fbf59-de17-4995-aa70-662209e6f2a3",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "ae270b9b-99ac-4360-bfba-6a6fb0affbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8fbf59-de17-4995-aa70-662209e6f2a3",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        },
        {
            "id": "aa70f665-7190-47a9-ae1b-8ef1f8025520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "e4d3286f-fccf-464f-af1e-678c6f69a722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa70f665-7190-47a9-ae1b-8ef1f8025520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f28ac10-6e26-4362-9355-fcbd426ab38f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa70f665-7190-47a9-ae1b-8ef1f8025520",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "9714a8ad-1238-456c-a236-094e0bb8727b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa70f665-7190-47a9-ae1b-8ef1f8025520",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        },
        {
            "id": "5974ecdc-f294-4f1d-8d4a-db01148ffbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "a1229db5-f727-4e7c-b003-c5d4205ec3c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5974ecdc-f294-4f1d-8d4a-db01148ffbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2fad84-6b43-4fab-ba6c-43b088da7bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5974ecdc-f294-4f1d-8d4a-db01148ffbbb",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "569551ff-c28c-4a72-9b22-63410f492538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5974ecdc-f294-4f1d-8d4a-db01148ffbbb",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        },
        {
            "id": "346da0a7-2186-4150-9c61-6cdc42507077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "24b69bf9-d4bb-430a-8599-de2bf7a020cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346da0a7-2186-4150-9c61-6cdc42507077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13bb38ce-1e3b-4b49-8bea-5b3062bc52ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346da0a7-2186-4150-9c61-6cdc42507077",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "630ea1c9-30ec-40f8-ade5-752c7af332b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346da0a7-2186-4150-9c61-6cdc42507077",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        },
        {
            "id": "8335f015-f1e2-414f-8d47-bb938487cd6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "compositeImage": {
                "id": "c3e9ae20-8972-497a-abc2-9e963624f202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8335f015-f1e2-414f-8d47-bb938487cd6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aad78ab4-8ac7-46c8-b736-a2405b4a0bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8335f015-f1e2-414f-8d47-bb938487cd6e",
                    "LayerId": "61a304b9-bf79-442a-9b67-f9545e306733"
                },
                {
                    "id": "7770483e-8b73-482b-91a8-7dffecea8ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8335f015-f1e2-414f-8d47-bb938487cd6e",
                    "LayerId": "0b56ce0c-118c-4618-8b1b-05269423a75c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61a304b9-bf79-442a-9b67-f9545e306733",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0b56ce0c-118c-4618-8b1b-05269423a75c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f139a2-f75d-433e-b468-57be074c5090",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}