{
    "id": "0f7a1b03-2f1a-437d-8f3b-f646e0aaf4af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bkang_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4183000-06d5-4ef6-b205-b20ed0abb348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f7a1b03-2f1a-437d-8f3b-f646e0aaf4af",
            "compositeImage": {
                "id": "dd0a5474-e61d-4020-95ce-453734ed92d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4183000-06d5-4ef6-b205-b20ed0abb348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1ce9b6-cc4c-4eb2-aa2a-552b27cf071b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4183000-06d5-4ef6-b205-b20ed0abb348",
                    "LayerId": "ca349fac-567d-4e63-9071-5674f633c7fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "ca349fac-567d-4e63-9071-5674f633c7fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7a1b03-2f1a-437d-8f3b-f646e0aaf4af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 28
}