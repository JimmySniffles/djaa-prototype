{
    "id": "6d082a36-9a63-4310-8883-718175b8d138",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tsnake_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37448c25-8a38-4d58-8b89-ca226fe895d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d082a36-9a63-4310-8883-718175b8d138",
            "compositeImage": {
                "id": "d2677bd0-b6a7-4f40-ac56-eceb31f3fb91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37448c25-8a38-4d58-8b89-ca226fe895d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0843d45-a1cf-4552-82ef-861c22c9a16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37448c25-8a38-4d58-8b89-ca226fe895d4",
                    "LayerId": "98bbe9b4-1925-41fb-90ab-4887024bfa8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "98bbe9b4-1925-41fb-90ab-4887024bfa8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d082a36-9a63-4310-8883-718175b8d138",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 14
}