{
    "id": "7ee7392b-25ad-4ba5-a754-7eaa586d623e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solace_sitting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 46,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "836771d3-bd50-47b3-9fa5-85e8b12b74cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee7392b-25ad-4ba5-a754-7eaa586d623e",
            "compositeImage": {
                "id": "a8fbd30e-ae0e-4863-b941-b28577b64f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "836771d3-bd50-47b3-9fa5-85e8b12b74cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f301dd25-893c-470f-ac84-9d95dd986a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "836771d3-bd50-47b3-9fa5-85e8b12b74cd",
                    "LayerId": "b6155c72-d3ac-4c58-ba52-535da674ba2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b6155c72-d3ac-4c58-ba52-535da674ba2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ee7392b-25ad-4ba5-a754-7eaa586d623e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}