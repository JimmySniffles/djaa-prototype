{
    "id": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boy_dodge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1af921eb-4f61-483b-a05f-090791b47201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "87bcc451-1b86-4741-b373-69c6ffe3087b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af921eb-4f61-483b-a05f-090791b47201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3979756d-218c-410d-bbae-8acb05efe9fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af921eb-4f61-483b-a05f-090791b47201",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "85e8e897-9898-4e9d-82aa-c4e4386d1eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "e99d0d6a-fa4a-4ba9-bd06-b8ff2212b01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e8e897-9898-4e9d-82aa-c4e4386d1eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1529f433-2543-46bc-8ed7-958275f48cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e8e897-9898-4e9d-82aa-c4e4386d1eda",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "28fe3e4d-fec1-422a-9833-c37a2ffd95f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "3ea0d521-90e7-42fc-bcf5-f27962d67f6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28fe3e4d-fec1-422a-9833-c37a2ffd95f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87aba444-0bd7-4dd8-8dcc-4bde6ad45de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28fe3e4d-fec1-422a-9833-c37a2ffd95f3",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "6655c0c8-7e6b-46a1-8618-9f40d87d4430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "73223b6d-3738-48c6-99fe-401c98a3bf88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6655c0c8-7e6b-46a1-8618-9f40d87d4430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb34eec7-1008-45ab-9848-13a950d435e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6655c0c8-7e6b-46a1-8618-9f40d87d4430",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "0bdf5ae7-507e-4964-aed1-35537b3363f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "1151c666-7f2b-4b41-b486-24cde0744cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdf5ae7-507e-4964-aed1-35537b3363f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcdfe4e-71b3-48da-8a1a-36e048dd160c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdf5ae7-507e-4964-aed1-35537b3363f9",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "4faa844c-45b0-4ec8-afc9-a51bb5052a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "d5de2aad-0739-4158-b870-2286e6808f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4faa844c-45b0-4ec8-afc9-a51bb5052a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb51030-2c05-4e26-9efc-6449d7b07ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4faa844c-45b0-4ec8-afc9-a51bb5052a3f",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "94ad0f1c-9a91-46d0-8ef1-7cae363453d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "1d4ea844-b4db-4076-a02b-9dd184eda980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ad0f1c-9a91-46d0-8ef1-7cae363453d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3716e73-4f97-45fa-a3ad-757164ba9b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ad0f1c-9a91-46d0-8ef1-7cae363453d7",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        },
        {
            "id": "d8abe72e-92c4-4ecf-91a9-6cd63fd6264c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "compositeImage": {
                "id": "113f257c-062f-49ae-9ba6-9787ff10d220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8abe72e-92c4-4ecf-91a9-6cd63fd6264c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf704de2-034d-4bea-bb7b-83e806b8294a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8abe72e-92c4-4ecf-91a9-6cd63fd6264c",
                    "LayerId": "6556cbea-6093-4263-adad-13da7956bb9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6556cbea-6093-4263-adad-13da7956bb9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49949a5b-0422-4c70-b836-bf79aa3e4b87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}