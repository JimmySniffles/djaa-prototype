{
    "id": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FoN_clb_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69654c8f-86a2-4807-8452-aacd900e90c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "27e69163-683d-4ce3-a7ad-c2b082819e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69654c8f-86a2-4807-8452-aacd900e90c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0411264-3bdc-46b5-ac16-4f2346f4c47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69654c8f-86a2-4807-8452-aacd900e90c2",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "3b057522-940a-4670-91b1-7459dc4c97de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69654c8f-86a2-4807-8452-aacd900e90c2",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        },
        {
            "id": "bdbe5825-d39b-4299-bff2-16f97ca95cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "21796bb4-0740-455f-8d6e-f29e2535145e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbe5825-d39b-4299-bff2-16f97ca95cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f78973-91f0-4de3-859a-7529ca7a1e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbe5825-d39b-4299-bff2-16f97ca95cd8",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "21f6b301-0eb8-4896-a323-2fbe318b76e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbe5825-d39b-4299-bff2-16f97ca95cd8",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        },
        {
            "id": "bcebc2f4-261a-42a4-ad09-52b4f56ae87b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "24b34c51-5c9a-4a99-b7f3-b6485a83ceea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcebc2f4-261a-42a4-ad09-52b4f56ae87b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e92927-4bb9-4807-92a4-68a95fa6d234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcebc2f4-261a-42a4-ad09-52b4f56ae87b",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "24a3520c-3f96-4d94-83bd-74596bb0e625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcebc2f4-261a-42a4-ad09-52b4f56ae87b",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        },
        {
            "id": "2b7c2b65-b2cd-44a6-8f02-2beab1e267a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "513c845d-4424-4d3d-b0d4-6a87a1ed9928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7c2b65-b2cd-44a6-8f02-2beab1e267a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0111b3d2-dfde-4b8a-81be-1481a419c7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7c2b65-b2cd-44a6-8f02-2beab1e267a6",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "b441e70d-1fc3-4d55-9a46-370c857422c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7c2b65-b2cd-44a6-8f02-2beab1e267a6",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        },
        {
            "id": "b6b9b693-b7df-424e-8fac-93f13d400149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "ec142de7-8c78-4a04-80f7-e90eb03f0495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b9b693-b7df-424e-8fac-93f13d400149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b642961-e6d1-42c9-8fe5-18fe02ee3455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b9b693-b7df-424e-8fac-93f13d400149",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "2dbda2b5-ea1c-48ec-8c17-8987c9970768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b9b693-b7df-424e-8fac-93f13d400149",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        },
        {
            "id": "0fafb761-7fea-462c-945d-3a025423cc97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "compositeImage": {
                "id": "9ac402c4-de76-4763-b03a-9039c6dba05e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fafb761-7fea-462c-945d-3a025423cc97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbea4850-26f7-440a-902d-adf28d662f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fafb761-7fea-462c-945d-3a025423cc97",
                    "LayerId": "7f5de8f1-e1b2-42be-b503-7455892b20ab"
                },
                {
                    "id": "870a701b-5f04-45b4-b966-e8d8f69b8dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fafb761-7fea-462c-945d-3a025423cc97",
                    "LayerId": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a70c4638-dc5c-4061-bd66-e992aa2f1fb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7f5de8f1-e1b2-42be-b503-7455892b20ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2b06986-cde4-4d55-9c6c-05e9671fdc39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}