{
    "id": "85021829-7467-4c2d-b5a3-89c469c1eda7",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Draw_Trail",
    "IncludedResources": [
        "Sprites\\spr_zigzag",
        "Backgrounds\\bck_grid",
        "Scripts\\draw_trail",
        "Scripts\\trail_init",
        "Scripts\\draw_trail_ext",
        "Scripts\\trail_calculate",
        "Scripts\\trail_destroy",
        "Objects\\obj_trail1",
        "Objects\\obj_trail_ext1",
        "Objects\\obj_trail2",
        "Objects\\obj_trail_ext2",
        "Objects\\obj_spawner",
        "Rooms\\room_example",
        "Included Files\\Instructions.pdf"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 123146089894126,
    "date": "2019-24-05 10:03:53",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.killer.drawtrail",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.6"
}